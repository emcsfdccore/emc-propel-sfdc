<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Comment__c</fullName>
        <externalId>false</externalId>
        <label>Comment/Rationale</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>CourseName__c</fullName>
        <externalId>false</externalId>
        <formula>Course__r.Course_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CourseName</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CourseNumber__c</fullName>
        <externalId>false</externalId>
        <formula>Course__r.Course_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CourseNumber</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Course__c</fullName>
        <externalId>false</externalId>
        <label>Course</label>
        <referenceTo>TEC_Course__c</referenceTo>
        <relationshipLabel>Projects</relationshipLabel>
        <relationshipName>Projects</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Development_Effort__c</fullName>
        <externalId>false</externalId>
        <label>Development Effort</label>
        <picklist>
            <picklistValues>
                <fullName>No development</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Partial development</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Full Development</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Development_Type__c</fullName>
        <externalId>false</externalId>
        <label>Development Type</label>
        <picklist>
            <picklistValues>
                <fullName>30 Day Update</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Discontinue</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>New Asset</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>New Asset 3rd Party</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>New Asset Consolidation</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>New Asset Localization</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>New Asset Modality</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>New Asset Replacement</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Update</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Update and Rename</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Update Localization</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>true</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Development_percent__c</fullName>
        <externalId>false</externalId>
        <label>Development percent</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Owner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Project Owner</label>
        <referenceTo>User</referenceTo>
        <relationshipName>ProjectOwner</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product_Version__c</fullName>
        <externalId>false</externalId>
        <label>Product Version</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Project_Status__c</fullName>
        <externalId>false</externalId>
        <label>Project Status</label>
        <picklist>
            <picklistValues>
                <fullName>Cancelled</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Completed</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Hold</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ID-Review Submitted</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>In Design</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>In Development</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>In ID-Review</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>In Localization</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>In Production</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>L10n Review</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>L10n Sent</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Pending</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Production Complete</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Production Submitted</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>VILT Duplication</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>VILT Encryption</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>VILT Logo Edit</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>VILT Production</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>VILT Ready to Build</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>VILT ReMaster</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>VILT Storyline Conversion</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>true</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Revision__c</fullName>
        <externalId>false</externalId>
        <label>Revision #</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <label>Project</label>
    <listViews>
        <fullName>All_Projects</fullName>
        <columns>NAME</columns>
        <columns>Owner__c</columns>
        <columns>Project_Status__c</columns>
        <columns>Course__c</columns>
        <columns>CourseNumber__c</columns>
        <columns>CourseName__c</columns>
        <columns>Start_Date__c</columns>
        <columns>End_Date__c</columns>
        <columns>Product_Version__c</columns>
        <columns>Comment__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <label>All Projects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Discontinue</fullName>
        <columns>NAME</columns>
        <columns>Course__c</columns>
        <columns>Owner__c</columns>
        <columns>Project_Status__c</columns>
        <columns>Start_Date__c</columns>
        <columns>End_Date__c</columns>
        <columns>Product_Version__c</columns>
        <columns>Comment__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Development_Type__c</field>
            <operation>equals</operation>
            <value>Discontinue</value>
        </filters>
        <filters>
            <field>Project_Status__c</field>
            <operation>notEqual</operation>
            <value>Cancelled</value>
        </filters>
        <filters>
            <field>End_Date__c</field>
            <operation>greaterThan</operation>
            <value>LAST_N_DAYS:30</value>
        </filters>
        <label>Discontinue(GT Today-30)</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Localization_Active</fullName>
        <columns>NAME</columns>
        <columns>Course__c</columns>
        <columns>Owner__c</columns>
        <columns>Project_Status__c</columns>
        <columns>Start_Date__c</columns>
        <columns>End_Date__c</columns>
        <columns>Product_Version__c</columns>
        <columns>Comment__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Development_Type__c</field>
            <operation>contains</operation>
            <value>New Asset Localization</value>
        </filters>
        <filters>
            <field>Project_Status__c</field>
            <operation>notEqual</operation>
            <value>Completed</value>
        </filters>
        <filters>
            <field>Project_Status__c</field>
            <operation>notEqual</operation>
            <value>Cancelled</value>
        </filters>
        <label>Localization (Active)</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Project Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Projects</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Project_Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Start_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>End_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Product_Version__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Comment__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Revision__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Comment__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Project_Status__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>Comment__c</lookupFilterFields>
        <lookupFilterFields>Project_Status__c</lookupFilterFields>
        <lookupFilterFields>CREATEDBY_USER</lookupFilterFields>
        <lookupFilterFields>CREATED_DATE</lookupFilterFields>
        <lookupPhoneDialogsAdditionalFields>Project_Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Comment__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>Project_Status__c</searchFilterFields>
        <searchFilterFields>Comment__c</searchFilterFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>CREATED_DATE</searchFilterFields>
        <searchFilterFields>CREATEDBY_USER</searchFilterFields>
        <searchResultsAdditionalFields>Project_Status__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>ProjectEndDate</fullName>
        <active>true</active>
        <errorConditionFormula>Start_Date__c &gt;  End_Date__c</errorConditionFormula>
        <errorDisplayField>End_Date__c</errorDisplayField>
        <errorMessage>End Date cannot be before Start Date.</errorMessage>
    </validationRules>
</CustomObject>
