<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>quotes numbers manually entered by services</description>
    <enableActivities>false</enableActivities>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Case_Name__c</fullName>
        <description>To link Detailed Products to Case</description>
        <externalId>false</externalId>
        <label>Case Name</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Quotes</relationshipLabel>
        <relationshipName>Quotes</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>CreatedByGroup__c</fullName>
        <externalId>false</externalId>
        <label>Created By Group</label>
        <length>200</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Number_of_Configurations__c</fullName>
        <externalId>false</externalId>
        <label>Number of Configurations</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Quote_Type__c</fullName>
        <externalId>false</externalId>
        <label>Quote Type</label>
        <picklist>
            <picklistValues>
                <fullName>New</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Upgrade</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Mixed</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Versions__c</fullName>
        <externalId>false</externalId>
        <label>Versions</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Quote</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Quote Number</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Quotes</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Quote_Number_Format</fullName>
        <active>true</active>
        <description>This validation rule validates that the values entered into the field are numeric and 10 or less characters.</description>
        <errorConditionFormula>NOT ( 
OR ( 
LEN (Name) = 0, 
REGEX(Name,&quot;[0-9]{10}&quot;) 
) 
)</errorConditionFormula>
        <errorDisplayField>Name</errorDisplayField>
        <errorMessage>Quote numbers must be 10 numeric digits.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Validation_for_No_of_Config_Min_Max</fullName>
        <active>true</active>
        <errorConditionFormula>OR((Number_of_Configurations__c) &gt; 40, (Number_of_Configurations__c) &lt; 0)</errorConditionFormula>
        <errorMessage>Number of Configurations should be within the 0 to 40 range only</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Validation_for_Number_of_Configuration</fullName>
        <active>true</active>
        <description>Number of Configurations is required for for 

DxP Quote Create/Update
CxP Quote Creation/Update
Quote Access &amp; Duplication
Pricing</description>
        <errorConditionFormula>AND(


OR (

ISPICKVAL(Case_Name__r.Type, &quot;DxP Quote Creation/Update&quot;), 
ISPICKVAL(Case_Name__r.Type, &quot;CxP Quote Creation/Update&quot;), 
ISPICKVAL(Case_Name__r.Type, &quot;Quote Access &amp; Duplication&quot;)

), 

 ISNULL( Number_of_Configurations__c )


)</errorConditionFormula>
        <errorDisplayField>Number_of_Configurations__c</errorDisplayField>
        <errorMessage>Number of Configurations is a required field.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Validation_for_Quote_Type</fullName>
        <active>true</active>
        <description>quote type is required for for 

DxP Quote Create/Update
CxP Quote Creation/Update
Quote Access &amp; Duplication
Pricing</description>
        <errorConditionFormula>AND(

OR(

ISPICKVAL(Case_Name__r.Type, &quot;DxP Quote Creation/Update&quot;), 
ISPICKVAL(Case_Name__r.Type, &quot;CxP Quote Creation/Update&quot;), 
ISPICKVAL(Case_Name__r.Type, &quot;Quote Access &amp; Duplication&quot;),
ISPICKVAL(Case_Name__r.Type, &quot;Pricing&quot;)
),

ISBLANK( 

TEXT(Quote_Type__c )

)

)</errorConditionFormula>
        <errorDisplayField>Quote_Type__c</errorDisplayField>
        <errorMessage>Quote Type is a required field.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Validation_for_Versions</fullName>
        <active>true</active>
        <errorConditionFormula>NOT ( 
OR ( 
LEN (Versions__c ) = 0, 
REGEX(Versions__c ,&quot;[0-9]{1}&quot;), 
REGEX(Versions__c ,&quot;[0-9]{2}&quot;) 
)
)</errorConditionFormula>
        <errorDisplayField>Versions__c</errorDisplayField>
        <errorMessage>Versions must be 2 numeric digits</errorMessage>
    </validationRules>
</CustomObject>
