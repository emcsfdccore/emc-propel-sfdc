<!--  ==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 |  14/11/2014    Matthew Wilson Req#      Initial Mockup of Tech Connect Program Applicaiton form.
 |  24/11/2014    Matthew Wilson           New mockup to account for new fields and layout changes
                                    
 +==================================================================================================================**/ 
-->

<apex:page standardcontroller="Tech_Connect_Partner__c" extensions="techConnSubmissionController" showHeader="false" sidebar="false" standardStylesheets="true">
    <html>     
        <apex:include pageName="EMC_Header"/><br/>
            
            <apex:form >
                
                <apex:messages />
                <apex:sectionheader title="EMC Tech Connect Program" subtitle="New Tech Connect Program Application"/>
                <apex:pageblock mode="edit" title="Application Details">
                <apex:pageMessages />
                    <apex:pageblockbuttons >
                        <apex:commandbutton value="Submit" action="{!SubmissionAction}"/>
                        <apex:commandbutton value="Cancel" action="{!Cancel}"/>
                    </apex:pageblockbuttons>

            <!-- **********   [Record Type : Master ]   **********  -->
            <apex:outputpanel >
                
                <apex:pageblocksection title="Primary Applicant Information" showheader="true" dir="LTR" columns="2" >
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Company_Name__c}" required="true" tabOrderHint="1"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Country__c}" required="false" tabOrderHint="9"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.First_Name__c}" required="true" tabOrderHint="2"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.State_Province__c}" required="false" tabOrderHint="10"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Last_Name__c}" required="true" tabOrderHint="3"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Street__c}" required="false" tabOrderHint="11"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Role__c}" required="false" tabOrderHint="4"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.City__c}" required="false" tabOrderHint="12"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Email__c}" required="true" tabOrderHint="5"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Zip_Postal_Code__c}" required="false" tabOrderHint="13"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Phone_Number__c}" required="true" tabOrderHint="6"/>
                    <apex:pageblocksectionitem />
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Source__c}" required="true" tabOrderHint="7"/>
                    <apex:pageblocksectionitem />
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Source_Details__c}" required="false" tabOrderHint="8"/>
                    <apex:pageblocksectionitem />
                </apex:pageblocksection>
                <apex:pageblocksection title="Company Information" showheader="true" columns="2">
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Company_Website__c}" required="true" tabOrderHint="14"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Dedicated_Sales_Team_for__c}" required="false" tabOrderHint="18"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Year_Founded__c}" required="false" tabOrderHint="15"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Hosted_SaaS_Solution_Offering_Available__c}" required="false" tabOrderHint="19"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Number_of_Employees__c}" required="false" tabOrderHint="16"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Publically_Held__c}" required="false" tabOrderHint="20"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Previous_Year_s_Revenue__c}" required="true" tabOrderHint="17"/>
                    <apex:pageblocksectionitem />
                </apex:pageblocksection>
                <apex:pageblocksection title="Company Offerings" showheader="true" columns="2">
                    <apex:inputfield value="{!Tech_Connect_Partner__c.of_Customers_integrating_with_EMC__c}" required="false" tabOrderHint="21"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Estimated__c}" required="false" tabOrderHint="25"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Name_of_Product_Offering__c}" required="true" tabOrderHint="22"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Customer_Use_Case_for_Working_with_EMC__c}" required="false" tabOrderHint="26"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Product_Category__c}" required="true" tabOrderHint="23"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Industry_Apps_that_Offering_Works_with__c}" required="false" tabOrderHint="27"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Product_Offering_Description__c}" required="true" tabOrderHint="24"/>
                    <apex:pageblocksectionitem />
                </apex:pageblocksection>
                
                <apex:pageblocksection title="Product Information" showheader="true" columns="2">
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Interested_EMC_Products__c}" required="true" tabOrderHint="28"/>
                    <apex:pageblocksectionitem />
                </apex:pageblocksection>
                <apex:pageblocksection title="Additional Company Contacts" showheader="true" columns="2">
                    <apex:inputfield value="{!Tech_Connect_Partner__c.First_NameAlliance__c}" required="false" tabOrderHint="29"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.First_NameTechnical__c}" required="false" tabOrderHint="35"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Last_NameAlliance__c}" required="false" tabOrderHint="30"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Last_NameTechnical__c}" required="false" tabOrderHint="36"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.EmailAlliance__c}" required="false" tabOrderHint="31"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.EmailTechnical__c}" required="false" tabOrderHint="37"/>
                    
                    <!-- Add empty row to improve readability -->
                    <apex:pageBlockSectionItem > </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem > </apex:pageBlockSectionItem>
                    <!-- End empty row -->
                    
                    <apex:inputfield value="{!Tech_Connect_Partner__c.First_Name_Marketing__c}" required="false" tabOrderHint="32"/>
                    <apex:pageBlockSectionItem > </apex:pageBlockSectionItem>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Last_NameMarketing__c}" required="false" tabOrderHint="33"/>
                    <apex:pageBlockSectionItem > </apex:pageBlockSectionItem>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.EmailMarketing__c}" required="false" tabOrderHint="34"/>
                    <apex:pageBlockSectionItem > </apex:pageBlockSectionItem>
                </apex:pageblocksection>
                <apex:pageblocksection title="Sales Information" showheader="true" columns="2">
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Percentage_sold_Direct__c}" required="false" tabOrderHint="38"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.List_of_Major_Resellers__c}" required="false" tabOrderHint="42"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Percentage_sold_OEM__c}" required="false" tabOrderHint="39"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.List_of_Major_Technology_Partnerships__c}" required="false" tabOrderHint="43"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Percentage_sold_Reseller__c}" required="false" tabOrderHint="40"/>
                    <apex:pageblocksectionitem />
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Percentage_sold_Other__c}" required="false" tabOrderHint="41"/>
                    <apex:pageblocksectionitem />
                </apex:pageblocksection>
                <apex:pageblocksection title="Geography, Customer and Industry Information" showheader="true" columns="2">
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Percentage_of_Enterprise_Customers__c}" required="false" tabOrderHint="44"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Geographies_Served__c}" required="false" tabOrderHint="48"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Percent_of__c}" required="false" tabOrderHint="45"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Primary_Industry_Vertical__c}" required="false" tabOrderHint="49"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Percentage_of_Commercial_Customers__c}" required="false" tabOrderHint="46"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Secondary_Industry_Vertical__c}" required="false" tabOrderHint="50"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Percentage_of_SMB_Customers__c}" required="false" tabOrderHint="47"/>
                    <apex:inputfield value="{!Tech_Connect_Partner__c.Tertiary_Industry_Vertical__c}" required="false" tabOrderHint="51"/>
                </apex:pageblocksection>
            </apex:outputpanel>
        </apex:pageblock>
    </apex:form>
    
    </html>
</apex:page>