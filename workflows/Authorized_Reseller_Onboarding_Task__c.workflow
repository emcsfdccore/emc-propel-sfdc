<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Status_to_In_Complete</fullName>
        <field>Status__c</field>
        <literalValue>Incomplete</literalValue>
        <name>Change Status to In Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Assigned1</fullName>
        <field>Date_Assigned__c</field>
        <formula>CreatedDate</formula>
        <name>Update Date Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Completed</fullName>
        <field>Date_Completed__c</field>
        <formula>LastModifiedDate</formula>
        <name>Update Date Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Completed_Field</fullName>
        <field>Date_Completed__c</field>
        <formula>NOW()</formula>
        <name>Update Date Completed Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_Status</fullName>
        <field>Status__c</field>
        <literalValue>Complete</literalValue>
        <name>Update Task Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>complete_task</fullName>
        <field>Status__c</field>
        <literalValue>Complete</literalValue>
        <name>complete task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Complete task when rejected by distributor</fullName>
        <actions>
            <name>complete_task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Distributor_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Not Approved,Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>POB Status update if account is not found by Contract Manager</fullName>
        <actions>
            <name>Change_Status_to_In_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Account_Found_in_SFDC__c</field>
            <operation>equals</operation>
            <value>Not Found</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Date Assigned</fullName>
        <actions>
            <name>Update_Date_Assigned1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Date Completed</fullName>
        <actions>
            <name>Update_Date_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Partner On Boarding Task Status</fullName>
        <actions>
            <name>Update_Date_Completed_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Task_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12</booleanFilter>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.EMC_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Not Approved,Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Distributor_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Not Approved,Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Powerlink_Email_Sent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Account_added_on_grouping__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Account_Found_in_SFDC__c</field>
            <operation>equals</operation>
            <value>Found</value>
        </criteriaItems>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Account_Found_in_SFDC__c</field>
            <operation>equals</operation>
            <value>Created By TRACK</value>
        </criteriaItems>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Account_added_on_grouping__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Associations_Created__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Partner_Profile_Updated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.CXP_Email_Sent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Legal_Agreement_Copy_Sent_to_DMS__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Authorized_Reseller_Onboarding_Task__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
