<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Email</fullName>
        <description>Approval Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CSK_Email_Templates/Article_Approved</template>
    </alerts>
    <alerts>
        <fullName>CSK_Send_Notification_to_Author</fullName>
        <description>CSK - Send Notification to Author</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CSK_Email_Templates/CSK_Update_Validation_Status</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Email</fullName>
        <description>Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CSK_Email_Templates/Article_Rejection</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Requested_By_TSA</fullName>
        <field>Approval_Requested_By__c</field>
        <formula>$User.FirstName +&quot; &quot; + $User.LastName</formula>
        <name>Approval Requested By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Article_Created_Date_update_tsa</fullName>
        <field>Article_Created_Date__c</field>
        <formula>now()</formula>
        <name>Article Created Date update tsa</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Channels_Audience_30_40_2InterApp</fullName>
        <field>IsVisibleInApp</field>
        <literalValue>1</literalValue>
        <name>CSK Channels - Audience 30/40-2InterApp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Channels_Audience_30_40_2_Cust_Po</fullName>
        <field>IsVisibleInCsp</field>
        <literalValue>1</literalValue>
        <name>CSK Channels - Audience 30/40- 2 Cust Po</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Channels_Audience_30_40_2_Pub_Kno</fullName>
        <field>IsVisibleInPkb</field>
        <literalValue>0</literalValue>
        <name>CSK Channels - Audience 30/40- 2 Pub Kno</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Channels_Audience_30_40_2_part_po</fullName>
        <field>IsVisibleInPrm</field>
        <literalValue>0</literalValue>
        <name>CSK Channels - Audience 30/40- 2 part po</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Channels_Audience_30_40_Cust_Porta</fullName>
        <field>IsVisibleInCsp</field>
        <literalValue>0</literalValue>
        <name>CSK Channels - Audience 30/40 Cust Porta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Channels_Audience_30_40_Internal_A</fullName>
        <field>IsVisibleInApp</field>
        <literalValue>1</literalValue>
        <name>CSK Channels - Audience 30/40-Internal A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Channels_Audience_30_40_Part_Porta</fullName>
        <field>IsVisibleInPrm</field>
        <literalValue>0</literalValue>
        <name>CSK Channels - Audience 30/40 Part Porta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Channels_Audience_30_40_Pub_Kno_Ba</fullName>
        <field>IsVisibleInPkb</field>
        <literalValue>0</literalValue>
        <name>CSK Channels - Audience 30/40 Pub Kno Ba</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Clear_Article_Alert</fullName>
        <description>KNOWLEDGE: Clears the field value of the article alert field</description>
        <field>Required_Article_Information__c</field>
        <formula>&quot; Article has all required information. &quot;</formula>
        <name>CSK Clear Article Alert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_In_Customer_Portal</fullName>
        <description>if the Audience Field is Level 50, only the Internal App should ever be true - all other channels should be false</description>
        <field>IsVisibleInCsp</field>
        <literalValue>0</literalValue>
        <name>CSK In Customer Portal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_In_PartnerPortal</fullName>
        <description>if the Audience Field is Level 50, only the Internal App should ever be true - all other channels should be false</description>
        <field>IsVisibleInPrm</field>
        <literalValue>0</literalValue>
        <name>CSK In PartnerPortal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Internal_App</fullName>
        <description>if the Audience Field is Level 50, only the Internal App should ever be true - all other channels should be false</description>
        <field>IsVisibleInApp</field>
        <literalValue>1</literalValue>
        <name>CSK Internal App</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Populate_Article_Alert</fullName>
        <description>KNOWLEDGE: Populates a message in the alert field</description>
        <field>Required_Article_Information__c</field>
        <formula>&quot;&lt;ul&gt;&lt;li&gt; All articles must have a Product added prior to submitting for approval.&lt;/li&gt;&quot; &amp; 
&quot;&lt;li&gt; To add a product, click ASSIGN ARTICLE METADATA and populate the PRODUCT field using the product selector. Click QUICK SAVE on the metadata page, and return to this view to submit for approval.&lt;/li&gt;&lt;/ul&gt;&quot;</formula>
        <name>CSK Populate Article Alert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Populate_Originally_Authored_By_Us</fullName>
        <field>Originally_Created_By__c</field>
        <formula>CreatedBy.FirstName + &apos; &apos; + CreatedBy.LastName</formula>
        <name>CSK - Populate Originally Authored By Us</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Update_Internal_App</fullName>
        <field>IsVisibleInApp</field>
        <literalValue>1</literalValue>
        <name>CSK - Update Internal App</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Update_Next_Review_Date</fullName>
        <field>Next_Review_Date__c</field>
        <formula>Requested_Publish_Date__c + 180</formula>
        <name>CSK - Update Next Review Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Update_Requested_Publish_Date</fullName>
        <field>Requested_Publish_Date__c</field>
        <formula>NOW()</formula>
        <name>CSK - Update Requested Publish Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Update_Validation_Status_Updated</fullName>
        <field>Validation_Status_Updated__c</field>
        <literalValue>No</literalValue>
        <name>CSK - Update Validation Status Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_in_Public_KnowledgeBase</fullName>
        <description>if the Audience Field is Level 50, only the Internal App should ever be true - all other channels should be false</description>
        <field>IsVisibleInPkb</field>
        <literalValue>0</literalValue>
        <name>CSK in Public KnowledgeBase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Create_Article_Metadata_Link</fullName>
        <field>Article_Metadata_Link__c</field>
        <formula>&quot;&lt;a href=&apos;/apex/KB_T3PickerScreen?aId=&quot; &amp;  Id &amp;  &quot;&amp;aType=&quot; &amp;  SUBSTITUTE(SUBSTITUTE(TEXT(ArticleType), &quot;__kav&quot;, &quot;&quot;), &quot;_&quot;, &quot; &quot;)  &amp; &quot; &apos;  target=&apos;_blank&apos; &gt;Assign Article Metadata&lt;/a&gt;&quot;</formula>
        <name>Create Article Metadata Link</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Create_OPT_Link</fullName>
        <field>Link_to_OPT__c</field>
        <formula>&quot;&lt;a href=&apos;http://opt.eng.emc.com/Front/OPTFramework2.asp&apos; target=&apos;_blank&apos;&gt;OPT Application&lt;/a&gt;&quot;</formula>
        <name>Create OPT Link</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>In_KCS_Review</fullName>
        <field>ValidationStatus</field>
        <literalValue>In KCS Review</literalValue>
        <name>In KCS Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>In_Technical_Review</fullName>
        <description>This will update the Validation Status to In Technical Review</description>
        <field>ValidationStatus</field>
        <literalValue>In Technical Review</literalValue>
        <name>In Technical Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KCS_Approved</fullName>
        <description>This will update the validation status to KCS Approved.</description>
        <field>ValidationStatus</field>
        <literalValue>Final Approved</literalValue>
        <name>KCS Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rework</fullName>
        <description>This will update the validation status to Rework.</description>
        <field>ValidationStatus</field>
        <literalValue>Rework</literalValue>
        <name>Rework</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish_As_New</fullName>
        <action>PublishAsNew</action>
        <description>This will publish the article as a new version.</description>
        <label>Publish As New</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <outboundMessages>
        <fullName>Published_TSA_Article</fullName>
        <apiVersion>27.0</apiVersion>
        <description>KNOWLEDGE: Outbound message to fire once article is published</description>
        <endpointUrl>http://soagatewaynoint.404.com/sst/runtime.asvc/com.actional.intermediary.aicKbauttsasfdc?WSDL</endpointUrl>
        <fields>ArticleType</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>sfdcbatchadmin1@emc.com</integrationUser>
        <name>Published TSA Article</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>CSK - Create Article Metadata Link</fullName>
        <actions>
            <name>Create_Article_Metadata_Link</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Create_OPT_Link</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>KNOWLEDGE: Creates a link to Article Metadata page for TSA articles.</description>
        <formula>1==1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - OutBoundMessage</fullName>
        <actions>
            <name>Published_TSA_Article</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>KNOWLEDGE: Sends an outbound message every time a TSA article is saved.</description>
        <formula>NOT(ISNULL( UrlName ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Populate Originally Authored By User</fullName>
        <actions>
            <name>Article_Created_Date_update_tsa</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CSK_Populate_Originally_Authored_By_Us</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>KNOWLEDGE: Populates author&apos;s name on TSA articles</description>
        <formula>1==1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Send Outbound Message</fullName>
        <actions>
            <name>Published_TSA_Article</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>KNOWLEDGE: Sends an outbound message if IsVisibleInCsp checkbox is true on TSA articles</description>
        <formula>IsVisibleInCsp = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Update Internal App</fullName>
        <actions>
            <name>CSK_Update_Internal_App</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TSA__kav.IsVisibleInApp</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>KNOWLEDGE: All TSA articles should be visible in Internal App.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Update Next Review Date</fullName>
        <actions>
            <name>CSK_Update_Next_Review_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>KNOWLEDGE: Updates Next Review Date to six months from Requested Publish Date for TSA articles.</description>
        <formula>ISCHANGED( Requested_Publish_Date__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Update Requested Publish Date</fullName>
        <actions>
            <name>CSK_Update_Requested_Publish_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TSA__kav.Requested_Publish_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>KNOWLEDGE: If Requested Publish Date is blank, update it to current date/time for TSA articles.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSK Channels - Audience 30%2F40</fullName>
        <actions>
            <name>CSK_Channels_Audience_30_40_2InterApp</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CSK_Channels_Audience_30_40_2_Cust_Po</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CSK_Channels_Audience_30_40_2_Pub_Kno</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CSK_Channels_Audience_30_40_2_part_po</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TSA__kav.Audience__c</field>
            <operation>equals</operation>
            <value>Level 40 = Employees and Partners,Level 30 = Customers</value>
        </criteriaItems>
        <description>KNOWLEDGE: If Audience level is 40 or 30, then Customer Portal AND Internal App should be true - all other values false</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK Channels Audience -50</fullName>
        <actions>
            <name>CSK_In_Customer_Portal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CSK_In_PartnerPortal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CSK_Internal_App</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CSK_in_Public_KnowledgeBase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TSA__kav.Audience__c</field>
            <operation>equals</operation>
            <value>Level 50 = Restricted</value>
        </criteriaItems>
        <description>KNOWLEDGE: If Audience level is 50, then Internal App should be true - all other values false</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK Clear Article Alert</fullName>
        <actions>
            <name>CSK_Clear_Article_Alert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>KNOWLEDGE: Populates the Required Article Information field with a confirmation that the article is ready for approval when a product is selected. This is reevaluated on every save.</description>
        <formula>NOT(  AND(  ISBLANK( T3_Product__c ),  ISBLANK( EMC_Internal_Application__c )  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK Populate Article Alert</fullName>
        <actions>
            <name>CSK_Populate_Article_Alert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>KNOWLEDGE: Populates the Required Article Information field with an alert if no product is selected. This is reevaluated on every save.</description>
        <formula>AND(  ISBLANK( T3_Product__c ),  ISBLANK( EMC_Internal_Application__c )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK Validation Status Check</fullName>
        <actions>
            <name>In_Technical_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Knowledge Author User,Knowledge Reviewer User</value>
        </criteriaItems>
        <criteriaItems>
            <field>TSA__kav.ValidationStatus</field>
            <operation>equals</operation>
            <value>Final Approved,In KCS Review,Technically Approved</value>
        </criteriaItems>
        <description>KNOWLEDGE: This rule will ensure that Reviewers &amp; Authors are not allowed to save articles in a Final Approved/In KCS Review/Technically Approved Status.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
