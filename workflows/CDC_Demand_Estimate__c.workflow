<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Request_Status_Project</fullName>
        <description>Updates the related Demand Request with the status from the primary estimate record.</description>
        <field>Project_Status__c</field>
        <formula>TEXT(Status__c)</formula>
        <name>Update Request Status (Project)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Related_Demand_Request__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>CDC DMT - Update Request CI Status</fullName>
        <active>false</active>
        <description>Updates the related Demand Request with the estimate status</description>
        <formula>AND( 
	ispickval(Related_Demand_Request__r.Initial_Work_Type_Definition__c, &quot;CI / QH&quot;),
	(	
		OR(
			(ischanged(Status__c)), 
			isnew()
		)
	)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CDC DMT - Update Request Project Status</fullName>
        <actions>
            <name>Update_Request_Status_Project</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the related Demand Request with the estimate status</description>
        <formula>AND( 
	ispickval(Related_Demand_Request__r.Initial_Work_Type_Definition__c, &quot;Project&quot;),
	(	
		OR(
			(ischanged(Status__c)), 
			isnew()
		)
	)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
