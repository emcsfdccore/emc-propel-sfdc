<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Reset_Outbound_Message_Flag</fullName>
        <description>Reset Outbound Message Flag</description>
        <field>SendOutboundNotification__c</field>
        <literalValue>0</literalValue>
        <name>Reset Outbound Message Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>PRM_DealReg_Acc_Assoc_CXP_Mapping_Msg</fullName>
        <apiVersion>21.0</apiVersion>
        <description>when a delete Account Association,CXP Mapping record is created or updated deleted.</description>
        <endpointUrl>http://soagateway-test.emc.com/sst/runtime.asvc/com.actional.intermediary.aicNotifyAttributeServiceEMCSFDC_DES?WSDL</endpointUrl>
        <fields>Id</fields>
        <fields>Integration_Operation__c</fields>
        <fields>Key1__c</fields>
        <fields>Key2__c</fields>
        <fields>Key3__c</fields>
        <fields>RecordId__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>kothawade_prasad@emc.com.prod</integrationUser>
        <name>PRM Child Object Update</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>PRM Deal Reg Account Association%2FCXP Mapping Integration</fullName>
        <actions>
            <name>Reset_Outbound_Message_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRM_DealReg_Acc_Assoc_CXP_Mapping_Msg</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Outbound_Message_Log__c.RecordId__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Message_Log__c.SendOutboundNotification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>PRM Deal Reg Account Association/CXP Mapping Integration</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PROPEL PRM Account Association Mapping Integration</fullName>
        <actions>
            <name>Reset_Outbound_Message_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRM_DealReg_Acc_Assoc_CXP_Mapping_Msg</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Outbound_Message_Log__c.RecordId__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Message_Log__c.SendOutboundNotification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Message_Log__c.Integration_Operation__c</field>
            <operation>contains</operation>
            <value>Account Association</value>
        </criteriaItems>
        <description>Sends the OB message when a Outbound Message Log record is created for Account Association object.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PROPEL PRM Partner Quoting Relation Mapping Integration</fullName>
        <actions>
            <name>Reset_Outbound_Message_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRM_DealReg_Acc_Assoc_CXP_Mapping_Msg</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Outbound_Message_Log__c.RecordId__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Message_Log__c.SendOutboundNotification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Message_Log__c.Integration_Operation__c</field>
            <operation>contains</operation>
            <value>Partner Quoting Relationships</value>
        </criteriaItems>
        <description>Sends the OB message when a Outbound Message Log record is created for  Partner Quoting Relation object.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PROPEL PRM Partner Service Enabled Product Mapping Integration</fullName>
        <actions>
            <name>Reset_Outbound_Message_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRM_DealReg_Acc_Assoc_CXP_Mapping_Msg</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Outbound_Message_Log__c.RecordId__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Message_Log__c.SendOutboundNotification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Message_Log__c.Integration_Operation__c</field>
            <operation>contains</operation>
            <value>Partner Service Enabled Products</value>
        </criteriaItems>
        <description>Sends the OB message when a Outbound Message Log record is created for  Partner Service Enabled Product object.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PROPEL PRM Partner Type Attribute Mapping Integration</fullName>
        <actions>
            <name>Reset_Outbound_Message_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRM_DealReg_Acc_Assoc_CXP_Mapping_Msg</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Outbound_Message_Log__c.RecordId__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Message_Log__c.SendOutboundNotification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Message_Log__c.Integration_Operation__c</field>
            <operation>contains</operation>
            <value>Partner Type Attributes</value>
        </criteriaItems>
        <description>Sends the OB message when a Outbound Message Log record is created for  Partner Type Attribute object.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
