<?xml version="1.0" encoding="UTF-8"?>
<<<<<<< HEAD
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata"/>

=======
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PROPEL_Populate_Deletion_UniqueKey</fullName>
        <description>PROPEL Populate Deletion UniqueKey to determine the combination uniqueness in database.</description>
        <field>Deletion_UniqueKey__c</field>
        <formula>Partner_Account__r.Partner_Quoting_Account_UCID__c &amp; &apos; - &apos; &amp;  Related_Account__c &amp; &apos; - &apos; &amp;  Text(Relationship_Type__c)</formula>
        <name>PROPEL Populate Deletion UniqueKey</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PROPEL_Populate_Relationship_Key</fullName>
        <description>Populating the relationship Key field by concatenating Partner Account Id, Related Account ID and Relationship Type field</description>
        <field>Relationship_Key__c</field>
        <formula>Partner_Account__c   &amp; &apos; - &apos; &amp; Related_Account__c  &amp; &apos; - &apos;  &amp;  TEXT(Relationship_Type__c)</formula>
        <name>PROPEL Populate Relationship Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PROPEL Partner Quoting Rel OR Update</fullName>
        <actions>
            <name>PROPEL_Populate_Deletion_UniqueKey</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PROPEL_Populate_Relationship_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Partner_Quoting_Relationship__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>PROPEL - Whenever a new Partner quoting Relationship record is created or updated, fire this workflow rule</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
>>>>>>> develop
