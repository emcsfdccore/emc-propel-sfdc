<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Activity_Type_On_Event</fullName>
        <description>This field update populates Activity Type with Type field</description>
        <field>Activity_Type__c</field>
        <formula>TEXT(Type)</formula>
        <name>Populate Activity Type On Event</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EventType_Other</fullName>
        <field>Type</field>
        <literalValue>Other</literalValue>
        <name>Update_EventType_Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isEvent</fullName>
        <description>This Field Update action Populates Date Accepted Field when a Lead is accepted</description>
        <field>Event__c</field>
        <literalValue>1</literalValue>
        <name>isEvent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Event_Type_Null</fullName>
        <actions>
            <name>Update_EventType_Other</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.Type</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Event Type field is Null when the record is created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate Activity Type Event</fullName>
        <actions>
            <name>Populate_Activity_Type_On_Event</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule populates the Activity type custom field.</description>
        <formula>OR(ISCHANGED(Type),ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>isNew</fullName>
        <actions>
            <name>isEvent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.OwnerId</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
