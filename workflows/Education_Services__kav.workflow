<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Email</fullName>
        <description>Approval Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ed_Service_Templates/Approval_Summary_Notification_Quality_Review</template>
    </alerts>
    <alerts>
        <fullName>Approval_Reminder_for_Functional_queue</fullName>
        <description>Approval Reminder for Functional queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>ES_Functional_Technical_Review</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ed_Service_Templates/Approval_Reminder_Notification_Functional</template>
    </alerts>
    <alerts>
        <fullName>Approval_Reminder_for_Quality_queue</fullName>
        <description>Approval Reminder for Quality queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>Ed_Services_Quality_KCS_Review</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ed_Service_Templates/Approval_Reminder_Notification_Quality</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_to_queue</fullName>
        <description>Email notification to queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>ES_Functional_Technical_Review</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ed_Service_Templates/Approval_Review_Notification_Functional_Review</template>
    </alerts>
    <alerts>
        <fullName>Functional_Approval</fullName>
        <description>Functional Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>Ed_Services_Quality_KCS_Review</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ed_Service_Templates/Approval_Review_Notification_Quality_Review</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Email</fullName>
        <description>Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ed_Service_Templates/Approval_Summary_Notification_Article_Rejection</template>
    </alerts>
    <fieldUpdates>
        <fullName>Final_Approval</fullName>
        <field>ValidationStatus</field>
        <literalValue>Final Approved</literalValue>
        <name>Final Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KCS_Review</fullName>
        <field>ValidationStatus</field>
        <literalValue>In KCS Review</literalValue>
        <name>KCS Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rewok_required</fullName>
        <field>ValidationStatus</field>
        <literalValue>Rework</literalValue>
        <name>Rewok required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Technical_Review</fullName>
        <field>ValidationStatus</field>
        <literalValue>In Technical Review</literalValue>
        <name>Technical Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish</fullName>
        <action>PublishAsNew</action>
        <label>Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>Approval Reminder %28Functional%29</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Education_Services__kav.ValidationStatus</field>
            <operation>equals</operation>
            <value>In Technical Review</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Approval_Reminder_for_Functional_queue</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Approval Reminder %28Quality%29</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Education_Services__kav.ValidationStatus</field>
            <operation>equals</operation>
            <value>In KCS Review</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Approval_Reminder_for_Quality_queue</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
