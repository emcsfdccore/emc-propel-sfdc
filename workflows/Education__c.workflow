<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Fall_Out_Queue</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Fall_Out_Queue</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Fall Out Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Master</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Master</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Master</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Flip Record Type To Fall out Queue</fullName>
        <actions>
            <name>Update_Record_Type_to_Fall_Out_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Education__c.OwnerId</field>
            <operation>startsWith</operation>
            <value>Education</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type To Master</fullName>
        <actions>
            <name>Update_Record_Type_to_Master</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Education__c.OwnerId</field>
            <operation>notContain</operation>
            <value>Education</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
