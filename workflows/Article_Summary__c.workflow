<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CSK_Populate_Article_Number</fullName>
        <field>Article_Number__c</field>
        <formula>Name</formula>
        <name>CSK - Populate Article Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CSK - Populate Article Number</fullName>
        <actions>
            <name>CSK_Populate_Article_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Article_Summary__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>KNOWLEDGE: Populates Article Number on Article Summary records.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
