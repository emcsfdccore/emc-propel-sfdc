<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Application_has_been_submitted</fullName>
        <ccEmails>techconnect@emc.com</ccEmails>
        <description>New Application has been submitted</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>techconnect@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Tech_Connect_Email_Templates/New_Application_Submitted_with_Tech_Logo</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Auto_Notification</fullName>
        <ccEmails>techconnect@emc.com</ccEmails>
        <description>Rejection Auto Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>techconnect@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Tech_Connect_Email_Templates/Rejection_Notification_Template_with_Tech_Logo</template>
    </alerts>
    <alerts>
        <fullName>Standard_Invite_Notification</fullName>
        <ccEmails>techconnect@emc.com</ccEmails>
        <description>Standard Invite Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>techconnect@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Tech_Connect_Email_Templates/Standard_Invite_Email_wtih_Tech_Logo</template>
    </alerts>
    <alerts>
        <fullName>Tech_Connect_Application_is_active_auto_notification</fullName>
        <ccEmails>techconnect@emc.com</ccEmails>
        <description>Tech Connect Application is active auto notification</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Primary_Relationship_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>techconnect@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Tech_Connect_Email_Templates/Active_Notification_Email_with_Tech_Logo</template>
    </alerts>
    <alerts>
        <fullName>Termination_Auto_Notification</fullName>
        <ccEmails>techconnect@emc.com</ccEmails>
        <ccEmails>maya.sudarsana@emc.com</ccEmails>
        <description>Termination Auto Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Relationship_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>techconnect@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Tech_Connect_Email_Templates/Termination_Auto_Notification_with_Tech_Logo</template>
    </alerts>
    <rules>
        <fullName>New Application Submitted</fullName>
        <actions>
            <name>New_Application_has_been_submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Tech_Connect_Partner__c.Application_Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Auto-notification is sent to to applicant when a new application is submitted from emc.com</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Rejection Auto Notification</fullName>
        <actions>
            <name>Rejection_Auto_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Tech_Connect_Partner__c.Application_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Auto Notification that is sent out when the status of the application changes to Rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Standard Invite Letter</fullName>
        <actions>
            <name>Standard_Invite_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Tech_Connect_Partner__c.Application_Status__c</field>
            <operation>equals</operation>
            <value>Invited (Standard)</value>
        </criteriaItems>
        <description>Auto-notification is sent when the application status is set to Invited (Standard)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Tech Connect Application Activated</fullName>
        <actions>
            <name>Tech_Connect_Application_is_active_auto_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Tech_Connect_Partner__c.Application_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <description>This is the auto notification that is sent when the tech connect application is activiated, when status changes to Active</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Termination Auto Notification</fullName>
        <actions>
            <name>Termination_Auto_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Tech_Connect_Partner__c.Application_Status__c</field>
            <operation>equals</operation>
            <value>Terminated</value>
        </criteriaItems>
        <description>Notification that is sent to the Tech Connect Team when a partner has been terminated from the prpgram</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
