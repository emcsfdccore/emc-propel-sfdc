<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Trigger_Integration_in_batch</fullName>
        <apiVersion>27.0</apiVersion>
        <endpointUrl>https://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.aicKbautcustsfdc?WSDL</endpointUrl>
        <fields>Id</fields>
        <fields>Object_ID__c</fields>
        <fields>Object_Type__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>sfdcbatchadmin1@emc.com</integrationUser>
        <name>Trigger Integration in batch</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Trigger Integration when KAID is added</fullName>
        <actions>
            <name>Trigger_Integration_in_batch</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Knowledge_Integration_Trigger__c.Object_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>KNOWLEDGE: Sends an outbound message on creation of a Knowledge Integration Trigger record.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
