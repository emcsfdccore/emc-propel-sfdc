<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Email</fullName>
        <description>Approval Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CSK_Email_Templates/CSK_SecurityKB_Knowledge_Article_Approved</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Email</fullName>
        <description>Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CSK_Email_Templates/CSK_SecurityKB_Knowledge_Article_Rejection</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Requested_By_SecKB</fullName>
        <field>Approval_Requested_By__c</field>
        <formula>$User.FirstName +&quot; &quot; + $User.LastName</formula>
        <name>Approval Requested By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Article_Created_Date_update_securitykb</fullName>
        <field>Article_Created_Date__c</field>
        <formula>now()</formula>
        <name>Article Created Date update securitykb</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Channels_Audience_30_40_2_Cust_Po</fullName>
        <field>IsVisibleInCsp</field>
        <literalValue>1</literalValue>
        <name>CSK Channels - Audience 30/40- 2 Cust Po</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Channels_Audience_30_40_2_Pub_Kno</fullName>
        <field>IsVisibleInPkb</field>
        <literalValue>0</literalValue>
        <name>CSK Channels - Audience 30/40- 2 Pub Kno</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Channels_Audience_30_40_2_part_po</fullName>
        <field>IsVisibleInPrm</field>
        <literalValue>0</literalValue>
        <name>CSK Channels - Audience 30/40- 2 part po</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Clear_Article_Alert</fullName>
        <description>KNOWLEDGE: Clears the field value of the article alert field</description>
        <field>Required_Article_Information__c</field>
        <formula>&quot; Article has all required information. &quot;</formula>
        <name>CSK Clear Article Alert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_In_Customer_Portal</fullName>
        <field>IsVisibleInCsp</field>
        <literalValue>0</literalValue>
        <name>CSK In Customer Portal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_In_PartnerPortal</fullName>
        <description>if the Audience Field is Level 50, only the Internal App should ever be true - all other channels should be false</description>
        <field>IsVisibleInPrm</field>
        <literalValue>0</literalValue>
        <name>CSK In PartnerPortal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Populate_Article_Alert</fullName>
        <description>KNOWLEDGE: Populates a message in the alert field</description>
        <field>Required_Article_Information__c</field>
        <formula>&quot;&lt;ul&gt;&lt;li&gt; All articles must have a Product added prior to submitting for approval.&lt;/li&gt;&quot; &amp; 
&quot;&lt;li&gt; To add a product, click ASSIGN ARTICLE METADATA and populate the PRODUCT field using the product selector. Click QUICK SAVE on the metadata page, and return to this view to submit for approval.&lt;/li&gt;&lt;/ul&gt;&quot;</formula>
        <name>CSK Populate Article Alert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Populate_Originally_Authored_By_Us</fullName>
        <field>Originally_Created_By__c</field>
        <formula>CreatedBy.FirstName + &apos; &apos; + CreatedBy.LastName</formula>
        <name>CSK - Populate Originally Authored By Us</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Populate_Security_KB_Disclaimer</fullName>
        <field>Disclaimer__c</field>
        <formula>&quot;Note: &quot; &amp; &quot;WARNING! This KB solution is designed to assist Customer Support and other customer facing organizations in addressing concerns that may arise with listed issues from customers. Do not forward KB solution as is directly to the customer. Changes to this solution must ONLY be performed after review and approval by Vulnerability Response Champion(s) for listed product(s) and Product Security Response Center (PSRC). Contact PSRC at Security_Alert@emc.com with any questions.&quot; &amp; 

&quot;&lt;br&gt;&lt;div style=&quot;&amp;&quot;text-align:center;&quot;&amp;&quot;&gt;&lt;b&gt;EMC Confidential&lt;/b&gt;&lt;/div&gt;&quot;</formula>
        <name>CSK - Populate Security KB Disclaimer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Technical_Detail_3</fullName>
        <field>Technical_Details__c</field>
        <formula>&quot;The EMC product embeds the vulnerable code/component.  EMC is working on a solution to address the issue and will provide regular status updates.&quot;</formula>
        <name>CSK - Technical Detail - 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Technical_Detail_4</fullName>
        <field>Technical_Details__c</field>
        <formula>&quot;Provide details on patch, upgrade, downgrade, workaround, hot fix.&quot;</formula>
        <name>CSK - Technical Detail - 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Technical_Detail_5</fullName>
        <field>Technical_Details__c</field>
        <formula>&quot;The component is a prerequisite to run the EMC product, but there is no known or expected affect on the EMC product when applying the vendor supplied security patch.&quot;</formula>
        <name>CSK - Technical Detail - 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Technical_Detail_Clear</fullName>
        <field>Technical_Details__c</field>
        <name>CSK - Technical Detail - Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Technical_Details_1</fullName>
        <field>Technical_Details__c</field>
        <formula>&quot;False positive&quot;</formula>
        <name>CSK - Technical Details - 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Technical_Details_2</fullName>
        <field>Technical_Details__c</field>
        <formula>&quot;The flaw exists but it is not exploitable.&quot;</formula>
        <name>CSK - Technical Details - 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_UpdateTechnicalDetails_1</fullName>
        <field>Technical_Details__c</field>
        <formula>&quot;False positive. Provide explanation&quot;</formula>
        <name>CSK - Update Technical Details - 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Update_Next_Review_Date</fullName>
        <field>Next_Review_Date__c</field>
        <formula>Requested_Publish_Date__c + 180</formula>
        <name>CSK - Update Next Review Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Update_Requested_Publish_Date</fullName>
        <field>Requested_Publish_Date__c</field>
        <formula>NOW()</formula>
        <name>CSK - Update Requested Publish Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Update_Technical_Details_2</fullName>
        <field>Technical_Details__c</field>
        <formula>&quot;The flaw exists but it is not exploitable. Provide explanation.&quot;</formula>
        <name>CSK - Update Technical Details - 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Update_Technical_Details_3</fullName>
        <field>Technical_Details__c</field>
        <formula>&quot;The EMC product embeds the vulnerable code/component.  EMC is working on a solution to address the issue and will provide regular status updates.&quot;</formula>
        <name>CSK - Update Technical Details - 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Update_Technical_Details_4</fullName>
        <field>Technical_Details__c</field>
        <formula>&quot;Provide details on patch, upgrade, downgrade, workaround, hot fix.&quot;</formula>
        <name>CSK - Update Technical Details - 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Update_Technical_Details_5</fullName>
        <field>Technical_Details__c</field>
        <formula>&quot;The component is a prerequisite to run the EMC product, but there is no known or expected affect on the EMC product when applying the vendor supplied security patch.&quot;</formula>
        <name>CSK - Update Technical Details - 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Update_Technical_Details_Clear</fullName>
        <field>Technical_Details__c</field>
        <name>CSK - Update Technical Details - Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_Update_Validation_Status_to_ITR</fullName>
        <description>Updates the validation status to &quot;In Technical Review&quot;</description>
        <field>ValidationStatus</field>
        <literalValue>In Technical Review</literalValue>
        <name>CSK Update Validation Status to ITR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSK_in_Public_KnowledgeBase</fullName>
        <description>if the Audience Field is Level 50, only the Internal App should ever be true - all other channels should be false</description>
        <field>IsVisibleInPkb</field>
        <literalValue>0</literalValue>
        <name>CSK in Public KnowledgeBase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Create_Article_Metadata_Link</fullName>
        <field>Article_Metadata_Link__c</field>
        <formula>&quot;&lt;a href=&apos;/apex/KB_T3PickerScreen?aId=&quot; &amp; Id &amp; &quot;&amp;aType=&quot; &amp; SUBSTITUTE(SUBSTITUTE(TEXT(ArticleType), &quot;__kav&quot;, &quot;&quot;), &quot;_&quot;, &quot; &quot;) &amp; &quot; &apos; target=&apos;_blank&apos; &gt;Assign Article Metadata&lt;/a&gt;&quot;</formula>
        <name>Create Article Metadata Link</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rework</fullName>
        <description>This will change the validation status to rework.</description>
        <field>ValidationStatus</field>
        <literalValue>Rework</literalValue>
        <name>Rework</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Technically_Approved</fullName>
        <description>This will update an articles validation status to Technically Approved</description>
        <field>ValidationStatus</field>
        <literalValue>Technically Approved</literalValue>
        <name>Technically Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_validation_Status</fullName>
        <description>This will update the validation status to Technically Approved</description>
        <field>ValidationStatus</field>
        <literalValue>Technically Approved</literalValue>
        <name>Update validation Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish_As_New</fullName>
        <action>PublishAsNew</action>
        <label>Publish As New</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>CSK - Create Article Metadata Link</fullName>
        <actions>
            <name>Create_Article_Metadata_Link</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>KNOWLEDGE: Creates a link to Article Metadata page for Non ESA Security articles.</description>
        <formula>1==1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Populate Originally Authored By User</fullName>
        <actions>
            <name>Article_Created_Date_update_securitykb</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CSK_Populate_Originally_Authored_By_Us</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>KNOWLEDGE: Populates author&apos;s name on Non ESA Security articles</description>
        <formula>1==1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Security KB Populate Disclaimer</fullName>
        <actions>
            <name>CSK_Populate_Security_KB_Disclaimer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Security_KB__kav.Title</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>KNOWLEDGE: Populate disclaimer on Non-ESA Security articles.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Technical Details - 1</fullName>
        <actions>
            <name>CSK_Technical_Details_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Security_KB__kav.Technical_Detail_Picker__c</field>
            <operation>equals</operation>
            <value>Detail 1</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Technical Details - 2</fullName>
        <actions>
            <name>CSK_Technical_Details_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Security_KB__kav.Technical_Detail_Picker__c</field>
            <operation>equals</operation>
            <value>Detail 2</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Technical Details - 3</fullName>
        <actions>
            <name>CSK_Technical_Detail_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Security_KB__kav.Technical_Detail_Picker__c</field>
            <operation>equals</operation>
            <value>Detail 3</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Technical Details - 4</fullName>
        <actions>
            <name>CSK_Technical_Detail_4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Security_KB__kav.Technical_Detail_Picker__c</field>
            <operation>equals</operation>
            <value>Detail 4</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Technical Details - 5</fullName>
        <actions>
            <name>CSK_Technical_Detail_5</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Security_KB__kav.Technical_Detail_Picker__c</field>
            <operation>equals</operation>
            <value>Detail 5</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Technical Details - Clear</fullName>
        <actions>
            <name>CSK_Technical_Detail_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Security_KB__kav.Technical_Detail_Picker__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Update Next Review Date</fullName>
        <actions>
            <name>CSK_Update_Next_Review_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>KNOWLEDGE: Updates Next Review Date to six months from Requested Publish Date for 	Non ESA Security articles.</description>
        <formula>ISCHANGED(Requested_Publish_Date__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Update Requested Publish Date</fullName>
        <actions>
            <name>CSK_Update_Requested_Publish_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Security_KB__kav.Requested_Publish_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>KNOWLEDGE: If Requested Publish Date is blank, update it to current date/time for Non ESA Security articles.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Update Technical Details - 1</fullName>
        <actions>
            <name>CSK_UpdateTechnicalDetails_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Security_KB__kav.Alert_Impact__c</field>
            <operation>equals</operation>
            <value>Not Applicable</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Update Technical Details - 2</fullName>
        <actions>
            <name>CSK_Update_Technical_Details_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Security_KB__kav.Alert_Impact__c</field>
            <operation>equals</operation>
            <value>Not Exploitable</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Update Technical Details - 3</fullName>
        <actions>
            <name>CSK_Update_Technical_Details_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Security_KB__kav.Alert_Impact__c</field>
            <operation>equals</operation>
            <value>Remedy in Progress</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Update Technical Details - 4</fullName>
        <actions>
            <name>CSK_Update_Technical_Details_4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Security_KB__kav.Alert_Impact__c</field>
            <operation>equals</operation>
            <value>Impacted - Apply EMC Remedy</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Update Technical Details - 5</fullName>
        <actions>
            <name>CSK_Update_Technical_Details_5</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Security_KB__kav.Alert_Impact__c</field>
            <operation>equals</operation>
            <value>Impacted - Apply Vendor Remedy</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK - Update Technical Details - Clear</fullName>
        <actions>
            <name>CSK_Update_Technical_Details_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Security_KB__kav.Alert_Impact__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK Channels - Audience 30%2F40</fullName>
        <actions>
            <name>CSK_Channels_Audience_30_40_2_Cust_Po</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CSK_Channels_Audience_30_40_2_Pub_Kno</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CSK_Channels_Audience_30_40_2_part_po</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Security_KB__kav.Audience__c</field>
            <operation>equals</operation>
            <value>Level 40 = Employees and Partners,Level 30 = Customers</value>
        </criteriaItems>
        <description>KNOWLEDGE: If Audience level is 40 or 30, then Customer Portal AND Internal App should be true - all other values false</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK Channels Audience -50</fullName>
        <actions>
            <name>CSK_In_Customer_Portal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CSK_In_PartnerPortal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CSK_in_Public_KnowledgeBase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Security_KB__kav.Audience__c</field>
            <operation>equals</operation>
            <value>Level 50 = Restricted</value>
        </criteriaItems>
        <description>KNOWLEDGE: If Audience level is 50, then Internal App should be true - all other values false</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK Clear Article Alert</fullName>
        <actions>
            <name>CSK_Clear_Article_Alert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>KNOWLEDGE: Populates the Required Article Information field with a confirmation that the article is ready for approval when a product is selected. This is reevaluated on every save.</description>
        <formula>NOT(  AND(  ISBLANK( T3_Product__c ),  ISBLANK( EMC_Internal_Application__c )  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSK Populate Article Alert</fullName>
        <actions>
            <name>CSK_Populate_Article_Alert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>KNOWLEDGE: Populates the Required Article Information field with a confirmation that the article is ready for approval when a product is selected. This is reevaluated on every save.</description>
        <formula>AND( ISBLANK( T3_Product__c ), ISBLANK( EMC_Internal_Application__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
