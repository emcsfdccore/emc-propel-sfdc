<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Notification_To_Submitter_Upon_GAF_Approval</fullName>
        <description>Email Notification To Submitter Upon GAF Approval</description>
        <protected>false</protected>
        <recipients>
            <field>GAF_Submitted_By__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_GAF_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/SENT_TO_PARTNER_GAF_APPROVER_AND_VELOCITY_TEAM_WHEN_PARTNER_HAS_APPROVED_GAF</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_To_Submitter_Upon_GAF_Approval_for_AMER</fullName>
        <description>Email Notification To Submitter Upon GAF Approval for AMER</description>
        <protected>false</protected>
        <recipients>
            <field>GAF_Submitted_By__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_GAF_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Deal_reg_Templates/GAF_Submitter_Notification_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_To_Submitter_Upon_GAF_Approval_for_AMER_LA</fullName>
        <description>Email Notification To Submitter Upon GAF Approval for AMER(LA)</description>
        <protected>false</protected>
        <recipients>
            <field>GAF_Submitted_By__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_GAF_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Deal_reg_Templates/GAF_Submitter_Notification_Template_LA</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_To_Submitter_Upon_GAF_Approval_for_APJ</fullName>
        <description>Email Notification To Submitter Upon GAF Approval for APJ</description>
        <protected>false</protected>
        <recipients>
            <field>GAF_Submitted_By__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_GAF_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/GAF_Submitter_Notification_Template_APJ</template>
    </alerts>
    <alerts>
        <fullName>Email_is_sent_upon_Submission_of_a_GAF_Record</fullName>
        <description>Email is sent upon Submission of a GAF Record</description>
        <protected>false</protected>
        <recipients>
            <field>GAF_Submitted_By__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_GAF_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/SENT_TO_PARTNER_GAF_APPROVER_WHEN_GAF_READY_FOR_APPROVAL</template>
    </alerts>
    <alerts>
        <fullName>Email_is_sent_upon_Submission_of_a_GAF_Record_for_AMER</fullName>
        <description>Email is sent upon Submission of a GAF Record for AMER</description>
        <protected>false</protected>
        <recipients>
            <field>GAF_Submitted_By__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_GAF_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Deal_reg_Templates/GAF_Partner_Notification_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_is_sent_upon_Submission_of_a_GAF_Record_for_AMER_LA</fullName>
        <description>Email is sent upon Submission of a GAF Record for AMER(LA)</description>
        <protected>false</protected>
        <recipients>
            <field>GAF_Submitted_By__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_GAF_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Deal_reg_Templates/GAF_Partner_Notification_Template_LA</template>
    </alerts>
    <alerts>
        <fullName>Email_is_sent_upon_Submission_of_a_GAF_Record_for_APJ</fullName>
        <description>Email is sent upon Submission of a GAF Record for APJ</description>
        <protected>false</protected>
        <recipients>
            <field>GAF_Submitted_By__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_GAF_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/GAF_Partner_Notification_Template_APJ</template>
    </alerts>
    <alerts>
        <fullName>Email_is_sent_when_Re_Approval_is_required_for_a_GAF_Record</fullName>
        <description>Email is sent when Re-Approval is required for a GAF Record</description>
        <protected>false</protected>
        <recipients>
            <field>GAF_Submitted_By__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_GAF_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Deal_reg_Templates/GAF_Partner_Notification_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_is_sent_when_Re_Approval_is_required_for_a_GAF_Record_for_AMER</fullName>
        <description>Email is sent when Re-Approval is required for a GAF Record for AMER</description>
        <protected>false</protected>
        <recipients>
            <field>GAF_Submitted_By__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_GAF_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Deal_reg_Templates/GAF_Partner_Notification_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_GAF_Status_to_Expired</fullName>
        <description>This field update will be used to set the value of GAF to expired when End Date is Reached.</description>
        <field>GAF_Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Update GAF Status to Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Flip Status of GAF to expired</fullName>
        <active>true</active>
        <criteriaItems>
            <field>GAF__c.GAF_Status__c</field>
            <operation>notEqual</operation>
            <value>Expired</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_GAF_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>GAF__c.GAF_End_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send Notification To GAF Submitter Upon Approval for AMER</fullName>
        <actions>
            <name>Email_Notification_To_Submitter_Upon_GAF_Approval_for_AMER</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow is used to send the email notification to Internal Users Upon Approval of GAF for AMER.</description>
        <formula>AND(TEXT(GAF_Status__c)==&apos;Partner Approved&apos;, Partner_Name__r.Theater1__c ==&apos;Americas&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Notification To GAF Submitter Upon Approval for AMER%28LA%29</fullName>
        <actions>
            <name>Email_Notification_To_Submitter_Upon_GAF_Approval_for_AMER_LA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>GAF__c.GAF_Status__c</field>
            <operation>equals</operation>
            <value>Partner Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>GAF__c.Partner_Theater__c</field>
            <operation>equals</operation>
            <value>Americas</value>
        </criteriaItems>
        <criteriaItems>
            <field>GAF__c.Cluster__c</field>
            <operation>equals</operation>
            <value>LA1</value>
        </criteriaItems>
        <criteriaItems>
            <field>GAF__c.Cluster__c</field>
            <operation>equals</operation>
            <value>LA2</value>
        </criteriaItems>
        <description>This workflow is used to send the email notification to Internal Users Upon Approval of GAF for AMER (LA).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Notification To GAF Submitter Upon Approval for APJ</fullName>
        <actions>
            <name>Email_Notification_To_Submitter_Upon_GAF_Approval_for_APJ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow is used to send the email notification to Internal Users Upon Approval of GAF for APJ.</description>
        <formula>AND(TEXT(GAF_Status__c)==&apos;Partner Approved&apos;, Partner_Name__r.Theater1__c ==&apos;APJ&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Notification To GAF Submitter Upon Approval for EMEA</fullName>
        <actions>
            <name>Email_Notification_To_Submitter_Upon_GAF_Approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow is used to send the email notification to Internal Users Upon Approval of GAF for EMEA.</description>
        <formula>AND(TEXT(GAF_Status__c)==&apos;Partner Approved&apos;, Partner_Name__r.Theater1__c ==&apos;EMEA&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Notification To Partner Approver Upon GAF Submission for AMER</fullName>
        <actions>
            <name>Email_is_sent_upon_Submission_of_a_GAF_Record_for_AMER</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow will be used to send Email notification to Partner Approver Upon GAF Submission for AMER</description>
        <formula>AND(OR(TEXT(GAF_Status__c)==&apos;EMC Submitted&apos;,TEXT(GAF_Status__c)==&apos;Reapproval Required&apos;), Partner_GAF_Approver__c != &apos;&apos;, Partner_Name__r.Theater1__c ==&apos;Americas&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Notification To Partner Approver Upon GAF Submission for AMER %28LA%29</fullName>
        <actions>
            <name>Email_is_sent_upon_Submission_of_a_GAF_Record_for_AMER_LA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3 AND (5 OR 6)</booleanFilter>
        <criteriaItems>
            <field>GAF__c.GAF_Status__c</field>
            <operation>equals</operation>
            <value>EMC Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>GAF__c.Partner_GAF_Approver__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>GAF__c.Partner_Theater__c</field>
            <operation>equals</operation>
            <value>Americas</value>
        </criteriaItems>
        <criteriaItems>
            <field>GAF__c.GAF_Status__c</field>
            <operation>equals</operation>
            <value>Reapproval Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>GAF__c.Cluster__c</field>
            <operation>equals</operation>
            <value>LA1</value>
        </criteriaItems>
        <criteriaItems>
            <field>GAF__c.Cluster__c</field>
            <operation>equals</operation>
            <value>LA2</value>
        </criteriaItems>
        <description>This workflow will be used to send Email notification to Partner Approver Upon GAF Submission for AMER(LA)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Notification To Partner Approver Upon GAF Submission for APJ</fullName>
        <actions>
            <name>Email_is_sent_upon_Submission_of_a_GAF_Record_for_APJ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow will be used to send Email notification to Partner Approver Upon GAF Submission for APJ</description>
        <formula>AND(OR(TEXT(GAF_Status__c)==&apos;EMC Submitted&apos;,TEXT(GAF_Status__c)==&apos;Reapproval Required&apos;), Partner_GAF_Approver__c != &apos;&apos;, Partner_Name__r.Theater1__c ==&apos;APJ&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Notification To Partner Approver Upon GAF Submission for EMEA</fullName>
        <actions>
            <name>Email_is_sent_upon_Submission_of_a_GAF_Record</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow will be used to send Email notification to Partner Approver Upon GAF Submission for EMEA</description>
        <formula>AND(OR(TEXT(GAF_Status__c)==&apos;EMC Submitted&apos;,TEXT(GAF_Status__c)==&apos;Reapproval Required&apos;), Partner_GAF_Approver__c != &apos;&apos;, Partner_Name__r.Theater1__c ==&apos;EMEA&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
