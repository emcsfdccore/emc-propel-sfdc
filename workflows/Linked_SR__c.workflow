<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CSK_Update_Linked_By_User</fullName>
        <field>Linked_Date_Time__c</field>
        <formula>now()</formula>
        <name>CSK - Update Linked By User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>SR_Linked_to_KB_Article</fullName>
        <apiVersion>27.0</apiVersion>
        <description>KNOWLEDGE: Raises outbound message when an SR record is updated or created.</description>
        <endpointUrl>https://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.aicKbservice?WSDL</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>sfdcbatchadmin1@emc.com</integrationUser>
        <name>SR Linked to KB Article</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>CSK - Update Linking User and Date</fullName>
        <actions>
            <name>CSK_Update_Linked_By_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Linked_SR__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>KNOWLEDGE: Updates Linked Date/Time on Linked SRs</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SR Link De-Link</fullName>
        <actions>
            <name>SR_Linked_to_KB_Article</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Linked_SR__c.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>Admin KBase</value>
        </criteriaItems>
        <description>KNOWLEDGE: Sends an outbound message every time SR is linked or de-linked</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
