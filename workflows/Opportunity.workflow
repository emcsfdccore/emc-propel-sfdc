<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Opportunity_Close_Date_Moved_to_from_a_Future_Quarter</fullName>
        <description>Alert Opportunity  Close Date Moved to/from a Future Quarter</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEAManagers290871</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>adrian.foley@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_Close_Date_Moved_to_from_a_Future_Quarter</template>
    </alerts>
    <alerts>
        <fullName>Alert_Opportunity_is_Closed</fullName>
        <description>Alert Opportunity is Closed</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEAManagers290871</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>adrian.foley@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sabine.cronin@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_Closed</template>
    </alerts>
    <alerts>
        <fullName>Alert_Opportunity_is_Closed_to_Competition</fullName>
        <description>Alert Opportunity is Closed to Competition</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEAManagers290871</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>adrian.foley@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jeroen.schumm@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sabine.cronin@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_Closed_to_Competition</template>
    </alerts>
    <alerts>
        <fullName>Alert_Opportunity_moved_from_Commit_to_Strong_Upside</fullName>
        <description>Alert Opportunity moved from Commit to Strong Upside</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEAManagers290871</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>adrian.foley@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kevin.odwyer@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_moved_from_Commit_to_Strong_Upside</template>
    </alerts>
    <alerts>
        <fullName>Alert_Opportunity_moved_from_Commit_to_Upside</fullName>
        <description>Alert Opportunity moved from Commit to Upside</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEAManagers290871</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>adrian.foley@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kevin.odwyer@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_moved_from_Commit_to_Upside</template>
    </alerts>
    <alerts>
        <fullName>Alert_Opportunity_moved_from_Strong_Upside_to_Upside</fullName>
        <description>Alert Opportunity moved from Strong Upside to Upside</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEAManagers290871</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>adrian.foley@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kevin.odwyer@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_moved_from_Strong_Upside_to_Upside</template>
    </alerts>
    <alerts>
        <fullName>Contract_Renewals_new_Account_Owner_Alert</fullName>
        <description>Contract Renewals new Account Owner Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_Account_Owner_Email</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Opportunity_Account_Owner_Change_Email_Alert</fullName>
        <description>EMEA Opportunity Account Owner Change Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/EMEA_Opportunity_Account_Owner_Change_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_Booked_deals_for_IIG</fullName>
        <description>Email Alert for Booked deals for IIG</description>
        <protected>false</protected>
        <recipients>
            <recipient>dumlao_casey@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fairchild_larry@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CMATemplates/Opportunity_moves_into_Booked</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Direct_Reps_For_EMEA</fullName>
        <description>Email Notification to Direct Reps For EMEA</description>
        <protected>false</protected>
        <recipients>
            <recipient>ATMOS-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BRS-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Area Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Avamar Area Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Avamar District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Avamar Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-CHANNEL REP</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Channel Area Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Channel District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Division Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-INSIDE SALES AM</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-INSIDE SALES DM</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Inside Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-SALES REP</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-SE</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-SE District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BUS OPS-CCG</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BUS OPS-CONFIG</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BUSINESS ANALYST</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BUSINESS MANAGER</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Bus Ops-Business Analyst</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CHANNEL-DISTRICT MANAGER</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CHANNEL-EXECUTION SALES REP</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CMA-Contract AM</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CMA-Contract DM</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CMA-Contract Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CMA-Inside Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Centera-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Centera-System Engineer</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel SE-System Engineer</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel-Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel-Divisional Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel-Regional Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel-Strategic Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>DCD-Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>DCD-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>DCD-Divisional Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>DCD-Region Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>DCD-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Dell-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>ECS-COUNTRY TSD MGR</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>ECS-CSD Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>ECS-MCSD MGR</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>EMEA IS Germany Dist Comm Hunter</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>ETC-Consultant</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Ed Services-District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Ed Services-Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAL-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAL-Partner District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAL-Partner Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAL-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAM-Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAM-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAM-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GFS-Area Finance Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GFS-Channel Finance Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GFS-Geo Finance Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GFS-Regional Finance Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GFS-Regional FinanceSpecialist</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Grid Business-District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Grid Business-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>INSIDE SALES-AREA MANAGER</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>INSIDE SALES-CONTRACTS REP</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>INSIDE SALES-REGIONAL MANAGER</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IONIX-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IONIX-Division Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IONIX-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IONIX-TS District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IONIX-TS Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-Division Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-EMEA NQ Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-EMEA Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-EMEA TQ Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-Sales Admin</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-TC</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>NAM-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>NAS-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>NAS-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>PRODUCT-SALES REP</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Product-Sales District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Rainfinity-BDM Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SALES DEV-AREA MANAGER</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SALES TEAM ROLE REPORT FROM SFDC</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SALES-REGIONAL MANAGER</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SALES-SMB DISTRICT MGR</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SALES-SMB SALES REP</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SE-Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SE-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SMARTS NQ-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SMARTS NQ-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SMARTS-SE</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Dev-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Dev-SA</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales NQ-EMEA TPM</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Commercial Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Commercial District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Commercial Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Corp District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Corporate Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Divisional Admin</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Divisional Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Divisional VicePresident</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Enterprise Farmer Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Enterprise Hunter Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Executive Vice President</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Field Admin</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Inside Partner Terr Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Partner Enterprise Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Partner Territory Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-SMB Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Sales Admin</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Sales Associate</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Sales Ops</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Team Leader</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-ADMIN</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Business Practice Lead</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Client Sol Lead Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Client Solutions Lead</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Implementation Specialist</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Operations</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Practice Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Pre Sales Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Pre Sales Comm District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Pre Sales Comm Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Pre Sales District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Pre Sales Division Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Pre Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Program Delivery Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Program Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Project Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Solutions Architect</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Tech Solutions Dir</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Tech Solutions Lead</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Technology Consultant</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>vSpecialist-Area Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>vSpecialist-District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>vSpecialist-Region Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>vSpecialist-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderAddress>pscna@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PRM_Templates/Partner_Perf_Rating_creation_Direct_Reps</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Direct_Reps_for_Americas</fullName>
        <description>Email Notification to Direct Reps for Americas</description>
        <protected>false</protected>
        <recipients>
            <recipient>ATMOS-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BRS-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Area Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Avamar Area Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Avamar District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Avamar Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-CHANNEL REP</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Channel Area Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Channel District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Division Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-INSIDE SALES AM</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-INSIDE SALES DM</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-Inside Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-SALES REP</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-SE</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BURA-SE District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BUS OPS-CCG</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BUS OPS-CONFIG</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BUSINESS ANALYST</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>BUSINESS MANAGER</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Bus Ops-Business Analyst</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CHANNEL-DISTRICT MANAGER</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CHANNEL-EXECUTION SALES REP</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CMA-Contract AM</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CMA-Contract DM</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CMA-Contract Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>CMA-Inside Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Centera-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Centera-System Engineer</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel SE-System Engineer</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel-Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel-Divisional Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel-Regional Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel-Strategic Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>DCD-Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>DCD-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>DCD-Divisional Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>DCD-Region Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>DCD-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Dell-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>ECS-COUNTRY TSD MGR</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>ECS-CSD Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>ECS-MCSD MGR</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>EMEA IS Germany Dist Comm Hunter</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>ETC-Consultant</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Ed Services-District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Ed Services-Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAL-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAL-Partner District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAL-Partner Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAL-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAM-Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAM-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GAM-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GFS-Area Finance Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GFS-Channel Finance Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GFS-Geo Finance Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GFS-Regional Finance Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>GFS-Regional FinanceSpecialist</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Grid Business-District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Grid Business-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>INSIDE SALES-AREA MANAGER</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>INSIDE SALES-CONTRACTS REP</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>INSIDE SALES-REGIONAL MANAGER</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IONIX-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IONIX-Division Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IONIX-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IONIX-TS District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>IONIX-TS Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-Division Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-EMEA NQ Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-EMEA Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-EMEA TQ Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-Sales Admin</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Inside Sales-TC</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>NAM-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>NAS-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>NAS-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>PRODUCT-SALES REP</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Product-Sales District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Rainfinity-BDM Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SALES DEV-AREA MANAGER</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SALES TEAM ROLE REPORT FROM SFDC</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SALES-REGIONAL MANAGER</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SALES-SMB DISTRICT MGR</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SALES-SMB SALES REP</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SE-Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SE-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SMARTS NQ-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SMARTS NQ-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>SMARTS-SE</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Dev-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Dev-SA</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales NQ-EMEA TPM</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Commercial Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Commercial District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Commercial Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Corp District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Corporate Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Divisional Admin</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Divisional Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Divisional VicePresident</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Enterprise Farmer Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Enterprise Hunter Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Executive Vice President</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Field Admin</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Inside Partner Terr Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Partner Enterprise Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Partner Territory Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-SMB Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Sales Admin</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Sales Associate</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Sales Ops</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales-Team Leader</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-ADMIN</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Business Practice Lead</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Client Sol Lead Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Client Solutions Lead</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Implementation Specialist</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Operations</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Practice Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Pre Sales Area Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Pre Sales Comm District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Pre Sales Comm Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Pre Sales District Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Pre Sales Division Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Pre Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Program Delivery Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Program Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Project Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Solutions Architect</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Tech Solutions Dir</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Tech Solutions Lead</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>TS-Technology Consultant</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>vSpecialist-Area Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>vSpecialist-District Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>vSpecialist-Region Mgr</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>vSpecialist-Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderAddress>pscna@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PRM_Templates/Partner_Perf_Rating_creation_Direct_Reps</template>
    </alerts>
    <alerts>
        <fullName>MCO_Analyst_Notification</fullName>
        <description>MCO Analyst Notification</description>
        <protected>false</protected>
        <recipients>
            <field>MCO__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Renewals_booking_process</template>
    </alerts>
    <alerts>
        <fullName>Manager_Requested_Email_Alert</fullName>
        <description>Manager Requested Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MEDDIC_templates/Manager_Requested_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Moves_into_Commit</fullName>
        <description>Opportunity Moves into Commit</description>
        <protected>false</protected>
        <recipients>
            <recipient>powell_david@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sudoh_nana@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yap_vivian@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>APJ_Alert_Templates/Opportunity_moves_into_Commit</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Oppty_downgrades_Commit</fullName>
        <description>Opportunity Oppty downgrades Commit</description>
        <protected>false</protected>
        <recipients>
            <recipient>powell_david@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sudoh_nana@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yap_vivian@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>APJ_Alert_Templates/Opportunity_Downgraded_from_Commit</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_moved_out_of_Strong_Upside_APJ</fullName>
        <description>Opportunity moved out of Strong Upside APJ</description>
        <protected>false</protected>
        <recipients>
            <recipient>powell_david@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sudoh_nana@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yap_vivian@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>APJ_Alert_Templates/Opportunity_moved_out_of_Strong_Upside</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_moved_to_Booked_APJ</fullName>
        <description>Opportunity moved to Booked APJ</description>
        <protected>false</protected>
        <recipients>
            <recipient>powell_david@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sudoh_nana@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yap_vivian@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>APJ_Alert_Templates/Opportunity_moved_to_Booked</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_moved_to_Closed_APJ</fullName>
        <description>Opportunity moved to Closed APJ</description>
        <protected>false</protected>
        <recipients>
            <recipient>powell_david@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sudoh_nana@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yap_vivian@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>APJ_Alert_Templates/Opportunity_moved_to_Closed</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_moved_to_Eval_APJ</fullName>
        <description>Opportunity moved to Eval APJ</description>
        <protected>false</protected>
        <recipients>
            <recipient>powell_david@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sudoh_nana@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yap_vivian@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>APJ_Alert_Templates/Opportunity_moved_to_Eval</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_moved_to_Strong_Upside_APJ</fullName>
        <description>Opportunity moved to Strong Upside APJ</description>
        <protected>false</protected>
        <recipients>
            <recipient>powell_david@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sudoh_nana@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yap_vivian@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>APJ_Alert_Templates/Opportunity_moved_to_Strong_Upside</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_moved_to_Submitted_APJ</fullName>
        <description>Opportunity moved to Submitted APJ</description>
        <protected>false</protected>
        <recipients>
            <recipient>powell_david@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sudoh_nana@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yap_vivian@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>APJ_Alert_Templates/Opportunity_moved_to_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Opportuniy_Rejected_Alert</fullName>
        <description>Opportuniy Rejected Alert</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>MCO__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SampleTemplates/Opportunity_Rejected_Rejected_Renewals</template>
    </alerts>
    <alerts>
        <fullName>Opportuniy_Rejected_RSS_Alert</fullName>
        <description>Opportuniy Rejected RSS Alert</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>MCO__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>RSS_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SampleTemplates/Opportunity_Rejected_RSS</template>
    </alerts>
    <alerts>
        <fullName>RSS_Coordinator_Notification</fullName>
        <description>RSS Coordinator Notification</description>
        <protected>false</protected>
        <recipients>
            <field>RSS_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Renewals_booking_process</template>
    </alerts>
    <alerts>
        <fullName>Rev_Rec_Analyst_Notification</fullName>
        <description>Rev Rec Analyst Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Rev_Rec_Analyst__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Renewals_booking_process</template>
    </alerts>
    <alerts>
        <fullName>Send_Integration_Error_Message</fullName>
        <description>Send Integration Error Message</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Opportunity_Integration_Error_Message</template>
    </alerts>
    <alerts>
        <fullName>Send_email_notification_when_ELA_will_be_checked</fullName>
        <description>Send email notification when ELA will be checked</description>
        <protected>false</protected>
        <recipients>
            <recipient>GFS-Area Finance Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/ELA_Opportunity_Email_Notification</template>
    </alerts>
    <alerts>
        <fullName>The_email_will_be_sent_to_respective_members_when_the_booking_status_of_the_oppo</fullName>
        <description>The email will be sent to respective members when the booking status of the opportunity is &apos;Rejected MCO&apos;</description>
        <protected>false</protected>
        <recipients>
            <field>MCO__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SampleTemplates/Opportunity_Rejection_MCO</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_Central</fullName>
        <description>Under Pen Opportunity Win Alert - Central</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsCentral</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_Central_500k</fullName>
        <description>Under Pen Opportunity Win Alert - Central 500k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsCentral</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Under_Pen_Alerts_Execs</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>larry.murray@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lee_johnc@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_Federal</fullName>
        <description>Under Pen Opportunity Win Alert - Federal</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsFederal</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_Federal_500k</fullName>
        <description>Under Pen Opportunity Win Alert - Federal 500k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsFederal</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Under_Pen_Alerts_Execs</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_LATAM</fullName>
        <description>Under Pen Opportunity Win Alert - LATAM</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsLATAM</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_LATAM_500k</fullName>
        <description>Under Pen Opportunity Win Alert - LATAM 500k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsLATAM</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Under_Pen_Alerts_Execs</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>larry.murray@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lee_johnc@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_MM_Central_100k</fullName>
        <description>Under Pen Opportunity Win Alert - MM Central 100k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsMMCentral</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_MM_Central_500k</fullName>
        <description>Under Pen Opportunity Win Alert - MM Central 500k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsMMCentral</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Under_Pen_Alerts_Execs</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>albanese_john@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dunfee_matt@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_MM_East_100k</fullName>
        <description>Under Pen Opportunity Win Alert - MM East 100k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsMMEast</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_MM_East_500k</fullName>
        <description>Under Pen Opportunity Win Alert - MM East 500k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsMMEast</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Under_Pen_Alerts_Execs</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>albanese_john@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dunfee_matt@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_MM_West100k</fullName>
        <description>Under Pen Opportunity Win Alert - MM West100k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsMMWest</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_MM_West_500k</fullName>
        <description>Under Pen Opportunity Win Alert - MM West 500k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsMMWest</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Under_Pen_Alerts_Execs</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>albanese_john@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dunfee_matt@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_Mid_Atlantic_South</fullName>
        <description>Under Pen Opportunity Win Alert - Mid-Atlantic/South</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsMidAtlanticSouth</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_Mid_Atlantic_South_500k</fullName>
        <description>Under Pen Opportunity Win Alert - Mid-Atlantic/South 500k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsMidAtlanticSouth</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Under_Pen_Alerts_Execs</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>larry.murray@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lee_johnc@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_NE_Canada</fullName>
        <description>Under Pen Opportunity Win Alert – NE Canada</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsNECanada</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_NE_Canada_500k</fullName>
        <description>Under Pen Opportunity Win Alert – NE Canada 500k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsNECanada</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Under_Pen_Alerts_Execs</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>larry.murray@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lee_johnc@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_NY_NJ</fullName>
        <description>Under Pen Opportunity Win Alert – NY NJ</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsNYNJ</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_NY_NJ_500k</fullName>
        <description>Under Pen Opportunity Win Alert – NY NJ 500k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsNYNJ</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Under_Pen_Alerts_Execs</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>larry.murray@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lee_johnc@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_West</fullName>
        <description>Under Pen Opportunity Win Alert - West</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsWest</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <alerts>
        <fullName>Under_Pen_Opportunity_Win_Alert_West_500k</fullName>
        <description>Under Pen Opportunity Win Alert - West 500k</description>
        <protected>false</protected>
        <recipients>
            <recipient>UnderPenAlertsWest</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Under_Pen_Alerts_Execs</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>larry.murray@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lee_johnc@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_do_not_use_this_email_address@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalOnly/Under_Pen_Win</template>
    </alerts>
    <fieldUpdates>
        <fullName>Booking_Status_modified_by_Department</fullName>
        <field>BookingStatus_Last_ModifiedBy_Department__c</field>
        <formula>$User.Department</formula>
        <name>Booking Status modified by Department</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Booking_Status_modified_by_User</fullName>
        <field>Booking_Status_Last_ModifiedBy__c</field>
        <formula>$User.FirstName  + &apos;        &apos; +  $User.LastName</formula>
        <name>Booking Status modified by User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Booking_Status_modified_by_User_s_Role</fullName>
        <field>Booking_Status_Last_Modified_By_Role__c</field>
        <formula>$UserRole.Name</formula>
        <name>Booking Status modified by User&apos;s Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Update_Box</fullName>
        <description>Update Forecast Amount from Quote when Opp is booked</description>
        <field>Update_Forecast_Amount_from_Quote__c</field>
        <literalValue>1</literalValue>
        <name>Check Update Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deal_Review_Modified_By</fullName>
        <description>Field update for modifying Deal Review Updated by field sets lastmodified date along with logged in user name.</description>
        <field>Deal_Review_Updated_by__c</field>
        <formula>TEXT(MONTH(Today()))+&quot;/&quot; +TEXT(DAY(Today()))+&quot;/&quot; +TEXT(YEAR(Today())) &amp;&quot; by &quot;&amp; $User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Deal Review Modified By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Initial_Forecast_Quarter_Field_Update</fullName>
        <field>Initial_Forecast_Quarter__c</field>
        <formula>CASE( 
MONTH(CloseDate) , 1 , &apos;Q1&apos; , 2 , &apos;Q1&apos; , 3 , &apos;Q1&apos; , 4 , &apos;Q2&apos; , 5 , &apos;Q2&apos; , 6 , &apos;Q2&apos; , 7 , &apos;Q3&apos; , 8 , &apos;Q3&apos; , 9 , &apos;Q3&apos; , 10 , &apos;Q4&apos; , 11 , &apos;Q4&apos; , 12 , &apos;Q4&apos; , 
&apos;Contact Your Admin&apos;) 
&amp; 
&apos; &apos; 
&amp; 
TEXT( YEAR(CloseDate) )</formula>
        <name>Initial Forecast Quarter Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Quote_Attached</fullName>
        <field>Quote_Attached__c</field>
        <literalValue>1</literalValue>
        <name>Mark Quote Attached</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_as_flipped_to_Booked_RT</fullName>
        <description>This action will update the &apos;Workflow Updated to Booked RT&apos; checkbox so that it is now checked.</description>
        <field>Workflow_Updated_to_Booked_RT__c</field>
        <literalValue>1</literalValue>
        <name>Mark as flipped to Booked RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Linked_ReUpdation</fullName>
        <field>Opportunity_Linked__c</field>
        <literalValue>0</literalValue>
        <name>Opportunity Linked ReUpdation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Linked_Updation</fullName>
        <field>Opportunity_Linked__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity Linked Updation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Opporutunity_Number</fullName>
        <field>Opportunity_Number__c</field>
        <formula>SFDC_Opportunity_number_generate__c</formula>
        <name>Populate Opporutunity Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Send_Oppty_Registration_Notificati</fullName>
        <description>Reset Send Oppty Registration Notification field to &apos;FALSE&apos;</description>
        <field>Send_Oppty_Registration_Notification__c</field>
        <literalValue>0</literalValue>
        <name>Reset Send Oppty Registration Notificati</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Channel_Update</fullName>
        <description>Update the Sales Channel Field To Indirect.</description>
        <field>Sales_Channel__c</field>
        <literalValue>Indirect</literalValue>
        <name>Sales Channel Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Forecast_Amount_to_zero</fullName>
        <description>Used to set the Forecast Amount on a Opportunity record to zero.</description>
        <field>Amount</field>
        <formula>0</formula>
        <name>Set Forecast Amount to zero</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_1_Discovery</fullName>
        <field>StageName</field>
        <literalValue>Pipeline</literalValue>
        <name>Stage-1 Discovery</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_3_Solution_Overview</fullName>
        <field>StageName</field>
        <literalValue>Upside</literalValue>
        <name>Stage-3 Solution Overview</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_5_Solution_Proposal</fullName>
        <field>StageName</field>
        <literalValue>Strong Upside</literalValue>
        <name>Stage-5 Solution Proposal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_7_Negotiate_Contracts</fullName>
        <field>StageName</field>
        <literalValue>Commit</literalValue>
        <name>Stage-7 Negotiate &amp; Contracts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_8_Close</fullName>
        <field>StageName</field>
        <literalValue>Won</literalValue>
        <name>Stage-8 Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Submitted_Or_Booked</fullName>
        <field>IIG_Sales_Stage__c</field>
        <literalValue>Stage-8 Close</literalValue>
        <name>Status Submitted Or Booked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Quote_Attached</fullName>
        <field>Quote_Attached__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Quote Attached</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRecordTypeOppBooked</fullName>
        <description>Update the Record Type to Booked Opportunity Record Type.</description>
        <field>RecordTypeId</field>
        <lookupValue>BookedOpportunityRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecordType Opp Booked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRecordTypeOppStandard</fullName>
        <description>Update the Record Type to Standard Opportunity Record Type.</description>
        <field>RecordTypeId</field>
        <lookupValue>StandardOpportunityRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecordType Opp Standard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DDO_Checkbox</fullName>
        <field>Direct_Deal_Override__c</field>
        <literalValue>0</literalValue>
        <name>Update DDO Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_IIG_Deal_Desk_Approval_Date</fullName>
        <description>Updates IIG Deal Desk Date to System</description>
        <field>IIG_Deal_Desk_Approval_Date__c</field>
        <formula>NOW()</formula>
        <name>Update IIG Deal Desk Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_IIG_Deal_Desk_Last_Updated_Date</fullName>
        <description>Updates IIG Deal Desk Date to System</description>
        <field>IIG_Deal_Desk_Last_Updated_Date__c</field>
        <formula>NOW()</formula>
        <name>Update IIG Deal Desk Last Updated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Modified_By</fullName>
        <field>Next_Steps_2__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Update Last Modified By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Modified_Date</fullName>
        <field>Next_Steps_Last_Updated_Date__c</field>
        <formula>LastModifiedDate</formula>
        <name>Update Last Modified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Most_Recent_Next_Steps</fullName>
        <field>Next_Steps__c</field>
        <formula>IF(ISBLANK(Next_Steps_new__c),&apos;&apos;,CreatedBy.FirstName +&apos; &apos;+ CreatedBy.LastName &amp;&quot; &quot;&amp; text(datevalue(CreatedDate))+&apos; &apos;+Next_Steps_new__c)</formula>
        <name>Update Most Recent Next Steps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Most_Recent_Sol_Win_Comments</fullName>
        <field>Solutions_Win_Comments__c</field>
        <formula>IF(ISBLANK(Most_Recent_5_Solutions_Win_Comments__c),&apos;&apos;,CreatedBy.FirstName +&apos; &apos;+ CreatedBy.LastName &amp;&quot; &quot;&amp; text(datevalue(CreatedDate))+&apos; &apos;+Most_Recent_5_Solutions_Win_Comments__c)</formula>
        <name>Update Most Recent Sol Win Comments</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RecordTypeID_Pre_Quote</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PreQuote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecordTypeID: Pre-Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RecordType_ID_To_Booked</fullName>
        <field>RecordTypeId</field>
        <lookupValue>New_Booked_Opportunity_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecordType ID: To Booked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RecordType_Id</fullName>
        <field>RecordTypeId</field>
        <lookupValue>New_Quote_Attached_RecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecordType Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RecordType_to_New_Booked_Record</fullName>
        <description>When ever stage equals Booked Record Type should be New Booked</description>
        <field>RecordTypeId</field>
        <lookupValue>New_Booked_Opportunity_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecordType to New Booked Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>populate_Solutions_Win_Last_Modified</fullName>
        <field>Solutions_Win_Last_Modified_By__c</field>
        <formula>$User.FirstName+&apos; &apos;+$User.LastName  +&apos; &apos;+  TEXT(MONTH(Today()))+&quot;/&quot; +TEXT(DAY(Today()))+&quot;/&quot; +TEXT(YEAR(Today()))</formula>
        <name>populate Solutions Win Last Modified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Create_Opportunity</fullName>
        <apiVersion>8.0</apiVersion>
        <endpointUrl>http://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.aicNotifyOpptyEMCSFDC?WSDL</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>integration_admin@emc.com</integrationUser>
        <name>Create Opportunity</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Create_Quote</fullName>
        <apiVersion>8.0</apiVersion>
        <endpointUrl>http://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.aicNotifyOpptyEMCSFDC?WSDL</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>integration_admin@emc.com</integrationUser>
        <name>Create Quote</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>PROPEL_Send_Opportunity_Registrations</fullName>
        <apiVersion>30.0</apiVersion>
        <description>Created for PROPEL</description>
        <endpointUrl>https://soagateway-test.emc.com/sst/runtime.asvc/com.actional.intermediary.aicNotifyOpportunityServiceEMCSFDC_DES</endpointUrl>
        <fields>Id</fields>
        <fields>Shared_to_Partner__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>integration_admin@emc.com</integrationUser>
        <name>PROPEL_Send Opportunity Registrations</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Presales_SubmittedOpportunity</fullName>
        <apiVersion>23.0</apiVersion>
        <description>When an Opportunity is Submitted, all attachments are sent to OPR repository (ECM)</description>
        <endpointUrl>https://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.sfdcSubmittedOpportunityWs?WSDL</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>integration_admin@emc.com</integrationUser>
        <name>Presales_SubmittedOpportunity</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Booking Status Email Alert</fullName>
        <actions>
            <name>Opportuniy_Rejected_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Booking_Status__c</field>
            <operation>equals</operation>
            <value>Rejected Renewals</value>
        </criteriaItems>
        <description>Send email alert when the opportuinity booking status is Rejected Renewals</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Booking Status Email Alert 2</fullName>
        <actions>
            <name>Opportuniy_Rejected_RSS_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Booking_Status__c</field>
            <operation>equals</operation>
            <value>Rejected RSS/SQI</value>
        </criteriaItems>
        <description>When Booking Status  = Rejected RSS/SQI</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Booking Status Email Alert1</fullName>
        <actions>
            <name>The_email_will_be_sent_to_respective_members_when_the_booking_status_of_the_oppo</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Booking_Status__c</field>
            <operation>equals</operation>
            <value>Rejected MCO</value>
        </criteriaItems>
        <description>When the MCO rejects the opportunity email alert will be sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Booking Status modified by Details</fullName>
        <actions>
            <name>Booking_Status_modified_by_Department</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Booking_Status_modified_by_User</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Booking_Status_modified_by_User_s_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED( Booking_Status__c ) ,OR(ISPICKVAL(Booking_Status__c, &quot;Submitted&quot;),  ISPICKVAL(Booking_Status__c, &quot;Submitted MCO&quot;), ISPICKVAL(Booking_Status__c, &quot;Submitted RSS/SQI&quot;),         ISPICKVAL(Booking_Status__c, &quot;WIP MCO&quot;), ISPICKVAL(Booking_Status__c, &quot;WIP RSS/SQI&quot;), ISPICKVAL(Booking_Status__c, &quot;Booked&quot;), ISPICKVAL(Booking_Status__c, &quot;Rejected RSS/SQI&quot;), ISPICKVAL(Booking_Status__c, &quot;Rejected MCO&quot;), ISPICKVAL(Booking_Status__c, &quot;Rejected Renewals&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create Inside Sales Opportunities</fullName>
        <actions>
            <name>Create_Opportunity</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Opportunity Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Integration_Error_Message__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create Opportunity</fullName>
        <actions>
            <name>Create_Opportunity</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <formula>AND(  Len(Opportunity_Number__c) = 0,  Len ( Integration_Error_Message__c ) = 0 )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Quote</fullName>
        <actions>
            <name>Create_Quote</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <formula>AND( NOT(ISPICKVAL(Sell_Relationship__c,&apos;&apos;)), NOT(ISPICKVAL(Quote_Operating_Unit__c ,&apos;&apos;)), Len(Quote_Cart_Number__c )=0, Len( Integration_Error_Message__c )=0, $User.Id&lt;&gt;&apos;00570000001JctW&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ELA Opportunity Update</fullName>
        <actions>
            <name>Send_email_notification_when_ELA_will_be_checked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The email notification will be sent only after the ELA checkbox has been checked and the 	   	        Opportunity has been saved.</description>
        <formula>OR(AND(ISNEW(),ELA2__c),AND(PRIORVALUE(ELA2__c) != ELA2__c, ELA2__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert for Booked deals for IIG</fullName>
        <actions>
            <name>Email_Alert_for_Booked_deals_for_IIG</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>To alert operations when an Opportunity is Booked</description>
        <formula>AND( ISPICKVAL( StageName , &apos;Booked&apos;),   OR( ISPICKVAL(PRIORVALUE(StageName),&apos;Pipeline&apos;),  ISPICKVAL(PRIORVALUE(StageName),&apos;Upside&apos;),  ISPICKVAL(PRIORVALUE(StageName),&apos;Strong Upside&apos;),  ISPICKVAL(PRIORVALUE(StageName),&apos;Commit&apos;) ),   ISPICKVAL(Sales_Force__c, &apos;ESG&apos;) ,   OR(  AND(  Month(DateValue(Now())) &lt;= 3,  Month(CloseDate)&lt;= 3),  AND(AND(Month(DateValue(Now())) &gt; 3,Month(DateValue(Now()))&lt;=6) ,  And(And(Month(CloseDate) &gt;3, Month (CloseDate) &lt;= 6))),  AND(  AND(Month(DateValue(Now())) &gt; 6,Month(DateValue(Now()))&lt;=9) ,And(  And(Month(CloseDate) &gt;6, Month (CloseDate) &lt;= 9))),  AND(  AND(Month(DateValue(Now())) &gt; 9,Month(DateValue(Now()))&lt;=12) ,  And(  And(Month(CloseDate) &gt;9, Month (CloseDate) &lt;= 12)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert when Oppty downgrades from commit</fullName>
        <actions>
            <name>Opportunity_Oppty_downgrades_Commit</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email Alert when Oppty downgrades from commit</description>
        <formula>AND( (Year(DateValue(Now())) = Year(CloseDate)), AND( OR(ISPICKVAL(StageName,&apos;Pipeline&apos;),ISPICKVAL(StageName,&apos;Upside&apos;), ISPICKVAL(StageName,&apos;Strong Upside&apos;)), (ISPICKVAL(PRIORVALUE(StageName),&apos;Commit&apos;)), (Dollar_Amount__c &gt;= 250000.0000), ISPICKVAL(Opportunity_Owner__r.Theater__c,&apos;APJ&apos;)), OR( AND( Month(DateValue(Now())) &lt;= 3, Month(CloseDate)&lt;= 3), AND(AND(Month(DateValue(Now())) &gt; 3,Month(DateValue(Now()))&lt;=6) , And(And(Month(CloseDate) &gt;3, Month (CloseDate) &lt;= 6))), AND( AND(Month(DateValue(Now())) &gt; 6,Month(DateValue(Now()))&lt;=9) ,And( And(Month(CloseDate) &gt;6, Month (CloseDate) &lt;= 9))), AND( AND(Month(DateValue(Now())) &gt; 9,Month(DateValue(Now()))&lt;=12) , And( And(Month(CloseDate) &gt;9, Month (CloseDate) &lt;= 12)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert when Oppty moves into commit</fullName>
        <actions>
            <name>Opportunity_Moves_into_Commit</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email Alert when Oppty moves into comitt</description>
        <formula>AND( (Year(DateValue(Now())) = Year(CloseDate)),  AND( (ISPICKVAL(StageName,&apos;Commit&apos;)), (OR( ISPICKVAL(PRIORVALUE(StageName),&apos;Pipeline&apos;), ISPICKVAL(PRIORVALUE(StageName),&apos;Upside&apos;), ISPICKVAL(PRIORVALUE(StageName),&apos;Strong Upside&apos;) )), Dollar_Amount__c  &gt;= 250000.0000, ISPICKVAL(Opportunity_Owner__r.Theater__c,&apos;APJ&apos;)), OR(  AND( Month(DateValue(Now())) &lt;= 3, Month(CloseDate)&lt;= 3),  AND(AND(Month(DateValue(Now())) &gt; 3,Month(DateValue(Now()))&lt;=6) , And(And(Month(CloseDate) &gt;3, Month (CloseDate) &lt;= 6))),  AND( AND(Month(DateValue(Now())) &gt; 6,Month(DateValue(Now()))&lt;=9) ,And( And(Month(CloseDate) &gt;6, Month (CloseDate) &lt;= 9))),  AND( AND(Month(DateValue(Now())) &gt; 9,Month(DateValue(Now()))&lt;=12) , And( And(Month(CloseDate) &gt;9, Month (CloseDate) &lt;= 12)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IIG Deal Desk Approval Checked</fullName>
        <actions>
            <name>Update_IIG_Deal_Desk_Approval_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IIG_Deal_Desk_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Fires when the IIG Deal Desk Approval field on Opportunity is checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IIG Deal Desk Attributes Update</fullName>
        <actions>
            <name>Update_IIG_Deal_Desk_Last_Updated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Fires when any of the following fields are changed on Opportunity: 
IIG Agreement Duration 
IIG License Discount (%) 
IIG Maintenance Discount 
IIG Maintenance Type 
IIG Non Standard Deal Comments 
IIG Non Standard Deal Elements.</description>
        <formula>ISCHANGED( IIG_Agreement_Duration__c ) || ISCHANGED( License_Discount__c ) || ISCHANGED( Maintenance_Rate__c ) || ISCHANGED( Maintenance_Type__c ) || ISCHANGED( IIG_Non_Standard_Deal_Comments__c ) || ISCHANGED( IIG_Non_Standard_Deal_Elements__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Initial Forecast Quarter Workflow Rule</fullName>
        <actions>
            <name>Initial_Forecast_Quarter_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Linked to Deal Reg Chk For Sales Channel</fullName>
        <actions>
            <name>Sales_Channel_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_DDO_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the Sales Channel Field to Indirect when the Linked to Deal Registration Check Box is Checked or partner Information is populated.</description>
        <formula>IF(ISPICKVAL( Sales_Channel__c , &quot;OEM&quot;),
false ,
(IF(ISPICKVAL( Sales_Channel__c , &quot;Indirect&quot;),
false,
OR(Partner__c &lt;&gt; null,Tier_2_Partner__c &lt;&gt; null, Linked_to_Deal_Registration__c = True))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MCO Analyst</fullName>
        <actions>
            <name>MCO_Analyst_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification to send MCO Analyst name</description>
        <formula>IF(ISNEW(), NOT(ISBLANK(MCO__c)), ISCHANGED(MCO__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Meddic Manager Requested</fullName>
        <actions>
            <name>Manager_Requested_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Manager_Requested__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify New EMC Contract Renewals Opportunity Account Owner</fullName>
        <actions>
            <name>Contract_Renewals_new_Account_Owner_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Send email alerts to users when they are made the Account Owner on an Opportunity and the new owner is a Contract Renewal User i.e. profile is &apos;EMC Contract Renewals User&apos;</description>
        <formula>ISCHANGED( Opportunity_Owner__c ) &amp;&amp; Opportunity_Owner__r.Profile_Name__c == &apos;EMC Contract Renewals User&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify New EMEA Opportunity Account Owner</fullName>
        <actions>
            <name>EMEA_Opportunity_Account_Owner_Change_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send email alerts to users when they are made the Account Owner on an Opportunity and the new owner is a EMEA Enterprise Users or ENEA Commercial Users or EMEA GAM Users Profiles</description>
        <formula>ISCHANGED( Opportunity_Owner__c )  &amp;&amp;  (  Opportunity_Owner__r.Profile_Name__c == &apos;EMEA Enterprise User&apos; ||  Opportunity_Owner__r.Profile_Name__c == &apos;EMEA Commercial User&apos; ||  (Opportunity_Owner__r.Profile_Name__c == &apos;EMC GAM User&apos;  &amp;&amp;  ISPICKVAL(Opportunity_Owner__r.Theater__c, &apos;EMEA&apos;)  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opportunity is Closed</fullName>
        <actions>
            <name>Alert_Opportunity_is_Closed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( StageName ) &amp;&amp;  ISPICKVAL(StageName , &apos;Closed&apos;)  &amp;&amp;  NOT(ISPICKVAL( Closed_Reason__c ,&apos;Lost to Competition&apos;)) &amp;&amp; Dollar_Amount__c &gt; 15000 &amp;&amp; chkSalesTeam__c &amp;&amp;   Current_Quarter__c  =  Close_Date_Quarter__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opportunity is Closed Date moved to%2Ffrom Future Quarter</fullName>
        <actions>
            <name>Alert_Opportunity_Close_Date_Moved_to_from_a_Future_Quarter</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>PRIORVALUE( CloseDate )  &lt;&gt;  CloseDate   &amp;&amp;  (  IF ( (FLOOR( (MONTH( PRIORVALUE( CloseDate ) ) -1)/3) = FLOOR( (MONTH( TODAY() )-1)/3 )) &amp;&amp; (YEAR( PRIORVALUE( CloseDate ) ) = YEAR( TODAY() )) &amp;&amp; (FLOOR( (MONTH( CloseDate )-1)/3 ) &gt; FLOOR( (MONTH( PRIORVALUE( CloseDate ) ) -1)/3)) &amp;&amp; (YEAR( CloseDate ) = YEAR( TODAY() )),true,false ) || IF ( (FLOOR( (MONTH( PRIORVALUE( CloseDate ) ) -1)/3) = FLOOR( (MONTH( TODAY() )-1)/3 )) &amp;&amp; (YEAR( PRIORVALUE( CloseDate ) ) = YEAR( TODAY() )) &amp;&amp; (YEAR( CloseDate ) &gt; YEAR( TODAY() )),true,false ) || IF ( (YEAR( PRIORVALUE( CloseDate ) ) &gt; YEAR( TODAY() )) &amp;&amp; (FLOOR( (MONTH( CloseDate )-1)/3 ) = FLOOR( (MONTH( TODAY() )-1)/3 )) &amp;&amp; (YEAR( CloseDate ) = YEAR( TODAY() )),true,false ) || IF ( (FLOOR( (MONTH( PRIORVALUE( CloseDate ) ) -1)/3) &gt; FLOOR( (MONTH( CloseDate )-1)/3 )) &amp;&amp; (YEAR( PRIORVALUE( CloseDate ) ) = YEAR( TODAY() )) &amp;&amp; (FLOOR( (MONTH( CloseDate )-1)/3 ) = FLOOR( (MONTH( TODAY() )-1)/3 )) &amp;&amp; (YEAR( CloseDate ) = YEAR( TODAY() )),true,false ) )  &amp;&amp;  (  ISPICKVAL(StageName , &apos;Commit&apos;) || ISPICKVAL(StageName , &apos;Upside&apos;) || ISPICKVAL(StageName , &apos;Strong Upside&apos;)  )  &amp;&amp;  Dollar_Amount__c &gt; 15000  &amp;&amp;  chkSalesTeam__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opportunity is Closed to Competition</fullName>
        <actions>
            <name>Alert_Opportunity_is_Closed_to_Competition</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( StageName ) &amp;&amp; ISPICKVAL(StageName , &apos;Closed&apos;)  &amp;&amp;  ISPICKVAL( Closed_Reason__c ,&apos;Lost to Competition&apos;)  &amp;&amp;  chkSalesTeam__c &amp;&amp; Current_Quarter__c = Close_Date_Quarter__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opportunity moved from Commit to Strong Upside</fullName>
        <actions>
            <name>Alert_Opportunity_moved_from_Commit_to_Strong_Upside</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( StageName )  &amp;&amp;   ISPICKVAL(StageName , &apos;Strong Upside&apos;)  &amp;&amp; ISPICKVAL(  PRIORVALUE(StageName)  , &apos;Commit&apos;) &amp;&amp;   Dollar_Amount__c &gt; 15000 &amp;&amp;  chkSalesTeam__c &amp;&amp;   Current_Quarter__c  =  Close_Date_Quarter__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opportunity moved from Commit to Upside</fullName>
        <actions>
            <name>Alert_Opportunity_moved_from_Commit_to_Upside</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( StageName )  &amp;&amp;   ISPICKVAL(StageName , &apos;Upside&apos;)  &amp;&amp; ISPICKVAL(  PRIORVALUE(StageName)  , &apos;Commit&apos;) &amp;&amp;   Dollar_Amount__c &gt; 15000 &amp;&amp; chkSalesTeam__c &amp;&amp;   Current_Quarter__c  =  Close_Date_Quarter__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opportunity moved from Strong Upside to Upside</fullName>
        <actions>
            <name>Alert_Opportunity_moved_from_Strong_Upside_to_Upside</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( StageName )  &amp;&amp;   ISPICKVAL(StageName , &apos;Upside&apos;)  &amp;&amp; ISPICKVAL(  PRIORVALUE(StageName)  , &apos;Strong Upside&apos;) &amp;&amp;   Dollar_Amount__c &gt; 15000 &amp;&amp;  chkSalesTeam__c &amp;&amp;   Current_Quarter__c  =  Close_Date_Quarter__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Integration Error Messages</fullName>
        <actions>
            <name>Send_Integration_Error_Message</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Integration_Error_Message__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Link Updation</fullName>
        <actions>
            <name>Opportunity_Linked_Updation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Opportunity Linked to True when an opportunity number gets populated</description>
        <formula>NOT(ISBLANK( BRS_Opportunity_Nbr__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Linked ReUpdation</fullName>
        <actions>
            <name>Opportunity_Linked_ReUpdation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Opportunity Linked to false in case BRS Opportunity Number gets removed</description>
        <formula>ISBLANK( BRS_Opportunity_Nbr__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity moved out of Strong Upside</fullName>
        <actions>
            <name>Opportunity_moved_out_of_Strong_Upside_APJ</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  AND(                  ISPICKVAL(PRIORVALUE(StageName),&apos;Strong Upside&apos;),    		  	OR(  		ISPICKVAL(StageName,&apos;Upside&apos;),                 ISPICKVAL(StageName,&apos;Pipeline&apos;) 	),  (Dollar_Amount__c &gt;= 250000.0000),  ISPICKVAL(Opportunity_Owner__r.Theater__c,&apos;APJ&apos;)), ISPICKVAL(Sales_Force__c, &apos;EMC&apos;) ,   (Year(DateValue(Now())) = Year(CloseDate)), OR( AND( Month(DateValue(Now())) &lt;= 3, Month(CloseDate)&lt;= 3), AND(AND(Month(DateValue(Now())) &gt; 3,Month(DateValue(Now()))&lt;=6) , And(And(Month(CloseDate) &gt;3, Month (CloseDate) &lt;= 6))), AND( AND(Month(DateValue(Now())) &gt; 6,Month(DateValue(Now()))&lt;=9) ,And( And(Month(CloseDate) &gt;6, Month (CloseDate) &lt;= 9))), AND( AND(Month(DateValue(Now())) &gt; 9,Month(DateValue(Now()))&lt;=12) , And( And(Month(CloseDate) &gt;9, Month (CloseDate) &lt;= 12)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity moved to Booked</fullName>
        <actions>
            <name>Opportunity_moved_to_Booked_APJ</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  AND(      		ISPICKVAL(StageName,&apos;Booked&apos;),  	OR(  		ISPICKVAL(PRIORVALUE(StageName),&apos;Pipeline&apos;),  		ISPICKVAL(PRIORVALUE(StageName),&apos;Upside&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Strong Upside&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Commit&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Closed&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Won&apos;),  		ISPICKVAL(PRIORVALUE(StageName),&apos;Submitted&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Eval&apos;) 	),  (Dollar_Amount__c &gt;= 250000.0000),  ISPICKVAL(Opportunity_Owner__r.Theater__c,&apos;APJ&apos;)), ISPICKVAL(Sales_Force__c, &apos;EMC&apos;) ,   (Year(DateValue(Now())) = Year(CloseDate)), OR( AND( Month(DateValue(Now())) &lt;= 3, Month(CloseDate)&lt;= 3), AND(AND(Month(DateValue(Now())) &gt; 3,Month(DateValue(Now()))&lt;=6) , And(And(Month(CloseDate) &gt;3, Month (CloseDate) &lt;= 6))), AND( AND(Month(DateValue(Now())) &gt; 6,Month(DateValue(Now()))&lt;=9) ,And( And(Month(CloseDate) &gt;6, Month (CloseDate) &lt;= 9))), AND( AND(Month(DateValue(Now())) &gt; 9,Month(DateValue(Now()))&lt;=12) , And( And(Month(CloseDate) &gt;9, Month (CloseDate) &lt;= 12)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity moved to Closed</fullName>
        <actions>
            <name>Opportunity_moved_to_Closed_APJ</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  AND(          OR(                  ISPICKVAL(PRIORVALUE(StageName),&apos;Pipeline&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Upside&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Strong Upside&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Commit&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Won&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Submitted&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Eval&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Booked&apos;)  	),   	          ISPICKVAL(StageName,&apos;Closed&apos;),   (Dollar_Amount__c &gt;= 250000.0000),  ISPICKVAL(Opportunity_Owner__r.Theater__c,&apos;APJ&apos;)), ISPICKVAL(Sales_Force__c, &apos;EMC&apos;) ,   (Year(DateValue(Now())) = Year(CloseDate)), OR( AND( Month(DateValue(Now())) &lt;= 3, Month(CloseDate)&lt;= 3), AND(AND(Month(DateValue(Now())) &gt; 3,Month(DateValue(Now()))&lt;=6) , And(And(Month(CloseDate) &gt;3, Month (CloseDate) &lt;= 6))), AND( AND(Month(DateValue(Now())) &gt; 6,Month(DateValue(Now()))&lt;=9) ,And( And(Month(CloseDate) &gt;6, Month (CloseDate) &lt;= 9))), AND( AND(Month(DateValue(Now())) &gt; 9,Month(DateValue(Now()))&lt;=12) , And( And(Month(CloseDate) &gt;9, Month (CloseDate) &lt;= 12)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity moved to Eval</fullName>
        <actions>
            <name>Opportunity_moved_to_Eval_APJ</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  AND(      		ISPICKVAL(StageName,&apos;Eval&apos;),  	OR(  		ISPICKVAL(PRIORVALUE(StageName),&apos;Pipeline&apos;),  		ISPICKVAL(PRIORVALUE(StageName),&apos;Upside&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Strong Upside&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Commit&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Submitted&apos;) 	),  (Dollar_Amount__c &gt;= 250000.0000),  ISPICKVAL(Opportunity_Owner__r.Theater__c,&apos;APJ&apos;)), ISPICKVAL(Sales_Force__c, &apos;EMC&apos;) ,   (Year(DateValue(Now())) = Year(CloseDate)), OR( AND( Month(DateValue(Now())) &lt;= 3, Month(CloseDate)&lt;= 3), AND(AND(Month(DateValue(Now())) &gt; 3,Month(DateValue(Now()))&lt;=6) , And(And(Month(CloseDate) &gt;3, Month (CloseDate) &lt;= 6))), AND( AND(Month(DateValue(Now())) &gt; 6,Month(DateValue(Now()))&lt;=9) ,And( And(Month(CloseDate) &gt;6, Month (CloseDate) &lt;= 9))), AND( AND(Month(DateValue(Now())) &gt; 9,Month(DateValue(Now()))&lt;=12) , And( And(Month(CloseDate) &gt;9, Month (CloseDate) &lt;= 12)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity moved to Strong Upside</fullName>
        <actions>
            <name>Opportunity_moved_to_Strong_Upside_APJ</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  AND(      		ISPICKVAL(StageName,&apos;Strong Upside&apos;),  	OR(  		ISPICKVAL(PRIORVALUE(StageName),&apos;Pipeline&apos;),  		ISPICKVAL(PRIORVALUE(StageName),&apos;Upside&apos;) 	),  (Dollar_Amount__c &gt;= 250000.0000),  ISPICKVAL(Opportunity_Owner__r.Theater__c,&apos;APJ&apos;)), ISPICKVAL(Sales_Force__c, &apos;EMC&apos;) ,   (Year(DateValue(Now())) = Year(CloseDate)), OR( AND( Month(DateValue(Now())) &lt;= 3, Month(CloseDate)&lt;= 3), AND(AND(Month(DateValue(Now())) &gt; 3,Month(DateValue(Now()))&lt;=6) , And(And(Month(CloseDate) &gt;3, Month (CloseDate) &lt;= 6))), AND( AND(Month(DateValue(Now())) &gt; 6,Month(DateValue(Now()))&lt;=9) ,And( And(Month(CloseDate) &gt;6, Month (CloseDate) &lt;= 9))), AND( AND(Month(DateValue(Now())) &gt; 9,Month(DateValue(Now()))&lt;=12) , And( And(Month(CloseDate) &gt;9, Month (CloseDate) &lt;= 12)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity moved to Submitted</fullName>
        <actions>
            <name>Opportunity_moved_to_Submitted_APJ</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  AND(      		ISPICKVAL(StageName,&apos;Submitted&apos;),  	OR(  		ISPICKVAL(PRIORVALUE(StageName),&apos;Pipeline&apos;),  		ISPICKVAL(PRIORVALUE(StageName),&apos;Upside&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Strong Upside&apos;),                 ISPICKVAL(PRIORVALUE(StageName),&apos;Commit&apos;) 	),  (Dollar_Amount__c &gt;= 250000.0000),  ISPICKVAL(Opportunity_Owner__r.Theater__c,&apos;APJ&apos;)), ISPICKVAL(Sales_Force__c, &apos;EMC&apos;) ,   (Year(DateValue(Now())) = Year(CloseDate)), OR( AND( Month(DateValue(Now())) &lt;= 3, Month(CloseDate)&lt;= 3), AND(AND(Month(DateValue(Now())) &gt; 3,Month(DateValue(Now()))&lt;=6) , And(And(Month(CloseDate) &gt;3, Month (CloseDate) &lt;= 6))), AND( AND(Month(DateValue(Now())) &gt; 6,Month(DateValue(Now()))&lt;=9) ,And( And(Month(CloseDate) &gt;6, Month (CloseDate) &lt;= 9))), AND( AND(Month(DateValue(Now())) &gt; 9,Month(DateValue(Now()))&lt;=12) , And( And(Month(CloseDate) &gt;9, Month (CloseDate) &lt;= 12)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty Status Submitted</fullName>
        <actions>
            <name>Status_Submitted_Or_Booked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Booked</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PROPEL_Populate Opportunity Number</fullName>
        <actions>
            <name>Populate_Opporutunity_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_RecordTypeID_Pre_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.SFDC_Opportunity_number_generate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>PROPEL Release 3: Populate Oppty number if SFDC Opportunity number generate is not null</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PROPEL_Retrigger_Opportunity_Registration</fullName>
        <actions>
            <name>Reset_Send_Oppty_Registration_Notificati</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PROPEL_Send_Opportunity_Registrations</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Send_Oppty_Registration_Notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Registration_Nbr__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Created to re trigger Opportunity registration OB for PROPEL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PROPEL_Send Opportunity Registrations_WF</fullName>
        <actions>
            <name>PROPEL_Send_Opportunity_Registrations</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Created for PROPEL</description>
        <formula>OR( ISCHANGED(Shared_to_Partner__c)&amp;&amp; NOT(ISNEW()) , AND(ISPICKVAL(Shared_to_Partner__c,&quot;YES&quot;), OR(ISCHANGED(Name),										ISCHANGED(Opportunity_Registration_Status__c),									ISCHANGED(Registration_Type__c),					ISCHANGED(Partner_Approving_User__c),							ISCHANGED(Partner__c),						ISCHANGED(Tier_2_Partner__c),								ISCHANGED(Account_Name1__c)							)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Solutions Win Last Modified</fullName>
        <actions>
            <name>populate_Solutions_Win_Last_Modified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR ( OR(AND(ISNEW(),Solutions_Win_Comments__c&lt;&gt;NULL),  PRIORVALUE(Solutions_Win_Comments__c)&lt;&gt; Solutions_Win_Comments__c),  OR(AND(ISNEW(),Services_Comments__c&lt;&gt;NULL),  PRIORVALUE(Services_Comments__c)&lt;&gt; Services_Comments__c),  OR(AND(ISNEW(),Presales_Manager_Comments__c&lt;&gt;NULL),  PRIORVALUE(Presales_Manager_Comments__c)&lt;&gt; Presales_Manager_Comments__c),  OR(AND(ISNEW(),Services_Opportunity_Number__c&lt;&gt;NULL),  PRIORVALUE(Services_Opportunity_Number__c)&lt;&gt; Services_Opportunity_Number__c),  OR(AND(ISNEW(),TEXT(Are_there_Services__c)&lt;&gt;NULL),  ISCHANGED(Are_there_Services__c)),  OR(AND(ISNEW(),TEXT(Does_Presales_have_the_Solutions_Win__c)&lt;&gt;NULL),  ISCHANGED(Does_Presales_have_the_Solutions_Win__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RSS%2FSQI Co-ordinator</fullName>
        <actions>
            <name>RSS_Coordinator_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification to send RSS/SQI Co-ordinator name</description>
        <formula>IF(ISNEW(), NOT(ISBLANK(RSS_Coordinator__c)), ISCHANGED(RSS_Coordinator__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rev Rec Analyst</fullName>
        <actions>
            <name>Rev_Rec_Analyst_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification to send Rev Rec Analyst name</description>
        <formula>IF(ISNEW(), NOT(ISBLANK(Rev_Rec_Analyst__c)), ISCHANGED(Rev_Rec_Analyst__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Revert RecordType to Pre Quote</fullName>
        <actions>
            <name>Update_RecordTypeID_Pre_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a new opportunity is created and user uses Update Sales Team/Splits page before Oppty number comes back from DxP. Rec type is changing to New Manually Created record type. This Rule reverts the record type to Pre-Quote Record Type in that case</description>
        <formula>AND(  (RecordTypeId = &apos;01270000000Q42o&apos;),  (PRIORVALUE(RecordTypeId ) =&apos;01270000000Q42t&apos; ),  NOT(ISBLANK( Opportunity_Number__c ) ) ,  ISBLANK( Quote_Cart_Number__c )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email Notification to Direct Reps for Americas</fullName>
        <actions>
            <name>Email_Notification_to_Direct_Reps_for_Americas</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( 
ISCHANGED(StageName) , 
Account_Owner_Theater__c =&quot;Americas&quot;, 
ISPICKVAL(StageName,&quot;Booked&quot;), 
Distributor_Reseller_VAR_Rating_Eligible__c=&quot;YES&quot; 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email Notification to Direct Reps for EMEA</fullName>
        <actions>
            <name>Email_Notification_to_Direct_Reps_For_EMEA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( 
ISCHANGED(StageName) , 
Account_Owner_Theater__c =&quot;EMEA&quot;, 
ISPICKVAL(StageName,&quot;Booked&quot;), 
Distributor_Reseller_VAR_Rating_Eligible__c=&quot;YES&quot; 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Outbound Message when Opportunity is Booked</fullName>
        <actions>
            <name>Mark_as_flipped_to_Booked_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Presales_SubmittedOpportunity</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends out an outbound message that kicks off the integration, which brings attachments from the opportunity over to OPR. This rule works when any Opportunity is changed to the Forecast Status submitted or booked.</description>
        <formula>AND 
( 
(ISPICKVAL(StageName, &apos;Submitted&apos;) || ISPICKVAL(StageName, &apos;Booked&apos;)), 
( $RecordType.Name != &apos;Booked Opportunity Record Type&apos;), 
( Account_TheaterText__c !=&apos;APJ&apos;), 
( Workflow_Updated_to_Booked_RT__c != True), 
(Consulting_Forecast__c &gt; 0) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Forecast Amount to zero on Cloned Opportunity creation</fullName>
        <actions>
            <name>Set_Forecast_Amount_to_zero</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.LeadSource</field>
            <operation>equals</operation>
            <value>Cloned Opportunity</value>
        </criteriaItems>
        <description>When a new Opportunity is created with a &apos;Lead Source&apos; of &apos;Cloned Opportunity&apos;, set the Forecast Amount value to zero</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Stage-1 Discovery</fullName>
        <actions>
            <name>Stage_1_Discovery</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
OR( ISPICKVAL(IIG_Sales_Stage__c ,&quot;Stage-1 Discovery&quot;), 
ISPICKVAL( IIG_Sales_Stage__c ,&quot;Stage-2 Scope / Qualify&quot;) 
),
ISCHANGED(IIG_Sales_Stage__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage-3 Solution Overview</fullName>
        <actions>
            <name>Stage_3_Solution_Overview</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
OR( ISPICKVAL(IIG_Sales_Stage__c ,&quot;Stage-3 Solution Overview&quot;), 
ISPICKVAL( IIG_Sales_Stage__c ,  &quot;Stage-4 Solution Validation&quot;) 
),
ISCHANGED(IIG_Sales_Stage__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage-5 Solution Proposal</fullName>
        <actions>
            <name>Stage_5_Solution_Proposal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
ISPICKVAL(IIG_Sales_Stage__c ,&quot;Stage-5 Solution Proposal&quot;), 
ISCHANGED(IIG_Sales_Stage__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage-6 Solution Selection</fullName>
        <actions>
            <name>Stage_7_Negotiate_Contracts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
OR( ISPICKVAL(IIG_Sales_Stage__c ,&quot;Stage-6 Solution Selection&quot;), 
ISPICKVAL( IIG_Sales_Stage__c , &quot;Stage-7 Negotiate &amp; Contracts&quot;) 
),
ISCHANGED(IIG_Sales_Stage__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage-8 Close</fullName>
        <actions>
            <name>Stage_8_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(
ISPICKVAL(IIG_Sales_Stage__c ,&quot;Stage-8 Close&quot;), 
ISCHANGED(IIG_Sales_Stage__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status changed after Booked</fullName>
        <actions>
            <name>Update_RecordType_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Record type should be New Booked Record type whenever stage equals Booked</description>
        <formula>AND( RecordTypeId==&apos;01270000000Q42j&apos;, NOT( ISPICKVAL(StageName, &apos;Booked&apos;) ), NOT( ISPICKVAL(StageName, &apos;Submitted&apos;) ), LEN(Quote_Cart_Number__c)!=0, ( RecordTypeId &lt;&gt; &apos;012700000005VYn&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status is Booked</fullName>
        <actions>
            <name>UpdateRecordTypeOppBooked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Booked</value>
        </criteriaItems>
        <description>Update to Booked Opportunity Record Type. Criteria: &quot;Status&quot; is equal to Booked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status not Booked</fullName>
        <actions>
            <name>UpdateRecordTypeOppStandard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Booked</value>
        </criteriaItems>
        <description>Update to Standard Opportunity Record Type. Criteria: &quot;Status&quot; is not equal to Booked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Unbook and Quote not Attached</fullName>
        <actions>
            <name>Update_RecordTypeID_Pre_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( RecordTypeId==&apos;01270000000Q42j&apos;, NOT( ISPICKVAL(StageName, &apos;Booked&apos;) ), NOT( ISPICKVAL(StageName, &apos;Submitted&apos;) ), LEN(Quote_Cart_Number__c)=0, ( RecordTypeId &lt;&gt; &apos;012700000005VYn&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR Central</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_Central</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos; ), Account_Owner_Division__c = &apos;Central&apos; , ISPICKVAL(StageName , &apos;Booked&apos;),Dollar_Amount__c &gt;= 100000,Dollar_Amount__c &lt; 500000, NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR Central 500k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_Central_500k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos; ), Account_Owner_Division__c = &apos;Central&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR Federal</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_Federal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos;), Account_Owner_Division__c = &apos;Federal&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 100000,Dollar_Amount__c &lt; 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR Federal 500k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_Federal_500k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos;), Account_Owner_Division__c = &apos;Federal&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR LATAM</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_LATAM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos;), Account_Owner_Division__c = &apos;LATAM&apos; , ISPICKVAL(StageName , &apos;Booked&apos;),Dollar_Amount__c &gt;= 100000,Dollar_Amount__c &lt; 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR LATAM 500k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_LATAM_500k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos;), Account_Owner_Division__c = &apos;LATAM&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR MM Central 100k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_MM_Central_100k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos;), Account_Owner_Division__c = &apos;Commercial&apos; ,	Account_Owner_Region__c = &apos;Central&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 100000,Dollar_Amount__c &lt; 500000,  NOT( ISBLANK( Quote_Cart_Number__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR MM Central 500k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_MM_Central_500k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos;), Account_Owner_Division__c = &apos;Commercial&apos; ,	Account_Owner_Region__c = &apos;Central&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 500000,  NOT( ISBLANK( Quote_Cart_Number__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR MM East 100k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_MM_East_100k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos;), Account_Owner_Division__c = &apos;Commercial&apos; ,	Account_Owner_Region__c = &apos;East&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 100000,Dollar_Amount__c &lt; 500000,  NOT( ISBLANK( Quote_Cart_Number__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR MM East 500k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_MM_East_500k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos;), Account_Owner_Division__c = &apos;Commercial&apos; ,	Account_Owner_Region__c = &apos;East&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 500000,  NOT( ISBLANK( Quote_Cart_Number__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR MM West 100k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_MM_West100k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos;), Account_Owner_Division__c = &apos;Commercial&apos; ,	Account_Owner_Region__c = &apos;West&apos; , ISPICKVAL(StageName , &apos;Booked&apos;),Dollar_Amount__c &gt;= 100000,Dollar_Amount__c &lt; 500000,  NOT( ISBLANK( Quote_Cart_Number__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR MM West 500k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_MM_West_500k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos;), Account_Owner_Division__c = &apos;Commercial&apos; ,	Account_Owner_Region__c = &apos;West&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 500000,  NOT( ISBLANK( Quote_Cart_Number__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR Mid-Atlantic%2FSouth</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_Mid_Atlantic_South</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos;), Account_Owner_Division__c = &apos;Mid-Atlantic/South&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 100000,Dollar_Amount__c &lt; 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR Mid-Atlantic%2FSouth 500k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_Mid_Atlantic_South_500k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos;), Account_Owner_Division__c = &apos;Mid-Atlantic/South&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR NE%2FCanada</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_NE_Canada</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos; ), Account_Owner_Division__c = &apos;NE/Canada&apos; , ISPICKVAL(StageName , &apos;Booked&apos;),Dollar_Amount__c &gt;= 100000,Dollar_Amount__c &lt; 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR NE%2FCanada 500k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_NE_Canada_500k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos; ), Account_Owner_Division__c = &apos;NE/Canada&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR NY%2FNJ</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_NY_NJ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos; ), Account_Owner_Division__c = &apos;NY/NJ&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 100000,Dollar_Amount__c &lt; 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR NY%2FNJ 500k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_NY_NJ_500k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos; ), Account_Owner_Division__c = &apos;NY/NJ&apos; , ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR West</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_West</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos; ), Account_Owner_Division__c = &apos;West&apos;, ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 100000,Dollar_Amount__c &lt; 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Under Pen Opportunity Win WR West 500k</fullName>
        <actions>
            <name>Under_Pen_Opportunity_Win_Alert_West_500k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( Under_Pen_Account_Type__c = &apos;STRATEGIC ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;DIVISIONAL ACCOUNT&apos;, Under_Pen_Account_Type__c = &apos;AREA ACCOUNT&apos; ), Account_Owner_Division__c = &apos;West&apos;, ISPICKVAL(StageName , &apos;Booked&apos;), Dollar_Amount__c &gt;= 500000,  NOT( ISBLANK( Quote_Cart_Number__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Forecast Amount to Quote Amount</fullName>
        <actions>
            <name>Check_Update_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update to Booked Record Type</description>
        <formula>AND(  OR(isPickVal(StageName,&quot;Booked&quot;),isPickVal(StageName,&quot;Submitted&quot;) ),  Len(Quote_Cart_Number__c)&gt;0,  $Profile.Name=&quot;System Administrator - API Only&quot;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Record Type when Status is Booked</fullName>
        <actions>
            <name>Update_RecordType_to_New_Booked_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Booked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Booked Opportunity Record Type</value>
        </criteriaItems>
        <description>Record type should be New Booked Record type whenever stage equals Booked</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update to Booked Record Type</fullName>
        <actions>
            <name>Update_RecordType_ID_To_Booked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3) AND 2 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Booked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Booked Opportunity Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Booked w/ Partner Perf Rating Record Type</value>
        </criteriaItems>
        <description>Update to Booked Opportunity Record Type. Criteria: &quot;Status&quot; is equal to Booked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update to Deal Review</fullName>
        <actions>
            <name>Deal_Review_Modified_By</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Document the name of the user and when they updated Deal Review field.</description>
        <formula>ISCHANGED( Deal_Review__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update to Next Steps Updated</fullName>
        <actions>
            <name>Update_Last_Modified_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Last_Modified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Document the name of the user and when they updated the next steps field.</description>
        <formula>ISCHANGED( Next_Steps__c )  || ( ISNEW() &amp;&amp; NOT( ISBLANK( Next_Steps__c) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update to Pre-Quote Record Type</fullName>
        <actions>
            <name>Uncheck_Quote_Attached</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_RecordTypeID_Pre_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Quote_Cart_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Booked</value>
        </criteriaItems>
        <description>Change the record type to Pre-Quote Record Type after the user creates and opportunity and the integration displays a Synergy Opportunity Number.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update to Quote Attached Record Type</fullName>
        <actions>
            <name>Mark_Quote_Attached</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_RecordType_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Quote is updated or not equal to null assign it to Quote Attached Record Type.</description>
        <formula>AND(  OR(ISNEW(),  ISCHANGED(Quote_Cart_Number__c),  ischanged(StageName)),  Len(Quote_Cart_Number__c)&gt;0,  NOT(ISPICKVAL( StageName,&quot;Booked&quot;)),  ( RecordTypeId &lt;&gt; &apos;012700000005VYn&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UpdateMostRecentNextStep</fullName>
        <actions>
            <name>Update_Most_Recent_Next_Steps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Next_Steps_new__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>UpdateMostRecentSolutionsWinComments</fullName>
        <actions>
            <name>Update_Most_Recent_Sol_Win_Comments</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Most_Recent_5_Solutions_Win_Comments__c</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
