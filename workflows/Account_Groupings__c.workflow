<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>End_Date_Auto_Populated</fullName>
        <description>This Field update will update the end Date after a Grouping is inactivated.</description>
        <field>End_Date__c</field>
        <formula>NOW()</formula>
        <name>End Date Auto Populated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>End Date Auto Populated</fullName>
        <actions>
            <name>End_Date_Auto_Populated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Groupings__c.Active__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This workflow Auto Populates the End Date every time a Grouping is Inactivated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
