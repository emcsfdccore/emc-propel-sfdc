<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AP_Case_Creation_Email</fullName>
        <description>AP Case Creation Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcaccountspayable@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Accounts_Payable/AP_Case_Creation_Template</template>
    </alerts>
    <alerts>
        <fullName>AP_Case_Status_changed_to_open_Email</fullName>
        <description>AP Case Status changed to open Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcaccountspayable@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Accounts_Payable/AP_Case_Status_Change</template>
    </alerts>
    <alerts>
        <fullName>Case_Closure_Email_Notification_Contact</fullName>
        <description>Presales Case Closure Email Notification Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Contact</template>
    </alerts>
    <alerts>
        <fullName>Case_Closure_Email_Notification_User</fullName>
        <description>Presales Case Closure Email Notification User</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_User</template>
    </alerts>
    <alerts>
        <fullName>ClosedDeployedCaseStatusandEmail</fullName>
        <description>Closed-Deployed Case Status and Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/SupportForcePortalCaseClosedDeployed</template>
    </alerts>
    <alerts>
        <fullName>ClosedDuplicateNotDeployedCaseStatusandEmail</fullName>
        <description>Closed-Duplicate, Not Deployed Case Status and Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/SupportForcePortalCaseClosedNotDeployed</template>
    </alerts>
    <alerts>
        <fullName>Deal_Reg_Case_Escalated_Final</fullName>
        <description>Deal Reg Case Escalated Final</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>dealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PSC_Email_Templates/PSC_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Education_Services_Case_Created</fullName>
        <description>Education Services - Case Created</description>
        <protected>false</protected>
        <recipients>
            <field>EDS_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>educationsupport@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Education_Services_Auto_Notifications/Case_Creation_Notification_with_EMC_Logo</template>
    </alerts>
    <alerts>
        <fullName>Education_Services_Case_Resolved</fullName>
        <description>Education Services - Case Resolved</description>
        <protected>false</protected>
        <recipients>
            <field>EDS_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>educationsupport@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Education_Services_Auto_Notifications/Resolution_Notification_With_EMC_Logo</template>
    </alerts>
    <alerts>
        <fullName>Education_Services_GA_Support_Case_Created</fullName>
        <description>Education Services GA Support - Case Created</description>
        <protected>false</protected>
        <recipients>
            <field>EDS_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emctraining@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Education_Services_Auto_Notifications/Case_Creation_Notification_with_EMC_Logo</template>
    </alerts>
    <alerts>
        <fullName>Education_Services_GA_Support_Case_Resolved</fullName>
        <description>Education Services GA Support - Case Resolved</description>
        <protected>false</protected>
        <recipients>
            <field>EDS_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emctraining@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Education_Services_Auto_Notifications/Resolution_Notification_With_EMC_Logo</template>
    </alerts>
    <alerts>
        <fullName>Education_Services_Send_Do_Not_Reply_Email</fullName>
        <description>Education Services - Send Do Not Reply Email</description>
        <protected>false</protected>
        <recipients>
            <field>From_Mail_After_Close__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>donotreplyeducation@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Education_Services_Auto_Notifications/ED_Closed_Auto_Response_with_EMC_logo</template>
    </alerts>
    <alerts>
        <fullName>Global_Education_Support_Case_Created</fullName>
        <description>Global Education Support - Case Created</description>
        <protected>false</protected>
        <recipients>
            <field>EDS_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>global_edusupport@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Education_Services_Auto_Notifications/GS_Case_Creation_With_EMC_Logo</template>
    </alerts>
    <alerts>
        <fullName>Global_Education_Support_Case_Resolved</fullName>
        <description>Global Education Support - Case Resolved</description>
        <protected>false</protected>
        <recipients>
            <field>EDS_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>global_edusupport@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Education_Services_Auto_Notifications/GE_Case_Resolution_With_EMC_Logo</template>
    </alerts>
    <alerts>
        <fullName>Global_Education_Support_Send_Do_Not_Reply_Email</fullName>
        <description>Global Education Support - Send Do Not Reply Email</description>
        <protected>false</protected>
        <recipients>
            <field>From_Mail_After_Close__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>global_edusupport@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Education_Services_Auto_Notifications/GE_Case_Closed_Auto_Response</template>
    </alerts>
    <alerts>
        <fullName>PSC_Case_Close_Auto_Reply_Final</fullName>
        <description>PSC Case Close Auto Reply Final</description>
        <protected>false</protected>
        <recipients>
            <field>From_Mail_After_Close__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>partnersupport@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PSC_Email_Templates/PSC_Reply_to_Closed_Case</template>
    </alerts>
    <alerts>
        <fullName>PSC_Case_Close_Auto_Reply_Final_Deal_Reg_Cases</fullName>
        <description>PSC Case Close Auto Reply Final - Deal Reg Cases</description>
        <protected>false</protected>
        <recipients>
            <field>From_Mail_After_Close__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>dealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PSC_Email_Templates/PSC_Reply_to_Closed_Case</template>
    </alerts>
    <alerts>
        <fullName>PSC_Case_Escalated_Final</fullName>
        <description>PSC Case Escalated Final</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>partnersupport@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PSC_Email_Templates/PSC_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>PreSales_CI_Case_Pending_Internal</fullName>
        <description>PreSales CI Case Pending Internal</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>cisfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/Pressales_CI_Case_Assignment</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Assignment</fullName>
        <description>PreSales Case Assignment</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Assignment_Notification_Contact</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Assignment_Final</fullName>
        <description>PreSales Case Assignment Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Assignment_notification_Final</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Assignment_Non_User_Contact</fullName>
        <description>PreSales Case Assignment Non User Contact</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Assignment_Notification_Non_UC</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Assignment_Others</fullName>
        <description>PreSales Case Assignment Others</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Assignment_notification_User</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Assignment_User</fullName>
        <description>PreSales Case Assignment User</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Assignment_notification_User</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Close_Auto_Reply</fullName>
        <description>PreSales Case Close Auto Reply</description>
        <protected>false</protected>
        <recipients>
            <field>From_Mail_After_Close__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Auto_Reply_Contact</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Closed_Notification</fullName>
        <description>PreSales Case Closed Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Contact</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Closed_Notification_Others</fullName>
        <description>PreSales Case Closed Notification Others</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_User</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Closed_Notification_User</fullName>
        <description>PreSales Case Closed Notification User</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_User</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Closed_System_Auto_Reply_Non_User_Contact</fullName>
        <description>PreSales Case Closed System Auto Reply Non User Contact</description>
        <protected>false</protected>
        <recipients>
            <field>From_Mail_After_Close__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Auto_Reply_Non_UC</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Created_Contact</fullName>
        <description>PreSales Case Created Contact</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Creation_Contact</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Created_Final</fullName>
        <description>PreSales Case Created Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Creation_User_Final</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Created_NonUser_Contact</fullName>
        <description>PreSales Case Created NonUser/Contact</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Creation_Non_User_Contact</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Created_Others</fullName>
        <description>PreSales Case Created Others</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Creation_User</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Created_User</fullName>
        <description>PreSales Case Created User</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Creation_User</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Escalated</fullName>
        <description>PreSales Case Escalated</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Escalation_Contact</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Escalated_Non_User_Contact</fullName>
        <description>PreSales Case Escalated Non User Contact</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Escalation_Non_UC</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Escalated_Others</fullName>
        <description>PreSales Case Escalated Others</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Escalation_User</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Escalated_user</fullName>
        <description>PreSales Case Escalated user</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Escalation_User</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Pending_Engineering</fullName>
        <description>PreSales Case Pending Engineering</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/Presales_Case_Routed_to_Engineer_Contact</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Pending_Engineering_Non_User_Contact</fullName>
        <description>PreSales Case Pending Engineering Non User Contact</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/Presales_Case_Routed_to_Engineer_Non_UC</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Pending_Engineering_Others</fullName>
        <description>PreSales Case Pending Engineering Others</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/Presales_Case_Routed_to_Engineering_User</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Pending_Engineering_User</fullName>
        <description>PreSales Case Pending Engineering User</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/Presales_Case_Routed_to_Engineering_User</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Resolved_Final</fullName>
        <description>PreSales Case Resolved Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Resolution_Final</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Resolved_Non_User_Contact</fullName>
        <description>PreSales Case Resolved Non User/Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Resolution_Non_UC</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Resolved_Others</fullName>
        <description>PreSales Case Resolved Others</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Resolution_User</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Resolved_User</fullName>
        <description>PreSales Case Resolved User</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Resolution_User</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Case_Status_Resolved</fullName>
        <description>PreSales Case Status Resolved</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Resolution_Contact</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Pending_Engineering_Final</fullName>
        <description>PreSales Pending Engineering Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/Presales_Case_Routed_to_Engineering_F</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Test</fullName>
        <ccEmails>emailtosalesforce@4pqhy43k2jjght6pp3l9glbwr.tkyspmak.t.le.sandbox.salesforce.com</ccEmails>
        <description>PreSales Test</description>
        <protected>false</protected>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Creation_Contact_Test</template>
    </alerts>
    <alerts>
        <fullName>Pre_Sales_Case_Close_Auto_Reply_Final</fullName>
        <description>Pre Sales Case Close Auto Reply Final</description>
        <protected>false</protected>
        <recipients>
            <field>From_Mail_After_Close__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Auto_Reply_Final</template>
    </alerts>
    <alerts>
        <fullName>Pre_Sales_Case_Closed_Final</fullName>
        <description>Pre Sales Case Closed Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_User_Final</template>
    </alerts>
    <alerts>
        <fullName>Pre_Sales_Case_Escalation_Final</fullName>
        <description>Pre Sales Case Escalation Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Escalation_Final</template>
    </alerts>
    <alerts>
        <fullName>Presales_Case_Close_Auto_Reply_user</fullName>
        <description>PreSales Case Close Auto Reply user</description>
        <protected>false</protected>
        <recipients>
            <field>From_Mail_After_Close__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Auto_Reply_User</template>
    </alerts>
    <alerts>
        <fullName>Presales_Case_Closed_Non_User_Contact</fullName>
        <description>Presales Case Closed Non User Contact</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Non_User_Contact</template>
    </alerts>
    <alerts>
        <fullName>Presales_Case_Closure_Email_Notification_User_Final</fullName>
        <description>Presales Case Closure Email Notification User Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_User_Final</template>
    </alerts>
    <alerts>
        <fullName>Presales_Case_Created</fullName>
        <description>PreSales Case Created</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Creation_Contact</template>
    </alerts>
    <alerts>
        <fullName>Presales_Case_Update_Notify_Service_Agent</fullName>
        <description>Presales Case Update Notify Service Agent</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/Presales_Case_Updated_Notify_to_Service_Agent</template>
    </alerts>
    <alerts>
        <fullName>Presales_SLA_Completed_Non_User_Contact</fullName>
        <description>Presales_SLA_Completed_Non_User_Contact</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>presalessupportsfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Non_User_Contact</template>
    </alerts>
    <alerts>
        <fullName>Pressales_CI_Case_Owner_Change</fullName>
        <description>Pressales CI Case Owner Change</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>cisfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/Pressales_CI_Case_Owner_Change</template>
    </alerts>
    <alerts>
        <fullName>RelatedContactNewCaseCreated</fullName>
        <description>Related Contact, New Case Created</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/SupportForcePortalCaseCreated</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Assignment_Contact</fullName>
        <description>SC Case Assignment Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Assignment_Notification_Contact</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Assignment_Final</fullName>
        <description>SC Case Assignment Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Assignment_notification_Final</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Assignment_Non_User_Contact</fullName>
        <description>SC Case Assignment Non User Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Assignment_Notification_Non_UC</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Assignment_Others</fullName>
        <description>SC Case Assignment Others</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Assignment_notification_User</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Assignment_User</fullName>
        <description>SC Case Assignment User</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Assignment_notification_User</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Close_Auto_Reply</fullName>
        <description>SC Case Close Auto Reply</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Auto_Reply_Contact</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Close_Auto_Reply_Final</fullName>
        <description>SC Case Close Auto Reply Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Auto_Reply_Final</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Close_Auto_Reply_user</fullName>
        <description>SC Case Close Auto Reply user</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Auto_Reply_User</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Closed_Contact</fullName>
        <description>SC Case Closed Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Contact</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Closed_Non_User_Contact</fullName>
        <description>SC Case Closed Non User Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Non_User_Contact</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Closed_Notification_Final</fullName>
        <description>SC Case Closed Notification Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_User_Final</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Closed_Notification_Others</fullName>
        <description>SC Case Closed Notification Others</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_User</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Closed_Notification_User</fullName>
        <description>SC Case Closed Notification User</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_User</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Closed_System_Auto_Reply_Non_User_Contact</fullName>
        <description>SC Case Closed System Auto Reply Non User Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Auto_Reply_Non_UC</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Closure_Email_Notification_Contact</fullName>
        <description>SC Case Closure Email Notification Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Contact</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Created_Contact</fullName>
        <description>SC Case Created Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Creation_Contact</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Created_NonUser_Contact</fullName>
        <description>SC Case Created NonUser/Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Creation_Non_User_Contact</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Created_Others</fullName>
        <description>SC Case Created Others</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Creation_User</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Created_User</fullName>
        <description>SC Case Created User</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Creation_User</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Created_User_Final</fullName>
        <description>SC Case Created User Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Creation_User_Final</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Escalated</fullName>
        <description>SC Case Escalated</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Escalation_Contact</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Escalated_Final</fullName>
        <description>SC Case Escalated Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Escalation_Final</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Escalated_Non_User_Contact</fullName>
        <description>SC Case Escalated Non User Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Escalation_Non_UC</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Escalated_Others</fullName>
        <description>SC Case Escalated Others</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Escalation_User</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Escalated_user</fullName>
        <description>SC Case Escalated user</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Escalation_User</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Resolved_Non_User_Contact</fullName>
        <description>SC Case Resolved Non User/Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Resolution_Non_UC</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Resolved_Others</fullName>
        <description>SC Case Resolved Others</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Resolution_User</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Resolved_User</fullName>
        <description>SC Case Resolved User</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Resolution_User</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Resolved_User_Final</fullName>
        <description>SC Case Resolved User Final</description>
        <protected>false</protected>
        <recipients>
            <recipient>Observer</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Resolution_Final</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_SLA_Completed_Email_Notification_User</fullName>
        <description>SC Case SLA Completed Email Notification User</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_User</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_SLA_Completed_Email_Notification_User_Final</fullName>
        <description>SC Case SLA Completed Email Notification User Final</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_User_Final</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_SLA_Completed_Non_User_Contact</fullName>
        <description>SC Case SLA Completed Non User Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Closed_Non_User_Contact</template>
    </alerts>
    <alerts>
        <fullName>SC_Case_Status_Resolved</fullName>
        <description>SC Case Status Resolved</description>
        <protected>false</protected>
        <recipients>
            <field>Additonal_Notification_Email_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additonal_Notification_Email_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>emcbusinessservices@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PreSales_Email_Template/PreSales_Case_Resolution_Contact</template>
    </alerts>
    <alerts>
        <fullName>SupportForceCaseCreatedCMA</fullName>
        <description>Support Force Case Created - CMA</description>
        <protected>false</protected>
        <recipients>
            <recipient>SalesOpsACN</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>SalesOpsCMA</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/SupportForcePortalNewCaseCreated</template>
    </alerts>
    <alerts>
        <fullName>SupportForceCaseCreatedEMEA</fullName>
        <description>Support Force Case Created - EMEA</description>
        <protected>false</protected>
        <recipients>
            <recipient>SalesOpsACN</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>SalesOpsEMEA</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/SupportForcePortalNewCaseCreated</template>
    </alerts>
    <alerts>
        <fullName>SupportForceCaseCreatedLATAM</fullName>
        <description>Support Force Case Created - LATAM</description>
        <protected>false</protected>
        <recipients>
            <recipient>SalesOpsACN</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>SalesOpsLATAM</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/SupportForcePortalNewCaseCreated</template>
    </alerts>
    <alerts>
        <fullName>SupportForceCaseCreatedNA</fullName>
        <description>Support Force Case Created - NA</description>
        <protected>false</protected>
        <recipients>
            <recipient>SalesOpsACN</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>SalesOpsNA</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/SupportForcePortalNewCaseCreated</template>
    </alerts>
    <fieldUpdates>
        <fullName>AP_Case_Status_Field_Update</fullName>
        <field>Status</field>
        <literalValue>Pending Hard Copy Invoice</literalValue>
        <name>AP Case Status Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CaseBUwContactBU</fullName>
        <description>Update Case BU with the Contact&apos;s BU</description>
        <field>BU__c</field>
        <formula>CASE(Contact.BU__c , 
&quot;CMA&quot;, &quot;CMA&quot;, 
&quot;EMEA&quot;, &quot;EMEA&quot;,
&quot;LATAM&quot;, &quot;LATAM&quot;,
&quot;NA&quot;, &quot;NA&quot;,
&quot;None&quot;)</formula>
        <name>Case BU w/ Contact BU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Changed_to_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Presales Case Status Changed to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ChangerecordtypetoDuplicate</fullName>
        <description>Change record type to &quot;Duplicate&quot; when status is set to &quot;Closed-Duplicate.&quot;</description>
        <field>RecordTypeId</field>
        <lookupValue>DuplicateRequirementRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change record type to &quot;Duplicate&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EdServices_Change_to_Accept_Status</fullName>
        <field>Status</field>
        <literalValue>Assigned</literalValue>
        <name>EdServices – Change to Accept Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EdServices_Change_to_New_Status</fullName>
        <field>Status</field>
        <literalValue>Submitted</literalValue>
        <name>EdServices - Change to New Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EdServices_Email</fullName>
        <description>Updates the EDS_Email__c field when the ESC on the Case is changed.</description>
        <field>EDS_Email__c</field>
        <formula>EDS_Contact_Email__r.Name</formula>
        <name>EdServices Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Education_Services_IsEmailToCase_Update</fullName>
        <field>EdServices_IsEmailToCase__c</field>
        <literalValue>1</literalValue>
        <name>Education Services_IsEmailToCase Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Education_Services_New_Email_is_False</fullName>
        <description>When the Status field = “Pending Customer Response”  or “Pending Internal Response” on a case with the Education Services Record Type, the value of the “New Email” checkbox = FALSE</description>
        <field>New_Email__c</field>
        <literalValue>0</literalValue>
        <name>Education Services –  New Email is False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FinalEmail</fullName>
        <description>Update to stop email loops</description>
        <field>Final_Email__c</field>
        <literalValue>1</literalValue>
        <name>FinalEmail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GES_Case_Closed_User</fullName>
        <field>IsMailAfterClose__c</field>
        <literalValue>0</literalValue>
        <name>GES Case Closed User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GES_Change_to_Accept_Status</fullName>
        <field>Status</field>
        <literalValue>Assigned</literalValue>
        <name>GES – Change to Accept Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GES_Change_to_New_Status</fullName>
        <field>Status</field>
        <literalValue>Submitted</literalValue>
        <name>GES - Change to New Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GES_IsEmailToCase_Update</fullName>
        <field>EdServices_IsEmailToCase__c</field>
        <literalValue>1</literalValue>
        <name>GES IsEmailToCase Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GES_New_Email_is_False</fullName>
        <description>When the Status field = “Pending Customer Response” or “Pending Internal Response” on a case with the Education Services Record Type, the value of the “New Email” checkbox = FALSE</description>
        <field>New_Email__c</field>
        <literalValue>0</literalValue>
        <name>GES – New Email is False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Global_Education_Support_Email</fullName>
        <description>Updates the EDS_Email__c field when the ESC on the Case is changed.</description>
        <field>EDS_Email__c</field>
        <formula>EDS_Contact_Email__r.Name</formula>
        <name>Global Education Support Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_the_Escalation_Detail_Readonly</fullName>
        <field>Escalation_Detail__c</field>
        <formula>&quot;Not Applicable&quot;</formula>
        <name>Make the Escalation Detail Readonly</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PVR_Approval_Status_field_update</fullName>
        <field>PVR_Approval_Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>PVR Approval Status field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PVR_Approval_Status_field_update_to</fullName>
        <field>PVR_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>PVR Approval Status field update to Appr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PVR_Approval_Status_field_update_to_Reje</fullName>
        <field>PVR_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>PVR Approval Status field update to Reje</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PartnerAccountCheck</fullName>
        <description>PartnerAccountCheck</description>
        <field>Partner_Account__c</field>
        <literalValue>0</literalValue>
        <name>PartnerAccountCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PartnerCaseCheck</fullName>
        <description>PartnerCaseBecomesTrue</description>
        <field>Partner_Case__c</field>
        <literalValue>1</literalValue>
        <name>PartnerCaseCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PreSales_Change_Status_to_IN_Progress</fullName>
        <description>Changes case status to In Progress when case is assigned by any user.</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>PreSales Change Status to IN Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PresalePopulate_Case_Assigned_Time_Field</fullName>
        <description>This action will populate the Time Case Assigned field with the current time</description>
        <field>Time_Case_Assigned__c</field>
        <formula>Now()</formula>
        <name>PresalePopulate Case Assigned Time Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PresalesPopulate_Last_Status_Change_Time</fullName>
        <description>Populates the current time every the status change into the Last Status Change field.</description>
        <field>Last_Status_Change__c</field>
        <formula>Now()</formula>
        <name>PresalesPopulate Last Status Change Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_Case_Closed_User</fullName>
        <description>Field to update after auto close reply</description>
        <field>IsMailAfterClose__c</field>
        <literalValue>0</literalValue>
        <name>Presales Case Closed User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_Case_Closed_field_Update</fullName>
        <description>Updating field to false after auto closed reply</description>
        <field>IsMailAfterClose__c</field>
        <literalValue>0</literalValue>
        <name>Presales Case Closed field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_Forecast_Stage_Time_Of_Request</fullName>
        <field>Forecast_Stage_at_time_of_request__c</field>
        <formula>Text(Opportunity_Name__r.StageName)</formula>
        <name>Presales Forecast Stage Time Of Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_Populate_Web2Case_Origin</fullName>
        <description>This field update flips the case origin of Web 2 Case cases to &quot;Web&quot;</description>
        <field>Origin</field>
        <literalValue>Web</literalValue>
        <name>Presales Populate Web2Case Origin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_Populate_isResolved_Field</fullName>
        <description>Populates the case.isresolved field</description>
        <field>isResolved__c</field>
        <literalValue>1</literalValue>
        <name>Presales Populate isResolved Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_Populate_wasEscalated</fullName>
        <description>This field update sets the wasEscalated field to TRUE</description>
        <field>wasEscalated__c</field>
        <literalValue>1</literalValue>
        <name>Presales Populate wasEscalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_Status_New_to_Open</fullName>
        <description>This action changes the case status from new to open</description>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Presales Status New to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_Update_ACQC_Email_SLA</fullName>
        <description>This field update changes the SLA for ACQC Email cases to 8 hours</description>
        <field>Case_Resolution_Time__c</field>
        <formula>8</formula>
        <name>Presales Update ACQC Email SLA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_VlabDemo_Status</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Presales_VlabDemo_Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RPQ_Flag</fullName>
        <field>RPQ_Complexity_Status__c</field>
        <literalValue>1</literalValue>
        <name>RPQ Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SC_AutoEscalate</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>SC_AutoEscalate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SC_Case_Closed_User</fullName>
        <field>IsMailAfterClose__c</field>
        <literalValue>0</literalValue>
        <name>SC Case Closed User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SC_Case_Closed_field_Update</fullName>
        <field>IsMailAfterClose__c</field>
        <literalValue>0</literalValue>
        <name>SC Case Closed field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SC_Case_Status_Changed_to_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>SC Case Status Changed to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SC_SLA_Case_Closed_Non_User_Contact</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>SC SLA Case Closed Non User Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SC_Status_Changed_to_Closed_User</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>SC Status Changed to Closed User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SC_Status_Changed_to_Closed_User_Final</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>SC Status Changed to Closed User Final</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Changed_to_Closed_User</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Presales Status Changed to Closed User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateProcessingStatusApprovedBooked</fullName>
        <field>Processing_Status__c</field>
        <literalValue>Approved &amp; Booked</literalValue>
        <name>UpdateProcessingStatusApprovedBooked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateProcessingStatusInProgress</fullName>
        <field>Processing_Status__c</field>
        <literalValue>RSS/SQI In Progress</literalValue>
        <name>UpdateProcessingStatusInProgress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateProcessingStatusOpen</fullName>
        <field>Processing_Status__c</field>
        <literalValue>Open</literalValue>
        <name>UpdateProcessingStatusOpen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateProcessingStatusRejected</fullName>
        <field>Processing_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>UpdateProcessingStatusRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_L1_Latest_Timestamp</fullName>
        <field>L1_Latest_Timestamp__c</field>
        <formula>NOW()</formula>
        <name>Update L1 Latest Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_L2_Latest_Timestamp</fullName>
        <field>L2_Latest_Timestamp__c</field>
        <formula>NOW()</formula>
        <name>Update L2 Latest Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_L3_Latest_Timestamp</fullName>
        <field>L3_Latest_Timestamp__c</field>
        <formula>NOW()</formula>
        <name>Update L3 Latest Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SLA_Completed</fullName>
        <description>Updates &apos;SLA Completed&apos; to BLANK.</description>
        <field>SLA_Completed__c</field>
        <name>Update SLA Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VCE_Case_Initial_Accept</fullName>
        <field>VCE_Case_Initial_Accept__c</field>
        <formula>NOW()</formula>
        <name>VCE Case Initial Accept</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VCE_Case_Relief_Time</fullName>
        <field>VCE_Case_Relief_Time__c</field>
        <formula>NOW()</formula>
        <name>VCE Case Relief Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VCE_Case_Relief_Time_Not_Empty</fullName>
        <field>VCE_Case_Relief_Time__c</field>
        <name>VCE Case Relief Time Not Empty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>VCECaseUpdateToOracle</fullName>
        <apiVersion>8.0</apiVersion>
<<<<<<< HEAD
        <endpointUrl>https://soagateway.noint.emc.com/sst/runtime.asvc/com.actional.intermediary.sfdcVceCaseUpdate</endpointUrl>
=======
        <endpointUrl>https://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.sfdcVceCaseUpdate</endpointUrl>
>>>>>>> master
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>vce.internal.owner@emc.com</integrationUser>
        <name>VCECaseUpdateToOracle</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>AP Case Creation</fullName>
        <actions>
            <name>AP_Case_Creation_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>AP_Case_Status_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Record_Type_Developer_Name__c</field>
            <operation>equals</operation>
            <value>Presales_Accounts_Payable_T_E_Vendor_Master_Treasury</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Theatre__c</field>
            <operation>equals</operation>
            <value>Latin America</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.AP_PCard_TE_VendMast_Treasury_Inquiry__c</field>
            <operation>equals</operation>
            <value>Invoice Submission</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AP Case Status changed to open</fullName>
        <actions>
            <name>AP_Case_Status_changed_to_open_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND($RecordType.DeveloperName =&quot;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&quot;, ISPICKVAL( PRIORVALUE(Status),&apos;Pending Hard Copy Invoice&apos; ), OR(ISPICKVAL(Status, &apos;Open&apos;) ,ISPICKVAL(Status, &apos;In Progress&apos;)), ISPICKVAL(Theatre__c , &apos;Latin America&apos;), ISPICKVAL( AP_PCard_TE_VendMast_Treasury_Inquiry__c , &apos;Invoice Submission&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Closed-Deployed</fullName>
        <actions>
            <name>ClosedDeployedCaseStatusandEmail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Deployed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Status = Closed - Deployed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Closed-Not Deployed%2CDuplicate</fullName>
        <actions>
            <name>ClosedDuplicateNotDeployedCaseStatusandEmail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Not Deployed,Closed - Duplicate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Status = Closed-Duplicate or Closed-Not Deployed and a Contact is linked to the case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Origin Support Force Portal</fullName>
        <actions>
            <name>CaseBUwContactBU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Support Force Portal</value>
        </criteriaItems>
        <description>Rule is true when case origin is Support Force Portal, when record is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case has Related Contact</fullName>
        <actions>
            <name>RelatedContactNewCaseCreated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Support Force Portal</value>
        </criteriaItems>
        <description>Trigger when contact is linked to a case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CaseStatusInProgress</fullName>
        <actions>
            <name>UpdateProcessingStatusInProgress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Record_Type_Developer_Name__c = &apos;Presales_Renewal_Policy_Variation_Request_PVR&apos;, ISCHANGED(Status),ISPICKVAL(Status, &apos;In Progress&apos;),  OR(ISBLANK( TEXT(Processing_Status__c) ), NOT(OR(ISPICKVAL(Processing_Status__c,&apos;RSS/SQI In Progress&apos;),ISPICKVAL(Processing_Status__c,&apos;Pending Renewals&apos;),ISPICKVAL(Processing_Status__c,&apos;MCO - Credit Approval in Progress&apos;),  ISPICKVAL(Processing_Status__c,&apos;MCO - Estimate of Credit in Progress&apos;),  ISPICKVAL(Processing_Status__c,&apos;Contract In Progress (EMEA)&apos;),  ISPICKVAL(Processing_Status__c,&apos;PVR Admin In Progress&apos;),  ISPICKVAL(Processing_Status__c,&apos;Approval In Progress&apos;),  ISPICKVAL(Processing_Status__c,&apos;Approved Pending Booking&apos;),ISPICKVAL(Processing_Status__c,&apos;Approved Booking on Hold&apos;)  ))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CaseStatusOpen</fullName>
        <actions>
            <name>UpdateProcessingStatusOpen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Renewals Policy Variation Request (PVR)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CaseStatusRejected</fullName>
        <actions>
            <name>UpdateProcessingStatusRejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Renewals Policy Variation Request (PVR)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CaseStatusResolved</fullName>
        <actions>
            <name>UpdateProcessingStatusApprovedBooked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Renewals Policy Variation Request (PVR)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Case Auto Reply on Change</fullName>
        <actions>
            <name>SC_Case_Close_Auto_Reply_Final</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SC_Case_Closed_field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Case_Auto_Reply_Last_Date__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Duplicate Status change Record Type</fullName>
        <actions>
            <name>ChangerecordtypetoDuplicate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Duplicate</value>
        </criteriaItems>
        <description>Changes the record type field on the Case to &quot;Duplicate Record Type&quot; when the status &quot;Closed - Duplicate&quot; is selected. (via closing the case).</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EdServices %E2%80%93 Assigned to New</fullName>
        <actions>
            <name>EdServices_Change_to_New_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Change the status of the Case from Assigned to New when the case is placed back in the queue</description>
        <formula>AND( LEFT(OwnerId, 3) = &quot;00G&quot;, Record_Type_Name__c  = &apos;Education Services Record Type&apos;, ISPICKVAL(Status,&apos;Assigned&apos;) = TRUE )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EdServices %E2%80%93 New to Assigned</fullName>
        <actions>
            <name>EdServices_Change_to_Accept_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Change the status of the Case from New to Assigned when the case is accepted from the queue</description>
        <formula>AND( LEFT(OwnerId, 3) != &quot;00G&quot;, Record_Type_Name__c  = &apos;Education Services Record Type&apos;, ISPICKVAL(Status,&apos;Submitted&apos;) = TRUE )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Education Services %E2%80%93 Update New Email Checkbox by Status</fullName>
        <actions>
            <name>Education_Services_New_Email_is_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When the Status field = “Pending Customer Response”  or “In Progress” on a case with the Education Services Record Type, the value of the “New Email” checkbox = FALSE</description>
        <formula>AND(  Record_Type_Name__c = &quot;Education Services Record Type&quot;,  OR(  ISPICKVAL(Status, &quot;In Progress&quot;),  ISPICKVAL(Status, &quot;Customer response required&quot;)  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Education Services - Case Creation Rule</fullName>
        <actions>
            <name>Education_Services_Case_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Education Services Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ParentId</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.EdServices_IsEmailToCase__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Record_Type_Developer_Name__c</field>
            <operation>equals</operation>
            <value>Education_Services_Proven</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>GA Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Sends an email notification to the requestor when a case is created</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Education Services - Case Resolution Rule</fullName>
        <actions>
            <name>Education_Services_Case_Resolved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3) AND 2 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Education Services Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Record_Type_Developer_Name__c</field>
            <operation>equals</operation>
            <value>Education_Services_Proven</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>GA Support</value>
        </criteriaItems>
        <description>Sends an email notification to the requestor when the case is resolved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Education Services - Email Field Update</fullName>
        <actions>
            <name>EdServices_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Education_Services_IsEmailToCase_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the EDS email field with the value from the Education Services Contact</description>
        <formula>AND(  OR(Record_Type_Name__c = &quot;Education Services Record Type&quot;,$RecordType.DeveloperName = &quot;Education_Services_Proven&quot; ),  ( EDS_Contact_Email__r.Name  &lt;&gt;  EDS_Email__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Education Services - New email when Closed</fullName>
        <actions>
            <name>Education_Services_Send_Do_Not_Reply_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FinalEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Presales_Case_Closed_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 5) AND  4</booleanFilter>
        <criteriaItems>
            <field>Case.IsMailAfterClose__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Education Services Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Final_Email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Record_Type_Developer_Name__c</field>
            <operation>equals</operation>
            <value>Education_Services_Proven</value>
        </criteriaItems>
        <description>Checks whether a new email has been received for closed cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Education Services GA Support - Case Creation Rule</fullName>
        <actions>
            <name>Education_Services_GA_Support_Case_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Education Services Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ParentId</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.EdServices_IsEmailToCase__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Record_Type_Developer_Name__c</field>
            <operation>equals</operation>
            <value>Education_Services_Proven</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>GA Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Sends an email notification to the requestor when a case is created</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Education Services GA Support - Case Resolution Rule</fullName>
        <actions>
            <name>Education_Services_GA_Support_Case_Resolved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3) AND 2 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Education Services Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Record_Type_Developer_Name__c</field>
            <operation>equals</operation>
            <value>Education_Services_Proven</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>GA Support</value>
        </criteriaItems>
        <description>Sends an email notification to the requestor when the case is resolved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Global Education Support %E2%80%93 Assigned to New</fullName>
        <actions>
            <name>GES_Change_to_New_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Change the status of the Case from Assigned to New when the case is placed back in the queue</description>
        <formula>AND( LEFT(OwnerId, 3) = &quot;00G&quot;, Record_Type_Developer_Name__c  = &apos;Global_Education_Support&apos;, ISPICKVAL(Status,&apos;Assigned&apos;) = TRUE )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Global Education Support %E2%80%93 New to Assigned</fullName>
        <actions>
            <name>GES_Change_to_Accept_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Change the status of the Case from New to Assigned when the case is accepted from the queue</description>
        <formula>AND( LEFT(OwnerId, 3) != &quot;00G&quot;,  Record_Type_Developer_Name__c = &apos;Global_Education_Support&apos;, ISPICKVAL(Status,&apos;Submitted&apos;) = TRUE )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Global Education Support %E2%80%93 Update New Email Checkbox by Status</fullName>
        <actions>
            <name>GES_New_Email_is_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When the Status field = “Pending Customer Response”  or “In Progress” on a case with the Education Services Record Type, the value of the “New Email” checkbox = FALSE</description>
        <formula>AND(   Record_Type_Developer_Name__c = &quot;Global_Education_Support&quot;,  OR(  ISPICKVAL(Status, &quot;In Progress&quot;),  ISPICKVAL(Status, &quot;Customer response required&quot;)  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Global Education Support - Case Creation Rule</fullName>
        <actions>
            <name>Global_Education_Support_Case_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Record_Type_Developer_Name__c</field>
            <operation>equals</operation>
            <value>Global_Education_Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ParentId</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.EdServices_IsEmailToCase__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Record_Type_Developer_Name__c</field>
            <operation>equals</operation>
            <value>EMCU_Record_Type</value>
        </criteriaItems>
        <description>Sends an email notification to the requestor when a case is created</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Global Education Support - Case Resolution Rule</fullName>
        <actions>
            <name>Global_Education_Support_Case_Resolved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3)AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Record_Type_Developer_Name__c</field>
            <operation>equals</operation>
            <value>Global_Education_Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Record_Type_Developer_Name__c</field>
            <operation>equals</operation>
            <value>EMCU_Record_Type</value>
        </criteriaItems>
        <description>Sends an email notification to the requestor when the case is resolved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Global Education Support - Email Field Update</fullName>
        <actions>
            <name>GES_IsEmailToCase_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Global_Education_Support_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the EDS email field with the value from the Education Services Contact</description>
        <formula>AND(OR(( Record_Type_Developer_Name__c = &quot;Global_Education_Support&quot;),Record_Type_Developer_Name__c = &quot;EMCU_Record_Type&quot;),( EDS_Contact_Email__r.Name  &lt;&gt;  EDS_Email__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Global Education Support - New email when Closed</fullName>
        <actions>
            <name>Global_Education_Support_Send_Do_Not_Reply_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FinalEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GES_Case_Closed_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 5) AND  4</booleanFilter>
        <criteriaItems>
            <field>Case.IsMailAfterClose__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Record_Type_Developer_Name__c</field>
            <operation>equals</operation>
            <value>Global_Education_Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Final_Email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Record_Type_Developer_Name__c</field>
            <operation>equals</operation>
            <value>EMCU_Record_Type</value>
        </criteriaItems>
        <description>Checks whether a new email has been received for closed cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PSC Case Closed System Auto Reply - Deal Reg Cases</fullName>
        <actions>
            <name>PSC_Case_Close_Auto_Reply_Final_Deal_Reg_Cases</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FinalEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Presales_Case_Closed_field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( (Final_Email__c = False),$User.PreSales_Case_Close_AutoReply__c = true, AND( NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), OR( Is_contact__c = True, Is_not_User_Contact__c = True, AND( Is_contact__c = False, Is_not_User_Contact__c = False,PreSales_Case_Close_AutoReply__c = True ) ), IsMailAfterClose__c = True, $RecordType.DeveloperName =&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PSC Case Closed System Auto Reply - PSC Cases</fullName>
        <actions>
            <name>PSC_Case_Close_Auto_Reply_Final</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FinalEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Presales_Case_Closed_field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( (Final_Email__c = False), $User.PreSales_Case_Close_AutoReply__c = true , AND( NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), OR( Is_contact__c = True, Is_not_User_Contact__c = True, AND( Is_contact__c = False, Is_not_User_Contact__c = False,PreSales_Case_Close_AutoReply__c = True ) ), IsMailAfterClose__c = True, OR( $RecordType.DeveloperName =&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName=&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName =&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName =&apos;PSC_Presales_Education_Services&apos;,$RecordType.DeveloperName =&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName =&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName =&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName =&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName =&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName =&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName=&apos;PSC_Presales_Cross_Border_Selling&apos;)	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PSC Case Escalated - Deal Reg Cases</fullName>
        <actions>
            <name>Deal_Reg_Case_Escalated_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( $User.PreSales_Case_Escalated__c = true,NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), Escalation_Request__c = true, $RecordType.DeveloperName ==&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PSC Case Escalated - PSC Cases</fullName>
        <actions>
            <name>PSC_Case_Escalated_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND($User.PreSales_Case_Escalated__c=true,NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), Escalation_Request__c = true, OR($RecordType.DeveloperName ==&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName==&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName==&apos;PSC_Presales_Cross_Border_Selling&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Partner Account Check</fullName>
        <actions>
            <name>PartnerAccountCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check Partner Account when there&apos;s a customer account name.</description>
        <formula>(ISBLANK(Customer_Account_Name__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Partner_Information_Filled_PartnerNoCheck</fullName>
        <actions>
            <name>PartnerCaseCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Partner Case Automatically Checked if the Partner Information is Filled</description>
        <formula>AND(Partner_Case__c=FALSE,NOT(ISBLANK(Partner_Grouping_Name__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales CI Case Owner Changed</fullName>
        <actions>
            <name>Pressales_CI_Case_Owner_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If the case is a child case (i.e. the parent filed is not empty) then don&apos;t send email notficiation</description>
        <formula>AND( $RecordType.DeveloperName ==&apos;Presales_Competitive_Analysis&apos;, ISCHANGED( OwnerId ),  ($User.Id != OwnerId ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PreSales CI Case Pending Internal</fullName>
        <actions>
            <name>PreSales_CI_Case_Pending_Internal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  $RecordType.DeveloperName ==&apos;Presales_Competitive_Analysis&apos;,  ISCHANGED(Status) ,  ISPICKVAL(Status, &apos;Pending Internal&apos;), ParentId == null)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Assignment Contact</fullName>
        <actions>
            <name>PreSales_Case_Assignment</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = True,  ISPICKVAL(Status, &apos;In Progress&apos;),    NOT(OR(   $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Assignment Final</fullName>
        <actions>
            <name>PreSales_Case_Assignment_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), ISPICKVAL( Status , &apos;In Progress&apos;), NOT( OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Group&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Global_Education_Support&apos;,$RecordType.DeveloperName ==&apos;EMCU_Record_Type&apos;, $RecordType.DeveloperName ==&apos;Education_Services_Record_Type&apos;, $RecordType.DeveloperName ==&apos;Education_Services_Proven&apos;, $RecordType.DeveloperName ==&apos;Presales_New_Maintenance_Quote_Request&apos;, $RecordType.DeveloperName ==&apos;Presales_Existing_Maintenance_Quote_Rework&apos;, $RecordType.DeveloperName ==&apos;Presales_Renewal_Policy_Variation_Request_PVR&apos;, $RecordType.DeveloperName ==&apos;Presales_Environmental_Calculator&apos;, $RecordType.DeveloperName ==&apos;Presales_Quoting&apos;, $RecordType.DeveloperName ==&apos;Presales_RPQ&apos;, $RecordType.DeveloperName ==&apos;Presales_Storage_Sizing&apos;, $RecordType.DeveloperName ==&apos;Presales_Technical_Drawings&apos;, $RecordType.DeveloperName ==&apos;Presales_Technical_Q_A&apos;, $RecordType.DeveloperName ==&apos;Presales_Competitive_Analysis&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Education_Services&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Cross_Border_Selling&apos; ) ), OR( Is_contact__c = True, Is_not_User_Contact__c = True, AND( Is_contact__c = False, PreSales_Case_Assigned__c = True ) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Assignment Non User Contact</fullName>
        <actions>
            <name>PreSales_Case_Assignment_Non_User_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_not_User_Contact__c = True,  ISPICKVAL(Status, &apos;In Progress&apos;),   NOT(OR(   $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Assignment Others</fullName>
        <actions>
            <name>PreSales_Case_Assignment_Others</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  ISPICKVAL(Status, &apos;In Progress&apos;),   NOT(OR(   $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Assignment User</fullName>
        <actions>
            <name>PreSales_Case_Assignment_User</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  ISPICKVAL(Status, &apos;In Progress&apos;),   PreSales_Case_Assigned__c = True, NOT(OR(   $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Closed System Auto Reply Contact</fullName>
        <actions>
            <name>PreSales_Case_Close_Auto_Reply</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Presales_Case_Closed_field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = True,  IsMailAfterClose__c = True, NOT(OR(   $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Closed System Auto Reply Final</fullName>
        <actions>
            <name>Pre_Sales_Case_Close_Auto_Reply_Final</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FinalEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Presales_Case_Closed_field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 	(Final_Email__c = False), 	AND(  		NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)),  		OR( 			Is_contact__c = True, Is_not_User_Contact__c = True,  			AND( Is_contact__c = False, Is_not_User_Contact__c = False,PreSales_Case_Close_AutoReply__c = True )  			),  		IsMailAfterClose__c = True,  		Record_Type_Name__c &lt;&gt; &apos;Duplicate Requirement Record Type&apos;,  		Record_Type_Name__c &lt;&gt; &apos;Standard Requirement Record Type&apos;,  		Record_Type_Name__c &lt;&gt; &apos;VCE Record Type&apos;,  		Record_Type_Name__c &lt;&gt; &apos;vLab Demo&apos;,  		Record_Type_Name__c &lt;&gt; &apos;Global Revenue Operations&apos;,  		Record_Type_Name__c &lt;&gt; &apos;Credit &amp; Collections&apos;,  		Record_Type_Name__c &lt;&gt; &apos;TRACK - Customer Master&apos;,  		Record_Type_Name__c &lt;&gt; &apos;Accounts Payable&apos;,  		Record_Type_Name__c &lt;&gt; &apos;TRACK - Resource Hierarchy&apos;,  		Record_Type_Name__c &lt;&gt; &apos;TRACK - Territory Assignment&apos;,  		Record_Type_Name__c &lt;&gt; &apos;Asset Based Selling Support&apos;,  		Record_Type_Name__c &lt;&gt; &apos;Maintenance Contract Operations&apos;,  		Record_Type_Name__c &lt;&gt; &apos;Install Base Group&apos;, 		Record_Type_Name__c &lt;&gt; &apos;Global Education Support Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;EMCU Record Type&apos;, 		Record_Type_Name__c &lt;&gt; &apos;Education Services Record Type&apos;, 		$RecordType.DeveloperName &lt;&gt; &apos;Education_Services_Proven&apos;, 		$RecordType.DeveloperName &lt;&gt; &apos;Presales_New_Maintenance_Quote_Request&apos;, 		$RecordType.DeveloperName &lt;&gt; &apos;Presales_Existing_Maintenance_Quote_Rework&apos;, 		$RecordType.DeveloperName &lt;&gt; &apos;Presales_Renewal_Policy_Variation_Request_PVR&apos;, 		$RecordType.DeveloperName &lt;&gt;&apos;Presales_Competitive_Analysis&apos; ,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Education_Services&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Cross_Border_Selling&apos;		) 	)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Closed System Auto Reply Non User Contact</fullName>
        <actions>
            <name>PreSales_Case_Closed_System_Auto_Reply_Non_User_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_not_User_Contact__c = True,  IsMailAfterClose__c = True,  NOT(OR(  $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Closed System Auto Reply User</fullName>
        <actions>
            <name>Presales_Case_Close_Auto_Reply_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Presales_Case_Closed_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  IsMailAfterClose__c = True, PreSales_Case_Close_AutoReply__c = True, Is_not_User_Contact__c = False, NOT(OR(   $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Created Contact</fullName>
        <actions>
            <name>PreSales_Case_Created_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>OR(  ( AND(  ISNEW(),  (Send_Mail_On_Update__c), Is_Email_To_Case__c=False,  Is_contact__c ,  ISPICKVAL(Status,&apos;Open&apos;),  NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ),  NOT(OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;) )) ),  ( AND(  NOT(ISNEW()),  ISCHANGED(Send_Mail_On_Update__c), ISCHANGED(Is_Email_To_Case__c),  Is_contact__c ,  IsCaseTeam__c=false,  ISPICKVAL(Status,&apos;Open&apos;), NOT($RecordType.DeveloperName!=&apos;Presales_vLab_Demo&apos; ), NOT(OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;) ))  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Created NonUser%2FContact</fullName>
        <actions>
            <name>PreSales_Case_Created_NonUser_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>OR(  ( AND(  ISNEW(),  Is_not_User_Contact__c, ISPICKVAL(Status,&apos;Open&apos;), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), NOT(OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;))) ),  ( AND(  NOT(ISNEW()),  ISCHANGED(Is_not_User_Contact__c),  ISPICKVAL(Status,&apos;Open&apos;), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), NOT(OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;))) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Created Others</fullName>
        <actions>
            <name>PreSales_Case_Created_Others</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>OR(  ( AND(  ISNEW(),  (Send_Mail_On_Update__c), Is_Email_To_Case__c=False,  Is_contact__c = False,  ISPICKVAL(Status,&apos;Open&apos;), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), NOT(OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;)))  ),  ( AND(  NOT(ISNEW()), ISCHANGED(Send_Mail_On_Update__c), ISCHANGED(Is_Email_To_Case__c),  Is_contact__c = False,  ISPICKVAL(Status,&apos;Open&apos;), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), NOT(OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;))) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Created User</fullName>
        <actions>
            <name>PreSales_Case_Created_User</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  Is_Email_To_Case__c=False,PreSales_Case_Created__c = True,  Is_contact__c = False,  NOT(Is_not_User_Contact__c), ISPICKVAL(Status,&apos;Open&apos;), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), NOT(OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;) ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Created User Final</fullName>
        <actions>
            <name>PreSales_Case_Created_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), ISPICKVAL(Status,&apos;Open&apos;), LEFT(OwnerId, 3) = &quot;00G&quot;, NOT( OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Group&apos;, $RecordType.DeveloperName ==&apos;Global_Education_Support&apos;, $RecordType.DeveloperName ==&apos;EMCU_Record_Type&apos;, $RecordType.DeveloperName ==&apos;Education_Services_Record_Type&apos;, $RecordType.DeveloperName ==&apos;Education_Services_Proven&apos;, $RecordType.DeveloperName ==&apos;Presales_New_Maintenance_Quote_Request&apos;, $RecordType.DeveloperName ==&apos;Presales_Existing_Maintenance_Quote_Rework&apos;, $RecordType.DeveloperName ==&apos;Presales_Renewal_Policy_Variation_Request_PVR&apos;, $RecordType.DeveloperName ==&apos;Presales_Environmental_Calculator&apos;, $RecordType.DeveloperName ==&apos;Presales_Quoting&apos;, $RecordType.DeveloperName ==&apos;Presales_RPQ&apos;, $RecordType.DeveloperName ==&apos;Presales_Storage_Sizing&apos;, $RecordType.DeveloperName ==&apos;Presales_Technical_Drawings&apos;, $RecordType.DeveloperName ==&apos;Presales_Technical_Q_A&apos;, $RecordType.DeveloperName ==&apos;Presales_Competitive_Analysis&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Education_Services&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Cross_Border_Selling&apos; ) ), OR( ( AND ( ISNEW(), (Send_Mail_On_Update__c), Is_Email_To_Case__c=False, Is_contact__c , NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; )) ), ( AND( NOT(ISNEW()),ISCHANGED(Send_Mail_On_Update__c), ISCHANGED(Is_Email_To_Case__c), Is_contact__c ,IsCaseTeam__c=false, NOT($RecordType.DeveloperName!=&apos;Presales_vLab_Demo&apos; ) ) ) , AND ( OR ( ( AND( ISNEW(), Is_not_User_Contact__c, NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; )) ), ( AND( NOT(ISNEW()), ISCHANGED(Is_not_User_Contact__c), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ) ) ) ) ), AND( OR( ( AND ( ISNEW(), (Send_Mail_On_Update__c), Is_Email_To_Case__c=False, Is_contact__c = False, NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ) ) ), ( AND( NOT(ISNEW()), ISCHANGED(Send_Mail_On_Update__c), ISCHANGED(Is_Email_To_Case__c), Is_contact__c = False, NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ) ) ) ) ), AND( Is_Email_To_Case__c=False, Is_contact__c = False,PreSales_Case_Created__c = True, ISNEW(), NOT(Is_not_User_Contact__c), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Escalated Contact</fullName>
        <actions>
            <name>PreSales_Case_Escalated</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = True,  IsEscalated = True, NOT(OR(  $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Escalated Final</fullName>
        <actions>
            <name>Pre_Sales_Case_Escalation_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), OR( Is_not_User_Contact__c = True, AND(Is_contact__c = False,PreSales_Case_Escalated__c = True), Is_contact__c = True ), IsEscalated = True, AND( Record_Type_Name__c &lt;&gt; &apos;Duplicate Requirement Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;Standard Requirement Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;VCE Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;vLab Demo&apos;, Record_Type_Name__c &lt;&gt; &apos;Global Revenue Operations&apos;, Record_Type_Name__c &lt;&gt; &apos;Credit &amp; Collections&apos;, Record_Type_Name__c &lt;&gt; &apos;TRACK - Customer Master&apos;, Record_Type_Name__c &lt;&gt; &apos;Accounts Payable&apos;, Record_Type_Name__c &lt;&gt; &apos;TRACK - Resource Hierarchy&apos;, Record_Type_Name__c &lt;&gt; &apos;TRACK - Territory Assignment&apos;, Record_Type_Name__c &lt;&gt; &apos;Asset Based Selling Support&apos;, Record_Type_Name__c &lt;&gt; &apos;Maintenance Contract Operations&apos;, Record_Type_Name__c &lt;&gt; &apos;Install Base Group&apos;,Record_Type_Name__c &lt;&gt; &apos;Education Services Record Type&apos;, $RecordType.DeveloperName &lt;&gt; &apos;Education_Services_Proven&apos;,Record_Type_Name__c &lt;&gt; &apos;Global Education Support Record Type&apos;,Record_Type_Name__c &lt;&gt; &apos;EMCU Record Type&apos;,$RecordType.DeveloperName &lt;&gt; &apos;Presales_New_Maintenance_Quote_Request&apos;,$RecordType.DeveloperName &lt;&gt; &apos;Presales_Existing_Maintenance_Quote_Rework&apos;,$RecordType.DeveloperName &lt;&gt; &apos;Presales_Renewal_Policy_Variation_Request_PVR&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Education_Services&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Cross_Border_Selling&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Escalated Non User Contact</fullName>
        <actions>
            <name>PreSales_Case_Escalated_Non_User_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_not_User_Contact__c = True,  IsEscalated = True, NOT(OR(  $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Escalated Others</fullName>
        <actions>
            <name>PreSales_Case_Escalated_Others</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  IsEscalated = True, NOT(OR(  $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Escalated User</fullName>
        <actions>
            <name>PreSales_Case_Escalated_user</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  IsEscalated = True, PreSales_Case_Escalated__c = True,  NOT(OR(  $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Pending Engineering Contact</fullName>
        <actions>
            <name>PreSales_Case_Pending_Engineering</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(ISPICKVAL(Status, &apos;Pending Engineering&apos;), Is_contact__c = True, NOT(OR(   $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Pending Engineering Final</fullName>
        <actions>
            <name>PreSales_Pending_Engineering_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), OR( Is_contact__c = True, Is_not_User_Contact__c = True, AND(PreSales_Case_Pending_Engg__c = True,Is_contact__c = False)), ISPICKVAL( Status , &apos;Pending Engineering&apos;), Record_Type_Name__c &lt;&gt; &apos;Duplicate Requirement Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;Standard Requirement Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;VCE Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;vLab Demo&apos;, Record_Type_Name__c &lt;&gt; &apos;Global Revenue Operations&apos;, Record_Type_Name__c &lt;&gt; &apos;Credit &amp; Collections&apos;, Record_Type_Name__c &lt;&gt; &apos;TRACK - Customer Master&apos;, Record_Type_Name__c &lt;&gt; &apos;Accounts Payable&apos;, Record_Type_Name__c &lt;&gt; &apos;TRACK - Resource Hierarchy&apos;, Record_Type_Name__c &lt;&gt; &apos;TRACK - Territory Assignment&apos;, Record_Type_Name__c &lt;&gt; &apos;Asset Based Selling Support&apos;, Record_Type_Name__c &lt;&gt; &apos;Maintenance Contract Operations&apos;, Record_Type_Name__c &lt;&gt; &apos;Install Base Group&apos;,Record_Type_Name__c &lt;&gt; &apos;EMCU Record Type&apos;,Record_Type_Name__c &lt;&gt; &apos;Global Education Support Record Type&apos;,$RecordType.DeveloperName &lt;&gt; &apos;Presales_New_Maintenance_Quote_Request&apos;,$RecordType.DeveloperName &lt;&gt; &apos;Presales_Existing_Maintenance_Quote_Rework&apos;,$RecordType.DeveloperName &lt;&gt; &apos;Presales_Renewal_Policy_Variation_Request_PVR&apos; , $RecordType.DeveloperName &lt;&gt;&apos;Presales_Environmental_Calculator&apos;, $RecordType.DeveloperName &lt;&gt;&apos;Presales_Quoting&apos;,$RecordType.DeveloperName &lt;&gt;&apos;Presales_RPQ&apos;,$RecordType.DeveloperName &lt;&gt;&apos;Presales_Storage_Sizing&apos;,$RecordType.DeveloperName &lt;&gt;&apos;Presales_Technical_Drawings&apos;,$RecordType.DeveloperName &lt;&gt;&apos;Presales_Technical_Q_A&apos;, $RecordType.DeveloperName &lt;&gt;&apos;Presales_Competitive_Analysis&apos; ,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Education_Services&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Cross_Border_Selling&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Pending Engineering Non User Contact</fullName>
        <actions>
            <name>PreSales_Case_Pending_Engineering_Non_User_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(ISPICKVAL(Status, &apos;Pending Engineering&apos;), Is_not_User_Contact__c = True, NOT(OR(   $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Pending Engineering Others</fullName>
        <actions>
            <name>PreSales_Case_Pending_Engineering_Others</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(ISPICKVAL(Status, &apos;Pending Engineering&apos;), Is_contact__c = False, NOT(OR(   $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Pending Engineering User</fullName>
        <actions>
            <name>PreSales_Case_Pending_Engineering_User</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(ISPICKVAL(Status, &apos;Pending Engineering&apos;), Is_contact__c = False, PreSales_Case_Pending_Engg__c = True, NOT(OR(   $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Resolved Contact</fullName>
        <actions>
            <name>PreSales_Case_Status_Resolved</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = True,  ISPICKVAL(Status, &apos;Resolved&apos;),   NOT(OR(  $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Resolved Final</fullName>
        <actions>
            <name>PreSales_Case_Resolved_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), OR( Is_contact__c = True, Is_not_User_Contact__c = True, AND( Is_contact__c = False, Is_not_User_Contact__c = False, PreSales_Case_Resolved__c=True ) ), ISPICKVAL( Status , &apos;Resolved&apos;), Record_Type_Name__c &lt;&gt; &apos;Duplicate Requirement Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;Standard Requirement Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;VCE Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;vLab Demo&apos;, Record_Type_Name__c &lt;&gt; &apos;Global Revenue Operations&apos;, Record_Type_Name__c &lt;&gt; &apos;Credit &amp; Collections&apos;, Record_Type_Name__c &lt;&gt; &apos;TRACK - Customer Master&apos;, Record_Type_Name__c &lt;&gt; &apos;Accounts Payable&apos;, Record_Type_Name__c &lt;&gt; &apos;TRACK - Resource Hierarchy&apos;, Record_Type_Name__c &lt;&gt; &apos;TRACK - Territory Assignment&apos;, Record_Type_Name__c &lt;&gt; &apos;Asset Based Selling Support&apos;, Record_Type_Name__c &lt;&gt; &apos;Maintenance Contract Operations&apos;, Record_Type_Name__c &lt;&gt; &apos;Install Base Group&apos;, Record_Type_Name__c &lt;&gt; &apos;Education Services Record Type&apos;,$RecordType.DeveloperName &lt;&gt; &apos;Education_Services_Proven&apos;,Record_Type_Name__c &lt;&gt; &apos;Global Education Support Record Type&apos;,$RecordType.DeveloperName &lt;&gt; &apos;EMCU_Record_Type&apos;,$RecordType.DeveloperName &lt;&gt; &apos;Presales_New_Maintenance_Quote_Request&apos;,$RecordType.DeveloperName &lt;&gt; &apos;Presales_Existing_Maintenance_Quote_Rework&apos;,$RecordType.DeveloperName &lt;&gt; &apos;Presales_Renewal_Policy_Variation_Request_PVR&apos;, $RecordType.DeveloperName &lt;&gt;&apos;Presales_Environmental_Calculator&apos;, $RecordType.DeveloperName &lt;&gt;&apos;Presales_Quoting&apos;,$RecordType.DeveloperName &lt;&gt;&apos;Presales_RPQ&apos;,$RecordType.DeveloperName &lt;&gt;&apos;Presales_Storage_Sizing&apos;,$RecordType.DeveloperName &lt;&gt;&apos;Presales_Technical_Drawings&apos;,$RecordType.DeveloperName &lt;&gt;&apos;Presales_Technical_Q_A&apos;, $RecordType.DeveloperName &lt;&gt;&apos;Presales_Competitive_Analysis&apos; ,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Education_Services&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Cross_Border_Selling&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Resolved Non User%2FContact</fullName>
        <actions>
            <name>PreSales_Case_Resolved_Non_User_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_not_User_Contact__c = True,  ISPICKVAL(Status, &apos;Resolved&apos;),   NOT(OR(  $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Resolved Others</fullName>
        <actions>
            <name>PreSales_Case_Resolved_Others</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  ISPICKVAL(Status, &apos;Resolved&apos;), Is_not_User_Contact__c = False, NOT(OR(   $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Case Resolved User</fullName>
        <actions>
            <name>PreSales_Case_Resolved_User</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  ISPICKVAL(Status, &apos;Resolved&apos;), PreSales_Case_Resolved__c = True, Is_not_User_Contact__c = False, NOT(OR(   $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Escalation field ready only</fullName>
        <actions>
            <name>Make_the_Escalation_Detail_Readonly</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.IsEscalated</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Last Status Change Time</fullName>
        <actions>
            <name>PresalesPopulate_Last_Status_Change_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This workflow populates the Last Status Change field on the case object with the current time with every change of the status field.</description>
        <formula>OR( ISNEW(), ISCHANGED( Status ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Populate isResolved Field</fullName>
        <actions>
            <name>Presales_Populate_isResolved_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.isResolved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Populates the isResolved field when the case is resolved for the first time.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreSales Time Case Assigned</fullName>
        <actions>
            <name>PresalePopulate_Case_Assigned_Time_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.IsFirstUsrAllocation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow populates the case.time_case_assigned__c with the current time when the case is no longer assigned to a queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Presales ACQC SLA Update</fullName>
        <actions>
            <name>Presales_Update_ACQC_Email_SLA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ACQC Email</value>
        </criteriaItems>
        <description>This workflow changes the SLA for ACQC Email cases to 8 hours</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Presales Analyst Group Level 1</fullName>
        <actions>
            <name>Update_L1_Latest_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 
ISPICKVAL( Sizing_Analyst_Group__c , &quot;Level 1&quot;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Presales Analyst Group Level 2</fullName>
        <actions>
            <name>Update_L2_Latest_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 
ISPICKVAL( Sizing_Analyst_Group__c , &quot;Level 2&quot;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Presales Analyst Group Level 3</fullName>
        <actions>
            <name>Update_L3_Latest_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 
ISPICKVAL( Sizing_Analyst_Group__c , &quot;Level 3&quot;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Presales Case Closed Contact</fullName>
        <actions>
            <name>PreSales_Case_Closed_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = True,   OR( ISPICKVAL(Status, &apos;Closed - Not Deployment Related&apos;), ISPICKVAL(Status, &apos;Closed - Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Not Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Duplicate&apos;), ISPICKVAL(Status, &apos;Closed&apos;) ), NOT(OR(  $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Education_Services_Record_Type&apos;, $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Presales Case Closed Final</fullName>
        <actions>
            <name>Pre_Sales_Case_Closed_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), OR( Is_contact__c = True, Is_not_User_Contact__c = True, AND( Is_contact__c = False, Is_not_User_Contact__c = False,PreSales_Case_Closed__c = True ) ), OR( ISPICKVAL( Status , &apos;Closed - Not Deployment Related&apos;), ISPICKVAL(Status, &apos;Closed - Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Not Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Duplicate&apos;), ISPICKVAL(Status, &apos;Closed&apos;) ), AND( Record_Type_Name__c &lt;&gt; &apos;Duplicate Requirement Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;Standard Requirement Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;VCE Record Type&apos;, Record_Type_Name__c &lt;&gt; &apos;vLab Demo&apos;, Record_Type_Name__c &lt;&gt; &apos;Education Services Record Type&apos;, $RecordType.DeveloperName &lt;&gt; &apos;Education_Services_Proven&apos;, Record_Type_Name__c &lt;&gt; &apos;Global Revenue Operations&apos;, Record_Type_Name__c &lt;&gt; &apos;Credit &amp; Collections&apos;, Record_Type_Name__c &lt;&gt; &apos;TRACK - Customer Master&apos;, Record_Type_Name__c &lt;&gt; &apos;TRACK - Resource Hierarchy&apos;, Record_Type_Name__c &lt;&gt; &apos;TRACK - Territory Assignment&apos;, Record_Type_Name__c &lt;&gt; &apos;Asset Based Selling Support&apos;, Record_Type_Name__c &lt;&gt; &apos;Maintenance Contract Operations&apos;, Record_Type_Name__c &lt;&gt; &apos;Accounts Payable&apos; , Record_Type_Name__c &lt;&gt; &apos;Install Base Group&apos;, Record_Type_Name__c &lt;&gt; &apos;Global Education Support Record Type&apos;,$RecordType.DeveloperName &lt;&gt; &apos;EMCU_Record_Type&apos;, $RecordType.DeveloperName &lt;&gt; &apos;Presales_New_Maintenance_Quote_Request&apos;, $RecordType.DeveloperName &lt;&gt; &apos;Presales_Existing_Maintenance_Quote_Rework&apos;, $RecordType.DeveloperName &lt;&gt; &apos;Presales_Renewal_Policy_Variation_Request_PVR&apos;, $RecordType.DeveloperName &lt;&gt;&apos;Presales_Environmental_Calculator&apos;, $RecordType.DeveloperName &lt;&gt;&apos;Presales_Quoting&apos;, $RecordType.DeveloperName &lt;&gt;&apos;Presales_RPQ&apos;, $RecordType.DeveloperName &lt;&gt;&apos;Presales_Storage_Sizing&apos;, $RecordType.DeveloperName &lt;&gt;&apos;Presales_Technical_Drawings&apos;, $RecordType.DeveloperName &lt;&gt;&apos;Presales_Technical_Q_A&apos;, $RecordType.DeveloperName &lt;&gt;&apos;Presales_Competitive_Analysis&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Education_Services&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName &lt;&gt;&apos;PSC_Presales_Cross_Border_Selling&apos; ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Presales Case Closed Non User Contact</fullName>
        <actions>
            <name>Presales_Case_Closed_Non_User_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(ISPICKVAL(Status, &apos;Closed&apos;), Is_not_User_Contact__c = True, NOT(OR(   $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Presales Case Closed Others</fullName>
        <actions>
            <name>PreSales_Case_Closed_Notification_Others</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  OR( ISPICKVAL(Status, &apos;Closed - Not Deployment Related&apos;), ISPICKVAL(Status, &apos;Closed - Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Not Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Duplicate&apos;), ISPICKVAL(Status, &apos;Closed&apos;) ), NOT(OR(  $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Presales Case Closed User</fullName>
        <actions>
            <name>PreSales_Case_Closed_Notification_User</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  Is_not_User_Contact__c = False, PreSales_Case_Closed__c = True, OR( ISPICKVAL(Status, &apos;Closed - Not Deployment Related&apos;), ISPICKVAL(Status, &apos;Closed - Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Not Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Duplicate&apos;), ISPICKVAL(Status, &apos;Closed&apos;) ), NOT(OR(  $RecordType.DeveloperName ==&apos;DuplicateRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;VCE_Record_Type&apos;,  $RecordType.DeveloperName ==&apos;StandardRequirementRecordType&apos;,  $RecordType.DeveloperName ==&apos;Education_Services_Record_Type&apos;, $RecordType.DeveloperName ==&apos;Presales_vLab_Demo&apos;,  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Presales Case Escalated Reporting</fullName>
        <actions>
            <name>Presales_Populate_wasEscalated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.IsEscalated</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow populates the wasEscalated field on the case record when a case is escalated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Presales Email to Case Status</fullName>
        <actions>
            <name>Presales_Status_New_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ACQC Email,CI Email,CQC Email,ESO Email,PSSC Email,Professional Services Email,SDC Email,TSSO Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>This workflow updates the case status of cases created via email to case from New to Open</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Presales Forecast Stage At Time Of Request</fullName>
        <actions>
            <name>Presales_Forecast_Stage_Time_Of_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Presales forecast stage at time of request</description>
        <formula>Opportunity_Name__c != NULL</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Presales Open to In Progress</fullName>
        <actions>
            <name>PreSales_Change_Status_to_IN_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Change the status of the Case from Open to In Progress  when the case is accepted from the queue.</description>
        <formula>AND( LEFT(OwnerId, 3) != &quot;00G&quot;, ISPICKVAL(Status,&apos;Open&apos;) = TRUE, Record_Type_Name__c  != &apos;Education Services Record Type&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Presales RPQ Fields Mandatory</fullName>
        <actions>
            <name>RPQ_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF((BEGINS ( OwnerId , &quot;005&quot;) &amp;&amp; Record_Type_Name__c=&apos;RPQ&apos;) &amp;&amp; (ISPICKVAL(RPQ_Complexity__c,&quot;&quot;)|| ISPICKVAL(RPQ_Status__c,&quot;&quot;)), true, false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Presales Update SLA Completed</fullName>
        <actions>
            <name>Update_SLA_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Resolved</value>
        </criteriaItems>
        <description>This workflow updates SLA Completed to BLANK whenever a case is newly created and Status is not equal to Resolved. Specifically meant to address cloned cases issue.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Presales Webform Origin Update</fullName>
        <actions>
            <name>Presales_Populate_Web2Case_Origin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow updates the case origin on web 2 case cases to Web</description>
        <formula>CreatedBy.Alias = &quot;guest&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Presales_SLA_Completed</fullName>
        <active>true</active>
        <description>This WFR is Time based and just includes the field Update</description>
        <formula>AND( NOT(ISBLANK( SLA_Completed__c )), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), OR( $RecordType.DeveloperName == &apos;Presales_Technical_Drawings&apos;,$RecordType.DeveloperName == &apos;Presales_RPQ&apos;,$RecordType.DeveloperName == &apos;Presales_Quoting&apos;,$RecordType.DeveloperName == &apos;Presales_Technical_Q_A&apos;,$RecordType.DeveloperName == &apos;Presales_Environmental_Calculator&apos;,$RecordType.DeveloperName == &apos;Presales_Storage_Sizing&apos;, $RecordType.DeveloperName ==&apos;Presales_Competitive_Analysis&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Cross_Border_Selling&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Status_Changed_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Presales_SLA_Completed_Contact</fullName>
        <active>true</active>
        <formula>AND( NOT(ISBLANK( SLA_Completed__c )), ( Is_contact__c = True ), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), NOT(OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;,$RecordType.DeveloperName ==&apos;Presales_Install_Base_Group&apos;,$RecordType.DeveloperName == &apos;Presales_New_Maintenance_Quote_Request&apos;,$RecordType.DeveloperName == &apos;Presales_Existing_Maintenance_Quote_Rework&apos;,$RecordType.DeveloperName == &apos;Presales_Renewal_Policy_Variation_Request_PVR&apos;,$RecordType.DeveloperName == &apos;Presales_Technical_Drawings&apos;,$RecordType.DeveloperName == &apos;Presales_RPQ&apos;,$RecordType.DeveloperName == &apos;Presales_Quoting&apos;,$RecordType.DeveloperName == &apos;Presales_Technical_Q_A&apos;,$RecordType.DeveloperName == &apos;Presales_Environmental_Calculator&apos;,$RecordType.DeveloperName == &apos;Presales_Storage_Sizing&apos;,$RecordType.DeveloperName == &apos;Presales_Competitive_Analysis&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Education_Services&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Cross_Border_Selling&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Closure_Email_Notification_Contact</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Case_Status_Changed_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Presales_SLA_Completed_Final</fullName>
        <active>false</active>
        <formula>AND(  NOT(ISPICKVAL(Origin , &apos;Oracle&apos;)),  NOT(ISBLANK( SLA_Completed__c )),  OR(  Is_contact__c = True,  Is_not_User_Contact__c = True,  AND(  Is_contact__c = False,  NOT(Is_not_User_Contact__c)  )),  NOT(Record_Type_Developer_Name__c = &apos;Presales_vLab_Demo&apos;),  NOT(  OR(  Record_Type_Developer_Name__c == &apos;Presales_Global_Revenue_Operations&apos;,  Record_Type_Developer_Name__c == &apos;Presales_Credit_Collections&apos;,  Record_Type_Developer_Name__c == &apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  Record_Type_Developer_Name__c == &apos;Presales_TRACK_Customer_Master&apos;,  Record_Type_Developer_Name__c == &apos;Presales_TRACK_Hierarchy&apos;,  Record_Type_Developer_Name__c == &apos;Presales_TRACK_Territory_Assignment&apos;,  Record_Type_Developer_Name__c == &apos;Presales_Install_Base_Data_Quality&apos;,  Record_Type_Developer_Name__c == &apos;Presales_MaintenanceContractOperations&apos;,  Record_Type_Developer_Name__c == &apos;Presales_Install_Base_Group&apos; )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Presales_Case_Closure_Email_Notification_User_Final</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Case_Status_Changed_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Presales_SLA_Completed_Non_User_Contact</fullName>
        <active>true</active>
        <formula>AND (NOT(ISBLANK( SLA_Completed__c )), ( Is_not_User_Contact__c = True), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), NOT(OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;,$RecordType.DeveloperName ==&apos;Presales_Install_Base_Group&apos;,$RecordType.DeveloperName == &apos;Presales_New_Maintenance_Quote_Request&apos;,$RecordType.DeveloperName == &apos;Presales_Existing_Maintenance_Quote_Rework&apos;,$RecordType.DeveloperName == &apos;Presales_Renewal_Policy_Variation_Request_PVR&apos;,$RecordType.DeveloperName == &apos;Presales_Technical_Drawings&apos;,$RecordType.DeveloperName == &apos;Presales_RPQ&apos;,$RecordType.DeveloperName == &apos;Presales_Quoting&apos;,$RecordType.DeveloperName == &apos;Presales_Technical_Q_A&apos;,$RecordType.DeveloperName == &apos;Presales_Environmental_Calculator&apos;,$RecordType.DeveloperName == &apos;Presales_Storage_Sizing&apos;,$RecordType.DeveloperName == &apos;Presales_Competitive_Analysis&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Education_Services&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Cross_Border_Selling&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Presales_Case_Closed_Non_User_Contact</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Case_Status_Changed_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Presales_SLA_Completed_User</fullName>
        <active>true</active>
        <formula>AND( NOT(ISBLANK( SLA_Completed__c )), Is_contact__c = False, NOT(Is_not_User_Contact__c), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos;), NOT(OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;,$RecordType.DeveloperName ==&apos;Presales_Install_Base_Group&apos;,$RecordType.DeveloperName == &apos;Presales_New_Maintenance_Quote_Request&apos;,$RecordType.DeveloperName == &apos;Presales_Existing_Maintenance_Quote_Rework&apos;,$RecordType.DeveloperName == &apos;Presales_Renewal_Policy_Variation_Request_PVR&apos;,$RecordType.DeveloperName == &apos;Presales_Technical_Drawings&apos;,$RecordType.DeveloperName == &apos;Presales_RPQ&apos;,$RecordType.DeveloperName == &apos;Presales_Quoting&apos;,$RecordType.DeveloperName == &apos;Presales_Technical_Q_A&apos;,$RecordType.DeveloperName == &apos;Presales_Environmental_Calculator&apos;,$RecordType.DeveloperName == &apos;Presales_Storage_Sizing&apos;, $RecordType.DeveloperName ==&apos;Presales_Competitive_Analysis&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Business_Partner_Program&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_System_Support&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Contract_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Deal_Registration_and_Lead_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Education_Services&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Invoicing_and_Credit_Queries&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Licensing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Logistics_Shipping&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Order_Management&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Pricing&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Channel_Systems_Training&apos;,$RecordType.DeveloperName ==&apos;PSC_Presales_Cross_Border_Selling&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Closure_Email_Notification_User</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Status_Changed_to_Closed_User</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Presales_VlabDemo</fullName>
        <actions>
            <name>Presales_VlabDemo_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>vLab Demo</value>
        </criteriaItems>
        <description>On Save the vLab case will automatically be set to a “closed” status.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Assignment Contact</fullName>
        <actions>
            <name>SC_Case_Assignment_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = True,   ISPICKVAL(Status, &apos;In Progress&apos;), PRIORVALUE(Status)=&apos;Open&apos;,   OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,    $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Assignment Final</fullName>
        <actions>
            <name>SC_Case_Assignment_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)),  	NOT(ISPICKVAL(Origin , &apos;Bulk Upload&apos;)),  	OR( Is_contact__c = true, Is_not_User_Contact__c = true, AND( Is_contact__c = False, PreSales_Case_Assigned__c = true ) ), ISPICKVAL(Status, &apos;In Progress&apos;), ISCHANGED(IsFirstUsrAllocation__c),   	OR(Record_Type_Name__c = &apos;TRACK - Customer Master&apos;, Record_Type_Name__c = &apos;TRACK - Resource Hierarchy&apos;, Record_Type_Name__c = &apos;TRACK - Territory Assignment&apos;, Record_Type_Name__c = &apos;Asset Based Selling Support&apos;, Record_Type_Name__c = &apos;Install Base Group&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Assignment Non User Contact</fullName>
        <actions>
            <name>SC_Case_Assignment_Non_User_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_not_User_Contact__c = True,  ISPICKVAL(Status, &apos;In Progress&apos;), ISPICKVAL(PRIORVALUE(Status) ,&quot;Open&quot;),   OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,    $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Assignment Others</fullName>
        <actions>
            <name>SC_Case_Assignment_Others</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False, ISPICKVAL(Status, &apos;In Progress&apos;),  ISPICKVAL(PRIORVALUE(Status) ,&quot;Open&quot;),  OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,    $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Assignment User</fullName>
        <actions>
            <name>SC_Case_Assignment_User</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,   ISPICKVAL(Status, &apos;In Progress&apos;), ISPICKVAL(PRIORVALUE(Status) ,&quot;Open&quot;),   PreSales_Case_Assigned__c = True, OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,    $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Closed Contact</fullName>
        <actions>
            <name>SC_Case_Closed_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = True,  OR( ISPICKVAL(Status, &apos;Closed - Not Deployment Related&apos;), ISPICKVAL(Status, &apos;Closed - Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Not Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Duplicate&apos;), ISPICKVAL(Status, &apos;Closed&apos;) ), OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Closed Final</fullName>
        <actions>
            <name>SC_Case_Closed_Notification_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  	NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)),  	NOT(ISPICKVAL(Origin , &apos;Bulk Upload&apos;)),  	ISPICKVAL(Status, &apos;Closed&apos;),  	OR(Record_Type_Name__c = &apos;TRACK - Customer Master&apos;, Record_Type_Name__c = &apos;TRACK - Resource Hierarchy&apos;, Record_Type_Name__c = &apos;TRACK - Territory Assignment&apos;, Record_Type_Name__c = &apos;Asset Based Selling Support&apos;, Record_Type_Name__c = &apos;Install Base Group&apos;), OR( Is_contact__c = True, Is_not_User_Contact__c = True, AND(Is_contact__c = False,PreSales_Case_Closed__c = True) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Closed Non User Contact</fullName>
        <actions>
            <name>SC_Case_Closed_Non_User_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_not_User_Contact__c = True,  ISPICKVAL(Status, &apos;Closed&apos;), OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Closed Others</fullName>
        <actions>
            <name>SC_Case_Closed_Notification_Others</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  OR( ISPICKVAL(Status, &apos;Closed - Not Deployment Related&apos;), ISPICKVAL(Status, &apos;Closed - Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Not Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Duplicate&apos;), ISPICKVAL(Status, &apos;Closed&apos;) ), OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Closed System Auto Reply Contact</fullName>
        <actions>
            <name>SC_Case_Close_Auto_Reply</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SC_Case_Closed_field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = True,  IsMailAfterClose__c = True, OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Closed System Auto Reply Final</fullName>
        <actions>
            <name>SC_Case_Close_Auto_Reply_Final</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SC_Case_Closed_field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND (OR(ISNULL( Case_Auto_Reply_Last_Date__c ),  Case_Auto_Reply_Last_Date__c &lt;&gt; TODAY()),AND( NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), OR(Is_contact__c = true, Is_not_User_Contact__c = true, AND(Is_contact__c = False, PreSales_Case_Close_AutoReply__c = true) ), IsMailAfterClose__c = true, OR( Record_Type_Name__c = &apos;Global Revenue Operations&apos;, Record_Type_Name__c = &apos;Credit &amp; Collections&apos;, Record_Type_Name__c = &apos;TRACK - Customer Master&apos;, Record_Type_Name__c = &apos;TRACK - Resource Hierarchy&apos;, Record_Type_Name__c = &apos;TRACK - Territory Assignment&apos;, Record_Type_Name__c = &apos;Asset Based Selling Support&apos;, Record_Type_Name__c = &apos;Maintenance Contract Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, Record_Type_Name__c = &apos;Install Base Group&apos;,$RecordType.DeveloperName == &apos;Presales_New_Maintenance_Quote_Request&apos;,$RecordType.DeveloperName == &apos;Presales_Existing_Maintenance_Quote_Rework&apos;,$RecordType.DeveloperName == &apos;Presales_Renewal_Policy_Variation_Request_PVR&apos; )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Closed System Auto Reply Non User Contact</fullName>
        <actions>
            <name>SC_Case_Closed_System_Auto_Reply_Non_User_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  IsMailAfterClose__c = True, Is_not_User_Contact__c = True, OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Closed System Auto Reply User</fullName>
        <actions>
            <name>SC_Case_Close_Auto_Reply_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SC_Case_Closed_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  IsMailAfterClose__c = True, PreSales_Case_Close_AutoReply__c = True, OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Closed User</fullName>
        <actions>
            <name>SC_Case_Closed_Notification_User</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  PreSales_Case_Closed__c = True, OR( ISPICKVAL(Status, &apos;Closed - Not Deployment Related&apos;), ISPICKVAL(Status, &apos;Closed - Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Not Deployed&apos;), ISPICKVAL(Status, &apos;Closed - Duplicate&apos;), ISPICKVAL(Status, &apos;Closed&apos;) ), OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Created Contact</fullName>
        <actions>
            <name>SC_Case_Created_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>OR(  ( AND(  ISNEW(),  (Send_Mail_On_Update__c), Is_Email_To_Case__c=False,  Is_contact__c ,  ISPICKVAL(Status,&apos;Open&apos;),  NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;)) ),  ( AND(  NOT(ISNEW()),  ISCHANGED(Send_Mail_On_Update__c), ISCHANGED(Is_Email_To_Case__c),  Is_contact__c ,  IsCaseTeam__c=false,  ISPICKVAL(Status,&apos;Open&apos;), NOT($RecordType.DeveloperName!=&apos;Presales_vLab_Demo&apos; ), OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;)))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Created NonUser%2FContact</fullName>
        <actions>
            <name>SC_Case_Created_NonUser_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>OR(  ( AND(  ISNEW(),  Is_not_User_Contact__c, ISPICKVAL(Status,&apos;Open&apos;), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;)) ),  ( AND(  NOT(ISNEW()),  ISCHANGED(Is_not_User_Contact__c),  ISPICKVAL(Status,&apos;Open&apos;), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;)) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Created Others</fullName>
        <actions>
            <name>SC_Case_Created_Others</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>OR(  ( AND(  ISNEW(),  (Send_Mail_On_Update__c), Is_Email_To_Case__c=False,  Is_contact__c = False,  ISPICKVAL(Status,&apos;Open&apos;), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;)) ),  ( AND(  NOT(ISNEW()), ISCHANGED(Send_Mail_On_Update__c), ISCHANGED(Is_Email_To_Case__c),  Is_contact__c = False,  ISPICKVAL(Status,&apos;Open&apos;), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;))) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Created User</fullName>
        <actions>
            <name>SC_Case_Created_User</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  Is_Email_To_Case__c=False,PreSales_Case_Created__c = True,  Is_contact__c = False,  NOT(Is_not_User_Contact__c), ISPICKVAL(Status,&apos;Open&apos;), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), OR( $RecordType.DeveloperName =&apos;Presales_Global_Revenue_Operations&apos;, $RecordType.DeveloperName =&apos;Presales_Credit_Collections&apos;, $RecordType.DeveloperName=&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, $RecordType.DeveloperName =&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName =&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName =&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName =&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName =&apos;Presales_MaintenanceContractOperations&apos;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Created User Final</fullName>
        <actions>
            <name>SC_Case_Created_User_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)),  	NOT(ISPICKVAL(Origin , &apos;Bulk Upload&apos;)),  	OR( $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Group&apos;),   	ISPICKVAL(Status,&apos;Open&apos;),   	LEFT(OwnerId, 3) = &quot;00G&quot;,   	OR(  AND(  OR( (  AND( ISNEW(), (Send_Mail_On_Update__c), Is_Email_To_Case__c=False, Is_contact__c , NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos;))  ),  ( AND( NOT(ISNEW()), ISCHANGED(Send_Mail_On_Update__c), ISCHANGED(Is_Email_To_Case__c), Is_contact__c , IsCaseTeam__c=false, NOT($RecordType.DeveloperName!=&apos;Presales_vLab_Demo&apos; ) )  )  )  ),  AND (  OR( (  AND( ISNEW(), Is_not_User_Contact__c, NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ))  ),  ( AND( NOT(ISNEW()), ISCHANGED(Is_not_User_Contact__c), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ) )  )  )  ),  AND(  ISNEW(), Is_Email_To_Case__c=False,PreSales_Case_Created__c = True, Is_contact__c = False, NOT(Is_not_User_Contact__c), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; )) )  	)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Escalated Contact</fullName>
        <actions>
            <name>SC_Case_Escalated</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = True,  IsEscalated = True, OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Escalated Non User Contact</fullName>
        <actions>
            <name>SC_Case_Escalated_Non_User_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_not_User_Contact__c = True,  IsEscalated = True, OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Escalated Others</fullName>
        <actions>
            <name>SC_Case_Escalated_Others</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  IsEscalated = True, OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Escalated User</fullName>
        <actions>
            <name>SC_Case_Escalated_user</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  IsEscalated = True, PreSales_Case_Escalated__c = True, OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Escalated User Final</fullName>
        <actions>
            <name>SC_Case_Escalated_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), OR( Is_contact__c = true, Is_not_User_Contact__c = true, AND(Is_contact__c = False, PreSales_Case_Escalated__c = true) ), IsEscalated = true, OR( Record_Type_Name__c = &apos;Global Revenue Operations&apos;, Record_Type_Name__c = &apos;Credit &amp; Collections&apos;, Record_Type_Name__c = &apos;TRACK - Customer Master&apos;, Record_Type_Name__c = &apos;TRACK - Resource Hierarchy&apos;, Record_Type_Name__c = &apos;TRACK - Territory Assignment&apos;, Record_Type_Name__c = &apos;Asset Based Selling Support&apos;, Record_Type_Name__c = &apos;Maintenance Contract Operations&apos;, $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;, Record_Type_Name__c = &apos;Install Base Group&apos;,$RecordType.DeveloperName == &apos;Presales_New_Maintenance_Quote_Request&apos;,$RecordType.DeveloperName == &apos;Presales_Existing_Maintenance_Quote_Rework&apos;,$RecordType.DeveloperName == &apos;Presales_Renewal_Policy_Variation_Request_PVR&apos; ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Resolved Contact</fullName>
        <actions>
            <name>SC_Case_Status_Resolved</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = True,  ISPICKVAL(Status, &apos;Resolved&apos;), OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Resolved Non User%2FContact</fullName>
        <actions>
            <name>SC_Case_Resolved_Non_User_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_not_User_Contact__c = True,  ISPICKVAL(Status, &apos;Resolved&apos;), OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Resolved Others</fullName>
        <actions>
            <name>SC_Case_Resolved_Others</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  Is_not_User_Contact__c = False, ISPICKVAL(Status, &apos;Resolved&apos;), OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Resolved User</fullName>
        <actions>
            <name>SC_Case_Resolved_User</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Is_contact__c = False,  PreSales_Case_Resolved__c = True,  ISPICKVAL(Status, &apos;Resolved&apos;), OR(  $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;,   $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;,  $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;,  $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC Case Resolved User Final</fullName>
        <actions>
            <name>SC_Case_Resolved_User_Final</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), ISPICKVAL(Status, &apos;Resolved&apos;), OR( Is_contact__c = true, Is_not_User_Contact__c = true, AND(Is_contact__c = False,PreSales_Case_Resolved__c = True)),  	OR( Record_Type_Name__c = &apos;TRACK - Customer Master&apos;, Record_Type_Name__c = &apos;TRACK - Resource Hierarchy&apos;, Record_Type_Name__c = &apos;TRACK - Territory Assignment&apos;, Record_Type_Name__c = &apos;Asset Based Selling Support&apos;, Record_Type_Name__c = &apos;Install Base Group&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SC_SLA_Completed</fullName>
        <active>true</active>
        <description>This WFR id Time based and just includes the field Update</description>
        <formula>AND( NOT(ISBLANK( SLA_Completed__c )), NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos;), OR( $RecordType.DeveloperName == &apos;Presales_New_Maintenance_Quote_Request&apos;, $RecordType.DeveloperName == &apos;Presales_Existing_Maintenance_Quote_Rework&apos;, $RecordType.DeveloperName == &apos;Presales_Renewal_Policy_Variation_Request_PVR&apos;, $RecordType.DeveloperName ==&apos;Presales_MaintenanceContractOperations&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SC_Case_Status_Changed_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SC_SLA_Completed_Contact</fullName>
        <active>true</active>
        <formula>AND( NOT(ISBLANK( SLA_Completed__c )), NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)),( Is_contact__c = True ), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), OR( $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Group&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SC_Case_Closure_Email_Notification_Contact</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SC_Case_Status_Changed_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SC_SLA_Completed_Contact_1</fullName>
        <active>true</active>
        <formula>AND( NOT(ISBLANK( SLA_Completed__c )), NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)),( Is_contact__c = True ),  OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;   ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SC_Case_Status_Changed_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SC_SLA_Completed_Non_User_Contact</fullName>
        <active>true</active>
        <formula>AND( NOT(ISBLANK( SLA_Completed__c )), NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)),( Is_not_User_Contact__c = True), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos; ), OR( $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Group&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SC_Case_SLA_Completed_Non_User_Contact</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SC_SLA_Case_Closed_Non_User_Contact</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SC_SLA_Completed_Non_User_Contact_1</fullName>
        <active>true</active>
        <formula>AND( NOT(ISBLANK( SLA_Completed__c )), NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)),( Is_not_User_Contact__c = True),  OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;   ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SC_SLA_Case_Closed_Non_User_Contact</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SC_SLA_Completed_User</fullName>
        <active>true</active>
        <formula>AND( NOT(ISBLANK( SLA_Completed__c )), NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)),Is_contact__c = False, NOT(Is_not_User_Contact__c), NOT($RecordType.DeveloperName=&apos;Presales_vLab_Demo&apos;), OR( $RecordType.DeveloperName ==&apos;Presales_TRACK_Customer_Master&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Hierarchy&apos;, $RecordType.DeveloperName ==&apos;Presales_TRACK_Territory_Assignment&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Data_Quality&apos;, $RecordType.DeveloperName ==&apos;Presales_Install_Base_Group&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SC_Case_SLA_Completed_Email_Notification_User</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SC_Status_Changed_to_Closed_User</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SC_SLA_Completed_User_1</fullName>
        <active>true</active>
        <formula>AND( NOT(ISBLANK( SLA_Completed__c )), NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)),Is_contact__c = False,  NOT(Is_not_User_Contact__c),  OR( $RecordType.DeveloperName ==&apos;Presales_Global_Revenue_Operations&apos;,  $RecordType.DeveloperName ==&apos;Presales_Credit_Collections&apos;,  $RecordType.DeveloperName ==&apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;   ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SC_Status_Changed_to_Closed_User</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SC_SLA_Completed_User_Final</fullName>
        <active>false</active>
        <formula>AND(   NOT(ISPICKVAL(Origin , &apos;Oracle Integration&apos;)), NOT(ISBLANK( SLA_Completed__c )), OR(  Is_contact__c = true,  Is_not_User_Contact__c = true,  AND(Is_contact__c = False,  NOT(Is_not_User_Contact__c))  ), Record_Type_Developer_Name__c &lt;&gt; &apos;Presales_vLab_Demo&apos;,  OR(  Record_Type_Developer_Name__c = &apos;Presales_Global_Revenue_Operations&apos;,  Record_Type_Developer_Name__c = &apos;Presales_Credit_Collections&apos;,  Record_Type_Developer_Name__c = &apos;Presales_Accounts_Payable_T_E_Vendor_Master_Treasury&apos;,  Record_Type_Developer_Name__c = &apos;Presales_TRACK_Customer_Master&apos;,  Record_Type_Developer_Name__c = &apos;TRACK - Territory Assignment&apos;,  Record_Type_Developer_Name__c = &apos;Presales_TRACK_Hierarchy&apos;,  Record_Type_Developer_Name__c = &apos;Presales_TRACK_Territory_Assignment&apos;,  Record_Type_Developer_Name__c = &apos;Presales_Install_Base_Data_Quality&apos;,  Record_Type_Developer_Name__c = &apos;Presales_MaintenanceContractOperations&apos;, Record_Type_Developer_Name__c = &apos;Presales_Install_Base_Group&apos;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SC_Case_SLA_Completed_Email_Notification_User_Final</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SC_Status_Changed_to_Closed_User_Final</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.SLA_Completed__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Support Force Case Created - CMA</fullName>
        <actions>
            <name>SupportForceCaseCreatedCMA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Support Force Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.BU__c</field>
            <operation>equals</operation>
            <value>CMA</value>
        </criteriaItems>
        <description>Triggered when a case is created via the support force portal</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Force Created - EMEA</fullName>
        <actions>
            <name>SupportForceCaseCreatedEMEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Support Force Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.BU__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <description>Triggered when a case is created via the support force portal</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Force Created - LATAM</fullName>
        <actions>
            <name>SupportForceCaseCreatedLATAM</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Support Force Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.BU__c</field>
            <operation>equals</operation>
            <value>LATAM</value>
        </criteriaItems>
        <description>Triggered when a case is created via the support force portal</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Support Force Created - NA</fullName>
        <actions>
            <name>SupportForceCaseCreatedNA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Support Force Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.BU__c</field>
            <operation>equals</operation>
            <value>NA</value>
        </criteriaItems>
        <description>Triggered when a case is created via the support force portal</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>VCECaseReliefTime</fullName>
        <actions>
            <name>VCE_Case_Relief_Time_Not_Empty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>It will update Case Relief Time to NULL when it is not empty and Status is neither Assigned Relief nor Closed.</description>
        <formula>ISPICKVAL(Origin,&apos;EMC&apos;) &amp;&amp;  NOT(ISBLANK(VCE_Case_Relief_Time__c)) &amp;&amp;  NOT(  ISPICKVAL( Status , &quot;Assigned - Relief&quot;) ||  ISPICKVAL( Status , &quot;Closed&quot;) ||  ISPICKVAL( Status , &quot;Closed - CHAT&quot;) ||  ISPICKVAL( Status , &quot;Closed - Defect Logged&quot;) ||  ISPICKVAL( Status , &quot;Closed - Initial Call&quot;) ||  ISPICKVAL( Status , &quot;Closed - No Cust. Validation&quot;) ||  ISPICKVAL( Status , &quot;Open - Relief Wrk Around Prv&apos;d&quot;) ||  ISPICKVAL( Status , &quot;Open - Relief Await Cust Veri.&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VCECaseUpdateToOracle</fullName>
        <actions>
            <name>VCECaseUpdateToOracle</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>This workflow will trigger an outbound message to the Oracle system if the transfer field on the case object is 1 and the case was not modified by EMC.</description>
        <formula>AND(            ! ISNEW(),            VCE_Transfer__c ==true,            NOT(ISCHANGED(VCE_Transfer__c)),            NOT(ISPICKVAL(VCE_Last_Modifying_Partner__c, &apos;EMC&apos;))         )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VCECreateCaseInitialAccept</fullName>
        <actions>
            <name>VCE_Case_Initial_Accept</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>It will populate the case initial accept field to NOW() when the last name of the prime engineer is populated.</description>
        <formula>ISPICKVAL(Origin,&apos;EMC&apos;) &amp;&amp; NOT(ISBLANK(VCE_Prime_Engineer_Last_Name__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VCECreateCaseReliefTime</fullName>
        <actions>
            <name>VCE_Case_Relief_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>It will update Case Relief Time to NOW() when it is empty and Status is Assigned Relief or Closed.</description>
        <formula>ISPICKVAL(Origin,&apos;EMC&apos;) &amp;&amp;  ISNULL(VCE_Case_Relief_Time__c) &amp;&amp;  (  ISPICKVAL( Status , &quot;Assigned - Relief&quot;) ||  ISPICKVAL( Status , &quot;Closed&quot;) ||  ISPICKVAL( Status , &quot;Closed - CHAT&quot;) ||  ISPICKVAL( Status , &quot;Closed - Defect Logged&quot;) ||  ISPICKVAL( Status , &quot;Closed - Initial Call&quot;) ||  ISPICKVAL( Status , &quot;Closed - No Cust. Validation&quot;) ||  ISPICKVAL( Status , &quot;Open - Relief Wrk Around Prv&apos;d&quot;) ||  ISPICKVAL( Status , &quot;Open - Relief Await Cust Veri.&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
