<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>After_2_Days_passed</fullName>
        <description>After 2 Days passed</description>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Last_EMC_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>emcsfdcalert@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PRM_Templates/PRM_LeadPullBack_2Days</template>
    </alerts>
    <alerts>
        <fullName>After_4_Days_Accepted</fullName>
        <description>After 4 Days Accepted</description>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Last_EMC_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>emcsfdcalert@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PRM_Templates/PRM_LeadDisposition_4Days</template>
    </alerts>
    <alerts>
        <fullName>After_4_days_passed</fullName>
        <description>After 4 days passed</description>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Last_EMC_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>emcsfdcalert@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PRM_Templates/PRM_LeadPullBack_4Days</template>
    </alerts>
    <alerts>
        <fullName>After_9_Days_Accepteds</fullName>
        <description>After 9 Days Accepteds</description>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Last_EMC_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>emcsfdcalert@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PRM_Templates/PRM_LeadDisposition_9Days</template>
    </alerts>
    <alerts>
        <fullName>Approval_of_a_Deal_Registration_AMER</fullName>
        <description>Approval of a Deal Registration AMER</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Account_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>oppregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR02</template>
    </alerts>
    <alerts>
        <fullName>Approval_of_a_Deal_Registration_APJ</fullName>
        <description>Approval of a Deal Registration APJ</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Account_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>pscapjdealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR02</template>
    </alerts>
    <alerts>
        <fullName>Approval_of_a_Deal_Registration_EMEA</fullName>
        <description>Approval of a Deal Registration EMEA</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Account_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>pscemeadealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR02</template>
    </alerts>
    <alerts>
        <fullName>CAM_Owner_Email</fullName>
        <description>CAM Owner Email</description>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/LeadsNewassignmentnotificationCAM</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR00_EMEA</fullName>
        <ccEmails>pscemeadealregistration@emc.com</ccEmails>
        <description>Email is sent upon submission of a Deal Registration for approval EMEA</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Distribution_VAR_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Distributor_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>pscemeadealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR00</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR01_APJ</fullName>
        <description>Email is sent upon Approval of a Deal Registration to the Linked Opportunity Owner for APJ</description>
        <protected>false</protected>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>pscapjdealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR01</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR01_Americas</fullName>
        <description>Email is sent upon Approval of a Deal Registration to the Linked Opportunity Owner for Americas</description>
        <protected>false</protected>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>oppregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR01</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR01_EMEA</fullName>
        <description>Email is sent upon Approval of a Deal Registration to the Linked Opportunity Owner for EMEA</description>
        <protected>false</protected>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>pscemeadealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR01</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR04_APJ</fullName>
        <description>Deal registration is available for extension &amp; will expire in 45 days APJ</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>pscapjdealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR04</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR04_Americas</fullName>
        <description>Deal registration is available for extension &amp; will expire in 45 days Americas</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>oppregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR04</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR04_EMEA</fullName>
        <description>Deal registration is available for extension &amp; will expire in 45 days EMEA</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>pscemeadealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR04</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR06_APJ</fullName>
        <description>Deal registration has expired For APJ</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Account_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>pscapjdealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR06</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR06_Americas</fullName>
        <description>Deal registration has expired For Americas</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Account_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>oppregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR06</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR06_EMEA</fullName>
        <description>Deal registration has expired For EMEA</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Account_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>pscemeadealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR06</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR07_APJ</fullName>
        <description>Email is sent upon approved extension request with updated expiration date for APJ.</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Account_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>pscapjdealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR07</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR07_Americas</fullName>
        <description>Email is sent upon approved extension request with updated expiration date for Americas</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Account_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>oppregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR07</template>
    </alerts>
    <alerts>
        <fullName>DR_Notification_DR07_EMEA</fullName>
        <description>Email is sent upon approved extension request with updated expiration date for EMEA</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Account_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>pscemeadealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR07</template>
    </alerts>
    <alerts>
        <fullName>Deal_registration_is_available_for_extension_will_expire_in_10_days_Americas</fullName>
        <description>Deal registration is available for extension &amp; will expire in 10 days Americas</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Linked_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>oppregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR04</template>
    </alerts>
    <alerts>
        <fullName>Dealreg_Field_Approval_Email_Notification_for_APJ</fullName>
        <ccEmails>PSCAPJDealRegistration@emc.com</ccEmails>
        <description>Dealreg Field Approval Email Notification for APJ</description>
        <protected>false</protected>
        <recipients>
            <field>Additional_Field_Approver_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Field_Approver_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Field_Approver_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Field_Approver__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>pscapjdealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR10</template>
    </alerts>
    <alerts>
        <fullName>Dealreg_Field_Approval_Email_Notification_for_Americas</fullName>
        <ccEmails>oppregistration@emc.com</ccEmails>
        <description>Dealreg Field Approval Email Notification for Americas</description>
        <protected>false</protected>
        <recipients>
            <field>Additional_Field_Approver_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Field_Approver_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Field_Approver_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Field_Approver__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>oppregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR10</template>
    </alerts>
    <alerts>
        <fullName>Dealreg_Field_Approval_Email_Notification_for_EMEA</fullName>
        <ccEmails>pscemeadealregistration@emc.com</ccEmails>
        <description>Dealreg Field Approval Email Notification for EMEA</description>
        <protected>false</protected>
        <recipients>
            <field>Additional_Field_Approver_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Field_Approver_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Field_Approver_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Field_Approver__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>pscemeadealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR10</template>
    </alerts>
    <alerts>
        <fullName>Email_is_sent_upon_submission_of_a_Deal_Registration_for_approval_Americas</fullName>
        <description>Email is sent upon submission of a Deal Registration for approval Americas</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Distribution_VAR_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Distributor_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>oppregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR00</template>
    </alerts>
    <alerts>
        <fullName>Email_is_sent_upon_submission_of_a_Deal_Registration_for_approval_Apj</fullName>
        <ccEmails>PSCAPJDealRegistration@emc.com</ccEmails>
        <description>Email is sent upon submission of a Deal Registration for approval Apj</description>
        <protected>false</protected>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Deal_Submitter_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Distribution_VAR_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Distributor_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>pscapjdealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR00</template>
    </alerts>
    <alerts>
        <fullName>INTGErrorMailNotification</fullName>
        <description>INTG Error Mail Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Integration_Error_Message</template>
    </alerts>
    <alerts>
        <fullName>Lead_Assignment_Alert_for_APJ_Contact_Us_Queue</fullName>
        <description>Lead Assignment Alert for APJ Contact Us Queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>COREAPJJAPANContactUsLeadsQueue</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Inside_Sales_APJ_Japan1/Japan_Contact_Us_Leads_Template</template>
    </alerts>
    <alerts>
        <fullName>Lead_Owner_Email</fullName>
        <description>Lead Owner Email</description>
        <protected>false</protected>
        <recipients>
            <field>Additional_Recipient_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Recipient_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/Lead_Owner_Change_Notification_to_New_Lead_Owner_2</template>
    </alerts>
    <alerts>
        <fullName>Lead_Rejection_Notification</fullName>
        <description>Lead Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/Lead_Rejection_Notification_to_CAM</template>
    </alerts>
    <alerts>
        <fullName>Rejection_of_a_Deal_Registration_APJ</fullName>
        <description>Rejection of a Deal Registration APJ</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Account_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>pscapjdealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR03</template>
    </alerts>
    <alerts>
        <fullName>Rejection_of_a_Deal_Registration_Americas</fullName>
        <description>Rejection of a Deal Registration Americas</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Account_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>oppregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR03</template>
    </alerts>
    <alerts>
        <fullName>Rejection_of_a_Deal_Registration_Emea</fullName>
        <description>Rejection of a Deal Registration Emea</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>DealReg_Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Account_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>DealReg_Channel_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>pscemeadealregistration@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Deal_reg_Templates/DR_Notification_DR03</template>
    </alerts>
    <alerts>
        <fullName>Tech_Refresh_CAM_Owner_Email</fullName>
        <description>Tech Refresh - CAM Owner Email</description>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/TechRefreshLeadsNewassignmentnotificationCAM</template>
    </alerts>
    <alerts>
        <fullName>Tech_Refresh_Lead_Owner_Email</fullName>
        <description>Tech Refresh - Lead Owner Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/TechRefreshLeadsNewassignmentnotificationLeadOwner</template>
    </alerts>
    <alerts>
        <fullName>This_notification_is_sent_to_EMCSocialSales_emc_com_email_upon_creation_of_a_lea</fullName>
        <description>This notification is sent using the lead header field eStore Queue Email email upon creation of a lead with Source-eBusiness and Source/ Program Detail = Contact Me</description>
        <protected>false</protected>
        <recipients>
            <field>eStore_Queue_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>storeinformation@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EBusiness_Templates/EMC_Social_Sale_Template</template>
    </alerts>
    <alerts>
        <fullName>This_notification_is_sent_to_EMCSocialSales_emc_com_upon_creation_of_a_lead_with</fullName>
        <description>This notification is sent using the lead header field eStore Queue Email email upon creation of a lead with Source-eBusiness and Source/ Program Detail = RFQ</description>
        <protected>false</protected>
        <recipients>
            <field>eStore_Queue_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>storeinformation@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EBusiness_Templates/EMC_Social_Sale_Template</template>
    </alerts>
    <alerts>
        <fullName>This_notification_is_sent_to_the_customer_email_upon_creation_of_a_lead_with_Sou</fullName>
        <description>This notification is sent to the customer email upon creation of a lead with Source-eBusiness and Source/ Program Detail = Contact Me</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>storeinformation@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PRM_Templates/eBusiness_Contact_Me_Lead_Creation</template>
    </alerts>
    <alerts>
        <fullName>This_notification_is_sent_using_the_lead_header_field_eStore_Queue_Email_email_u</fullName>
        <description>This notification is sent using the lead header field eStore Queue Email email upon creation of a lead with Source = eBusiness and Source/ Program Detail = RFQPunch</description>
        <protected>false</protected>
        <recipients>
            <field>eStore_Queue_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>storeinformation@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PRM_Templates/eBusiness_Notification_RFQPunch_Lead_Creation</template>
    </alerts>
    <alerts>
        <fullName>This_notification_is_sent_when_an_eBusiness_Lead_with_Source_eStore_and_Source_P</fullName>
        <description>This notification is sent when an eBusiness Lead with Source = eStore and Source/ Program Detail = RFQ is assigned to a Partner User.</description>
        <protected>false</protected>
        <recipients>
            <field>eBus_Lead_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>eBusiness_Additional_Notification_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>storeinformation@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PRM_Templates/eBusiness_Notification_RFQ_Lead_Owner_Updated_to_Partner_User</template>
    </alerts>
    <alerts>
        <fullName>eBusiness_RFQ_Lead_Creation</fullName>
        <description>This notification is sent to the customer email upon creation of a lead with Source-eBusiness and Source/ Program Detail = RFQ</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>storeinformation@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PRM_Templates/eBusiness_RFQ_Lead_Creation</template>
    </alerts>
    <alerts>
        <fullName>eBusiness_RFQ_Punch_Lead_Owner_Created</fullName>
        <description>This notification is sent when an eBusiness Lead with Source = eBusiness and Source/ Program Detail = RFQPunch is assigned to a Partner User.</description>
        <protected>false</protected>
        <recipients>
            <field>eBus_Lead_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>eBusiness_Additional_Notification_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>storepartner@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PRM_Templates/eBusiness_Notification_RFQ_Punch_Lead_Owner_Created</template>
    </alerts>
    <fieldUpdates>
        <fullName>AMER_Tech_Refresh_Ownership</fullName>
        <field>OwnerId</field>
        <lookupValue>AMERTechRefreshRenewalsQueue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>AMER Tech Refresh Ownership</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_Deal_Reg_Update</fullName>
        <field>DealReg_Deal_Registration_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved Deal Reg Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AreaManagerEmail</fullName>
        <field>Area_Manager_Email__c</field>
        <name>Area Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CityUpdate</fullName>
        <description>Update the city field on the lead with the Billing City text from the account.</description>
        <field>City</field>
        <formula>Related_Account__r.BillingCity</formula>
        <name>City Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ClearQOwnershipEndonStart</fullName>
        <field>Q_Ownership_End__c</field>
        <name>Clear Q Ownership End on Start</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CompanyUpdate</fullName>
        <description>Update the lead company with the company name from the account.</description>
        <field>Company</field>
        <formula>Related_Account__r.Name</formula>
        <name>Company Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Company_Update</fullName>
        <description>Update the lead company with the company name from the account.</description>
        <field>Company</field>
        <formula>Related_Account__r.Name</formula>
        <name>Company Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CountryUpdate</fullName>
        <description>Update the standard country text field with the billing country data from the account.</description>
        <field>Country</field>
        <formula>Related_Account__r.BillingCountry</formula>
        <name>Country Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CreateUpdateDateAcceptedtoNullonU</fullName>
        <description>This field updates Accept Lead field to null when Lead&apos;s Owner is changed.</description>
        <field>Date_Accepted__c</field>
        <name>Create Update Date Accepted to Null on U</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DR_Approved_Record_Type_Update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Deal_Registration_Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DR Approved Record Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DR_Rejected_Record_Type_Update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Deal_Registration_Rejected</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DR Rejected Record Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_F_U</fullName>
        <field>Passed_to_Renewals_Date__c</field>
        <formula>TODAY()</formula>
        <name>Date F U</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DistrictManagerEmail</fullName>
        <description>Used to update the District Manager&apos;s email address when an appointment is set on the lead.</description>
        <field>District_Manager_Email__c</field>
        <formula>District_Manager__r.Email</formula>
        <name>District Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DomesticDUNSEntityClear</fullName>
        <description>Clear out the value in the Domestic DUNS/Entity field.</description>
        <field>Domestic_DUNS_Entity__c</field>
        <name>Domestic DUNS/Entity Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DomesticDUNSEntityUpdate</fullName>
        <description>Update the domestic DUNS/Entity on the lead with info from the account.</description>
        <field>Domestic_DUNS_Entity__c</field>
        <formula>Related_Account__r.Domestic_DUNS_Entity__c</formula>
        <name>Domestic DUNS/Entity Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMCClassification</fullName>
        <field>EMC_Classification__c</field>
        <formula>Related_Account__r.EMC_Classification__c</formula>
        <name>EMC Classification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMCPullBackDeal</fullName>
        <field>EMC_Pull_Back__c</field>
        <literalValue>1</literalValue>
        <name>EMCPullBackDeal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMC_Pull_Back</fullName>
        <field>EMC_Pull_Back__c</field>
        <literalValue>1</literalValue>
        <name>EMC Pull Back</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMC_Pull_back_Date_Stamp</fullName>
        <description>EMC_Pull_back_Date_Stamp</description>
        <field>EMC_Pull_Back_Date_Time_Stamp__c</field>
        <formula>Now()</formula>
        <name>EMC Pull back Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMC_Vertical_Update</fullName>
        <description>Update the EMC Vertical field on the lead with the info from the account.</description>
        <field>EMC_Major_Vertical__c</field>
        <formula>Related_Account__r.EMC_Major_Vertical__c</formula>
        <name>EMC Vertical Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_Tech_Refresh_Ownership</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEATechRefreshRenewalsQueue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>EMEA Tech Refresh Ownership</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ebusiness_Status</fullName>
        <field>eBus_Lead_Status__c</field>
        <literalValue>New</literalValue>
        <name>Ebusiness Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EmcPullTimeStamp</fullName>
        <field>EMC_Pull_Back_Date_Time_Stamp__c</field>
        <formula>Now()</formula>
        <name>EmcPullTimeStamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flip_to_Tech_Refresh_Page_Layout</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Tech_Refresh_Lead_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Flip to Tech Refresh Page Layout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GlobalDUNSEntityClear</fullName>
        <description>Clear out the value in the Global DUNS/Entity field.</description>
        <field>Global_DUNS_Entity__c</field>
        <name>Global DUNS/Entity Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GlobalDUNSEntityUpdate</fullName>
        <description>Update the global DUNS/Entity on the lead with info from the account.</description>
        <field>Global_DUNS_Entity__c</field>
        <formula>Related_Account__r.Global_DUNS_Entity__c</formula>
        <name>Global DUNS/Entity Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IncrementOwnershipChangeCount</fullName>
        <field>Number_of_Ownership_Changes__c</field>
        <formula>IF( ISNULL( Number_of_Ownership_Changes__c ) , 1, Number_of_Ownership_Changes__c + 1)</formula>
        <name>Increment Ownership Change Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>InstallBaseIndicator</fullName>
        <field>Install_Base_Indicator__c</field>
        <formula>Related_Account__r.Indicator__c</formula>
        <name>Install Base Indicator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leadownerchange</fullName>
        <field>DealRegOwner__c</field>
        <literalValue>1</literalValue>
        <name>Leadownerchange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leadownercheck</fullName>
        <field>LeadOwnerchek__c</field>
        <literalValue>1</literalValue>
        <name>Leadownercheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ParentDUNSEntity</fullName>
        <description>Update the Parent DUNS/Entity on the lead with info from the account.</description>
        <field>Parent_DUNS_Entity__c</field>
        <formula>Related_Account__r.Parent_DUNS_Entity__c</formula>
        <name>Parent DUNS/Entity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ParentDUNSEntityClear</fullName>
        <description>Clear out the value in the Parent DUNS/Entity field.</description>
        <field>Parent_DUNS_Entity__c</field>
        <name>Parent DUNS/Entity Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PartyIDClear</fullName>
        <description>Clear out the value in the Party ID field.</description>
        <field>Party_ID__c</field>
        <name>Party ID Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PartyIDUpdate</fullName>
        <description>Update the party id on the lead with info from the account.</description>
        <field>Party_ID__c</field>
        <formula>Related_Account__r.Party_ID__c</formula>
        <name>Party ID Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PartyNumberClear</fullName>
        <description>Clear out the value in the Party Number field.</description>
        <field>Party_Number__c</field>
        <name>Party Number Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PartyNumberUpdate</fullName>
        <description>Update the Party number on the lead with the info from the account.</description>
        <field>Party_Number__c</field>
        <formula>Related_Account__r.Party_Number__c</formula>
        <name>Party Number Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Party_ID_Update</fullName>
        <description>Update the party id on the lead with info from the account.</description>
        <field>Party_ID__c</field>
        <formula>Related_Account__r.Party_ID__c</formula>
        <name>Party ID Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Party_Number_Update</fullName>
        <description>Update the Party number on the lead with the info from the account.</description>
        <field>Party_Number__c</field>
        <formula>Related_Account__r.Party_Number__c</formula>
        <name>Party Number Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pass_to_Partner_Check</fullName>
        <field>Passed_to_Partner__c</field>
        <literalValue>1</literalValue>
        <name>Pass to Partner - Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Passed_to_Renewals_By_F_U</fullName>
        <field>Passed_to_Renewals_By__c</field>
        <formula>$User.FirstName  &amp; &apos; &apos;  &amp;   $User.LastName</formula>
        <name>Passed to Renewals By F U</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Passed_to_Renewals_Role_Update</fullName>
        <description>Update the role of the user</description>
        <field>Passed_to_Renewals_By_Role__c</field>
        <formula>$UserRole.Name</formula>
        <name>Passed to Renewals Role Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PopulateCreatedbyBU</fullName>
        <description>Workflow to populate the Created by BU field with the BU of the User who creates the Lead/Prospect record. Only applies when Lead Source is Manual or Manual Import.</description>
        <field>Created_by_BU__c</field>
        <formula>CASE( $User.BU__c , &quot;EMEA&quot;, &quot;EMEA&quot;, &quot;CMA&quot;,&quot;CMA&quot;, &quot;NA&quot;, &quot;NA&quot;, &quot;LATAM&quot;, &quot;LATAM&quot;, &quot;&quot;)</formula>
        <name>Populate Created by BU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PopulateDateClosedConverted</fullName>
        <description>Populate the Date Closed/Converted field with NOW.</description>
        <field>Date_Closed_Converted__c</field>
        <formula>NOW()</formula>
        <name>Populate Date Closed/Converted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PopulateDateQualified</fullName>
        <description>Populate the Date Qualified field with NOW – Date/Time stamp</description>
        <field>Date_Qualified__c</field>
        <formula>NOW()</formula>
        <name>Populate Date Qualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Qualifiedby</fullName>
        <description>Populate the Qualified by field with Reps name</description>
        <field>Qualified_by__c</field>
        <formula>$User.FirstName  + &apos; &apos; + $User.LastName</formula>
        <name>Qualified by</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QualifiedbyRole</fullName>
        <description>Populate the Qualified by Role field with Reps Role Name</description>
        <field>Qualified_by_Role__c</field>
        <formula>$UserRole.Name</formula>
        <name>Qualified by Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_Deal_Reg_Field_Update</fullName>
        <field>DealReg_Deal_Registration_Status__c</field>
        <literalValue>PSC Declined</literalValue>
        <name>Rejected Deal Reg Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Renewls_Department_Update</fullName>
        <description>Update the department</description>
        <field>Passed_to_Renewals_by_Department__c</field>
        <formula>$User.Department</formula>
        <name>Renewls Department Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Outbound_Message_Flag</fullName>
        <description>Reset Outbound Message Flag</description>
        <field>SendOutboundNotification__c</field>
        <literalValue>0</literalValue>
        <name>Reset Outbound Message Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetQOwnershipEndDate</fullName>
        <field>Q_Ownership_End__c</field>
        <formula>Today()</formula>
        <name>Set Q Ownership End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetQOwnershipStartDate</fullName>
        <field>Q_Ownership_Start__c</field>
        <formula>Today()</formula>
        <name>Set Q Ownership Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Closed_Reason_Detail_to_Null</fullName>
        <field>Closed_Reason_Details__c</field>
        <name>Set Closed Reason Detail to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Closed_Reason_to_None</fullName>
        <field>Closed_Reason__c</field>
        <name>Set Closed Reason to None</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_isClosed_to_False</fullName>
        <field>isClosed__c</field>
        <literalValue>0</literalValue>
        <name>Set Lead isClosed to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Linked_to_Account</fullName>
        <field>RecordTypeId</field>
        <lookupValue>LinkedtoAccountLeadRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Linked to Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Manually_Created</fullName>
        <field>RecordTypeId</field>
        <lookupValue>ManuallyCreatedLeadRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Manually Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SiteDUNSEntityUpdate</fullName>
        <description>Update the site DUNS/Entity on the lead with the data from the account.</description>
        <field>Site_DUNS_Entity__c</field>
        <formula>Related_Account__r.Site_DUNS_Entity__c</formula>
        <name>Site DUNS/Entity Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SitedDUNSEntityClear</fullName>
        <description>Clear out the value of the Site DUNS/Entity field.</description>
        <field>Site_DUNS_Entity__c</field>
        <name>Sited DUNS/Entity Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StateProvinceUpdate</fullName>
        <description>Update state/province on the lead with the Billing State/Province from the account.</description>
        <field>State</field>
        <formula>Related_Account__r.BillingState</formula>
        <name>State/Province Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StreetUpdate</fullName>
        <description>Update street on the lead with the Billing Street info from the account.</description>
        <field>Street</field>
        <formula>Related_Account__r.BillingStreet</formula>
        <name>Street Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Handoff_to_TRACK</fullName>
        <field>DealReg_Handoff_to_TRACK__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Handoff to TRACK</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAcceptLeadtoNull</fullName>
        <field>Accept_Lead__c</field>
        <literalValue>0</literalValue>
        <name>Update Accept Lead to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDateAcceptedtoNull</fullName>
        <description>This field updates Accept Lead field to null when Lead&apos;s Owner is changed.</description>
        <field>Date_Accepted__c</field>
        <name>Update Date Accepted to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateOwnerChangeDate</fullName>
        <description>This Field update action updates Last lead Owner field when Lead&apos;s owner is changed.</description>
        <field>Last_Owner_Change_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Owner Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRecordTypeClosedConverted</fullName>
        <description>Used to update the Lead record type to &quot;Converted/Closed&quot; to control page layouts and read-only fields</description>
        <field>RecordTypeId</field>
        <lookupValue>ClosedConvertedLeadRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecordType Closed/Converted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRecordTypeLinkedtoAccount</fullName>
        <description>Used to update the Lead record type to &quot;Linked to Account&quot; to control page layouts and read-only fields</description>
        <field>RecordTypeId</field>
        <lookupValue>LinkedtoAccountLeadRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecordType Linked to Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRecordTypeManuallyCreated</fullName>
        <description>Used to update the Lead record type to &quot;Manually Created&quot;</description>
        <field>RecordTypeId</field>
        <lookupValue>ManuallyCreatedLeadRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecordType Manually Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ApprovalStatus_Field_to_Submitted</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Submitted By Field Rep</literalValue>
        <name>Update ApprovalStatus Field to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Flagged for Field Rep Review</literalValue>
        <name>Update Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Field_Rep_Exp</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Field Rep SLA Expired</literalValue>
        <name>Update Approval Status to Field Rep Exp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approved_By_Field_Rep</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved By Field Rep</literalValue>
        <name>Update Approved By Field Rep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Channel_to_Indirect</fullName>
        <field>Channel__c</field>
        <literalValue>INDIRECT</literalValue>
        <name>Update Channel to Indirect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DR_Status</fullName>
        <field>DealReg_Deal_Registration_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update DR Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Field_Approval_Rejection_Date_Tim</fullName>
        <field>Deal_Field_Approval_Rejection_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Field Approval/Rejection Date/Tim</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_IsExpired_Checkbox</fullName>
        <field>IsExpired__c</field>
        <literalValue>1</literalValue>
        <name>Update IsExpired Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Type</fullName>
        <field>Lead_Type_Based_on_Linking__c</field>
        <literalValue>Renewals</literalValue>
        <name>Update Lead Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Originator</fullName>
        <description>Update Originator to Partner</description>
        <field>Lead_Originator__c</field>
        <literalValue>Partner</literalValue>
        <name>Update Originator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Originator_Details</fullName>
        <field>Originator_Details__c</field>
        <literalValue>Partner Created</literalValue>
        <name>Update Originator Details</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PSC_Approval_Rejection_Date_Time</fullName>
        <field>DealReg_PSC_Approval_Rejection_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update PSC Approval/Rejection Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Passed_From_Partner</fullName>
        <field>Passed_from_Distributor__c</field>
        <literalValue>1</literalValue>
        <name>Update Passed From Partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Submitted</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Deal_Registration_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Rejected_By_Field_Rep</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected By Field Rep</literalValue>
        <name>Update Rejected By Field Rep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Force</fullName>
        <field>Sales_Force__c</field>
        <literalValue>EMC</literalValue>
        <name>Update Sales Force</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved_By_Field_Rep</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved By Field Rep</literalValue>
        <name>Update Status to Approved By Field Rep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Expired</fullName>
        <field>DealReg_Deal_Registration_Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Update Status to Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected_By_Field_Rep</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected By Field Rep</literalValue>
        <name>Update Status to Rejected By Field Rep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdatedDateAccepted</fullName>
        <description>This Field Update action Populates Date Accepted Field when a Lead is accepted</description>
        <field>Date_Accepted__c</field>
        <formula>NOW()</formula>
        <name>Updated Date Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Status_Field</fullName>
        <field>Status</field>
        <literalValue>Converted to Opportunity</literalValue>
        <name>Updated Status Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdatedisBooked</fullName>
        <description>This Field Update action Populates Date Accepted Field when a Lead is accepted</description>
        <field>isBooked__c</field>
        <literalValue>1</literalValue>
        <name>Updated isBooked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateisClosed</fullName>
        <description>Update isClosed checkbox to true</description>
        <field>isClosed__c</field>
        <literalValue>1</literalValue>
        <name>Update isClosed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateisConverted</fullName>
        <description>Update is Converted checkbox to true</description>
        <field>isConverted__c</field>
        <literalValue>1</literalValue>
        <name>Update isConverted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ZipPostalUpdate</fullName>
        <description>Update the zip/postal code on lead with zip/postal from the account.</description>
        <field>PostalCode</field>
        <formula>Related_Account__r.BillingPostalCode</formula>
        <name>Zip/Postal Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateDealValue</fullName>
        <description>update Deal Value</description>
        <field>Deal_Value__c</field>
        <formula>0.00</formula>
        <name>update Deal Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateopportunityCloseDate</fullName>
        <description>update opportunity Close Date</description>
        <field>Opportunity_Close_Date__c</field>
        <formula>(Today()+30)</formula>
        <name>update opportunity Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateopportunitystatus</fullName>
        <description>update opportunity status</description>
        <field>Opportunity_Status__c</field>
        <literalValue>Pipeline</literalValue>
        <name>update opportunity status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Deal_Reg_Integration</fullName>
        <apiVersion>21.0</apiVersion>
        <description>https://soagateway-test.emc.com/sst/runtime.asvc/com.actional.intermediary.aicNotifyDealRegServiceEMCSFDC_QAX</description>
        <endpointUrl>https://soagateway-test.emc.com/sst/runtime.asvc/com.actional.intermediary.aicNotifyDealRegServiceEMCSFDC_DES</endpointUrl>
        <fields>DealReg_Deal_Registration_Status__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>integration_admin@emc.com</integrationUser>
        <name>PRM_DealReg_Deal Reg Integration</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>UpdateLead</fullName>
        <apiVersion>8.0</apiVersion>
        <description>INTG Message</description>
        <endpointUrl>https://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.sfdcLead</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>integration_admin@emc.com</integrationUser>
        <name>Update Lead</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>AMER Tech Refresh Ownership</fullName>
        <actions>
            <name>AMER_Tech_Refresh_Ownership</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Pass to Renewals</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Theater__c</field>
            <operation>equals</operation>
            <value>Americas</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Area Manager Email</fullName>
        <actions>
            <name>AreaManagerEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used to populate the Area Manager&apos;s email to be used in the Appt. Set Email Template.</description>
        <formula>isChanged( Area_Manager__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Channel Manager Email</fullName>
        <active>false</active>
        <description>Used to populate the Channel Manager&apos;s email to be used in the Appt. Set Email Template.</description>
        <formula>isChanged(  Channel_Manager__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check for Q Assignment End</fullName>
        <actions>
            <name>SetQOwnershipEndDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Bypassed for Deal Reg</description>
        <formula>BEGINS( OwnerId , &quot;005&quot;) &amp;&amp; BEGINS( PRIORVALUE( OwnerId ) , &quot;00G&quot;) &amp;&amp; DealReg_Deal_Registration__c == false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check for Q Ownership Start</fullName>
        <actions>
            <name>ClearQOwnershipEndonStart</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetQOwnershipStartDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Bypassed for Deal Reg</description>
        <formula>BEGINS( OwnerId , &quot;00G&quot;) &amp;&amp; DealReg_Deal_Registration__c == false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Lead with account to New or working</fullName>
        <actions>
            <name>Set_Closed_Reason_Detail_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Closed_Reason_to_None</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Lead_isClosed_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Record_Type_to_Linked_to_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  NOT( ISNEW() ),  ISPICKVAL( PRIORVALUE( Status ) , &apos;Closed&apos; ),  OR(  ISPICKVAL( Status , &apos;New&apos; ),  ISPICKVAL(Status , &apos;Working&apos; )  ),  NOT(ISBLANK( Related_Account__c ))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Closed Lead withOUT account to New or working</fullName>
        <actions>
            <name>Set_Closed_Reason_Detail_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Closed_Reason_to_None</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Lead_isClosed_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Record_Type_to_Manually_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  NOT( ISNEW() ),  ISPICKVAL( PRIORVALUE( Status ) , &apos;Closed&apos; ),  OR(  ISPICKVAL( Status , &apos;New&apos; ),  ISPICKVAL(Status , &apos;Working&apos; )  ),  ISBLANK( Related_Account__c )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DEALREG_Update Approval Status on  Field Rep SLA Expiration</fullName>
        <actions>
            <name>Update_Approval_Status_to_Field_Rep_Exp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.DealReg_Field_SLA_Expired__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Deal Reg expire in 10 days For APJ</fullName>
        <active>true</active>
        <formula>AND(  ISPICKVAL( DealReg_Theater__c , &quot;APJ&quot;), DealReg_Deal_Registration__c, OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Approved&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Submitted&quot;)  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR04_APJ</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>-10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg expire in 10 days For Americas</fullName>
        <active>true</active>
        <formula>AND(  ISPICKVAL( DealReg_Theater__c , &quot;Americas&quot;), DealReg_Deal_Registration__c, OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Approved&quot;), ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Submitted&quot;)  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Deal_registration_is_available_for_extension_will_expire_in_10_days_Americas</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>-10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg expire in 10 days For EMEA</fullName>
        <active>true</active>
        <formula>AND(  ISPICKVAL( DealReg_Theater__c , &quot;EMEA&quot;), DealReg_Deal_Registration__c, OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Approved&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Submitted&quot;)  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR04_EMEA</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>-10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg expire in 30 days For APJ</fullName>
        <active>false</active>
        <formula>AND(  ISPICKVAL( DealReg_Theater__c , &quot;APJ&quot;), DealReg_Deal_Registration__c, OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Approved&quot;), ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Submitted&quot;)  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR04_APJ</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg expire in 30 days For Americas</fullName>
        <active>false</active>
        <formula>AND(  ISPICKVAL( DealReg_Theater__c , &quot;Americas&quot;), DealReg_Deal_Registration__c, OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Approved&quot;), ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Submitted&quot;)  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR04_Americas</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg expire in 30 days For EMEA</fullName>
        <active>false</active>
        <formula>AND(  ISPICKVAL( DealReg_Theater__c , &quot;EMEA&quot;), DealReg_Deal_Registration__c, OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Approved&quot;),ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Submitted&quot;)  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR04_EMEA</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg expire in 45 days For APJ</fullName>
        <active>true</active>
        <formula>AND(  ISPICKVAL( DealReg_Theater__c , &quot;APJ&quot;), DealReg_Deal_Registration__c, OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Approved&quot;), ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Submitted&quot;)  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR04_APJ</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg expire in 45 days For Americas</fullName>
        <active>true</active>
        <formula>AND(  ISPICKVAL( DealReg_Theater__c , &quot;Americas&quot;), DealReg_Deal_Registration__c, OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Approved&quot;), ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Submitted&quot;)  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR04_Americas</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg expire in 45 days For EMEA</fullName>
        <active>true</active>
        <formula>AND(  ISPICKVAL( DealReg_Theater__c , &quot;EMEA&quot;), DealReg_Deal_Registration__c, OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Approved&quot;), ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Submitted&quot;)  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR04_EMEA</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg is Approved for APJ</fullName>
        <actions>
            <name>Approval_of_a_Deal_Registration_APJ</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>DR_Notification_DR01_APJ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification sent after Approval for APJ</description>
        <formula>DealReg_Deal_Registration__c  &amp;&amp; $User.Id &lt;&gt; &apos;00570000001L8zr&apos;  &amp;&amp; NOT( ISBLANK( DealReg_Related_Opportunity_Number__c) )  &amp;&amp; ISPICKVAL( DealReg_Deal_Registration_Status__c ,&quot;Approved&quot;)  &amp;&amp; ISPICKVAL( DealReg_Theater__c , &quot;APJ&quot;)  &amp;&amp; OR( AND(ischanged( Resend_Outbound_Message__c ), BEGINS(Resend_Outbound_Message__c, &apos;Synergy Opportunity No is changed&apos;) ), AND(DealReg_Send_Approval_Notification__c,IsChanged(DealReg_Send_Approval_Notification__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Deal Reg is Approved for Americas</fullName>
        <actions>
            <name>Approval_of_a_Deal_Registration_AMER</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>DR_Notification_DR01_Americas</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification sent after Approval for Americas</description>
        <formula>DealReg_Deal_Registration__c  &amp;&amp; $User.Id &lt;&gt; &apos;00570000001L8zr&apos;  &amp;&amp; NOT( ISBLANK( DealReg_Related_Opportunity_Number__c) )  &amp;&amp; ISPICKVAL( DealReg_Deal_Registration_Status__c ,&quot;Approved&quot;)  &amp;&amp; ISPICKVAL( DealReg_Theater__c , &quot;Americas&quot;)  &amp;&amp; OR( AND(ischanged( Resend_Outbound_Message__c ), BEGINS(Resend_Outbound_Message__c, &apos;Synergy Opportunity No is changed&apos;) ), AND(DealReg_Send_Approval_Notification__c,IsChanged(DealReg_Send_Approval_Notification__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Deal Reg is Approved for EMEA</fullName>
        <actions>
            <name>Approval_of_a_Deal_Registration_EMEA</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>DR_Notification_DR01_EMEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification sent after Approval for EMEA</description>
        <formula>DealReg_Deal_Registration__c  &amp;&amp; $User.Id &lt;&gt; &apos;00570000001L8zr&apos;  &amp;&amp; NOT( ISBLANK( DealReg_Related_Opportunity_Number__c) )  &amp;&amp; ISPICKVAL( DealReg_Deal_Registration_Status__c ,&quot;Approved&quot;)  &amp;&amp; ISPICKVAL( DealReg_Theater__c , &quot;EMEA&quot;)  &amp;&amp; OR( AND(ischanged( Resend_Outbound_Message__c ), BEGINS(Resend_Outbound_Message__c, &apos;Synergy Opportunity No is changed&apos;) ), AND(DealReg_Send_Approval_Notification__c,IsChanged(DealReg_Send_Approval_Notification__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Deal Reg is expired for APJ</fullName>
        <active>false</active>
        <formula>AND(   NOT(OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Expired&quot;), ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Closed-Won&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Closed-Lost&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;PSC Declined&quot;)  )  ), DealReg_Deal_Registration__c, ISPICKVAL( DealReg_Theater__c , &quot;APJ&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR06_APJ</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg is expired for APJ1</fullName>
        <active>true</active>
        <formula>AND(   NOT(OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Expired&quot;), ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Closed-Won&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Closed-Lost&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;PSC Declined&quot;)  )  ), DealReg_Deal_Registration__c, ISPICKVAL( DealReg_Theater__c , &quot;APJ&quot;),  Is_Extension_Request_Not_Submitted__c,  NOT(ISBLANK(DealReg_Expiration_Date__c))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR06_APJ</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>15</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg is expired for Americas</fullName>
        <active>false</active>
        <formula>AND(  NOT(OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Expired&quot;), ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Closed-Won&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Closed-Lost&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;PSC Declined&quot;)  )  ), DealReg_Deal_Registration__c, ISPICKVAL( DealReg_Theater__c , &quot;Americas&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR06_Americas</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg is expired for Americas1</fullName>
        <active>true</active>
        <formula>AND(  NOT(OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Expired&quot;), ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Closed-Won&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Closed-Lost&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;PSC Declined&quot;)  )  ), DealReg_Deal_Registration__c, ISPICKVAL( DealReg_Theater__c , &quot;Americas&quot;) ,  Is_Extension_Request_Not_Submitted__c,  NOT(ISBLANK(DealReg_Expiration_Date__c))   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR06_Americas</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>27</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg is expired for EMEA</fullName>
        <active>false</active>
        <formula>AND(  NOT(OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Expired&quot;), ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Closed-Won&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Closed-Lost&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;PSC Declined&quot;)  )  ),  DealReg_Deal_Registration__c, ISPICKVAL( DealReg_Theater__c , &quot;EMEA&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR06_EMEA</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deal Reg is expired for EMEA1</fullName>
        <active>true</active>
        <formula>AND(  NOT(OR( ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Expired&quot;), ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Closed-Won&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;Closed-Lost&quot;),  ISPICKVAL( DealReg_Deal_Registration_Status__c , &quot;PSC Declined&quot;)  )  ),  DealReg_Deal_Registration__c, ISPICKVAL( DealReg_Theater__c , &quot;EMEA&quot;) ,  Is_Extension_Request_Not_Submitted__c,  NOT(ISBLANK(DealReg_Expiration_Date__c))   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DR_Notification_DR06_EMEA</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.DealReg_Expiration_Date__c</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>DealReg Handoff to TRACK Unchecked</fullName>
        <actions>
            <name>Uncheck_Handoff_to_TRACK</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>&quot;Handoff to TRACK&quot; checkbox should be unchecked when Related Account is selected</description>
        <formula>AND(  NOT(ISBLANK(Related_Account__c )), (ISBLANK( DealReg_TRACK_Party_Number__c))   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DealReg_Expiration Date has been extended for APJ</fullName>
        <actions>
            <name>DR_Notification_DR07_APJ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(DealReg_Expiration_Date__c), DealReg_Expiration_Date__c &gt; PRIORVALUE(DealReg_Expiration_Date__c), NOT(ISPICKVAL( DealReg_Deal_Registration_Status__c,&apos;Expired&apos; )), ISPICKVAL( DealReg_Theater__c , &quot;APJ&quot;),DealReg_Expiration_Date_changed_By_PSC__c,Len( Synergy_Opportunity_Number_1__c )&gt; 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DealReg_Expiration Date has been extended for Americas</fullName>
        <actions>
            <name>DR_Notification_DR07_Americas</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(DealReg_Expiration_Date__c), DealReg_Expiration_Date__c &gt; PRIORVALUE(DealReg_Expiration_Date__c), NOT(ISPICKVAL( DealReg_Deal_Registration_Status__c,&apos;Expired&apos; )), ISPICKVAL( DealReg_Theater__c , &quot;Americas&quot;),DealReg_Expiration_Date_changed_By_PSC__c,Len( Synergy_Opportunity_Number_1__c )&gt; 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DealReg_Expiration Date has been extended for EMEA</fullName>
        <actions>
            <name>DR_Notification_DR07_EMEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(DealReg_Expiration_Date__c), DealReg_Expiration_Date__c &gt; PRIORVALUE(DealReg_Expiration_Date__c), NOT(ISPICKVAL( DealReg_Deal_Registration_Status__c,&apos;Expired&apos; )), ISPICKVAL( DealReg_Theater__c , &quot;EMEA&quot;),DealReg_Expiration_Date_changed_By_PSC__c,Len( Synergy_Opportunity_Number_1__c )&gt; 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DealReg_Update Approval Status</fullName>
        <actions>
            <name>Update_Approval_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.DealReg_Field_Review_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Dealreg Field Approval Notification for APJ</fullName>
        <actions>
            <name>Dealreg_Field_Approval_Email_Notification_for_APJ</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Field_Approval_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DealReg_Deal_Registration__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DealReg_Theater__c</field>
            <operation>equals</operation>
            <value>APJ</value>
        </criteriaItems>
        <description>Dealreg Field Approval Notification for APJ</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Dealreg Field Approval Notification for Americas</fullName>
        <actions>
            <name>Dealreg_Field_Approval_Email_Notification_for_Americas</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Field_Approval_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DealReg_Deal_Registration__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DealReg_Theater__c</field>
            <operation>equals</operation>
            <value>Americas</value>
        </criteriaItems>
        <description>Dealreg Field Approval Notification for Americas</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Dealreg Field Approval Notification for EMEA</fullName>
        <actions>
            <name>Dealreg_Field_Approval_Email_Notification_for_EMEA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Field_Approval_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DealReg_Deal_Registration__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DealReg_Theater__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <description>Dealreg Field Approval Notification for EMEA</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Originator and Details</fullName>
        <actions>
            <name>Update_Channel_to_Indirect</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Originator</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Originator_Details</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Sales_Force</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL($UserRole.PortalType, &quot;Partner&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>District Manager Email</fullName>
        <actions>
            <name>DistrictManagerEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used to populate the District Manager&apos;s email to be used in the Appt. Set Email Template.</description>
        <formula>isChanged(  District_Manager__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EBUSINESS RFQ PUNCH LEAD DIRECT ASSIGNMENT TO PARTNER USER</fullName>
        <actions>
            <name>eBusiness_RFQ_Punch_Lead_Owner_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This notification is sent when an eBusiness Lead with Source = eStore and Source/ Program Detail = RFQPunch is assigned to a Partner User.</description>
        <formula>AND( BEGINS(OwnerId , &apos;005&apos;) ,
 ISCHANGED(OwnerId),
 ISPICKVAL(Lead_Originator__c, &apos;eStore&apos;) ,
 ISPICKVAL( Originator_Details__c , &apos;RFQPunch&apos;) , NOT(eBus_Lead_Owner_Org__c =&apos;EMC&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMEA Tech Refresh Ownership</fullName>
        <actions>
            <name>EMEA_Tech_Refresh_Ownership</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Pass to Renewals</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Theater__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flip to Tech Refresh Page Layout</fullName>
        <actions>
            <name>Flip_to_Tech_Refresh_Page_Layout</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Originator_Details__c</field>
            <operation>contains</operation>
            <value>Tech Refresh</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>INTG Notification on Integration Error</fullName>
        <actions>
            <name>INTGErrorMailNotification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send mail notification when error occurred in integration process.
Bypassed for Deal Reg</description>
        <formula>AND( LEN( Integration_Error_Message__c )&gt;0 ,              NOT ( ISPICKVAL(Status ,&apos;Closed&apos;) ),             DealReg_Deal_Registration__c == false,            $User.Id &lt;&gt; &apos;00570000001L8zr&apos;    )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>INTG-TP%232-StatusClosed</fullName>
        <actions>
            <name>UpdateLead</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>Update Lead status in Synergy based on Salesforce.com Status Triggers.</description>
        <formula>AND(  ( ISPICKVAL( Status, &apos;Closed&apos;) ),  ( LEN(Synergy_Lead_ID__c) &gt;0 ) ,   ( LEN(Integration_Error_Message__c)=0)  ,  NOT( OR ( OwnerId = &apos;00G70000001ITxA&apos;,OwnerId =&apos;00G70000001ITx9&apos;)) ,  ( LastModifiedById &lt;&gt; &apos;00570000001JctW&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>INTG-TP%232-StatusQualified-SyngLead</fullName>
        <actions>
            <name>UpdateLead</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>Update Lead status in Synergy based on Salesforce.com Status Triggers.</description>
        <formula>AND(  ( ISPICKVAL( Status, &apos;Qualified&apos;) ),  ( LEN(Synergy_Lead_ID__c) &gt;0 ) , ( LEN(Integration_Error_Message__c)=0)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>INTG-TP%233-StatusQualified-ManualLead</fullName>
        <actions>
            <name>UpdateLead</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Create Lead When Qualified.</description>
        <formula>OR( AND( not (ispickval(Reporting_Owner__r.UserType,&quot;PowerPartner&quot;)), ( ISPICKVAL( Status, &apos;Qualified&apos;) ), ( LEN(Synergy_Lead_ID__c) = 0) , ( LEN (Integration_Error_Message__c) = 0), ISBLANK(Related_Account__c) ), AND( (ispickval(Reporting_Owner__r.UserType,&quot;PowerPartner&quot;)), ( ISPICKVAL( Status, &apos;Converted to Opportunity&apos;) ), ( LEN(Synergy_Lead_ID__c) = 0) , ( LEN (Integration_Error_Message__c) = 0), ISBLANK(Related_Account__c) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>INTG-TP%235-StatusConverted</fullName>
        <actions>
            <name>UpdateLead</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>Convert Lead Status to Convert to Opportunity and synchronize Salesforce.com and Synergy with Opportunity Number.</description>
        <formula>AND(  ( ISPICKVAL( Status, &apos;Converted to Opportunity&apos;) ), ( LEN( Synergy_Integration_Opportunity_Number__c )=0) ,   ( LEN(Integration_Error_Message__c) = 0)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISR%2FField Rep Email</fullName>
        <active>false</active>
        <description>Used to populate the ISR/Field Rep’s email to be used in the Appt. Set Email Template.</description>
        <formula>isChanged(  ISR_Field_Rep__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IsQualifiedLead</fullName>
        <actions>
            <name>PopulateDateQualified</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Qualifiedby</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QualifiedbyRole</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Triggered when status is &quot;Qualified&quot;. Used to populate the &quot;Date Qualified&quot;, &quot;Qualified by&quot; and &quot;Qualifed by Role&quot; fields.</description>
        <formula>AND (ISPICKVAL( Status ,&quot;Qualified&quot;),   LEN(Qualified_by__c) =0)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Assignment Alert for APJ Contact Us Queue</fullName>
        <actions>
            <name>Lead_Assignment_Alert_for_APJ_Contact_Us_Queue</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Core APJ Japan Contact Us Leads</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>-</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Owner Change Notification to CAM</fullName>
        <actions>
            <name>CAM_Owner_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Lead Owner Change Notification to CAM
Bypassed for Deal Reg</description>
        <formula>AND(  $User.Id &lt;&gt; &apos;00570000001L8zr&apos;, ISCHANGED( OwnerId ) ,  OR (   NOT(Tier_2_Partner__r.Reporting_Owner__r.Opt_Out_of_Notifications__c),  NOT(Partner__r.Reporting_Owner__r.Opt_Out_of_Notifications__c)  ),  Not(  OR (  Ispickval( Originator_Details__c,&apos;Maintenance Tech Refresh Alert&apos;),  Ispickval( Originator_Details__c,&apos;Warranty Tech Refresh Alert&apos;),  Ispickval( Originator_Details__c,&apos;Dell Tech Refresh Alert&apos;)  )  ),  ISPICKVAL( Reporting_Owner__r.UserType,&quot;PowerPartner&quot; ) , DealReg_Deal_Registration__c == false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Owner Change Notification to Lead Owner</fullName>
        <actions>
            <name>Lead_Owner_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Lead Owner Change Notification to Lead owner
Bypassed for Deal Reg</description>
        <formula>AND( ISCHANGED( OwnerId ) ,NOT( ISPICKVAL(Lead_Originator__c ,&apos;eStore&apos;) ) ,NOT(Reporting_Owner__r.Opt_Out_of_Notifications__c), Not( OR ( Ispickval( Originator_Details__c,&apos;Maintenance Tech Refresh Alert&apos;), Ispickval( Originator_Details__c,&apos;Warranty Tech Refresh Alert&apos;), Ispickval( Originator_Details__c,&apos;Dell Tech Refresh Alert&apos;) ) ), ISPICKVAL( Reporting_Owner__r.UserType,&quot;PowerPartner&quot; ), DealReg_Deal_Registration__c == false, $User.Id &lt;&gt; &apos;00570000001L8zr&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Rejection Notification to Previous CAM</fullName>
        <actions>
            <name>Lead_Rejection_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Lead Rejection Notification to Previous CAM
Bypassed for Deal Reg</description>
        <formula>AND( OR(ISCHANGED( No_of_Times_Rejected__c) ,ISCHANGED( EMC_Lead_Rejected__c)), EMC_Lead_Rejected__c, DealReg_Deal_Registration__c == false, $User.Id &lt;&gt; &apos;00570000001L8zr&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leads Passed from Distributor</fullName>
        <actions>
            <name>Update_Passed_From_Partner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If current user is distributor and T2 is updated then Leads Passed from Distributor should be checked
Bypassed for Deal Reg</description>
        <formula>AND( OR ( $Profile.Name=&quot;EMEA Distributor Partner User&quot;, $Profile.Name=&quot;EMEA Distributor Super User&quot; ) , ISCHANGED(Tier_2_Partner__c ),  DealReg_Deal_Registration__c ==false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Manual Lead Account Removed</fullName>
        <actions>
            <name>PartyIDClear</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PartyNumberClear</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateRecordTypeManuallyCreated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Related Account is NULL and Synergy Id is NULL
5/10/2011: Bypassed for Deal Reg</description>
        <formula>AND( LEN( Related_Account__c )=0, LEN( Synergy_Lead_ID__c )=0, DealReg_Deal_Registration__c == false )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manual Lead Linked to Account</fullName>
        <actions>
            <name>CityUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Company_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CountryUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EMC_Vertical_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Party_ID_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Party_Number_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>StateProvinceUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>StreetUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ZipPostalUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Related Account NOT NULL .
Taken Out Synergy Lead Id Condition and merged all three manual lead workflows on 11 May 2010.Other two are deactivated.</description>
        <formula>AND( NOT(ISBLANK(Related_Account__c)), OR( AND( ISCHANGED( Related_Account__c ) ), AND( ISNEW(), LEN(Related_Account__c) &lt;&gt; 0 ), AND( NOT( LEN( Related_Account__c )=0 ), ( LastModifiedById =&apos;00570000001JctW&apos;) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Manual Lead Linked to Account 2</fullName>
        <actions>
            <name>PartyIDUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PartyNumberUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Related Account NOT NULL and Synergy Lead Id is NULL</description>
        <formula>OR(  AND(  ISCHANGED( Related_Account__c ),  LEN( Synergy_Lead_ID__c )=0  ),  AND(  ISNEW(), LEN(Synergy_Lead_ID__c)=0, LEN(Related_Account__c) &lt;&gt; 0  ),  AND( NOT(  LEN( Related_Account__c )=0  ),  ( LastModifiedById =&apos;00570000001JctW&apos;)  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Manual Lead Linked to Account3</fullName>
        <actions>
            <name>CompanyUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Related Account NOT NULL and Synergy Lead Id is NULL</description>
        <formula>OR( AND( ISCHANGED( Related_Account__c ), LEN( Synergy_Lead_ID__c )=0 , LEN( Related_Account__c )&lt;&gt;0 ), AND( ISNEW(), LEN(Synergy_Lead_ID__c)=0, LEN(Related_Account__c) &lt;&gt; 0 ), AND( NOT( LEN( Related_Account__c )=0 ), ( LastModifiedById =&apos;00570000001JctW&apos;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM_DealReg_Update Integration</fullName>
        <actions>
            <name>Reset_Outbound_Message_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Deal_Reg_Integration</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Update message on certain set of fields.</description>
        <formula>SendOutboundNotification__c=true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM_Deal_Reg_Creation</fullName>
        <actions>
            <name>Deal_Reg_Integration</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When Opportunity no gets populated on Related opportunity send message only if it is non null and status is approved and above.</description>
        <formula>AND(DealReg_Deal_Registration__c, Related_Opportunity__c !=null, OR(AND(ISCHANGED (DealReg_Deal_Registration_Status__c), OR (ISPICKVAL(DealReg_Deal_Registration_Status__c ,&apos;Approved&apos; ),ISPICKVAL(DealReg_Deal_Registration_Status__c ,&apos;Expired&apos; ))),AND(ISPICKVAL(DealReg_Deal_Registration_Status__c ,&apos;Approved&apos; ),OR(ISCHANGED(LastName),ISCHANGED(FirstName),ISCHANGED(DealReg_Expiration_Date__c),ISCHANGED(DealReg_Deal_Registration_Type__c),ISCHANGED(DealReg_Deal_Submitter_Name__c),ISCHANGED(DealReg_Deal_Submitter_Rep_Email__c),ISCHANGED(DealReg_Deal_Description__c),ISCHANGED(Related_Account__c),ISCHANGED(Partner__c),ISCHANGED(Tier_2_Partner__c),ISCHANGED(Related_Opportunity__c)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM_LeadPullBack_daysafterpassed</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Passed_to_Partner__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Evaluate__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Pass to Renewals,Converted to Opportunity,Rejected to Marketing,Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Last_EMC_Owner__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DealReg_Deal_Registration__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>PRM_LeadPullBack_daysafterpassed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>After_2_Days_passed</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.SLA_For_Passed_to_Partner_date_time__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>After_4_days_passed</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Final_SLA_For_Passed_to_Partnerdate_time__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>EMC_Pull_Back</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>EMC_Pull_back_Date_Stamp</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Leadownercheck</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.SLA_For_Passed_to_Partner_Change_Owner__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>PRM_LeadRegisteredPullBack_Leadafteraccepted</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Passed_to_Partner__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DealReg_Deal_Registration__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Pass to Renewals,Converted to Opportunity,Rejected to Marketing,Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Last_EMC_Owner__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>PRM_LeadRegisteredPullBack_Leadafteraccepted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>After_9_Days_Accepteds</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Final_SLA_For_Accepted_date_time__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>EMCPullBackDeal</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>EmcPullTimeStamp</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Leadownerchange</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.SLA_For_Accepted_Change_Owner_date_time__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>After_4_Days_Accepted</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.SLA_For_Accepted_date_time__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Party Id or Related Account NOT NULL</fullName>
        <actions>
            <name>UpdateRecordTypeLinkedtoAccount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to update RecordTypeId to &quot;Linked to Account&quot;. Criteria: When Party ID or Related Account are not NULL
4/29/2011: Bypassed for Deal Reg</description>
        <formula>AND( OR( NOT( ISPICKVAL( Status, &quot;Converted to Opportunity&quot; )), NOT( ISPICKVAL( Status, &quot;Closed&quot; )) ), ( OR( (NOT( LEN( Related_Account__c )=0 )), (NOT( LEN( Party_ID__c )=0 )) )   ) ) &amp;&amp; AND(NOT( $RecordType.Id = &apos;012Q00000000M2S&apos;), NOT( $RecordType.Id = &apos;012Q00000000M2N&apos;), NOT( $RecordType.Id = &apos;012Q00000000M2X&apos;), DealReg_Deal_Registration__c == false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pass To Renewals</fullName>
        <actions>
            <name>Date_F_U</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Passed_to_Renewals_By_F_U</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Pass to Renewals</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pass to Renwals</fullName>
        <actions>
            <name>Update_Lead_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Pass to Renewals</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Created by BU</fullName>
        <actions>
            <name>PopulateCreatedbyBU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DealReg_Deal_Registration__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Workflow to populate the Created by BU field with the BU of the User who creates the Lead/Prospect record. Only applies when Lead Source is Manual or Manual Import.
4/29/2011: Bypassed for Deal Reg</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate Status and Opportunity Close Date</fullName>
        <actions>
            <name>updateDealValue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>updateopportunityCloseDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>updateopportunitystatus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Opportunity_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Opportunity_Close_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Synergy_Integration_Opportunity_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Converted to Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DealReg_Deal_Registration__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Populate Status and Opportunity Close Date.
Bypassed for Deal Reg</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Renewals Role Update</fullName>
        <actions>
            <name>Passed_to_Renewals_Role_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Pass to Renewals</value>
        </criteriaItems>
        <description>Update the user role</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Renewls Department Update</fullName>
        <actions>
            <name>Renewls_Department_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Pass to Renewals</value>
        </criteriaItems>
        <description>Update department of the user</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Tech Refresh - Lead Owner Change Notification to CAM</fullName>
        <actions>
            <name>Tech_Refresh_CAM_Owner_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Tech Refresh Lead Owner Change Notification to CAM
Bypassed for Deal Reg</description>
        <formula>AND(  ISCHANGED( OwnerId ) ,  OR (   NOT(Tier_2_Partner__r.Reporting_Owner__r.Opt_Out_of_Notifications__c),  NOT(Partner__r.Reporting_Owner__r.Opt_Out_of_Notifications__c)  ),  OR (  Ispickval( Originator_Details__c,&apos;Maintenance Tech Refresh Alert&apos;),  Ispickval( Originator_Details__c,&apos;Warranty Tech Refresh Alert&apos;),  Ispickval( Originator_Details__c,&apos;Dell Tech Refresh Alert&apos;)  ),  ISPICKVAL( Reporting_Owner__r.UserType,&quot;PowerPartner&quot; ), DealReg_Deal_Registration__c == false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tech Refresh - Lead Owner Change Notification to Lead Owner</fullName>
        <actions>
            <name>Tech_Refresh_Lead_Owner_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Tech Refresh Lead Owner Change Notification to Lead owner
Bypassed for Deal Reg</description>
        <formula>AND(  ISCHANGED( OwnerId ) ,  NOT(Reporting_Owner__r.Opt_Out_of_Notifications__c),  OR  (  Ispickval( Originator_Details__c,&apos;Maintenance Tech Refresh Alert&apos;),  Ispickval( Originator_Details__c,&apos;Warranty Tech Refresh Alert&apos;),  Ispickval( Originator_Details__c,&apos;Dell Tech Refresh Alert&apos;)  ),  ISPICKVAL( Reporting_Owner__r.UserType,&quot;PowerPartner&quot; ) , DealReg_Deal_Registration__c == false  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Accept Lead to Null</fullName>
        <actions>
            <name>Ebusiness_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IncrementOwnershipChangeCount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateAcceptLeadtoNull</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateDateAcceptedtoNull</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateOwnerChangeDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Bypassed for Deal Reg</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; DealReg_Deal_Registration__c == false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Status</fullName>
        <actions>
            <name>PopulateDateClosedConverted</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateRecordTypeClosedConverted</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Updated_Status_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateisConverted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>bypassed for deal reg</description>
        <formula>AND(  NOT( ISPICKVAL(Status,&quot;Converted to Opportunity&quot;)),  NOT( ISBLANK( Related_Opportunity__c )),  NOT( DealReg_Deal_Registration__c )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Pass to Partner</fullName>
        <actions>
            <name>Pass_to_Partner_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Bypassed for Deal Reg</description>
        <formula>OR( LEN(Partner__c)&gt;0, LEN( Tier_2_Partner__c )&gt;0)&amp;&amp; (DealReg_Deal_Registration__c == false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>eBusiness Contact Me Lead Created</fullName>
        <actions>
            <name>This_notification_is_sent_to_EMCSocialSales_emc_com_email_upon_creation_of_a_lea</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>This_notification_is_sent_to_the_customer_email_upon_creation_of_a_lead_with_Sou</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Originator__c</field>
            <operation>equals</operation>
            <value>eStore</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Originator_Details__c</field>
            <operation>equals</operation>
            <value>Contact Me</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.eBus_RFQ_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This notification is sent to the customer email upon creation of a lead with Source=eStore and Source/ Program Detail = Contact Me</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>eBusiness RFQ Lead Assigned to Partner User</fullName>
        <actions>
            <name>This_notification_is_sent_when_an_eBusiness_Lead_with_Source_eStore_and_Source_P</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This notification is sent when an eBusiness Lead with Source = eStore and Source/ Program Detail = RFQ is assigned to a Partner User.</description>
        <formula>AND( BEGINS(OwnerId , &apos;005&apos;),  ISCHANGED(OwnerId),  ISPICKVAL(Lead_Originator__c, &apos;eStore&apos;), ISPICKVAL(Originator_Details__c , &apos;RFQ&apos;), NOT(eBus_Lead_Owner_Org__c =&apos;EMC&apos;),  NOT(AND(ISPICKVAL($User.UserType, &apos;PowerPartner&apos;), LastModifiedById = $User.Id, OwnerId = $User.Id)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>eBusiness RFQ Lead Created1</fullName>
        <actions>
            <name>This_notification_is_sent_to_EMCSocialSales_emc_com_upon_creation_of_a_lead_with</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>eBusiness_RFQ_Lead_Creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Originator__c</field>
            <operation>equals</operation>
            <value>eStore</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Originator_Details__c</field>
            <operation>equals</operation>
            <value>RFQ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.eBus_RFQ_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This notification is sent to the customer email upon creation of a lead with Source=eStore and Source/ Program Detail = RFQ</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>eBusiness RFQPunch Lead Created</fullName>
        <actions>
            <name>This_notification_is_sent_using_the_lead_header_field_eStore_Queue_Email_email_u</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This notification is sent to the sales users upon creation of a lead with Source=eStore and Source/ Program Detail = RFQPunch</description>
        <formula>AND(ISPICKVAL(Lead_Originator__c, &apos;eStore&apos;) ,      ISPICKVAL( Originator_Details__c , &apos;RFQPunch&apos;) ,     NOT(eBus_RFQ_ID__c=null) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>isAccepted</fullName>
        <actions>
            <name>UpdatedDateAccepted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Accept_Lead__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DealReg_Deal_Registration__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This work flow populates Date Accepted field when a Lead is accepted.
Bypassed for Deal Reg</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>isBooked</fullName>
        <actions>
            <name>UpdatedisBooked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Opportunity_Status__c</field>
            <operation>equals</operation>
            <value>Booked</value>
        </criteriaItems>
        <description>Marked as booked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>isClosed</fullName>
        <actions>
            <name>UpdateRecordTypeClosedConverted</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateisClosed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Triggered when lead status = &quot;Closed&quot;, used to update a field to be validated against.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>isClosed OR Converted</fullName>
        <actions>
            <name>PopulateDateClosedConverted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Triggered when status is either &quot;Closed&quot; or &quot;Converted to Opportunity.&quot; Used to populate the &quot;Date Closed/Converted&quot; field.</description>
        <formula>OR(  ISPICKVAL( Status ,&quot;Converted to Opportunity&quot;), ISPICKVAL( Status ,&quot;Closed&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>isConverted</fullName>
        <actions>
            <name>UpdateRecordTypeClosedConverted</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateisConverted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Converted to Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DealReg_Deal_Registration__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Triggered when lead status = &quot;Converted to Opportunity&quot;, used to update a field to be validated against.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
