<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>EBC_Related_Oppty_Forecast_update</fullName>
        <field>Forecasted_Amount_Cur__c</field>
        <formula>Opportunity_Name__r.Amount</formula>
        <name>EBC Related Oppty Forecast update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Related Opportunity Forecast Amount</fullName>
        <actions>
            <name>EBC_Related_Oppty_Forecast_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This WF is used to calculate related opportunity forcast amount , so the same can be used in roll-up summary.</description>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
