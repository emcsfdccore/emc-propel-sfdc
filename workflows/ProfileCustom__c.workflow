<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Default_Owner_Profile</fullName>
        <description>Set Default Owner</description>
        <field>OwnerId</field>
        <lookupValue>defauktownerhidden@emc.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Default Owner Profile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Default Owner Profile</fullName>
        <actions>
            <name>Set_Default_Owner_Profile</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Default Owner</description>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
