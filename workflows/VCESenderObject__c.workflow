<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>VCESenderObjectUpdateToOracle</fullName>
        <apiVersion>8.0</apiVersion>
        <endpointUrl>https://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.sfdcVceSenderObjectUpdate</endpointUrl>
        <fields>Id</fields>
        <fields>IdToSend__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>vce.internal.owner@emc.com</integrationUser>
        <name>VCESenderObjectUpdateToOracle</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>VCESenderObjectUpdateToOracle</fullName>
        <actions>
            <name>VCESenderObjectUpdateToOracle</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
