<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>PreSales_Send_to_RPQ_to_Wiki</fullName>
        <apiVersion>23.0</apiVersion>
        <endpointUrl>https://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.SFRPQWikiService</endpointUrl>
        <fields>Attachment_ID__c</fields>
        <fields>Case__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>rpq.interface@emc.com</integrationUser>
        <name>PreSales Send to RPQ to Wiki</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>PreSales Send RPQ to Wiki</fullName>
        <actions>
            <name>PreSales_Send_to_RPQ_to_Wiki</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RPQ_Wiki_Attachments__c.Send_to_Wiki__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Rule kicks off when RPQ Wiki Attachment record is set to &quot;Send to Wiki&quot;. Kicks off outbound message and integration to RPQ Wiki</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
