<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EBS_Opportunity_Approval_Acknowledgement</fullName>
        <description>EBS Opportunity Approval Acknowledgement</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EBS_Opportunities/EBS_Opportunity_Acknowledgement</template>
    </alerts>
    <alerts>
        <fullName>Email_Approved</fullName>
        <description>Email Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EBS_Opportunities/EBS_Opportunity_Approved</template>
    </alerts>
    <alerts>
        <fullName>Email_Rejected</fullName>
        <description>Email Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EBS_Opportunities/EBS_Opportunity_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Commit_to_Realize</fullName>
        <field>Opportunity_Phase__c</field>
        <literalValue>Value Realized</literalValue>
        <name>Commit to Realize</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Defined_To_Commit</fullName>
        <field>Opportunity_Phase__c</field>
        <literalValue>Value Committed</literalValue>
        <name>Defined To Commit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Defined_to_Realized</fullName>
        <field>Opportunity_Phase__c</field>
        <literalValue>Value Realized</literalValue>
        <name>Defined to Realized</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Stage_Update_to_Closed</fullName>
        <field>Opportunity_Stage__c</field>
        <literalValue>Closed</literalValue>
        <name>Opportunity Stage Update to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Realize_To_Close</fullName>
        <field>Opportunity_Phase__c</field>
        <literalValue>Closed</literalValue>
        <name>Realize To Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Review_and_Prioritize_to_Develop_Busines</fullName>
        <field>Opportunity_Stage__c</field>
        <literalValue>Develop Business Case</literalValue>
        <name>Review and Prioritize to Develop Busines</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_to_Run</fullName>
        <field>Opportunity_Stage__c</field>
        <literalValue>Run Project</literalValue>
        <name>Stage Update to Run Profit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Odds_Percent_Field</fullName>
        <field>Odds_percent__c</field>
        <formula>CASE(Opportunity_Stage__c,&apos;Identify and Qualify Opportunity&apos;, 0.05, &apos;Complete ROM/Assesment&apos;, 0.20, &apos;Review and Prioritize Pipeline&apos;, 0.25, &apos;Develop Business Case&apos;, 0.30, &apos;Obtain Funding Approval&apos; , 0.50, &apos;Run Project&apos; , 0.75 , &apos;Measure Benefits&apos;,0.90,&apos;Opportunity Closed&apos;,1.00, null )</formula>
        <name>Update Odds Percent Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Odds Percent</fullName>
        <actions>
            <name>Update_Odds_Percent_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EBS_Opportunity__c.Override_Odds__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
