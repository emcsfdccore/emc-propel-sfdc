<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_Opportunity_Account_Owner_Current_User</fullName>
        <description>Email to Opportunity Account Owner &amp; Current User</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>AFM__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Registration_ID_Update_Email_MOJO</template>
    </alerts>
    <rules>
        <fullName>Registration_ID_Update</fullName>
        <actions>
            <name>Email_to_Opportunity_Account_Owner_Current_User</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Rules evaluates to true when Registration ID field on GFS Trade-Ins/Competitive Swap object is updated and is not blank</description>
        <formula>NOT( ISBLANK( Registration_ID__c )  ||  ISNULL( Registration_ID__c )  )  &amp;&amp;  ISCHANGED( Registration_ID__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
