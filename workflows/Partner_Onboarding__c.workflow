<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>POB_Step_3_to_6</fullName>
        <field>Step_Name__c</field>
        <formula>&quot;STEP6&quot;</formula>
        <name>POB Step 3 to 6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>POB_status_update</fullName>
        <field>Current_Status_of_Onboarding__c</field>
        <literalValue>In Progress</literalValue>
        <name>POB status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>POB_status_update1</fullName>
        <field>Current_Status_of_Onboarding__c</field>
        <literalValue>In Progress</literalValue>
        <name>POB status update1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Step2_3</fullName>
        <field>Step_Name__c</field>
        <formula>&quot;STEP3&quot;</formula>
        <name>Step2-3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Application_Submitted</fullName>
        <field>Date_Application_Submitted__c</field>
        <formula>CreatedDate</formula>
        <name>Update Date Application Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Onboarding_Completed</fullName>
        <field>Date_Onboarding_Completed__c</field>
        <formula>NOW()</formula>
        <name>Update Date Onboarding Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>step_5_to_6</fullName>
        <field>Step_Name__c</field>
        <formula>&quot;STEP6&quot;</formula>
        <name>step 5 to 6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change deferred to in progress</fullName>
        <actions>
            <name>POB_status_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL( Current_Status_of_Onboarding__c, &apos;Deferred&apos;),  ISCHANGED( Distributor_Approver__c ),  Distributor_Approver__c &lt;&gt; NULL)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Date onboarding completed field update</fullName>
        <actions>
            <name>Update_Date_Onboarding_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Partner_Onboarding__c.Current_Status_of_Onboarding__c</field>
            <operation>equals</operation>
            <value>Partner Onboarded</value>
        </criteriaItems>
        <criteriaItems>
            <field>Partner_Onboarding__c.Current_Status_of_Onboarding__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>POB Step 3 to 6</fullName>
        <actions>
            <name>POB_Step_3_to_6</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Partner_Onboarding__c.Authorized_Reseller_Party_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Partner_Onboarding__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>STEP3</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>POB Step 5 to 6</fullName>
        <actions>
            <name>step_5_to_6</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Partner_Onboarding__c.Account_Created_by_TRACK__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Date Application Submitted</fullName>
        <actions>
            <name>Update_Date_Application_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Partner_Onboarding__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Updation of STEP from 2 to 3</fullName>
        <actions>
            <name>Step2_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Partner_Onboarding__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>STEP2</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
