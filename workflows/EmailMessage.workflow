<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Presales_New_Email_Indicator</fullName>
        <field>New_Email__c</field>
        <literalValue>1</literalValue>
        <name>Presales New Email Indicator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Auto_Reply_Date_to_Today_Email</fullName>
        <field>Case_Auto_Reply_Last_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Auto Reply Date to Today-Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>PreSales New Email Indicator</fullName>
        <actions>
            <name>Presales_New_Email_Indicator</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WFR for Auto Reply on Closed Case</fullName>
        <actions>
            <name>Update_Auto_Reply_Date_to_Today_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(     CONTAINS(TEXT(Parent.Status),&apos;Closed&apos;),      OR(ISNULL( Parent.Case_Auto_Reply_Last_Date__c ),  Parent.Case_Auto_Reply_Last_Date__c &lt;&gt; TODAY()),      OR(Parent.Is_contact__c = true, Parent.Is_not_User_Contact__c = true, AND(Parent.Is_contact__c = False, Parent.PreSales_Case_Close_AutoReply__c = true) ),     OR( Parent.RecordType.Name = &apos;Global Revenue Operations&apos;, Parent.RecordType.Name = &apos;Credit &amp; Collections&apos;, Parent.RecordType.Name = &apos;TRACK - Customer Master&apos;, Parent.RecordType.Name = &apos;TRACK - Resource Hierarchy&apos;, Parent.RecordType.Name = &apos;TRACK - Territory Assignment&apos;, Parent.RecordType.Name = &apos;Asset Based Selling Support&apos;, Parent.RecordType.Name = &apos;Maintenance Contract Operations&apos;, Parent.RecordType.Name ==&apos;Accounts Payable&apos;, Parent.RecordType.Name = &apos;Install Base Group&apos;,Parent.RecordType.Name == &apos;Renewals Quote Request&apos;,Parent.RecordType.Name == &apos;Renewals Existing Quote Rework&apos;,Parent.RecordType.Name == &apos;Renewals Policy Variation Request (PVR)&apos;)     )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
