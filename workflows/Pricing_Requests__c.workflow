<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>APJ_ANZTier_1_Submission_mail</fullName>
        <description>APJ ANZTier 1 Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>ANZ_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>APJ_GC_Tier_1_Submission_mail</fullName>
        <description>APJ GC Tier 1 Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>GC_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>APJ_India_Tier_1_Submission_mail</fullName>
        <description>APJ India Tier 1 Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>India_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>APJ_JapanTier_1_Submission_mail</fullName>
        <description>APJ JapanTier 1 Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>Japan_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>APJ_Korea_Tier_1_Submission_mail</fullName>
        <description>APJ Korea Tier 1 Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>Korea_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>APJ_SEA_Tier_1_Submission_mail</fullName>
        <description>APJ SEA Tier 1 Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>SEA_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>APJ_Uncategorized_Tier_1_Submission_mail</fullName>
        <description>APJ Uncategorized Tier 1 Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>APJ_Uncategorized</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>EMEA_France_Tier_1_Submission_mail</fullName>
        <description>EMEA France Tier 1 Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>France_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>EMEA_PSC_Submission_mail</fullName>
        <description>EMEA PSC Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEA_PSC_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Russia_Submission_mail</fullName>
        <description>EMEA Russia Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>Russia_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>EMEA_South_Tier_1_Submission_mail</fullName>
        <description>EMEA South Tier 1 Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>Europe_South_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>EMEA_TEEAM_Submission_mail</fullName>
        <description>EMEA TEEAM Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>TEEAM_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>EMEA_UKI_Submission_mail</fullName>
        <description>EMEA UKI Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>UKI_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Uncategorize_Submission_mail</fullName>
        <description>EMEA Uncategorize Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEA_Uncategorized</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>EMEA_germany_Tier_1_Submission_mail</fullName>
        <description>EMEA germany Tier 1 Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>Germany_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>Europe_North_Tier_1_Submission_mail</fullName>
        <description>Europe North Tier 1 Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>Europe_North_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <alerts>
        <fullName>PAJ_PSC_Submission_mail</fullName>
        <description>PAJ PSC Submission mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>APJ_PSC_Tier_1_Pricing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Transformation_Pricing_Request/Transformation_PAR_Submission_C7</template>
    </alerts>
    <fieldUpdates>
        <fullName>AMER_Reject_Status</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>AMER Reject Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AMER_Status_Update</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>AMER Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ANZ_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>ANZ Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ANZ_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>ANZ Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>APJ</fullName>
        <field>OwnerId</field>
        <lookupValue>APJ_Tier1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>APJ</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_Approve_Status</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>EMEA Approve Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_FranceTier_1_Final_approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>EMEA FranceTier 1 Final approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_France_Tier_1_Final_Rejection</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>EMEA France Tier 1 Final Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_France_Tier_2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>EMEA France Tier 2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_Germany_Tier_1_Final_approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>EMEA Germany Tier 1 Final approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_Germany_Tier_1_Final_rejection</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>EMEA Germany Tier 1 Final rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_Germany_Tier_2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>EMEA Germany Tier 2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_North_Tier_1_Final_Rejection</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>EMEA North Tier 1 Final Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_North_Tier_1_Final_approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>EMEA North Tier 1 Final approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_North_Tier_2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>EMEA North Tier 2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_Reject_Status</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>EMEA Reject Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_RussiaFinal_approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>EMEA RussiaFinal approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_Russia_Final_Rejection</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>EMEA Russia Final Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_South_Final_Rejection</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>EMEA South Final Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_South_Tier_Final_approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>EMEA South Tier Final approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_TEEAM_Final_Rejection</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>EMEA TEEAM Final Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_TEEAM_Final_approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>EMEA TEEAM Final approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_UKI_Final_Rejection</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>EMEA UKI Final Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_UKI_Final_approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>EMEA UKI Final approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_Uncategorized_Final_Rejectio</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>EMEA Uncategorized Final Rejectio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_Uncategorized_Final_approva</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>EMEA Uncategorized Final approva</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_UncategoryTier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>EMEA UncategoryTier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_Uncategory_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>EMEA Uncategory Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_APJ_ANZ</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval APJ ANZ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_APJ_GC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval APJ GC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_APJ_India</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval APJ India</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_APJ_Japan</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval APJ Japan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_APJ_Korea</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval APJ Korea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_APJ_PSC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval APJ PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_APJ_SEA</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval APJ SEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_APJ_Uncategorized</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval APJ Uncategorized</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_EMEA_PSC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval EMEA PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Rejection</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Final Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Rejection_APJ_ANZ</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Final Rejection APJ ANZ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Rejection_APJ_GC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Final Rejection APJ GC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Rejection_APJ_India</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Final Rejection APJ India</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Rejection_APJ_Japan</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Final Rejection APJ Japan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Rejection_APJ_Korea</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Final Rejection APJ Korea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Rejection_APJ_PSC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Final Rejection APJ PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Rejection_APJ_SEA</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Final Rejection APJ SEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Rejection_APJ_Uncategorized</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Final Rejection APJ Uncategorized</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Rejection_EMEA_PSC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Final Rejection EMEA PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>France_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>France Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>France_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>France Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GC_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>GC Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GC_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>GC Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Germany_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>Germany Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Germany_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>Germany Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>India_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>India Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>India_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>India Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Japan_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>Japan Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Japan_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>Japan Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Korea_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>Korea Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Korea_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>Korea Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>North_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>North Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>North_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>North Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_APJ_ANZ</fullName>
        <field>OwnerId</field>
        <lookupValue>APJ_ANZ_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update APJ ANZ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_APJ_GC</fullName>
        <field>OwnerId</field>
        <lookupValue>APJ_GC_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update APJ GC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_APJ_India</fullName>
        <field>OwnerId</field>
        <lookupValue>APJ_India_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update APJ India</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_APJ_Japan</fullName>
        <field>OwnerId</field>
        <lookupValue>APJ_Japan_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update APJ Japan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_APJ_Korea</fullName>
        <field>OwnerId</field>
        <lookupValue>APJ_Korea_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update APJ Korea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_APJ_PSC</fullName>
        <field>OwnerId</field>
        <lookupValue>APJ_PSC_Tier_1_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update APJ PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_APJ_SEA</fullName>
        <field>OwnerId</field>
        <lookupValue>APJ_SEA_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update APJ SEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_APJ_Uncategorized</fullName>
        <field>OwnerId</field>
        <lookupValue>APJ_Uncategorized_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update APJ Uncategorized</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_Americas</fullName>
        <field>OwnerId</field>
        <lookupValue>Americas_Tier1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update Americas</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_EMEA</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_Tier1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update EMEA</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_EMEA_France</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_France_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update EMEA France</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_EMEA_Germany</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_Germany_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update EMEA Germany</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_EMEA_North</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_West_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update EMEA North</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_EMEA_PSC</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_PSC_Tier_1_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update EMEA PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_EMEA_Russia</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_Russia_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update EMEA Russia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_EMEA_South</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_West_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update EMEA South</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_EMEA_TEEAM</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_TEEAM_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update EMEA TEEAM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_EMEA_UKI</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_UKI_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update EMEA UKI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Update_EMEA_Uncategorized</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_Uncategorized_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Update EMEA Uncategorized</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_APJ_ANZ</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>TODAY()</formula>
        <name>PAR Submitted date for APJ ANZ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_APJ_GC</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>Today()</formula>
        <name>PAR Submitted date for APJ GC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_APJ_India</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>TODAY()</formula>
        <name>PAR Submitted date for APJ India</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_APJ_Japan</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>TODAY()</formula>
        <name>PAR Submitted date for APJ Japan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_APJ_Korea</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>TODAY()</formula>
        <name>PAR Submitted date for APJ Korea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_APJ_PSC</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>TODAY()</formula>
        <name>PAR Submitted date for APJ PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_APJ_SEA</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>TODAY()</formula>
        <name>PAR Submitted date for APJ SEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_APJ_Uncategorized</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>TODAY()</formula>
        <name>PAR Submitted date for APJ Uncategorized</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_Americas</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>TODAY()</formula>
        <name>PAR Submitted date for Americas</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_EMEA_France</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>Today()</formula>
        <name>PAR Submitted date for EMEA France</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_EMEA_Germany</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>Today()</formula>
        <name>PAR Submitted date for EMEA Germany</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_EMEA_North</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>Today()</formula>
        <name>PAR Submitted date for EMEA North</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_EMEA_PSC</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>Today()</formula>
        <name>PAR Submitted date for EMEA PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_EMEA_Russia</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>Today()</formula>
        <name>PAR Submitted date for EMEA Russia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_EMEA_South</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>Today()</formula>
        <name>PAR Submitted date for EMEA South</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_EMEA_TEEAM</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>Today()</formula>
        <name>PAR Submitted date for EMEA TEEAM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_EMEA_UK</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>Today()</formula>
        <name>PAR Submitted date for EMEA UK</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAR_Submitted_date_for_EMEA_Uncategoriz</fullName>
        <field>PAR_Submitted_Date__c</field>
        <formula>TODAY()</formula>
        <name>PAR Submitted date for EMEA Uncategoriz</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_ANZ_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall ANZ flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_APJ_PSC_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall APJ PSC flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_EMEA_PSC_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall EMEA PSC flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_EMEA_UC_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall EMEA UC flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Field_Americas_initial</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall Field Americas initial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_France_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall France flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_GC_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall GC flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Germany_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall Germany flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_India_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall India flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Japan_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall Japan flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Korea_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall Korea flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_North_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall North flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Russia_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall Russia flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_SEA_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall SEA flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_South_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall South flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Status_Americas</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recall Status Americas</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Status_EMEA_PSC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recall Status EMEA_PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Status_SEA</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recall Status SEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_TEEM_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall TEEM flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_UC_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall UC flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_UKI_flag</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Recall UKI flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_ANZ</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field ANZ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_APJ_PSC</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field APJ PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_Americas</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field Americas</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_EMEA_PSC</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field EMEA PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_EMEA_UC</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field EMEA UC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_Europe_North</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field Europe North</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_Europe_South</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field Europe South</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_France</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field France</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_GC</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field GC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_Germany</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field Germany</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_India</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field India</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_Japan</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field Japan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_Korea</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field Korea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_Russia</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field Russia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_SEA</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field SEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_Status_ANZ</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled Field Status ANZ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_Status_GC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled Field Status GC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_Status_Japan</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled Field Status Japan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_Status_Korea</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled Field Status Korea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_TEEAM</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field TEEAM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_UKI</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field UKI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Field_Uncategorized</fullName>
        <field>Recalled_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Recalled Field Uncategorized</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Status_UC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled Status UC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_Status_field_EMEA_Germany</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled Status field EMEA Germany</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_field</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_field_EMEA_France</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled field EMEA France</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_field_EMEA_North</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled field EMEA North</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_field_EMEA_TEEAM</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled field EMEA TEEAM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_field_EMEA_UKI</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled field EMEA UKI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_field_Status_EMEA_Russia</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled field Status EMEA Russia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_field_Status_EMEA_South</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled field Status EMEA South</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_field_Status_EMEA_Uncategorized</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled field Status EMEA Uncategorized</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_field_status_update_APJ_PSC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>New</literalValue>
        <name>Recalled field status update APJ PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Russia_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>Russia Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Russia_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>Russia Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SEA_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>SEA Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SEA_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>SEA Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>South_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>South Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>South_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>South Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_APJ_GC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update APJ GC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_APJ_India</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update APJ India</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_APJ_Japan</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update APJ Japan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_APJ_Korea</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update APJ Korea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_APJ_PSC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update APJ PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_APJ_SEA</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update APJ SEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_APJ_Uncategorized</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update APJ Uncategorized</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_EMEA_France</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update EMEA France</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_EMEA_Germany</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update EMEA Germany</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_EMEA_North</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update EMEA North</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_EMEA_PSC</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update EMEA PSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_EMEA_Russia</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update EMEA Russia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_EMEA_South</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update EMEA South</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_EMEA_TEEAM</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update EMEA TEEAM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_EMEA_UKI</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update EMEA UKI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_EMEA_Uncategorized</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Status Update EMEA Uncategorized</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submission_Update</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Submission Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submission_Update_APJ_ANZ</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Submission Update APJ ANZ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submission_Update_EMEA</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Submission Update EMEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submission_update_Americas</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Submission update Americas</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TEEAM_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>TEEAM Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TEEAM_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>TEEAM Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier2AccessEMEA</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier2AccessEMEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier2_AccessAPJ</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier2 AccessAPJ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier2_Access_EMEA_Russia</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier2 Access EMEA Russia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier2_Access_EMEA_South</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier2 Access EMEA South</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier2_Access_EMEA_TEEAM</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier2 Access EMEA TEEAM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier2_Access_EMEA_UKI</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier2 Access EMEA UKI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier2_Access_EMEA_Uncategorized</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier2 Access EMEA Uncategorized</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier_2_Access_APJ_ANZ</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier 2 Access APJ ANZ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier_2_Access_APJ_GC</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier 2 Access APJ GC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier_2_Access_APJ_India</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier 2 Access APJ India</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier_2_Access_APJ_Japan</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier 2 Access APJ Japan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier_2_Access_APJ_Korea</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier 2 Access APJ Korea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier_2_Access_APJ_SEA</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier 2 Access APJ SEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier_2_Access_APJ_Uncategorized</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>1</literalValue>
        <name>Tier 2 Access APJ Uncategorized</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UC_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>UC Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UC_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>UC Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UKI_Escalate_Flag</fullName>
        <field>APJ_Request_Escalation__c</field>
        <literalValue>0</literalValue>
        <name>UKI Escalate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UKI_Tier2_Access</fullName>
        <field>Tier2_Access__c</field>
        <literalValue>0</literalValue>
        <name>UKI Tier2 Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Trigger_OB_flag</fullName>
        <description>PROPEL- after triggering OB message reset Trigger_OB_flag field value</description>
        <field>Trigger_OB_flag__c</field>
        <literalValue>0</literalValue>
        <name>Update Trigger_OB_flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>PROPEL_AIC_OB_PAR_Details</fullName>
        <apiVersion>31.0</apiVersion>
        <endpointUrl>http://soagateway-test.emc.com/sst/runtime.asvc/com.actional.intermediary.aicNotifyParDataServiceEMCSFDC_DES?WSDL</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>kothawade_prasad@emc.com.prod</integrationUser>
        <name>PROPEL AIC OB PAR Details</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>PROPEL_AIC New PAR Created or Updated</fullName>
        <actions>
            <name>Update_Trigger_OB_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PROPEL_AIC_OB_PAR_Details</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>A new PAR is created and associated with a Quote and PAR Status change.</description>
        <formula>OR(AND( ISNEW(),NOT(ISBLANK(Opportunity_Name__c)),NOT(ISBLANK(Primary_Deal_Quote_Number__c))), 	   Trigger_OB_flag__c=true,    AND(ISCHANGED( Approval_Status__c ) ,NOT(ISBLANK(Opportunity_Name__c)),NOT(ISBLANK(Primary_Deal_Quote_Number__c)),  											    NOT( ISPICKVAL(Opportunity_Name__r.StageName, &apos;Closed&apos;)), 												 NOT( ISPICKVAL(Opportunity_Name__r.StageName, &apos;Submitted&apos;)), 												 NOT( ISPICKVAL(Opportunity_Name__r.StageName, &apos;Booked&apos;)), 												 NOT( ISPICKVAL(Opportunity_Name__r.StageName, &apos;Eval&apos;)) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
