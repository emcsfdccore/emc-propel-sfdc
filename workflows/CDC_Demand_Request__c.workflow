<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CDC_DMT_Send_Email_on_Toolkit_Related_Demand_Request</fullName>
        <description>CDC DMT - Send Email on Toolkit Related Demand Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>CDC_Toolkit_Support_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CDC_DMT_Email_Notifications/CDC_DMT_Email_to_James_on_Toolkit_Request</template>
    </alerts>
    <alerts>
        <fullName>CDC_DMT_Send_Requester_Email_at_CI_Scope_Lock</fullName>
        <description>CDC DMT - Send Requester Email at CI Scope Lock</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>emcsfdcalert@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CDC_DMT_Email_Notifications/CDC_DMT_Email_to_Requester_for_CI_Scope_Lock</template>
    </alerts>
    <alerts>
        <fullName>CDC_DMT_Send_Requester_Email_at_when_Submit_for_Review_is_Clicked</fullName>
        <description>CDC DMT - Send Requester Email at when Submit for Review is Clicked</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CDC_DMT_Email_Notifications/CDC_DMT_Email_to_Requester_on_Submit_for_Review</template>
    </alerts>
    <alerts>
        <fullName>CDC_DMT_Send_email_to_BA_on_BA_Assignment</fullName>
        <description>CDC DMT - Send email to BA on BA Assignment</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Business_Architect__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>emcsfdcalert@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CDC_DMT_Email_Notifications/CDC_DMT_Email_to_BA_on_BA_Assignment</template>
    </alerts>
    <alerts>
        <fullName>CDC_DMT_Send_email_to_Requester_on_BA_Assignment</fullName>
        <description>CDC DMT - Send email to Requester on BA Assignment</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>emcsfdcalert@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CDC_DMT_Email_Notifications/CDC_DMT_Email_to_Requester_on_BA_Assignment</template>
    </alerts>
    <alerts>
        <fullName>CDC_Federal_Impact_checked</fullName>
        <description>CDC Demand Request Federal Impact Checked</description>
        <protected>false</protected>
        <recipients>
            <recipient>CDC_Federal_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CDC_DMT_Email_Notifications/CDC_Federal_Impact_checked</template>
    </alerts>
    <fieldUpdates>
        <fullName>CDC_DMT_Change_Owner_to_On_Hold_Queue</fullName>
        <description>Sets Owner to On Hold Queue when Status = On Hold</description>
        <field>OwnerId</field>
        <lookupValue>CDC_On_Hold_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CDC DMT - Change Owner to On Hold Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CDC_DMT_Change_Owner_to_Rejected_Queue</fullName>
        <description>Sets Queue to Rejected Queue when Status = Rejected</description>
        <field>OwnerId</field>
        <lookupValue>CDC_Rejected_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CDC DMT - Change Owner to Rejected Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CDC_DMT_Change_Owner_to_Submission_Que</fullName>
        <description>TOOLKIT - Sets Demand Request owner to CDC Submission Queue</description>
        <field>OwnerId</field>
        <lookupValue>CDC_Submission_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CDC DMT - Change Owner to Submission Que</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CDC_DMT_Change_Owner_to_Toolkit_Suppor</fullName>
        <description>TOOLKIT - Assigns the OWNER field to the CDC Toolkit Support Queue</description>
        <field>OwnerId</field>
        <lookupValue>CDC_Toolkit_Support_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CDC DMT - Change Owner to Toolkit Suppor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CDC_DMT_Update_Ready_for_Development</fullName>
        <description>Added 4/20/2015 for Request 2007</description>
        <field>Ready_for_Development_Date__c</field>
        <formula>Today()</formula>
        <name>CDC DMT - Update Ready for Development</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CDC_DMT_Update_Ready_for_Review_Date</fullName>
        <description>Added 4/20/2015 for Request 2007</description>
        <field>Ready_For_Review_Date__c</field>
        <formula>TODAY()</formula>
        <name>CDC DMT - Update Ready for Review Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CDC DMT - Ready for Development</fullName>
        <actions>
            <name>CDC_DMT_Update_Ready_for_Development</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CDC_Demand_Request__c.OwnerId</field>
            <operation>equals</operation>
            <value>CDC CI Node</value>
        </criteriaItems>
        <description>Added 4/20/2015 for Request 2007</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CDC DMT - Ready for Review</fullName>
        <actions>
            <name>CDC_DMT_Update_Ready_for_Review_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CDC_Demand_Request__c.Ready_for_Review__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Added 4/20/2015 for Request 2007</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CDC DMT - Send Email On Toolkit Request</fullName>
        <actions>
            <name>CDC_DMT_Send_Email_on_Toolkit_Related_Demand_Request</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CDC_DMT_Change_Owner_to_Toolkit_Suppor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CDC_Demand_Request__c.Platforms_Impacted__c</field>
            <operation>includes</operation>
            <value>Salesforce.com - CDC Demand Toolkit</value>
        </criteriaItems>
        <description>TOOLKIT - Sends an email to a designated contact when a request is for the CDC Demand &amp; Delivery Toolkit</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CDC DMT - Send Email at CI Scope Lock</fullName>
        <actions>
            <name>CDC_DMT_Send_Requester_Email_at_CI_Scope_Lock</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>TOOLKIT: Sends email to requester when request is scheduled for release and scope lock for CI is reached</description>
        <formula>AND( 
NOT( ISBLANK( Planned_Release__c ) ), 
ISPICKVAL(Status__c, &quot;8 - CI Scope Lock (CI only)&quot;), 
RecordType.DeveloperName = &quot;CI_Work_Type&quot; 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CDC DMT - Send Email at Submit for Review</fullName>
        <actions>
            <name>CDC_DMT_Send_Requester_Email_at_when_Submit_for_Review_is_Clicked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CDC_Demand_Request__c.Ready_for_Review__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>TOOLKIT: Sends email to requester when the Submit for Review Button is clicked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CDC DMT - Send Email when BA is Assigned</fullName>
        <actions>
            <name>CDC_DMT_Send_email_to_BA_on_BA_Assignment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CDC_DMT_Send_email_to_Requester_on_BA_Assignment</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>TOOLKIT: Sends email to requester and Business Architect when Architect is Assigned to any demand request</description>
        <formula>ISCHANGED( Primary_Business_Architect__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CDC DMT - Set Owner to On Hold Queue</fullName>
        <actions>
            <name>CDC_DMT_Change_Owner_to_On_Hold_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CDC_Demand_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>N/A - On Hold</value>
        </criteriaItems>
        <description>TOOLKIT - Sets the record owner to the CDC On Hold Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CDC DMT - Set Owner to Queue on Create</fullName>
        <actions>
            <name>CDC_DMT_Change_Owner_to_Submission_Que</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CDC_Demand_Request__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>TOOLKIT - Sets the record owner to the CDC Demand Submission Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CDC DMT - Set Owner to Rejected Queue</fullName>
        <actions>
            <name>CDC_DMT_Change_Owner_to_Rejected_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CDC_Demand_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>N/A - Rejected</value>
        </criteriaItems>
        <description>TOOLKIT - Sets the record owner to the CDC Rejected Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CDC Demand Request Federal Impact</fullName>
        <actions>
            <name>CDC_Federal_Impact_checked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>OR( Federal_Impact__c = True , INCLUDES( Platforms_Impacted__c, &apos;Salesforce.com - Federal&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
