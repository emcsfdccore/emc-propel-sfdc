<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_for_System_Admin_Access_Approval</fullName>
        <ccEmails>anand.sharma2@emc.com</ccEmails>
        <description>Notification for System Admin Access Approval</description>
        <protected>false</protected>
        <recipients>
            <field>User_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>System_Admin_Utility/Notification_for_System_Admin_User_Approval</template>
    </alerts>
    <alerts>
        <fullName>Notification_for_System_Admin_Access_Request</fullName>
        <ccEmails>Anand.sharma2@emc.com</ccEmails>
        <description>Notification for System Admin Access Request</description>
        <protected>false</protected>
        <recipients>
            <field>User_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>System_Admin_Utility/Notification_for_System_Admin_User_Request</template>
    </alerts>
    <rules>
        <fullName>System Admin Approval Notification</fullName>
        <actions>
            <name>Notification_for_System_Admin_Access_Approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( Is_a_Temp_System_Admin__c)  , Is_a_Temp_System_Admin__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>System Admin Request Notification</fullName>
        <actions>
            <name>Notification_for_System_Admin_Access_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( Temp_End_Date_Time__c)  ,  ISBLANK(Temp_End_Date_Time__c), NOT(Is_a_Temp_System_Admin__c)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
