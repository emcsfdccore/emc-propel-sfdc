<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateTROwnerCreditCollectionQueue</fullName>
        <field>OwnerId</field>
        <lookupValue>Credit_Collections</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>UpdateTROwnerCreditCollectionQueue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateTRRecTypeToReadOnly</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Transaction_Register_Read_Only</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>UpdateTRRecTypeToReadOnly</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TROwnerCreditCollection</fullName>
        <actions>
            <name>UpdateTROwnerCreditCollectionQueue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Transaction_Register__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Transaction Register Read</fullName>
        <actions>
            <name>UpdateTRRecTypeToReadOnly</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Transaction_Register__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
