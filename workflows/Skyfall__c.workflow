<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Request_more_info_when_record_is_created</fullName>
        <description>Request more info when record is created</description>
        <protected>false</protected>
        <recipients>
            <recipient>CDC_Innovation</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <field>AkashicRecord__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SkyFall_Creation_text</template>
    </alerts>
    <rules>
        <fullName>Skyfall Creation</fullName>
        <actions>
            <name>Request_more_info_when_record_is_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
