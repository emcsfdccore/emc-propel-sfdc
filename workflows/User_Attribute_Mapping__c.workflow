<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>APJ_ANZ_Notification_Email_Alert</fullName>
        <description>APJ ANZ Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>APJANZUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>APJ_Greater_China_Notification_Email_Alert</fullName>
        <description>APJ Greater China Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>APJGreaterChinaUserAttributeNotific</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>APJ_IS_Notification_Email_Alert</fullName>
        <description>APJ IS Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>APJISUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>APJ_India_Notification_Email_Alert</fullName>
        <description>APJ India Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>APJIndiaUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>APJ_Japan_Notification_Email_Alert</fullName>
        <description>APJ Japan Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>APJJapanUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>APJ_Korea_Notification_Email_Alert</fullName>
        <description>APJ Korea Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>APJKoreaUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>APJ_South_East_Asia_Notification_Email_Alert</fullName>
        <description>APJ South East Asia Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>APJSouthEastAsiaUserAttributeNotif</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>APJ_TS_Notification_Email_Alert</fullName>
        <description>APJ TS Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>APJTSUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>CMA_Role_Notification</fullName>
        <description>CMA Role Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>dumlao_casey@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fairchild_larry@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>Comm_US_Notification</fullName>
        <description>Comm US Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>CommUSUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_East_Notification_Email_Alert</fullName>
        <description>EMEA East  Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEAEastUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_France_Notification_Email_Alert</fullName>
        <description>EMEA France Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEAFranceUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Germany_Notification_Email_Alert</fullName>
        <description>EMEA Germany Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEAGermanyUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Italy_Notification_Email_Alert</fullName>
        <description>EMEA Italy Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEAItalyUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_North_Notification_Email_Alert</fullName>
        <description>EMEA North Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEANorthUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Notification</fullName>
        <description>EMEA Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>haberlin_eoin2@emc.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>prendergast_david@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_STA_Notification_Email_Alert</fullName>
        <description>EMEA STA Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEASTAUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_South_Notification_Email_Alert</fullName>
        <description>EMEA South Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEASouthUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_TS_Notification_Email_Alert</fullName>
        <description>EMEA TS Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEATSUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_UKI_Notification_Email_Alert</fullName>
        <description>EMEA UKI Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEAUKIUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>LATAM_Notification_Email_Alert</fullName>
        <description>LATAM Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>LATAMUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>NA_Central_Notification_Email_Alert</fullName>
        <description>NA Central Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>NACentralUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>NA_Federal_Notification_Email_Alert</fullName>
        <description>NA Federal Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>NAFederalUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>NA_IS_NA_SMB_Notification_Email_Alert</fullName>
        <description>NA IS/NA SMB Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>NAISUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>NA_IS_Notification</fullName>
        <description>NA IS Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>bourque_brian@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>NA_Mid_Atlantic_South_Notification_Email_Alert</fullName>
        <description>NA Mid-Atlantic/South Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>NAMidAtlanticSouthUserAttributeNot</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>NA_NE_Canada_Notification_Email_Alert</fullName>
        <description>NA NE/Canada Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>NANECanadaUserAttributeNot</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>NA_NY_NJ_Notification_Email_Alert</fullName>
        <description>NA NY/NJ Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>NANYNJUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>NA_TME_Notification_Email_Alert</fullName>
        <description>NA TME Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>NATMEUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>NA_TS_Notification_Email_Alert</fullName>
        <description>NA TS Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>NATSUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>NA_West_Notification_Email_Alert</fullName>
        <description>NA West Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>NAWestUserAttributeNotification</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Role_Attribute_Notification</template>
    </alerts>
    <alerts>
        <fullName>User_Attribute_Mapping_update_alert</fullName>
        <description>User Attribute Mapping update alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>michael.gram@emc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/User_Attributes_Changed</template>
    </alerts>
    <rules>
        <fullName>APJ ANZ Notification</fullName>
        <actions>
            <name>APJ_ANZ_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Comm ANZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Ent ANZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ ANZ</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with APJ ANZ, APJ Ent ANZ, APJ Comm ANZ</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>APJ Greater China Notification</fullName>
        <actions>
            <name>APJ_Greater_China_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4</booleanFilter>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Greater China</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ China</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Ent China</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Comm China</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with APJ Greater China, APJ China, APJ Ent China, APJ Comm China</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>APJ IS Notification</fullName>
        <actions>
            <name>APJ_IS_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9</booleanFilter>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Japan IS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ ANZ IS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Greater China IS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ China IS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Taiwan IS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Hong Kong IS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Korea IS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ South Asia IS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ India IS</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with APJ Japan IS, APJ ANZ IS, APJ Greater China IS, APJ China IS, APJ Taiwan IS, APJ Hong Kong IS, APJ Korea IS, APJ South Asia IS, APJ India IS</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>APJ India Notification</fullName>
        <actions>
            <name>APJ_India_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ India</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Ent India</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Comm India</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with APJ India, APJ Ent India, APJ Comm India</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>APJ Japan Notification</fullName>
        <actions>
            <name>APJ_Japan_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Japan</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Ent Japan</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Comm Japan</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with APJ Japan, APJ Ent Japan, APJ Comm Japan</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>APJ Korea Notification</fullName>
        <actions>
            <name>APJ_Korea_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Korea</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Ent Korea</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Comm Korea</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with APJ Korea, APJ Ent Korea, APJ Comm Korea</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>APJ South East Asia Notification</fullName>
        <actions>
            <name>APJ_South_East_Asia_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13</booleanFilter>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ South East Asia</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Indonesia</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Comm Indonesia</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Ent Indonesia</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Malaysia</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Comm Vietnam</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Philippines</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Comm Philippines</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Singapore</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Comm Singapore</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Ent Singapore</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Thailand</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ Comm Thailand</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with APJ SE Asia,APJ Indonesia,APJ Comm Indonesia,APJ Ent Indonesia,APJ Mlysia, APJ Com Vtnm,APJ Philippines,APJ Com Philippines,APJ Sngpre,APJ Com Sngapre, APJ Ent Sngapre,APJ Thailand,APJ Comm Thailand</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>APJ TS Notification</fullName>
        <actions>
            <name>APJ_TS_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>APJ TS</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with APJ TS</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CMA Notification</fullName>
        <actions>
            <name>CMA_Role_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>CMA</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with CMA</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Comm US Notification</fullName>
        <actions>
            <name>Comm_US_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>Comm US</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with Comm US</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA East Notification</fullName>
        <actions>
            <name>EMEA_East_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>EMEA East</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with EMEA East</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA France Notification</fullName>
        <actions>
            <name>EMEA_France_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>EMEA France</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with EMEA France</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA Germany Notification</fullName>
        <actions>
            <name>EMEA_Germany_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>EMEA Germany</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with EMEA Germany</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA Italy Notification</fullName>
        <actions>
            <name>EMEA_Italy_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>EMEA Italy</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with EMEA Italy</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA North Notification</fullName>
        <actions>
            <name>EMEA_North_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>EMEA North</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with EMEA North</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA Notification</fullName>
        <actions>
            <name>EMEA_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>EMEA</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with EMEA</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA STA Notification</fullName>
        <actions>
            <name>EMEA_STA_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>EMEA STA</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with EMEA STA</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA South Notification</fullName>
        <actions>
            <name>EMEA_South_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>EMEA South</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with EMEA South</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA TS Notification</fullName>
        <actions>
            <name>EMEA_TS_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>EMEA TS</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with EMEA TS</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA UKI Notification</fullName>
        <actions>
            <name>EMEA_UKI_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>EMEA UKI</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with EMEA UKI</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LATAM Notification</fullName>
        <actions>
            <name>LATAM_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>LATAM</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with LATAM</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NA Central Notification</fullName>
        <actions>
            <name>NA_Central_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA Central</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with NA Central</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NA Federal Notification</fullName>
        <actions>
            <name>NA_Federal_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA Federal</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with NA Federal</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NA IS Notification</fullName>
        <actions>
            <name>NA_IS_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA IS</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with NA IS</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NA IS%2FNA SMB Notification</fullName>
        <actions>
            <name>NA_IS_NA_SMB_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA IS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA SMB</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with NA IS,NA SMB</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NA Mid-Atlantic%2FSouth Notification</fullName>
        <actions>
            <name>NA_Mid_Atlantic_South_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA Mid-Atlantic/South</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA Mid Atlantic/South</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with NA Mid-Atlantic/South, NA Mid Atlantic/South</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NA NE%2FCanada Notification</fullName>
        <actions>
            <name>NA_NE_Canada_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA New England/Canada</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA-NE/Canada</value>
        </criteriaItems>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA NE/Canada</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with NA New England/Canada, NA-NE/Canada, NA NE/Canada</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NA NY%2FNJ Notification</fullName>
        <actions>
            <name>NA_NY_NJ_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA NY/NJ</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with NA NY/NJ</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NA TME Notification</fullName>
        <actions>
            <name>NA_TME_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA TME</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with NA TME</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NA TS Notification</fullName>
        <actions>
            <name>NA_TS_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA TS</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with NA TS</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NA West Notification</fullName>
        <actions>
            <name>NA_West_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_Attribute_Mapping__c.Name</field>
            <operation>startsWith</operation>
            <value>NA West</value>
        </criteriaItems>
        <description>Send Email to Notify for creation of Role Starts with NA West</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify User Attribute Mapping record update</fullName>
        <actions>
            <name>User_Attribute_Mapping_update_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>WR-166058</description>
        <formula>NOT( ISNEW() )  &amp;&amp;  OR(  ISCHANGED(Area__c),   ISCHANGED(BU__c),   ISCHANGED(Country__c),   ISCHANGED(District__c),   ISCHANGED(	Division__c),   ISCHANGED(Forecast_Group__c),   ISCHANGED(Region__c),   ISCHANGED(Role_Id__c),   ISCHANGED(Sales_Role__c),  ISCHANGED(Super_Area__c), ISCHANGED(	Super_Division__c), ISCHANGED(Theater__c),ISCHANGED(Name) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
