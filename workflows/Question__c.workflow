<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Question_Notifcation</fullName>
        <description>Question Notifcation</description>
        <protected>false</protected>
        <recipients>
            <field>Assign_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Qujestion_Notification</template>
    </alerts>
    <rules>
        <fullName>Question Notification</fullName>
        <actions>
            <name>Question_Notifcation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Question Notification</description>
        <formula>ISCHANGED(  Assign_To__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
