<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contract_Expiration_Notification_Email</fullName>
        <description>Contract Expiration Notification Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Expiration</template>
    </alerts>
    <rules>
        <fullName>Contract Expiration Notification</fullName>
        <active>true</active>
        <description>This workflow sends an email to the Contract Owner before contract expires.</description>
        <formula>NOT(ISBLANK(EndDate))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_Expiration_Notification_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.Notification_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
