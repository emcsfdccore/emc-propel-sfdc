<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Forecast_Updated</fullName>
        <field>Forecast_Updated__c</field>
        <formula>LastModifiedDate</formula>
        <name>Forecast Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bypass_Field</fullName>
        <field>Bypass_Opportunity_Line_item_Validations__c</field>
        <literalValue>0</literalValue>
        <name>Update Bypass Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Forecast_Amount</fullName>
        <description>Update the Forecast Amount with the Quote Amount</description>
        <field>UnitPrice</field>
        <formula>Quote_Amount__c</formula>
        <name>Update Forecast Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Forecast_Summary_Type</fullName>
        <description>Mechanism for populating the Forecast Summary Type(For Rollup) field so that, in turn, it can be used as filter crtieria for the Forecast Bucket felds on the opportuity object</description>
        <field>Forecast_Summary_Type_For_Rollup__c</field>
        <formula>TEXT(Product2.Forecast_Summary_Type__c)</formula>
        <name>Update Forecast Summary Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Type</fullName>
        <field>Product_Type_Rollup__c</field>
        <formula>TEXT(Product2.Product_Type__c)</formula>
        <name>Update Product Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Reset Bypass Validation Flag</fullName>
        <actions>
            <name>Update_Bypass_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(Opportunity.Update_Forecast_Amount_from_Quote__c =true,true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Forecast Amount</fullName>
        <actions>
            <name>Update_Forecast_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Rule to Update the Forecast Amount based on an update from the Quote.</description>
        <formula>AND( Opportunity.Update_Forecast_Amount_from_Quote__c = TRUE, LEN( Opportunity.Quote_Cart_Number__c)!=0, NOT(ISBLANK(Quote_Amount__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Forecast Summary Type</fullName>
        <actions>
            <name>Update_Forecast_Summary_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Forecast_Summary_Type__c</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Forecast Updated</fullName>
        <actions>
            <name>Forecast_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( UnitPrice )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the Product Type</fullName>
        <actions>
            <name>Update_Product_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Product Type supports validation rules on Opportunity. This rule populates the Product Type to leverage.</description>
        <formula>OR(ISNEW(),  ISCHANGED(Product2Id))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
