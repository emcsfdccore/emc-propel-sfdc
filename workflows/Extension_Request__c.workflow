<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_on_Rejection_of_Extension_Request</fullName>
        <description>Email on Rejection of Extension Request</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Additional_Contact_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Additional_Notification_Email_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Channel_Account_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Oppty_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Deal_reg_Templates/DR_Extension_Request_Reject</template>
    </alerts>
    <fieldUpdates>
        <fullName>Auto_Approved_By_Field_Rep</fullName>
        <field>Approved_By_EMEA_Field_Rep__c</field>
        <literalValue>1</literalValue>
        <name>Auto Approved By Field Rep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ApprovalStatus_Field_to_Submitted</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Submitted By Field Rep</literalValue>
        <name>Update ApprovalStatus Field to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_of_ER_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Approval Status of ER to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Field_Approval_Rejection_DateTime</fullName>
        <field>DealReg_Field_SLA_Expire_on__c</field>
        <formula>NOW()</formula>
        <name>Update Field Approval/Rejection DateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Field_Approval_Rejection_for_ER</fullName>
        <field>Deal_Field_Approval_Rejection_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Field Approval/Rejection for ER</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Field_to_Submitted</fullName>
        <field>Extension_Request_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update Status Field to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved</fullName>
        <field>Extension_Request_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved_By_Field_Rep</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved By Field Rep</literalValue>
        <name>Update Status to Approved By Field Rep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected</fullName>
        <field>Extension_Request_Status__c</field>
        <literalValue>PSC Declined</literalValue>
        <name>Update Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected_By_Rep</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected By Field Rep</literalValue>
        <name>Update Status to Rejected By Rep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Email on Rejection of Extension Request</fullName>
        <actions>
            <name>Email_on_Rejection_of_Extension_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( 
IsRejected__c, 
ISPICKVAL( Extension_Request_Status__c ,&quot;PSC Declined&quot;), 
Len( Deal_Registration__r.Synergy_Opportunity_Number_1__c)&gt; 0 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
