<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_to_Account_Owner_of_all_profiled_accounts_to_build_CAP_for_the_upco</fullName>
        <ccEmails>gowda_ashwini@emc.com</ccEmails>
        <description>Notification to Account Owner of all profiled accounts to build CAP for the upcoming quarter/year.</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Communication_to_the_CAM_at_the_end_of_Q4</template>
    </alerts>
    <fieldUpdates>
        <fullName>ATP_field</fullName>
        <field>ATP_Facilitated_Account__c</field>
        <literalValue>1</literalValue>
        <name>ATP field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_to_Partner_Profile</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Partner_Profile_record_type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change to &apos;Partner Profile Record Type&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_to_Partner_Profile_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Partner_Profile_record_type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change to &apos;Partner Profile Record Type&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_BUU_Flag</fullName>
        <field>Business_Unit_Ultimate__c</field>
        <literalValue>1</literalValue>
        <name>Check BUU Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Channel_Leverage_Account</fullName>
        <field>Channel_Leverage_Account__c</field>
        <literalValue>1</literalValue>
        <name>Check Channel Leverage Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_DU_Flag</fullName>
        <field>Domestic_Ultimate__c</field>
        <literalValue>1</literalValue>
        <name>Check DU Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Dell_Black_Account</fullName>
        <field>Dell_Black_Account__c</field>
        <literalValue>1</literalValue>
        <name>Check Dell Black Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_GU_Flag</fullName>
        <field>Global_Ultimate__c</field>
        <literalValue>1</literalValue>
        <name>Check GU Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Global_Account</fullName>
        <field>Global_Account__c</field>
        <literalValue>1</literalValue>
        <name>Check Global Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_NTApp_100_Account</fullName>
        <field>NTApp_100_Account__c</field>
        <literalValue>1</literalValue>
        <name>Check NTApp 100 Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Core_Quota_Rep_Updated</fullName>
        <field>Core_Quota_Rep_Updated__c</field>
        <formula>TEXT(TODAY())  &amp; &apos; by &apos;&amp;  $User.FirstName &amp; &apos; &apos; &amp;  $User.LastName</formula>
        <name>Core Quota Rep Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Customer_Profiled_Account_Flag_Updated</fullName>
        <field>Customer_Profiled_Account_Flag_Updated__c</field>
        <formula>TEXT(TODAY())  &amp; &apos; by &apos;&amp;  $User.FirstName &amp; &apos; &apos; &amp;  $User.LastName</formula>
        <name>Customer Profiled Account Flag Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Customer_Profiled_Account_Link_Updated</fullName>
        <field>Customer_Profiled_Account_Link_Updated__c</field>
        <formula>TEXT(TODAY())  &amp; &apos; by &apos;&amp;  $User.FirstName &amp; &apos; &apos; &amp;  $User.LastName</formula>
        <name>Customer Profiled Account Link Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_Channel_revenue_Partner_to_Yes</fullName>
        <field>Channel_Revenue_Partner__c</field>
        <literalValue>Yes</literalValue>
        <name>Default Channel revenue Partner to Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>District_Lookup_Updated</fullName>
        <field>District_Lookup_Updated__c</field>
        <formula>TEXT(TODAY())  &amp; &apos; by &apos;&amp;  $User.FirstName &amp; &apos; &apos; &amp;  $User.LastName</formula>
        <name>District Lookup Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flip_Profile_Complete_Flag</fullName>
        <description>Flip Profile Complete Flag</description>
        <field>Profile_Complete__c</field>
        <literalValue>1</literalValue>
        <name>Flip Profile Complete Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Outbound_Message_Flag</fullName>
        <description>Reset the flag to send out bound message.</description>
        <field>SendOutboundNotification__c</field>
        <literalValue>0</literalValue>
        <name>Reset Outbound Message Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Approved_for_Finder</fullName>
        <description>Uncheck Approved for Finder</description>
        <field>Approved_for_Partner_Finder__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Approved for Finder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_BUU_Flag</fullName>
        <field>Business_Unit_Ultimate__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck BUU Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Channel_Leverage_Account</fullName>
        <field>Channel_Leverage_Account__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Channel Leverage Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_DU_Flag</fullName>
        <field>Domestic_Ultimate__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck DU Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Dell_Black</fullName>
        <field>Dell_Black_Account__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Dell Black</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_GU_Flag</fullName>
        <field>Global_Ultimate__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck GU Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Global_Account</fullName>
        <field>Global_Account__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Global Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Lead_Oppty</fullName>
        <field>Lead_Oppty_Enabled__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Lead &amp; Oppty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_NTApp_100_Account</fullName>
        <field>NTApp_100_Account__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck NTApp 100 Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Partner_Complete</fullName>
        <description>Uncheck Partner Complete</description>
        <field>Profile_Complete__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Partner Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateOwnertoEMCDefaultPrivate</fullName>
        <description>Change Owner to EMC Default Owner so that it is not shared with other users.</description>
        <field>OwnerId</field>
        <lookupValue>defauktownerhidden@emc.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Owner to EMC Default (Private)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateOwnertoStandaoneAdminShared</fullName>
        <description>Update Account owner field to Standaone Admin owner so that the Account is shared with all users (via sharing rule).</description>
        <field>OwnerId</field>
        <lookupValue>standaloneadmin@emc.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Owner to Standaone Admin (Shared)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRecordTypePartner</fullName>
        <description>Field Update to change the Account Record Type to Partner Account Record Type.</description>
        <field>RecordTypeId</field>
        <lookupValue>PartnerAccountRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecordType Partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRecordTypeStandard</fullName>
        <description>Field update to change Account Record Type to Standard Account Record Type.</description>
        <field>RecordTypeId</field>
        <lookupValue>StandardAccountRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecordType Standard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_AR_Training_Field_to_False</fullName>
        <field>Authorized_Reseller_Training_Complete__c</field>
        <literalValue>0</literalValue>
        <name>Update AR Training Field to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_AR_Training_Field_to_True</fullName>
        <field>Authorized_Reseller_Training_Complete__c</field>
        <literalValue>1</literalValue>
        <name>Update AR Training Field to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Acc_Rec_Type_NonCustomer_Profiled</fullName>
        <description>Sets the Account Record Type to Customer Non-Profiled Account 
(Account Presentation)</description>
        <field>RecordTypeId</field>
        <lookupValue>Customer_Non_Profiled_Account_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Acc Rec Type-NonCustomer Profiled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Acc_Record_Type_Customer_Account</fullName>
        <description>Sets the Account Record Type to Customer Account 
(Account Presentation)</description>
        <field>RecordTypeId</field>
        <lookupValue>StandardAccountRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Acc Record Type-Customer Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Acc_Record_Type_Customer_Profiled</fullName>
        <description>Sets the Account Record Type to Customer Profiled Account
(Account Presentation)</description>
        <field>RecordTypeId</field>
        <lookupValue>Customer_Profiled_Account_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Acc Record Type-Customer Profiled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Segmentation</fullName>
<<<<<<< HEAD
        <description>Updates with &quot;NOT DEFINED&quot; value</description>
=======
        <description>Default Account Segmentation to NOT DEFINED on account creation  (Support iMAP solution)</description>
>>>>>>> master
        <field>EMC_Classification__c</field>
        <formula>&quot;NOT DEFINED&quot;</formula>
        <name>Update Account Segmentation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Site_Field</fullName>
        <field>Site</field>
        <formula>IF(TEXT( IIG_Account_Flag__c ) == &apos;&apos;, TEXT( Account_Flag__c ), TEXT( IIG_Account_Flag__c ))</formula>
        <name>Update Account Site Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Allow_Lead_Oppty</fullName>
        <field>Lead_Oppty_Enabled__c</field>
        <literalValue>1</literalValue>
        <name>Update Allow Lead &amp; Oppty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approved_for_Partner_Finder</fullName>
        <description>Update Account to be Approved for Finder</description>
        <field>Approved_for_Partner_Finder__c</field>
        <literalValue>1</literalValue>
        <name>Update Approved for Partner Finder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Association_Account</fullName>
        <field>APPR_MTV__Association_Account__c</field>
        <literalValue>1</literalValue>
        <name>Update Association Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
<<<<<<< HEAD
        <fullName>Update_Domestic_DUNS</fullName>
        <description>Default Domestic DUNS on party creation</description>
        <field>Domestic_DUNS_Entity__c</field>
        <formula>Party_Number__c</formula>
        <name>Update Domestic DUNS</name>
=======
        <fullName>Update_Domestic_Duns_Entity</fullName>
        <description>Default Domestic DUNS entity on account creation (iMAP Support)</description>
        <field>Domestic_DUNS_Entity__c</field>
        <formula>Party_Number__c</formula>
        <name>Update Domestic Duns Entity</name>
>>>>>>> master
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_GU_Date_Changed_to_Today</fullName>
        <field>GU_Date_Changed__c</field>
        <formula>TODAY()</formula>
        <name>Update GU Date Changed to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
<<<<<<< HEAD
        <fullName>Update_Global_DUNS</fullName>
        <field>Global_DUNS_Entity__c</field>
        <formula>Party_Number__c</formula>
        <name>Update Global DUNS</name>
=======
        <fullName>Update_Global_Ultimate_Entity</fullName>
        <description>Default Global DUNS entity on account creation (iMAP Support)</description>
        <field>Global_DUNS_Entity__c</field>
        <formula>Party_Number__c</formula>
        <name>Update Global Ultimate Entity</name>
>>>>>>> master
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Disty_Account</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Distributor_Partner_Account_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Disty Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Partial</fullName>
        <field>RecordTypeId</field>
        <lookupValue>T2_Partner_Profile_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Partial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
<<<<<<< HEAD
        <fullName>Update_Site_DUNS</fullName>
        <description>Update site duns with party number</description>
        <field>Site_DUNS_Entity__c</field>
        <formula>Party_Number__c</formula>
        <name>Update Site DUNS</name>
=======
        <fullName>Update_Site_Duns_Entity</fullName>
        <description>Default SITE DUNS to Party Number on account creation  (Support iMAP solution)</description>
        <field>Site_DUNS_Entity__c</field>
        <formula>Party_Number__c</formula>
        <name>Update Site Duns Entity</name>
>>>>>>> master
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Type_to_Partner</fullName>
        <field>Type</field>
        <literalValue>Partner</literalValue>
        <name>Update Type to Partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Distributor_Profile_Record_Typ</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Distributor_Partner_Profile_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Distributor Profile Record Typ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Partner_Account_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PartnerAccountRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Partner Account RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>PRM_DealReg_Profiled_Account_Update</fullName>
        <apiVersion>21.0</apiVersion>
<<<<<<< HEAD
        <endpointUrl>http://soagateway-test.emc.com/sst/runtime.asvc/com.actional.intermediary.aicNotifyAccountServiceEMCSFDC_DES?WSDL</endpointUrl>
=======
        <endpointUrl>https://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.sfdcPrmParterAttribute?WSDL</endpointUrl>
>>>>>>> master
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>kothawade_prasad@emc.com.prod</integrationUser>
        <name>PRM Attribute Update</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Allow Lead %26 Oppty</fullName>
        <actions>
            <name>Update_Allow_Lead_Oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.Child_Partner_Users_Count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.PROFILED_ACCOUNT_FLAG__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Authorized Reseller Training Complete</fullName>
        <actions>
            <name>Update_AR_Training_Field_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (3 AND 4) OR (5 AND 6) OR (7 AND 8)</booleanFilter>
        <criteriaItems>
            <field>Account.Count_of_Affiliate_Enablement_Center_SA__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Affiliate_Enablement_Center_SE__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Affiliate_Enablement_Center_SA__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Velocity_System_Engineers__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Sales_Accreditations__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Affiliate_Enablement_Center_SE__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Sales_Accreditations__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Velocity_System_Engineers__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>Used to check the Authorized Reseller Training Complete field based on the required criteria.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Authorized Reseller Training Incomplete</fullName>
        <actions>
            <name>Update_AR_Training_Field_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (3 AND 4) OR (5 AND 6) OR (7 AND 8)</booleanFilter>
        <criteriaItems>
            <field>Account.Count_of_Affiliate_Enablement_Center_SA__c</field>
            <operation>lessThan</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Affiliate_Enablement_Center_SE__c</field>
            <operation>lessThan</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Affiliate_Enablement_Center_SA__c</field>
            <operation>lessThan</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Velocity_System_Engineers__c</field>
            <operation>lessThan</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Sales_Accreditations__c</field>
            <operation>lessThan</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Affiliate_Enablement_Center_SE__c</field>
            <operation>lessThan</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Sales_Accreditations__c</field>
            <operation>lessThan</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_of_Velocity_System_Engineers__c</field>
            <operation>lessThan</operation>
            <value>1</value>
        </criteriaItems>
        <description>Used to check the Authorized Reseller Training Complete field based on the required criteria.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change to Partner Acct Record Type_Update Owner Standalone</fullName>
        <actions>
            <name>UpdateOwnertoStandaoneAdminShared</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Allow_Lead_Oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Used to change an Account to the Partner Account Record Type AND to change to owner to Standalone Admin when the Type picklist field is changed to Partner.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Change to Standard Acct Record Type_Update Owner EMC Default</fullName>
        <actions>
            <name>UpdateOwnertoEMCDefaultPrivate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateRecordTypeStandard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4) AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Customer Account Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>EMC Default Owner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account__c</field>
            <operation>notEqual</operation>
            <value>EMC (Internal) Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Customer_Profiled_Account__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Customer_Profiled_Account_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Used to change an Account to the Standard Account Record Type AND to change to owner to EMC Default Owner when the Type picklist field is changed to null.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Check BUU Flag</fullName>
        <actions>
            <name>Check_BUU_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF((Hub_Info__r.Business_Unit_Ultimate__c =True),True,False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check Channel Leverage Account</fullName>
        <actions>
            <name>Check_Channel_Leverage_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(( Hub_Info__r.Channel_Leverage_Account__c =True),True,False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check DU Flag</fullName>
        <actions>
            <name>Check_DU_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF((Hub_Info__r.Domestic_Ultimate__c =True),True,False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check Dell Black</fullName>
        <actions>
            <name>Check_Dell_Black_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(( Hub_Info__r.Dell_Black_Account__c =True),True,False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check GU Flag</fullName>
        <actions>
            <name>Check_GU_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF((Hub_Info__r.Global_Ultimate__c =True),True,False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check Global Account</fullName>
        <actions>
            <name>Check_Global_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF((Hub_Info__r.Global_Account__c =True),True, False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check NTApp 100 Account</fullName>
        <actions>
            <name>Check_NTApp_100_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF((Hub_Info__r.NTApp_100_Account__c =True),True, False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Core Quota Rep</fullName>
        <actions>
            <name>Core_Quota_Rep_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW() ||  ISCHANGED(   District_Lookup__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Customer Account</fullName>
        <actions>
            <name>Update_Acc_Record_Type_Customer_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Fires when the Customer Profiled Account checkbox is not selected and an Customer Profiled Account is not populated in the lookup on a non-partner Account 
(Account Presentation)</description>
        <formula>((NOT(Customer_Profiled_Account__c) &amp;&amp; ISBLANK(Customer_Profiled_Account_Lookup__c)) || (ISNULL(District_Lookup__c) &amp;&amp; ISNULL( Core_Quota_Rep__c )) ) &amp;&amp;  NOT(ISPICKVAL( Type , &apos;Partner&apos;)) &amp;&amp;  $RecordType.DeveloperName &lt;&gt; &apos;Distributor_Partner_Account_Record_Type&apos; &amp;&amp;  $RecordType.DeveloperName &lt;&gt; &apos;Distributor_Partner_Profile_Record_Type&apos; &amp;&amp;  $RecordType.DeveloperName &lt;&gt; &apos;PartnerAccountRecordType&apos; &amp;&amp;  $RecordType.DeveloperName &lt;&gt; &apos;Partner_Profile_record_type&apos; &amp;&amp;  $RecordType.DeveloperName &lt;&gt; &apos;T2_Partner_Profile_Record_Type&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Customer Profiled Account</fullName>
        <actions>
            <name>UpdateOwnertoEMCDefaultPrivate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Acc_Record_Type_Customer_Profiled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Account.Customer_Profiled_Account__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Customer Profiled Account Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>EMC Default Owner</value>
        </criteriaItems>
        <description>Fires when the Customer Profiled Account chechkbox is select on Account
(Account Presentation)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Customer Profiled Account Flag</fullName>
        <actions>
            <name>Customer_Profiled_Account_Flag_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW() ||  ISCHANGED( Customer_Profiled_Account__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Customer Profiled Account Link</fullName>
        <actions>
            <name>Customer_Profiled_Account_Link_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW() ||  ISCHANGED(  Customer_Profiled_Account_Lookup__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Default Association Account</fullName>
        <actions>
            <name>Update_Association_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>District Lookup</fullName>
        <actions>
            <name>District_Lookup_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW() ||  ISCHANGED(   District_Lookup__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Ensure Type %3D Parter on Partner Accounts</fullName>
        <actions>
            <name>Update_Type_to_Partner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update the field Type to Partner when Partner Account is checked. Avoid Synergy Updates overwriting this field.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flip the record type to %27Distributor Partner Account Record Type%27</fullName>
        <actions>
            <name>Update_Record_Type_to_Disty_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Account.PROFILED_ACCOUNT_FLAG__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Type__c</field>
            <operation>includes</operation>
            <value>Distributor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <description>If the PROFILED_ACCOUNT_FLAG is not checked and Partner Type includes Distributor, then display the Page Layout created for a “Distributor Partner Account” record type.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flip the record type to %27Distributor Partner Profile Record Type%27</fullName>
        <actions>
            <name>Update_to_Distributor_Profile_Record_Typ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Account.PROFILED_ACCOUNT_FLAG__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Type__c</field>
            <operation>includes</operation>
            <value>Distributor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <description>If the PROFILED_ACCOUNT_FLAG is checked and Partner Type includes Distributor, then display the Page Layout created for a “Distributor Partner Profile” record type.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flip the record type to %27Partial Partner Profile Record Type%27</fullName>
        <actions>
            <name>Update_Record_Type_to_Partial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) AND (3 or 4) AND 5</booleanFilter>
        <criteriaItems>
            <field>Account.PROFILED_ACCOUNT_FLAG__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Type__c</field>
            <operation>includes</operation>
            <value>Distribution VAR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Model__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <description>If the PROFILED_ACCOUNT_FLAG is checked, and the Type is T2 then display the Page Layout created for a “Partner Profile” record type.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flip the record type to %27Partner Account Record Type%27</fullName>
        <actions>
            <name>Update_to_Partner_Account_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND (2 OR 5)) AND (3 or 4)</booleanFilter>
        <criteriaItems>
            <field>Account.PROFILED_ACCOUNT_FLAG__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Type__c</field>
            <operation>excludes</operation>
            <value>Distributor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If the PROFILED_ACCOUNT_FLAG is not checked, then display the Page Layout created for a “Partner Account” record type.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flip the record type to %27Partner Profile Record Type%27</fullName>
        <actions>
            <name>Change_to_Partner_Profile</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND (2 OR 5)) AND (3 or 4)</booleanFilter>
        <criteriaItems>
            <field>Account.PROFILED_ACCOUNT_FLAG__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Type__c</field>
            <operation>excludes</operation>
            <value>Distributor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If the PROFILED_ACCOUNT_FLAG is checked, then display the Page Layout created for a “Partner Profile” record type.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flip the record type to Profile Account</fullName>
        <actions>
            <name>Change_to_Partner_Profile_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) AND (3 or 4) AND 5</booleanFilter>
        <criteriaItems>
            <field>Account.PROFILED_ACCOUNT_FLAG__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Type__c</field>
            <operation>includes</operation>
            <value>Distribution VAR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Model__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>This workflow is to be used to flip the record type back to partner profile if the T2 is a Closed Model.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Non Customer Profiled Account</fullName>
        <actions>
            <name>UpdateOwnertoEMCDefaultPrivate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Acc_Rec_Type_NonCustomer_Profiled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND (5 OR 6)</booleanFilter>
        <criteriaItems>
            <field>Account.Customer_Profiled_Account__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Customer_Profiled_Account_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Customer Non-Profiled Account Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>EMC Default Owner</value>
        </criteriaItems>
        <description>Fires when the Customer Profiled Account checkbox is not selected and an Customer Profiled Account is populated in the lookup on a non-partner Account 
(Account Presentation)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to Build CAP for Account Owners for Upcoming Year</fullName>
        <actions>
            <name>Notification_to_Account_Owner_of_all_profiled_accounts_to_build_CAP_for_the_upco</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>At the end of the third quarter (September 30st) a notification should be sent to the Account Owner of all profiled accounts that reminds them it is time to start building the Channel Account Plan for the upcoming quarter/year.</description>
        <formula>CAP_Creation_Date__c=Today()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRM Deal Reg Non Profield Partner Attribute Integration</fullName>
        <actions>
            <name>PRM_DealReg_Profiled_Account_Update</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>When Non profiled partner accounts has user count  0 and changes more than 0 or more 0 changes 0 send message to create / delete account.</description>
        <formula>OR( AND( ISCHANGED(Child_Partner_Users_Count__c), Child_Partner_Users_Count__c &gt;0, PRIORVALUE(Child_Partner_Users_Count__c) =0 ), AND( ISCHANGED(Child_Partner_Users_Count__c), Child_Partner_Users_Count__c =0, PRIORVALUE(Child_Partner_Users_Count__c) &gt;0 ), AND( ISCHANGED(Child_Partner_Users_Count__c), Child_Partner_Users_Count__c &gt;0, PRIORVALUE(Child_Partner_Users_Count__c) &gt;0 ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM Deal Reg Non Profield Partner Attribute Update Integration</fullName>
        <actions>
            <name>PRM_DealReg_Profiled_Account_Update</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>Non Porfiled Account having greater than 0 users and any update happens to 13 attributes send message to EIS.</description>
        <formula>AND (  ISPICKVAL(Type, &apos;Partner&apos;) , Child_Partner_Users_Count__c &gt;0 ,  PROFILED_ACCOUNT_FLAG__c  =false, OR( ISCHANGED( Party_ID__c ), ISCHANGED( Partner_Type__c ), ISCHANGED( Master_Grouping__c ), ISCHANGED( Grouping__c ), ISCHANGED( Velocity_Services_Implement__c ), ISCHANGED( VSI_Approved_Delivery_Products__c ), ISCHANGED( Affiliate_Services__c ), ISCHANGED( Master_Site_ID__c ), ISCHANGED( Master_Agreement__c ), ISCHANGED( Advanced_Consolidate_Specialty__c ), ISCHANGED( Backup_and_Recovery_Speciality__c ), ISCHANGED( Consolidate_Specialty__c ), ISCHANGED( Governance_and_Archive_Specialty__c ), ISCHANGED(  Americas_VNXE_Distributor__c ), ISCHANGED(  Velocity_Solution_Provider_Tier__c  )  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM_DealReg_Profiled Account Delete</fullName>
        <actions>
            <name>PRM_DealReg_Profiled_Account_Update</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <formula>AND(  PROFILED_ACCOUNT_FLAG__c = FALSE,  ISCHANGED(PROFILED_ACCOUNT_FLAG__c),  Grouping__c = NULL  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM_DealReg_Profiled Account Insert</fullName>
        <actions>
            <name>PRM_DealReg_Profiled_Account_Update</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <formula>AND(  OR(  ISNEW(),  ISCHANGED(PROFILED_ACCOUNT_FLAG__c)  ),  PROFILED_ACCOUNT_FLAG__c = TRUE,  Grouping__c &lt;&gt; NULL  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM_DealReg_Profiled Account Update</fullName>
        <actions>
            <name>Reset_Outbound_Message_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRM_DealReg_Profiled_Account_Update</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <formula>AND( PROFILED_ACCOUNT_FLAG__c = TRUE,   Grouping__c &lt;&gt; NULL  ,   OR( ISCHANGED( Party_ID__c ),          ISCHANGED( Partner_Type__c ),          ISCHANGED( Master_Grouping__c ),         ISCHANGED( Grouping__c ),          ISCHANGED( Velocity_Services_Implement__c ),         ISCHANGED( VSI_Approved_Delivery_Products__c ),         ISCHANGED( Affiliate_Services__c ),        ISCHANGED( Master_Site_ID__c ),         ISCHANGED( Master_Agreement__c ),        ISCHANGED( Advanced_Consolidate_Specialty__c ),       ISCHANGED( Backup_and_Recovery_Speciality__c ),      ISCHANGED( Consolidate_Specialty__c ),     ISCHANGED( Governance_and_Archive_Specialty__c ),    ISCHANGED(  Americas_VNXE_Distributor__c),   ISCHANGED(   Velocity_Solution_Provider_Tier__c  )   ) )||(SendOutboundNotification__c=true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PROPEL - Partner Accounts Creation And Update</fullName>
        <actions>
            <name>PRM_DealReg_Profiled_Account_Update</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>1)Partner Account created AND/OR fields updated
2)Account change TO/FROM a Partner Account
3)Account change TO/FROM a Partner Profiled Account
4)New Partner Quoting Account created AND/OR fields updated
5)Account change TO/FROM Partner Quoting Account</description>
        <formula>OR(AND(ISNEW(), NOT(CONTAINS(RecordType.DeveloperName,&apos;Partner&apos;)),CONTAINS(TEXT(Type),&apos;Partner&apos;)),AND(NOT(CONTAINS(RecordType.DeveloperName,&apos;Partner&apos;)),ISCHANGED(Type),CONTAINS(TEXT(Type),&apos;Partner&apos;)), AND(CONTAINS(RecordType.DeveloperName,&apos;Partner&apos;),OR(ISCHANGED( RecordTypeId),ISCHANGED( Reporting_Segmentation__c),ISCHANGED( Reporting_Segmentation_Group__c),ISCHANGED( Grouping__c),ISCHANGED( Master_Grouping__c),ISCHANGED( Profiled_Account__c),ISCHANGED( PROFILED_ACCOUNT_FLAG__c))),AND(CONTAINS( $Setup.PROPEL_Partner_Account_RecordTypes__c.Record_Type_ID__c ,PRIORVALUE(RecordTypeId)) ,ISCHANGED(RecordTypeId), NOT(CONTAINS( RecordType.DeveloperName , &apos;Partner&apos;))),AND( Partner_Quoting_Account__c,OR(ISCHANGED(Quoting_EPOA__c),ISCHANGED(Quoting_Payment_Terms__c),ISCHANGED(Quoting_Freight_Terms__c),ISCHANGED(Quoting_FOBIncoterms__c),ISCHANGED(Quoting_Autobook_Enabled__c),ISCHANGED(Partner_Quoting_Account_Name__c))),AND(ISCHANGED(Partner_Quoting_Account__c),Partner_Quoting_Account__c,CONTAINS(RecordType.DeveloperName,&apos;Partner&apos;)),AND(ISCHANGED(Partner_Quoting_Account__c), NOT(Partner_Quoting_Account__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Partner Fields Completed</fullName>
        <actions>
            <name>Flip_Profile_Complete_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Account.Application_Environments__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Self_Description_Long__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partnership_Overview__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Product_Focus__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Business_Focus__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Industry_Verticals__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Verifies all Partner-editable Account fields are populated. Displays flag as true</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Partner Fields InCompleted</fullName>
        <actions>
            <name>Uncheck_Partner_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 or 2  or 3  or 4  or 5  or 6</booleanFilter>
        <criteriaItems>
            <field>Account.Application_Environments__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Self_Description_Long__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partnership_Overview__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Product_Focus__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Business_Focus__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Industry_Verticals__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Verifies all Partner-editable Account fields are populated. Displays flag as false if not.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck ATP Facilitated Account</fullName>
        <actions>
            <name>ATP_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF((Hub_Info__r.ATP_Facilitated_Account__c=True) ,True, False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck BUU Flag</fullName>
        <actions>
            <name>Uncheck_BUU_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF((Hub_Info__r.Business_Unit_Ultimate__c =False),True,False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Channel Leverage Account</fullName>
        <actions>
            <name>Uncheck_Channel_Leverage_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(( Hub_Info__r.Channel_Leverage_Account__c =False),True,False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck DU Flag</fullName>
        <actions>
            <name>Uncheck_DU_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF((Hub_Info__r.Domestic_Ultimate__c =False),True,False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Dell Black</fullName>
        <actions>
            <name>Uncheck_Dell_Black</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(( Hub_Info__r.Dell_Black_Account__c =False),True,False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck GU Flag</fullName>
        <actions>
            <name>Uncheck_GU_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF((Hub_Info__r.Global_Ultimate__c =False),True,False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Global Account</fullName>
        <actions>
            <name>Uncheck_Global_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF((Hub_Info__r.Global_Account__c =False),True, False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck NTApp 100 Account</fullName>
        <actions>
            <name>Uncheck_NTApp_100_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF((Hub_Info__r.NTApp_100_Account__c =False),True, False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Site Field</fullName>
        <actions>
            <name>Update_Account_Site_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Site</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Account to NOT be Approved for Finder</fullName>
        <actions>
            <name>Uncheck_Approved_for_Finder</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_Lead_Oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Account to be Approved for Finder</fullName>
        <actions>
            <name>Default_Channel_revenue_Partner_to_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Approved_for_Partner_Finder</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 or 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.PROFILED_ACCOUNT_FLAG__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
<<<<<<< HEAD
        <fullName>Update DUNS on Create</fullName>
=======
        <fullName>Update Fields On Create - iMAP</fullName>
>>>>>>> master
        <actions>
            <name>Update_Account_Segmentation</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
<<<<<<< HEAD
            <name>Update_Domestic_DUNS</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Global_DUNS</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Site_DUNS</name>
=======
            <name>Update_Domestic_Duns_Entity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Global_Ultimate_Entity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Site_Duns_Entity</name>
>>>>>>> master
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Status__c</field>
            <operation>equals</operation>
            <value>A</value>
        </criteriaItems>
<<<<<<< HEAD
        <description>Update Site, Domestic and Global DUNS information to equal party number when new account record is created.</description>
=======
        <description>Update DUNS Numbers, and Segmentation on account creation</description>
>>>>>>> master
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update GU Date Changed</fullName>
        <actions>
            <name>Update_GU_Date_Changed_to_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Global_DUNS_Entity__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Type to Partner</fullName>
        <actions>
            <name>UpdateOwnertoStandaoneAdminShared</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Type_to_Partner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
