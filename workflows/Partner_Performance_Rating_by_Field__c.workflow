<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Flip_to_Saved_Record_when_Saved</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Saved_Partner_Performance_Rating</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Flip to Saved Record when Saved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Products</fullName>
        <field>Products_Sold__c</field>
        <formula>Opportunity__r.Products__c</formula>
        <name>Populate Products</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Flip to Saved Record when Saved</fullName>
        <actions>
            <name>Flip_to_Saved_Record_when_Saved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Partner_Performance_Rating_by_Field__c.Pre_Sales_Rating__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Products</fullName>
        <actions>
            <name>Populate_Products</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Partner_Performance_Rating_by_Field__c.Pre_Sales_Rating__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
