<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>updateschedule</fullName>
        <description>Update to fire trigger</description>
        <field>Update_Me__c</field>
        <formula>Text(VALUE( Update_Me__c ) +1)</formula>
        <name>Update Schedule</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Run Scheduler</fullName>
        <active>true</active>
        <description>Schedule the operation on specific time (Next schedule time)</description>
        <formula>Update_Me__c == &apos;0&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>updateschedule</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Job_Scheduler__c.Next_Schedule__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
