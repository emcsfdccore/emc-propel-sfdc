<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>updateCourseName</fullName>
        <field>SearchableCourseName__c</field>
        <formula>TEC_Asset__r.Name + 

IF(ISPICKVAL( Modality__c , &quot;Online Lab&quot;), &apos; - &apos;+TEXT(Modality__c), 
IF(ISPICKVAL( Modality__c , &quot;Express&quot;), &apos; - &apos;+TEXT(Modality__c), 
IF(ISPICKVAL( Modality__c , &quot;Online ILT&quot;), &apos; - &apos;+TEXT(Modality__c), 
IF(ISPICKVAL( Modality__c , &quot;Video ILT – Stream&quot;), &apos; - &apos;+ &apos;Video ILT&apos;, 
IF(ISPICKVAL( Modality__c , &quot;Video ILT&quot;), &apos; - &apos;+&apos;Video ILT&apos;, &quot;&quot;)))))</formula>
        <name>updateCourseName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>updateCourseName</fullName>
        <actions>
            <name>updateCourseName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TEC_Course__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
