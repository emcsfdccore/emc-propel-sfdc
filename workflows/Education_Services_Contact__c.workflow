<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Education_Geography_Update</fullName>
        <field>Geography_Edu_Contact__c</field>
        <formula>IF((TEXT(Country__c))=&quot;&quot;,&quot;&quot;, 
IF(CONTAINS(Geography_EMEA__c, TEXT(Country__c)), &quot;EMEA&quot;, 
IF(CONTAINS(Geography_Americas__c, TEXT(Country__c)), &quot;Latin America&quot;,
IF(CONTAINS(Geography_APJ__c, TEXT(Country__c)), &quot;APJ&quot;,
IF(CONTAINS(Geography_United_States__c, TEXT(Country__c)), &quot;United States&quot;, &quot;N/A&quot;
)
)
)
)
)</formula>
        <name>Education Geography Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Education Services Geography</fullName>
        <actions>
            <name>Education_Geography_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISCHANGED(Country__c),ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
