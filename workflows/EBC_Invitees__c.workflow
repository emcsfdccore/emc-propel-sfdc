<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Feedback_Submision_Alert_Cork_Field_EMEA_Singpoare</fullName>
        <description>Feedback Submision Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Attendee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EBC_Templates/Feedback_Submitted</template>
    </alerts>
    <rules>
        <fullName>Send Feedback%2C Cork%2C Field EMEA%2C Singapore</fullName>
        <actions>
            <name>Feedback_Submision_Alert_Cork_Field_EMEA_Singpoare</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Briefing_Event__r.Feedback_Status_Picklist__c, &quot;Submitted&quot;) &amp;&amp;  Feedback_Output__c   = TRUE &amp;&amp;  Feedback_Submitted__c = TRUE &amp;&amp; OR(CONTAINS(Briefing_Event__r.Briefing_Center__r.Name, &quot;Cork&quot;), CONTAINS(Briefing_Event__r.Briefing_Center__r.Name, &quot;Singapore&quot;), CONTAINS(Briefing_Event__r.Briefing_Center__r.Name, &quot;Field EMEA&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Feedback%2C Field Americas%2C Tele-Briefings</fullName>
        <actions>
            <name>Feedback_Submision_Alert_Cork_Field_EMEA_Singpoare</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Briefing_Event__r.Feedback_Status_Picklist__c, &quot;Submitted&quot;) &amp;&amp; Feedback_Output__c  = TRUE &amp;&amp;  Feedback_Submitted__c = TRUE &amp;&amp; OR(CONTAINS(Briefing_Event__r.Briefing_Center__r.Name, &quot;Field Americas&quot;), CONTAINS(Briefing_Event__r.Briefing_Center__r.Name, &quot;Tele-Briefings&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Feedback%2C Hopkinton %26 RSA</fullName>
        <actions>
            <name>Feedback_Submision_Alert_Cork_Field_EMEA_Singpoare</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Briefing_Event__r.Feedback_Status_Picklist__c, &quot;Submitted&quot;) &amp;&amp;  Feedback_Output__c  = TRUE &amp;&amp; Feedback_Submitted__c = TRUE &amp;&amp; OR(CONTAINS(Briefing_Event__r.Briefing_Center__r.Name, &quot;Hopkinton&quot;), CONTAINS(Briefing_Event__r.Briefing_Center__r.Name, &quot;RSA, Bedford, MA&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Feedback%2C Santa Clara</fullName>
        <actions>
            <name>Feedback_Submision_Alert_Cork_Field_EMEA_Singpoare</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Briefing_Event__r.Feedback_Status_Picklist__c, &quot;Submitted&quot;) &amp;&amp; Feedback_Output__c  = TRUE &amp;&amp;  Feedback_Submitted__c = TRUE &amp;&amp; CONTAINS(Briefing_Event__r.Briefing_Center__r.Name, &quot;Santa Clara&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Feedback%2C Tokyo%2C Bangalore%2C Field APJ</fullName>
        <actions>
            <name>Feedback_Submision_Alert_Cork_Field_EMEA_Singpoare</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Briefing_Event__r.Feedback_Status_Picklist__c, &quot;Submitted&quot;) &amp;&amp; Feedback_Output__c  = TRUE &amp;&amp;  Feedback_Submitted__c = TRUE &amp;&amp; OR(CONTAINS(Briefing_Event__r.Briefing_Center__r.Name, &quot;Tokyo&quot;), CONTAINS(Briefing_Event__r.Briefing_Center__r.Name, &quot;Bangalore&quot;), CONTAINS(Briefing_Event__r.Briefing_Center__r.Name, &quot;Field APJ&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Feedback%2C Tour Only</fullName>
        <actions>
            <name>Feedback_Submision_Alert_Cork_Field_EMEA_Singpoare</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Briefing_Event__r.Feedback_Status_Picklist__c, &quot;Submitted&quot;) &amp;&amp; Feedback_Output__c   = TRUE &amp;&amp;  Feedback_Submitted__c = TRUE &amp;&amp; CONTAINS(Briefing_Event__r.Briefing_Center__r.Name, &quot;Tour Only&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
