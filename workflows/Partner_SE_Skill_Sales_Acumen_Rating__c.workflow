<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Core_TC_to_Validate_email_alert</fullName>
        <description>Core TC to Validate email alert</description>
        <protected>false</protected>
        <recipients>
            <field>Core_TC_to_Validate__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Core_TC_to_Validate_Email_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Populate_Ratings_Date</fullName>
        <description>Populate the “Rating Date” with the date the Partner TC last updated the Partner SE Skill &amp; Sales Acumen Rating record.</description>
        <field>Rating_Date__c</field>
        <formula>TODAY()</formula>
        <name>Populate Ratings Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Core TC to Validate</fullName>
        <actions>
            <name>Core_TC_to_Validate_email_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Partner_SE_Skill_Sales_Acumen_Rating__c.Core_TC_to_Validate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Partner_SE_Skill_Sales_Acumen_Rating__c.Ratings_Validated_by_Core_TC__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
