<?xml version="1.0" encoding="UTF-8"?>
<<<<<<< HEAD
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata"/>

=======
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PROPEL_Populate_Partner_Type_Key</fullName>
        <description>Populating the Partner type Key field by concatenating Partner Account IDand Partner Price ListType field</description>
        <field>Partner_Type_Key__c</field>
        <formula>Partner_Account__c   &amp; &apos; - &apos; &amp; text(Partner_Price_List_Type__c)</formula>
        <name>PROPEL Populate Partner Type Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PROPEL Partner Type Attribute Create OR Update</fullName>
        <actions>
            <name>PROPEL_Populate_Partner_Type_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>PROPEL - Whenever a new Partner Type Attribute record is created or updated, fire this workflow rule</description>
        <formula>OR( ISNEW(), ISCHANGED( Partner_Account__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
>>>>>>> develop
