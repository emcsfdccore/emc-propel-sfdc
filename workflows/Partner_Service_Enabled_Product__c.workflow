<?xml version="1.0" encoding="UTF-8"?>
<<<<<<< HEAD
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata"/>

=======
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PROPEL_Populate_Service_Enabled_Key</fullName>
        <description>Populating the Service Enabled Key field by concatenating Partner Account ID, Partner Service Enabled Product and Service Level.</description>
        <field>Service_Enabled_Key__c</field>
        <formula>Partner_Account__c &amp; &apos; - &apos; &amp; TEXT(Service_Enabled_Product__c) &amp; &apos; - &apos; &amp; TEXT(Service_Level__c)</formula>
        <name>PROPEL Populate Service Enabled Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PROPEL Partner Service Enabled Product Create OR Update</fullName>
        <actions>
            <name>PROPEL_Populate_Service_Enabled_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>PROPEL - Whenever a new Partner Service Enabled Product record is created or updated, fire this workflow rule</description>
        <formula>OR( ISNEW(), ISCHANGED( Partner_Account__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
>>>>>>> develop
