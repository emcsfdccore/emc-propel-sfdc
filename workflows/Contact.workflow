<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Active_Flag_set_to_False</fullName>
        <description>Active Flag set to False</description>
        <field>Active__c</field>
        <literalValue>0</literalValue>
        <name>Active Flag set to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Record_Type_To_Standard</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Standard</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type To Standard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Partner_SE_Record_Type_Update</fullName>
        <description>This updated the Contact to a Partner SE record type when the Partner SE box is checked</description>
        <field>RecordTypeId</field>
        <lookupValue>Partner_SE_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Partner SE Record Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetDefaultOwner</fullName>
        <description>Set Default Owner</description>
        <field>OwnerId</field>
        <lookupValue>defauktownerhidden@emc.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Default Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UncheckFundedHeadFlag</fullName>
        <field>Funded_Head__c</field>
        <literalValue>0</literalValue>
        <name>UncheckFundedHeadFlag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Partner_Contact_Checkbox</fullName>
        <field>Partner_Contact2__c</field>
        <literalValue>1</literalValue>
        <name>Update Partner Contact Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_to_Partner_Contact</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Partner_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record to Partner Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Expired Funded Head</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Funded_Head__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Partner Contact,Partner SE Disti/Direct Reseller Record Type,Distribution VAR Contact Record Type,Partner SE VAR Record Type</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>UncheckFundedHeadFlag</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contact.Funded_End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Flip to Partner Contact Record Type</fullName>
        <actions>
            <name>Update_Record_to_Partner_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Contact.Partner_Contact2__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>EMC Internal Contact</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Partner_SE__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Distribution VAR Contact Record Type</value>
        </criteriaItems>
        <description>Flip the Record Type to Partner Contact Record Type when Partner Contact is unchecked</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Flip to Standard Contact Record Type</fullName>
        <actions>
            <name>Change_Record_Type_To_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Partner_Contact2__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>EMC Internal Contact,Support Force Portal User Contact,Partner SE Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Partner_Type__c</field>
            <operation>excludes</operation>
            <value>Distribution VAR</value>
        </criteriaItems>
        <description>Flip the Record Type to Standard contact Record Type when Partner Contact is unchecked</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Inactivate Support Force Portal User Contact</fullName>
        <actions>
            <name>Active_Flag_set_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Force Portal User Contact</value>
        </criteriaItems>
        <description>Inactivate Support Force Portal User Contact</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Partner SE</fullName>
        <actions>
            <name>Partner_SE_Record_Type_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3) AND 2 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Contact.Partner_SE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Partner_Contact2__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Inside_Partner_SE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Partner SE VAR Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Partner SE Disti/Direct Reseller Record Type</value>
        </criteriaItems>
        <description>This flips record type to Partner SE when the Partner SE box is checked</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Default Owner</fullName>
        <actions>
            <name>SetDefaultOwner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Default Owner</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Partner Contact Checkbox</fullName>
        <actions>
            <name>Update_Partner_Contact_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Record_to_Partner_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.IsPartner</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Partner_SE__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Distribution VAR Contact Record Type</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
