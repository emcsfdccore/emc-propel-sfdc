<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Owner_Change_Email_Alert</fullName>
        <description>Account_Owner_Change_Email_Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Team_Member_Added__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_Account_Owner_Change_Email</template>
    </alerts>
    <alerts>
        <fullName>GFS_Registration_Alert</fullName>
        <description>GFS Registration Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email_notification_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Team_Member_Added__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Register_With_GFS</template>
    </alerts>
    <alerts>
        <fullName>Notify_Opportunity_Moved_Commit_to_Strong_Upside</fullName>
        <description>Notify Opportunity Moved Commit to Strong Upside</description>
        <protected>false</protected>
        <recipients>
            <field>Team_Member_Added__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_moved_from_Commit_to_Strong_Upside_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Opportunity_Moved_Commit_to_Upside</fullName>
        <description>Notify Opportunity Moved Commit to Upside</description>
        <protected>false</protected>
        <recipients>
            <field>Team_Member_Added__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_moved_from_Commit_to_Upside_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Opportunity_Moved_Strong_Upside_to_Upside</fullName>
        <description>Notify Opportunity Moved Strong Upside to Upside</description>
        <protected>false</protected>
        <recipients>
            <field>Team_Member_Added__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_Moved_to_Strong_Upside_to_Upside_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Opportunity_Moved_to_Future_Quarter</fullName>
        <description>Notify Opportunity Moved to Future Quarter</description>
        <protected>false</protected>
        <recipients>
            <field>Team_Member_Added__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_Close_Date_Moved_to_Future_Quarter_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Opportunity_is_closed</fullName>
        <description>Notify Opportunity is closed</description>
        <protected>false</protected>
        <recipients>
            <field>Team_Member_Added__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_Closed_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_Opportunity_is_closed_to_Competition</fullName>
        <description>Notify Opportunity is closed to Competition</description>
        <protected>false</protected>
        <recipients>
            <field>Team_Member_Added__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/Opportunity_Closed_to_Competition_Template</template>
    </alerts>
    <alerts>
        <fullName>Notify_TA_Sync_Error_to_TRACK_and_ADMIN</fullName>
        <description>Notify TA Sync Error to TRACK and ADMIN</description>
        <protected>false</protected>
        <recipients>
            <field>Email_notification_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email_notification_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Team_Member_Added__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/TA_Sync_Error_Email_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_When_SO_Number_field_on_Opportunity_Account_Theater_APJ_is_Updated_MOJO</fullName>
        <description>Notify When SO Number field on Opportunity (Account Theater = APJ) is Updated (MOJO)</description>
        <protected>false</protected>
        <recipients>
            <recipient>MOJO_GFS_APJ</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <field>User_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/SO_Number_Update_Email_MOJO</template>
    </alerts>
    <alerts>
        <fullName>Notify_When_SO_Number_field_on_Opportunity_Account_Theater_Americas_is_Updated_M</fullName>
        <description>Notify When SO Number field on Opportunity (Account Theater = Americas) is Updated (MOJO)</description>
        <protected>false</protected>
        <recipients>
            <recipient>MOJO_GFS_AMERICA</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <field>User_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/SO_Number_Update_Email_MOJO</template>
    </alerts>
    <alerts>
        <fullName>Notify_When_SO_Number_field_on_Opportunity_Account_Theater_EMEA_is_Updated_MOJO</fullName>
        <description>Notify When SO Number field on Opportunity (Account Theater = EMEA) is Updated (MOJO)</description>
        <protected>false</protected>
        <recipients>
            <recipient>MOJO_GFS_EMEA</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <field>User_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Forecast_Status/SO_Number_Update_Email_MOJO</template>
    </alerts>
    <alerts>
        <fullName>Renewals_Task_Email_Alert</fullName>
        <description>Renewals Task Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email_notification_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>User2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>User_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SampleTemplates/Task_Email_Alert_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_Mail_to_Oppty_Team_Member_Added_Alert</fullName>
        <description>Send Mail to Oppty Team Member Added Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Team_Member_Added__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/OAR_Alert</template>
    </alerts>
    <alerts>
        <fullName>Under_pen_Email_Alert_EMEA</fullName>
        <description>Under pen Win Email Alert EMEA</description>
        <protected>false</protected>
        <recipients>
            <field>Email_notification_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Team_Member_Added__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/Under_Pen_Opportunity_Win_EMEA</template>
    </alerts>
    <rules>
        <fullName>Account Owner Change Notification</fullName>
        <actions>
            <name>Account_Owner_Change_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This fire on opportunity owner change, whenever opportunity gets updated.</description>
        <formula>IF(  OR  (  AND(  Team_Member_Added__r.Send_Oppty_Email_Alert__c =true,  Condition__c =&apos;Send Owner Change for Contract Renewals&apos;  ),  Condition__c=&apos;Send Owner Change Alert&apos;  )  , true, false)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GFS Member Added</fullName>
        <actions>
            <name>GFS_Registration_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>Condition__c =&apos;GFS Registration&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opportunity Closed</fullName>
        <actions>
            <name>Notify_Opportunity_is_closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Condition__c =&apos;Opportunity Closed&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opportunity Closed to Competition</fullName>
        <actions>
            <name>Notify_Opportunity_is_closed_to_Competition</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Condition__c =&apos;Opportunity Closed to Competition&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opportunity From Commit to Strong Upside</fullName>
        <actions>
            <name>Notify_Opportunity_Moved_Commit_to_Strong_Upside</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Condition__c =&apos;Opportunity moved from Commit to Strong Upsided&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opportunity From Commit to Upside</fullName>
        <actions>
            <name>Notify_Opportunity_Moved_Commit_to_Upside</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Condition__c =&apos;Opportunity moved from Commit to Upside&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opportunity From Strong Upside to Upside</fullName>
        <actions>
            <name>Notify_Opportunity_Moved_Strong_Upside_to_Upside</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Condition__c =&apos;Opportunity moved from Strong Upside to Upside&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opportunity Moved to%2Ffrom Future Quarter</fullName>
        <actions>
            <name>Notify_Opportunity_Moved_to_Future_Quarter</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Condition__c =&apos;Opportunity Close Date Moved to Future Quarter&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify TA Sync Error %23 Track %23 Admin</fullName>
        <actions>
            <name>Notify_TA_Sync_Error_to_TRACK_and_ADMIN</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send email notifications to the TRACK Team and System Admin</description>
        <formula>Condition__c = $Label.TA_Sync_Error</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OAR Notify When SO%23 Updated for APJ Theater Account Opportunity %28MOJO%29</fullName>
        <actions>
            <name>Notify_When_SO_Number_field_on_Opportunity_Account_Theater_APJ_is_Updated_MOJO</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Fires when SO# field on Opportunity belonging to APJ theater account is updated, which inserts an OAR record 
(MOJO Project)</description>
        <formula>Condition__c  =  $Label.OAR_Condition_for_MOJO  &amp;&amp;   CONTAINS( Text_1__c , &apos;APJ&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OAR Notify When SO%23 Updated for America Theater Account Opportunity %28MOJO%29</fullName>
        <actions>
            <name>Notify_When_SO_Number_field_on_Opportunity_Account_Theater_Americas_is_Updated_M</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Fires when SO# field on Opportunity belonging to America theater account is updated, which inserts an OAR record 
(MOJO Project)</description>
        <formula>Condition__c  =  $Label.OAR_Condition_for_MOJO  &amp;&amp;   CONTAINS( Text_1__c , &apos;America&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OAR Notify When SO%23 Updated for EMEA Theater Account Opportunity %28MOJO%29</fullName>
        <actions>
            <name>Notify_When_SO_Number_field_on_Opportunity_Account_Theater_EMEA_is_Updated_MOJO</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Fires when SO# field on Opportunity belonging to EMEA theater account is updated, which inserts an OAR record 
(MOJO Project)</description>
        <formula>Condition__c  =  $Label.OAR_Condition_for_MOJO  &amp;&amp;   CONTAINS( Text_1__c , &apos;EMEA&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Renewals Task Email Alert</fullName>
        <actions>
            <name>Renewals_Task_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OAR_Member_Added__c.Text_4__c</field>
            <operation>contains</operation>
            <value>Renewals Type Email</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Mail to Oppty Team Member Added</fullName>
        <actions>
            <name>Send_Mail_to_Oppty_Team_Member_Added_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(Team_Member_Added__r.Send_OAR_Alert__c =true,  Condition__c =&apos;Channel Visibility&apos;), true, false)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Underpen Win EMEA Alert</fullName>
        <actions>
            <name>Under_pen_Email_Alert_EMEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send Email Alert to EMEA Team FOR Under Pen Win.</description>
        <formula>Condition__c=&apos;UnderPen Win Email Alert&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
