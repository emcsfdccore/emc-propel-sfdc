<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_notification_before_15_days_of_concession</fullName>
        <description>Email notification before 15 days of concession</description>
        <protected>false</protected>
        <recipients>
            <field>Core_Quota_rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>maintenancecontractoperationssfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PVR/before_15_days_of_concession</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_before_45_days_of_concession</fullName>
        <description>Email notification before 45 days of concession</description>
        <protected>false</protected>
        <recipients>
            <field>Core_Quota_rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>maintenancecontractoperationssfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PVR/before_45_days_of_concession</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_to_Renewals_Rep_15_days</fullName>
        <description>Email notification to Renewals Rep-15 days</description>
        <protected>false</protected>
        <recipients>
            <field>Renewals_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>maintenancecontractoperationssfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PVR/Renewals_Rep_15_days</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_to_Renewals_Rep_45_days</fullName>
        <description>Email notification to Renewals Rep-45 days</description>
        <protected>false</protected>
        <recipients>
            <field>Renewals_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>maintenancecontractoperationssfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PVR/Renewals_Rep_45_days</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_to_Renewals_Rep_90_days</fullName>
        <description>Email notification to Renewals Rep-90 days</description>
        <protected>false</protected>
        <recipients>
            <field>Renewals_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>maintenancecontractoperationssfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PVR/to_Renewals_Rep_90_days</template>
    </alerts>
    <alerts>
        <fullName>Notification_before_90_days_of_concession</fullName>
        <description>Email notification before 90 days of concession</description>
        <protected>false</protected>
        <recipients>
            <field>Core_Quota_rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>maintenancecontractoperationssfdc@emc.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PVR/Concession_before_90_Days</template>
    </alerts>
    <fieldUpdates>
        <fullName>PVR_Disposition_c_updation</fullName>
        <field>PVR_Disposition__c</field>
        <literalValue>Time &amp; Material</literalValue>
        <name>PVR_Disposition__c updation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>To_update_value_to_none</fullName>
        <field>PVR_Disposition__c</field>
        <literalValue>None</literalValue>
        <name>To update value to none</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PVR Expiry 30 days after Concession End Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>PVR_Expiry__c.Concession_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>PVR_Expiry__c.PVR_Disposition__c</field>
            <operation>equals</operation>
            <value>None</value>
        </criteriaItems>
        <description>PVR Expiry 30 days after Concession End Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>PVR_Disposition_c_updation</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>PVR_Expiry__c.Concession_End_Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>To generate email notification before 15 days of concession date</fullName>
        <active>true</active>
        <description>To generate email notification before 15 days of concession date for data migration and data migration extension sub type</description>
        <formula>AND(  NOT( ISBLANK( Concession_End_Date__c ) ) ,   Notice_15_Day__c = True,  ISPICKVAL(PVR_Disposition__c , &apos;None&apos;) ,  Concession_End_Date__c &gt;= Today() + 15         )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_notification_before_15_days_of_concession</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_notification_to_Renewals_Rep_15_days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>PVR_Expiry__c.Concession_End_Date__c</offsetFromField>
            <timeLength>-15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>To generate email notification before 45 days of concession date</fullName>
        <active>true</active>
        <description>To generate email notification before 45 days of concession date for data migration and data migration extension sub type</description>
        <formula>AND(  NOT( ISBLANK( Concession_End_Date__c ) ) ,   Notice_45_Day__c = True,  ISPICKVAL(PVR_Disposition__c , &apos;None&apos;) ,  Concession_End_Date__c &gt;= Today() + 45        )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_notification_before_45_days_of_concession</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_notification_to_Renewals_Rep_45_days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>PVR_Expiry__c.Concession_End_Date__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>To generate email notification before 90 days of concession date</fullName>
        <active>true</active>
        <description>To generate email notification before 90 days of concession date</description>
        <formula>AND(       NOT( ISBLANK( Concession_End_Date__c ) ) ,       Notice_90_Day__c = True,        ISPICKVAL(PVR_Disposition__c , &apos;None&apos;) ,       Concession_End_Date__c &gt;= Today() +90                 )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_notification_to_Renewals_Rep_90_days</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Notification_before_90_days_of_concession</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>PVR_Expiry__c.Concession_End_Date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>To update null value to none</fullName>
        <actions>
            <name>To_update_value_to_none</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PVR_Expiry__c.PVR_Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
