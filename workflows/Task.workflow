<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Completion_Date_Update</fullName>
        <field>Completion_Date__c</field>
        <formula>TODAY()</formula>
        <name>Completion Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Activity_Type_On_Task</fullName>
        <description>This field updates the Activity Type field.</description>
        <field>Activity_Type__c</field>
        <formula>TEXT(Type)</formula>
        <name>Populate Activity Type On Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_Time_Capture</fullName>
        <field>Status_Update_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Status Update Time Capture</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_TaskType_Email</fullName>
        <description>If an Email is sent through SFDC, change the Type of the Task to Email.</description>
        <field>Type</field>
        <literalValue>Email</literalValue>
        <name>Update_TaskType_Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_TaskType_Proposal</fullName>
        <field>Type</field>
        <literalValue>Proposal</literalValue>
        <name>Update_TaskType_Proposal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VCE_Task_Initial_Accept</fullName>
        <field>VCE_Task_Initial_Accept__c</field>
        <formula>NOW()</formula>
        <name>VCE Task Initial Accept</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>VCETaskInsertToOracle</fullName>
        <apiVersion>18.0</apiVersion>
<<<<<<< HEAD
        <endpointUrl>https://soagateway.noint.emc.com/sst/runtime.asvc/com.actional.intermediary.sfdcVceTaskInsert</endpointUrl>
=======
        <endpointUrl>https://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.sfdcVceTaskInsert</endpointUrl>
>>>>>>> master
        <fields>Id</fields>
        <fields>VCE_Assigned_To__c</fields>
        <fields>WhatId</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>vce.internal.owner@emc.com</integrationUser>
        <name>VCETaskInsertToOracle</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>VCETaskUpdateToOracle</fullName>
        <apiVersion>8.0</apiVersion>
<<<<<<< HEAD
        <endpointUrl>https://soagateway.noint.emc.com/sst/runtime.asvc/com.actional.intermediary.sfdcVceTaskUpdate</endpointUrl>
=======
        <endpointUrl>https://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.sfdcVceTaskUpdate</endpointUrl>
>>>>>>> master
        <fields>Id</fields>
        <fields>VCE_Assigned_To__c</fields>
        <fields>WhatId</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>vce.internal.owner@emc.com</integrationUser>
        <name>VCETaskUpdateToOracle</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Completion Date Update</fullName>
        <actions>
            <name>Completion_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Update the completion date on task.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Activity Type Task</fullName>
        <actions>
            <name>Populate_Activity_Type_On_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule populates the Activity type custom field.</description>
        <formula>OR(ISCHANGED( Type ) , ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status Update Time Capture</fullName>
        <actions>
            <name>Status_Update_Time_Capture</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5</booleanFilter>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Not Started</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Waiting on someone else</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Deferred</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VCECreateTaskInitialAccept</fullName>
        <actions>
            <name>VCE_Task_Initial_Accept</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow will populate the task initial accept field to NOW() when the last name of the engineer is populated.</description>
        <formula>ISPICKVAL(VCE_Assigned_To__c, &apos;EMC&apos;) &amp;&amp; NOT(ISBLANK(VCE_Owner_Engineer_Last_Name__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VCETaskInsertToOracle</fullName>
        <actions>
            <name>VCETaskInsertToOracle</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>AND(  Transfer__c == true,  NOT(ISPICKVAL(VCE_Last_Modifying_Partner__c, &apos;EMC&apos;))  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>VCETaskUpdateToOracle</fullName>
        <actions>
            <name>VCETaskUpdateToOracle</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>AND(           ! ISNEW(),           Transfer__c ==true,           NOT(ISCHANGED(Transfer__c)),           NOT(ISPICKVAL(VCE_Last_Modifying_Partner__c, &apos;EMC&apos;))          )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
