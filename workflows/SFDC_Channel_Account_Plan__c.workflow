<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Communication_for_CAP_LOCK_by_Partner_Approver</fullName>
        <description>Approval Communication for CAP LOCK by Partner Approver</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>EMC_Sales_Mgt_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Approval_Communication_for_CAP_LOCK_by_Partner_Approver</template>
    </alerts>
    <alerts>
        <fullName>Approval_communication_By_Partner_Approval</fullName>
        <description>Approval communication By Partner Approval</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>EMC_Sales_Mgt_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Approval_communication_By_Partner_Approval</template>
    </alerts>
    <alerts>
        <fullName>Approval_communication_of_QBR_Process_by_Partner_Approver_to_CAM</fullName>
        <description>Approval communication of QBR Process by Partner Approver to CAM</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>EMC_Sales_Mgt_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Approval_communication_of_QBR_Process_by_Partner_Approver</template>
    </alerts>
    <alerts>
        <fullName>Communicates_to_the_Partner_that_the_CAP_has_been_locked</fullName>
        <description>Communicates to the Partner that the CAP has been locked</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>EMC_Sales_Mgt_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Partner_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Communicates_to_the_Partner_that_the_CAP_has_been_locked</template>
    </alerts>
    <alerts>
        <fullName>Notification_Communication_to_Partner_Portal_on_CAP</fullName>
        <description>Notification Communication to Partner Portal on CAP</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notification_Communication_to_Partner_Portal_on_CAP</template>
    </alerts>
    <alerts>
        <fullName>Notification_For_Quarterly_Review_With_Parnter</fullName>
        <description>Notification For Quarterly Review With Parnter</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>EMC_Sales_Mgt_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notification_For_Quarterly_Review_With_Partner</template>
    </alerts>
    <alerts>
        <fullName>Notification_communication_for_QBR_Process_to_EMC_Approver</fullName>
        <description>Notification communication for QBR Process to EMC Approver</description>
        <protected>false</protected>
        <recipients>
            <field>EMC_Sales_Mgt_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notification_communication_for_QBR_Process_to_EMC_Approver</template>
    </alerts>
    <alerts>
        <fullName>Notification_communication_for_QBR_Process_to_Partner_Approver</fullName>
        <description>Notification communication for QBR Process to Partner Approver</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notification_communication_for_QBR_Process_to_Partner_Approver</template>
    </alerts>
    <alerts>
        <fullName>Notification_communication_to_Partner_Approver</fullName>
        <description>Notification communication to Partner Approver</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notification_communication_to_Partner_Approval</template>
    </alerts>
    <alerts>
        <fullName>Notification_communication_to_Regional_Sales_Management</fullName>
        <description>Notification communication to Regional Sales Management</description>
        <protected>false</protected>
        <recipients>
            <field>EMC_Sales_Mgt_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notification_communication_to_Regional_Sales_Management</template>
    </alerts>
    <alerts>
        <fullName>Rejection_communication_By_Partner_Approval</fullName>
        <description>Rejection communication By Partner Approval</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>EMC_Sales_Mgt_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rejection_communication_By_Partner_Approval</template>
    </alerts>
    <alerts>
        <fullName>Rejection_communication_of_QBR_Process_by_EMC_Approver_to_CAM</fullName>
        <description>Rejection communication of QBR Process by EMC Approver to CAM</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rejection_communication_of_QBR_Process_by_EMC_Approver_to_CAM</template>
    </alerts>
    <alerts>
        <fullName>Rejection_communication_to_Regional_Sales_Mgmts</fullName>
        <description>Rejection communication to Regional Sales Mgmts</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rejection_communication_to_Regional_Sales_Mgmt</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Status_for_Approval_By_EMC_User</fullName>
        <description>Once CAP is submitted for EMC Approval, update Status to Pending EMC Approval.</description>
        <field>Status__c</field>
        <literalValue>Pending EMC Approval</literalValue>
        <name>Update Status for Approval By EMC User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_for_Approval_By_Partner</fullName>
        <field>Status__c</field>
        <literalValue>Pending Partner Approval</literalValue>
        <name>Update Status for Approval By Partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_if_Rejected_By_Approver</fullName>
        <description>Update Planning Status to New if Approver Rejects the CAP.</description>
        <field>Status__c</field>
        <literalValue>New</literalValue>
        <name>Update Status if Rejected By Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Approval communication for QBR Process by Partner Approver</fullName>
        <actions>
            <name>Approval_communication_of_QBR_Process_by_Partner_Approver_to_CAM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Once the Partner updates the status to “Partner Approved” a notification would be sent to the EMC CAM to proceed with the final lock of the plan using the “Lock CAP” checkbox.</description>
        <formula>AND( ISPICKVAL(QBR_Approval_Status__c,&apos;Partner Approved&apos;), ISCHANGED(QBR_Approval_Status__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Communication on CAP Approval By Partner Approver</fullName>
        <actions>
            <name>Approval_communication_By_Partner_Approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If the Channel Account Plan is approved by the designated partner approver, a notification should be sent to the Account Owner and the internal management who approved it</description>
        <formula>AND( ISPICKVAL(Status__c,&apos;Approved&apos;), ISCHANGED(Status__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Communication on CAP Rejection By Partner Approver</fullName>
        <actions>
            <name>Rejection_communication_By_Partner_Approval</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Status_if_Rejected_By_Approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the plan is rejected by the designated partner approver, a notification should be sent to the Account Owner and the internal management who approved it with the rejection reason in the notification.</description>
        <formula>AND( ISPICKVAL( Status__c,&apos;Partner Rejected&apos;), ISCHANGED(Status__c)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Communication to the Partner that the CAP has been locked</fullName>
        <actions>
            <name>Communicates_to_the_Partner_that_the_CAP_has_been_locked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This notification communicates to the Partner that the CAP has been locked for the previous period.</description>
        <formula>AND(  Lock_CAP__c, ISCHANGED(Lock_CAP__c)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notification Communication to EMC Approver to Update QBR Approval Status</fullName>
        <actions>
            <name>Notification_communication_for_QBR_Process_to_EMC_Approver</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>To initiate the QBR Process, the CAM would check the “Initiate QBR Process” checkbox on the CAP.This would send a notification to the EMC Approver to update the status of the plan.</description>
        <formula>AND( Initiate_QBR_Process__c,  OR( ISNEW(),  ISCHANGED(Initiate_QBR_Process__c) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notification For Quarterly Review every Quarter with Partner</fullName>
        <actions>
            <name>Notification_For_Quarterly_Review_With_Parnter</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This notification communicates to the CAM that it is time to hold the quarterly business review (QBR) with the partner/distributor.</description>
        <formula>Quarterly_Notification_Date__c=Today()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification communication for QBR Process to Partner Approver</fullName>
        <actions>
            <name>Notification_communication_for_QBR_Process_to_Partner_Approver</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Once EMC Approver updates the “QBR Approval Status” field to “EMC Approved”, a notification then would be sent to the Partner User to update the status to “Partner Approved” or “Partner Rejected.”</description>
        <formula>AND( ISPICKVAL(QBR_Approval_Status__c,&apos;EMC Approved&apos;), ISCHANGED(QBR_Approval_Status__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notification to Partner Portal</fullName>
        <actions>
            <name>Notification_Communication_to_Partner_Portal_on_CAP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The designated Partner users should be notified that the Channel Account Plan is visible and accessible in the Partner Portal.</description>
        <formula>AND( NOT(ISBLANK(Partner_Approver__c)), OR( ISNEW(), ISCHANGED(Partner_Approver__c) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rejection communication of QBR Process by EMC Approver to CAM</fullName>
        <actions>
            <name>Rejection_communication_of_QBR_Process_by_EMC_Approver_to_CAM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If the plan is rejected by EMC Approver, the CAM Should be able to re-initiate the QBR Process in which case the notification should be sent again.</description>
        <formula>AND( ISPICKVAL(QBR_Approval_Status__c,&apos;EMC Rejected&apos;), ISCHANGED(QBR_Approval_Status__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
