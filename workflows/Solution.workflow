<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>NewSolutionCreated</fullName>
        <description>New Solution Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>SalesOps</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>InternalOnly/NewSolutionCreatedNotification</template>
    </alerts>
    <alerts>
        <fullName>Solutions_Email_Alert_at_Solution_Submission</fullName>
        <description>Presales Solutions_Email Alert at Solution Submission</description>
        <protected>false</protected>
        <recipients>
            <recipient>kothawade_prasad@emc.com.prod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PreSales_Email_Template/Presales_Solution_Approval_Process_Submission_To_Approval</template>
    </alerts>
    <alerts>
        <fullName>Solutions_Email_Notification_Final_Approval</fullName>
        <description>Presales Solutions - Email Notification - Final Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PreSales_Email_Template/Presales_Solutions_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Solutions_Email_Notification_Recalled</fullName>
        <description>Presales Solutions - Email Notification - Recalled</description>
        <protected>false</protected>
        <recipients>
            <recipient>kothawade_prasad@emc.com.prod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PreSales_Email_Template/Presales_Solution_Approval_Process_Recalled</template>
    </alerts>
    <alerts>
        <fullName>Solutions_Email_Notification_Rejected</fullName>
        <description>Presales Solutions - Email Notification - Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PreSales_Email_Template/Presales_Solution_Approval_Process_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Solution_Status_Update_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>PresalesSolution Status Update - Approve</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Solution_Status_Update_Recall</fullName>
        <field>Status</field>
        <literalValue>Draft</literalValue>
        <name>Presales Solution Status Update - Recall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Solution_Status_Update_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Presales Solution Status Update - Reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Solution_Status_Update_Submission</fullName>
        <field>Status</field>
        <literalValue>In Review</literalValue>
        <name>Presales Solution Status Update - Submit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Solution Created</fullName>
        <actions>
            <name>NewSolutionCreated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Solution.Status</field>
            <operation>equals</operation>
            <value>Reviewed</value>
        </criteriaItems>
        <description>Notification email to all BU&apos;s when a new solution is created and visible in Self-Service Portal.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
