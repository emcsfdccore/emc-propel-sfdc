<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Resolved_Field</fullName>
        <description>Populates the Resolved field on the tracking object</description>
        <field>Resolved__c</field>
        <literalValue>1</literalValue>
        <name>Populate Resolved Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PreSales Populate Resolved</fullName>
        <actions>
            <name>Populate_Resolved_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.isResolved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Populates the Presales_Case_SLA_Tracking__c.Resolved__c field when the parent is in a resolved status.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
