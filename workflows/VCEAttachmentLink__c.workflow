<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>VCEAttachmentLinkUpdateToOracle</fullName>
        <apiVersion>8.0</apiVersion>
        <endpointUrl>https://webmailnoint.emc.com</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>vce.internal.owner@emc.com</integrationUser>
        <name>VCEAttachmentLinkUpdateToOracle</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>VCEAttachmentLinkUpdateToOracle</fullName>
        <actions>
            <name>VCEAttachmentLinkUpdateToOracle</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <formula>AND(               (Case__r.VCE_Transfer__c == true),               NOT(ISPICKVAL(Attached_By__c , &apos;EMC&apos;))         )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
