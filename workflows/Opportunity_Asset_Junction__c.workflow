<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OAJ_Unique_Relation_Key</fullName>
        <description>Combination of Related Opportunity and Related Asset so that only one unique combination record is created in system.</description>
        <field>Unique_Key__c</field>
        <formula>Related_Asset__c &amp; &apos;-&apos; &amp; Related_Opportunity__c</formula>
        <name>OAJ Unique Relation Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Unique OAJ records</fullName>
        <actions>
            <name>OAJ_Unique_Relation_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Asset_Junction__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Combination of Related Opportunity and Related Asset so that only one unique combination record is created in system.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
