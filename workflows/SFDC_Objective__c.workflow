<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_to_partner_approver_when_an_objective_is_updated</fullName>
        <description>Notification to partner approver when an objective is updated</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notification_to_partner_approver_when_an_objective_is_updated</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_APJ_Unlocked</fullName>
        <field>RecordTypeId</field>
        <lookupValue>APJ_Objective</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to APJ Unlocked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Americas_Unlocked</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Americas_Objective</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Americas Unlocked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_EMEA_Unlocked</fullName>
        <field>RecordTypeId</field>
        <lookupValue>EMEA_Objective</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to  EMEA Unlocked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Flip Record Type to APJ Unlocked</fullName>
        <actions>
            <name>Update_Record_Type_to_APJ_Unlocked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SFDC_Objective__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>APJ Objective Locked</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type to Americas Unlocked</fullName>
        <actions>
            <name>Update_Record_Type_to_Americas_Unlocked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SFDC_Objective__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Americas Objective Locked</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Flip Record Type to EMEA Unlocked</fullName>
        <actions>
            <name>Update_Record_Type_to_EMEA_Unlocked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SFDC_Objective__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMEA Objective Locked</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notification when Channel Account Plan Objective is updated</fullName>
        <actions>
            <name>Notification_to_partner_approver_when_an_objective_is_updated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The PRM tool will send a notification to a the Partner Approver when a Channel Account Plan Objective is updated by a Channel Manager.</description>
        <formula>AND( $User.Id =  Partner_Approver__r.Id, OR( ISPICKVAL(Channel_Account_Plan__r.Status__c,&apos;Approved&apos;), ISPICKVAL(Channel_Account_Plan__r.Status__c,&apos;At Risk&apos;), ISPICKVAL(Channel_Account_Plan__r.Status__c,&apos;Achieved&apos;) ), OR(RecordTypeId = $Setup.ObectiveRecordTypeId__c.APJObjective__c , RecordTypeId = $Setup.ObectiveRecordTypeId__c.APJObjectiveLocked__c , RecordTypeId = $Setup.ObectiveRecordTypeId__c.EMEAObjective__c , RecordTypeId = $Setup.ObectiveRecordTypeId__c.EMEAObjectiveLocked__c ), NOT( ISNEW()), OR( ISCHANGED(Status__c), ISCHANGED(Days_Left_Over__c), ISCHANGED(Stage__c), ISCHANGED(Tracking_Comments__c), ISCHANGED(Metric_Goal__c), ISCHANGED(Metric_Result__c), ISCHANGED(Goal_Attained__c), ISCHANGED( Objective_Owner__c ) ), CreatedBy.Opt_Out_of_Notifications__c=False )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
