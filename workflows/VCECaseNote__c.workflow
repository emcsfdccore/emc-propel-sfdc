<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>VCECaseNoteUpdateToOracle</fullName>
        <apiVersion>8.0</apiVersion>
        <endpointUrl>https://soagatewaynoint.emc.com/sst/runtime.asvc/com.actional.intermediary.sfdcVceCaseNoteUpdate</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>vce.internal.owner@emc.com</integrationUser>
        <name>VCECaseNoteUpdateToOracle</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>VCECaseNoteUpdateToOracle</fullName>
        <actions>
            <name>VCECaseNoteUpdateToOracle</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>AND(            (Case__r.VCE_Transfer__c == true),             NOT(ISPICKVAL(Last_Modifying_Partner__c, &apos;EMC&apos;))          )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
