<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>OpptyCollab_Email_alert_when_ISR_shares_Oppty_with_APJ_PSC</fullName>
        <description>OpptyCollab_Email alert when ISR shares Oppty with America PSC</description>
        <protected>false</protected>
        <recipients>
            <field>PSC_Approval_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/OpptyCollab_ISR_sends_PSC</template>
    </alerts>
    <alerts>
        <fullName>OpptyCollab_Email_alert_when_ISR_shares_Oppty_with_America_PSC</fullName>
        <description>OpptyCollab_Email alert when ISR shares Oppty with APJ PSC</description>
        <protected>false</protected>
        <recipients>
            <field>PSC_Approval_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/OpptyCollab_ISR_sends_PSC</template>
    </alerts>
    <alerts>
        <fullName>OpptyCollab_Email_alert_when_ISR_shares_Oppty_with_EMEA_PSC</fullName>
        <description>OpptyCollab_Email alert when ISR shares Oppty with EMEA PSC</description>
        <protected>false</protected>
        <recipients>
            <field>PSC_Approval_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/OpptyCollab_ISR_sends_PSC</template>
    </alerts>
    <alerts>
        <fullName>OpptyCollab_Rejected_by_PSC</fullName>
        <description>OpptyCollab_Rejected by PSC</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PRM_Templates/OpptyCollab_RejectedBy_PSC</template>
    </alerts>
    <fieldUpdates>
        <fullName>OpptyCollab_Flip_Type_to_Inactive</fullName>
        <description>Changes Record Type to Inactive</description>
        <field>RecordTypeId</field>
        <lookupValue>Inactive_Registration</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>OpptyCollab Flip Type to Inactive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OpptyCollab Inactive Registration</fullName>
        <actions>
            <name>OpptyCollab_Flip_Type_to_Inactive</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Registration__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected by Partner,Cancelled</value>
        </criteriaItems>
        <description>This workflow changes the Record Type to Inactive based on Status</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OpptyCollab_APJ_Notify PSC</fullName>
        <actions>
            <name>OpptyCollab_Email_alert_when_ISR_shares_Oppty_with_America_PSC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Registration__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted to PSC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Registration__c.Theater1__c</field>
            <operation>equals</operation>
            <value>APJ</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OpptyCollab_Americas_Notify PSC</fullName>
        <actions>
            <name>OpptyCollab_Email_alert_when_ISR_shares_Oppty_with_APJ_PSC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Registration__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted to PSC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Registration__c.Theater1__c</field>
            <operation>equals</operation>
            <value>Americas</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OpptyCollab_EMEA_Notify PSC</fullName>
        <actions>
            <name>OpptyCollab_Email_alert_when_ISR_shares_Oppty_with_EMEA_PSC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Registration__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted to PSC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Registration__c.Theater1__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OpptyCollab_Rejected by PSC</fullName>
        <actions>
            <name>OpptyCollab_Rejected_by_PSC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Registration__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected by PSC</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
