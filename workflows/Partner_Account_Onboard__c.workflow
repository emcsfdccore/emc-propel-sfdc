<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PAO_Return_layout_to_Unlocked</fullName>
        <description>This workflow returns the Partner facing layout to the &apos;Unlocked&apos; layout to remove the message showing the license allowance is reached if the limit has been updated</description>
        <field>RecordTypeId</field>
        <lookupValue>Partner_Unlocked</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PAO- Return layout to Unlocked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Partner_Facing_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Partner_Locked</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Partner Facing Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Filp to Locked Partner Layout</fullName>
        <actions>
            <name>Update_Partner_Facing_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow flips the Partner facing layout to the &apos;Locked&apos; layout to inform the partner user of license count reached.</description>
        <formula>Licenses_Allowed__c  &lt;=  Partner_Users__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Filp to Unlocked Partner Layout</fullName>
        <actions>
            <name>PAO_Return_layout_to_Unlocked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow returns the Partner facing layout to the &apos;Unlocked&apos; layout to remove the message showing the license allowance is reached if the limit has been updated</description>
        <formula>Partner_Users__c &lt; Licenses_Allowed__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
