<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Work_Notification</fullName>
        <description>Work Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Resource__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Work_Notification</template>
    </alerts>
    <rules>
        <fullName>Work Notification</fullName>
        <actions>
            <name>Work_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
