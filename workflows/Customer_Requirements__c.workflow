<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateUniqueCustomerID</fullName>
        <field>Unique_Customer_ID__c</field>
        <formula>Name</formula>
        <name>UpdateUniqueCustomerID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>UniqueCustomerRequirements</fullName>
        <actions>
            <name>UpdateUniqueCustomerID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),AND(NOT(ISNEW()),ISCHANGED(Name)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
