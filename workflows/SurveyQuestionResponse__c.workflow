<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Presales_Survey_CSAT_1</fullName>
        <description>This updates the Response Value field with the Likert Scale value for the survey response (Strongly Disagree = 1)</description>
        <field>Response_Value__c</field>
        <formula>1</formula>
        <name>Presales Survey CSAT 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_Survey_CSAT_2</fullName>
        <description>This updates the Response Value field with the Likert Scale value for the survey response (Disagree = 2)</description>
        <field>Response_Value__c</field>
        <formula>2</formula>
        <name>Presales Survey CSAT 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_Survey_CSAT_3</fullName>
        <description>This updates the Response Value field with the Likert Scale value for the survey response (Neither = 3)</description>
        <field>Response_Value__c</field>
        <formula>3</formula>
        <name>Presales Survey CSAT 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_Survey_CSAT_4</fullName>
        <description>This updates the Response Value field with the Likert Scale value for the survey response (Agree = 4)</description>
        <field>Response_Value__c</field>
        <formula>4</formula>
        <name>Presales Survey CSAT 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Presales_Survey_CSAT_5</fullName>
        <description>This updates the Response Value field with the Likert Scale value for the survey response (Strongly Agree = 5)</description>
        <field>Response_Value__c</field>
        <formula>5</formula>
        <name>Presales Survey CSAT 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Presales Survey CSAT Agree</fullName>
        <actions>
            <name>Presales_Survey_CSAT_4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SurveyQuestionResponse__c.Response__c</field>
            <operation>equals</operation>
            <value>Agree</value>
        </criteriaItems>
        <description>This updates the Response Value field with the Likert Scale value for the survey response</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Presales Survey CSAT Disagree</fullName>
        <actions>
            <name>Presales_Survey_CSAT_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SurveyQuestionResponse__c.Response__c</field>
            <operation>equals</operation>
            <value>Disagree</value>
        </criteriaItems>
        <description>This updates the Response Value field with the Likert Scale value for the survey response</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Presales Survey CSAT Neither</fullName>
        <actions>
            <name>Presales_Survey_CSAT_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SurveyQuestionResponse__c.Response__c</field>
            <operation>equals</operation>
            <value>Neither</value>
        </criteriaItems>
        <description>This updates the Response Value field with the Likert Scale value for the survey response</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Presales Survey CSAT Strongly Agree</fullName>
        <actions>
            <name>Presales_Survey_CSAT_5</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SurveyQuestionResponse__c.Response__c</field>
            <operation>equals</operation>
            <value>Strongly Agree</value>
        </criteriaItems>
        <description>This updates the Response Value field with the Likert Scale value for the survey response</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Presales Survey CSAT Strongly Disagree</fullName>
        <actions>
            <name>Presales_Survey_CSAT_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SurveyQuestionResponse__c.Response__c</field>
            <operation>equals</operation>
            <value>Strongly Disagree</value>
        </criteriaItems>
        <description>This updates the Response Value field with the Likert Scale value for the survey response</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
