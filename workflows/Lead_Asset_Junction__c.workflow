<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>LAJ_Unique_Relation_Key</fullName>
        <description>Combination of Related Lead and Related Asset so that only one unique combination record is created in system.</description>
        <field>Unique_Key__c</field>
        <formula>Related_Asset__c &amp; &apos;-&apos; &amp; Related_Lead__c</formula>
        <name>LAJ Unique Relation Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Unique LAJ records</fullName>
        <actions>
            <name>LAJ_Unique_Relation_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead_Asset_Junction__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Combination of Related Lead and Related Asset so that only one unique combination record is created in system.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
