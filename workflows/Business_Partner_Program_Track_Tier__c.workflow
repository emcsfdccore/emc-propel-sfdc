<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Account_DMR_Tier_AuthReseller</fullName>
        <field>DMR_Tier__c</field>
        <literalValue>Authorized Reseller</literalValue>
        <name>Update Account DMR Tier - AuthReseller</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_DMR_Tier_Gold</fullName>
        <field>DMR_Tier__c</field>
        <literalValue>Gold</literalValue>
        <name>Update Account DMR Tier - Gold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_DMR_Tier_Platinum</fullName>
        <field>DMR_Tier__c</field>
        <literalValue>Platinum</literalValue>
        <name>Update Account DMR Tier - Platinum</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_DMR_Tier_Silver</fullName>
        <field>DMR_Tier__c</field>
        <literalValue>Silver</literalValue>
        <name>Update Account DMR Tier - Silver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Disti_Tier_Base</fullName>
        <field>Distributor_Tier__c</field>
        <literalValue>Base</literalValue>
        <name>Update Account Disti Tier - Base</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Disti_Tier_VA</fullName>
        <field>Distributor_Tier__c</field>
        <literalValue>Value Accelerator</literalValue>
        <name>Update Account Disti Tier - VA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_SP_Tier_AuthReseller</fullName>
        <field>Velocity_Solution_Provider_Tier__c</field>
        <literalValue>Authorized Reseller</literalValue>
        <name>Update Account SP Tier - AuthReseller</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_SP_Tier_Gold</fullName>
        <field>Velocity_Solution_Provider_Tier__c</field>
        <literalValue>Gold</literalValue>
        <name>Update Account SP Tier - Gold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_SP_Tier_Platinum</fullName>
        <field>Velocity_Solution_Provider_Tier__c</field>
        <literalValue>Platinum</literalValue>
        <name>Update Account SP Tier - Platinum</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_SP_Tier_Silver</fullName>
        <field>Velocity_Solution_Provider_Tier__c</field>
        <literalValue>Silver</literalValue>
        <name>Update Account SP Tier - Silver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>BPP DMR Current AuthReseller</fullName>
        <actions>
            <name>Update_Account_DMR_Tier_AuthReseller</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the DMR Track within the current time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;DMR&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Authorized Reseller&quot;) ,  Tier_Start_Date__c &lt;= TODAY(),  Tier_End_Date__c &gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BPP DMR Current Gold</fullName>
        <actions>
            <name>Update_Account_DMR_Tier_Gold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the DMR Track within the current time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;DMR&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Gold&quot;) ,  Tier_Start_Date__c &lt;= TODAY(),  Tier_End_Date__c &gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BPP DMR Current Platinum</fullName>
        <actions>
            <name>Update_Account_DMR_Tier_Platinum</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Solution Provider field within the current time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;DMR&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Platinum&quot;) ,  Tier_Start_Date__c &lt;= TODAY(),  Tier_End_Date__c &gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BPP DMR Current Silver</fullName>
        <actions>
            <name>Update_Account_DMR_Tier_Silver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the DMR Track within the current time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;DMR&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Silver&quot;) ,  Tier_Start_Date__c &lt;= TODAY(),  Tier_End_Date__c &gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BPP DMR Future AuthReseller</fullName>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the DMRTrack for a future time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;DMR&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Authorized Reseller&quot;) ,  Tier_Start_Date__c &gt; TODAY(),  Tier_End_Date__c &gt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Account_DMR_Tier_AuthReseller</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Business_Partner_Program_Track_Tier__c.Tier_Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BPP DMR Future Gold</fullName>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the DMRTrack for a future time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;DMR&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Gold&quot;) ,  Tier_Start_Date__c &gt; TODAY(),  Tier_End_Date__c &gt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Account_DMR_Tier_Gold</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Business_Partner_Program_Track_Tier__c.Tier_Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BPP DMR Future Platinum</fullName>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the DMRTrack for a future time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;DMR&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Platinum&quot;) ,  Tier_Start_Date__c &gt; TODAY(),  Tier_End_Date__c &gt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Account_DMR_Tier_Platinum</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Business_Partner_Program_Track_Tier__c.Tier_Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BPP DMR Future Silver</fullName>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the DMRTrack for a future time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;DMR&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Silver&quot;) ,  Tier_Start_Date__c &gt; TODAY(),  Tier_End_Date__c &gt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Account_DMR_Tier_Silver</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Business_Partner_Program_Track_Tier__c.Tier_Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BPP Disti Current Base</fullName>
        <actions>
            <name>Update_Account_Disti_Tier_Base</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Disti Track within the current time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;Distributor&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Base&quot;) ,  Tier_Start_Date__c &lt;= TODAY(),  Tier_End_Date__c &gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BPP Disti Current Value Accelerator</fullName>
        <actions>
            <name>Update_Account_Disti_Tier_VA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Disti Track within the current time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;Distributor&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Value Accelerator&quot;) ,  Tier_Start_Date__c &lt;= TODAY(),  Tier_End_Date__c &gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BPP Disti Future Base</fullName>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Disti Track for a future time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;Distributor&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Base&quot;) ,  Tier_Start_Date__c &gt; TODAY(),  Tier_End_Date__c &gt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Account_Disti_Tier_Base</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Business_Partner_Program_Track_Tier__c.Tier_Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BPP Disti Future Value Accelerator</fullName>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Disti Track within the current time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;Distributor&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Value Accelerator&quot;) ,  Tier_Start_Date__c &gt; TODAY(),  Tier_End_Date__c &gt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Account_Disti_Tier_VA</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Business_Partner_Program_Track_Tier__c.Tier_Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BPP SP Current Auth Reseller</fullName>
        <actions>
            <name>Update_Account_SP_Tier_AuthReseller</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Solution Provider field within the current time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;Solution Provider&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Authorized Reseller&quot;) ,  Tier_Start_Date__c &lt;= TODAY(),  Tier_End_Date__c &gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BPP SP Current Gold</fullName>
        <actions>
            <name>Update_Account_SP_Tier_Gold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Solution Provider field within the current time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;Solution Provider&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Gold&quot;) ,  Tier_Start_Date__c &lt;= TODAY(),  Tier_End_Date__c &gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BPP SP Current Platinum</fullName>
        <actions>
            <name>Update_Account_SP_Tier_Platinum</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Solution Provider field within the current time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;Solution Provider&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Platinum&quot;) ,  Tier_Start_Date__c &lt;= TODAY(),  Tier_End_Date__c &gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BPP SP Current Silver</fullName>
        <actions>
            <name>Update_Account_SP_Tier_Silver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Solution Provider field within the current time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;Solution Provider&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Silver&quot;) ,  Tier_Start_Date__c &lt;= TODAY(),  Tier_End_Date__c &gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BPP SP Future AuthReseller</fullName>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Solution Provider Track for a future time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;Solution Provider&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Authorized Reseller&quot;) ,  Tier_Start_Date__c &gt; TODAY(),  Tier_End_Date__c &gt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Account_SP_Tier_AuthReseller</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Business_Partner_Program_Track_Tier__c.Tier_Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BPP SP Future Gold</fullName>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Solution Provider Track for a future time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;Solution Provider&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Gold&quot;) ,  Tier_Start_Date__c &gt; TODAY(),  Tier_End_Date__c &gt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Account_SP_Tier_Gold</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Business_Partner_Program_Track_Tier__c.Tier_Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BPP SP Future Platinum</fullName>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Solution Provider Track for a future time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;Solution Provider&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Platinum&quot;) ,  Tier_Start_Date__c &gt; TODAY(),  Tier_End_Date__c &gt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Account_SP_Tier_Platinum</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Business_Partner_Program_Track_Tier__c.Tier_Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BPP SP Future Silver</fullName>
        <active>true</active>
        <description>When a BPP Track and Tier record is created for the Solution Provider Track for a future time period, it will write the value to the account.</description>
        <formula>AND( ISPICKVAL(BPP_Track__c, &quot;Solution Provider&quot;) ,  ISPICKVAL( BPP_Tier__c , &quot;Silver&quot;) ,  Tier_Start_Date__c &gt; TODAY(),  Tier_End_Date__c &gt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Account_SP_Tier_Silver</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Business_Partner_Program_Track_Tier__c.Tier_Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
