<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Reset_Tagged_for_Followup_Flag</fullName>
        <description>Reset the Tagged for Followup Flag to false.</description>
        <field>Tagged_for_Follow_up__c</field>
        <literalValue>0</literalValue>
        <name>Reset Tagged for Followup Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Comments_Last_Modified_By</fullName>
        <field>Comments_Last_Updated_By__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Update Comments Last Modified By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Comments_Last_Modified_Date</fullName>
        <field>Comments_Last_Updated_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Comments Last Modified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Reset Followup Flag</fullName>
        <actions>
            <name>Reset_Tagged_for_Followup_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow sets the Tagged For Follow-up field to false for any FI records where the flag was previously true and the Status Code field is updated.</description>
        <formula>AND( Tagged_for_Follow_up__c , OR( ISCHANGED( Primary_Status_Code__c ) , ISCHANGED( Secondary_Status_Code__c ) )   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update to Comments Updated</fullName>
        <actions>
            <name>Update_Comments_Last_Modified_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Comments_Last_Modified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(  Comments__c  ) || ( ISNEW() &amp;&amp; NOT( ISBLANK( Comments__c) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
