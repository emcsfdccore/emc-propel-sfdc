/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER           WR          DESCRIPTION                               
 |  ====            =========           ==          =========== 
 |  06.10.2012      Smitha             MOJO        Initial Creation.Trigger for inserting records in Opportunity Asset Junction Object.
 |  27.dec.2012     Krishna Pydavula   212171      To post the Feed items on Opportunity. 
 | 27 Feb 2014      Sneha Jain          FR 9667    Commenting the code for chatter post
 | 10 Mar 2014      Jaspreet Singh                  To Post Feed Items on Opportunity
 | 28 Jan 2015      Jaideep Mahato     396342       To invoke 'OpptyCommentsUtils' class after insertion
+========================================================================================================================*/

trigger AfterInsertCreateAssetOptyJnobjRecs on Opportunity (after insert) {
    
     //Trigger BYPASS Logic
    if(CustomSettingBypassLogic__c.getInstance()!=null)
    {
      if(CustomSettingBypassLogic__c.getInstance().By_Pass_Opportunity_Triggers__c)
      {
            return;
            
      }
    }      
    
    List<Id> AssetIDs= new List<Id>();
    String strAssetIds;    
    String dataErrs = '';
    Database.Saveresult[] results;
    Set<Id> oppIds = new Set<Id>();
     //added to create parameters as input to visual flow: Karan 2/2/15 Platform Innovation
    Map<String, Object> chaterPostParams = new Map<String, Object>();
    List<Opportunity_Asset_Junction__c> lstInsertJnObj = new List<Opportunity_Asset_Junction__c>();
    
    for(Opportunity o: Trigger.New)
    {        
        if(o.Asset_Ids__c!=null)
        {
               //Population Value in Asset Id field in opty to a string
               strAssetIds= o.Asset_Ids__c;        
        }     
        if(strAssetIds!=null)
        {
            //Getting comma separated value from string to List of ID.
            AssetIDs= strAssetIds.Split(',');        
        }
        
        for(Id aid :AssetIDs)
        {
            //Creating record in Junciton Object
            Opportunity_Asset_Junction__c jnObj = new Opportunity_Asset_Junction__c();
            jnObj.Related_Asset__c = aid;
            jnObj.Related_Opportunity__c = o.id; 
            jnObj.Related_Account__c=o.accountId;
            lstInsertJnObj.add(jnObj);      
        }        
        system.debug('lstInsertJnObj'+lstInsertJnObj);
        if((lstInsertJnObj!=null)&&(lstInsertJnObj.size() > 0))
        {
            //Inserting records in junction object.
            try{             
                results = Database.Insert(lstInsertJnObj, true);   
            //Commenting the code for chatter feed post - FR 9667
            //SFA_MOJO_OptyLinkDelinkController.Feedpost(o.id);            
            }
            catch(exception e){
                  if(e.getTypeName()=='System.DmlException'){                             
                     o.adderror(e.getdmlmessage(0));
              }
              else{
                  throw e;
              }
            }          
        }
        if(results != null && results.size() >0)
        {
            for(Database.SaveResult sr : results)
            {
                if(!sr.isSuccess())
                {   
                    //Looking for errors if any.                
                    for (Database.Error err : sr.getErrors()) {
                        dataErrs += err.getMessage();
                    }
                    System.debug('An exception occurred while attempting an update on ' + sr.getId());
                    System.debug('ERROR: ' + dataErrs);
                }
            }
        }   

        oppIds.add(o.Id);

    }

        //Added to Restrict TC FAilure
        if(!Test.isRunningTest()){
        //added code to initiate visual flow on stage change: Karan 2/2/15 Platform innovation project
        chaterPostParams.put('OppId', trigger.new[0].Id);
        chaterPostParams.put('loggedInUserId', userinfo.getUserId());
        Flow.Interview.Chatter_Posts PosttoChatter = new Flow.Interview.Chatter_Posts(chaterPostParams);
        PosttoChatter.start();
        chaterPostParams.clear();
        }

    /* code commented as chatter posts will be initiated via visual flows, Karan 2/2/15 Platform innovation project
     // Pass the set of Ids to afterInsertFeedItems method of OpportunityTriggerHandler class
    if(oppIds.size() != 0){
        Visibility_ChatterPost obj = new Visibility_ChatterPost();
        obj.afterInsertFeedItems(oppIds);
    }*/
  
        // Included this code for Opportunity Page Re Design - Feb 2015 Release 
        OpptyCommentsUtils.commentsHistory(Trigger.new,null,Trigger.isInsert);

        //End of Opportunity Page Re Design  - Feb 2015 Release

 

    
}