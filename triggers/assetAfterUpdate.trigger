trigger assetAfterUpdate on Asset__c (after update) {
    list<asset__c> lstAssetForChatterPost = new list<asset__c>();
    set<Id> setAssetIdtForSwapValueUpdate = new set<Id>();
       for(asset__c assetObj : trigger.new){
        if(assetObj.Deffered_to_Renewals__c && assetObj.Deffered_to_Renewals__c != trigger.oldmap.get(assetObj.id).Deffered_to_Renewals__c &&
           assetObj.deferred_on__c == trigger.oldmap.get(assetObj.id).deferred_on__c){
           lstAssetForChatterPost.add(assetObj); 
        }
        if(assetObj.Chatter_Notification_Indicator__c != trigger.oldmap.get(assetObj.id).Chatter_Notification_Indicator__c){
           lstAssetForChatterPost.add(assetObj); 
        }
         //if(assetObj.Swap_Value__c != trigger.oldmap.get(assetObj.id).Swap_Value__c ||
         //Replaced the above if condition with below line for BugFix CDC req# 1741
         if(assetObj.Swap_Trade_In_Cost_Relief_Value__c != trigger.oldmap.get(assetObj.id).Swap_Trade_In_Cost_Relief_Value__c ||
            (assetObj.Product_Family__c != trigger.oldmap.get(assetObj.id).Product_Family__c)){
           setAssetIdtForSwapValueUpdate.add(assetObj.Id); 
        }
    }
    //if(!lstAssetForChatterPost.isempty() ) {
      // new R2R_Asset_Management().postChatterFeed(lstAssetForChatterPost); 
   // }
    if(!setAssetIdtForSwapValueUpdate.isempty()){
       new R2R_Utility().getOpptyRelatedToAsset(setAssetIdtForSwapValueUpdate); 
    }
    
}