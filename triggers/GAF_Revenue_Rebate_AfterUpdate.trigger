/*==========================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE        DEVELOPER       WR      DESCRIPTION                               
 |  ====        =========       ==      =========== 
 |  12/11/2014  Sneha Jain              Trigger on GAF R&R After Update
 |============================================================================================================*/

trigger GAF_Revenue_Rebate_AfterUpdate on GAF_Revenue_Rebate__c (after update){
   
    Map<Id,GAF_Revenue_Rebate__c> GAF_RR_AccMap = new Map<Id,GAF_Revenue_Rebate__c>();
    
    //Collect the Profiled Account Id for each GAF R&R record
    for (GAF_Revenue_Rebate__c gaf : Trigger.new){        
        GAF_RR_AccMap.put(gaf.Profiled_Account__c,gaf);
    }
    
    //Method call to stamp date time on GAF_Data_Refresh_Date__c of Account records
    if(GAF_RR_AccMap.keyset().size()>0){
        PRM_BPP_GAF_RevenueRefreshDate gafRefresh = new PRM_BPP_GAF_RevenueRefreshDate();
        gafRefresh.updateProfiledAccount(GAF_RR_AccMap);
    }   
}