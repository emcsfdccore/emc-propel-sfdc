/**
Date      :  10/3/2012                     
Name      :  Rajeev Satapathy
WR        :  207264      
Description: This trigger is used to update several fields related to task  
WR        :  230704
Description: Added the condition to insert record into OAR Member added custom object   
WR        :  259803
Description: Added the condition for sending the email to lead owner, if task is related to lead and also stop the trigger to fired twice.        
**/

/*
trigger UpdateTypePicklist on Task(before update) { 
            
         if(trigger.isupdate){   
            String Curr = Userinfo.getDefaultCurrency();
            String UName = Userinfo.getName();
            DateTime ModDate = DateTime.now();
            
            set<id> stypes = new set<id>();
            for(recordtype objr : [Select Id From RecordType where sObjectType='Task' and developername = 'Renewals_Record_Type']){
            stypes.add(objr.id);
            }
                   
            for(Task T: Trigger.new){ 
            
             //Create an old and new map so that we can compare values  
              Task oldT = Trigger.oldMap.get(T.ID);    
              Task newT = Trigger.newMap.get(T.ID);
            
              
             //Retrieve the old and new Status Value   
              String OldStat = oldT.Status;
              String NewStat = newT.Status;
              String NewCom = newT.Task_Comments__c;
              String OldCom = oldT.Task_Comments__c;
             
            if(stypes.contains(T.RecordTypeId)){
              
                      
            // To diplay the custom type on records 
               if(T.Type__c != NULL)
                 {                  
                  T.Type = T.Type__c;
                 }    
                
                if(OldStat!= NewStat){
                      
               T.Status_Update_Date_Time__c = DateTime.now();
               }
           
               if (T.Type__c == Null){
                 T.AddError('Please select a value for the field Type');
               }
                  
            }
}          
   
   
// End Of Udpate Condtion

// Insert Record Into OAR Member Added Custom Object   

if(!TaskTriggerHelper.hasAlreadyfired()){
if(trigger.isInsert){
List<OAR_Member_Added__c> objList = new List<OAR_Member_Added__c>();

System.Debug('Stypes ------>' + stypes);
System.Debug('NEw trigger------>' +Trigger.new);
map<id,opportunity> opps = new map<id,opportunity>();
map<id,account> acc = new map<id,account>();
for(Task T: Trigger.new){
if((stypes.contains(T.RecordTypeId))&&(T.Status == 'Completed')){

OAR_Member_Added__c obj = new OAR_Member_Added__c();
obj.Text_1__c = T.Type__c;
obj.Text_2__c = T.Type_Detail__c;
obj.Text_3__c = T.Subject;


// WR# 258057- Code starts here

obj.Text_4__c = 'Renewals Type Email';

//WR# 258057 Code ends above


obj.Task_Comments__c = T.Description;
obj.Text_7__c = T.Status;

if(t.whatid!=null&&t.whatid.getsobjecttype()==opportunity.sobjecttype) {
    opps.put(t.whatid,null);
    }
opps.putAll([select id,Opportunity_Number__c from opportunity where id in :opps.keyset()]);
if(opps.containskey(t.whatid)) {
      String Oppnum = opps.get(t.whatid).Opportunity_Number__c;
      obj.Text_5__c = Oppnum;
   }
   
if(t.whatid!=null&&t.whatid.getsobjecttype()==account.sobjecttype) {
    acc.put(t.whatid,null);
    }
acc.putAll([select id,Name from account where id in :acc.keyset()]);
if(acc.containskey(t.whatid)) {
      String Accname = acc.get(t.whatid).Name;
      obj.Text_6__c = Accname;
   }   

String TempObj = Userinfo.getUserId();
System.Debug('Task Owner........' +TempObj);
if( t.ownerid.getsobjecttype() == user.sobjecttype ) {
  obj.user_1__c = t.ownerid;
}
if( t.CreatedById.getsobjecttype() == user.sobjecttype ) {
  obj.User2__c = t.CreatedById;
}
System.Debug('Trigger.oldMap.get(T.Id).Owner....' + T.Owner);

String urlForObj= URL.getSalesforceBaseUrl().toExternalForm() + '/'+T.WhatId;
obj.Task_Related_To_URL__c = urlForObj;

String RelUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/'+T.Id;
obj.Task_URL__c = RelUrl;

objList.add(obj);
}
}
if(!objList.isEmpty())

insert objList;
TaskTriggerHelper.setAlreadyfired();
}
}
   }
}


*/

trigger UpdateTypePicklist on Task(after Insert,after Update) { 

// Insert Record Into OAR Member Added Custom Object   
System.debug('########');
if(!TaskTriggerHelper.hasAlreadyfired()){
TaskTriggerHelper.setAlreadyfired();

System.debug('******* After Insert');
List<OAR_Member_Added__c> objList = new List<OAR_Member_Added__c>();
List<Attachment> a1=new List<Attachment>();

 // System.Debug('Stypes ------>' + stypes);
System.Debug('NEw trigger------>' +Trigger.new);
map<id,opportunity> opps = new map<id,opportunity>();
map<id,account> acc = new map<id,account>();
map<id,lead> mapled = new map<id,lead>(); 
for( Integer i=0;i<Trigger.new.size();i++){

if(((Trigger.isUpdate && Trigger.new[i].status != Trigger.old[i].status) || Trigger.isInsert)){



for(Task T: Trigger.new){

System.debug('******* before Insert');
 set<id> stypes = new set<id>();
 for(recordtype objr : [Select Id From RecordType where sObjectType='Task' and developername = 'Renewals_Record_Type']){
 stypes.add(objr.id);
 }

if(stypes.contains(T.RecordTypeId)&&(T.Status == 'Completed')){

OAR_Member_Added__c obj = new OAR_Member_Added__c();
obj.Text_1__c = T.Type__c;
obj.Text_2__c = T.Type_Detail__c;
obj.Text_3__c = T.Subject;



// WR# 258057- Code starts here

obj.Text_4__c = 'Renewals Type Email';
System.debug('******* renewals type' + obj.Text_4__c );


//WR# 258057 Code ends above


obj.Task_Comments__c = T.Description;
obj.Text_7__c = T.Status;

if(t.whatid!=null&&t.whatid.getsobjecttype()==opportunity.sobjecttype) {
    opps.put(t.whatid,null);
    }
opps.putAll([select id,Opportunity_Number__c from opportunity where id in :opps.keyset()]);
if(opps.containskey(t.whatid)) {
      String Oppnum = opps.get(t.whatid).Opportunity_Number__c;
      obj.Text_5__c = Oppnum;
      Opportunity Opp1 = [select account.name from opportunity where id=:T.Whatid];
      obj.Text_6__c=Opp1.account.name;
   }
  
if(t.whatid!=null&&t.whatid.getsobjecttype()==account.sobjecttype) {
    acc.put(t.whatid,null);
    }
acc.putAll([select id,Name from account where id in :acc.keyset()]);
if(acc.containskey(t.whatid)) {
      String Accname = acc.get(t.whatid).Name;
      obj.Text_6__c = Accname;
   }   

String TempObj = Userinfo.getUserId();
System.Debug('Task Owner........' +TempObj);
if( t.ownerid.getsobjecttype() == user.sobjecttype ) {
  obj.user_1__c = t.ownerid;
}

if( t.CreatedById.getsobjecttype() == user.sobjecttype && t.CreatedById != null) {
  obj.User2__c = t.CreatedById;
}

System.Debug('Trigger.oldMap.get(T.Id).Owner....' + T.Owner);

String urlForObj= URL.getSalesforceBaseUrl().toExternalForm() + '/'+T.WhatId;
obj.Task_Related_To_URL__c = urlForObj;

String RelUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/'+T.Id;
obj.Task_URL__c = RelUrl;

// for lead owner
/*
If(t.whoid != null ){

String urlForObj1= URL.getSalesforceBaseUrl().toExternalForm() + '/'+T.WhoId;
obj.Task_Related_To_URL__c = urlForObj1;
lead led=[select ownerid,Related_Account__r.Name from lead where id=:t.whoid];
obj.Text_6__c = led.Related_Account__r.Name;

for(user usr:[select email from user where id=:led.ownerid]){ 
obj.Email_notification_1__c=usr.email; 
system.debug('email add'+usr.email); 
}

}
*/

if(t.whoid!=null&&t.whoid.getsobjecttype()==lead.sobjecttype) {
   mapled.put(t.whoid,null);
   }
mapled.putAll([select ownerid,Related_Account__r.Name from lead where id in :mapled.keyset()]);
if(mapled.containskey(t.whoid)) {

   String ledid = mapled.get(t.whoid).ownerid;
  // System.debug('ledid'+ledid);
   // lead led1 = [select Related_Account__r.Name from lead where id=:T.Whoid];
   // obj.Text_6__c=led1.Related_Account__r.Name;
   obj.Text_6__c=mapled.get(t.whoid).Related_Account__r.Name;
   String urlForObj1= URL.getSalesforceBaseUrl().toExternalForm() + '/'+T.WhoId;
   obj.Task_Related_To_URL__c = urlForObj1;
  // system.debug('obj.Task_Related_To_URL__c '+obj.Task_Related_To_URL__c );
      for(user usr:[select email from user where id=:ledid]){ 
      obj.Email_notification_1__c=usr.email; 
    //  system.debug('obj.Email_notification_1__c'+obj.Email_notification_1__c);
    
      }
   }
// for Attachment on email template

for(Attachment a:[Select name,body,Id,parentid From Attachment where parentid=:t.id limit 1]){
attachment at=new attachment();
at.name=a.name;
at.body=a.body;
at.parentid='00X70000001ST05';
a1.add(at);
insert a1[0];
}

// obj.Text_6__c = acc1.name;

objList.add(obj);

}
}
if(!objList.isEmpty())
insert objList;
delete a1;


}

}

  } 

   
// update several fields related to task  

    
if(trigger.isBefore){   
            String Curr = Userinfo.getDefaultCurrency();
            String UName = Userinfo.getName();
            DateTime ModDate = DateTime.now();
            
            set<id> stypes = new set<id>();
            for(recordtype objr : [Select Id From RecordType where sObjectType='Task' and developername = 'Renewals_Record_Type']){
            stypes.add(objr.id);
            }
                   
            for(Task T: Trigger.new){ 
            
             //Create an old and new map so that we can compare values  
              Task oldT = Trigger.oldMap.get(T.ID);    
              Task newT = Trigger.newMap.get(T.ID);
            
              
             //Retrieve the old and new Status Value   
              String OldStat = oldT.Status;
              String NewStat = newT.Status;
              String NewCom = newT.Task_Comments__c;
              String OldCom = oldT.Task_Comments__c;
             
            if(stypes.contains(T.RecordTypeId)){
              
                      
            // To diplay the custom type on records 
               if(T.Type__c != NULL)
                 {                  
                  T.Type = T.Type__c;
                 }    
                
                if(OldStat!= NewStat){
                      
               T.Status_Update_Date_Time__c = DateTime.now();
               }
           
               if (T.Type__c == Null){
                 T.AddError('Please select a value for the field Type');
               }
                  
            }
}    
}      

  
   
// End Of Udpate Condtion




   }