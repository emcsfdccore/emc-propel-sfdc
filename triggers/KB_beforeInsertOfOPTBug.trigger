trigger KB_beforeInsertOfOPTBug on OPT_Bug__c(after insert, after update, before insert, before update){

Set<Id> setArticleNo = new Set<Id>();
Set<Id> setArticleNo1 = new Set<Id>();
for(OPT_Bug__c objOPTBug: trigger.new){
    if(Trigger.isBefore){
        String srNumber = objOPTBug.Article_Number__c;
        srNumber = srNumber.leftPad(9);
        srNumber = srNumber.Replace(' ','0');
        objOPTBug.Article_Number__c = srNumber; 
    }
    setArticleNo.add(objOPTBug.Id);
    if(Trigger.isAfter && Trigger.isUpdate && objOPTBug.Article_Number__c != trigger.oldMap.get(objOPTBug.Id).Article_Number__c){
        setArticleNo1.add(objOPTBug.Id);        
    }
}
Map<String,KnowledgeArticleVersion> mapKnowArt = new Map<String,KnowledgeArticleVersion>();
List<KnowledgeArticleVersion> lstKnowArt = new List<KnowledgeArticleVersion>();
//List<KnowledgeArticleVersion> lst = [SELECT id,ArticleNumber, KnowledgeArticleId FROM KnowledgeArticleVersion WHERE PublishStatus = 'Online' and Language = 'en_US' and ArticleNumber='000155762' limit 1];



//lstKnowArt = [Select id,KnowledgeArticleId,ArticleNumber,PublishStatus,CreatedById from KnowledgeArticleVersion where ArticleNumber in:setArticleNo and PublishStatus = 'Online' and Language = 'en_US' ];

system.debug('setArticleNo1--->'+setArticleNo1);
if(Trigger.isInsert && Trigger.isAfter && !KB_CalculateCountOfSR.hasAlreadyChangedShare())
KB_CalculateCountOfSR.getKnowledgeId(setArticleNo,false);
if(Trigger.isUpdate && Trigger.isAfter && setArticleNo1.size()>0 && !KB_CalculateCountOfSR.hasAlreadyChangedShare())
KB_CalculateCountOfSR.getKnowledgeId(setArticleNo1,false);
/*for(KnowledgeArticleVersion knowArtObj: lstKnowArt){
    mapKnowArt.put(knowArtObj.ArticleNumber,knowArtObj);
}
system.debug('mapKnowArt--->'+mapKnowArt);
for(OPT_Bug__c lnkObj :trigger.new){
    lnkObj.Originally_Created_By__c = mapKnowArt.get(lnkObj.Article_Number__c).CreatedById;
    lnkObj.Article_Version_ID__c = mapKnowArt.get(lnkObj.Article_Number__c).Id;
}
*/
}