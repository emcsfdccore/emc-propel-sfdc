/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req                  DESCRIPTION                               
 |  ====            =========       ======                  =========== 
 |  11-02-2013      Anand Sharma    System Administration   
 | 
 +=========================================================================================================================*/

trigger afterInsertSystemAdminRequestLog on System_Admin_Request_Logs__c (after insert) {
    SystemAdministration_Utility objUtils = new SystemAdministration_Utility();
    objUtils.syncSystemAdminLogWithSystemAdminUser(trigger.new);
}