trigger beforeInsertBeforeUpdateLeadAssetJunction on Lead_Asset_Junction__c (before Insert) {
    R2R_Utility.validateLeadLinking(trigger.new);
}