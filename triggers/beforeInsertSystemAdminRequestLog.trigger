/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req                  DESCRIPTION                               
 |  ====            =========       ======                  =========== 
 |  11-02-2013      Anand Sharma    System Administration   
 | 
 +=========================================================================================================================*/

trigger beforeInsertSystemAdminRequestLog on System_Admin_Request_Logs__c (before insert) {
    SystemAdministration_Utility.populateValuesOnLog(trigger.new);
}