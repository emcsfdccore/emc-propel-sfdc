trigger CaseAfterTrigger on Case (after insert,after update) {
	System.debug('In CaseAfterTriggerHelper.isCaseAfterTriggerRunning--------> '+CaseAfterTriggerHelper.isCaseAfterTriggerRunning);
    if(CaseAfterTriggerHelper.isCaseAfterTriggerRunning){
        System.debug('CaseAfterTrigger is already running..This is in IF & not executing else part');
        //Its already running.
    }else{    
        //CaseAfterTriggerHelper.isCaseAfterTriggerRunning = true;
        List<case> caseList = new List<case>();
        List<case> caseForCP = new List<case>();
        if(Trigger.isInsert && Trigger.isAfter){
            caseList = trigger.new;
            System.debug('In CaseAfterTrigger--------> '+caseList.size());
            if(caseList.size() >0){

                CaseAfterTriggerHelper.CaseCompetitorProductCreator(caseList);
            }
            for(case c : trigger.new){
                if(c.Competitor_New__c != null){
                    caseForCP.add(c); 
                }
            }
            if(caseForCP.size() > 0){
                //CaseAfterTriggerHelper.CaseInitialCompetitorProductCreator(caseForCP);
            }
            
        }
       else if(Trigger.isUpdate && Trigger.isAfter && CaseAfterTriggerHelper.isInitialCompetitorUpdate){
            for(case c : trigger.new){
                case cOld = Trigger.oldMap.get(c.id);
                if((c.Competitor_New__c != null  && c.Competitor_New__c != cOld.Competitor_New__c) || (c.Competitor_Product_New__c != cOld.Competitor_Product_New__c) ){
                    caseForCP.add(c);
                }
            }
            If(caseForCP.size() > 0 ){
                CaseAfterTriggerHelper.CaseInitialCompetitorProductCreator(caseForCP,trigger.newMap);
            }
        }
        
    }
}