/*===========================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER                WR       DESCRIPTION                               

 |  ====          =========                ==       =========== 

 |18July2013   Ganesh Soma                262901    Added Custom Setting logic to Bypass duplicate creation of Groups                                  
 +=====================================================================================================================================================================*/
trigger CreateIndividualUserGroup on User (after insert) {
	
	 //Added by Ganesh:WR#262901
	//Get the Custom setting flag to check duplicate Group creation
	ByPass_Duplicate_Group_Creation_Settings__c FlagByPassDuplicateGroup = ByPass_Duplicate_Group_Creation_Settings__c.getValues('ByPass_Duplicate_Group');
	if(FlagByPassDuplicateGroup!=null)
	{	
	   boolean ByPass = (boolean)FlagByPassDuplicateGroup.ByPass_Duplicate_Group_Creation__c;  
	   System.debug('SFDCDEV**********ByPass******'+ByPass);
	   //if 'ByPass' flag is TRUE then NO NEED to execute below logic
       if(!ByPass)
       {
		    //Apex class called to create User Grops and Group Members
		    //User CheckUser=[Select id from User where id=:'00570000001JNZ9'];
		    list<User> lstUserToCreateGroupMember = new list<User>();
		    System.debug('UserInfo.getUserId()***'+UserInfo.getUserId());
		    if(!(UserInfo.getUserId().contains('00570000001JNZ9')))
		    
		    {
		        CreateIndividualUserGroup.CreateGroup(Trigger.New);
		    }
		    for(User userNew:trigger.new){
		        map<string,ProfilesAndGroups__c> mapCustomSetting = ProfilesAndGroups__c.getall();
		        if(!mapCustomSetting.isEmpty() && mapCustomSetting.containsKey(userNew.ProfileId)&& userNew.isActive){
		            lstUserToCreateGroupMember.add(userNew);
		        }      
		    }
		    if(lstUserToCreateGroupMember.size()>0){
		        PRM_InsertDeleteGroupMembers obj = new PRM_InsertDeleteGroupMembers();
		        obj.insertGroupMember(lstUserToCreateGroupMember);
		    }  
       }  
	}
}