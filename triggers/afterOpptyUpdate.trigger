/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 | 12/09/2012    Hemavathi N M   204033       Trigger for updating record on Pricing Request Share object on modification of oppty owner
 
 | 08/05/2013    Hemavathi N M   244102       Trigger when certain fields are updated on oppty Page.         
 | 08/08/2014    Sneha Jain      PROPEL -I.006 Modified condition for update of the PAR statuses                                                                            
 | 09/16/2014    Sneha Jain      WR# 291(b)   R2R Nov'14 Rel - When oppty status changes to COMMIT and the oppty contains any assets, post a chatter message to inform the user to take care of assets before Closing the opportunity                                                                   
  | 02/02/2015    Karan Shekhar                Update code to initiate Visual flow for chatter posts   
 +==================================================================================================================**/
trigger afterOpptyUpdate on Opportunity (after update) {

    Transformation_PricingShare_Class pricObj = new Transformation_PricingShare_Class();
    List<Opportunity> lstOpportunity = new List<Opportunity>();
    Map<Id,Opportunity> mapOpportunity = new map<Id,Opportunity>();
    list<Pricing_Requests__c> lstPAR = new list<Pricing_Requests__c>();
    list<Id> opptyId = new list<Id>();
    list<Id> oldId = new list<id>();
    list<Id> newId = new list<id>();
    //added to create parameters as input to visual flow: Karan 2/2/15 Platform Innovation
    Map<String, Object> chaterPostParams = new Map<String, Object>();
    //WR# 291(b) Creating a set of opportunities on which chatter message needs to be posted - 09/16/2014
    Set<Opportunity> oppIdSet = new Set<Opportunity>();

    if(trigger.isAfter && trigger.isUpdate){
        for(Opportunity prObj : Trigger.new){
           
            if(prObj.Opportunity_Owner__c!=null && (prObj.Opportunity_Owner__c != trigger.oldMap.get(prObj.id).Opportunity_Owner__c) && !prObj.bypass_validation__c){
                newId.add(prObj.Opportunity_Owner__c);
                oldId.add(trigger.oldMap.get(prObj.id).Opportunity_Owner__c);
                mapOpportunity.put(prObj.Id,prObj);
            }
            
            // MAY Release
            if(prObj.StageName == 'Pipeline' || prObj.StageName == 'Upside'|| prObj.StageName == 'Strong Upside'|| prObj.StageName == 'Commit'||prObj.StageName == 'Eval' ){
            //PROPEL I.006 - Modified condition to update PAR when either quote number OR quote version is changed
            //if((prObj.Quote_Cart__c != trigger.oldMap.get(prObj.id).Quote_Cart__c && prObj.Quote_Version__c == trigger.oldMap.get(prObj.id).Quote_Version__c)|| prObj.AccountId != trigger.oldMap.get(prObj.id).AccountId || prObj.Quote_Amount__c != trigger.oldMap.get(prObj.id).Quote_Amount__c || prObj.Partner__c != trigger.oldMap.get(prObj.id).Partner__c || prObj.Tier_2_Partner__c != trigger.oldMap.get(prObj.id).Tier_2_Partner__c){
            if((prObj.Quote_Cart__c != trigger.oldMap.get(prObj.id).Quote_Cart__c || prObj.Quote_Version__c != trigger.oldMap.get(prObj.id).Quote_Version__c)|| prObj.AccountId != trigger.oldMap.get(prObj.id).AccountId || prObj.Quote_Amount__c != trigger.oldMap.get(prObj.id).Quote_Amount__c || prObj.Partner__c != trigger.oldMap.get(prObj.id).Partner__c || prObj.Tier_2_Partner__c != trigger.oldMap.get(prObj.id).Tier_2_Partner__c){
                System.debug('Opportunity fields value changed');
                opptyId.add(prObj.Id);
                }
            } // MAY Release END   
/*   code commented as chatter posts will be initiated via visual flows, Karan 2/2/15 Platform innovation project
            //WR# 291(b) Filling the set of opportunities on which chatter message needs to be posted - 09/16/2014
            if(prObj.StageName != trigger.oldMap.get(prObj.id).StageName && prObj.StageName == 'Commit'){
                oppIdSet.add(prObj);
            } */  
            //added code to initiate visual flow on stage change: Karan 2/2/15 Platform innovation project
            if(prObj.StageName != trigger.oldMap.get(prObj.id).StageName && prObj.StageName == 'Commit' && OpportunityHelperClass.run==true){
                chaterPostParams.put('OppId', prObj.Id);
                chaterPostParams.put('loggedInUserId', userinfo.getUserId());   
                Flow.Interview.Chatter_Posts PosttoChatter = new Flow.Interview.Chatter_Posts(chaterPostParams);
                PosttoChatter.start();  
                chaterPostParams.clear();
                OpportunityHelperClass.run= false;
            }
        }
          /*   code commented as chatter posts will be initiated via visual flows, Karan 2/2/15 Platform innovation project
        //WR# 291(b) - R2R Nov'14 Rel - When oppty status changes to COMMIT and the oppty contains any assets, post a chatter message to inform the user to take care of assets before Closing the opportunity.
        if(oppIdSet != null && oppIdSet.size()>0){
            Set<Opportunity> oppSet = new Set<Opportunity>();
            Map<Id,Integer> oppOAJCountMap = new Map<Id,Integer>();
            List<Opportunity_Asset_Junction__c> oajList = new List<Opportunity_Asset_Junction__c>();
            oajList = [select Id,Related_Opportunity__c from Opportunity_Asset_Junction__c where Related_Opportunity__c in: oppIdSet];
            if(oajList !=null && oajList.size()>0){
                for(Opportunity_Asset_Junction__c oaj :oajList){
                    Integer count = 0;
                    if(oppOAJCountMap != null && oppOAJCountMap.containsKey(oaj.Related_Opportunity__c)){
                        count = oppOAJCountMap.get(oaj.Related_Opportunity__c) + 1;
                        oppOAJCountMap.put(oaj.Related_Opportunity__c,count);
                    }
                    else{
                        oppOAJCountMap.put(oaj.Related_Opportunity__c,1);
                    }
                }
            }
            if(oppOAJCountMap != null && oppOAJCountMap.size()>0){
                for(Opportunity opp: oppIdSet){ 
                    if(oppOAJCountMap.containsKey(opp.Id)){
                        oppSet.add(opp);
                    }
                }
            }
            
            system.debug('---List of opps for chatter post---'+oppSet);
            if(oppSet !=null && oppSet.size()>0){
                R2R_ChatterPost chatPost = new R2R_ChatterPost();
                chatPost.afterUpdateFeedItems(oppSet);
            }
        }*/
        
        System.debug('newId--->'+ newId + 'oldId--->' + oldId);
        if(newId.size() > 0){
            pricObj.updateShare(oldId,Trigger.new);
        } 
        if(mapOpportunity.size() > 0){
            pricObj.updateTheaterFieldOppty(mapOpportunity);
        }
        //MAY Release
        if(opptyId.size()>0){
            System.debug('OpptyID in trigger---->'+ opptyId);
            pricObj.UpdateApprovalStatus(opptyId, lstPAR);
        }
        //MAY Release END
        
        
 
        Transformation_recovery_oppty rObj = new Transformation_recovery_oppty();
        List<Opportunity> lstOppty = new List<Opportunity>();
     
        for(Opportunity opptyObj : Trigger.new){
            if((opptyObj.Name!=null && (opptyObj.StageName == 'Closed' && trigger.oldMap.get(opptyObj.id).StageName != 'Booked'))){
                    lstOppty.add(opptyObj);
            }
        }
        System.debug('lstOppty------>'+lstOppty);
        if(lstOppty.size() > 0){
            rObj.removeRecoveryOppty(lstOppty);
        }
    }
}