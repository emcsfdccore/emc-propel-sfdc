/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER           WR          DESCRIPTION                               
 |  ====            =========           ==          =========== 
 |  05.07.2013      Anirudh Singh       R2R         This trigger will be used to create Lead Asset Junction Records.
 |   
+========================================================================================================================*/


trigger AfterInsertCreateAssetLeadJnobjRecs on Lead (after insert) {
    
     //Trigger BYPASS Logic
    if(CustomSettingBypassLogic__c.getInstance()!=null)
    {
      if(CustomSettingBypassLogic__c.getInstance().By_Pass_Lead_Triggers__c)
      {
            return;
      }
    }
     Map<lead,List<Id>> mapleadWithAssetId = new map<lead,List<id>>();
     List<Id> AssetIDs= new List<Id>();
     String strAssetIds=''; 
    for(lead leadObj: Trigger.New)
    {  
         AssetIDs.clear(); 
         //System.Debug(leadObj.Asset_Ids__c);    
        strAssetIds= leadObj.Asset_Ids__c;  
        if(strAssetIds!=null){
        integer assetIdlength=strAssetIds.length();         
        if(assetIdlength>0)
        {
            
            //Getting comma separated value from string to List of ID.
           if(!strAssetIds.contains(',')){             
               AssetIDs.add(strAssetIds);
           }
           else{               
                AssetIDs= strAssetIds.Split(',');                
           }            
        }}
        if(!assetIds.isempty()){
           mapleadWithAssetId.put(leadObj,assetIds); 
        }
     }   
     if(!mapleadWithAssetId.isempty()){
        R2R_Lead_Management.insertLeadAssetJunction(mapleadWithAssetId);
     }
}