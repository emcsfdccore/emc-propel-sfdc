/*
*  Created By       :- Sunil Arora
*  Created Date     :- 26/02/2010
*  Last Modified By :- Sunil Arora
*  Description      :- This trigger is used to create/Delete Invitees.
   
    Modified History
    1. Modified By:- Vijo Joy
       Modified Reason:- To allow users to modify dates (start,end) when the briefing status is still 'requested'
       Modified on: Jan 3, 2013
       Modified for:- WR#207305
    
    2. Modified By:- Vijo Joy
       Modified Reason:- To set the requestor field, without recursively calling the 'EBC_AddEventFromBE' trigger.
       Modified on: 27th Feb, 2013
       Modified for:- WR#226913
       
    3. Modified By:- Shalabh Sharma
       Modified Reason:- To bypass High Volume Portal users, updated query on user.
       Modified On:- 20th June, 2013
       
    3. Modified By:- Víctor Galván
       Modified Reason:- To fix the process of assigning the Requestor in a Briefing Event. 
       Modified On:- 26th August, 2013
*/
trigger EBC_addInvitees on EBC_Briefing_Event__c (before update,after update) {


    if(trigger.isBefore && trigger.new.size()==1)
    {                          
          //To check whether user has selected correct Briefing Venue or not.
          if(trigger.new[0].Briefing_Venue__c !=null)
           {
               EBC_Briefing_Venue__c[] bv=[Select id from EBC_Briefing_Venue__c where Briefing_Center__c=:trigger.new[0].Briefing_Center__c and Id=:trigger.new[0].Briefing_Venue__c];
               if(bv.size()==0)
               {
                  trigger.new[0].addError(System.Label.Invalid_Briefing_Venue+''); //If user select invalid Briefing Venue
               }                   
           }

            //If user update Briefing_Venue__c then Room_Assignment__c should be null as it will be updated again by VF page.
            if(trigger.new[0].Briefing_Venue__c != trigger.old[0].Briefing_Venue__c)
            {
               trigger.new[0].Room_Assignment__c=null;
            }
            
            //Vijo: WR#207305: modifying the below condition to exclude briefing event with 'requested' status
            //old condition-->if(trigger.new[0].Start_Date__c != trigger.old[0].Start_Date__c || trigger.new[0].End_Date__c != trigger.old[0].End_Date__c)
            if((trigger.new[0].Briefing_Status__c!='Requested'||trigger.old[0].Briefing_Status__c!='Requested') && (trigger.new[0].Start_Date__c != trigger.old[0].Start_Date__c || trigger.new[0].End_Date__c != trigger.old[0].End_Date__c))
            {
                trigger.new[0].Room_Assignment__c=null;
                trigger.new[0].Briefing_Status__c='Reschedule';
            }
        
            //Vijo: WR#226913:27th Feb: modifying the below condition to skip briefing events that has the requested status in either before or after update
           //old condition-->if(trigger.new[0].Requestor_Name__c !=null && trigger.new[0].Requestor_Name__c != trigger.old[0].Requestor_Name__c)
            if((trigger.new[0].Briefing_Status__c!='Requested'||trigger.old[0].Briefing_Status__c!='Requested')&&(trigger.new[0].Requestor_Name__c !=null && trigger.new[0].Requestor_Name__c != trigger.old[0].Requestor_Name__c))
            {
                Contact cont=[Select id,Email from Contact where Id =:trigger.new[0].Requestor_Name__c];
                Boolean userFound = false;
                if(cont.Email!=null)
                {
                    User[] usr=[Select Id,Profile.UserLicenseId from User where Email =:cont.Email and IsActive=true];
                    if(usr.size()>0)
                    {
                    	UserLicense userLic= [Select u.Name, u.Id From UserLicense u WHERE u.Id=:usr[0].Profile.UserLicenseId];
                   //This condition has been added to let that users with license Chatter Free can be Requestor in a 
                   //Briefing Event. If Contact has a user, this must have Salesforce or Salesforce Platform license to 
                   //be Owner of the record. WR #289707
                    	if(userLic.Name=='Salesforce' || userLic.Name=='Salesforce Platform'){
	                        trigger.new[0].Requestor_Owner__c=usr[0].Id;
	                        trigger.new[0].ownerId=usr[0].Id;
	                        userFound = true;
                    	}
                    }
                }
                //If contact does not have a User, we use a Default User that already exists and assign it as Owner
                //of the record. WR #289707
                if(!userFound)
                {
                    User[] usrs=[Select Id from User where Name=:'Default EBC User' Limit 1];
                    if(usrs.size()>0)
                    {
                       trigger.new[0].Requestor_Owner__c=usrs[0].Id;
                       trigger.new[0].ownerId=usrs[0].Id;
                    }   
                }
    

            }                 
    } 
    else if(trigger.isAfter)
    {
         //Deleting the Event record,if Briefing Status is Closed or Cancelled.
           if(trigger.new[0].Briefing_Status__c=='Cancel' && trigger.new[0].Standard_Event_Id__c!=null && trigger.new[0].Standard_Event_Id__c!='')
           {
                Event ev=new Event(Id=trigger.new[0].Standard_Event_Id__c);
                delete ev;
           }
           
           if(trigger.new[0].Room_Assignment__c ==null && trigger.old[0].Room_Assignment__c !=null)
           {
             EBC_Room_Calendar__c[] rc=[Select Id from EBC_Room_Calendar__c where Briefing_Event__c =: trigger.new[0].Id];
             if(rc.size() != 0)
             {
                delete rc;
             }
             
           }
    }
}