trigger createTECCourse on TECCourse_Details__c (before insert) {
    
    system.debug('testing Trigger Start');
    
    List <TECCourse_Details__c> couToInsert = new List <TECCourse_Details__c> ();
    // or whatever your custom object name put instead of arc_emc__Course__c
    List <TEC_Course__c> newCourse= new List <TEC_Course__c> ();

    for (TECCourse_Details__c c : Trigger.new) {
        
        
        if (c  != null) 
        {
          for(String courseModality: c.Modality__c.split(';'))
            {
                // split out the multi-select picklist using the semicolon delimiter
                for(String courseLanguage: c.Language__c.split(';'))
                {
    
                            TEC_Course__c v = new TEC_Course__c (); //instantiate the object to put values for future record
                            v.Abstract__c= c.Abstract__c;
                            
                            system.debug('Abstract'+c.Abstract__c);
                            v.Content_Location__c = c.Content_Location__c;
                              system.debug('Modality'+courseModality);
                            v.Modality__c = courseModality;
                            system.debug('courseLanguage'+courseLanguage);
                            v.Language__c=courseLanguage;
                            system.debug('v courseLanguage'+v.Language__c);
                            v.Video_ITLT_Project__c=c.Video_ITLT_Project__c;
                            v.TEC_Asset__c = c.Asset__c;
                            v.Abstract__c = c.Abstract__c;
                            v.Content_Location__c = c.Content_Location__c;
                            v.Discontinue_From__c = c.Discontinue_From__c;
                            v.Discontinue_Reasons__c = c.Discontinue_Reasons__c;
                            v.Display_For_Learner__c = c.Display_For_Learner__c;
                            v.Duration__c= c.Duration_Hours__c;
                            v.External_Deeplink__c = c.External_Deeplink__c;
                            v.Internal_Deeplink__c = c.Internal_Deeplink__c;
                            //v.Language__c = c.Language__c;
                            v.Maximum_Count__c = c.Maximum_Count__c;
                            v.Minimum_Count__c = c.Minimum__c;
                            //v.Modality__c = c.Modality__c;
                            v.MTL_Location__c = c.MTL_Location__c;
                            v.Price__c = c.Price__c;
                            v.Replace_By__c = c.Replace_By__c;
                            v.Status__c = c.Status__c;
                            v.Test_Mastery_Score__c = c.Test_Mastery_Score__c;
                            v.Training_Units__c = c.Training_Units__c;
                             // v.Module__c = c.Module__c;
                            newCourse.add(v);
                }//end for splitting lang
                
           }  //end of for splitting Modality
            
            
            
        }//end if
        
                
        }
        

   //once loop is done, you need to insert new records in SF
    // dml operations might cause an error, so you need to catch it with try/catch block.
    try {
        system.debug('before inserted');
        insert newCourse; 
    system.debug('Row inserted');
    } catch (system.Dmlexception e) {
        system.debug (e);
    }



    }