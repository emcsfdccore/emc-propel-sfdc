/*===========================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER                WR       DESCRIPTION                               

 |  ====          =========                ==       =========== 

 | 07/10/2010     Karthik Shivprakash      --       This trigger will handle 
                                                    only user after insert
 |                                                  event. 
 
 | 18/10/2010     Arif                     1436     Method 'createAccountShare'
                                                    and 'updateContact'is restricted to
 |                                                  call for Partner User only for 
                                                    avoiding Future call within Future call
 |                                                  exception. 
 
 | 26/10/2010    Arif                      1436     Changed the logic of Identifying Partner
                                                    User by adding Profile.UserLicence.Name 
                                                    
 | 02/01/2012   Accenture              Added method to create Queue    
 
 | 22-Feb-12    Leonard V                            Restirct execution of code for Users 
                                                     having Chatter free licence  
                                                     
 |05-Apr-12    Leonard V                            Changed Execution logic for Chatter free user 
 |18July2013   Ganesh Soma                262901    Invoking method 'CreateIndividualUserGroupsandGroupMembersForPartnerOrInternalUser' of class 'PRM_PartnerUserGroup'                                  
 |28-Oct-2014  Haytham Lotfy                        excluded the High volume customers from method CreateIndividualUserGroupsandGroupMembersForPartnerOrInternalUser 
 |24-Apr-2015   Sneha Jain              Cases for PSC - Added a method to assign new users to Cases for PSC public group
 +=====================================================================================================================================================================*/
trigger UserAfterInsert on User (after insert) {
 
    Set<Id> chatterFreeProfSet = new Set<Id>();
    Map<Id,User> mapNotChatterUsers = new Map<Id,User>();
    Set<Id> adminProfilesSet = new Set<Id>();//Added by Ganesh:WR#262901    
      
    //Added for Chatter Free User Restriction       
    Map<String,Presales_ChatterFree__c> mapChatterProfId = Presales_ChatterFree__c.getall();
    
    for(Presales_ChatterFree__c chatFree : mapChatterProfId.values()){
        chatterFreeProfSet.add(chatFree.Profile_Id__c);
    }
    
    //Added by Ganesh:WR#262901
    //Bypass for 'Administrator', 'EMC System Admin', 'Solution Delivery Team' profiles
    Map<String,Admin_Profiles_Setting__c> mapAdminProfileIds = Admin_Profiles_Setting__c.getall();
    if(mapAdminProfileIds!=null){
        for(Admin_Profiles_Setting__c adminProfiles : mapAdminProfileIds.values()){
            adminProfilesSet.add(adminProfiles.Profile_Id__c);
        }
    }
    
    for (User usr : Trigger.new){       
        if(!(chatterFreeProfSet.contains(usr.ProfileId)) && !(adminProfilesSet).contains(usr.ProfileId) && (usr.UserType != 'CspLitePortal')) {
            mapNotChatterUsers.put(usr.id,usr);
        }
    }   
    //End Of Chatter Free  
 
    PRM_ContactUserSynchronization contactSync = new PRM_ContactUserSynchronization();     
    PRM_PartnerUserGroup Obj = new PRM_PartnerUserGroup();
    set<string> setAccountIdsToCreateQueue = new set<string>();
    Map<Id,User> mapPartnerUsers = new Map<Id,User>();
    Set<Id> partnerUserId = new Set<Id>();
    if(Trigger.isInsert)
    {
        //used to add the users on group based on their profiles on update
       //Obj.createUserGroupOnNew(Trigger.newMap);       
       
        //Added by Ganesh:WR#262901        
        if(mapNotChatterUsers.size()>0)
        {
           //Get the Custom setting flag to check duplicate Group creation
            ByPass_Duplicate_Group_Creation_Settings__c FlagByPassDuplicateGroup = ByPass_Duplicate_Group_Creation_Settings__c.getValues('ByPass_Duplicate_Group');
            if(FlagByPassDuplicateGroup!=null)
            {
               boolean ByPass = (boolean)FlagByPassDuplicateGroup.ByPass_Duplicate_Group_Creation__c;       
               System.debug('SFDCDEV**********ByPass******'+ByPass);  
               if(!ByPass){ //if 'ByPass' flag is False then execute 'createUserGroupOnNew' method     
                  Obj.createUserGroupOnNew(mapNotChatterUsers);
               }else{ //if 'ByPass' flag is TRUE then execute 'CreateIndividualUserGroupsandGroupMembersForPartnerOrInternalUser' method   
                obj.CreateIndividualUserGroupsandGroupMembers(mapNotChatterUsers); 
               } 
            }     
        } 
        
        //used to creat sharing on account during the update of profile to 
        //super user profiles.
        
        //added by Arif(changed the logic)(1436)
        for(User user:[Select Profile.UserLicense.Name,Id
                       from user where Profile.UserLicense.Name = 'Gold Partner' 
                       and Id in:Trigger.newMap.Keyset()]){
           partnerUserId.add(user.Id);             
        }
        if(partnerUserId.size()>0){ 
            for(Id user:partnerUserId){
                if(user == Trigger.newMap.get(user).Id){ 
                    mapPartnerUsers.put(user,Trigger.newMap.get(user));   
                }
            }
             
        } 
        //till this part      
        if(mapPartnerUsers.size()>0){              
            PRM_AccountVisibitlity.createAccountShare(mapPartnerUsers.keyset());
            //This is used to call the updateContact method of PRM_ContactUserSynchronization
            //and pass the New Map and null Map to it.
            contactSync.updateContact(mapPartnerUsers,null);            
        }    
    }


   // for(User userObj :trigger.new){
    for(User userObj :mapNotChatterUsers.values()){
        if(userObj.UserType=='PowerPartner' && userObj.AccountId!=null ){
           setAccountIdsToCreateQueue.add(userObj.AccountId);
        }
    }
    if(setAccountIdsToCreateQueue.size()>0){
       QueueHelper.generateQueueNamesToCreateQueue(setAccountIdsToCreateQueue);
       QueueHelper.QueueCreationExecuted=false;
    }
    
    //Cases for PSC - Added a method to assign new users to Cases for PSC public group - 24-Apr-2015
    PSC_CaseVisibilitySharing pscCaseSharingObj = new PSC_CaseVisibilitySharing();
    pscCaseSharingObj.userAssignmentToPSCGroup(trigger.new,null);    
    
}