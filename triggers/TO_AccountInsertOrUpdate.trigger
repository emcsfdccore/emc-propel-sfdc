/*
 * Created By   :-  Sunil Arora
 * Created Date :-  22nd Sep,2009
 * Modified By  :-  Sunil Arora
 * Modified Date:-  22nd Sep,2009
 * Description  :-  This triggger is used to update TA_Assigment_Type__c field on Account and
                   will run on insert and update event.
 * Modified By  :-  Karthik S
 * WR:- 1077 and 10778  
 * Modified Date:-  22nd Sep,2010
 * Description  :-  This triggger is used to fetch the newly the created accounts
                   to create partner grouping or suggest grouping.
  Defect 104 : Added by Saravanan  
  Added/Modified by Saravanan on 3-Feb 2011 auto populating specality fields Req - 2030.
 * Modified By : Shipra Misra on 20.01.11. Updated Partner Type Picklist field as per the value in Partner Type Multiselect Picklist on account.               
  17-10-2011    Srinivas Nalllapati     WR-178236   Create exemption for certain EMC Classifications for Top Offender
 * Modified By : Arif on 15/11/2011 Updated Trigger to invoke setNonDistributorDirectResllerFlag if Partner Type is changed for Account     
 * Modified By : Accenture on 12/23/2011 Updated Trigger to invoke populateClusterOnAccount based on Account's Billing Country.
 * Modified By : Accenture on 22/01/2011 Updtaed Trigger to incorporate BYpass Logic.Write all the code after Bypass Logic. 
  Arif 197466 : Added part for cloud builder 
  Arif PAN    : Added a flag isPopulateVelocitySpecialty which will restrict the calculation of 'Velocity Specialty Achieved' field(DONOT MIGRATE THIS FILE 
  WITH THIS CHANGE AS THIS IS A PART OF OCTOBER 2012 RELEASE) 
  Arif PAN    : Added call of method 'syncPANAttributes' from PRM_PAN_VPP_Operations class(DONOT MIGRATE THIS FILE 
  WITH THIS CHANGE AS THIS IS A PART OF OCTOBER 2012 RELEASE) 
  Anirudh     : Updated trigger to invoke PRM and specialities deployed functionality for only Partner Accounts. 
  Anand 20-02-2013: Added Isilon logic
  Naga  05-02-2014: Added Counntry,State Classification logic  WR-339289
  Akash Rastogi 25-11-14 Test Class fix for Account lookup filter
  Vivek Barange  17-Feb-2015: Added CPA Lock related Functionlites
  Vivek Barange  10-Apr-2015: #1692 - Make account CPA flag false for null A:D and Partners
 */


trigger TO_AccountInsertOrUpdate on Account (before insert, before update) 
{ 
    //Trigger BYPASS Logic
    if(CustomSettingBypassLogic__c.getInstance()!=null){
        System.Debug('1111--->');
        if(CustomSettingBypassLogic__c.getInstance().By_Pass_Account_Triggers__c){
            return;
        }
    }


    //For WR-178236
    TA_AssignmentRuleAccountUpdate TAAssginmentRuleObj = new TA_AssignmentRuleAccountUpdate(); //Added by Naga WR#339289
    PRM_DealReg_Operations PRMDealRegObj = new PRM_DealReg_Operations();
    List<Account> lstAccountsToUpdate = new List<Account>();
    List<Account> lstAccountsToSetCluster = new List<Account>();

    PRM_AccountGrouping accountObject = new PRM_AccountGrouping();
    if(trigger.isInsert){
        for(Account acc:trigger.new){
            //For fixing test class lookup filter issue
            if(Test.isRunningTest())
                acc.Status__c = 'A';
            //End fixing test class lookup filter issue
            //  Country,State Clasification code added by Naga WR#339289
            if((acc.BillingCountry != null && acc.BillingCountry != '' )&&(acc.BillingState != null && acc.BillingState != '' )){
                TAAssginmentRuleObj.setTAAssignmentType(acc,'Create');
            }
            if(acc.Partner_Type__c != null && acc.Partner_Type__c != '' ){
                lstAccountsToUpdate.add(acc); 
            }
            if(acc.BillingCountry != null && acc.BillingCountry != '' ){           
                lstAccountsToSetCluster.add(acc); 
            }
        }
        //Added for the WR1077 and 1078 by karthik
        accountObject.processAccountForGrouping(trigger.new);
    }
    else{
        //It will update TA_Assigment_Type__c only when BillingState,BillingCountry or EMC_Classification__c gets updated.
        for(Id newAccountId: Trigger.newMap.keySet())
        {
            Account newAccount = Trigger.newMap.get(newAccountId);
            Account oldAccount = Trigger.oldMap.get(newAccountId);
            if( newAccount.BillingCountry!=oldAccount.BillingCountry){
                TAAssginmentRuleObj.setTAAssignmentType(newAccount,'Update');
            }else if((newAccount.BillingState!=oldAccount.BillingState) && (newAccount.BillingCountry=='United States' || newAccount.BillingCountry=='Canada')){
                TAAssginmentRuleObj.setTAAssignmentType(newAccount,'Update'); //WR#339289
            }
        }
    }   
    
    //Start - #1752
    Map<String, boolean> mapLockCPAWithDistrict = new Map<String, boolean>();
    Set<String> accountDistricts = new Set<String>();
    Set<Id> distLookupIds = new Set<Id>();
    for ( Account a : Trigger.new ) {
        if(a.Protect_this_party_as_CPA__c) {
            if(a.Account_District__c != null) {
                accountDistricts.add(a.Account_District__c);
            }
            if(a.District_Lookup__c != null) {
                distLookupIds.add(a.District_Lookup__c);
            }
        }
    }
    
    
    
    Set<String> lockedCPAAccountDistrict = new Set<String>();
    if(!accountDistricts.isEmpty()) {
        
        List<Account> lstAccount = [Select Id, Account_District__c, Protect_this_party_as_CPA__c from Account where District_Lookup__c IN :distLookupIds and Account_District__c = :accountDistricts and type != 'Partner' and isPartner = false and Protect_this_party_as_CPA__c = true];
        
        for(Account acc : lstAccount) {
            lockedCPAAccountDistrict.add(acc.Account_District__c);          
        }
        
    }
    //End - #1752
    
    // Fix for defect 104
    for ( Account a : Trigger.new ) {
        //Start - #1752
        //Commented to restrict error thrown
        /*if(!accountDistricts.isEmpty()) {
            if(Trigger.isInsert || (Trigger.isUpdate && Trigger.oldMap.get(a.Id).Protect_this_party_as_CPA__c != a.Protect_this_party_as_CPA__c)){
                if(lockedCPAAccountDistrict.contains(a.Account_District__c)) {
                    a.Protect_this_party_as_CPA__c.addError(System.Label.CPA_Lock_Already_Exist);               
                }
            }           
        }*/
        //End - #1752
        a.Reporting_Owner__c = a.OwnerId;
        //Added by Shipra on 20.01.11 just updated Partner Type Text from Partner Type. 
        //a.Partner_Type_Text__c = a.Partner_Type__c;
        a.Partner_Type_Picklist__c = a.Partner_Type__c;

        // Added by Saravanan on 3-Feb 2011 auto populating specality fields Req - 2030.       
        if(a.Account_Level__c != '' || a.Account_Level__c !=null && a.IsPartner== true){  
            a.Velocity_Specialties_Achieved__c = '';
            String tmp_velocityAchived = '';
            System.debug('a.Velocity_Specialties_Achieved__c tmp_velocityAchived===>' +tmp_velocityAchived);
            tmp_velocityAchived = tmp_velocityAchived + a.Velocity_Specialties_Achieved__c;

            If ( a.Consolidate_Specialty__c == 'Deployed'){
                if (( a.Velocity_Specialties_Achieved__c != 'Consolidate' 
                        && a.Velocity_Specialties_Achieved__c == 'Advanced Consolidate' ) ||
                        (a.Velocity_Specialties_Achieved__c != '' )) {
                    a.Velocity_Specialties_Achieved__c = 'Consolidate;' ;
                } 
            }else If ( a.Consolidate_Specialty__c == 'Not Deployed'){
                if ( a.Velocity_Specialties_Achieved__c == 'Consolidate' 
                        && a.Velocity_Specialties_Achieved__c != 'Advanced Consolidate' ) {
                    a.Velocity_Specialties_Achieved__c = (tmp_velocityAchived.replace('Consolidate', ''));
                }   
            }

            If ( a.Advanced_Consolidate_Specialty__c == 'Deployed'){
                if (( a.Velocity_Specialties_Achieved__c == 'Consolidate' 
                        && a.Velocity_Specialties_Achieved__c != 'Advanced Consolidate' ) ||
                        (a.Velocity_Specialties_Achieved__c != '' )) {
                    a.Velocity_Specialties_Achieved__c = a.Velocity_Specialties_Achieved__c+'Advanced Consolidate;' ;
                } 
            }else If ( a.Advanced_Consolidate_Specialty__c == 'Not Deployed'){
                if ( a.Velocity_Specialties_Achieved__c != 'Consolidate' 
                        && a.Velocity_Specialties_Achieved__c == 'Advanced Consolidate' ) {
                    a.Velocity_Specialties_Achieved__c = (tmp_velocityAchived.replace('Advanced Consolidate', ''));
                }   
            }

            If ( a.Backup_and_Recovery_Speciality__c == 'Deployed'){
                if ( a.Velocity_Specialties_Achieved__c == 'Backup and Recovery' 
                        || a.Velocity_Specialties_Achieved__c != '' ) {
                    a.Velocity_Specialties_Achieved__c = a.Velocity_Specialties_Achieved__c+'Backup and Recovery;' ;
                } 
            }else If ( a.Backup_and_Recovery_Speciality__c == 'Not Deployed'){
                if ( a.Velocity_Specialties_Achieved__c == 'Backup and Recovery' ) {
                    a.Velocity_Specialties_Achieved__c = (tmp_velocityAchived.replace('Backup and Recovery', ''));
                }   
            }   

            If ( a.Governance_and_Archive_Specialty__c == 'Deployed'){
                if ( a.Velocity_Specialties_Achieved__c == 'Governance and Archive' 
                    || a.Velocity_Specialties_Achieved__c != '' ) {
                a.Velocity_Specialties_Achieved__c = a.Velocity_Specialties_Achieved__c+'Governance and Archive;' ;
            } 
            }else If ( a.Governance_and_Archive_Specialty__c == 'Not Deployed'){
                if ( a.Velocity_Specialties_Achieved__c == 'Governance and Archive' ) {
                    a.Velocity_Specialties_Achieved__c = (tmp_velocityAchived.replace('Governance and Archive', ''));
                }   
            }
            /******************Added logic for Isilon Speciality *******************************************/
            If ( a.Isilon_Track_Specialty__c == 'Deployed'){
                if ( a.Velocity_Specialties_Achieved__c == 'Isilon' 
                        || a.Velocity_Specialties_Achieved__c != '' ) {
                    a.Velocity_Specialties_Achieved__c = a.Velocity_Specialties_Achieved__c+'Isilon;' ;
                } 
            }else If ( a.Isilon_Track_Specialty__c == 'Not Deployed'){
                if ( a.Velocity_Specialties_Achieved__c == 'Isilon' ) {
                    a.Velocity_Specialties_Achieved__c = (tmp_velocityAchived.replace('Isilon', ''));
                }   
            }
            /******************End Added logic for Isilon Speciality *******************************************/

            If ( a.Cloud_Builder_Practice__c == 'Deployed'){
                if ( a.Velocity_Specialties_Achieved__c == 'Cloud Builder' 
                        || a.Velocity_Specialties_Achieved__c != '' ) {
                    a.Velocity_Specialties_Achieved__c = a.Velocity_Specialties_Achieved__c+'Cloud Builder;' ;
                } 
            }else If ( a.Cloud_Builder_Practice__c == 'Not Deployed'){
                if ( a.Velocity_Specialties_Achieved__c == 'Cloud Builder' ) {
                    a.Velocity_Specialties_Achieved__c = (tmp_velocityAchived.replace('Cloud Builder', ''));
                }   
            }

            If ( a.Cloud_Provider_Practice__c == 'Deployed'){
                if ( a.Velocity_Specialties_Achieved__c == 'Cloud Provider' 
                        || a.Velocity_Specialties_Achieved__c != '' ) {
                    a.Velocity_Specialties_Achieved__c = a.Velocity_Specialties_Achieved__c+'Cloud Provider;' ;
                } 
            }else If ( a.Cloud_Provider_Practice__c == 'Not Deployed'){
                if ( a.Velocity_Specialties_Achieved__c == 'Cloud Provider' ) {
                    a.Velocity_Specialties_Achieved__c = (tmp_velocityAchived.replace('Cloud Provider', ''));
                }   
            }

            if (a.Velocity_Specialties_Achieved__c != null){
                a.Velocity_Specialties_Achieved__c = ((string)a.Velocity_Specialties_Achieved__c).replace('null', '');
            }
        }

        System.debug('a.Velocity_Specialties_Achieved__c===>' +a.Velocity_Specialties_Achieved__c);
        // End Req 2030

        if(trigger.isUpdate && a.IsPartner==true){
            /* if(a.Partner_Type__c != null && a.Partner_Type__c != '' &&
             a.Partner_Type__c != trigger.oldmap.get(a.Id).Partner_Type__c){
             lstAccountsToUpdate.add(a);
          }  */
            Map<Id,Account> TriggerOldMap = new Map<Id,Account>();
            Map<Id,Account> TriggerNewMap = new Map<Id,Account>();
            TriggerOldMap.put(a.Id,Trigger.OldMap.get(a.Id));
            TriggerNewMap.put(a.Id,Trigger.NewMap.get(a.Id));
            if(a.Partner_Type__c != trigger.oldmap.get(a.Id).Partner_Type__c){
                lstAccountsToUpdate.add(a);    
            }
            if(a.BillingCountry !=trigger.oldmap.get(a.Id).BillingCountry){
                lstAccountsToSetCluster.add(a); 
            }
            PRM_PAN_VPP_Operations PANOperations = new PRM_PAN_VPP_Operations();
            PANOperations.syncPANAttributes(TriggerOldMap,TriggerNewMap);
        }
        
        //Start: 1692
        if((Trigger.isInsert && (a.Account_District__c == null || a.Type == 'Partner')) || (Trigger.isUpdate && ((Trigger.oldMap.get(a.Id).Account_District__c != a.Account_District__c && a.Account_District__c == null) || (Trigger.oldMap.get(a.Id).Type != a.Type && a.Type == 'Partner')))) {
            a.Customer_Profiled_Account__c = false;
            a.Customer_Profiled_Account_Lookup__c = null;
        }
        //End: 1692
    }
    if(lstAccountsToUpdate.size()>0){      
        PRMDealRegObj.setNonDistributorDirectResllerFlag(lstAccountsToUpdate);
    }
    if(lstAccountsToSetCluster.size()>0){      
        PRMDealRegObj.populateClusterOnAccount(lstAccountsToSetCluster);
    }
}