/*==================================================================================================================+
 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 | 16 Mar 15    Aagesh J    GBS Sprint 3   To prevent chatter comment on closed cases.                                      
 +==================================================================================================================**/

trigger CaseFeedClosedStatusCommennt on FeedComment (before insert) {
if(Trigger.New != null)
    {
     GBS_ChatterOnClosedCases.ClosedCaseChatterComment(Trigger.New);
    }
}