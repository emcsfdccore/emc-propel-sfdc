trigger SkyFall on Idea (after update) {

List<Idea> M = trigger.new;
Map<id, idea> ideaMap = new Map<id, idea>();
public Static List<Skyfall__c> lstSkyfall = new List<Skyfall__c>();
if(M != null && M.size()>0){
   for(Idea qDivision : M){
      if ( (qDivision.status == 'Present to Leadership') && (qDivision.Skyfall__c == null) ) {
      Skyfall__c MI6 = new Skyfall__c();
      MI6.Body__c = qDivision.Body;   
      MI6.Title__c = qDivision.Title;
      MI6.AkashicRecord__c = qDivision.CreatedByID;
      MI6.ownerId = qDivision.CreatedByID;
      MI6.Idea_Submitted_Date__c = qDivision.CreatedDate;
      MI6.Category__c = qDivision.Category;
      MI6.Requirement_Gathering_Date__c = date.TODAY() + 14;
      MI6.Related_Idea__c = qDivision.id;
      lstSkyfall.add(MI6);
      ideaMap.put(qDivision.id, qDivision);
      }
   }
   if(!lstSkyfall.isEmpty()) {
  // try {
       insert lstSkyfall;       
       // link skyfall back to idea
       List<Idea> lstIdea = new List<Idea>();
       for (Skyfall__c skyfall : lstSkyfall){
           if (ideaMap.containsKey(skyfall.Related_Idea__c)) {
               Idea idea = new Idea();
               idea.id = skyfall.Related_Idea__c;
               idea.skyfall__c = skyfall.id;
               lstIdea .add(idea);
           }
       }
       update lstIdea;
    //} catch(Exception e){}
   }
}
}