/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 | 01 Aug 2013      Anirudh Singh   R2R         This Trigger is used to set values on Trade Ins Swap Object for before Update Operation.
 | 03 Mar 2014      Sneha Jain      FR 9658     Preventing override of AFM user field by removing the event condition of 'before update'
+========================================================================================================================*/

trigger beforeUpdatebeforeinsertOnTradeInsCompetitiveSwap on Trade_Ins_Competitive_Swap__c (before insert) {
    list<Trade_Ins_Competitive_Swap__c> lstTradeInsToProcess = new list<Trade_Ins_Competitive_Swap__c>();
    SFA_MOJO_GFSTradeInSwapTriggerCode GFSObj = new SFA_MOJO_GFSTradeInSwapTriggerCode();
    for(Trade_Ins_Competitive_Swap__c tradeIns : trigger.new){
        if(tradeIns.Related_Opportunity__c !=null){
           lstTradeInsToProcess.add(tradeIns);
        }
    }    
    if(lstTradeInsToProcess.size()>0){
        GFSObj.setAFMDetails(lstTradeInsToProcess);
    }
}