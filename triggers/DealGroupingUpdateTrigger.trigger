/*==============================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE          DEVELOPER       WR       DESCRIPTION                               
 |  ====          =========       ==       =========== 
 |  14-01-2015   Akash Rastogi    1611     Updating related opportunity record whenever Deal_Grouping__c record is updated
 +=============================================================================*/

trigger DealGroupingUpdateTrigger on Deal_Grouping__c (after Update) {
    Set<Id> dealGrouping = new Set<Id>();
    for(Deal_Grouping__c dg: trigger.new)
    {
        if(trigger.oldMap.get(dg.Id).Deal_Grouping_Next_Steps__c != dg.Deal_Grouping_Next_Steps__c ){
            dealGrouping.add(dg.Id);
        }
        
    }
    if(!dealGrouping.isEmpty()) {
            Opp_UpdateOwner.updateOpportunity(null, dealGrouping, Trigger.isUpdate); 
            if(Opp_UpdateOwner.isFailed){
                 for(Deal_Grouping__c dg: trigger.new)
                {
                    dg.addError(Opp_UpdateOwner.strErrorMessage);                   
                }
            }
    }


}