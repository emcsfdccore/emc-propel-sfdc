/*=======================================================================================================+
|  HISTORY  |                                                                           
|  DATE          DEVELOPER        WR                    DESCRIPTION                               
|  ====          =========        ==                    =========== 
| 03-Mar-2014    Srikrishna SM    FR-9549          This Trigger is used to populate Comma Separated List of
                                                   Asset Campaign Junction object names on Parent Asset object
+=====================================================================================================*/
trigger AssetCampaignJncAfterInsertUpdateDelete on Asset_Campaign_Junction__c (after insert, after update, after delete) {
     // fires after both insert and update
    if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isAfter){
        
        //Create Parent Id Set
        Set<Id> parentidSet = new Set<Id>();
        //Populate Parent Id Set
        for(Asset_Campaign_Junction__c assetCampaignJun : Trigger.new){
            parentidSet.add(assetCampaignJun.Related_Asset__c);
        }
        // process the Assets
        AssetCampaignJunctionHandler.processAssetCampaignJunctions(parentidSet);  
    }else if(Trigger.isDelete && Trigger.isAfter){
        Set<Id> parentidSet = new Set<Id>();
        for (Id acJun : Trigger.oldMap.keySet()){
            parentidSet.add(Trigger.oldMap.get(acJun).Related_Asset__c);
        }
        // process the Assets
        AssetCampaignJunctionHandler.processAssetCampaignJunctions(parentidSet);
    }
}