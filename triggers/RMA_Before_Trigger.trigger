/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER          WR/Req                  DESCRIPTION                               
 |  ====            =========          ======                  =========== 
 |  20/04/2015      Partha Baruah      RMA May Release       This Trigger is for all before Insert,update events
 +=======================================================================================================================*/
 
 trigger RMA_Before_Trigger on Case_RMA__c ( before update, before insert) {
    
    CaseRMA_Operations obj = new CaseRMA_Operations();
 
   If(trigger.isInsert){
     for(Case_RMA__c CRma :trigger.New){
       CRma.Last_Status_Change__c = system.now();
       If(CRma.RMA_Case_Status__c=='Closed'){
       CRma.addError(system.label.RMA_RestrictStatus_Close);
       }
	   If(CRma.RMA_Case_Status__c=='In Reconciliation'){
	   CRma.addError(system.label.RMA_RestrictStatus_InReconcialtion);
	   }
     }
   }
    If(trigger.isUpdate){
       for(Case_RMA__c CRma :trigger.New){
     
        if(CRma.ownerId!=trigger.oldMap.get(CRma.id).ownerId)
       {
         obj.ownershipCheck(trigger.newMap, trigger.oldMap);
       }
       if(CRma.RMA_Case_Status__c!=trigger.oldMap.get(CRma.id).RMA_Case_Status__c){
       
       CRma.Last_Status_Change__c = system.now();
     
      }
 
   }

}
}