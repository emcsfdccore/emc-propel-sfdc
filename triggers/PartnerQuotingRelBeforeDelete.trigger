/*========================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER       WR        DESCRIPTION                               

 |  ====          =========       ==        =========== 

 |  11 Jun 2014     Sneha Jain    PROPEL I.002 PRM Attibutes Initial Creation - Make an entry in Outbound message Log object for Delete PQR only when the 
                                             combination of "PQA UCID - Related Acc ID - Rel Type" is unique in the database
 +=================================================================================================*/
 
trigger PartnerQuotingRelBeforeDelete on Partner_Quoting_Relationship__c (before delete) {
    
    //Make an entry in Outbound message Log object for Delete PQR only when the combination of "PQA UCID - Related Acc ID - Rel Type" is unique in the database
    PROPEL_PRMAttributesHelper.deletePartnerQuotingRel(Trigger.old);
}