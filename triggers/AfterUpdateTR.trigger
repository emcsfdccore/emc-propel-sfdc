trigger AfterUpdateTR on Transaction_Register__c(after update) {
    GBS_Transaction_Register_CaseCreation.onUpdateTRLogic((Map<Id, Transaction_Register__c>)Trigger.oldMap, (Map<Id,Transaction_Register__c>)Trigger.newMap);
}