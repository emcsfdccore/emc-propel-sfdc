/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  06/10/2014      Sneha Jain      WR-294      R2R Nov'14 Rel: Count the # of Assets on a Lead
 |  06/04/2015      Vivek           CI#1872     Update the Lead related info to asset object
 +=========================================================================================================================*/
Trigger afterInsertAfterDeleteLeadAssetJunction on Lead_Asset_Junction__c (after Insert,after Delete,after update) {
  R2R_Lead_Management leadManagementObj = new R2R_Lead_Management();
  R2R_AssetStatus statusObj = new R2R_AssetStatus();
  List<Lead_Asset_Junction__c> lstTriggeredData = Trigger.new;
  if(trigger.isinsert){
      leadManagementObj.setLeadType(trigger.new);
      statusObj.setsalesPlan(trigger.new);     
      // WR - 294 Passes the inserted LAJ records to the R2R_Lead_Management class
      leadManagementObj.updateLeadAssetCount(trigger.new);
  } 
  if(trigger.isdelete){
      leadManagementObj.setLeadType(trigger.old);
      statusObj.setsalesPlan(trigger.old);
      // WR - 294 Passes the deleted LAJ records to the R2R_Lead_Management class
      leadManagementObj.updateLeadAssetCount(trigger.old);
		lstTriggeredData = trigger.old;	  
  }
	
    //Start: 1872
	R2R_Lead_Management.updateAssetFromLead(lstTriggeredData);
	//End: 1872
	
}