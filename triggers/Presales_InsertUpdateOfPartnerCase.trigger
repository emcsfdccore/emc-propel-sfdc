/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 | 26/04/2012    Leonard Victor            This trigger is used for populating Partner Related Fields on Case
 | 30/05/2012    Leonard Victor            Updated trigger to include partner case creation through SFDC system
 | 09/12/2014    Vivek Barange   #1310     Updated trigger to restrict all profile users to update Case subtype field 
 |                        except Business Admin or Systems Admin profiles
 | 09/12/2014    Akash Rastogi   #1335     Added below code to validate parent field on child case
 | 08/14/2014    Leonard Victor   1236     Updated trigger to include case description lock logic
 | 10/16/2014    Bindu            1475      Updated the trigger to call caseDescriptionLock method
 | 17/02/2014    Akash Rastogi    1837     Commented "If" Condition to bypass the code for all record types
 +==================================================================================================================**/



trigger Presales_InsertUpdateOfPartnerCase on Case (before insert, before update) {
      public static List<Case> lstNewCases = new List<Case>();
  
      public static Boolean updateGrp = false;
      public List<Id> partnerCase = new List<Id>();
      Presales_PartnerCaseOperation prtObj = new Presales_PartnerCaseOperation();
       Presales_CheckRecordType objCheckRecordType = new Presales_CheckRecordType();
       lstNewCases = objCheckRecordType.checkRecordType(trigger.new);
       if(lstNewCases!=null && lstNewCases.size()>0){
        
        
        if(trigger.isInsert){
                
            
              for(Case csLoop : lstNewCases){
          
                if(csLoop.Partner_Grouping_Name__c!=null){
                  partnerCase.add(csLoop.Partner_Grouping_Name__c);
              }
                 // WR#1837
                 //if(csLoop.Record_Type_Developer_Name__c=='Presales_Accounts_Payable_T_E_Vendor_Master_Treasury'||csLoop.Record_Type_Developer_Name__c=='Presales_Global_Revenue_Operations' ||csLoop.Record_Type_Developer_Name__c=='Presales_Credit_Collections'){
                    if(csLoop.Description!=null){
                    if(!csLoop.Description.contains('\r') && csLoop.Description.contains('\n'))
                        csLoop.Description = csLoop.Description.replaceall('\n','\r\n');
                        }
                 // WR#1837
                 //}

              }
            
            if(lstNewCases.size()>0){
            prtObj.insertPartner(lstNewCases ,updateGrp, partnerCase);
            }
            
        }
        
        
      if(trigger.isUpdate){
        
        
        
        for(Case caseLoop : lstNewCases){
        
        if(caseLoop.Partner_Grouping_Name__c!=trigger.oldMap.get(caseLoop.id).Partner_Grouping_Name__c){
            updateGrp = true;
            partnerCase.add(caseLoop.Partner_Grouping_Name__c);
        System.debug('Inside Grouping Update Case');    

            
        }
        
        else if(caseLoop.Contact_Email1__c!=trigger.oldMap.get(caseLoop.id).Contact_Email1__c || caseLoop.ContactId!=trigger.oldMap.get(caseLoop.id).ContactId){
             updateGrp = false;
            partnerCase.add(caseLoop.Partner_Grouping_Name__c);
        }  
            
        
            if(partnerCase.size()>0){
            prtObj.insertPartner(lstNewCases ,updateGrp, partnerCase);
            }

      }
      // #1475 - Added below code to call the caseDescriptionLock
      prtObj.caseDescriptionLock(trigger.new,trigger.oldMap);  

       }
     
  //#1310 - Updated below code to restrict all profile user to update Case subtype field except Business Admin or Systems Admin profiles
  //#1335 - Added below code to validate parent field on child case  
    if(trigger.isUpdate) {
      Presales_PartnerCaseOperation.validateCaseSubType(trigger.new, trigger.oldMap);
      }

    }
}