/*========================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER       WR        DESCRIPTION                               

 |  ====          =========       ==        =========== 

 |  11 Jun 2014     Sneha Jain    PROPEL I.002 PRM Attibutes Initial Creation - When a PSEP record is deleted, insert a record in Outbound Message Log object
 +=================================================================================================*/
 
trigger PartnerServiceEnabledProdAfterDelete on Partner_Service_Enabled_Product__c (after delete) {

    //For all deleted Partner Service Enabled Products records, insert a record in Outbound Message Log object
    PROPEL_PRMAttributesHelper.deletePartnerServiceEnabledProd(Trigger.old);
}