/*===================================================================================================================================
|
|History 
|Date             Developer Name    WR        Details 
|----             --------------    --        -------
|14 January 2012  Anirudh Singh     219167    This trigger is used to invoke calculateTotalCompetency() of EMC_Consulting_Operations 
|                                             class
|11 Jun 2014      Vivek Barange     989       One of the competency must be mandatory On opportunity- Detailed Product.
|14-April-2015    Vinod Jetti       1849       called populatePortfolio method
====================================================================================================================================*/
trigger BeforeUpdateInsertDetailedProduct on Detailed_Product__c (before update,before insert) {
    
    Set<Id> optyIds = new Set<Id>();
    list<Detailed_Product__c> lstDetailedProductToProcess = new list<Detailed_Product__c>();
   //Created for 1849
    List<Detailed_Product__c> lstDetailproducts = new List<Detailed_Product__c>();
    //End of 1849
    for (Detailed_Product__c newDP : Trigger.new) {
        if(Trigger.IsUpdate && Trigger.Old != Trigger.New){
                if(newDP.Total_Competency__c != null ){
                    newDP.Is_Total_Competency_Updated__c = True; 
                }               
                Detailed_Product__c oldDP=Trigger.oldMap.get(newDP.ID);
                if(oldDP.Sub_Practice_1_Dollar__c != newDP.Sub_Practice_1_Dollar__c ||
                oldDP.Sub_Practice_2_Dollar__c != newDP.Sub_Practice_2_Dollar__c ||
                oldDP.Sub_Practice_3_Dollar__c != newDP.Sub_Practice_3_Dollar__c ||
                oldDP.Sub_Practice_4_Dollar__c != newDP.Sub_Practice_4_Dollar__c ||
                oldDP.Sub_Practice_5_Dollar__c != newDP.Sub_Practice_5_Dollar__c)
                {
                  lstDetailedProductToProcess.add(newDP);
                } 
               //Start of 1849
                if(Trigger.oldMap.get(newDP.Id).Offer_aligned_to_play__c != newDp.Offer_aligned_to_play__c) {
                    lstDetailproducts.add(newDP);
                }
                // End of 1849
        }
        else{
          newDP.Is_Total_Competency_Updated__c = False;  
        }   
      if(Trigger.IsInsert){
           if(newDP.Sub_Practice_1_Dollar__c != null || newDP.Sub_Practice_2_Dollar__c !=null ||
              newDP.Sub_Practice_3_Dollar__c != null || newDP.Sub_Practice_4_Dollar__c !=null ||
              newDP.Sub_Practice_5_Dollar__c !=null )
            {   
              lstDetailedProductToProcess.add(newDP);   
            }
            //Start of 1849
            if(newDp.Offer_aligned_to_play__c != null) {
                lstDetailproducts.add(newDP);
            }
            //end of 1849
      }
      
      optyIds.add(newDP.Opportunity__c);
        
    }
    if(lstDetailedProductToProcess.size()>0){
        new EMC_Consulting_Operations().calculateTotalCompetency(lstDetailedProductToProcess);
    }
    //Start of 1849
    if(lstDetailproducts.size()>0){
        EMC_Consulting_Operations.populatePortfolio(lstDetailproducts);
    }
    //End of 1849
    EMC_Consulting_Operations.validateCompetency(Trigger.new, optyIds);
}