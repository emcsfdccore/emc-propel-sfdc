/*==============================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE       DEVELOPER     WR       DESCRIPTION                               
 |  ====       =========     ==       =========== 
 |  21 Sep 2010   Arif      148781      Calling 'populateCompetitorLogicFormulaField'
 |                                      method from class populateCompetitorLogicFormula     
 |  10 Dec 2010   Srinivas  151217      Uncheck 'Update Forecast Amt from Quote' when no Quote attached on Oppty                                                  
 |  08 Mar 2011   Srinivas  161482      Changed Logic for  'Update Forecast Amt from Quote'
 |  18 Jun 2011   Srinivas  163357      Update/Delete SP Tracking date when "Service Provider" Added/Deleted
 |  24-Aug-2011   Srinivas  174234      Fix "Insufficent Privileges" error on Opportunity. When User lost access to some Account record 
                                        which they previously might be having and they select same account on opportunity (may be form recently viewed items),
                                        they strat receiving 'Insifficent priviledges' error.
| 10-Sep-2011     Shipra M  172224      Oppty OAR: Trigger OAR when Account Owner changes from any House Account to a real user.
| 22-Sep-2011     Shipra M  173667      Place Account Theater on the Opportunity into a Text Field 
| 18-nov-2011     Shipra M  173965      New Alliance Fields.Channel visibilty.
| 18-Nov-2011     Kaustav D  NA         PRM Partner Leverage flag checker
| 05-Dec-2011     Shipra M  173965      Added 2 maps to store old & new onwer Id against updated opportunity. 
| 23-Dec-2011    Shipra M   182949      Fix developer script  exception.
  22/01/2011      Accenture             Updated trigger to incorporate ByPass Logic
|                                       NOTE: Please write all the code after BYPASS Logic
| 14/02/2012     Shipra M   187056      Emea Inside sales email alert.
| 29-Mar-2013    Uday A     246616      Updating Initial_Forecast_Quarter__c on Opportunity when ever opportunity is created and deactivated the workflow.
|                                       Also created a new helper class in EMC_ConvertLead to get the theater from a query which was there already. If it hasn't querired then query for theater.
| 18-Jun-2013    Anirudh Singh          Updated code to invoke setOpportunityType() method of R2R_Opportunity_Management class if owner is updated for Renewals Opportunity.
| 19-Nov-2013    Leonard Victor         Added Logic For Assigned Date
| 10-Feb-2014   Jaypal Nimesh           Added Trigger.OldMap parameter in salesChannel method call. For Backward Arrow March Release. WR# 340066
| 10-Feb-2014    Sayan Choudhury        Added logic for Populating Closed By field for CI WR 248
| 14.04.2014    Srikrishna SM   353275  Added Logic to calculate Meddic counter, meddic last updated and meddic last updated by fields
| 09-10-14-    Vivek Barange   #0752      Tigger to make sales_force__c field to auto populated 
|                                        based on the Account Owners BU Attribute. 
| 14-10-14     Akash Rastogi  WR#1424   Added logic to populate 'Opportunity Sales Play' field with selected values from 'Sales Play' picklist
| 20-01-2015   Akash Rastogi  WR#1611   Added logic to populate 'Deal_Grouping_Next_Steps__c' for corresponding 'Deal_Grouping_Name__c' on insert and Update of Opportunity
| 2/4/2015     Bindu          CI 1901   Added logic to populate Term_Notification_Sent_On__c and Booking_Certificate_Sent_On__c fields
+=============================================================================*/
trigger PopulateOpportunityOwner on Opportunity (before insert, before update )
   
{
   //Trigger BYPASS Logic 
   if(CustomSettingBypassLogic__c.getInstance()!=null){
      if(CustomSettingBypassLogic__c.getInstance().By_Pass_Opportunity_Triggers__c){
                return;
      }
   }
   
   Set<Id> dealGroupings = new Set<Id>();
   List<Opportunity> lstOpportunityWithDealGrouping = new List<Opportunity>();
   list<Opportunity> lstOpptyToSetOpptyType = new list<Opportunity>();
    if(!PRM_Partner_Leverage.PRM_Partner_Leverage_flag)
    {
          //Added by Srinivas Nallapati for WR-174234
          AccountUtils acctUtil = new AccountUtils();
          acctUtil.CheckUserAccessForPartnerAccountsOnOpportunity(trigger.new);   
          //end of WR-174234      
          Set<Id> setAccID=new Set<Id>();
          //Added by Srinivas Nallapati for WR-151217
          if(Userinfo.getProfileId() == '00e70000000wRaBAAU')
          {
              system.debug('inside if');
              for(Opportunity oppty: Trigger.New)
              {   
                  
                    if(Trigger.isUpdate) 
                    {
                        Opportunity oldOpp = Trigger.oldMap.get(oppty.Id);
                        if(oppty.Quote_Cart_Number__c == null && oldOpp.Quote_Cart_Number__c != null)
                            oppty.Update_Forecast_Amount_from_Quote__c= false;
                        if(oppty.Quote_Cart_Number__c != null && (oppty.Quote_Cart_Number__c != oldOpp.Quote_Cart_Number__c))
                                oppty.Update_Forecast_Amount_from_Quote__c= true;
                        
                          
                    }
                    if(Trigger.isInsert)
                    { 
                        if(oppty.Quote_Cart_Number__c != null)
                            oppty.Update_Forecast_Amount_from_Quote__c= true;
                    } 
              }     
          }   
          //Update end by Shipra
       //  Code Added for CI WR 248 //     
        if(trigger.isInsert){
         for(Opportunity oppty: trigger.new){
                 if(oppty.StageName== 'Closed')
                      oppty.Closed_By__c =  UserInfo.getName();
            }
        }
        else if(trigger.IsUpdate){
        for(Opportunity oppty: trigger.new)
            {
                 if(oppty.StageName == 'Closed' && trigger.newMap.get(oppty.id).StageName != trigger.oldMap.get(oppty.id).StageName)
                     oppty.Closed_By__c =  UserInfo.getName();
            }

        }
      //  End Of Code for CI WR 248 //
          //WR 173667
          if(Trigger.isInsert || Trigger.isUpdate)
          {
          

              
              if(Util.IsTheaterUpdatedFromAccount!=true)
              {
                  for(Opportunity OpptyAccnt:Trigger.new)
                  {
                    setAccID.add(OpptyAccnt.Accountid);
                  }
                  if(setAccID.size()>0 && setAccID!=null)
                  {
                    system.debug('set account ids '+setAccID);
                    // Uday 246616
                     // We have created a new map in EMC_ConvertLead Class and use that map to get the account theater
                        EMC_ConvertLead theaterUtil = new EMC_ConvertLead();
                        Map<id,Account> theater = theaterUtil.theaterhelper(setAccID);
                        // Commented and queried this SOQL from EMC_ConvertLead class
                        // Map<id,Account> mapaccRecord= new Map<id,Account>([Select id,Theater1__c ,BillingCountry from Account where id in:(setAccID)]);
                         for(Opportunity oppTheater:Trigger.new)
                         {
                             if(oppTheater.AccountId!=null)
                               {
                                    if(theater != null && theater.size()>0){
                                        system.debug('The account is '+oppTheater.AccountId+'is insert/update '+Trigger.isInsert+' '+Trigger.isUpdate);
                                        system.debug('The theater map is '+theater);
                                        if(theater.get(oppTheater.AccountId) != null){
                                            oppTheater.Account_TheaterText__c= theater.get(oppTheater.AccountId).Theater1__c;
                                        }
                                        
                                    }
                               }
                         }
                    // Uday 246616
                  }
              }
          }
         //173667.
          //end of WR-151217
         //Added by Srinivas Nallapati for WR-163357
         if (Trigger.isInsert)
         {    
              for(Opportunity oppty: Trigger.New){
                 if(oppty.Service_Provider__c!= null)
                      oppty.SP_Tracking_Date__c = system.now();

                 ///Updated for notification sent on fields 
              
                  
                      
                     // Opportunity oldOppty = Trigger.oldMap.get(OpptyNotificn.Id);                      
                     
                        if(oppty.T15_Notification__c == true){
                            oppty.T15_Notification_Sent_On__c = System.now();
                            oppty.Notification_Sent_on__c = DateTime.now();
                        }    
                        if(oppty.T45_Notification__c == true){
                            oppty.T45_Notification_Sent_On__c = System.now();
                            oppty.Notification_Sent_on__c = DateTime.now();
                        }    
                        if(oppty.T90_Notification__c == true){
                            oppty.T90_Notification_Sent_On__c = System.now();
                            oppty.Notification_Sent_on__c = DateTime.now();
                      }
                      // Added for 1901
                      
                        if(oppty.Term_Notification__c == true){
                            oppty.Term_Notification_Sent_On__c = System.now();
                            oppty.Notification_Sent_on__c = DateTime.now();
                        }   
                        if(oppty.Booking_Certificate__c == true){
                            oppty.Booking_Certificate_Sent_On__c = System.now();
                            oppty.Notification_Sent_on__c = DateTime.now();
                        } 
                      //End of 1901
            
            ////Updated end for notification sent on fields



              }
         }
         else if(Trigger.isUpdate){
            for(Opportunity oppty: Trigger.New)
            {
                if(oppty.Service_Provider__c != null && trigger.oldMap.get(oppty.id).Service_Provider__c == null)
                    oppty.SP_Tracking_Date__c = system.now();
                else if(oppty.Service_Provider__c == null && trigger.oldMap.get(oppty.id).Service_Provider__c != null)
                         oppty.SP_Tracking_Date__c = null; 
                         
                //change for WR-
                if( (oppty.StageName != trigger.oldMap.get(oppty.id).StageName) && (oppty.StageName=='Submitted' || oppty.StageName=='Booked')){         
                    oppty.IIG_Renewals_Sales_Stage__c = 'Stage-6 Booking & Transitioning';
                }
                //End of change for WR-      
                
                ///Updated for notification sent on fields 
              
                  
                      
                     // Opportunity oldOppty = Trigger.oldMap.get(OpptyNotificn.Id);                      
                     
                        if(oppty.T15_Notification__c == true && trigger.oldMap.get(oppty.id).T15_Notification__c != oppty.T15_Notification__c){
                            oppty.T15_Notification_Sent_On__c = System.now();
                            oppty.Notification_Sent_on__c = DateTime.now();
                        }    
                        if(oppty.T45_Notification__c == true && trigger.oldMap.get(oppty.id).T45_Notification__c != oppty.T45_Notification__c){
                            oppty.T45_Notification_Sent_On__c = System.now();
                            oppty.Notification_Sent_on__c = DateTime.now();
                        }    
                        if(oppty.T90_Notification__c == true && trigger.oldMap.get(oppty.id).T90_Notification__c != oppty.T90_Notification__c){
                            oppty.T90_Notification_Sent_On__c = System.now();
                            oppty.Notification_Sent_on__c = DateTime.now();
                      }
                      //Added fr 1901
                      if(oppty.Term_Notification__c == true && trigger.oldMap.get(oppty.id).Term_Notification__c != oppty.Term_Notification__c){
                            oppty.Term_Notification_Sent_On__c = System.now();
                            oppty.Notification_Sent_on__c = DateTime.now();
                        }    
                        if(oppty.Booking_Certificate__c == true && trigger.oldMap.get(oppty.id).Booking_Certificate__c != oppty.Booking_Certificate__c){
                            oppty.Booking_Certificate_Sent_On__c = System.now();
                            oppty.Notification_Sent_on__c = DateTime.now();
                        }
                      //End of 1901
            
            ////Updated end for notification sent on fields
            }
         }
         //Adding this piece of code for Channel vis functionality.
         List<House_Account_For_OAR__c> lstHouseAccnt = House_Account_For_OAR__c.getAll().values();
         Map<id,Opportunity> mapOpptyHouseAccount = new Map<Id,Opportunity>();
         if(Trigger.isInsert || Trigger.isUpdate)
         {
            for(Opportunity opptyHouseAccount:Trigger.New)
            {
                //Updated for WR # 1424
                if(opptyHouseAccount.Business_Solution__c != null) {
                List<String> lstBs = opptyHouseAccount.Business_Solution__c.split(';');
                
                System.debug('lstBs--'+lstBs);

                String oppBS = '';
                String temp = '';
                for(String bs : lstBs) {
                    temp = '';
                    bs = bs.trim();
                    temp = oppBS+','+bs;
                    
                    if((temp.substring(1,temp.length())).length() < 255) {
                        if(oppBS=='')
                            oppBS = bs;
                        else
                            oppBS=oppBS+','+bs; 
                       
                   }
                    else
                        break;
                      
                }
                 
                 
                opptyHouseAccount.Opportunity_Sales_Play__c = oppBS;

                }
                // end of WR # 1424
                System.debug('The value of owner is'+opptyHouseAccount.ownerId);
                Opportunity oldOpportunity=new Opportunity();
                if(Trigger.isUpdate)
                {
                    oldOpportunity = Trigger.oldMap.get(opptyHouseAccount.Id);
                }
                //ID idOpptyOwner=opptyHouseAccount.Opportunity_Owner__r.id;
                for(House_Account_For_OAR__c objHouseAccnt:lstHouseAccnt)
                {
                    //objHouseAccnt.Name == idOpptyOwner &&
                    System.debug('Tha value of objHouseAccnt.Name is'+objHouseAccnt.Name);
                    if(opptyHouseAccount.OwnerId==objHouseAccnt.Name)
                    {
                        System.Debug('Entered inside House Account Functionality');
                        if(Trigger.isInsert)
                        {
                            if((opptyHouseAccount.Partner__c !=null||opptyHouseAccount.Tier_2_Partner__c!=null ||opptyHouseAccount.Primary_Alliance_Partner__c!=null ||
                                                            opptyHouseAccount.Secondary_Alliance_Partner__c!=null ||opptyHouseAccount.Service_Provider__c!=null))
                            {
                                System.debug('entered the house account condition');                    
                                mapOpptyHouseAccount.put(opptyHouseAccount.id, opptyHouseAccount);
                                break;
                            }
                        }
                        if(Trigger.isUpdate)
                        {
                            if((opptyHouseAccount.Partner__c !=oldOpportunity.Partner__c||opptyHouseAccount.Tier_2_Partner__c!=oldOpportunity.Tier_2_Partner__c ||opptyHouseAccount.Primary_Alliance_Partner__c!=oldOpportunity.Primary_Alliance_Partner__c ||
                                                            opptyHouseAccount.Secondary_Alliance_Partner__c!=oldOpportunity.Secondary_Alliance_Partner__c ||opptyHouseAccount.Service_Provider__c!=oldOpportunity.Service_Provider__c))
                            {
                                System.debug('entered the house account condition');                    
                                mapOpptyHouseAccount.put(opptyHouseAccount.id, opptyHouseAccount);
                                break;
                            }
                        }
                    }
                 }
            }
            if(mapOpptyHouseAccount.size()>0 && mapOpptyHouseAccount!=null)
            {
                Opp_UpdateOwner oppUpdateOwner = new Opp_UpdateOwner();
                map<id,Opportunity_Assignment_Rule__c> mapOppResourceId=new map<id,Opportunity_Assignment_Rule__c>();
                System.debug('Update Owner entered');
                System.debug('mapOpptyHouseAccount'+mapOpptyHouseAccount);
                mapOppResourceId=oppUpdateOwner.updateAccountOwner(mapOpptyHouseAccount,mapOppResourceId);
                //172069***
                List<OAR_Member_Added__c> lstOarMemAdded= new List<OAR_Member_Added__c>();
                Set<String> setCheckOARDuplicate= new Set<String>();
                //172069***
                System.debug('mapOppResourceId--->'+mapOppResourceId);
                for(Opportunity oppty: mapOpptyHouseAccount.values())
                {
                    if(mapOppResourceId.size()>0 && mapOppResourceId != null && mapOppResourceId.keySet().contains(oppty.id))
                    {
                        if(Trigger.isUpdate)
                        {
                            Util.mapOldOpportunityOwner.put(Oppty.id, Oppty.OwnerId);
                        }
                        Oppty.OwnerId=mapOppResourceId.get(Oppty.id).Resource_Name__c;
                        Oppty.Opportunity_Owner__c=mapOppResourceId.get(Oppty.id).Resource_Name__c;
                        //Updated By Shipra to add a record on OAR Member Added.
                        OAR_Member_Added__c OarMemAdded = new OAR_Member_Added__c();
                        String strUserAndOpptyId='';
                        strUserAndOpptyId=mapOppResourceId.get(Oppty.id).id+'|'+Oppty.Id;
                        if(setCheckOARDuplicate.contains(strUserAndOpptyId))continue;
                        setCheckOARDuplicate.add(strUserAndOpptyId);
                        System.debug('THE VALUE OF SET FOR OAR ON OPPORTUNITY IS :::'+setCheckOARDuplicate);
                        OarMemAdded.Team_Member_Added__c =mapOppResourceId.get(Oppty.id).Resource_Name__c;
                        OarMemAdded.Text_1__c =Oppty.Id;
                        OarMemAdded.Text_2__c =Oppty.Name;
                        OarMemAdded.Assignment_Rule__c=mapOppResourceId.get(Oppty.id).id;
                        OarMemAdded.Condition__c='Channel Visibility';
                        OarMemAdded.Text_3__c=Oppty.Opportunity_Number__c;
                        OarMemAdded.Text_4__c=Oppty.Account_Name1__c;
                        lstOarMemAdded.add(OarMemAdded);
                        //end of update by shipra.
                        if(Trigger.isUpdate)
                        {
                            Util.mapNewOpportunityOwner.put(Oppty.id, Oppty.OwnerId);
                        }
                        System.debug('Assign entered');
                    }
                       
                }
                //172069***
                Database.Saveresult[] arrMemberSendMail=Database.insert(lstOarMemAdded,true);
                System.debug('+++After Team member details have been added to OAR Member Added object'+lstOarMemAdded);
                //172069***  
            }
         }
         
         if (Trigger.isInsert)
         {    
              
            for(Opportunity oppty: Trigger.New)
            {
                oppty.Opportunity_Owner__c=oppty.ownerId;

                //Added For populating Assigned Date
                oppty.Assignment_date__c = System.now(); 

                // Created by Uday 246616
                if(oppty.closedate != null)
                {                 
                 if(oppty.CloseDate.month() == 1)
                 oppty.Initial_Forecast_Quarter__c = 'Q1'+' '+ oppty.CloseDate.year();
                 else if(oppty.CloseDate.month() == 2)
                 oppty.Initial_Forecast_Quarter__c = 'Q1'+' '+oppty.CloseDate.year(); 
                 else if(oppty.CloseDate.month() == 3)
                 oppty.Initial_Forecast_Quarter__c = 'Q1'+' '+oppty.CloseDate.year(); 
                 else if(oppty.CloseDate.month() == 4)
                 oppty.Initial_Forecast_Quarter__c = 'Q2'+' '+oppty.CloseDate.year(); 
                 else if(oppty.CloseDate.month() == 5)
                 oppty.Initial_Forecast_Quarter__c = 'Q2'+' '+oppty.CloseDate.year();       
                 else if(oppty.CloseDate.month() == 6)
                 oppty.Initial_Forecast_Quarter__c = 'Q2'+' '+oppty.CloseDate.year(); 
                 else if(oppty.CloseDate.month() == 7)
                 oppty.Initial_Forecast_Quarter__c = 'Q3'+' '+oppty.CloseDate.year(); 
                 else if(oppty.CloseDate.month() == 8)
                 oppty.Initial_Forecast_Quarter__c = 'Q3'+' '+oppty.CloseDate.year(); 
                 else if(oppty.CloseDate.month() == 9)
                 oppty.Initial_Forecast_Quarter__c = 'Q3'+' '+oppty.CloseDate.year();
                  else if(oppty.CloseDate.month() == 10)
                 oppty.Initial_Forecast_Quarter__c = 'Q4'+' '+oppty.CloseDate.year(); 
                 else if(oppty.CloseDate.month() == 11)
                 oppty.Initial_Forecast_Quarter__c = 'Q4'+' '+oppty.CloseDate.year(); 
                 else if(oppty.CloseDate.month() == 12)
                 oppty.Initial_Forecast_Quarter__c = 'Q4'+' '+oppty.CloseDate.year(); 
                 else 
                 oppty.Initial_Forecast_Quarter__c = 'Contact Your Admin'+' '+oppty.CloseDate.year();     
                }
               // Uday 246616 
               lstOpptyToSetOpptyType.add(oppty); 
                //WR#1611
                if(oppty.Deal_Grouping_Name__c != null) {
                    dealGroupings.add(oppty.Deal_Grouping_Name__c);
                    lstOpportunityWithDealGrouping.add(oppty);
                }       
            }
        //WR#1611
        if(!lstOpportunityWithDealGrouping.isEmpty()) {
            Opp_UpdateOwner.updateOpportunity(lstOpportunityWithDealGrouping, dealGroupings, Trigger.isUpdate);
        }
        if(!lstOpptyToSetOpptyType.isempty()){
            new R2R_Opportunity_Management().setOpportunityType(lstOpptyToSetOpptyType);
        }
                        
            
         }
         else
         {   
             if(util.CheckOpportunityAccess)
             {   
                for(Id newOppId: Trigger.newMap.keySet())
                {
                    Opportunity newOpportunity = Trigger.newMap.get(newOppId);
                    Opportunity oldOpportunity = Trigger.oldMap.get(newOppId);
                    if(newOpportunity.ownerid!=oldOpportunity.ownerid)
                    {
                        newOpportunity.Opportunity_Owner__c=newOpportunity.ownerid;
                        if(Opp_MassUserReassignment.IsMassReassignChannelVisCalled)
                        {
                            Util.isChannelVisibilityExecuted=false;
                        }
                        System.debug('SUNIL **** NEW OWNER = '+newOpportunity.Opportunity_Owner__c+'   **OLD OWNER ='+oldOpportunity.Opportunity_Owner__c);
                        //Added For populating Assigned Date
                        System.debug('123newOpportunity.StageName--->'+newOpportunity.StageName);
                        if(newOpportunity.StageName!='Closed'){
                         System.debug('inside if----->');
                        newOpportunity.Assignment_date__c = System.now(); 
                        }
                    } 
                    
                    if(newOpportunity.Opportunity_Owner__c!=oldOpportunity.Opportunity_Owner__c)
                    {
                      //Added For populating Assigned Date
                        System.debug('123newOpportunity.StageName--->'+newOpportunity.StageName);
                        if(newOpportunity.StageName!='Closed'){
                         System.debug('inside if----->');
                        newOpportunity.Assignment_date__c = System.now(); 
                        }
                        
                    }   
                  
                                
                    System.debug('oppty.Opportunity_Owner__c && oppty.ownerId'+newOpportunity.ownerId+' *** '+oldOpportunity.ownerId ); 
                    
                     //Added For Setting Assigned Date Boolean
                        if(newOpportunity.StageName=='Booked' && oldOpportunity.StageName=='Closed'){
                        newOpportunity.Date_Assigned_Checked__c = true; 
                       system.debug('newOpportunity.Assignment_date__c----'+newOpportunity.Assignment_date__c);

                        }

                        else{

                              newOpportunity.Date_Assigned_Checked__c = false; 

                        }
                }
             }
             //Included this code for S2S Functionality
             EMCBRS_S2S_Utils s2sUtils = new EMCBRS_S2S_Utils();
             s2sUtils.clearBRSOpptyData(trigger.oldMap,trigger.newMap);
             /* Added by Arif 
                WR - 148781    */ 
             // populateCompetitorLogicFormula populateObj = new populateCompetitorLogicFormula();
             //  populateObj.populateCompetitorLogicFormulaField(trigger.new); 
             
             /********************/
             CurrencyConversionHelper converter = new CurrencyConversionHelper(); // initializing the helper class
             for(Opportunity oppty: Trigger.New)
             {
                  Double amount = oppty.Amount; // opportunity amount
                  oppty.Dollar_Amount__c = converter.convertToUSD(amount  , oppty.CurrencyIsoCode);
             }
         }
    }
    if(trigger.isUpdate)
    {
        List<OAR_Member_Added__c> lstteam=new List<OAR_Member_Added__c>();
        Set<String> setCheckOwnerDuplicateRecord= new Set<String>();
        
        for(Id newOppId: Trigger.newMap.keySet())
        {
            Opportunity newOpportunity = Trigger.newMap.get(newOppId);
            Opportunity oldOpportunity = Trigger.oldMap.get(newOppId);
            if(newOpportunity.Opportunity_Owner__c!=oldOpportunity.Opportunity_Owner__c )
            {
                System.debug('entered account owner changes');
                String strUserAndOpptyId='';
                strUserAndOpptyId=newOpportunity.OwnerId+'|'+newOpportunity.Id;
                if(setCheckOwnerDuplicateRecord.contains(strUserAndOpptyId))continue;
                setCheckOwnerDuplicateRecord.add(strUserAndOpptyId);
                OAR_Member_Added__c oppMemAdd= new OAR_Member_Added__c();
                oppMemAdd.Text_1__c=newOpportunity.Id;
                oppMemAdd.Text_2__c=newOpportunity.Name;
                oppMemAdd.Text_3__c=newOpportunity.Opportunity_Number__c;
                oppMemAdd.Text_4__c=newOpportunity.Account_Name1__c;
                oppMemAdd.Text_5__c=newOpportunity.StageName;
                oppMemAdd.Text_6__c=newOpportunity.Initial_Forecast_Quarter__c;
                oppMemAdd.Text_7__c=newOpportunity.Country__c;
                oppMemAdd.Text_8__c=newOpportunity.Closed_Reason__c;
                oppMemAdd.Text_9__c=newOpportunity.Competitor_Lost_To__c;
                oppMemAdd.Text_10__c=newOpportunity.Competitor_Product__c;
                oppMemAdd.Number_1__c=newOpportunity.Amount;
                oppMemAdd.Number_2__c=newOpportunity.Dollar_Amount__c;
                oppMemAdd.Account_1__c=newOpportunity.Partner__c;
                oppMemAdd.Account_2__c=newOpportunity.Tier_2_Partner__c;
                if(newOpportunity.LeadSource==System.Label.Contract_Renewal)
                {
                    oppMemAdd.Condition__c='Send Owner Change for Contract Renewals';
                }
                else
                {
                    oppMemAdd.Condition__c='Send Owner Change Alert';
                }
                oppMemAdd.User_1__c=newOpportunity.Opportunity_Owner__c;
                oppMemAdd.Team_Member_Added__c=newOpportunity.Opportunity_Owner__c;
                oppMemAdd.Date_1__c=newOpportunity.CloseDate;
                lstteam.add(oppMemAdd);
                System.debug('lstteam===>'+lstteam);
                
            }
            if(newOpportunity.OwnerId != oldOpportunity.OwnerId && (newOpportunity.Opportunity_Type__c==null || newOpportunity.Opportunity_Type__c=='' || newOpportunity.Opportunity_Type__c=='Renewals')){
               lstOpptyToSetOpptyType.add(newOpportunity);      
            }
        }
        if(lstteam!=null && lstteam.size()>0)
        {
            Enterprise_Under_Pen_Email_Notification.insertEmailMemberAlertRecords(lstteam);
        }
        if(!lstOpptyToSetOpptyType.isempty()){
            new R2R_Opportunity_Management().setOpportunityType(lstOpptyToSetOpptyType);
        }
    }

   /////validating OEM_Partner__c field
   
   if(Trigger.isInsert){
     
        OppyOEMPartnerAccounts opptyOEMObj = new OppyOEMPartnerAccounts();
        //Added second parameter below by Jaypal Nimesh. WR# 340066. Date: 10-Feb-2014
        opptyOEMObj.salesChannel(trigger.new, null);
        opptyOEMObj.OEMPartnerAccounts(trigger.new);
        
        //#0752 - Calling method to update sales_force__c field
        OpportunityHelperClass.updateSalesForceField(trigger.new);   
        //End of 752 Changes
        
       
}
    if(Trigger.isUpdate){

        OppyOEMPartnerAccounts opptyOEMObj = new OppyOEMPartnerAccounts();
        //Added second parameter below by Jaypal Nimesh. WR# 340066. Date: 10-Feb-2014
        opptyOEMObj.salesChannel(trigger.new, trigger.oldMap);
        opptyOEMObj.OEMPartnerAccountsUpdate(trigger.new,Trigger.oldMap);
        
        List<Opportunity> lstOpptyToUpdate = new List<Opportunity>();
        //#0752 - Calling method to update sales_force__c field
        List<Opportunity> lstOpty = new List<Opportunity>(); 
        for(Opportunity opty : trigger.new) {
            if(opty.Opportunity_Owner__c != trigger.oldMap.get(opty.Id).Opportunity_Owner__c
                || opty.Sales_Force__c == null ||  opty.Sales_Force__c == '')  {
                lstOpty.add(opty);
            }
            
            //WR#1611
            if(trigger.oldMap.get(opty.Id).Deal_Grouping_Name__c != opty.Deal_Grouping_Name__c) {
                dealGroupings.add(opty.Deal_Grouping_Name__c);
                lstOpportunityWithDealGrouping.add(opty);
            }
        }
        if(lstOpty != null && !lstOpty.isEmpty()) {
            OpportunityHelperClass.updateSalesForceField(lstOpty);
        }   
        //End of 752 Changes
        //WR#1611
        if(!lstOpportunityWithDealGrouping.isEmpty()) {
            Opp_UpdateOwner.updateOpportunity(lstOpportunityWithDealGrouping, dealGroupings, Trigger.isUpdate); 
        }
    }
    //////validating OEM_Partner__c field ends here 
    //MEDDIC related logic executes here by calling helper class
    OpportunityHelperClass.meddicFieldUpdate(trigger.new, trigger.old, Trigger.isBefore, Trigger.isAfter, Trigger.isInsert, Trigger.isUpdate);

    
  }   
     
//End of trigger