//Trigger commented as part of PI March 2015
trigger AfterUpdateRule on Velocity_Rules__c (after update) {
        new PRM_VPP_DeltaIdentifer().deltaRuleMarking(Trigger.oldMap,Trigger.newMap);
}