/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req                  DESCRIPTION                               
 |  ====            =========       ======                  =========== 
 |  11-02-2013      Anand Sharma    System Administration    | 
 +=========================================================================================================================*/

trigger beforeInsertUpdateonSystemAdminUser on System_Admin_Users__c (before insert, before update, before delete) {
    Set<Id> setUserIds = new Set<Id>();
    List<System_Admin_Users__c> lstSysAdminUsers = new List<System_Admin_Users__c>();
    
    if(Trigger.isInsert ){      
        for(System_Admin_Users__c  objUser : Trigger.New){
            setUserIds.add(objUser.User_Name__c);
            lstSysAdminUsers.add(objUser);
        }
    }
    if(Trigger.isUpdate){        
        for(System_Admin_Users__c  objUser : Trigger.New){
            if(objUser.User_Name__c != trigger.OldMap.get(objUser.Id).User_Name__c ){                   
                setUserIds.add(objUser.User_Name__c);
                lstSysAdminUsers.add(objUser);
            }
        }
    }
    if(Trigger.isDelete){
        for(System_Admin_Users__c  objUser : Trigger.Old){
            setUserIds.add(objUser.User_Name__c);
        }
    }
    if(lstSysAdminUsers.size() >0){
        SystemAdministration_Utility.populatedProfileId(lstSysAdminUsers);        
    }
    if(setUserIds.size() >0){
        SystemAdministration_Utility.sendEmailOnNewModifiedDeleteUser(setUserIds );
    }
}