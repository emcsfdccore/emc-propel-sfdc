/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
|  04/Sep/2014  Srikrishna SM       BPP Project         After Insert trigger for Assigning Multiple PSM's to Partner Account.
|                                                       Logic written in class PRM_BPP_MultiplePSMsToPartnerAccount
+=====================================================================================================*/
trigger AfterInsertPSM on Additional_Partner_Sales_Managers__c (after insert) {

    PRM_BPP_MultiplePSMsToPartnerAccount.onInsertPSMLogic((List<Additional_Partner_Sales_Managers__c>)Trigger.new);
}