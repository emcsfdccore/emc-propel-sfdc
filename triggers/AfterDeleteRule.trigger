//Trigger commented as part of PI March 2015
trigger AfterDeleteRule on Velocity_Rules__c (after delete) {
            new PRM_VPP_DeltaIdentifer().deltaRuleMarking(Trigger.oldMap,Trigger.newMap);
}