/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR         DESCRIPTION
|  ====          =========               ==         =========== 
|Testclass name                                    BookingOperations_TC
| 29-Jan-12     krishna pydavula       223453      To Validate the system admin profiles.
| 05-May-14     Bhanu Prakash                      Code commented and de-activated trigger as Booking__c object is ready to decommition
| Trigger deactivated as part of PI March 2015
+=====================================================================================================*/  

trigger BeforeUpdateBooking on Booking__c (before delete, before insert) {
  /*  
    Map<string,BookingPermission__c> bookingProfileid= BookingPermission__c.getAll();
    string standProfiled=userinfo.getProfileId();
    if (Trigger.IsInsert) {
        
        for(Booking__c booking:trigger.new)
        {
            if(bookingProfileid.containskey(standProfiled))
            {
                booking.addError(System.Label.Booking_Validation); 
            }
        
        }   
    }
    if (Trigger.IsDelete) {
        
        for(Booking__c booking:trigger.Old)
        {
            if(bookingProfileid.containskey(standProfiled))
            {
                booking.addError(System.Label.Booking_Validation);
            }
        
        }   
    }
*/
}