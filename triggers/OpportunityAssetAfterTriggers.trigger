trigger OpportunityAssetAfterTriggers on Opportunity_Asset_Junction__c (after insert,after delete ) 
{
    set<id> setOpptyId = new set<Id>();
    if (Trigger.isInsert && Trigger.isAfter && Trigger.New != null) 
    {
        //system.debug('#### Entered Trigger code');
        SFA_MOJO_OppAssetJunctionTriggerCode.updateAssetRecords(Trigger.NewMap, Trigger.isUpdate);
        new R2R_Opportunity_Management().setOpportunityType(trigger.new,true);
        
        for(Opportunity_Asset_Junction__c junctionObj :Trigger.New){
            setOpptyId.add(junctionObj.Related_Opportunity__c);
        }
    }
    
    if(!Util.blnUpdateAssetId && !Trigger.isdelete)
    {       
        Util.blnUpdateAssetId = true;
        Set<Id> optyToUpdate = new Set<Id>();
        List<Opportunity> lstQueryOpty = new List<Opportunity>();
        List<Opportunity> lstUpdateOpty = new List<Opportunity>();
        String dataErrs = '';
        Database.Saveresult[] results;  
        
        
        for(Opportunity_Asset_Junction__c jnObj : Trigger.New)
        {
            if(jnObj.Related_Opportunity__c!=null)
            {
                optyToUpdate.add(jnObj.Related_Opportunity__c);
            }
        }
                        
        if((optyToUpdate!=null)&&(optyToUpdate.size() > 0))
        {
            lstQueryOpty = [Select name,id,accountId,RecordTypeId,Asset_Ids__c from opportunity where id in :optyToUpdate];             
            for(Opportunity opty : lstQueryOpty)
            {
                if(opty.Asset_Ids__c!=null)
                {
                    opty.Asset_Ids__c = null;
                    lstUpdateOpty.add(opty);
                }
            }
                                            
            if((lstUpdateOpty!=null)&&(lstUpdateOpty.size() > 0))
            {
                results = Database.Update(lstUpdateOpty, false);
            }
            
            if(results != null && results.size() >0)
            {
                for(Database.SaveResult sr : results)
                {
                    if(!sr.isSuccess())
                    {   
                        //Looking for errors if any.                
                        for (Database.Error err : sr.getErrors()) {
                            dataErrs += err.getMessage();
                        }
                        System.debug('An exception occurred while attempting an update on ' + sr.getId());
                        System.debug('ERROR: ' + dataErrs);
                    }
                }
            }
            
            
        }
        
    }
    if(Trigger.isAfter && Trigger.isDelete){
        new R2R_Opportunity_Management().setOpportunityType(trigger.old,false);
        for(Opportunity_Asset_Junction__c junctionObj :Trigger.old){
            setOpptyId.add(junctionObj.Related_Opportunity__c);
        }
        
    }  
    if(!setOpptyId.isempty()){
       new R2R_Utility().setTotalSwapValueOnTradeIn(setOpptyId); 
    }
  
    
 
 
     
}