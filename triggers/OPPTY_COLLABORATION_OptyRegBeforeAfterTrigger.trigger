/*===========================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER                WR        DESCRIPTION                               

 |  ====          =========                ==        =========== 

 | 03/10/2014     Bhanu Prakash         347846       This Trigger on Oppty Reg. will perform sharing and removing sharing on Partners
 | 03/24/2014     Bhanu Prakash                      Modified code as per Dennis Code review comments
 | 04/09/2014     Shalabh Sharma                     Added method call for sendEmail 
 | 15/Sep/2014     Aagesh Jose           CI 1229      Added a method to update email fields in opportunity page
 +===========================================================================*/
trigger OPPTY_COLLABORATION_OptyRegBeforeAfterTrigger on Opportunity_Registration__c (after insert,after update,before insert,before update) {
  
    List<Opportunity_Registration__c> lstOpptyRegToShare = new List<Opportunity_Registration__c >();
    List<Opportunity_Registration__c> lstOpptyRegCanl = new List<Opportunity_Registration__c >();
    List<Opportunity_Registration__c> lstOppReg = new List<Opportunity_Registration__c >();
    List<Opportunity_Registration__c> lstOppRegRej = new List<Opportunity_Registration__c >();
    OptyCollab_Custom_Setting__c opptyCustObj = OptyCollab_Custom_Setting__c.getValues('Theater_PSC_Emails');
    Map<Id, Opportunity_Registration__c> mapOptyReg = new Map<Id, Opportunity_Registration__c>();
   
   for(Opportunity_Registration__c opptyReg : Trigger.new){
        
        if (opptyReg.Status__c.equals('Submitted to Partner')){
            lstOpptyRegToShare.add(opptyReg);
        }
        if (opptyReg.Status__c.equals('Cancelled') || opptyReg.Status__c.equals('Rejected by Partner')){
            lstOpptyRegCanl.add(opptyReg);
        }
       //Set email address to send PSC for approval based on Theater -START
       if(Trigger.isBefore){
            if (opptyReg.Theater1__c != null && opptyReg.Theater1__c.equals('APJ')){
                opptyReg.PSC_Approval_Email__c=opptyCustObj.APJ_PSC_approve_email__c;
            }
            if (opptyReg.Theater1__c != null && opptyReg.Theater1__c.equals('EMEA')){
                opptyReg.PSC_Approval_Email__c=opptyCustObj.EMEA_PSC_approver_email__c;
            }
            if (opptyReg.Theater1__c != null && opptyReg.Theater1__c.equals('Americas')){
                opptyReg.PSC_Approval_Email__c=opptyCustObj.AMERICASPSCApproveremail__c;
            }
          }
          
         //Set email address to send PSC for approval based on Theater -END
       //Get list of Oppty Reg. objects for update
        if (Trigger.isUpdate && Trigger.isAfter ){
         if (opptyReg.Status__c != trigger.oldMap.get(opptyReg.Id).Status__c
            || (opptyReg.Registration_Type__c!=null && opptyReg.Registration_Type__c != trigger.oldMap.get(opptyReg.Id).Registration_Type__c)){
                lstOppReg.add(opptyReg);
         }
         if((opptyReg.Status__c.equals('Rejected by Partner') || (opptyReg.Status__c.equals('Cancelled') && opptyReg.Partner_Approval_Status__c!=null && opptyReg.Partner_Approval_Status__c.equals('Accepted')) || opptyReg.Status__c.equals('Submitted to Partner')) && opptyReg.Status__c != trigger.oldMap.get(opptyReg.Id).Status__c){
            lstOppRegRej.add(opptyReg);
         }
        }
    }
    if (Trigger.isAfter && Trigger.isUpdate && !lstOpptyRegToShare.isEmpty()){
       //Calling util class to share opportunity with partners with Read access
        OPPTY_COLLABORATION_Util.shareOptty(lstOpptyRegToShare);
    }
     if(lstOppReg.size()>0){
        new OPPTY_COLLABORATION_Util().updateRegOnOppty(lstOppReg);
    }
     if (Trigger.isAfter && Trigger.isUpdate && !lstOppRegRej.isEmpty()){
        //Calling util class to remove access for partners 
        OPPTY_COLLABORATION_Util objUtil = new OPPTY_COLLABORATION_Util();
        objUtil.sendEmail(lstOppRegRej);    
    }
    
    if (Trigger.isAfter && Trigger.isUpdate && !lstOpptyRegCanl.isEmpty()){
        //Calling util class to remove access for partners 
        OPPTY_COLLABORATION_Util.cancelOpptySharing(lstOpptyRegCanl);    
    }
    //1229 changes start from here
    for(Opportunity_Registration__c oppregobj : Trigger.new)
        {
            if(oppregobj.Related_Opportunity__c!=null) {
                mapOptyReg.put(oppregobj.Related_Opportunity__c, oppregobj);
            }
            
        }
        if (Trigger.isAfter && Trigger.isInsert && !mapOptyReg.isEmpty() || Trigger.isAfter && Trigger.isUpdate && !mapOptyReg.isEmpty())
       {   
        
        OPPTY_COLLABORATION_Util.updateOpporunityemail(mapOptyReg);        

        }
    //1229 changes ends here
    
    
}