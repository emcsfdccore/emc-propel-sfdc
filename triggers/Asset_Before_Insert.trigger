/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  26.09.2012      Avinash K       MOJO        Initial Creation.Creating this Trigger to update the value of the Disposition
                                                status field of Asset before insert
 |  29.01.2014      Sneha Jain      R2R         Handling new EMC-DeInstall records from SAM Db - FR 9579     
 |  17.09.2014      Partha Baruah   R2R-302/380 Restrict new Assets from creation (coming from SAM) when "Marked for Deletion" is Yes
+========================================================================================================================*/
trigger Asset_Before_Insert on Asset__c (before insert, before update) 
{

    Map<Id,Asset__c> mapAsset = new Map<Id,Asset__c>();
    Integer min = Integer.valueOf(System.Label.R2R_One_Year);
    system.debug('min--->'+min);
    map<string,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getall();   
    List<Asset__c> flipAssetRTList = new List<Asset__c>();
    //Code added for newly inserted records from SAM Db with Record type as EMC Install but IB Status as 'Deinstall - Cust Site'
    //The code should flip the Record type to EMC De-Install
    if(Trigger.isInsert)
    {
        for(Asset__c assetDeInstall : Trigger.new)
        {
            //R2R Nov'14 Rel - WR 302/380 :Restrict new Assets from creation (coming from SAM) when Marked for Deletion is Yes
            if(assetDeInstall.Marked_for_Deletion__c=='Yes' && (assetDeInstall.RecordTypeId == DataValueMap.get('EMC Install Record Type').DataValue__c || assetDeInstall.RecordTypeId == DataValueMap.get('EMC Install - NonActionable').DataValue__c)){
                assetDeInstall.addError(System.Label.R2R_Error_MarkedforDeletion_Checked)  ;
            }
            else
            {
                if(assetDeInstall.RecordTypeId == DataValueMap.get('EMC Install Record Type').DataValue__c && assetDeInstall.Install_Base_Status__c == 'Deinstall - Cust Site'){
                
                    flipAssetRTList.add(assetDeInstall);
             }
            }
        }
        if(flipAssetRTList != null && flipAssetRTList.size() <> 0)
        {
            new R2R_Asset_Management().flipAssetRT(flipAssetRTList,DataValueMap.get('EMC Install - NonActionable').DataValue__c);
        }
    }
    
    if(Trigger.isInsert && Trigger.isBefore)
    {
        SFA_Mojo_AssetTrigger.setDispositionStatusBeforeInsert(Trigger.New);        
        if(DataValueMap.get('IntegrationUser').DataValue__c != null && (userinfo.getuserid()==DataValueMap.get('IntegrationUser').DataValue__c)){
            R2R_Integration_Management.mapAssetsWithAccount(Trigger.New,trigger.IsInsert);            
        }  
        new R2R_AssetStatus().setSalesPlan(Trigger.New);
    }
    for(Asset__c assetRecord: Trigger.new){
        if(Trigger.isInsert && assetRecord.Previously_Refreshed__c==True){
            assetRecord.One_Year_after_Previously_Refreshed__c = System.NOW().addMinutes(min);
        }
        if(Trigger.isUpdate && assetRecord.Previously_Refreshed__c==True && assetRecord.Previously_Refreshed__c != Trigger.oldMap.get(assetRecord.Id).Previously_Refreshed__c){
            assetRecord.One_Year_after_Previously_Refreshed__c = System.NOW().addMinutes(min);
        }
        if(Trigger.isUpdate && assetRecord.Previously_Refreshed__c==False && assetRecord.Previously_Refreshed__c != Trigger.oldMap.get(assetRecord.Id).Previously_Refreshed__c){
            assetRecord.One_Year_after_Previously_Refreshed__c = null;
        }
    }
}