/*========================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER       WR        DESCRIPTION                               

 |  ====          =========       ==        =========== 

 |  11 Jun 2014     Sneha Jain  PROPEL I.002 PRM Atrributes Initial Creation - When a new PTA record is created,insert a record in Outbound Message Log object    
 +=================================================================================================*/
 
trigger PartnerTypeAttributeAfterInsert  on Partner_Type_Attribute__c (after Insert) {
    
    //For all new Partner Attribute Type records, insert a record in Outbound Message Log object
    PROPEL_PRMAttributesHelper.insertPartnerTypeAttr(Trigger.new);
}