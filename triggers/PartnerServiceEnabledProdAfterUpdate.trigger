/*========================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER       WR        DESCRIPTION                               

 |  ====          =========       ==        =========== 

 |  11 Jun 2014     Sneha Jain    PROPEL I.002 PRM Attibutes Initial Creation - When a PSEP record is updated with either Account or Service Level Value,insert a record in Outbound Message Log object
 +=================================================================================================*/
trigger PartnerServiceEnabledProdAfterUpdate  on Partner_Service_Enabled_Product__c (after update) {
    //Checking the flag for preventing recursive trigger run
    //Bhanu - Disabled the Code as per Dennis update
    if(TriggerContextUtility.firstRun)
    {    
        //For all updated Partner Service Enabled Products records, insert a record in Outbound Message Log object
        PROPEL_PRMAttributesHelper.updatePartnerServiceEnabledProd(Trigger.new,Trigger.oldMap);
    }    
}