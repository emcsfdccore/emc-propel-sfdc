/*==============================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE               DEVELOPER                WR       DESCRIPTION                               
 |  ====                =========                ==       =========== 
 |  1 May 2013       Prachi Bhasin           246408      Creation
 | 18 March 2014    Sayan Choudhury    CI WR 392   Added logic for CI WR 392
 +==============================================================================================================*/

trigger ReportingFieldInsideSalesAlerts on Lead(before update) {
    System.debug('Trigger.New'+Trigger.New);
    Map<Id,Lead> leadOldRecordsMap= new Map<Id,Lead>();
    Map<Id,Lead> leadNewRecordsMap= new Map<Id,Lead>();
    List<Lead> leadLst = new List<Lead>();
    List <Id> oldOwnerId= new List <Id>();
    List <Id> newOwnerId= new List <Id>();
                LeadReportingFieldChange reportObj = new LeadReportingFieldChange();
                for(Lead objLead : Trigger.new){
                      if(objLead.OwnerId!=trigger.oldMap.get(objLead.id).OwnerId){
                                 leadNewRecordsMap.put(objLead.id,objLead);
                                 newOwnerId.add(objLead.ownerid);
                                 leadOldRecordsMap.put(objLead.id,trigger.oldMap.get(objLead.id));
                                 oldOwnerId.add(trigger.oldMap.get(objLead.id).ownerid);
                        if(objLead.LeadSource=='eStore')
                            leadLst.add(objLead);
                        }
                }
                // Logic added for CI WR 392 //
                if(!leadLst.isEmpty()){
                    PRM_Lead_eBusiness_Integration eBizIntObj = new PRM_Lead_eBusiness_Integration();
                    eBizIntObj.settingEbusinessEmailidField(leadLst);
                }
                // End Of Logic CI WR 392//
                if(!leadNewRecordsMap.isEmpty() && !leadOldRecordsMap.isEmpty() && !newOwnerId.isEmpty() && !oldOwnerId.isEmpty()){
                             reportObj.updateReportField(leadNewRecordsMap,leadOldRecordsMap,newOwnerId,oldOwnerId);
 
                }
}