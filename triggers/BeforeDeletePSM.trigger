/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
|  04/Sep/2014  Srikrishna SM       BPP Project         Before Delete trigger for deleting Account Team Member from Partner
|                                                       Account.Logic written in class PRM_BPP_MultiplePSMsToPartnerAccount
+=====================================================================================================*/
trigger BeforeDeletePSM on Additional_Partner_Sales_Managers__c (before delete) {

    PRM_BPP_MultiplePSMsToPartnerAccount.beforeDeletePSMLogic((List<Additional_Partner_Sales_Managers__c>)Trigger.old);   
}