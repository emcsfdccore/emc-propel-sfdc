/*========================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER       WR        DESCRIPTION                               

 |  ====          =========       ==        =========== 

 |  11 Jun 2014     Sneha Jain    PROPEL I.002 PRM Atrributes  Initial Creation - When a PTA record is deleted, insert a record in Outbound Message Log object    
 +=================================================================================================*/

trigger PartnerTypeAttributeAfterDelete on Partner_Type_Attribute__c (after delete) {

    //For all deleted Partner Attribute Type records, insert a record in Outbound Message Log object
    PROPEL_PRMAttributesHelper.deletePartnerTypeAttr(Trigger.old);
}