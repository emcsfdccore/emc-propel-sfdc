trigger AfterInsertTR on Transaction_Register__c(after insert) {
    GBS_Transaction_Register_CaseCreation.onInsertTRLogic((List<Transaction_Register__c>)Trigger.new);
}