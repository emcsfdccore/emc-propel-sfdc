trigger KB_insertLinkedSR on Linked_SR__c (before insert,after insert, before update, after update) {


List<Linked_SR__c> lstLinkSR = trigger.new;
List<Linked_SR__c> lstOldLinkSR = new List<Linked_SR__c>();
KB_SubmitArticleForApproval obj = new KB_SubmitArticleForApproval();
Set<String> setArticleNo = new Set<String>();
Set<Id> setLinkedSR = new Set<Id>();
Set<Id> setLinkedSR1 = new Set<Id>();

KB_CalculateCountOfSR objSR = new KB_CalculateCountOfSR();
for(Linked_SR__c linkSRObj: trigger.new){
    setArticleNo.add(linkSRObj.Article_Number__c);
    setLinkedSR.add(linkSRObj.Id);
    if(Trigger.isUpdate && Trigger.IsAfter && linkSRObj.Article_Number__c != trigger.oldMap.get(linkSRObj.Id).Article_Number__c && linkSRObj.Article_Number__c !=null){
        setLinkedSR1.add(linkSRObj.Id);    
    }
}
Map<String,KnowledgeArticleVersion> mapKnowArt = new Map<String,KnowledgeArticleVersion>();
List<KnowledgeArticleVersion> lstKnowArt = new List<KnowledgeArticleVersion>();
//lstKnowArt = [Select id,KnowledgeArticleId,ArticleNumber,PublishStatus,CreatedById from KnowledgeArticleVersion where ArticleNumber in:setArticleNo and PublishStatus = 'Online' and Language = 'en_US' ];
system.debug('lstKnowArt--->'+lstKnowArt);
/*for(KnowledgeArticleVersion knowArtObj: lstKnowArt){
  mapKnowArt.put(knowArtObj.ArticleNumber,knowArtObj);
}*/
//system.debug('mapKnowArt--->'+mapKnowArt);
for(Linked_SR__c lnkObj :lstLinkSR){
    
    if(Trigger.isInsert && Trigger.isBefore){
        String srNumber = lnkObj.Article_Number__c;
        srNumber = srNumber.leftPad(9);
        srNumber = srNumber.Replace(' ','0');
        lnkObj.Article_Number__c = srNumber;
        lnkObj.Linking_User__c  = UserInfo.getUserId();
        lnkObj.SRARTICLEID__c = lnkObj.SR_Number__c+lnkObj.Article_Number__c;
        
    //lnkObj.Article_ID__c = mapKnowArt.get(lnkObj.Article_Number__c).KnowledgeArticleId;
    //lnkObj.Article_Version_ID__c = mapKnowArt.get(lnkObj.Article_Number__c).Id;
    //lnkObj.Originally_Created_By__c = mapKnowArt.get(lnkObj.Article_Number__c).CreatedById;
        //obj.submitArticleForApproval(trigger.new);
    }
    
    if(Trigger.isBefore && Trigger.isUpdate){
        lnkObj.SRARTICLEID__c = lnkObj.SR_Number__c+lnkObj.Article_Number__c;
        if(lnkObj.Linked__c == false){
            lnkObj.Article_Solved_My_Problem__c = false;    
        }
        if(lnkObj.Article_Number__c!=trigger.oldMap.get(lnkObj.Id).Article_Number__c){
        String srNumber = lnkObj.Article_Number__c;
        srNumber = srNumber.leftPad(9);
        srNumber = srNumber.Replace(' ','0');
        lnkObj.Article_Number__c = srNumber;
      //lnkObj.Article_ID__c = mapKnowArt.get(lnkObj.Article_Number__c).KnowledgeArticleId;
      //lnkObj.Article_Version_ID__c = mapKnowArt.get(lnkObj.Article_Number__c).Id;
      //lnkObj.Originally_Created_By__c = mapKnowArt.get(lnkObj.Article_Number__c).CreatedById;
      
      lstOldLinkSR.add(lnkObj);
            
            objSR.calculateCountofSROnInsert(trigger.new,false);
        }
        
    }
}
if(Trigger.isInsert && Trigger.isAfter){        
    obj.submitArticleForApproval(trigger.new);
    if(!KB_CalculateCountOfSR.hasAlreadyChangedShare()){
        KB_CalculateCountOfSR.getKnowledgeId(setLinkedSR,true);
    }
}
if(Trigger.isAfter && Trigger.isUpdate && setLinkedSR1.size()>0 && (!KB_CalculateCountOfSR.hasAlreadyChangedShare())){
    KB_CalculateCountOfSR.getKnowledgeId(setLinkedSR1,true);
}
if(Trigger.isBefore && Trigger.isUpdate){
  obj.submitArticleForApproval(lstOldLinkSR);
}
if(Trigger.isInsert && Trigger.isBefore){
        
        objSR.calculateCountofSROnInsert(trigger.new,false);
}
}