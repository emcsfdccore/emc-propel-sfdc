/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
|  04/Sep/2014  Srikrishna SM       BPP Project         After Update trigger for Assigning Multiple PSM's to Partner Account.
|                                                               Logic written in class PRM_BPP_MultiplePSMsToPartnerAccount
+=====================================================================================================*/
trigger AfterUpdatePSM on Additional_Partner_Sales_Managers__c (after update) {

    PRM_BPP_MultiplePSMsToPartnerAccount.onUpdatePSMLogic((Map<Id, Additional_Partner_Sales_Managers__c>)Trigger.oldMap, (Map<Id,Additional_Partner_Sales_Managers__c>)Trigger.newMap);
}