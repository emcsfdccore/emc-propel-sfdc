/*========================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER       WR        DESCRIPTION                               

 |  ====          =========       ==        =========== 

 |  11 Jun 2014     Sneha Jain    PROPEL I.002 PRM Attibutes Initial Creation - When a new PSEP record is created,insert a record in Outbound Message Log object
 +=================================================================================================*/

trigger PartnerServiceEnabledProdAfterInsert  on Partner_Service_Enabled_Product__c (after Insert) {
   
    //For all new Partner Service Enabled Products records, insert a record in Outbound Message Log object
    PROPEL_PRMAttributesHelper.insertPartnerServiceEnabledProd(Trigger.new);
}