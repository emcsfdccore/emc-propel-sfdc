/*===================================================================================================================================
|22/01/2011      Accenture                              Updated trigger to incorporate ByPass Logic
|                                                       NOTE: Please write all the code after BYPASS Logic 
|14/02/2012     Shipraa M                               SFA - EMEA email alert changes (FD Required)
|14/April/2014   Bhanuprakash Jangam                    Commented Opportunity update log
 ===================================================================================================================================*/

trigger Opportunity_UpdateLog on Opportunity (after insert,After Update) {
  //Trigger BYPASS Logic
   if(CustomSettingBypassLogic__c.getInstance()!=null){
      if(CustomSettingBypassLogic__c.getInstance().By_Pass_Opportunity_Triggers__c){
                return;
      }
   }
    //Inside EMEA Alerts.
    Map<Id,Opportunity> mapNewOpp= new Map<Id,Opportunity>();
    Map<Id,Opportunity> mapOldOpp= new Map<Id,Opportunity>();
    Map<Id,Opportunity> mapBookedOpp= new Map<Id,Opportunity>();
    //WR 301 - R2R
    Set<Opportunity> oppSet = new Set<Opportunity>();
    Map<Id,Id> oppDupIdMap = new Map<Id,Id>();
    
    if(trigger.isUpdate)
    {
        for(Id newoppId:trigger.newMap.keySet())
        {
            Opportunity newOpportunity=trigger.newMap.get(newoppId);
            Opportunity oldOpportunity=trigger.oldMap.get(newoppId);
            if(newOpportunity.StageName!=oldOpportunity.StageName || newOpportunity.CloseDate!=oldOpportunity.CloseDate)
            {
                mapNewOpp.put(newoppId,newOpportunity);
                mapOldOpp.put(newoppId,oldOpportunity);
                if(newOpportunity.StageName=='Booked' && newOpportunity.Under_Pen_Account_Type__c!=null &&newOpportunity.Opportunity_Owner__c!=null)
                {
                    System.debug('newOpportunity.Under_Pen_Account_Type_Legacy__c==>'+newOpportunity.Under_Pen_Account_Type__c);
                    mapBookedOpp.put(newoppId,newOpportunity);
                }
            }
            
            //WR 301 - R2R Scenarios - There is at-least one asset attached on the Opportunity AND either of -
            //1) Stage Name is changed to CLOSED and Reason is 'Duplicate' and Duplicate Oppty is populated
            //2) Stage Name is already CLOSED and Reason is changed from something to 'Duplicate' and Duplicate Oppty is populated
            //3) Stage Name and Reason is already there but only Duplicate Opportunity value is changed
            if(((newOpportunity.StageName!=oldOpportunity.StageName && newOpportunity.StageName == 'Closed' && newOpportunity.Closed_Reason__c == 'Duplicate' && newOpportunity.Duplicate_Opportunity__c !=null) || (newOpportunity.StageName == 'Closed' && newOpportunity.Closed_Reason__c != oldOpportunity.Closed_Reason__c && newOpportunity.Closed_Reason__c == 'Duplicate' && newOpportunity.Duplicate_Opportunity__c !=null) || (newOpportunity.StageName == 'Closed' && newOpportunity.Closed_Reason__c == 'Duplicate' && newOpportunity.Duplicate_Opportunity__c != oldOpportunity.Duplicate_Opportunity__c && newOpportunity.Duplicate_Opportunity__c !=null)) && newOpportunity.Asset_Count__c !=0){
            
                oppSet.add(newOpportunity);
                oppDupIdMap.put(newoppId,newOpportunity.Duplicate_Opportunity__c);
            }
            if(oppSet !=null && oppSet.size()>0){
                R2R_Opportunity_Management oppAsset = new R2R_Opportunity_Management();
                //oppAsset.transferAssetToDuplicateOpp(oppSet,oppDupIdMap);
            }
            
        }
        if(mapNewOpp!=null && mapNewOpp.size()>0)
        {
            Opp_EMEAInsideSalesAlerts oppEmeaAlerts = new Opp_EMEAInsideSalesAlerts();
            oppEmeaAlerts.updateEmailNotificationRecords(mapNewOpp,mapOldOpp);
        }
        if(mapBookedOpp!=null && mapBookedOpp.size()>0)
        {
            //For Under pen email alerts.
            if(!Util.Underpen_EMAIL_Alert_fired)
            {
                Enterprise_Under_Pen_Email_Notification.updateEmailFieldsForEnterpriseNotification(mapBookedOpp);
            }
        }
        
    }
    //End of Inside EMEA Alerts.
    if(!EMC_BRS_S2S_AutoForward.Opportunity_UpdateLogFlag){
        EMC_BRS_S2S_AutoForward.Opportunity_UpdateLogFlag = true;
        Map<Id,Id> opptyOwnerMap = new Map<Id,Id>();
        Profiles__c profile = Profiles__c.getInstance();
        System.debug('UserInfo.getProfileId()  '+ UserInfo.getProfileId());
        System.debug('profile.System_Admin_API_Only__c '+profile.System_Admin_API_Only__c );
        System.debug('util.CheckOpportunityAccess '+util.CheckOpportunityAccess ); 
        if(util.CheckOpportunityAccess){
          if(trigger.isInsert|| trigger.isUpdate){
            for(Opportunity oppty:trigger.new){
                
                    if( UserInfo.getProfileId() == profile.System_Admin_API_Only__c && trigger.isUpdate && trigger.oldMap.get(oppty.Id).OwnerId!=trigger.newMap.get(oppty.Id).OwnerId){
                       System.debug('Yes');
                        opptyOwnerMap.put(oppty.Id,oppty.OwnerId);
                    }
                    if(trigger.isInsert || trigger.oldMap.get(oppty.Id).OwnerId!=trigger.newMap.get(oppty.Id).OwnerId || Util.mapOldOpportunityOwner.get(oppty.Id)!=Util.mapNewOpportunityOwner.get(oppty.Id)){
                        System.debug('No');
                        opptyOwnerMap.put(oppty.Id,oppty.OwnerId);
                    }
                
            }
        }
        if(!opptyOwnerMap.isEmpty() &&  Opp_MassUserReassignment.ExecuteOwnerChangeTrigger){
            System.debug('Yes1'); 
            OP_SSF_CommonUtilsInterface utils = OP_SSF_CommonUtils.getInstance();
            utils.changeOwner(opptyOwnerMap,false);
        } 
        //Bhanu, commented to stop opportunity updates - START
       /* if(Trigger.isAfter && Trigger.isUpdate && UserInfo.getProfileId() != profile.System_Admin_API_Only__c){
            System.debug('No1');
            OpportunityOperation OpptyOperation = new OpportunityOperation();
            OpptyOperation.checkOpportunityUpdates(trigger.newMap,trigger.oldMap);
        }*/
        //Bhanu, commented to stop opportunity updates - END
        }
        //Included this code for S2S functionality
        if(trigger.isUpdate){
            //System.debug('*******welcome');
            EMCBRS_S2S_Utils s2sUtils = new EMCBRS_S2S_Utils();
            s2sUtils.upsertSharedOpportunities(Trigger.oldMap,Trigger.newMap);
        }
    }
  }