/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER          WR/Req                  DESCRIPTION                               
 |  ====            =========          ======                  =========== 
 |  20/04/2015      Partha Baruah      RMA May Release       This Trigger is for all After Insert,update events
 +=======================================================================================================================*/
 
 
trigger RMA_After_Trigger on Case_RMA__c (after Update,after insert)
{
    CaseRMA_Operations obj = new CaseRMA_Operations();
    List <Case_RMA__c> caseRMA= [select id,ownerId,LastModifiedDate,CreatedDate,Carrier_tracking_number__c,RMA_Case_Status__c from Case_RMA__c where id in : trigger.newMap.keyset()];
    
    if(trigger.isUpdate && !CaseRMA_Operations.chkFlag )
    {
        obj.updateOwner(trigger.newMap,trigger.oldMap);
        
        
    }
    if(trigger.isInsert && !CaseRMA_Operations.chkFlag)
    {
        obj.populateOwnerOnInsertion(trigger.newMap);
        obj.createChildRecod(Trigger.newMap); 
    }
        
        boolean chkForChange=false;
      
    if(trigger.isUpdate && !CaseRMA_Operations.chkFlag){
        for(Case_RMA__c CN : caseRMA){
           if(CN.RMA_Case_Status__c != trigger.oldMap.get(CN.Id).RMA_Case_Status__c)
              {
                obj.createChildRecod(Trigger.newMap);           
              }
            }  
    }       
}