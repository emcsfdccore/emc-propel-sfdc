/*
*  Created By        :- Sunil Arora
*  Created Date      :- 26/02/2010
*  Last Modified By  :- Yamilett Salazar
*  Last Modified Date:- 16/07/2013, WR #273539
*  Description       :- This trigger is used to update Presenter in Session Presenter
*/

trigger EBC_UpdateSessionPresenter on EBC_Session_Presenter__c (before insert, before update){
  EBC_CopyContactAsPresenter e = new EBC_CopyContactAsPresenter();
  
  if(Trigger.isInsert){
    e.copyOnInsert(Trigger.new);
  }
  
  if(Trigger.isUpdate){
    e.copyOnUpdate(Trigger.newMap, Trigger.oldMap);
  }
}