/*========================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER       WR        DESCRIPTION                               

 |  ====          =========       ==        =========== 

 |  11 Jun 2014     Sneha Jain    PROPEL I.002 PRM Attibutes Initial Creation - When a new PQR record is created,insert a record in Outbound Message Log object    
 +=================================================================================================*/
trigger PartnerQuotingRelAfterInsert  on Partner_Quoting_Relationship__c (after Insert) {

    //For all new Partner Quoting Relationships records, insert a record in Outbound Message Log object
    PROPEL_PRMAttributesHelper.insertPartnerQuotingRel(Trigger.new);
}