trigger OpportunityAssetBeforeTriggers on Opportunity_Asset_Junction__c (before Delete, before update,before Insert) {
  Set<Id> optyToUpdate = new Set<Id>();
   
   // Delete Triggers 
    if (Trigger.isBefore && Trigger.isDelete) 
      {
       
       SFA_MOJO_OppAssetJunctionTriggerCode junctionObj = new SFA_MOJO_OppAssetJunctionTriggerCode();
       for(Opportunity_Asset_Junction__c jnObj : Trigger.Old)
            {
                if(jnObj.Related_Opportunity__c!=null)
                {
                    optyToUpdate.add(jnObj.Related_Opportunity__c);
                }
            }
            junctionObj.updateTradeInsAndSwaps(optyToUpdate);
        
      }
       
       
       //Update Triggers
       if (Trigger.isUpdate && Trigger.isBefore && Trigger.New != null) 
        {
            SFA_MOJO_OppAssetJunctionTriggerCode.updateAssetRecords(Trigger.NewMap, Trigger.isUpdate);
        }
        if(Trigger.isBefore && Trigger.isInsert){
           new R2R_Opportunity_Management().validateAssetOpportunityLinking(trigger.new);
        }
        
}