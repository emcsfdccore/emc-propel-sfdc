trigger EBC_DefaultBriefingVenue on EBC_Briefing_Event__c (before insert) {

  EBC_DefaultBriefingVenue ebc = new EBC_DefaultBriefingVenue();
  ebc.setBriefingVenue(Trigger.new);
}