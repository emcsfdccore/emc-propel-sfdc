/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 |  02/01/2012    Shalabh Sharma           Trigger to map chatter post on Opportunity to Opportunity Reporting 
 |  13Nov2013     Leonard Victor           Metohod Call to Disable auto closure after 96hrs
 |  18/09/2014    Bisna V P     CI 1086        Fixed 101 SOQL exception
 +==================================================================================================================**/

trigger Presales_afterInsertBeforeDeleteOfFeedItem on FeedItem (after Insert, before delete) {
  Presales_OpportunityReporting objAttmt= new Presales_OpportunityReporting();
  EDServices_Emailmsg  edServiceObj = new EDServices_Emailmsg();
  if(trigger.isInsert){
    Map<Id,FeedItem> mapFeed = new Map<Id,FeedItem>();
    objAttmt.mapChatterPost(trigger.newMap,false);
    for(FeedItem feed:trigger.new){
      String s = feed.ParentId;
      if(s.substring(0,3)=='500'){
        mapFeed.put(feed.Id,feed);  
      }  
    }
    //Condition added for CI 1086
    if(!mapFeed.isEmpty() && mapFeed.size()>0)
    objAttmt.isFirstPost(mapFeed); 
    
    if(mapFeed.size()>0){

        edServiceObj.disableAutoClosureFeedItem(mapFeed);

    }
  }
  if(trigger.isDelete){
    objAttmt.mapChatterPost(trigger.oldMap,true);
  }
}