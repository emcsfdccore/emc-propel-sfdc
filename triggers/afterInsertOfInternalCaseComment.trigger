trigger afterInsertOfInternalCaseComment on Presales_Internal_Case_Comment__c (after Insert, after update) {
    List<Presales_Internal_Case_Comment__c> lstComment = new List<Presales_Internal_Case_Comment__c>();
    Map<String,List<Presales_Internal_Case_Comment__c>> mapIdComment = new Map<String,List<Presales_Internal_Case_Comment__c>>();
    Map<String,String> mapCaseCreatorId = new Map<String,String>();
    Set<String> setCreator = new Set<String>();
    Set<String> setCase = new Set<String>();
    List<Case> lstCase = new List<Case>();
    Map<Id,Case> mapCase = new Map<Id,Case>();
    for(Presales_Internal_Case_Comment__c objComment : trigger.new){
        if(objComment.Number_of_Transactions__c!=null){
            setCreator.add(objComment.CreatedById);
            setCase.add(objComment.Case__c);
        }
    }
    if(setCase.size()>0)
    mapCase = new Map<Id,Case>([Select Id,of_Agents__c from Case where Id in:setCase]);
    system.debug('mapCase-->'+mapCase);
    lstComment = [Select Id,CreatedById,Case__c,Number_of_Transactions__c from Presales_Internal_Case_Comment__c where CreatedById in: setCreator AND Case__c in: setCase];
    system.debug('lstComment-->'+lstComment);
    for(Presales_Internal_Case_Comment__c objCom:lstComment){
        mapCaseCreatorId.put(objCom.Case__c,objCom.CreatedById);
        if(objCom.Number_of_Transactions__c!=null){
            if(mapIdComment.containsKey(objCom.CreatedById)){
                mapIdComment.get(objCom.CreatedById).add(objCom);
            }
            else{
                List<Presales_Internal_Case_Comment__c> lst = new List<Presales_Internal_Case_Comment__c>();
                lst.add(objCom);
                mapIdComment.put(objCom.CreatedById,lst);
            }
        }
    }
    system.debug('mapIdComment-->'+mapIdComment);
    for(Case objCase : mapCase.values()){
        if(mapIdComment.containsKey(mapCaseCreatorId.get(objCase.Id)) && mapIdComment.get(mapCaseCreatorId.get(objCase.Id)).size()<=1){
            Integer i = (Integer)mapCase.get(objCase.Id).of_Agents__c;
            system.debug('i--->'+i);
            
            objCase.of_Agents__c = i+1; 
            lstCase.add(objCase);
        }
    }
    if(lstCase.size()>0)
    update lstCase;
}