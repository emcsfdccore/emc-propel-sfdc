/*==================================================================================================================+

     |  HISTORY  |                                                                           

     |  DATE          DEVELOPER                WR             DESCRIPTION                               

     |  ====          =========                ==             =========== 

     |  04/22/2013   Ganesh Soma                              Updated code to make the SLA_Completed__ field value to blank when the Case status changed from Resolved to some other status.
                                                              checking condition to restrict recursive execution of calculateClosureTime method
     |  11-Nov-2013    Leonard Victor                         Updated Logic for AutoAssignemnt
     |  22-May-2014    Abinaya                                Added new Education record type in condition to trigger AutoAssignemnt rule.
     |  17/06/2014     Sayan Choudhury        CI 704          Added Logic for CI WR 704
     |  26/06/2014     Leonard Victor         CI 704          Updated Code for ReClaulating Closure Time
     |  11/07/2014     Abinaya M S            CI 847          Bypass AutoAssignemnt rule for case origin 'OBC' in Line No 48.
     |  10-Nov-2014    Vinod                  CI 1488         Added new Education Services Proven record type in the condition to trigger AutoAssignment rule,, in line No 50 & Column 322.
     
     |  16-01-2014     Vivek Barange          CI 1607         Populate SLA for ED Service cases 
     |  17March2015    Bisna V P            WR#1880           Added logic for New Record Type EMCU
     |  31stMarch15    Sneha Jain          Cases For PSC      Differentiating case insert / update operation via Recursive Flag
+==============================================================================================================================================================**/

    trigger Edservices_toCaseTrigger on Case (after insert, before update, before insert,after update) {
        String rtName = System.label.EDServices_RecordType;
        List<Case> lstCase = new List<Case>();
        List<Case> lstAutoAssignmentCase = new List<Case>();
        
        //Added for Recalculating Closure Time RQ 704 CI
        Presales_Calculate_Closure_Time closClass = new Presales_Calculate_Closure_Time();
        EdServices_CheckRecordType edServObj = new EdServices_CheckRecordType();
        lstCase = edServObj.checkRecordType(trigger.new);
        
        Boolean flagOnlyInsert;
        EDServices_Emailmsg objS = new EDServices_Emailmsg();
        
        //Cases for PSC Differentiating case insert / update operation via Recursive Flag
            if(trigger.isInsert && trigger.isAfter){
                PSCOperations.OnInsertFlag = true;
            EDServices_Emailmsg obj = new EDServices_Emailmsg();
            for(Case cs:lstCase){
                //Commented to avoid Governot Limit Exception
               /* if(lstCase.size()>0){
                    obj.autoAssignmentToQueue(lstCase);
                    
                }*/

                System.debug('cs.Record_Type_Developer_Name__c----->'+cs.Record_Type_Developer_Name__c);
                System.debug('cs.CreatedBy----->'+cs);
                System.debug('cs.Origin----->'+cs.Origin);
                System.debug('cs.Status----->'+cs.Status);
                //1880 change; adding EMCU_Record_Type in if condition
                if((cs.Record_Type_Developer_Name__c == System.label.Global_Education_Service ||cs.Record_Type_Developer_Name__c == System.label.EMCU_Record_Type) && cs.Origin!='Phone'){
                    System.debug('inside if----->');
                    lstAutoAssignmentCase.add(cs);

                }//WR-922:Included Education Services Proven RecordType in the below condition to assign Autoassignrule in case.
                //WR-847:Bypass AutoAssignemnt rule for case origin 'OBC' and case type Education Services RecordType.
                //WR-1488:Included Education Services Proven RecordType in the below condition to assign Autoassignrule in case, at Column 322.
                else if(((cs.Record_Type_Developer_Name__c == System.label.EDServices_RecordType || cs.Record_Type_Developer_Name__c == System.label.EDServices_Proven_RecordType) && !(cs.Origin=='Phone' || cs.Origin=='Advisor'|| cs.Origin=='OBC')) || ((cs.Record_Type_Developer_Name__c == System.label.EDServices_RecordType || cs.Record_Type_Developer_Name__c == System.label.EDServices_Proven_RecordType) && cs.Origin=='Phone' && (cs.createdbyid == System.label.EDU_Site_Profile_ID || cs.createdbyid == System.label.Biz_Service_Site) )){
                    lstAutoAssignmentCase.add(cs);
                  }


            }

            if(lstAutoAssignmentCase.size()>0){

                obj.autoAssignmentToQueue(lstAutoAssignmentCase);

        }
        System.debug('lstAutoAssignmentCase---->'+lstAutoAssignmentCase);
        }
        
        
        
        //Added by Ganesh on 22Arpil2013
          if(trigger.isBefore && trigger.isUpdate){
            Map<Id,Case> mapResolvedCase = new Map<Id,Case>();
            Map<Id,Case> newCaseOwnerChanged = new Map<Id,Case>();

            
            for(Case cs:lstCase){   

                if(cs.status!=Trigger.oldMAp.get(cs.id).Status){
                        cs.Last_Status_Change__c = System.now();
                }   

                 if(cs.OwnerId != null && cs.OwnerId!= trigger.oldMap.get(cs.id).OwnerId){
                            newCaseOwnerChanged.put(cs.id,cs);
                }
                
                
               if(cs.Status == 'Resolved' && cs.Status != trigger.oldMap.get(cs.id).Status){          
                   mapResolvedCase.put(cs.Id,cs); 
                   
                if(cs.isResolved__c == false)
                    cs.isResolved__c = true;
            
            
               }
               else if(trigger.oldMap.get(cs.id).Status== 'Resolved'&& cs.Status != trigger.oldMap.get(cs.id).Status){
                    cs.SLA_Completed__c = null;
                }//WR-922:Included Education Services Proven RecordType in the below condition to update New_Email checkbox in case.
                if(cs.Status != trigger.oldMap.get(cs.Id).Status && (cs.Record_Type_Name__c == 'Education Services Record Type' ||cs.Record_Type_Developer_Name__c == System.label.EDServices_Proven_RecordType || cs.Record_Type_Developer_Name__c == 'Global_Education_Support' || cs.Record_Type_Developer_Name__c == System.label.EMCU_Record_Type)
                && (cs.Status == 'In Progress' || cs.Status == 'Customer Response Required')){
                    cs.New_Email__c = false;
                }
                
            
            
            } 

            if(!newCaseOwnerChanged.isEmpty()){
                Presales_Operations oppObj = new Presales_Operations();
                oppObj.checkFirstAllocation(newCaseOwnerChanged,trigger.oldMap);    
            }
            if(mapResolvedCase.size()>0){
              Presales_Calculate_Closure_Time obj = new Presales_Calculate_Closure_Time();
                obj.calculateClosureTime(mapResolvedCase,trigger.oldMap);
            }
        }
        //Added logic for CI WR 704//
        if(trigger.isBefore && trigger.isUpdate){
           for(Case cs : lstCase){
            if(cs.IsFirstUsrAllocation__c == true && cs.IsFirstUsrAllocation__c != trigger.oldMap.get(cs.id).IsFirstUsrAllocation__c){
                cs.Time_Case_Assigned__c = System.now();
            }
           }
           closClass.calculateClosureTime(Trigger.newMap,Trigger.oldMap);
        }//END of Logic For CI WR 704//

    
        //Cases for PSC Differentiating case insert via Recursive Flag
        if(trigger.isBefore && trigger.isInsert){
            PSCOperations.OnInsertFlag = true;
            for(Case cs :lstCase ){
                cs.Last_Status_Change__c = System.now();
            
            }
            flagOnlyInsert=true;
            objS.FieldUpdatesForWorkFlows(lstCase, flagOnlyInsert);
        }
        
        //Cases for PSC Differentiating case update operation via Recursive Flag
        if(trigger.isBefore && trigger.isUpdate){
            flagOnlyInsert=false;
            if(PSCOperations.OnInsertFlag){
                flagOnlyInsert = true;
            }
            objS.FieldUpdatesForWorkFlows(lstCase, flagOnlyInsert);
            
        }
        
        System.debug('Before populateChild---->');
        
        //Start - 1607
        if(Trigger.isUpdate && Trigger.isAfter){
        Presales_SLA_Class psc = new Presales_SLA_Class();
        List<Case> newCaseLst = new List<Case>();
        List<Case> oldCaseLst = new List<Case>();

          for(Case cs:lstCase){
                
                if(cs.Status != trigger.oldMap.get(cs.id).status){
                    newCaseLst.add(cs);
                    oldCaseLst.add(trigger.oldMap.get(cs.id));
                }


          }
           
          if(!Presales_SLA_Class.slaChanged() && !newCaseLst.isEmpty() && !oldCaseLst.isEmpty())
                  psc.populateChildData(lstCase, trigger.old); 
                  System.debug('After populateChild---->');
        }   
        
        //End - 1607
        
    }