/*===================================================================================================================================

| History 
| ------------
| 26 Mar 2013  Prasad Kothawade     R2R      Asset before trigger. Sets asset statuses.  
| 20 Nov 2013  Jaspreet Singh       R2R      Added Code for GDW field update
| 27 Feb 2014  Sneha Jain           FR 9644  Added consition for Contract Termination in Progress field
| 06 Mar 2014  Sneha Jain           FR 9601  Added one more field of Install Base status to IF condition
| 03 Sep 2014  Partha Baruah        WR 276   Added one more field "EOSL Date" to IF condition
| 21 Nov 2014  Garima Bansal        WR 276   R2R Dec'14 Release - Added Non Renewable Reason fields to the IF condition
| 21-Apr-2015  Vinod Jetti          CI#1876  Updated code to call 'R2R_Utility' method 
===================================================================================================================================*/
trigger AssetBeforeTriggers on Asset__c (before update) {
   map<string,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getall();
   string EMCIntsallId = DataValueMap.get('EMC Install Record Type').datavalue__c;
   string EMCDeIntsallId = DataValueMap.get('EMC Install - NonActionable').datavalue__c;
   List<Asset__c> lstAssetToUpdate = new List<Asset__c>();
   List<Asset__c> lstAssetUpdated = new list<Asset__c>();
   List<Asset__c> lstAssetUpdated1 = new list<Asset__c>();
   map<Id,Asset__c> mapNewAssetToProcess = new map<Id,Asset__c>();
   map<Id,Asset__c> mapOldAssetToProcess = new map<Id,Asset__c>();
   Set<ID> assetId = new Set<Id>();
   List<Asset__c> listNewAssetToProcessGDW = new List<Asset__c>();
   List<Asset__c> listOldAssetToProcessGDW = new List<Asset__c>();
     // R2R Set asset status    
    if (Trigger.isUpdate && !R2R_AssetStatus.StatusSetingCompleted){
        R2R_AssetStatus StatusUpdate= new R2R_AssetStatus();
        //FR 9644 : Added consition for Contract Termination in Progress field
        //FR 9601 : Added one more field of Install Base status to IF condition
        //WR 276  : Added field "EOSL Date" to the IF condition
        for(Asset__c assetObj : Trigger.NewMap.values()){
            /*if(assetObj.Red_Zone_Priority__c != Trigger.OldMap.get(assetObj.Id).Red_Zone_Priority__c || 
                   assetObj.Deffered_to_Renewals__c != Trigger.OldMap.get(assetObj.Id).Deffered_to_Renewals__c
                   ||assetObj.Previously_Refreshed__c != Trigger.OldMap.get(assetObj.Id).Previously_Refreshed__c){
            */
            if(assetObj.Red_Zone_Priority__c != Trigger.OldMap.get(assetObj.Id).Red_Zone_Priority__c || 
                   assetObj.Deffered_to_Renewals__c != Trigger.OldMap.get(assetObj.Id).Deffered_to_Renewals__c
                   ||assetObj.Previously_Refreshed__c != Trigger.OldMap.get(assetObj.Id).Previously_Refreshed__c 
                   || assetObj.Contract_Termination_in_Progress__c != Trigger.OldMap.get(assetObj.Id).Contract_Termination_in_Progress__c
                   || assetObj.End_of_Service_Life_Date__c != Trigger.OldMap.get(assetObj.Id).End_of_Service_Life_Date__c || assetObj.order_type__c!= Trigger.OldMap.get(assetObj.Id).order_type__c ||assetObj.SMS_Related_Asset__c!= Trigger.OldMap.get(assetObj.Id).SMS_Related_Asset__c
                   ||((assetObj.RecordTypeId==EMCIntsallId  || assetObj.RecordTypeId==EMCDeIntsallId) && (assetObj.Install_Base_Status__c != trigger.oldmap.get(assetObj.id).Install_Base_Status__c)) ){
                   mapNewAssetToProcess.put(assetObj.id,assetObj);
                   mapOldAssetToProcess.put(assetObj.id,Trigger.OldMap.get(assetObj.Id));
            }
            //#1876 - Start
            if(assetObj.Estimated_Raw_Capacity_TB__c != Trigger.OldMap.get(assetObj.Id).Estimated_Raw_Capacity_TB__c){
                   lstAssetUpdated.add(assetObj); 
            }
            if(assetObj.Number_of_Ports__c != Trigger.OldMap.get(assetObj.Id).Number_of_Ports__c){
                lstAssetUpdated1.add(assetObj);
            } 
            //#1876 - End
            //StatusUpdate.setInstallBaseStatus(Trigger.NewMap,Trigger.OldMap);
            
            if((assetObj.RecordTypeId==EMCIntsallId  || assetObj.RecordTypeId==EMCDeIntsallId ) && 
                assetObj.Install_Base_Status__c != trigger.oldmap.get(assetObj.id).Install_Base_Status__c){
                lstAssetToUpdate.add(assetObj);         
            }
       } 
       if(!mapNewAssetToProcess.isempty() && !mapOldAssetToProcess.isempty()){
        system.debug('#### SS before Salesplan method :: '+mapNewAssetToProcess);
               StatusUpdate.setSalesPlan(mapNewAssetToProcess,mapOldAssetToProcess);
               system.debug('#### SS AFTER Salesplan method :: '+mapNewAssetToProcess);
       }
       if(userinfo.getuserid()==DataValueMap.get('IntegrationUser').DataValue__c){
               boolean isInsert = false;
               R2R_Integration_Management.mapAssetsWithAccount(Trigger.New,isInsert);            
       }   
    }   
    if(lstAssetToUpdate.size()>0){
       new R2R_Asset_Management().setRecordTypeOnAsset(lstAssetToUpdate); 
    }   
    //#1876 Changes - Start
    if(!lstAssetUpdated.isempty()){
            new R2R_Utility().getOpportunityReregistration(lstAssetUpdated,true);
    }
    if(!lstAssetUpdated1.isempty()){
            new R2R_Utility().getOpportunityReregistration(lstAssetUpdated1,false);
    }
   //#1876 Changes - End 
    if(Trigger.isUpdate){
        //Remove Duplicate Assets
        for(Asset__c assetObj : Trigger.newMap.values()){
            assetId.add(assetObj.Id);
        }
        
        //Create new and old lists which will be passed to updateGDWField method
        if(!assetId.isEmpty()){
            for(Id tempAssetId : assetId){
                listNewAssetToProcessGDW.add(Trigger.newMap.get(tempAssetId));
                listOldAssetToProcessGDW.add(Trigger.oldmap.get(tempAssetId));
            }
        }
        //Call the field update method 
        if(listNewAssetToProcessGDW.size()>0 && listOldAssetToProcessGDW.size()>0){
            new R2R_Asset_Management().updateGDWField(listNewAssetToProcessGDW, listOldAssetToProcessGDW);
        }
    }
}