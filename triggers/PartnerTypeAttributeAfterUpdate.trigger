/*========================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER       WR        DESCRIPTION                               

 |  ====          =========       ==        =========== 

 |  11 Jun 2014     Sneha Jain   PROPEL I.002 PRM Atrributes Initial Creation - When a PTA record is updated with wither Account or Business Partner Program Tier Value,insert a record in                                          Outbound Message Log object    
 +=================================================================================================*/
 
trigger PartnerTypeAttributeAfterUpdate  on Partner_Type_Attribute__c (after update) {
    //Checking the flag for preventing recursive trigger run
    if(TriggerContextUtility.firstRun)
    {    
        PROPEL_PRMAttributesHelper.updatePartnerTypeAttr(Trigger.new,Trigger.oldMap);   
    }   
}