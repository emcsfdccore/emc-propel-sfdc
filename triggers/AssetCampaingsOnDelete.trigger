/*=======================================================================================================+
|  HISTORY  |                                                                           
|  DATE          DEVELOPER        WR                    DESCRIPTION                               
|  ====          =========        ==                    =========== 
| 07-Mar-2014    Srikrishna SM    FR-9549               This Trigger is used to update Comma Separated List of
                                                        Asset Campaign object names on Parent Asset object
+=====================================================================================================*/
trigger AssetCampaingsOnDelete on Asset_Based_Campaigns__c (before delete) {
    //Construct a map of Id and Asset_Based_Campaigns__c Objects
    Map<Id, Asset_Based_Campaigns__c> idABCMap = new Map<Id, Asset_Based_Campaigns__c>([Select Id, Name from Asset_Based_Campaigns__c where Id in : Trigger.old]);
    //Get a list of all Junction objects which are associated to these Asset_Based_Campaigns__c Objects
    List<Asset_Campaign_Junction__c> assetCampaignjunctionList = [Select Id, Name, Related_Asset__c, Related_Asset_Campaign__c from Asset_Campaign_Junction__c where Related_Asset_Campaign__c in : idABCMap.keySet()];
    //Construct a set of Asset__c object id's
    Set<Id> assetIdSet = new Set<Id>();
    //Ppulate assetIdSet
    for(Asset_Campaign_Junction__c acJunction : assetCampaignjunctionList){
        assetIdSet.add(acJunction.Related_Asset__c);
    }
    //Get a list of Course__c objects in which these junction objects are associated
    List<Asset__c> assetList = [Select Id, Name, Asset_Based_Campaigns__c from Asset__c where id in :assetIdSet];
    for(Asset__c asset : assetList){
        for(Asset_Campaign_Junction__c acJun : assetCampaignjunctionList){
            String name = '';
            List<String> nameToSplit = new List<String>();
            Set<String> stringSet = new Set<String>();
            if(asset.Id == acJun.Related_Asset__c){
                name = idABCMap.get(acJun.Related_Asset_Campaign__c).Name;
                if(asset.Asset_Based_Campaigns__c!= '' || asset.Asset_Based_Campaigns__c != null){
                    nameToSplit = asset.Asset_Based_Campaigns__c.split(',');
                    stringSet.addAll(nameToSplit);
                    stringSet.remove(name);
                }
                if(stringSet.size() == 0){
                    asset.Asset_Based_Campaigns__c = '';
                }else{
                    String updatedCampaignsString = '';
                    for(String cName : stringSet) {
                        updatedCampaignsString += (updatedCampaignsString==''?'':',')+cName;
                    }
                    asset.Asset_Based_Campaigns__c = updatedCampaignsString;
                }
            }
        }
    }
    if(!assetList.isEmpty())
    update assetList;
}