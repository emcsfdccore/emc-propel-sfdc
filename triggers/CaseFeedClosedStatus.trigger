/*==================================================================================================================+
 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 | 16 Mar 15    Aagesh J    GBS Sprint 3   To prevent chatter post on closed cases.
 +==================================================================================================================**/

trigger CaseFeedClosedStatus on FeedItem (before insert) {

     GBS_ChatterOnClosedCases.ClosedCaseChatter(Trigger.New);
     
     GBS_HelpTrainingHelper.helpTrainGroupContentValidation(trigger.new);

}