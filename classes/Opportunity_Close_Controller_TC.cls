@isTest
private class Opportunity_Close_Controller_TC{

static testmethod void opptycloseTest(){
    CustomSettingDataHelper.dataValueMapCSData();
    CustomSettingDataHelper.eBizSFDCIntCSData();
    CustomSettingDataHelper.bypassLogicCSData();
    CustomSettingDataHelper.profilesCSData();
    CustomSettingDataHelper.dealRegistrationCSData();
    Id recTypeId = [Select r.Id, r.name from RecordType r where r.name =: 'EMC Install' limit 1].Id;
    Id recTypeCIId = [Select r.Id, r.name from RecordType r where r.name =: 'Competitive Install' limit 1].Id;
    //Create Account
    List<Account> acc = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();       
      insert acc;
// Create opportunity
    Opportunity oppTest = new Opportunity();
    oppTest.AccountId =acc[0].id;
    oppTest.Name = 'TEST';
    oppTest.StageName = 'Pipeline';
    oppTest.CloseDate = Date.today()+10;
    oppTest.Sales_Force__c = 'EMC';
    oppTest.Sales_Channel__c = 'Direct';
    oppTest.VMWare_Related__c = 'VMWare Backup';
    oppTest.Amount = 222.00;
    insert oppTest;
  List<Opportunity> oppList = new List<Opportunity>();
  
  
    oppList.add(new Opportunity(Name = 'TestOpp0',AccountId = acc[0].Id, CloseDate = system.today() + 5, StageName = 'Submitted',Sales_Channel__c='Direct',Sales_Force__c='EMC'/*, Duplicate_Opportunity__c = opptest.id*/));
    oppList.add(new Opportunity(Name = 'TestOpp1',AccountId = acc[0].Id, CloseDate = system.today() + 7, StageName = 'Booked', Competitor_Lost_To__c = 'Dell', Competitor_Product__c= 'Compaq', close_Comments__c = 'TestClosed'));
    oppList.add(new Opportunity(Name = 'TestOpp2',AccountId = acc[0].Id, CloseDate = system.today() + 7, StageName = 'Pipeline', Duplicate_Opportunity__c = opptest.id, close_Comments__c = 'TestClosed'));
    oppList.add(new Opportunity(Name = 'TestOpp3',AccountId = acc[0].Id, CloseDate = system.today() + 5, StageName = 'Pipeline',Opportunity_type__c = 'Refresh', close_Comments__c = 'TestClosed'));
    oppList.add(new Opportunity(Name = 'TestOpp4',AccountId = acc[0].Id, CloseDate = system.today() + 7, StageName = 'Eval',Opportunity_type__c = 'Refresh', close_Comments__c = 'TestClosed'));
    // CI 1559 Replaced Closed_Reason__c value of Lost Funding to Project Cancelled
    oppList.add(new Opportunity(Name = 'TestOpp5',AccountId = acc[0].Id, CloseDate = system.today(), close_Comments__c = 'TestClosed',Closed_Reason__c = 'Project Cancelled', StageName = 'Closed',Sales_Channel__c='Direct', Sales_Force__c='EMC',Opportunity_type__c = 'Refresh'));
    oppList.add(new Opportunity(Name = 'TestOpp6',AccountId = acc[0].Id, CloseDate = system.today() + 5, StageName = 'Pipeline',Opportunity_type__c = 'Refresh'));
    oppList.add(new Opportunity(Name = 'TestOpp7',AccountId = acc[0].Id, CloseDate = system.today() + 7, StageName = 'Booked', Competitor_Lost_To__c = 'Amazon', Competitor_Product__c= 'Test', close_Comments__c = 'TestClosed'));    
    oppList.add(new Opportunity(Name = 'TestOpp8',AccountId = acc[0].Id, CloseDate = system.today() + 7, StageName = 'Booked', Competitor_Lost_To__c = 'Amazon', Competitor_Product__c= 'Test', close_Comments__c = 'TestClosed'));
    
    insert oppList;
    
   oppList[6].Opportunity_type__c = 'Refresh';
   update oppList;
 //create asset
 List<Asset__c> assetList = new List<Asset__c>();
 assetList.add (new asset__c( Name = 'TestAsset12',Customer_Name__c = acc[0].Id,RecordTypeId = recTypeId));
 assetList.add (new asset__c( Name = 'TestAsset12',Customer_Name__c = acc[0].Id,RecordTypeId = recTypeId));
 insert assetList;
 
 list <Opportunity_Asset_Junction__c> oajlist = new list<Opportunity_Asset_Junction__c>();
 oajlist.add(new Opportunity_Asset_Junction__c(Related_opportunity__c = oppList[0].id, related_asset__c = assetList[0].id));
 oajlist.add(new Opportunity_Asset_Junction__c(Related_opportunity__c = oppList[1].id, related_asset__c = assetList[0].id));
 oajlist.add(new Opportunity_Asset_Junction__c(Related_opportunity__c = oppList[2].id, related_asset__c = assetList[1].id));
insert oajlist;

test.starttest();
apexpages.currentpage().getparameters().put('ID', oppList[5].id);
apexpages.standardcontroller Opp1 = new apexpages.standardcontroller(oppList[5]);
Opportunity_Close_Controller Oppclose = new Opportunity_Close_Controller(Opp1);

oppList[2].Closed_Reason__c='Duplicate';
update oppList[2];
apexpages.currentpage().getparameters().put('ID', oppList[2].id);
apexpages.standardcontroller Opp2 = new apexpages.standardcontroller(oppList[2]);
Opportunity_Close_Controller Oppclose1 = new Opportunity_Close_Controller(Opp2);
Oppclose1.getclosedReasons();
Oppclose1.saveOpp();
Oppclose1.saveAssetOp();

apexpages.currentpage().getparameters().put('ID', oppList[0].id);
apexpages.standardcontroller Opp3 = new apexpages.standardcontroller(oppList[0]);
Opportunity_Close_Controller Oppclose2 = new Opportunity_Close_Controller(Opp3);
Oppclose2.saveOpp();
Oppclose2.operations = 'Defer Assets';
Oppclose2.saveAssetOp();

oppList[4].Closed_Reason__c='';
update oppList[4];
apexpages.currentpage().getparameters().put('ID', oppList[4].id);
apexpages.standardcontroller Opp4 = new apexpages.standardcontroller(oppList[4]);
Opportunity_Close_Controller Oppclose3 = new Opportunity_Close_Controller(Opp4);
Oppclose3.saveOpp();

oppList[1].Closed_Reason__c='Competition - Create Competitive Asset';
update oppList[1];
apexpages.currentpage().getparameters().put('ID', oppList[1].id);
apexpages.standardcontroller Opp5 = new apexpages.standardcontroller(oppList[1]);
Opportunity_Close_Controller Oppclose4 = new Opportunity_Close_Controller(Opp5);
Oppclose4.saveOpp();
Oppclose4.saveAssetOp();


apexpages.currentpage().getparameters().put('ID', oppList[7].id);
apexpages.standardcontroller Opp6 = new apexpages.standardcontroller(oppList[7]);
Opportunity_Close_Controller Oppclose5 = new Opportunity_Close_Controller(Opp6);
Oppclose5.reasons = 'Competition - Create Competitive Asset';
Oppclose5.saveOpp();
//replaced Chose Incumbent with Lost to Competition CI 1559
oppList[8].Closed_Reason__c='Lost to Competition';
update oppList[8];
apexpages.currentpage().getparameters().put('ID', oppList[8].id);
apexpages.standardcontroller Opp8 = new apexpages.standardcontroller(oppList[8]);
Opportunity_Close_Controller Oppclose8 = new Opportunity_Close_Controller(Opp8);
Oppclose5.saveOpp();
test.stoptest();
}
}