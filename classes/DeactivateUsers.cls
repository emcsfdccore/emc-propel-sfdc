/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE        DEVELOPER     DESCRIPTION                               
 |  ====        =========     =========== 
 | 13.08.2013   Sneha Jain    Class scheduled to run daily for deactivating users based on contacts 
 | 18.12.2013   Srikrishna SM 1) Class is modified to remove hard coded value for LAST_N_DAYS Date Literal  
 | 							  2) Class is modified to add condition to not to fecth contacts with null email ids  
+===========================================================================*/

global class DeactivateUsers implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {   
        Set<String> contactEmailSet = new Set<String>();
        Map<String,CustomSettingDataValueMap__c> mapData = CustomSettingDataValueMap__c.getAll();
        
        Integer days = Integer.valueOf(mapData.get('UserDeativation_LastModifiedDate').DataValue__c);
        //Do not fetch contacts if the email of the contact is null
        String query = 'select Id,RecordType.Name,Account.name,Active__c,email from Contact where Active__c = false and RecordType.Name = \'EMC Internal Contact\' and Account.name = \'EMC (Internal) Account\' and email != null and LastModifiedDate = LAST_N_DAYS: ' + days;
        for(Contact con : Database.query(query))
        //for(Contact con : [select Id,RecordType.Name,Account.name,Active__c,email from Contact where Active__c = false and RecordType.Name = 'EMC Internal Contact' and Account.name = 'EMC (Internal) Account' and LastModifiedDate = LAST_N_DAYS :2])
        //for(Contact con : [select Id,RecordType.Name,Account.name,Active__c,email from Contact where Id='0037000000tCSGM'])
        {
            //Collecting email ids of the contacts to deactivate users with corresponding email ids.
            contactEmailSet.add(con.email);
        }
        
        Map<string,ProfilesAndGroups__c> mapCustomSetting = ProfilesAndGroups__c.getall();
         
        //Test query
        if(test.isRunningTest())
        {
            return Database.getQueryLocator([select Id,Name,Email,isActive,profileID,createdDate from User where isActive = true AND profileID NOT IN : mapCustomSetting.keySet() AND createdDate = today Order by createdDate desc limit 5]);
        }
        else
        {   
            //Querying the records which are currently active but associated with inactive Resource
            return Database.getQueryLocator([select Id,Name,Email,isActive,profileID from User where email in : contactEmailSet AND isActive = true AND profileID NOT IN : mapCustomSetting.keySet() order by Email asc]);
        }
        
    }
    
    global void execute(Database.BatchableContext BC, SObject[] scope)
    {
        String emailBody = '';
        List<User> deactivateUserList = new List<User>();
        List<User> multipleUserList = new List<User>();
        List<User> updateUserList = new List<User>();
        Map<String,List<User>> emailUserMap = new Map<String,List<User>>();
        
        //Map of emails with user
        for(User uList : (List<User>)scope)
        {
            if(emailUserMap.containsKey(uList.email))
            {
                List<User> tempUserList = emailUserMap.get(uList.email);
                tempUserList.add(uList);
                emailUserMap.put(uList.email,tempUserList);
            }
            else
            {                 
                List<User> tempUserList = new List<User>();
                tempUserList.add(uList);
                emailUserMap.put(uList.email,tempUserList);
            }   
        }    
        //system.debug('---emailUserMap---'+emailUserMap);
        Integer count = 0;
        for(String emails : emailUserMap.keySet())
        {
            count++;
            String tempEmailBody ='';
            if(emailUserMap.get(emails).size() > 1)
            {
                //More than one user with an email value - don't deactivate
                for(User u :emailUserMap.get(emails))
                {
                    multipleUserList.add(u);
                    if(count == 1)
                    {
                        if(emailBody !='')
                        {
                            emailBody = emailBody + ' , ' + u.Name;
                        }
                        else
                        {
                            emailBody = 'Email Id : ' + emails + ' is present on following Users : ' + u.Name;
                        }
                    }
                    else if(count >1)
                    {
                        if(tempEmailBody !='')
                        {
                            tempEmailBody = tempEmailBody + ' , ' + u.Name;
                        }
                        else
                        {
                            tempEmailBody = '<br>' + 'Email Id : ' + emails + ' is present on following Users : ' + u.Name;
                        }
                    }
                }
                emailBody = emailBody + tempEmailBody;  
            }
            else if(emailUserMap.get(emails).size() == 1)
            {
                //Single user with an email value - consider for deactivation
                deactivateUserList.add(emailUserMap.get(emails)[0]);
            }
        }
        //system.debug('---emailUserMap2---'+emailUserMap);
        //system.debug('---deactivateUserList---'+deactivateUserList);
        if(multipleUserList != null && multipleUserList.size() >0)
        {                       
            // Send an email to the Admins notifying of multiple users with same email address
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            //Pick email ids from Custom setting to send the mail 
            Map<String,User_Deactivation_Notification__c> usrEmailsMap = User_Deactivation_Notification__c.getAll();
            List<String> emailList = new List<String>();
            if(usrEmailsMap != null && usrEmailsMap.get('EmailIDs') != null && usrEmailsMap.get('EmailIDs').Email_Ids__c !=null)
            {
                Integer countStr=0;
                String emailIds = usrEmailsMap.get('EmailIDs').Email_Ids__c;
                for(string str :emailIds.split(','))
                {
                    emailList.add(str);                     
                }
            }
            mail.setToAddresses(emailList);
            mail.setSubject('Multiple users with same email address ');
            mail.setHtmlBody('Hello,' + '<br> <br>' + 'While performing the user deactivation activity on ' + system.now() + ' ,following discrepancies were found : ' + '<br> <br> <br>' + '<b>' + emailBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });                   
        } 
        
        //Updating the users and making them inactive
        if(deactivateUserList !=null && deactivateUserList.size() > 0 )
        {           
            for(User usr : deactivateUserList)
            {
                usr.isActive = false;
                updateUserList.add(usr);           
            }
            if(updateUserList != null && updateUserList.size()>0)
            {
                Database.update(updateUserList,false);
            }  

            //Group member deletion
            List<GroupMember> grpMemberList = [select UserOrGroupId,Id,GroupId From GroupMember where UserOrGroupId IN: updateUserList];
            if(grpMemberList != null && grpMemberList.size() > 0)
            {
                database.delete(grpMemberList,false);
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){
        //system.debug('----finish method----');
    }      
}