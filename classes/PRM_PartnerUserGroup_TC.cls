/*====================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER                WR       DESCRIPTION                               

 |  ====          =========                ==       =========== 

 | 21/10/2010      Suman B           Req# 1117    Modified the test class as per change in Profile name 
 |                                                'Direct Reseller Partner User' using custom settings. 
 | 20/06/2011      Anirudh Singh                  Modified the Test Class as per the Updated Profile Names        
 | Aug-2011        Saravanan C                 		Test Class cleanup 
 | 12/11/2011     Anand Sharma            			Fixed test fail issue
 | 1-12-2011     	Anil               				Fixed Mixed_DML Exceptions and removed Queries  
 | 27/11/2013    Srikrishna       	 				Modified this class to decrease the processing time and updated with latest API (29)
 |=============================================================================================================== */
@isTest
private class PRM_PartnerUserGroup_TC {
	
	static TestMethod void partnerUserGroup(){
		//insert custom setting data required for the test class -- starts here
		User adminUser = [Select id from User where isActive = true and profile.Name='System Administrator' limit 1];
	    system.runAs(adminUser){
	    	CustomSettingDataHelper.dataValueMapCSData();
			CustomSettingDataHelper.eBizSFDCIntCSData();	
			CustomSettingDataHelper.profilesCSData();
			CustomSettingDataHelper.usersCSData();
			CustomSettingDataHelper.adminProfilesCSData();	
			CustomSettingDataHelper.usersGroupCSData();	
	    }
		
		//insert custom setting data required for the test class --  ends here	
		Map<Id,User> olduserMap = new Map<Id,User>();
        Map<Id,User> newuserMap = new Map<Id,User>();
        
	    User u;
	    
        Profile amerUserProf = [select Id from Profile where Name=: 'EMC Grouping Administrator'];
        u = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.id,null,null);  
        u.IsActive=true;
        User adminUser1 = [Select id from User where isActive = true and profile.Name='System Administrator' limit 1];
        System.runAs(adminUser1){
         insert u;
        }
        olduserMap.put(u.id,u); 
        User u1 = [Select id,ProfileId,email,Profile_Name__c from User where id =: u.id];
        u1.email = 'asd@abc.com';			         				   
        u1.ProfileId = '00e70000001Fbrf';
        newuserMap.put(u1.id,u1);
        
        System.Debug(' user olduserMap ===> ' + olduserMap );
        System.Debug(' user newuserMap ===> ' + newuserMap);
        system.runAs(adminUser){
        	PRM_PartnerUserGroup obj = new PRM_PartnerUserGroup();
        	obj.createUserGroupOnUpdate(olduserMap,newuserMap);
        	obj.createProfiledUserGrouping(newuserMap);
        	obj.deleteProfiledUserGrouping(newuserMap);
        	obj.createUserGroupOnNew(newuserMap);
        } 
          
    }
		
}