/*====================================================================================+
 |  HISTORY  |
 |  DATE          DEVELOPER               WR                        DESCRIPTION 
 |  ====          =========               ==                        =========== 
    22 Oct 2013    Avinash K            Backward Arrow Project      Removed "seeAllData = true" and created custom data needed
                                                                    for this test class
    05 Dec 2013    Sneha Jain           R2R Project                 Optimizing the test class according to coding standards                                                                 
    10 July 2013   Vivek Baranges        #1006                       Modified test class to increase test coverage    
 +======================================================================================*/


@isTest
public Class R2R_LeadLink_Controller_TC
{
    public static testmethod void testsLeadLinkController()
    {
        // Fetching either EMC Install or Competitive Install record type 
        //List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where DeveloperName in ('EMC_Install','Competitive_Install') limit 2];
        
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where RecordType.DeveloperName like 'EMC_Install' or Name Like 'Competitive_Install'];

        // Code inserted by Avinash for Backward Arrow project begins below
        // Creating data for the custom setting objects being referenced in the dependent classes called by this test class
        
        //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
        }
        // Code inserted by Avinash ends above

        //Creating Account record
        List<Account> accList = new List<Account>();
        accList = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        accList[0].Business_Unit_Ultimate__c = true;
        insert accList;
        
        //Creating Hub Account Data records
        List<Hub_Info__c> hubInfoList = new List<Hub_Info__c>();
        
        hubInfoList.add(new Hub_Info__c(Name = 'Test Hub Info',Golden_Site_Name__c = 'Test Golden Site',Golden_Site_Identifier__c = 1234));
        hubInfoList.add(new Hub_Info__c(Name = 'Test Hub Info2',Golden_Site_Name__c = 'Test Golden Site2',Golden_Site_Identifier__c = 12345));
        insert hubInfoList;
        hubInfoList[1].BUU_Lookup__c = hubInfoList[0].Id;
        update hubInfoList[1];
        system.debug('---hubInfoList---'+hubInfoList[0]);
        //Creating District Lookup records
        List<District_Lookup__c> districtList = new List<District_Lookup__c>();
        districtList.add(new District_Lookup__c(Name = 'Test District'));
        insert districtList;
        system.debug('---districtList---'+districtList[0]);
        //Updating Account's info
        accList[0].Hub_Info__c = hubInfoList[1].Id;
        accList[0].District_Lookup__c = districtList[0].Id;
        accList[0].Business_Unit_Ultimate__c = true;
        update accList[0];
        Account acct = [select id,name, Business_Unit_Ultimate__c,Account_District__c,Hub_Info__r.Golden_Site_Name__c,District_Lookup__r.Name,Hub_Info__r.BUU_Lookup__r.Golden_Site_Name__c from Account where Id =:  accList[0].Id ] ;
        system.debug('-----Business_Unit_Ultimate__c ---' + acct.Business_Unit_Ultimate__c);  
        system.debug('-----Hub_Info__c ---' + acct.Hub_Info__r.BUU_Lookup__r.Golden_Site_Name__c); 
        system.debug('-----Hub_Info__c ---' + acct.Hub_Info__r.Golden_Site_Name__c);        
        system.debug('-----District_Lookup__c ---' + acct.District_Lookup__r.Name);    
        system.debug('-----account district ---' + acct.Account_District__c);        
        //Creating Lead records
        List<Lead> leadList = new List<Lead>();
        
        Lead lead0= new Lead();
        lead0.Company='TestLead';
        lead0.LastName='Test123';
        lead0.Status='New';
        lead0.Sales_Force__c='EMC';
        lead0.Lead_Originator__c='Customer Intelligence';
        leadList.add(lead0);
         
        Lead lead1= new Lead();
        lead1.Company='TestLead1';
        lead1.LastName='Test1234';
        lead1.Status='New';
        lead1.Sales_Force__c='EMC';
        lead1.Lead_Originator__c='Customer Intelligence';
        lead1.Related_Account__c = accList[1].Id;
        leadList.add(lead1);

        Lead lead2= new Lead();
        lead2.Company='TestLead12';
        lead2.LastName='Test1244';
        lead2.Status='New';
        lead2.Sales_Force__c='EMC';
        lead2.Lead_Originator__c='Customer Intelligence';
        Lead2.City ='City';
        lead2.Channel__c ='Inside';
        lead2.Street ='Street';
        lead2.Related_Account__c = accList[0].Id;
        leadList.add(lead2);

        insert leadList;

        //Creating Opportunity records
        List<Opportunity> listOpp=new List<Opportunity>();
        Opportunity opp=new Opportunity();
        opp.Name='Testoppty12';
        opp.AccountId = accList[0].Id;
        opp.Opportunity_Type__c='Refresh';
        opp.CloseDate=System.today()+5;
        opp.Sales_Channel__c='Direct';
        opp.Sales_Force__c='EMC';
        opp.StageName='Pipeline';
        listOpp.add(opp);
        try
        {
            insert listOpp;
        }
        catch(Exception e)
            {System.debug(+e);}


        //Creating Asset records
        List<Asset__c> listSelectedAssets=new List<Asset__c>();
        
        //Creating Competitive Install asset
        Asset__c asset1= new Asset__c();
        asset1.Name='TestassetComp';
        asset1.Customer_Name__c=accList[0].id;
        if(recordType[0].DeveloperName == 'Competitive_Install')
        {
            asset1.RecordTypeId=recordType[0].id;
        }   
        else
        {
            asset1.RecordTypeId=recordType[1].id;
        }
        asset1.Product_Name_Vendor__c='Dell';
        asset1.Install_Base_Status__c = 'Displaced by EMC';
        listSelectedAssets.add(asset1);

        //Creating EMC Install asset
        Asset__c asset2= new Asset__c();
        asset2.Name='TestassetEMC';
        asset2.Customer_Name__c=accList[0].id;
        if(recordType[0].DeveloperName == 'EMC_Install')
        {
            asset2.RecordTypeId=recordType[0].id;
        }   
        else
        {
            asset2.RecordTypeId=recordType[1].id;
        }
        asset2.Product_Name_Vendor__c='EMC';
        listSelectedAssets.add(asset2);
        
        //Creating Competitive Install asset
        Asset__c asset3= new Asset__c();
        asset3.Name='TestassetComp1';
        asset3.Customer_Name__c=accList[1].id;
        if(recordType[0].DeveloperName == 'Competitive_Install')
        {
            asset3.RecordTypeId=recordType[0].id;
        }   
        else
        {
            asset3.RecordTypeId=recordType[1].id;
        }
        asset3.Product_Name_Vendor__c='Dell';
        asset3.Install_Base_Status__c = 'Displaced by EMC';
        listSelectedAssets.add(asset3);
        
        //Creating EMC Install asset
        Asset__c asset4= new Asset__c();
        asset4.Name='TestassetEMC1';
        asset4.Customer_Name__c=accList[1].id;
        if(recordType[0].DeveloperName == 'EMC_Install')
        {
            asset4.RecordTypeId=recordType[0].id;
        }   
        else
        {
            asset4.RecordTypeId=recordType[1].id;
        }
        asset4.Product_Name_Vendor__c='EMC';
        listSelectedAssets.add(asset4);
        
        insert listSelectedAssets;
        
        //Lead_Asset_Junction__c LeadJunction = R2R_datahelper.leadAssetJunction(listSelectedAssets[0].id,leadList[0].id);
       
        //listSelectedAssets= controller.getselected();
        string rendersection = Apexpages.currentPage().getParameters().put('rendersection','asset');
        string accdist = Apexpages.currentPage().getParameters().put('accDist','Test Golden Site: Test District');
        string assetId = Apexpages.currentPage().getParameters().put('assetid',listSelectedAssets[0].id);
        string leadToFetch= Apexpages.currentPage().getParameters().put('selLead',leadList[2].id);
        string leadIdFromURL= Apexpages.currentPage().getParameters().put('leadId',leadList[1].id); 
        string assetaccid=Apexpages.currentPage().getParameters().put('accid',accList[0].id);
        string strLead = Apexpages.currentPage().getParameters().put('retURL','leadId12345678'+leadList[1].id);

        //Creating Lead Asset Junction record with lead status =Swap and EMC asset attached
        List<Lead_Asset_Junction__c> listlaj= new List<Lead_Asset_Junction__c>();
        
        Lead_Asset_Junction__c laj1= new Lead_Asset_Junction__c();
        laj1.Related_Asset__c=listSelectedAssets[2].id;
        laj1.Related_Lead__c=leadList[1].id;
        listlaj.add(laj1);

        //Creating Lead asset junction with Lead status= Refresh and COmp asset attached
        Lead_Asset_Junction__c laj2= new Lead_Asset_Junction__c();
        laj2.Related_Asset__c=listSelectedAssets[0].id;
        laj2.Related_Lead__c=leadList[2].id;
        listlaj.add(laj2);
        
        Lead_Asset_Junction__c laj3= new Lead_Asset_Junction__c();
        laj3.Related_Asset__c=listSelectedAssets[3].id;
        laj3.Related_Lead__c=leadList[1].id;
        listlaj.add(laj3);
        
        Lead_Asset_Junction__c laj4= new Lead_Asset_Junction__c();
        laj4.Related_Asset__c=listSelectedAssets[1].id;
        laj4.Related_Lead__c=leadList[2].id;
        listlaj.add(laj4);
        
        insert listlaj;
     
        //Creating Opportunity asset junction records
        List<Opportunity_Asset_Junction__c> listOaj= new List<Opportunity_Asset_Junction__c>();

        Opportunity_Asset_Junction__c Oaj= new Opportunity_Asset_Junction__c();
        Oaj.Related_Asset__c=listSelectedAssets[0].id;
        Oaj.Related_Opportunity__c=listOpp[0].id;
        Oaj.Related_Account__c=accList[0].id;
        listOaj.add(Oaj);

        Opportunity_Asset_Junction__c Oaj1= new Opportunity_Asset_Junction__c();
        Oaj1.Related_Asset__c=listSelectedAssets[1].id;
        Oaj1.Related_Opportunity__c=listOpp[0].id;
        Oaj1.Related_Account__c=accList[0].id;
        listOaj.add(Oaj1);
        Test.startTest();
        try
        {
            insert listOaj;
        }
        catch(Exception e)
        {
            System.debug(+e);
        }
     
        R2R_LeadLink_Controller obj=new R2R_LeadLink_Controller();  
        Boolean showClearList=true;
        obj.SearchLead();
        obj.strInputCode ='testLead';
        String str1 = null;
        obj.SearchLead();
        obj.strInputCode ='testLead*';
        str1 = 'Test';
        obj.SearchLead();
        obj.intErrored = 0;
        obj.leadlink = listlaj[0];
        obj.linkAssestToLead();
        obj.SelectedRecord();
        obj.cancelLink();
        obj.getselectedAssets();
        //obj.processSelected();
        obj.setSelectedAssetId = null;
        obj.validateLinking();
        //system.debug('--------**' + listSelectedAssets[1].id);
        //obj.setSelectedAssetId.add(listSelectedAssets[1].id);
        obj.leadlink = listlaj[3];
        obj.validateLinking();
        obj.setSelectedAssetId = new Set<Id>{listSelectedAssets[0].id};
        obj.validateLinking();
        
        rendersection = Apexpages.currentPage().getParameters().put('rendersection','asset');
        assetaccid=Apexpages.currentPage().getParameters().put('accid',accList[0].id);
        R2R_LeadLink_Controller obj0=new R2R_LeadLink_Controller();
        
        leadIdFromURL= Apexpages.currentPage().getParameters().put('leadId',leadList[2].id);
        strLead = Apexpages.currentPage().getParameters().put('retURL',null);
        R2R_LeadLink_Controller obj00 = new R2R_LeadLink_Controller();
        
        rendersection = Apexpages.currentPage().getParameters().put('rendersection','asset');
        accdist = Apexpages.currentPage().getParameters().put('accDist','');
        leadIdFromURL= Apexpages.currentPage().getParameters().put('leadId',leadList[2].id); 
        assetId = Apexpages.currentPage().getParameters().put('assetid',listSelectedAssets[2].id);
        assetaccid=Apexpages.currentPage().getParameters().put('accid',accList[1].id);
        R2R_LeadLink_Controller obj1=new R2R_LeadLink_Controller();
        
        rendersection = Apexpages.currentPage().getParameters().put('rendersection','asset');
        assetaccid=Apexpages.currentPage().getParameters().put('accid',null);
        R2R_LeadLink_Controller obj2=new R2R_LeadLink_Controller();     
                
        rendersection = Apexpages.currentPage().getParameters().put('rendersection','lead');
        R2R_LeadLink_Controller obj3=new R2R_LeadLink_Controller(); 

        leadToFetch= Apexpages.currentPage().getParameters().put('selLead','');
        R2R_LeadLink_Controller obj4=new R2R_LeadLink_Controller();     
        
        
        rendersection = Apexpages.currentPage().getParameters().put('rendersection','asset');
        accdist = Apexpages.currentPage().getParameters().put('accDist',null);
        R2R_LeadLink_Controller obj5=new R2R_LeadLink_Controller(); 
        
        Test.stopTest();
    }
}