public class R2R_OpportunityMassClose 
{
    
    public List<Opportunity> lstOpp = new List<Opportunity>();
    public List<massCloseWrapper> lstWrapper {get;set;} {lstWrapper = new List<massCloseWrapper>();}

    public Boolean blnRecordPresent {get;set;}
    public Set<Id> setOppId = new Set<Id>();

    public List<Opportunity> lstSelectedOpp = new List<Opportunity>(); 
    //Opportunties for which action has been selected and have to be closed

    public PageReference prefReturn;
    public Integer intCountError;
    public Integer intCountSaved;
    public Boolean blnShowCompactTable {get;set;} {blnShowCompactTable = false;}
    public Boolean blnShowRenewals {get;set;} {blnShowRenewals = false;}

    public Map<Id,Id> mapOppIdDupId = new Map<Id,Id>();
    public List<Opportunity> lstClosedOpps = new List<Opportunity>();
    public Set<Id> setAssetIds = new Set<Id>();

    public Set<Opportunity> setOpp = new Set<Opportunity>();
    Map<Id,Id> mapOppIdDupOppId = new Map<Id,Id> ();
    public R2R_Opportunity_Management oppAsset = new R2R_Opportunity_Management();

    public Boolean blnActions;
    public Boolean blnShowSave {get;set;} {blnShowSave = true;}

    public R2R_OpportunityMassClose(ApexPages.StandardSetController controller) 
    {
        lstOpp = controller.getSelected();

        if(lstOpp != null && lstOpp.size() >0)
        {
            blnRecordPresent = true;

            for(Opportunity opp: lstOpp)
                setOppId.add(opp.id);
        }

        lstOpp = [SELECT Name, (SELECT ID, Related_Asset__c FROM Opportunity_Asset_Junction__r),stageName, Closed_Reason__c, Close_Comments__c, Duplicate_Opportunity__c, Competitor_Lost_To__c, Competitor_Product__c, Renewals_Close_Details__c, Closed_Reason_Action__c, Asset_Count__c, Opportunity_Type__c, AccountId, Account_Name1__c
                    FROM Opportunity
                    WHERE Id IN :setOppId limit 5000];


        //Loop on Queried Oppty's and add it to List Wrapper.
        ApexPages.Message msgOppCloseMsg;
        String strOppCloseMsg = '';

        for(Opportunity opp: lstOpp)
        {
            String validationStages = System.Label.Opportunity_Close_Validation;

            if(validationStages!= null && validationStages != '' && validationStages.containsIgnoreCase(opp.StageName))
            {

                    if(!strOppCloseMsg.contains(System.Label.R2R_Already_Closed_Opportunities_Error))
                        strOppCloseMsg = System.Label.R2R_Already_Closed_Opportunities_Error;
                    strOppCloseMsg += '\n '+opp.Name+'; ';
            }
            else
            {
                opp.StageName = 'Closed';
                lstWrapper.add(new massCloseWrapper(opp, '', false,'', false, false));
            }
        }

        if(strOppCloseMsg != null && strOppCloseMsg != '')
        {
            msgOppCloseMsg = new ApexPages.Message(ApexPages.Severity.ERROR,strOppCloseMsg);
            ApexPages.addMessage(msgOppCloseMsg);
        }

        if(lstWrapper == null || lstWrapper.size() == 0)
            blnShowSave = false;

        lstWrapper = WrapperSort(lstWrapper);
    }

    public void validateCloseActionValues()
    {
        blnActions = true;

        for(massCloseWrapper oppWrap: lstWrapper)
        {
            
            // system.debug('#### oppWrap.blnShowPostCloseActions :: '+oppWrap.blnShowPostCloseActions);
            // system.debug('#### oppWrap.strPostCloseAction :: '+oppWrap.strPostCloseAction);

            if(oppWrap.blnShowPostCloseActions && oppWrap.strPostCloseAction == '--None--')
            {
                blnActions = false;
                oppWrap.strSaveResult = '\n' + System.Label.R2R_Select_Action_Error;
                // break;
            }
            else
            {
                oppWrap.strSaveResult = '';
            }
        }

        if(!blnActions)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.R2R_Generic_Select_Action_Error);
            ApexPages.addMessage(myMsg);
        }

        // return blnActions;
    }

    public PageReference customSave()
    {
        validateCloseActionValues();

        String retPage =ApexPages.currentPage().getUrl();
        intCountSaved = 0;
        lstSelectedOpp = new List<Opportunity>();

        for (massCloseWrapper oppWrap : lstWrapper) 
        {
            if(oppWrap.strSaveResult == '')
                lstSelectedOpp.add(oppWrap.objOpp);
        }

        // system.debug('#### lstSelectedOpp :: '+lstSelectedOpp);

        List<Asset__c> lstCompAsset = new List<Asset__c>();

        if(lstSelectedOpp != null && lstSelectedOpp.size() > 0)
        {
            Database.SaveResult[] lstOppDMLResult = Database.update(lstSelectedOpp,false);

            getDMLResultData(lstOppDMLResult);

            intCountSaved = lstSelectedOpp.size() - intCountError;

            if(intCountError <= 0 && blnActions)
            {   
                // Setting Page Reference for Custom Save button.This returns to Previous page after Oppty Update. 
                retPage = ApexPages.currentPage().getParameters().get('retURL');
                prefReturn = new PageReference(retPage);
            }
            else
            {
                //if there exist some errored records send Message to VF apge that some records have errored out.
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,intCountSaved+' Opportunities out of '+lstWrapper.size()+' were successfully Closed. Please check the last column for the success/error details.');
                //Add the message to VF page.
                ApexPages.addMessage(msg); 
            }

            /*
            1. Iterate through List Wrapper, process asset records only for those Opps where the save result message is equal to success data label 
            2. Do the transfer or Defer assets
            3. Add the success or error message again
            4. Disable the Post Close Action Picklist on success by disabling blnShowPostCloseActions
            */
            for(massCloseWrapper oppWrap: lstWrapper)
            {
                if(oppWrap.strSaveResult == System.Label.Inline_Edit_Result_Success)
                    lstClosedOpps.add(oppWrap.objOpp);

                // system.debug('#### oppWrap after closed and before actions :: '+oppWrap);

                if(oppWrap.strSaveResult == System.Label.Inline_Edit_Result_Success)
                {
                    if(oppWrap.strPostCloseAction == 'Defer Assets')
                    {
                        for(Opportunity_Asset_Junction__c objOAJ: oppWrap.objOpp.Opportunity_Asset_Junction__r)
                        {
                            setAssetIds.add(objOAJ.Related_Asset__c);
                            oppWrap.blnActionCompleted = true;
                            oppWrap.blnDisablePostCloseActions = true;
                            if(!oppWrap.strSaveResult.contains(System.Label.R2R_Opportunity_Closed_Deferred))
                                oppWrap.strSaveResult += '\n '+System.Label.R2R_Opportunity_Closed_Deferred;
                        }
                    }
                    else if(oppWrap.strPostCloseAction == 'Transfer Assets')
                    {
                        if(oppWrap.objOpp.Duplicate_Opportunity__c == null)
                        {
                            oppWrap.strSaveResult = oppWrap.strSaveResult.remove(System.Label.Inline_Edit_Result_Success);
                            if(!oppWrap.strSaveResult.contains(System.Label.R2R_Transfer_to_Opp_Required))
                                oppWrap.strSaveResult += 'Opportunity Closed. \n '+ System.Label.R2R_Transfer_to_Opp_Required;
                            prefReturn = null;
                        }
                        else
                        {
                            setOpp.add(oppWrap.objOpp);
                            mapOppIdDupOppId.put(oppWrap.objOpp.Id,oppWrap.objOpp.Duplicate_Opportunity__c);
                            try
                            {
                                oppAsset.transferAssetToDuplicateOpp(setOpp,mapOppIdDupOppId);
                                if(!oppWrap.strSaveResult.contains(System.Label.R2R_Opportunity_Closed_Transferred))
                                    oppWrap.strSaveResult += '\n '+System.Label.R2R_Opportunity_Closed_Transferred;
                                oppWrap.blnActionCompleted = true;
                                oppWrap.blnDisablePostCloseActions = true;
                            }
                            catch(Exception e)
                            {
                                oppWrap.strSaveResult = oppWrap.strSaveResult.remove(System.Label.Inline_Edit_Result_Success);
                                oppWrap.strSaveResult = e.getMessage();
                                oppWrap.blnActionCompleted = false;
                            }
                        }
                    }
                    else if(oppWrap.strPostCloseAction == 'No Action')
                        oppWrap.blnDisablePostCloseActions = true;


                    // system.debug('#### strPostCloseAction :: '+oppWrap.strPostCloseAction);
                    // system.debug('#### oppWrap.objOpp.Closed_Reason__c :: '+oppWrap.objOpp.Closed_Reason__c);
                    // system.debug('#### oppWrap.blnAutoCreateAsset :: '+oppWrap.blnAutoCreateAsset);

                    if(oppWrap.objOpp.Closed_Reason__c == 'Lost to Competition' && oppWrap.blnAutoCreateAsset
                        && !oppWrap.blnAssetCreated)
                    {

                        // system.debug('#### Inside Inertion Statement');

                        //Create new Competitive Record when Closed Reason selected = 'Competition - Create Competitive Asset'
                        Asset__c compAsset = new Asset__c();
                        compAsset.Name = oppWrap.objOpp.Competitor_Lost_To__c + ' - ' + oppWrap.objOpp.Account_Name1__c;
                        compAsset.Customer_Name__c = oppWrap.objOpp.AccountId;
                        compAsset.Install_Base_Status__c = 'Install';
                        compAsset.Product_Name_Vendor__c = oppWrap.objOpp.Competitor_Lost_To__c;
                        compAsset.Product_Family__c = oppWrap.objOpp.Competitor_Product__c;
                        compAsset.Model__c = oppWrap.objOpp.Competitor_Product__c;
                        compAsset.Opportunity_Name__c = oppWrap.objOpp.Id;
                        
                        lstCompAsset.add(compAsset);
                        if(!oppWrap.strSaveResult.contains(System.Label.R2R_Competitive_Asset_Created))
                            oppWrap.strSaveResult += '\n '+ System.Label.R2R_Competitive_Asset_Created;
                        oppWrap.blnAssetCreated = true;
                    }

                }
            }

            // system.debug('#### lstCompAsset BEFORE INSERT:: '+lstCompAsset);

            if(lstCompAsset != null && lstCompAsset.size() > 0)
                insert lstCompAsset;

            // system.debug('#### lstCompAsset :: '+lstCompAsset);
            // system.debug('#### setAssetIds :: '+setAssetIds);

            if(setAssetIds != null && setAssetIds.size() > 0)
            {
                try
                {
                    R2R_AssetStatus.StatusSetingCompleted = false;
                    R2R_AssetStatus.deferAsset(new List<Id> (setAssetIds));
                    // oppWrap.blnActionCompleted = true;
                    // oppWrap.strSaveResult = 'Opportunity Closed and Assets Deferred';

                }
                catch (Exception e)
                {
                    // oppWrap.blnActionCompleted = false;
                    // oppWrap.SstrResultMessage = e;
                    // System.debug('#### Exception: '+e);
                }
                setAssetIds.clear();
            }

            lstWrapper = WrapperSort(lstWrapper);

            return prefReturn;
        }

        lstWrapper = WrapperSort(lstWrapper);

        return null;
    }


    public void getDMLResultData(Database.SaveResult[] lstOppDMLResult)
    {   
        Map<Id,massCloseWrapper> mapOppWrapper = new Map<Id,massCloseWrapper>();
        
        for(massCloseWrapper oppWrap: lstWrapper)
            mapOppWrapper.put(oppWrap.objOpp.id, oppWrap);

        // system.debug('#### lstWrapper INSIDE get DML :: '+lstWrapper);

        lstWrapper.clear();
        
        //Hold String for return Page url.
        prefReturn = null;
        
        intCountError=0;
        
        //Holds Value of DML Result Message
        String strResultMessage = '';

        Boolean blnCloseFailed;
        
        //Check if Database.Saveresult has some records.
        if(lstOppDMLResult !=null)
        {
            //Loop on Saveresult records.
            for (integer i = 0; i < lstOppDMLResult.size(); i++) 
            {
                //Initializing strResultMessage as blank for each result record.
                strResultMessage ='';
                
                // system.debug('#### lstOppDMLResult[i].isSuccess() :: '+lstOppDMLResult[i].isSuccess());

                if (!lstOppDMLResult[i].isSuccess()) 
                {
                    //Looping on Database error for ith record and adding error messages.
                    for (Database.Error err : lstOppDMLResult[i].getErrors()) 
                        strResultMessage += err.getMessage();
                    
                    intCountError += 1;
                    blnCloseFailed = false;
                }
                else
                {
                    strResultMessage +=System.Label.Inline_Edit_Result_Success;

                    
                    // system.debug('#### INSIDE success CLOSED message');
                }
                    
                //Add Success/Error message along with Opportunity record to list wrapper.
                
                if(mapOppWrapper != null && lstSelectedOpp != null && lstSelectedOpp.size() > 0 && lstSelectedOpp.get(i) != null && 
                    mapOppWrapper.get(lstSelectedOpp.get(i).id) != null)
                {
                    lstWrapper.add(new massCloseWrapper(lstSelectedOpp.get(i),strResultMessage + mapOppWrapper.get(lstSelectedOpp.get(i).id).strSaveResult, mapOppWrapper.get(lstSelectedOpp.get(i).id).blnAutoCreateAsset, mapOppWrapper.get(lstSelectedOpp.get(i).id).strPostCloseAction, mapOppWrapper.get(lstSelectedOpp.get(i).id).blnAssetCreated, mapOppWrapper.get(lstSelectedOpp.get(i).id).blnDisablePostCloseActions));

                    // system.debug('#### OPP WRAP for Selected Ones :: '+mapOppWrapper.get(lstSelectedOpp.get(i).id));
                    mapOppWrapper.remove(lstSelectedOpp.get(i).id);
                }
            }

            for (massCloseWrapper oppWrap : mapOppWrapper.values()) 
            {
                lstWrapper.add(new massCloseWrapper(oppWrap.objOpp, oppWrap.strSaveResult, oppWrap.blnAutoCreateAsset, oppWrap.strPostCloseAction, oppWrap.blnAssetCreated, oppWrap.blnDisablePostCloseActions));
                // system.debug('#### oppWrap non selected ones :: '+oppWrap);
            }

            lstWrapper = WrapperSort(lstWrapper);
        }
        mapOppWrapper.clear();
    }



/**
Wrapper Class
**/

    public class massCloseWrapper
    {
        //Holds opportunity record. Defining get,set property.
        public Opportunity objOpp {get; set;}
        
        //Holds Error String. Defining get,set property.
        public String strSaveResult {get;set;}

        public String strPostCloseAction {get;set;}
        public Boolean blnActionCompleted {get;set;}
        public Boolean blnShowPostCloseActions {get;set;}
        public Boolean blnAutoCreateAsset {get;set;}
        public Boolean blnAssetCreated {get;set;} 
        public Boolean blnDisablePostCloseActions {get;set;} 

        
        //Defining Constructor with arguments (Opportunity, success message).
        massCloseWrapper(Opportunity objOpp, String strSaveResult, Boolean blnAutoCreateAsset, String strPostCloseAction, Boolean blnAssetCreated, Boolean blnDisablePostCloseActions)
        {
            this.objOpp = objOpp;
            this.strSaveResult = strSaveResult;
            // this.blnActionCompleted = blnActionCompleted;
            this.strPostCloseAction = strPostCloseAction;
            if(this.objOpp.Asset_Count__c > 0 && this.objOpp.Opportunity_Type__c != 'Renewals')
                blnShowPostCloseActions = true;
            else 
                blnShowPostCloseActions = false;

            this.blnAutoCreateAsset = blnAutoCreateAsset;
            this.blnAssetCreated = blnAssetCreated;
            this.blnDisablePostCloseActions = blnDisablePostCloseActions;
        }

        public List<SelectOption> getPostCloseActions()
        {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('--None--','--None--'));
            options.add(new SelectOption('No Action','No Action'));
            options.add(new SelectOption('Defer Assets','Defer Assets'));
            options.add(new SelectOption('Transfer Assets','Transfer Assets'));
            return options;
        }

        public PageReference setClosedReason()
        {
            if(this.objOpp.Closed_Reason__c == 'Lost to Competition')
                blnAutoCreateAsset = true;
            else
                blnAutoCreateAsset = false;

            return null;
        }
    }

    private List<massCloseWrapper> WrapperSort(List<massCloseWrapper> lstWrapper)
    {
        List<String> lstString = new List<String>();
        Map<String, massCloseWrapper> mapOppNameWrapper = new Map<String, massCloseWrapper>();

        for(massCloseWrapper oppWrap:lstWrapper)
        {
            lstString.add(oppWrap.objOpp.Name);
            mapOppNameWrapper.put(oppWrap.objOpp.Name, oppWrap);
        }

        lstString.sort();
        lstWrapper.clear();

        for (String strOppName : lstString) 
        {
            lstWrapper.add(mapOppNameWrapper.get(strOppName));
        }

        return lstWrapper;

    }
}