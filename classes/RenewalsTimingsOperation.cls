/**

Created By : Rajeev Satapathy
Created Date : 11 Spet 2012
Purpose : To calculate the RenewalsTimings field value on opportunity based on the Close Date and Primary Contract Start Date.
WR :  207264     
WR: 230704 -  Created and Udpated the renewals Logic and Country Theatre Mapping for the field Renewals Area
**/
    


public class RenewalsTimingsOperation{

    public  void calculateRenewals(Opportunity opty){
        if(opty.HW_TLA_Start_Date__c !=  Null && opty.CloseDate != Null){
            Integer primnaryContractQrtr= getQuarter(opty.HW_TLA_Start_Date__c - 1);
            Integer closeDateQtr = getQuarter(opty.CloseDate);
            Integer Pyear =  (opty.HW_TLA_Start_Date__c - 1).year();
            Integer Cyear =  opty.CloseDate.year();
            
        if(((opty.HW_TLA_Start_Date__c - 1).daysBetween(opty.CloseDate))> 0){
              if(Cyear == Pyear){
                if((closeDateQtr - primnaryContractQrtr > 2)){
                opty.Renewals_Timing__c = 'Extreme Expired';
                }
               else if((closeDateQtr - primnaryContractQrtr == 1)){
                 opty.Renewals_Timing__c = 'Expired';
                }
               else if((closeDateQtr - primnaryContractQrtr == 2 )){
                opty.Renewals_Timing__c = 'Expired';
                }
               else if(closeDateQtr - primnaryContractQrtr == 0){
                opty.Renewals_Timing__c = 'Current';
                }
              }

              if(Cyear - Pyear == 1){
                if((closeDateQtr - primnaryContractQrtr == -3)||(closeDateQtr - primnaryContractQrtr == -2)){
                opty.Renewals_Timing__c = 'Expired';
                }
//Updated the code as part of the ERFC - 12/12/12 - Work Request - #222754 ,230704
                else if(
                        (closeDateQtr - primnaryContractQrtr == 1)||
                        (closeDateQtr - primnaryContractQrtr == 2)||
                        (closeDateQtr - primnaryContractQrtr == 3)||
                        (closeDateQtr - primnaryContractQrtr == -1)||
                        (closeDateQtr - primnaryContractQrtr == 0))
                         {
                opty.Renewals_Timing__c = 'Extreme Expired';
                }
              }
              if(Cyear - Pyear >= 2){
              opty.Renewals_Timing__c = 'Extreme Expired';
              }
              
            }
            if(((opty.CloseDate).daysBetween(opty.HW_TLA_Start_Date__c - 1))> 0){
            if(Cyear == Pyear){
                if((primnaryContractQrtr - closeDateQtr == 2)){
                opty.Renewals_Timing__c = 'Future';
                }
                else if((primnaryContractQrtr - closeDateQtr == 1)){
                opty.Renewals_Timing__c = 'Future';
                }
                else if((primnaryContractQrtr - closeDateQtr > 2)){
                opty.Renewals_Timing__c = 'Extreme Future';
                }
                else if(primnaryContractQrtr - closeDateQtr == 0){
                opty.Renewals_Timing__c = 'Current';
                }
              }

             if(Pyear - Cyear == 1){
                if((primnaryContractQrtr - closeDateQtr == -3)||(primnaryContractQrtr - closeDateQtr == -2)){
                opty.Renewals_Timing__c = 'Future';
                }
//Updated the code as part of the ERFC - 12/12/12 - Work Request - #222754,#230704
                else if(
                        (primnaryContractQrtr - closeDateQtr == 1)||
                        (primnaryContractQrtr - closeDateQtr == 2)||
                        (primnaryContractQrtr- closeDateQtr == 3)||
                        (primnaryContractQrtr - closeDateQtr == -1)||
                        (primnaryContractQrtr - closeDateQtr == 0)
                        ){
                opty.Renewals_Timing__c = 'Extreme Future';
                }
              }
              if(Pyear - Cyear >=2){
               opty.Renewals_Timing__c = 'Extreme Future';         
           }           
           }
  //Updated the code as part of the ERFC - 12/12/12 - Work Request - #222754
           if(opty.CloseDate == opty.HW_TLA_Start_Date__c - 1)
           {
               opty.Renewals_Timing__c = 'Current';
           }
      }  
   }
   
    public static Integer  getQuarter(Date quarterDate){         
        Integer monthValue = quarterDate.month();
        if(monthValue == 1 ||monthValue == 2 || monthValue == 3){
            return 1;
        }
        if(monthValue == 4 ||monthValue == 5 || monthValue == 6){
            return 2;
        }
        if(monthValue == 7 ||monthValue == 8 || monthValue == 9){
            return 3;
        }        
        else{
            return 4;
        }                
   }
  
  // Method for Renewals Area Calculation  - WR # 230704
   public void populateTheaterFromCountryOnOppty(Opportunity oppObj){
            Map<String,RenewalsAreaMapping__c> DataValueMap = RenewalsAreaMapping__c.getAll();
            String strCountryname = '';
                System.debug('acc.Country__c-->' + oppObj.Country__c);
                if(OppObj.Country__c != null){
                    strCountryname = oppObj.Country__c;
                    System.debug('strCountryname.length()-->' + strCountryname.length());
                    if(strCountryname.length() >= 36){
                        strCountryname = strCountryname.substring(0, 36);
                        System.debug('strCountryname trim-->' + strCountryname);
                    }                   
                    if(DataValueMap.containsKey(strCountryname)){
                        oppObj.Renewals_Area__c= DataValueMap.get(strCountryname).Theater__c; 
                        system.debug('DealReg_Theater__c --'+ oppObj.Renewals_Area__c);
                    }                   
               }
      }
 
}