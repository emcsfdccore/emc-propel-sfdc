/*=========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  23.09.2010      Anand Sharma    #779        Test class for the unit test of PRM_LeadAcceptReject class.             
 |  06.10.2010      Anand Sharma                Better code coverage.
 |  14/12/2011      Anil                        Removed Query for fetching Partner and used Custom setting Data Helper
 |  11-JAN-2012   Anil                      Removed role Id
 |  25-Nov-2013     Naga            #311032		This test class is modified to reduce the processing time
 +=========================================================================================================================*/
@isTest
private class PRM_LeadAcceptReject_TC {

    PRM_LeadAcceptReject_TC(){
    	CustomSettingDataHelper.dataValueMapCSData();
    	CustomSettingDataHelper.bypassLogicCSData();
    	CustomSettingDataHelper.eBizSFDCIntCSData();
    	CustomSettingDataHelper.profilesCSData();
    	CustomSettingDataHelper.dealRegistrationCSData();
    	CustomSettingDataHelper.distributorPartnerUsersCSData();
    }
    static testMethod void prmLeadAcceptRejectUnitTest() {
       PRM_LeadAcceptReject_TC prmLeadAcc = new PRM_LeadAcceptReject_TC();        
       User partner;
       User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
       List<Account> acc = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();       
       insert acc;
       
       Contact cont = UserProfileTestClassDataHelper.createContact();
       cont.AccountId=acc[0].Id;
       User insertUser1 = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
       System.runAs(insertUser1){
       	  insert cont;
       }
       System.runAs(insertUser)
       { 
	      Profile amerUserProf = [select Id from Profile where Name=: 'EMEA Distributor Super User'];
	      partner = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.id,null,cont.Id);
	      insert partner;
       }
        /* create lead data */
        Lead testLead2 = new Lead(LastName='testlm2', Accept_Lead__c = false, Reject_Lead__c = false, 
                                  Rejection_Reason__c=null, firstname='testfm2', company='test2', email='test2@test.com');
        insert testLead2;
        
        /* update owner of lead record */
        testLead2.OwnerId = partner.Id;
        update testLead2;
        
        /* run as partner user to reject lead */
        System.runAs(partner){          
            
            /* set the lead id and parent value */          
            System.currentPageReference().getParameters().put('Id',testLead2.Id);
            System.currentPageReference().getParameters().put('parent','true'); 

            /* Create a new instance of the object */ 
            PRM_LeadAcceptReject objLeadAcceptReject = new PRM_LeadAcceptReject();
            
            /* set rejection reason on lead */          
            objLeadAcceptReject.leadObject.Rejection_Reason__c = 'insufficent reason';
            
            /*call method to reject lead */
            objLeadAcceptReject.Reject();
             
        }         
    }
}