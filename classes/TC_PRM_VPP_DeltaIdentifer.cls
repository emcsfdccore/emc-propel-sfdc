@isTest
private class TC_PRM_VPP_DeltaIdentifer {
      
        @testSetup static void TestData()
        {
            System.runAs(new user(Id = UserInfo.getUserId()))
                {
                    CustomSettingDataHelper.dataValueMapCSData();
                    CustomSettingDataHelper.bypassLogicCSData();
                    CustomSettingDataHelper.eBizSFDCIntCSData();
                    CustomSettingDataHelper.profilesCSData();
                    CustomSettingDataHelper.countryTheaterMapCSData();
                    CustomSettingDataHelper.dealRegistrationCSData();
                    CustomSettingDataHelper.VCEStaticCSData();
                }
                
            // create account record.
            List<Account> acc = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();  
            insert acc; 
        
            // Create Account Grouping records.
            list<Account_Groupings__c > listAG = new list<Account_Groupings__c >();
            listAG.add(new Account_Groupings__c(process_on_after__c = System.today()+6, Name = 'Account Grouping0'));
            listAG.add(new Account_Groupings__c(process_on_after__c = System.today()+6, Name = 'Account Grouping1'));
            insert listAG;
            
            // create contact record.
            list<Contact> listCon = new list<Contact>();
            listCon.add(new Contact(accountID = acc[0].id, LastName = 'Test Contact0'));
            listCon.add(new Contact(accountID = acc[0].id, LastName = 'Test Contact1'));
            insert listCon;
            
            //Create Education_Groups records.
            list <Education_Groups__c> listEduGroup = new list <Education_Groups__c>();
            listEduGroup.add(new Education_Groups__c(Education_Group_Name__c = 'Group name0', Mentoring__c = true));
            listEduGroup.add(new Education_Groups__c(Education_Group_Name__c = 'Group name1', Mentoring__c = false));
            insert listEduGroup;
            
            acc[0].Grouping__c = listAG[0].id;
            acc[1].Grouping__c = listAG[0].id;
            update acc;
        }
        
    static testMethod void AccountGroupingDeltaTest() 
    {
        list<Account_Groupings__c > lisPanAG = [select id, name from Account_Groupings__c where name in ('Account Grouping0', 'Account Grouping1')];
        
        list<Account_Groupings__c > lisLogicalAG = new list<Account_Groupings__c >();
        lisLogicalAG.add(new Account_Groupings__c(process_on_after__c = System.today()+6, Name = 'Account Grouping0', Logical_Block_Level_Grouping__c = lisPanAG[1].id, Pan_Theater_Level_Grouping__c = lisPanAG[0].id));
        lisLogicalAG.add(new Account_Groupings__c(process_on_after__c = System.today()+6, Name = 'Account Grouping0'));
        insert lisLogicalAG;
        
        list<Account_Groupings__c > listAG = new list<Account_Groupings__c >();
        listAG.add(new Account_Groupings__c(process_on_after__c = System.today()+6, Name = 'Account Grouping0', Logical_Block_Level_Grouping__c =lisLogicalAG[0].id ));
        listAG.add(new Account_Groupings__c(process_on_after__c = System.today()+6, Name = 'Account Grouping1',Logical_Block_Level_Grouping__c =lisLogicalAG[1].id ));
        insert listAG;
        
        listAG[0].active__c=false;
        listAG[0].Inactivation_Reason__c='XXXXX'; 
        update listAG;
        
        List<Account> acc = [ select id, name, Grouping__c from account where name in ('TestPartnerAcc', 'TestPartnerAcc1')]; 
        
        acc[0].Grouping__c = listAG[0].id;
        update acc;
        
        list<Revenue__c>  listRev= new list<Revenue__c>();
        listRev.add( new Revenue__c( Q1_Growth_Revenue__c=100, Grouping__c=acc[0].Grouping__c, Account__c = acc[0].id )); 
        insert listRev;
        
        listRev[0].Q1_Total_Revenue__c=200;
        update listRev;
        
        delete listRev;
        
    }
    
    static testMethod void EducationDeltaTest() 
    {
                    
        List<Account> acc = [ select id, name, Grouping__c from account where name in ('TestPartnerAcc', 'TestPartnerAcc1')];
        
        list<Education_Master__c> listEduMaster= new list<Education_Master__c>();
        listEduMaster.add(new Education_Master__c(ESBI_ID__c = 'text', Skills_Verification_Required__c = true));
        listEduMaster.add(new Education_Master__c(ESBI_ID__c = 'text1', Skills_Verification_Required__c = true));
        insert listEduMaster ;
        
        list<Contact> listCon = [select id , name from contact where name in('Test Contact0', 'Test Contact1')];
        
        // Inserting , updating and deleting Education__c
        list<Education__c> listEduc = new list<Education__c>();
        listEduc.add( new Education__c( Partner_Grouping__c= acc[0].Grouping__c, Category__c = 'QuickStarts' , Education_Master__c = listEduMaster[0].id , Contact__c = listCon[0].id)); 
        listEduc.add( new Education__c( Partner_Grouping__c= acc[0].Grouping__c, Category__c = 'QuickStarts', Contact__c = listCon[0].id ));        
        insert listEduc;
        
        listEduc[0].Category__c='Product Certifications';
        listEduc[0].Education_Master__c = listEduMaster[1].id;
        update listEduc;
        
        delete listEduc;
        
        list <Education_Groups__c> listEduGroup = [select id, Education_Group_Name__c from Education_Groups__c where Education_Group_Name__c = 'Group name0' ];
        
        // Inserting , updating and deleting Education_Group_Member__c
        list<Education_Group_Member__c> listEduMember= new list<Education_Group_Member__c>();
        listEduMember.add(new Education_Group_Member__c(Education_Master__c = listEduMaster[0].id , Education_Group__c = listEduGroup[0].id));
        insert listEduMember;
        
        update listEduMember;
        
        delete listEduMember;
        
        // Inserting , updating and deleting Contact
        listCon[1].accountID = acc[1].id;
        update listCon;
        delete listCon;
        
    }

    static testMethod void RulesDeltaTest() 
    {
        
        list <Velocity_Rules__c> listVR = new list <Velocity_Rules__c>();
        listVR.add(new Velocity_Rules__c(Theater__c='APJ',cluster__c='NA',  Specialty_Rule_Type__c='NA' , Tier__c='Affiliate Elite', recordtypeid='01270000000Q6iXAAS'));
        listVR.add(new Velocity_Rules__c(Theater__c='APJ',cluster__c='NA',  Specialty_Rule_Type__c='NA' , Tier__c='Affiliate Elite', recordtypeid='01270000000Q6iSAAS'));
        listVR.add(new Velocity_Rules__c(Theater__c='APJ',cluster__c='NA',  Specialty_Rule_Type__c='NA' , Tier__c='Affiliate Elite', recordtypeid='01270000000Q6icAAC'));
        listVR.add(new Velocity_Rules__c(Theater__c='APJ',cluster__c='NA',  Specialty_Rule_Type__c='NA' , Tier__c='Affiliate Elite', recordtypeid='01270000000Q8iDAAS'));
        listVR.add(new Velocity_Rules__c(Theater__c='APJ',cluster__c='NA',  Specialty_Rule_Type__c='NA' , Tier__c='Affiliate Elite', recordtypeid='01270000000Q6iNAAS'));
        listVR.add(new Velocity_Rules__c(Theater__c='APJ',cluster__c='NA',  Specialty_Rule_Type__c='NA' , Tier__c='Affiliate Elite'));
        insert  listVR;
        
        listVR[1].Tier__c='XXXX';
        update  listVR;
        
       list <Education_Groups__c> listEduGroup = [select id, Education_Group_Name__c from Education_Groups__c where Education_Group_Name__c = 'Group name0' ];
        
        list<Velocity_Rule_Member__c> objVelRuleMember = new list<Velocity_Rule_Member__c>();
        objVelRuleMember.add(new Velocity_Rule_Member__c (Speciality_Rule__c=listVR[0].id,Group__c=listEduGroup[0].Id));
        objVelRuleMember.add(new Velocity_Rule_Member__c (Speciality_Rule__c=listVR[0].id,Group__c=listEduGroup[0].Id));
        insert objVelRuleMember;
        
        update objVelRuleMember; 
        
        database.delete (objVelRuleMember,false);
        
        database.delete (listVR,false);
         
    }
}