public class PSCOperations{
   
    //Recursive control flag
    public static boolean OnInsertFlag = false;
    
    public List<Case> pscCaseURLupdateLst=new List<Case>(); 
    
    public void setCaseOrigin(List<Case> lstCase){
        List<Case> lstPSCcases=new List<Case>();
        Map<String,Schema.RecordTypeInfo> recordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        for(String str : recordTypes.keySet()){
            for(Case caseRecord:lstCase){
                if(Str.containsIgnoreCase('PSC')){
                    if(recordTypes.get(str).getRecordTypeId()==caseRecord.RecordTypeID){
                        caseRecord.Origin = System.Label.PSC_Partner_Central_Origin;
                    }
                }
            }
        }    
    }
    public void setPartnerType(List<Case> lstCase){
        List<Case> lstPSCcases=new List<Case>();
        Map<String,Schema.RecordTypeInfo> recordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
 
            for(String str : recordTypes.keySet()){
                for(Case caseRecord:lstCase)
                {
                    if(Str.containsIgnoreCase('PSC')){
                        if(recordTypes.get(str).getRecordTypeId()==caseRecord.RecordTypeID){
                        lstPSCcases.add(caseRecord);
                        }
                    }
                }
            }
            if(lstPSCcases!=null && lstPSCcases.size()>0){
                Set<Id> partnerAccSet = new Set<Id>();
                List<Case> casesToUpdate=new List<Case>();
                for(Case pscCase:lstPSCcases){
                    if(pscCase.Partner_Account_Name__c!=null){
            
                        partnerAccSet.add(pscCase.Partner_Account_Name__c);
                        casesToUpdate.add(pscCase);
                        //mapCaseDealRegNum.put(pscCase.Id,pscCase);
                    }
                }  
            
                List<Account> accList=[SELECT id,name,Partner_Type_Picklist__c FROM Account WHERE Id IN:partnerAccSet];
                if(accList.size()>0){
                    for(Case caseRec:casesToUpdate){
                        for(Account acc:accList){
                            if(caseRec.Partner_Account_Name__c==acc.Id){
                                
                                caseRec.Partner_Type__c = acc.Partner_Type_Picklist__c;
                                
                            }
                       
                        }
                   
                    }
                } 
            }
    
    }

    
    public void updateDealRegUrl(List<Case> lstCase){
        List<Case> lstPSCcases=new List<Case>();
        Map<String,Schema.RecordTypeInfo> recordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
 
        for(String str : recordTypes.keySet()){
   
            for(Case caseRecord:lstCase)
                {
                    if(Str.containsIgnoreCase('PSC')){
                        if(recordTypes.get(str).getRecordTypeId()==caseRecord.RecordTypeID){
                        lstPSCcases.add(caseRecord);
                   
                        }
                    }
           
            
                }
           
        }          
        if(lstPSCcases!=null && lstPSCcases.size()>0){
            Set<String> dealRegforURlSet = new Set<String>();
            List<Case> casesToUpdate=new List<Case>();
            for(Case pscCase:lstPSCcases){
                if(pscCase.Deal_Registration_Number__c!=null && pscCase.Deal_Registration_Number__c!=''){
            
                    dealRegforURlSet.add(pscCase.Deal_Registration_Number__c);
                    casesToUpdate.add(pscCase);
                    //mapCaseDealRegNum.put(pscCase.Id,pscCase);
                }
            }  
                
            List<Lead> LeadList=[SELECT id,name,DealReg_Deal_Registration_Number__c,EMC_Classification_Formula__c FROM Lead WHERE DealReg_Deal_Registration_Number__c IN:dealRegforURlSet];
               
                if(LeadList.size()>0){
                    for(Case caseRec:casesToUpdate){
                        for(Lead leadRec:LeadList){
                            if(caseRec.Deal_Registration_Number__c==leadRec.DealReg_Deal_Registration_Number__c){
                                /*if(caseRec.Origin=='Web'){
                                caseRec.Deal_Registration_URL__c=Label.PSC_ServerURL+ '/' + leadRec.Id;
                                }
                                else{
                                caseRec.Deal_Registration_URL__c=URL.getSalesforceBaseUrl().toExternalForm() + '/' + leadRec.Id;
                                }*/
                                caseRec.Deal_Reg_URL__c='<a href=/' +leadRec.Id + '>' +caseRec.Deal_Registration_Number__c  + '</a>';
                                caseRec.Lead_Name__c = leadRec.Id;
                                caseRec.Lead_EMC_Classification__c = leadRec.EMC_Classification_Formula__c;
                                pscCaseURLupdateLst.add(caseRec);
                                
                            }
                       
                        
                        }
                   
                    }
                }   
        }    
    }
    public void updateOpptyUrl(List<Case> lstCase){
        List<Case> lstPSCcases=new List<Case>();
        Map<String,Schema.RecordTypeInfo> recordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
 
        for(String str : recordTypes.keySet()){
   
            for(Case caseRecord:lstCase)
                {
                 if(Str.containsIgnoreCase('PSC')){
                    if(recordTypes.get(str).getRecordTypeId()==caseRecord.RecordTypeID){
                        lstPSCcases.add(caseRecord);
                   
                    }
                }
           
            
            }
           
                
        }
        if(lstPSCcases!=null && lstPSCcases.size()>0){
            Set<String> opptyforURlSet = new Set<String>();
            List<Case> casesToUpdate=new List<Case>();
            for(Case pscCase:lstPSCcases){
                if(pscCase.Opp_Number__c!=null && pscCase.Opp_Number__c!=''){
           
                opptyforURlSet.add(pscCase.Opp_Number__c);
                casesToUpdate.add(pscCase);
                //mapCaseDealRegNum.put(pscCase.Id,pscCase);
                }
            }  
                
            List<Opportunity> OpptyList=[SELECT id,Opportunity_Number__c FROM Opportunity WHERE Opportunity_Number__c IN:opptyforURlSet];
               
                if(OpptyList.size()>0){
                    for(Case caseRec:casesToUpdate){
                        for(Opportunity oppRec:OpptyList){
                           if(caseRec.Opp_Number__c==oppRec.Opportunity_Number__c){
                                /*if(caseRec.Origin=='Web'){
                                caseRec.Opportunity_URL__c=Label.PSC_ServerURL+ '/' + oppRec.Id;
                                }
                                else{
                                caseRec.Opportunity_URL__c=URL.getSalesforceBaseUrl().toExternalForm() + '/' + oppRec.Id;
                                }*/
                                caseRec.Oppty_URL__c='<a href=/' +oppRec.Id+ '>' +caseRec.Opp_Number__c + '</a>';
                                caseRec.Opportunity_Name__c=oppRec.Id;
                                //pscCaseURLupdateLst.add(caseRec);
                                
                           
                            }
                       
                        
                        }
                   
                    }
                }   
        }
    }
    public void updateStatus(List<Case> lstCase,map<id,Case> oldMap){
        System.debug('updateStatus-->');
        List<Case> lstPSCcases=new List<Case>();   
        Map<String,Schema.RecordTypeInfo> recordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
 
        for(String str : recordTypes.keySet()){
   
            for(Case caseRecord:lstCase)
            {
                 if(Str.containsIgnoreCase('PSC')){
                    if(recordTypes.get(str).getRecordTypeId()==caseRecord.RecordTypeID){
                        lstPSCcases.add(caseRecord);
                   
                    }
                }
           
            
            }
           
        } 
        if(lstPSCcases!=null && lstPSCcases.size()>0){
            for(Case caseObj : lstPSCcases ){
            
                if((caseObj.Status == 'Open' && caseObj.ownerId != oldMap.get(caseObj.Id).OwnerId) && ( String.ValueOf(oldMap.get(caseObj.Id).OwnerId).substring(0,3)=='00G' && String.ValueOf(caseObj.OwnerId).substring(0,3)=='005') && (caseObj.IsFirstUsrAllocation__c !=oldMap.get(caseObj.Id).IsFirstUsrAllocation__c) ){
                 caseObj.Status ='In Progress'; 
                    System.debug('caseObj.Status-->'+caseObj.Status);
                }
               
            }
        }   
    }
    
    public void restrictReqTostatusChange(List<Case> lstCase,map<id,Case> oldMap){
        User user = [Select profileId,name from User where id=:Userinfo.getuserid()];
        Map<String,Schema.RecordTypeInfo> recordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        List<Case> lstPSCcases=new List<Case>(); 
 
        for(String str : recordTypes.keySet()){
   
            for(Case caseRecord:lstCase)
            {
                 if(Str.containsIgnoreCase('PSC')){
                    if(recordTypes.get(str).getRecordTypeId()==caseRecord.RecordTypeID){
                        lstPSCcases.add(caseRecord);
                   
                    }
                }
           
            
            }
           
        }
        if(lstPSCcases!=null && lstPSCcases.size()>0){
            List<String> profNames=new List<String>();
            Map<Id,Profile> profMap;
            PSC_FieldMapping__c pscCustSet=PSC_FieldMapping__c.getValues('Case Requestors');
            profNames=pscCustSet.DependentValues__c.split(',');
            profMap=new Map<Id,Profile>([Select id,name from profile where id in :profNames]);
            for(Case caseObj:lstPSCcases){
                                    
                if((caseObj.Status!=oldMap.get(caseObj.Id).Status)&& profMap.get(user.profileId)!=null){
                caseObj.addError(Label.PSC_Req_Prof_Restrict);
                return;
                }
                               
            }
        }       
               
    }
    public void autoEmailtoDealRegApprovers(List<Case> lstCase){
        PSC_FieldMapping__c fieldMapConfig = PSC_FieldMapping__c.getValues('DealApproversEmailTemplateId');
       
        Map<Id,Lead> leadMap;
        List<Case> caseToupdateLst=new List<Case>();
        List<Lead> leadToUpdate = new List<Lead>();
        List<Case> lstPSCcases=new List<Case>();
        Messaging.SingleEmailMessage singleEmail;
        List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();
        Map<String,Schema.RecordTypeInfo> recordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        for(String str : recordTypes.keySet()){
  
            for(Case caseRecord:lstCase)
            {
                 if(Str.containsIgnoreCase('PSC')){
                    if(recordTypes.get(str).getRecordTypeId()==caseRecord.RecordTypeID){
                        lstPSCcases.add(caseRecord);
                  
                    }
                }
          
            
            }
          
        }            
        if(lstPSCcases!=null && lstPSCcases.size()>0){
        Map<Id,Lead> mapCaseIDLead;
        Map<Id,Set<Id>> mapDealRegIdUserLst;
      
        Set<String> dealRegNoSet = new Set<String>();
        Set<ID> userIdSet= new Set<ID>();
        List<Case> caseList = new List<Case>();
        for(Case pscCase:lstPSCcases){
            //System.debug('pscCase--->'+pscCase.RecordType);
            //System.debug('pscCase--->'+pscCase.RecordType.DeveloperName);
            System.debug('pscCase--->'+pscCase.Type);
            System.debug('pscCase--->'+pscCase.Subject);
            System.debug('pscCase--->'+pscCase.Deal_Registration_Number__c);
            if(pscCase.Type =='Program and Process Queries' && pscCase.Subject != null && pscCase.Subject !='' && pscCase.Subject.contains('Field Approval Request for Deal Registration') && pscCase.Deal_Registration_Number__c != null){
                dealRegNoSet.add(pscCase.Deal_Registration_Number__c);
                caseList.add(pscCase);
            }
        }
        if(dealRegNoSet !=null && dealRegNoSet.size()>0){
            leadMap = new Map<Id,Lead>([SELECT id,DealReg_Distributor_Direct_Reseller__c,DealReg_Deal_Registration_Number__c,Case__c,Additional_Field_Approver_1__c,Additional_Field_Approver_2__c,Partner_Manager__c,Additional_Notification_Email_1__c,Field_Approver_Manager__c,Field_Approver__c,Company,DealReg_Submission_Date__c,DealReg_Expected_Deal_Value__c,DealReg_Deal_Description__c,DealReg_Deal_Registration_Justification__c,DealReg_Expected_Close_Date__c,Street,City,State,Country,DealReg_Distribution_VAR_Name__c,DealReg_Registered_Products__c,DealReg_Deal_Submitter_Name__c,DealReg_Deal_Submitter_Rep_Email__c,DealReg_Department_Project_Name__c,DealReg_Project_Scope__c,DealReg_Application__c,Dealreg_Is_it_a_VSPEX_Deal__c,DealReg_Is_Vmware_related_solution__c,PSC_Recommendation__c,Field_Approval_Required__c,DealReg_Deal_Registration__c from Lead where DealReg_Deal_Registration_Number__c IN: dealRegNoSet]);
          
            for(Id drID: leadMap.keySet()){
              
                System.debug('drID--->'+drID);
                System.debug('drID--->'+leadMap.get(drID).Partner_Manager__c);
                System.debug('drID--->'+leadMap.get(drID).Additional_Field_Approver_2__c);
                userIdSet.add(leadMap.get(drID).Partner_Manager__c);
                userIdSet.add(leadMap.get(drID).Additional_Notification_Email_1__c);
              
            }
            Map<Id,User> mapUserIdEmail =new Map<Id,User>([SELECT id,email from User where id IN:userIdSet]);
          
            if(caseList !=null && caseList.size()>0){
          
                List<Case> caseListToUpdate = [Select Id,ThreadId__c, Deal_Registration_Number__c,Lead_Name__c from case where Id IN: caseList];
              
                
                
                for(Case cs: caseListToUpdate){
                    for(Id drID: leadMap.keySet()){
                        if(cs.Deal_Registration_Number__c!=null && leadMap.get(drID).DealReg_Deal_Registration_Number__c!=null && cs.Deal_Registration_Number__c==leadMap.get(drID).DealReg_Deal_Registration_Number__c){
                            cs.Lead_Name__c=drID;
                            //Lead ld = new Lead(Id = drID);
                            caseToupdateLst.add(cs);
                            //leadToUpdate.add(ld);
                                                                                                               
                            leadMap.get(drID).Case__c=cs.Id;
                            System.debug('---->DealReg_Distributor_Direct_Reseller__c'+leadMap.get(drID).DealReg_Distributor_Direct_Reseller__c);
                            String sub=Label.APPROVAL_REQUEST+': Registration #:'+(leadMap.get(drID).DealReg_Deal_Registration_Number__c!=null?leadMap.get(drID).DealReg_Deal_Registration_Number__c:'')+' '+Label.Distributor_Direct_Reseller+':'+(leadMap.get(drID).DealReg_Distributor_Direct_Reseller__c!=null?leadMap.get(drID).DealReg_Distributor_Direct_Reseller__c:'')+' '+Label.VAR+':'+(leadMap.get(drID).DealReg_Distribution_VAR_Name__c!=null?leadMap.get(drID).DealReg_Distribution_VAR_Name__c:'')+' '+Label.for + ' '+(leadMap.get(drID).Company!=null?leadMap.get(drID).Company:'')+ cs.ThreadId__c;
                                                                                                               
                                                                                                               
                            String body = Label.review_the_registration_request_below+':<br></br><br></br>'+Label.ALL_REGIONS+'<ol>'+
  '<li>'+Label.ALL_REGIONS_Point1+'</li>'+
  '<li>'+Label.ALL_REGIONS_Point2+'</li>'+
  '<li>'+Label.ALL_REGIONS_Point3_1+Label.ALL_REGIONS_Point3_2+'</li>'+
'</ol>'+Label.PSC_Recommendation+':'+(leadMap.get(drID).PSC_Recommendation__c!=null?leadMap.get(drID).PSC_Recommendation__c:'')+
'<br></br><br></br>'+'**'+Label.PSC_Recommendation_Note+
'<br></br><br></br>'+
'<table>'+
     '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Deal_Registration_Number+': '+(leadMap.get(drID).DealReg_Deal_Registration_Number__c!=null?leadMap.get(drID).DealReg_Deal_Registration_Number__c:'')+'</td>'+
    '</tr>'+
     '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Submission_Date+': '+ leadMap.get(drID).DealReg_Submission_Date__c+'</td>'+
    '</tr>'+
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Expected_Deal_Value+': '+ '&nbsp;' +(leadMap.get(drID).DealReg_Expected_Deal_Value__c!=null?leadMap.get(drID).DealReg_Expected_Deal_Value__c:0.0)+'</td>'+
    '</tr>'+
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Deal_Description + (leadMap.get(drID).DealReg_Deal_Description__c!=null?leadMap.get(drID).DealReg_Deal_Description__c:'')+'</td>'+
    '</tr>'+
   
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Deal_Justification+': '+ (leadMap.get(drID).DealReg_Deal_Registration_Justification__c!=null?leadMap.get(drID).DealReg_Deal_Registration_Justification__c:'')+'</td>'+
    '</tr> '+
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Expected_Close_Date1+': '+ leadMap.get(drID).DealReg_Expected_Close_Date__c+'</td>'+
    '</tr>'+
 
   
   
    '<tr>'+
         '<td>'+'&nbsp;'+'</td>'+
     '</tr>'+
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Company+': '+ (leadMap.get(drID).Company!=null?leadMap.get(drID).Company:'')+'</td>'+
    '</tr>'+
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Customer_Contact_Name+':'+ (leadMap.get(drID).Company!=null?leadMap.get(drID).Company:'')+'</td>'+
    '</tr>'+
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Street_Address+': '+ (leadMap.get(drID).Street!=null?leadMap.get(drID).Street:'')+'</td>'+
    '</tr>'+        
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.City+': '+ (leadMap.get(drID).City!=null?leadMap.get(drID).City:'')+'</td>'+
    '</tr>'+
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.State_Province+': '+ (leadMap.get(drID).State!=null?leadMap.get(drID).State:'')+'</td>'+
    '</tr>'+
     '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Country+': '+(leadMap.get(drID).Country!=null?leadMap.get(drID).Country:'')+'</td>'+
    '</tr>'+
     '<tr>'+
         '<td>'+'&nbsp;'+'</td>'+
     '</tr>'+
     '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Distributor_Direct_Reseller+': '+ (leadMap.get(drID).DealReg_Distributor_Direct_Reseller__c!=null?leadMap.get(drID).DealReg_Distributor_Direct_Reseller__c:'')+'</td>'+
    '</tr>'+
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.VAR+': '+(leadMap.get(drID).DealReg_Distribution_VAR_Name__c!=null?leadMap.get(drID).DealReg_Distribution_VAR_Name__c:'') +'</td>'+
    '</tr>'+
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Registration_Products+': '+ (leadMap.get(drID).DealReg_Registered_Products__c!=null?leadMap.get(drID).DealReg_Registered_Products__c:'')+'</td>'+
    '</tr>'+
    '<tr>'+
         '<td>'+'&nbsp;'+'</td>'+
     '</tr>'+
     '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Deal_Submitter_Name+': '+ (leadMap.get(drID).DealReg_Deal_Submitter_Name__c!=null?leadMap.get(drID).DealReg_Deal_Submitter_Name__c:'')+'</td>'+
    '</tr>'+
     '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Deal_Submitter_Email_Address+': '+ (leadMap.get(drID).DealReg_Deal_Submitter_Rep_Email__c!=null?leadMap.get(drID).DealReg_Deal_Submitter_Rep_Email__c:'')+'</td>'+
    '</tr>'+
    '<tr>'+
         '<td>'+'&nbsp;'+'</td>'+
     '</tr>'+    
     '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Department_Project_Name+': '+ (leadMap.get(drID).DealReg_Department_Project_Name__c!=null?leadMap.get(drID).DealReg_Department_Project_Name__c:'')+'</td>'+
     '</tr>'+
     '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Project_Scope+': '+ (leadMap.get(drID).DealReg_Project_Scope__c!=null?leadMap.get(drID).DealReg_Project_Scope__c:'')+'</td>'+
    '</tr>'+ 
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Application+': '+ (leadMap.get(drID).DealReg_Application__c!=null?leadMap.get(drID).DealReg_Application__c:'')+'</td>'+
    '</tr>'+
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Is_this_a_VSPEX_Registration+': '+ (leadMap.get(drID).Dealreg_Is_it_a_VSPEX_Deal__c!=null?leadMap.get(drID).Dealreg_Is_it_a_VSPEX_Deal__c:'')+'</td>'+
    '</tr>'+
     '<tr>'+
        '<td width="17%">'+'</td>'+
        '<td width="83%">'+Label.Is_this_a_VMWare_related_Solution+': '+ (leadMap.get(drID).DealReg_Is_Vmware_related_solution__c!=null?leadMap.get(drID).DealReg_Is_Vmware_related_solution__c:'')+'</td>'+
    '</tr>'+
    '<tr>'+
        '<td width="15%">'+'</td>'+
        '<td width="85%">'+Label.Competitive_Vendor+': '+(leadMap.get(drID).Dealreg_Is_it_a_VSPEX_Deal__c!=null?leadMap.get(drID).Dealreg_Is_it_a_VSPEX_Deal__c:'')+'</td>'+
    '</tr>'+
    '<tr>'+
        '<td>'+'&nbsp;'+'</td>'+
    '</tr>'+
'</table>'+
 
 
'<br/><br/>'+Label.Thanks+
'<br/>'+Label.EMC_Deal_Reg_Team;
 
                            List<String> emailLst=new List<String>();
                            if(leadMap.get(drID).Additional_Field_Approver_1__c!=null)
                            {
                            emailLst.add(leadMap.get(drID).Additional_Field_Approver_1__c);
                            }
                            if(leadMap.get(drID).Additional_Field_Approver_2__c!=null){
                            emailLst.add(leadMap.get(drID).Additional_Field_Approver_2__c);
                            }
                            if(leadMap.get(drID).Field_Approver_Manager__c!=null){
                            emailLst.add(leadMap.get(drID).Field_Approver_Manager__c);
                            }
                            if(leadMap.get(drID).Field_Approver__c!=null){
                            emailLst.add(leadMap.get(drID).Field_Approver__c);
                            }
                            if(mapUserIdEmail.get(leadMap.get(drID).Partner_Manager__c)!=null){
                           
                            emailLst.add(mapUserIdEmail.get(leadMap.get(drID).Partner_Manager__c).email);
                          
                            }
                            if(mapUserIdEmail.get(leadMap.get(drID).Additional_Notification_Email_1__c)!=null){
                          
                                emailLst.add(mapUserIdEmail.get(leadMap.get(drID).Additional_Notification_Email_1__c).email);
                          
                            }
                            System.debug('emailLst---->'+emailLst);
                            singleEmail = new Messaging.SingleEmailMessage();
                            //singleEmail.setTemplateId(fieldMapConfig.DependentValues__c);
                            singleEmail.setHtmlBody(body);
                            singleEmail.setSubject(sub);
                            singleEmail.setOrgWideEmailAddressId(Label.PSC_field_approval_org_Id);
                            singleEmail.setSaveAsActivity(false);
                            //singleEmail.setWhatId(cs.Id); //Link to Case
                            //singleEmail.setTargetObjectId(cs.Id);
                            singleEmail.setToAddresses(emailLst);
                            if(leadMap.get(drID).Field_Approval_Required__c==true && leadMap.get(drID).DealReg_Deal_Registration__c==true){
                            lstEmail.add(singleEmail);
                            }
                          
                        }
                      
                    }
                }
            }
            try{
                /*if(leadToUpdate != null && leadToUpdate.size() >0){
                    update leadToUpdate;
                }*/
                if(caseToupdateLst != null && caseToupdateLst.size()>0){
                    update caseToupdateLst;
                }
              
             System.debug('lstEmail2---->'+lstEmail);
             
                List<Messaging.SendEmailResult> lstSendEmailResult = Messaging.sendEmail(lstEmail);
                
            }catch(Exception ce){
          
            System.debug('ce---->'+ce);
            }
        }
    }   
      
    }
    
    public void eduServiceEmailpopulate(List<Case> newCaseList){
    
        system.debug('Inside edu function---->'+newCaseList);
        List<Case> lstEduCases=new List<Case>();
        Map<String,Schema.RecordTypeInfo> recordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
 
        for(String str : recordTypes.keySet()){
   
            for(Case caseRecord:newCaseList)
                {
                    if(Str.equalsIgnoreCase('Education Services Record Type')){
                        if(recordTypes.get(str).getRecordTypeId()==caseRecord.RecordTypeID){
                        lstEduCases.add(caseRecord);
                   
                        }
                    }           
                }          
        }
            
        if(lstEduCases != null && lstEduCases.size() > 0){
            System.debug('lstEduCases--->'+lstEduCases);
            List<String> eduEmailList = new List<String>();
        
            for(Case eduCase : lstEduCases){
            
                if(eduCase.EMC_Account_Email_Address__c != null){
                    
                    eduEmailList.add(eduCase.EMC_Account_Email_Address__c);
                }
                
            }
            
            if(eduEmailList != null && eduEmailList.size() > 0){
                System.debug('eduEmailList--->'+eduEmailList);
                Map<Id,Education_Services_Contact__c> eduContactMap = new Map<Id,Education_Services_Contact__c>([Select Id,Name from Education_Services_Contact__c where Name IN: eduEmailList]);
                /*
                Map<String,Education_Services_Contact__c> eduContactMap = new Map<String,Education_Services_Contact__c>();
                
                List<Education_Services_Contact__c> eduContactList = [Select Id,Name from Education_Services_Contact__c where Name IN: eduEmailList];
                
                for(Education_Services_Contact__c esc : eduContactList){
                    
                    if()
                }*/
                
                List<Education_Services_Contact__c> eduContactListToInsert = new List<Education_Services_Contact__c>();
                List<Case> pendingCaseList = new List<Case>();
                if(!eduContactMap.isEmpty()){
                    for(Id eduId : eduContactMap.keySet()){
                
                        for(Case eduCase : lstEduCases){
                        
                            if(eduCase.EMC_Account_Email_Address__c == eduContactMap.get(eduId).Name){
                            System.debug('eduCase.EMC_Account_Email_Address__c'+eduCase.EMC_Account_Email_Address__c);
                            eduCase.EDS_Contact_Email__c = eduId;
                            }
                            /* else{
                            pendingCaseList.add(eduCase);
                            eduContactListToInsert.add(new Education_Services_Contact__c(Name = eduCase.EMC_Account_Email_Address__c));
                            }*/
                        }               
                    
                    }
                }
                else{
                        for(Case eduCase : lstEduCases){
                            pendingCaseList.add(eduCase);
                            eduContactListToInsert.add(new Education_Services_Contact__c(Name = eduCase.EMC_Account_Email_Address__c));
                            
                        }
                    
                    
                }
                System.debug('eduContactListToInsert--->'+eduContactListToInsert);
                insert eduContactListToInsert;
                Map<String,Education_Services_Contact__c> neweduContactMap = new Map<String,Education_Services_Contact__c>();
                for(Education_Services_Contact__c esc : eduContactListToInsert){
                    
                    neweduContactMap.put(esc.Name, esc);
                }
                
                for(Case cs : pendingCaseList){
                    
                    cs.EDS_Contact_Email__c = neweduContactMap.get(cs.EMC_Account_Email_Address__c).Id;
                }
            }   
            
        }       
            
        }

}