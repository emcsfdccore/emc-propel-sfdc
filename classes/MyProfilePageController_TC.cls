/*=====================================================================================================+
|  HISTORY  
|
|  DATE             DEVELOPER        WR             DESCRIPTION    
|  ====             =========        ==             ===========  
| 18/Feb/2014       Aagesh Jose     BackArrow      Created test class to move test methods from base class          
+=====================================================================================================*/
@isTest
private class MyProfilePageController_TC{
    
    static testMethod void testSave() {         
        // Modify the test to query for a portal user that exists in your org
        List<User> existingPortalUsers = [SELECT id, profileId, userRoleId FROM User WHERE UserRoleId <> null AND UserType='CustomerSuccess'];

        if (existingPortalUsers.isEmpty()) {
            User currentUser = [select id, title, firstname, lastname, email, phone, mobilephone, fax, street, city, state, postalcode, country
                                FROM User WHERE id =: UserInfo.getUserId()];
            MyProfilePageController controller = new MyProfilePageController();
            System.assertEquals(currentUser.Id, controller.getUser().Id, 'Did not successfully load the current user');
            //System.assert(controller.isEdit == false, 'isEdit should default to false');
            controller.edit();
            //System.assert(controller.isEdit == true);
            controller.cancel();
           //System.assert(controller.isEdit == false);
        
            Contact c = new Contact();
            c.LastName = 'TestContact';
            insert c;
            
            //MyProfilePageController.setContactFields(c, currentUser);

//Avinash'c Code starts...
            User u1 = controller.getUser();
            u1.Contact = c;
//Avinash'c Code ends.

            controller.save();
            System.assert(Page.ChangePassword.getUrl().equals(controller.changePassword().getUrl()));
            
//Avinash'c Code starts...
            Boolean bln = controller.getIsEdit();
//Avinash'c Code ends.
        } 
        else 
        {
            User existingPortalUser = existingPortalUsers[0];
            String randFax = Math.rint(Math.random() * 1000) + '5551234';
            
            System.runAs(existingPortalUser) 
            {
                MyProfilePageController controller = new MyProfilePageController();
                System.assertEquals(existingPortalUser.Id, controller.getUser().Id, 'Did not successfully load the current user');
                //System.assert(controller.isEdit == false, 'isEdit should default to false');
                controller.edit();
                //System.assert(controller.isEdit == true);
                
                controller.cancel();
                //System.assert(controller.isEdit == false);
                
                controller.getUser().Fax = randFax;
                controller.save();
                //System.assert(controller.isEdit == false);
                
            }
                
            // verify that the user and contact were updated
            existingPortalUser = [Select id, fax, Contact.Fax from User where id =: existingPortalUser.Id];
            System.assert(existingPortalUser.fax == randFax);
            System.assert(existingPortalUser.Contact.fax == randFax);
        }
  
        
    }
    
    static testMethod void ReportFinderUtil_TC()
    {
        String strid;
        List<Report> rp = [select name from Report limit 1];
        ReportFinderUtil rpu = new ReportFinderUtil();
        if(rp != null && rp.size() >0)
            strid = rpu.findReportId(rp[0].name);
        strid = rpu.findReportId('reportName');
    }
}