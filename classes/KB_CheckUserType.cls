global class KB_CheckUserType {



    webService static String chkUserType(String inputString) {

        System.debug('inputString------->'+inputString);

        String licenseType  = '';

        try{
        User usrObj = [Select Profile.UserLicense.Name,Id,FederationIdentifier  from user where FederationIdentifier =:inputString];
        licenseType = usrObj.Profile.UserLicense.Name;
        }
        catch (Exception ex){
            return null;

        }

        if(licenseType=='Overage High Volume Customer Portal'){

            return 'Portal User';


        }
        else {

            return 'Platform User';

        }



        return null;
       
    }
}