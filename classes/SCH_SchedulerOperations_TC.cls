@isTest
private class SCH_SchedulerOperations_TC {
/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE       DEVELOPER       WR       DESCRIPTION                               
 |  ====       =========       ==       =========== 
 |  18-JUN-09  S Douglas                Created
 |  28-Nov-13  Jaypal Nimesh  Backward  Modified class to optimize the time and
 |                               Arrow   Improve the code coverage.
 | 6-jan-2015  Alok Kumar               modified to fix test class failure
 +===========================================================================*/

  public static testMethod void testScheduler()
  {
        DateTime testDt;
        String nullString = null;
        System.Debug('Test calcNewRunDate******************************************************************************');
        testDt = SCH_SchedulerOperations.calcNewRunDate(DateTime.newInstance(2000,1,1,0,1,0), 'Daily');
        System.assert(testDt == DateTime.newInstance(2000,1,2,0,1,0), 'Incorrect new run date calculated for Weekly recurrence');
       
        //my code         
        testDt = SCH_SchedulerOperations.calcNewRunDate(DateTime.newInstance(2000,1,1,0,1,0), 'weekly');
        testDt = SCH_SchedulerOperations.calcNewRunDate(DateTime.newInstance(2000,1,1,0,1,0), 'Fortnightly');
        testDt = SCH_SchedulerOperations.calcNewRunDate(DateTime.newInstance(2000,1,1,0,1,0), 'Monthly');
        testDt = SCH_SchedulerOperations.calcNewRunDate(DateTime.newInstance(2000,1,1,0,1,0), 'Quarterly');
        testDt = SCH_SchedulerOperations.calcNewRunDate(DateTime.newInstance(2000,1,1,0,1,0), 'yearly');
        // my code

        Job_Scheduler__c sched = new Job_Scheduler__c();
        sched.Name = 'UnitTest_AccountTeamMemberBatchDelete';
        sched.Start_Date__c = Date.newInstance(2000,1,1);  //January 1, 2000
        sched.Operations__c = 'Batch Delete Account Team Members';  //'Test1';
        sched.Occurence__c = 'Daily';
        sched.End_Date__c = null;
        sched.Schedule_Hour__c = 0;
        sched.Minutes__c = 1;
        

        //Bulk cases
        List <Job_Scheduler__c> bulkScheds = new List <Job_Scheduler__c> ();
        bulkScheds.add(new Job_Scheduler__c(Name = 'UnitTest_EndDateBeforeToday', Start_Date__c = Date.newInstance(2000,1,1),
                                        Operations__c = 'Test', Occurence__c = 'Daily',
                                        End_Date__c = Date.newInstance(2000,1,5), Schedule_Hour__c = 0, Minutes__c = 1));

        
        //Test insert which should populate Next Schedule and also set update flag to 0
        insert sched;  
        insert bulkScheds;
        
        Test.startTest();
        
        Job_Scheduler__c insertedSched =  [select id, Next_Schedule__c, Update_Me__c from Job_Scheduler__c where Id = :sched.Id limit 1];       

        System.assertEquals('0', insertedSched.Update_Me__c, 'Incorrect Update Flag Value for Insert');

        //Test the after update trigger which should create a new schedule record with Next Schedule incremented
        sched.Update_Me__c = '1';
        
        update sched;

        //Check bulk case with end date before today
        insertedSched =  [select id, Next_Schedule__c, Update_Me__c from Job_Scheduler__c where Id = :bulkScheds[0].Id];

        System.assert(insertedSched.Next_Schedule__c == DateTime.newInstance(2000,1,1,0,1,0), 'Incorrect Next Schedule Value for Bulk Insert EndDateBeforeToday');
        System.assertEquals('1', insertedSched.Update_Me__c, 'Incorrect Flag Value for Bulk Insert EndDateBeforeToday');

        SCH_SchedulerOperations.resetFlagsForUnitTest();

        Test.stopTest();
        
  } //testScheduler
  
  public static testMethod void testSchedulerTASync()
  {
    
    system.runAs(new User(Id = UserInfo.getUserId())){
        
        CustomSettingDataHelper.dataValueMapCSData();
    }
    
    Job_Scheduler__c sched3 = new Job_Scheduler__c();
    sched3.Name = 'Inbound New Schedules';
    sched3.Operations__c = 'Update TA Sync Record';
    sched3.Start_Date__c = Date.newInstance(2013,1,1);
    sched3.Account_Locking__c=true;
    sched3.End_Date__c = null;
    sched3.status__c= 'Pending';
    sched3.Schedule_Hour__c =0;
    sched3.Minutes__c = 1;
    
    insert sched3;
    
    Test.StartTest();
    
    SCH_SchedulerOperations.executeBatch(sched3);
    
    Test.Stoptest();
  }
  
  public static testMethod void testSchedulerAP()
  {
    system.runAs(new User(Id = UserInfo.getUserId())){
        
        CustomSettingDataHelper.dataValueMapCSData();
    }
    Job_Scheduler__c sched2 = new Job_Scheduler__c();
    sched2.Name = 'Core Quota Rep 1';
    sched2.Start_Date__c = Date.newInstance(2000,1,1);  //January 1, 2000
    sched2.Operations__c = 'Core Quota Rep';  //'Test1';
    sched2.Occurence__c = 'Daily';
    sched2.End_Date__c = null;
    sched2.Schedule_Hour__c = 0;
    sched2.Minutes__c = 1;
    
    insert sched2;
    
    Test.StartTest();
    
    SCH_SchedulerOperations.executeBatch(sched2);
    
    Test.Stoptest();
  }
  
  public static testMethod void testSchedulerR2R()
  {
    Job_Scheduler__c sched2 = new Job_Scheduler__c();
    sched2.Name = 'Core Quota Rep 1';
    sched2.Start_Date__c = Date.newInstance(2000,1,1);  //January 1, 2000
    sched2.Operations__c = 'R2R Sales Strategy - Calculation';  //'Test1';
    sched2.Occurence__c = 'Daily';
    sched2.End_Date__c = null;
    sched2.Schedule_Hour__c = 0;
    sched2.Minutes__c = 1;
    
    insert sched2;
    
    Test.StartTest();
    
    SCH_SchedulerOperations.executeBatch(sched2);
    
    Test.Stoptest();
  }
}