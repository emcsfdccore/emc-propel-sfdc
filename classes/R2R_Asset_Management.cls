/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  28/03/2013      Anirudh Singh               This Class will be used for all the Operations on Asset Related to Asset
 |                                              Management
 |  19/11/2013      Jaspreet Singh              Added method updateGDWField 
 |  29/01/2014      Sneha Jain      FR 9579     Handling new EMC-DeInstall records from SAM Db
 +=========================================================================================================================*/

public class R2R_Asset_Management{
  /* @Method <This method is used to feed chatter post on Asset.>
    @param <It is taking List<Asset__c> as Parameter from AssetAfterUpdateTrigger>
    @returntype - <void> >
    @throws exception - <No Exception>
    */  
  public void postChatterFeed(List<Asset__c> lstAssetForMES){
 
    List<FeedItem> lstFeedItemToPost = new List<FeedItem>();
    set<Id> setAssetForOpptyPost = new set<Id>();
    list<Opportunity_Asset_Junction__c> lstAssetJucntion = new list<Opportunity_Asset_Junction__c>();    
    MaP<id,string> mapAssetWithNotification = new map<id,string>();
    Map<id,Asset__C> mapassetdetails =new Map<id,Asset__C>([Select Deffered_to_Renewals__c,Chatter_Notification_Indicator__c,
                                                            Maintenance_Engagement_Status1__c,RecordType.DeveloperName from Asset__c where Id in :lstAssetForMES and RecordType.DeveloperName='EMC_Install']);
    if(!mapassetdetails.isempty()){ 
        for(Asset__c assetObj :lstAssetForMES){
            if(assetObj.Deffered_to_Renewals__c || assetObj.Chatter_Notification_Indicator__c.contains('Post notification on Asset')
               || assetObj.Chatter_Notification_Indicator__c.contains('both')){
                FeedItem chatterpost = new FeedItem();                        
                chatterpost.ParentId = assetObj.id;
                chatterpost.Body = 'Maintenance Engagement Status Status has been changed to '+  '\n \n'+assetobj.Maintenance_Engagement_Status1__c; 
                lstFeedItemToPost.add(chatterpost);
            }
            if(assetObj.Chatter_Notification_Indicator__c != null && assetObj.Chatter_Notification_Indicator__c.contains('Opportunity')){
                mapAssetWithNotification.put(assetObj.id,assetObj.Chatter_Notification_Indicator__c);
                setAssetForOpptyPost.add(assetObj.Id);
            }
        }
    }   
    if(!setAssetForOpptyPost.isempty()){        
        lstAssetJucntion = [select id,Related_Opportunity__c,Related_Opportunity__r.StageName ,Related_Asset__c from Opportunity_Asset_Junction__c where Related_Asset__c in :setAssetForOpptyPost and Related_Opportunity__r.StageName in ('Pipeline','Upside','Strong Upside','Commit') ];
    }
    if(!lstAssetJucntion.isempty()){
        for(Opportunity_Asset_Junction__c assetJunctionObj :lstAssetJucntion){
            if(mapAssetWithNotification.containskey(assetJunctionObj.Related_Asset__c) 
            && (mapAssetWithNotification.get(assetJunctionObj.Related_Asset__c).contains('150')
            || mapAssetWithNotification.get(assetJunctionObj.Related_Asset__c).contains('120') ||
            mapAssetWithNotification.get(assetJunctionObj.Related_Asset__c).contains('90')) && 
            (assetJunctionObj.Related_Opportunity__r.StageName == 'Pipeline' || assetJunctionObj.Related_Opportunity__r.StageName == 'Upside')
            ){
                FeedItem chatterpost = new FeedItem();  
                chatterpost.ParentId = assetJunctionObj.Related_Opportunity__c;
                chatterpost.Body = 'Maintenance Engagement Status of the related asset is more than 90. '+  '\n \n'; 
                lstFeedItemToPost.add(chatterpost);
            } 
            else{
                FeedItem chatterpost = new FeedItem();  
                chatterpost.ParentId = assetJunctionObj.Related_Opportunity__c;
                chatterpost.Body = 'Maintenance Engagement Status of the related asset is more than 90. '+  '\n \n'; 
                lstFeedItemToPost.add(chatterpost);
            }       
        }
    }
    
    if(!lstFeedItemToPost.isempty()){
       insert  lstFeedItemToPost;
    }   
    
  }
/* @Method <This method is used to feed chatter post on Asset.>
    @param <It is taking List<Asset__c> as Parameter from AssetBeforeUpdateTrigger>
    @returntype - <void> >
    @throws exception - <No Exception>
*/  
  public void setRecordTypeOnAsset(List<Asset__c> lstAsset){
       map<string,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getall();
       string ActionableStatus = DataValueMap.get('R2R Actionable Status').datavalue__c;
       for(asset__c assetObj :lstAsset){
           if(ActionableStatus.contains(assetObj.Install_Base_Status__c)){
              assetObj.RecordTypeId=DataValueMap.get('EMC Install Record Type').Datavalue__c;
           }
           else{
             assetObj.RecordTypeId=DataValueMap.get('EMC Install - NonActionable').Datavalue__c;   
           }
       
       }

  }   
  
  /* @Method <This method is used to update GDW field on Asset.>
    @param <It is taking List<Asset__c> old list and new list as Parameter from AssetBeforeUpdateTrigger>
    @returntype - <void> >
    @throws exception - <No Exception>
  */ 
  
  public void updateGDWField(List<Asset__c> listNewAssetToProcessGDW, List<Asset__c> listOldAssetToProcessGDW){
    if(listNewAssetToProcessGDW.size()>0 && listOldAssetToProcessGDW.size()>0){
        for(Integer i=0; i<listNewAssetToProcessGDW.size(); i++){
            if(listNewAssetToProcessGDW[i].Asset_Depreciation_Term__c != listOldAssetToProcessGDW[i].Asset_Depreciation_Term__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Serial_Number__c != listOldAssetToProcessGDW[i].Serial_Number__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }
            else if(listNewAssetToProcessGDW[i].Comments__c != listOldAssetToProcessGDW[i].Comments__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }
            else if(listNewAssetToProcessGDW[i].Name != listOldAssetToProcessGDW[i].Name){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Deffered_to_Renewals__c != listOldAssetToProcessGDW[i].Deffered_to_Renewals__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].IB_Instance_Number__c != listOldAssetToProcessGDW[i].IB_Instance_Number__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Maintenance_Engagement_Status1__c != listOldAssetToProcessGDW[i].Maintenance_Engagement_Status1__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Monthly_Depreciation__c != listOldAssetToProcessGDW[i].Monthly_Depreciation__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Opportunity_Number_Refresh__c != listOldAssetToProcessGDW[i].Opportunity_Number_Refresh__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Opportunity_Number_Renew__c != listOldAssetToProcessGDW[i].Opportunity_Number_Renew__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Hardware_Post_Warranty_Estimate_Annual_N__c != listOldAssetToProcessGDW[i].Hardware_Post_Warranty_Estimate_Annual_N__c ){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].RecordType != listOldAssetToProcessGDW[i].RecordType){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Refresh_Swap_Deferal_Date__c != listOldAssetToProcessGDW[i].Refresh_Swap_Deferal_Date__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Sales_Plan__c != listOldAssetToProcessGDW[i].Sales_Plan__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Swap_Trade_In_Cost_Relief_Value__c != listOldAssetToProcessGDW[i].Swap_Trade_In_Cost_Relief_Value__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Application__c != listOldAssetToProcessGDW[i].Application__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Software_Post_Warranty_Estimate_Annual__c != listOldAssetToProcessGDW[i].Software_Post_Warranty_Estimate_Annual__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Total_Post_Warranty_Maintenance_Annual__c != listOldAssetToProcessGDW[i].Total_Post_Warranty_Maintenance_Annual__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Core_Quota_Rep_from_Account__c != listOldAssetToProcessGDW[i].Core_Quota_Rep_from_Account__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Party_Number__c != listOldAssetToProcessGDW[i].Party_Number__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            }else if(listNewAssetToProcessGDW[i].Product_Name_Vendor__c != listOldAssetToProcessGDW[i].Product_Name_Vendor__c){
                listNewAssetToProcessGDW[i].GDW_Trigger_Date__c = System.now();
                break;
            } 
        }
    }
  }
  
    //Code added for newly inserted records from SAM Db with Record type as EMC Install but IB Status as 'Deinstall - Cust Site'
    //The code should flip the Record type to EMC De-Install
    //Handling new EMC-DeInstall records from SAM Db - FR 9579 : 
    public void flipAssetRT( List<Asset__c> assetList, String recordType)
    {
        for(Asset__c assetRT : assetList)
        {
            assetRT.RecordTypeId = recordType;
        }
    }
}