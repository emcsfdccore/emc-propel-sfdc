/**

 This Class determines the notification Strategy for each case updated. 
 Using appropriate strategy objects to determine appropriate sender, recipient and template.
 Trigger Presales_SlaTracking calls this class
 
 Step 1: Find cases only whose status has been changed
 Step 2: Find all the registry records
 Step 3: build Notifications for each registry record and send email

 Find users from contact emails, if user found send email to user, otherwise send email to Contact and if contact not found 
 send email to email address.
========================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE           DEVELOPER                WR                DESCRIPTION                               
 |  ====           =========                ==                =========== 
 |  10/10/2014     Arvind & Prabhat
 |  13/01/2014     Vivek Barange            1548              If the case is a child case (i.e. the parent filed is not empty) AND the "Also for Child Case" is FALSE then don't send email notficiation

 +======================================================================================================================================
 **/

public without sharing class NotifyHelper
{
    public static Boolean blnIsAutoNotificationCalled = false;

    private Map<Id,Case> mapNewCases;
    private Map<Id, Case> oldMap;
    private Messaging.SingleEmailMessage[] notifications = new Messaging.SingleEmailMessage[0];
    public list<Case> finalCases=new list<Case>();
    public list<String> finalCasetype=new list<String>();
    public list<String> finalCaseSubtype=new list<String>();
    public list<String> finalStatus=new list<String>();
    // private List<Notification_Strategy__c> regListBlank= new List<Notification_Strategy__c>();
    // private List<Notification_Strategy__c> regList=new List<Notification_Strategy__c>();
    public List<Notification_Strategy__c>  registyCaseList=new List<Notification_Strategy__c>();
    private Messaging.SingleEmailMessage singleEmail = new Messaging.SingleEmailMessage();
    public Map<String,List<Notification_Strategy__c>>  registryMap= new Map<String,List<Notification_Strategy__c>>();
    public List<Notification_Strategy__c > registry=new List<Notification_Strategy__c >();
    
    public List<CaseTeamMember> lstTeamMembers = new List<CaseTeamMember>();
    public Boolean blnIsCaseTeam = false;
    public Boolean blnIsEmail = false;
    public Boolean blnIsContact = false;
    public Set<String> setQueryEmail = new Set<String>();
    public Set<String> setUserEmail = new Set<String>();
    
    public Set<String> setNonUserContactEmail = new Set<String>();
    public Set<Id> setNonUserContactId = new Set<Id>();
    public Set<String> setNonUserNonContactEmail = new Set<String>();
    public Map<id,Set<String>> mapSetNonUserNonContactEmail = new Map<id,Set<String>>();

    public Set<String> setCaseTeamUserEmail = new Set<String>();
    public Set<Id> setCaseTeamMemberUserIds = new Set<Id>();
    public Set<String> setCaseId = new Set<String>();
    public Map<Id,Contact> mapIdContact;
    public Set<ID> setFinalId = new Set<Id>();
    public Map<id,set<id>> MapsetNonUserContactId = new Map<id,set<id>>();
    //public Map<id,set<id>> mapSetFinalId=new Map<id,set<id>>();
    public List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();
    public List<User> lstUser = new List<User>();
    public Map<Id, List<CaseTeamMember>> mapCaseIdTeamMember = new Map<Id,List<CaseTeamMember>>();
    public Map<String, Set<String>> mapKeyStringSetEmail = new Map<String, Set<String>>();
    public Map<String, Id> mapEmailUserId = new Map<String, Id>();
    
    public Map<Id,String> mapCaseIdContactEmail = new Map<Id,String>();
    public Map<String, Id> mapContactEmailCaseId = new Map<String, Id>();
    
    public Map<String, User> mapContactEmailUser = new Map<String,User>();
    public Set<String> setFinalEmailIds = new Set<String>();
    public Map<Id, User> mapIDUser = new Map<Id,User>();

    public Set<String> setSpecialRecordTypes = new Set<String>();
    public Set<String> setPresalesRecordTypes = new Set<String>();
    public Set<String> setPresalesClosedStatus = new Set<String>();
    public Set<String> setSiteUserId = new Set<String>();

    public Map<Id, Set<String>> mapCaseIdRecipientDetail = new Map<Id, Set<String>> ();

    ///// This map stores the Case Team member emails ids (as set) for each case (as key)
    public Map<Id,Set<String>> mapCaseIdSetCTMEmails = new Map<Id,Set<String>>();
    /////

    
    /**
    Constructor that calls the methods and collects all the notifications for all the cases being updated and their corresponding Registry records
    **/
    private NotifyHelper(Map<Id,Case> mapNewCases, Map<Id, Case> oldMap, Boolean blnIsInsert) 
    {
        // system.debug('#### Inside Constructor - collectNotifications Start');
        
        
        Map<String,AutoNoti_Special_RecordTypes__c> mapSpcRecType=AutoNoti_Special_RecordTypes__c.getall();
        
        if(mapSpcRecType != null && mapSpcRecType.values() != null)
        {
            for(AutoNoti_Special_RecordTypes__c rec: mapSpcRecType.values())
            {
                setSpecialRecordTypes.add(rec.Case_Record_Type_Name__c);
            }
        }
        
        
        Map<String,AutoNoti_PresalesRecordTypes__c> mapPreSalesRecType=AutoNoti_PresalesRecordTypes__c.getall();
        if(mapPreSalesRecType != null && mapPreSalesRecType.values() != null)
        {
            for(AutoNoti_PresalesRecordTypes__c rec: mapPreSalesRecType.values())
            {
                setPresalesRecordTypes.add(rec.Case_Record_Type_Name__c);
            }
        }
        
        
        Map<String,AutoNoti_PresalesClosedStatus__c> mapCloseStatuses=AutoNoti_PresalesClosedStatus__c.getall();
        if(mapCloseStatuses != null && mapCloseStatuses.values() != null)
        {
            for(AutoNoti_PresalesClosedStatus__c rec: mapCloseStatuses.values())
            {
                setPresalesClosedStatus.add(rec.Closed_Status__c);
            }
        }
        
        // System.debug('setSpecialRecordTypes: '+setSpecialRecordTypes);
        // System.debug('setPresalesRecordTypes: '+setPresalesRecordTypes);
        // System.debug('setPresalesClosedStatus: '+setPresalesClosedStatus);

        setSiteUserId.add('00570000002fVW8');
        setSiteUserId.add('00570000002fVWD');
        setSiteUserId.add('00570000001faMf');

        this.mapNewCases = mapNewCases;
        this.oldMap = oldMap;       
        
        findStatusChangedCases(blnIsInsert);   //Step 1: Find cases only whose status has been changed
        calculateRegistry();        //Step 2: Find all the registry records
        collectNotifications();     //Step 3: build Notifications for each registry record and link to the Cases
    }
    
    /**
    //Step 1: Find cases only whose status has been changed AND collect all the contact emails associated with those cases  
    **/
    private void findStatusChangedCases(Boolean blnIsInsert) 
    {   
        for (Case c: mapNewCases.values()) 
        {
            // only for changes in status!
            // system.debug('#### Inside findStatusChangedCases');

            Case oldCase;

            if(!blnIsInsert)
                oldCase = oldMap.get(c.Id);
            
            // system.debug('#### blnIsInsert :: '+blnIsInsert);
            // system.debug('#### Trigger.isUpdate :: '+Trigger.isUpdate);
            // system.debug('#### Trigger.isAfter :: '+Trigger.isAfter);
            if(oldCase != null)
                // system.debug('#### Old Owner Id  :: '+(string)oldCase.OwnerId);

            // system.debug('#### (blnIsInsert || c.Status != oldCase.Status) :: '+(blnIsInsert || c.Status != oldCase.Status));
            // system.debug('#### c.Origin :: '+c.Origin);
            // system.debug('#### c.Status :: '+c.Status);
            // system.debug('#### strOwnerId :: '+(string)c.OwnerId);

            if ((blnIsInsert || c.Status != oldCase.Status || ((string)c.OwnerId != (string)oldCase.OwnerId && c.Status == 'Open')) && c.Origin != 'Oracle Integration' && c.Origin != 'Bulk Upload' && c.Auth_Reseller_Onboarding__c==null) 
            {
                String strOwnerId = (string)c.OwnerId;
                
                // system.debug('#### INSIDE findStatusChangedCases FIRST IF stmt');
                // system.debug('#### setSiteUserId :: '+setSiteUserId);

                if(!(c.Status == 'Open' && !strOwnerId.startsWith('00G')) || 
                    (c.Status == 'Open' && setSiteUserId.contains(strOwnerId.subString(0,15))))
                {
                    // system.debug('#### Inside IF OF Constructor 1 st class');
                    finalCases.add(c);
                    setCaseId.add(c.id);
                    finalCasetype.add(c.Record_Type_Name__c);
                    finalCaseSubtype.add(c.Type);
                    finalStatus.add(c.status);
                    
                    // system.debug('#### casse ID :: '+c.id);
                    // system.debug('#### Contact :: '+c.ContactId);
                    // system.debug('#### finalCasetype :: '+finalCasetype);
                    // system.debug('#### finalCaseSubtype :: '+finalCaseSubtype);                  
                    // system.debug('#### finalStatus :: '+finalStatus); 

                    if(c.Contact_Email1__c != null)
                    {
                        mapCaseIdContactEmail.put(c.id,c.Contact_Email1__c);
                        mapContactEmailCaseId.put(c.Contact_Email1__c, c.id);
                    }    
                    else if (c.SuppliedEmail != null && (c.Origin.equalsIgnoreCase('Email') || c.Origin.equalsIgnoreCase('Web')))
                    {
                        mapCaseIdContactEmail.put(c.id,c.SuppliedEmail);
                        mapContactEmailCaseId.put(c.SuppliedEmail, c.id);

                    }

                }               
             
            }   
        }
    }  
     
    /**
    Step 2: Find all the registry records for all the cases found in findStatusChangedCases() method above.
    Also it does (a) Query all the Case Team Members associated with the cases and referred in the registry records (b) Query all the users for Registry records associated with Contact/Case Team
    **/
    private void calculateRegistry() 
    {   
            
        registry=[SELECT Case_Subtype__c, Case_Type__c, CreatedById, CreatedDate, CurrencyIsoCode, IsDeleted, From__c, Name, IsActive__c, 
                                  LastModifiedById, LastModifiedDate, OwnerId, ConnectionReceivedId, Receiver__c, Id,ConnectionSentId, 
                                  Template_ID__c,From_ID__C,Status__c, Also_for_Child_Case__c,SystemModstamp, Template_Name__c, Recipient_Type__c 
                                  FROM Notification_Strategy__c  
                                  WHERE isActive__c=true and Case_Type__c=:finalCasetype and status__c=:finalStatus
                                  ORDER BY Receiver__c ASC];

        //Create a strategy registry map, to be used in linking case to the registry
        registryMap= new Map<String,List<Notification_Strategy__c>>();

        for (Notification_Strategy__c reg: registry)
        {
            if(registryMap.get(reg.Case_type__c+reg.Case_SubType__c+reg.status__c) == null)
                registryMap.put(reg.Case_type__c+reg.Case_SubType__c+reg.status__c, new List<Notification_Strategy__c>());
            registryMap.get(reg.Case_type__c+reg.Case_SubType__c+reg.status__c).add(reg);

            // system.debug('#### Reg :: '+reg);

            if(reg.Recipient_Type__c.equalsIgnoreCase('CaseTeam'))
                blnIsCaseTeam = true;

            if (reg.Recipient_Type__c.equalsIgnoreCase('Object') && reg.Receiver__c.equalsIgnoreCase('Contact'))
            {
                blnIsContact = true;
            }        
        }

        // system.debug('#### registryMap :: '+registryMap);
        // system.debug('#### blnIsEmail :: '+blnIsEmail);
        // system.debug('#### blnIsCaseTeam :: '+blnIsCaseTeam);

        // If caseteam is mentioned in any registry record for this case, find all case teammembers - start
        if(blnIsCaseTeam)
        {
            lstTeamMembers = [SELECT ParentId, MemberId, Member.Email, TeamRoleId, TeamRole.Name
                                FROM CaseTeamMember 
                                WHERE ParentId IN :setCaseId and TeamRole.Name IN ('Observer','Primary') 
                                limit 5000];
            
            
            // system.debug('#### lstTeamMembers :: '+lstTeamMembers);
            setCaseTeamMemberUserIds.clear();
            for (CaseTeamMember ctm: lstTeamMembers) 
            {
                // setCaseTeamUserEmail.add(ctm.Member.Email);
                setCaseTeamMemberUserIds.add(ctm.MemberId);
                if(mapCaseIdTeamMember.get(ctm.ParentId) == null)
                    mapCaseIdTeamMember.put(ctm.ParentId, new List<CaseTeamMember>());
                mapCaseIdTeamMember.get(ctm.ParentId).add(ctm);

                

            }
            // system.debug('#### setCaseTeamMemberUserIds in CaseTeam Type :: '+setCaseTeamMemberUserIds);
            
            blnIsCaseTeam = false;
        }

        // Find all users based on contact emails and the Case Team Member Ids
        if((blnIsContact && mapCaseIdContactEmail.values() != null) || (setCaseTeamMemberUserIds.size() > 0))
        {
            mapIDUser = new Map<Id,User>([select Id, Name, Email, PreSales_Case_Assigned__c, PreSales_Case_Close_AutoReply__c, PreSales_Case_Closed__c, PreSales_Case_Created__c, PreSales_Case_Escalated__c, PreSales_Case_Pending_Engg__c, PreSales_Case_Pending_Internal__c, PreSales_Case_Resolved__c 
                                    from User 
                                    where isActive = true AND (email IN :mapCaseIdContactEmail.values() OR id IN :setCaseTeamMemberUserIds) 
                                    limit 10000]);
        }

        // system.debug('#### mapIDUser :: '+mapIDUser);

        // Find out which emails have users  linked  -start
        if(blnIsContact)
        {
            Set<String> setContactEmail = new Set<String>(mapCaseIdContactEmail.values());

            for (User usr : mapIDUser.values()) 
            {
                if(setContactEmail.contains(usr.email))
                {
                    //if multiple user found for a single email address it will override to send email only once.
                    mapContactEmailUser.put(usr.email, usr); 
                    
                    setContactEmail.remove(usr.email);
                }
            }

            //if there are some emails without any user, find the "contact" of that case
            if (setContactEmail != null && setContactEmail.size() > 0) 
            {
                setNonUserContactEmail.addAll(setContactEmail);
                id lContactId;
                Set<id> setnonUserCntId;
                Set<String> setnonUserNonCntEmail;
                for (String strEmail : setNonUserContactEmail) 
                 {  
                 
                    lContactId=mapNewCases.get(mapContactEmailCaseId.get(strEmail)).ContactId;                    
                    if( lContactId != null)
                    {
                        //setNonUserContactId.add(mapNewCases.get(mapContactEmailCaseId.get(strEmail)).ContactId);
                        setnonUserCntId=MapsetNonUserContactId.get(mapContactEmailCaseId.get(strEmail));
                        
                        if (setnonUserCntId == null)
                               setnonUserCntId = new Set<id>();                           
                              
                        setnonUserCntId.add(lContactId);
                        MapsetNonUserContactId.put(mapContactEmailCaseId.get(strEmail),setnonUserCntId);
                    }
                    else
                        //setNonUserNonContactEmail.add(strEmail);
                    {
                        setnonUserNonCntEmail=mapSetNonUserNonContactEmail.get(mapContactEmailCaseId.get(strEmail));
                        
                        if (setnonUserNonCntEmail== null)
                               setnonUserNonCntEmail= new Set<String>();                           
                              
                        setnonUserNonCntEmail.add(strEmail);
                        mapSetNonUserNonContactEmail.put(mapContactEmailCaseId.get(strEmail),setnonUserNonCntEmail);
                    }
                 }
            }

            

            blnIsContact = false;
        }

    }

    /**
    Step 3: Link each Case to Registry Data:
    (a)It iterates through each of the case and for each of its associated registry records, calls getEmailMessage() method to construct a list of notifications based on the details mentioned in each registry records 
    (b) It also constructs a notification for Additional Notification Email fields on Case (if populated)
    **/ 
    private void collectNotifications() 
    {     
        for (Case c: finalCases) 
        {
            
            setFinalId.clear();
            lstEmail.clear();
            setFinalEmailIds.clear();

            // system.debug('#### setSpecialRecordTypes :: '+setSpecialRecordTypes);

            // system.debug('#### c.Record_Type_Name__c :: '+c.Record_Type_Name__c);
            // system.debug('#### Registry Key from Case :: '+c.Record_Type_Name__c+c.type+c.status);

            // system.debug('#### registryMap :: '+registryMap);

            //if registry doesn't have caseSubtype, fetch values based on casetype and Status only
            if(setSpecialRecordTypes.contains(c.Record_Type_Name__c))
                registyCaseList = registryMap.get(c.Record_Type_Name__c+'null'+c.status);

            else
                registyCaseList = registryMap.get(c.Record_Type_Name__c+c.type+c.status);
            
            // system.debug('#### registyCaseList :: '+registyCaseList);  
             
            if (registyCaseList != null && registyCaseList.size()>0)
            {   
                //Start - 1548
                boolean notify = false;
                //End - 1548
                for(Notification_Strategy__c reg: registyCaseList)
                {
                    // system.debug('#### REG in FOR Loop :: '+reg);                        
                    //notifications.addAll(this.getEmailMessage(reg ,c));
                    //Start - 1548
                    if(c.Parentid != null && !reg.Also_for_Child_Case__c) {
                        notify = true;

                        break;
                    }
                    //End - 1548
                    this.getEmailMessage(reg ,c);
                }
                //Start - 1548
                if(notify) continue;
                //End - 1548
                List<String> lstAdditionalEmail = new List<String>();

                /*Messaging.SingleEmailMessage additionalEmail = new Messaging.SingleEmailMessage(); //create a new SingleEmailMessage
            
                additionalEmail.setTemplateId(registyCaseList.get(0).Template_ID__c);
                additionalEmail.setOrgWideEmailAddressId(registyCaseList.get(0).From_ID__c.subString(0,15));
                additionalEmail.setSaveAsActivity(false);
                additionalEmail.setWhatId(c.id);
                additionalEmail.setTargetObjectId(System.Label.Dummy_Contact_Id);*/

                if (c.Additonal_Notification_Email_1__c != null) lstAdditionalEmail.add(c.Additonal_Notification_Email_1__c.trim());
                if (c.Additonal_Notification_Email_2__c != null) lstAdditionalEmail.add(c.Additonal_Notification_Email_2__c.trim());
                if (c.Additonal_Notification_Email_3__c != null) lstAdditionalEmail.add(c.Additonal_Notification_Email_3__c.trim());
                if (c.Additonal_Notification_Email_4__c != null) lstAdditionalEmail.add(c.Additonal_Notification_Email_4__c.trim());
                if (c.Additonal_Notification_Email_5__c != null) lstAdditionalEmail.add(c.Additonal_Notification_Email_5__c.trim());
 
                //Include Non user Non Contact case creator email in this too
                setNonUserNonContactEmail = mapSetNonUserNonContactEmail.get(c.id);
                if (setNonUserNonContactEmail != null && setNonUserNonContactEmail.size() > 0) 
                {
                    for(String strEmail: setNonUserNonContactEmail)
                        if(!addedToRecipients(c,strEmail))
                            lstAdditionalEmail.add(strEmail);
                }

                /*//Add Additional emails outgoing emails
                {
                    additionalEmail.setToAddresses(lstAdditionalEmail);
                    notifications.add(additionalEmail);
                }*/
            setFinalEmailIds.addAll(lstAdditionalEmail);


            notifications.addAll(constructEmailMessage(registyCaseList[0], c));   
            }
        }
        // system.debug('#### notifications :: '+notifications);                                       
    }

    
    /**
    This method is used to just construct list of notifications for a single Case and single Registration entry.
    (a) Identifies the type of recipient (Emails/Case Team-Observer/Case Team-Primary, Case Contact)
    (b) Uses the associated User's email preferences (in case of only Case Contact and Case Team recipients) to construct notifications
    **/  
    public List<Messaging.SingleEmailMessage> getEmailMessage(Notification_Strategy__c  reg, Case objCase)
    {
        //setFinalId.clear();
        //lstEmail.clear();

        // system.debug('#### reg :: '+reg);
        // system.debug('#### objCase :: '+objCase.id);

        // system.debug('#### objCase.Contact_Email1__c :: '+objCase.Contact_Email1__c);
        
        if(reg.Recipient_Type__c == 'Object' && reg.Receiver__c == 'Contact')
        {
            // system.debug('#### Inside Contact getEmailMessage :: ');
            // system.debug('#### mapContactEmailUser :: '+mapContactEmailUser);

            if (mapContactEmailUser != null && mapCaseIdContactEmail.get(objCase.id) != null &&
                 mapContactEmailUser.get(mapCaseIdContactEmail.get(objCase.id)) != null) 
            {
                User usr = mapContactEmailUser.get(mapCaseIdContactEmail.get(objCase.id));

                // system.debug('#### mapCaseIdContactEmail.get(objCase.id) :: '+mapCaseIdContactEmail.get(objCase.id));
                // system.debug('#### User for Contact :: '+usr);                
                // system.debug('#### User record from Contact before adding :: '+usr);
                // system.debug('#### objCase.IsFirstUsrAllocation__c :: '+objCase.IsFirstUsrAllocation__c);

                if(usr != null)
                {
                    if((objCase.Status == 'Open' && usr.PreSales_Case_Created__c) || 
                        (objCase.Status == 'In Progress' && usr.PreSales_Case_Assigned__c) ||
                        (objCase.Status == 'Rejected') || 
                        (objCase.Status == 'Closed' && usr.PreSales_Case_Closed__c) || 
                        (objCase.Status == 'Pending Customer') ||
                        (objCase.Status == 'Pending Engineering' && usr.PreSales_Case_Pending_Engg__c) || 
                        (objCase.Status == 'Pending Internal' && usr.PreSales_Case_Pending_Internal__c) || 
                        (objCase.Status == 'Resolved' && usr.PreSales_Case_Resolved__c) ||
                        (setPresalesRecordTypes.contains(objCase.Record_Type_Name__c) && setPresalesClosedStatus.contains(objCase.Status) && usr.PreSales_Case_Closed__c))
                    {
                        if(!addedToRecipients(objCase,usr.id)) setFinalId.add(usr.id);
                        // system.debug('#### Added Contact User :: setFinalId :: '+setFinalId);
                    }
                }
                
            }
            setNonUserContactId=MapsetNonUserContactId.get(objCase.id);
            if(setNonUserContactId != null && setNonUserContactId.size() > 0)
            {
                for (Id  contactId: setNonUserContactId) 
                {
                    if(!addedToRecipients(objCase,contactId)) setFinalId.add(contactId);
                }
                
            }

            // system.debug('#### setFinalId after Contact type :: '+setFinalId);
        }
        
        else if(reg.Recipient_Type__c.equalsIgnoreCase('CaseTeam'))
        {
            // system.debug('#### inside case team type');

            if(reg.Receiver__c.equalsIgnoreCase('Observer'))
            {
                // system.debug('#### inside Observer');
                
                // system.debug('#### case team members :: '+mapCaseIdTeamMember.get(objCase.id));

                if(mapCaseIdTeamMember != null && mapCaseIdTeamMember.get(objCase.id) != null)
                {
                    for (CaseTeamMember ctm: mapCaseIdTeamMember.get(objCase.id)) 
                    {
                        // system.debug('#### Team Role  :: '+ctm.TeamRole.Name);

                        if(ctm.TeamRole != null && ctm.TeamRole.Name.equalsIgnoreCase('Observer'))
                        {
                            User usr = mapIDUser.get(ctm.MemberId);

                            // system.debug('#### objCase Status :: '+objCase.Status);
                            // system.debug('#### Usr :: '+usr);
                            // system.debug('#### User Open Preference :: '+usr.PreSales_Case_Created__c);

                            if(usr != null)
                            {
                                if((objCase.Status == 'Open' && usr.PreSales_Case_Created__c) || 
                                    (objCase.Status == 'In Progress' && usr.PreSales_Case_Assigned__c) ||
                                    (objCase.Status == 'Rejected') || 
                                    (objCase.Status == 'Closed' && usr.PreSales_Case_Closed__c) || 
                                    (objCase.Status == 'Pending Customer') ||
                                    (objCase.Status == 'Pending Engineering' && usr.PreSales_Case_Pending_Engg__c) || 
                                    (objCase.Status == 'Pending Internal' && usr.PreSales_Case_Pending_Internal__c) || 
                                    (objCase.Status == 'Resolved' && usr.PreSales_Case_Resolved__c) ||
                                    (setPresalesRecordTypes.contains(objCase.Record_Type_Name__c) && setPresalesClosedStatus.contains(objCase.Status) && usr.PreSales_Case_Closed__c))
                                {
                                    
                                    
                                    ///// 
                                    /*
                                    Modified if condition and if block below to prevent duplicate emails when (a) Contact and a case team member have same email ids and (b) when 2 case team members have same email ids
                                    */
                                    if(mapCaseIdContactEmail.get(objCase.id) != ctm.Member.Email && 
                                        (mapCaseIdSetCTMEmails.get(ctm.ParentId) == null || !mapCaseIdSetCTMEmails.get(ctm.ParentId).contains(ctm.Member.Email)) &&
                                        !addedToRecipients(objCase,ctm.MemberId))
                                    {
                                        setFinalId.add(ctm.MemberId);
                                        addCTMMemberEmailForCase(ctm);
                                    } 
                                }
                            }
                            else
                            {
                                String strMemberId = (string)ctm.MemberId;
                                if(strMemberId.startsWith('003'))
                                    if(mapCaseIdContactEmail.get(objCase.id) != ctm.Member.Email && 
                                        (mapCaseIdSetCTMEmails.get(ctm.ParentId) == null || !mapCaseIdSetCTMEmails.get(ctm.ParentId).contains(ctm.Member.Email)) &&
                                        !addedToRecipients(objCase,ctm.MemberId))
                                    { 
                                        setFinalId.add(ctm.MemberId);
                                        addCTMMemberEmailForCase(ctm);
                                    }
                            }
                        }
                    }
                }
                // system.debug('#### setFinalId after Observer type :: '+setFinalId);
            }

            else if(reg.Receiver__c.equalsIgnoreCase('Primary'))
            {
                // system.debug('#### Inside primary');
                if(mapCaseIdTeamMember != null && mapCaseIdTeamMember.get(objCase.id) != null)
                {
                    for (CaseTeamMember ctm: mapCaseIdTeamMember.get(objCase.id)) 
                    {
                        if(ctm.TeamRole != null && ctm.TeamRole.Name.equalsIgnoreCase('Primary'))
                        {
                            User usr = mapIDUser.get(ctm.MemberId);

                            if(usr != null)
                            {
                                if((objCase.Status == 'Open' && usr.PreSales_Case_Created__c) || 
                                    (objCase.Status == 'In Progress' && usr.PreSales_Case_Assigned__c) ||
                                    (objCase.Status == 'Rejected') || 
                                    (objCase.Status == 'Closed' && usr.PreSales_Case_Closed__c) || 
                                    (objCase.Status == 'Pending Customer') ||
                                    (objCase.Status == 'Pending Engineering' && usr.PreSales_Case_Pending_Engg__c) || 
                                    (objCase.Status == 'Pending Internal' && usr.PreSales_Case_Pending_Internal__c) || 
                                    (objCase.Status == 'Resolved' && usr.PreSales_Case_Resolved__c) ||
                                    (setPresalesRecordTypes.contains(objCase.Record_Type_Name__c) && setPresalesClosedStatus.contains(objCase.Status) && usr.PreSales_Case_Closed__c))
                                {
                                    if(mapCaseIdContactEmail.get(objCase.id) != ctm.Member.Email && 
                                        (mapCaseIdSetCTMEmails.get(ctm.ParentId) == null || !mapCaseIdSetCTMEmails.get(ctm.ParentId).contains(ctm.Member.Email)) &&
                                        !addedToRecipients(objCase,ctm.MemberId))
                                    { 
                                        setFinalId.add(ctm.MemberId);
                                        addCTMMemberEmailForCase(ctm);
                                    }
                                }
                            }
                            else
                            {
                                String strMemberId = (string)ctm.MemberId;
                                if(strMemberId.startsWith('003'))
                                    if(mapCaseIdContactEmail.get(objCase.id) != ctm.Member.Email && 
                                        (mapCaseIdSetCTMEmails.get(ctm.ParentId) == null || !mapCaseIdSetCTMEmails.get(ctm.ParentId).contains(ctm.Member.Email)) &&
                                        !addedToRecipients(objCase,ctm.MemberId))
                                    {
                                        setFinalId.add(ctm.MemberId);
                                        addCTMMemberEmailForCase(ctm);
                                    }
                            }
                        }
                    }
                }
                // system.debug('#### setFinalId after Primary type :: '+setFinalId);

            }
        }

        else if (reg.Recipient_Type__c.equalsIgnoreCase('Email') && reg.Receiver__c != null && reg.Receiver__c != '') 
        {

            //setFinalEmailIds.clear();

            Set<String> setTemp=new Set<String>() ;

            setTemp.addAll(reg.Receiver__c.split(','));

            for (String strEmail : setTemp) 
            {
                strEmail = strEmail.trim();
                if(!addedToRecipients(objCase,strEmail)) setFinalEmailIds.add(strEmail);    
            }

            // system.debug('#### setFinalEmailIds after EMail type :: '+setFinalEmailIds);
        }

        //constructEmailMessage(reg, objCase);
        // system.debug('#### lstEmail :: '+lstEmail);
    
        blnIsAutoNotificationCalled = true;
        //mapSetFinalId.put(objCase.id,setFinalId);

        /////Clearing the mapCaseIdSetCTMEmails
        // mapCaseIdSetCTMEmails.clear();

        return lstEmail;

    }


    /**
    This method just creates singleEmailMessage list for a single Case and its single registry record
    **/
    private List<Messaging.SingleEmailMessage>constructEmailMessage(Notification_Strategy__c  reg, Case objCase)
    {
        lstEmail.clear();
        Boolean blnFristID = true; 
        // system.debug('#### Reg inside constructEmailMessage :: '+reg);
        // system.debug('#### Case # inside constructEmailMessage :: '+objCase.Subject);
        //setFinalId=mapSetFinalId.get(objCase.id); 

       if(setFinalId != null && setFinalId.size()>0)
       {       
        for (integer i=0;i< setFinalId.size() ; i++) 
        {
            
            // system.debug('#### lstEmail inside contructmessage :: '+lstEmail);
            singleEmail = new Messaging.SingleEmailMessage(); //create a new SingleEmailMessage
            
            singleEmail.setTemplateId(reg.Template_ID__c);
            singleEmail.setOrgWideEmailAddressId(reg.From_ID__c.subString(0,15));
            singleEmail.setSaveAsActivity(false);
            singleEmail.setWhatId(objCase.id); //Link to Case
            singleEmail.setTargetObjectId(new List<ID>(setFinalId)[i]);
            
            /////

            for(String str: setFinalEmailIds)
            {
                if((mapCaseIdContactEmail != null && mapCaseIdContactEmail.get(objCase.Id) != null && str.equalsIgnoreCase(mapCaseIdContactEmail.get(objCase.Id))) || 
                    (mapCaseIdSetCTMEmails != null && mapCaseIdSetCTMEmails.get(objCase.Id) != null && mapCaseIdSetCTMEmails.get(objCase.Id).contains(str)))
                    setFinalEmailIds.remove(str);
            }

            /////


            if(i==0 && setFinalEmailIds != null && setFinalEmailIds.size() > 0)
                singleEmail.setToAddresses(new List<string> (setFinalEmailIds));
            
            lstEmail.add(singleEmail);

            // system.debug('#### lstEmail inside contructmessage 2 :: '+lstEmail);            
        }
       }

        else if(setFinalEmailIds != null && setFinalEmailIds.size() > 0)
        {
            
            singleEmail = new Messaging.SingleEmailMessage(); //create a new SingleEmailMessage
            
            singleEmail.setTemplateId(reg.Template_ID__c);
            singleEmail.setOrgWideEmailAddressId(reg.From_ID__c.subString(0,15));
            singleEmail.setSaveAsActivity(false);
            singleEmail.setWhatId(objCase.id); //Link to Case
            singleEmail.setTargetObjectId(System.Label.Dummy_Contact_Id);

            singleEmail.setToAddresses(new List<string> (setFinalEmailIds));

            lstEmail.add(singleEmail);

            setFinalEmailIds.clear();
        }
         return lstEmail;
    }



    /**
    These methods checks if the email/id is already added for a particular Case or not and returns a boolean to avoid sending
    duplicate emails for same recipient for same status update
    **/
    private Boolean addedToRecipients(Case objCase, String strDetail)
    {
        Boolean blnIsAddedToRecipients = false;

        if(mapCaseIdRecipientDetail.get(objCase.Id) == null)
                mapCaseIdRecipientDetail.put(objCase.Id, new Set<String>());
        
        blnIsAddedToRecipients = mapCaseIdRecipientDetail.get(objCase.Id).contains(strDetail);
        
        mapCaseIdRecipientDetail.get(objCase.Id).add(strDetail);

        return blnIsAddedToRecipients;

    }    

    
    /**
    This method is called from trigger. This method is first entry to this class.
    (a) Calls the Class constructor which inturn gets the cases, registry records and creates the list of notifications
    (b) The notofication list is actually sent from this method using the Messaging.sendEmail() method
    **/
    public static void notify(Map<Id, Case> mapNewCases, Map<Id,Case> mapTriggerOld, Boolean blnIsInsert)
    {
        Boolean noneOrAll= true;
        if(System.Label.AutoNoti_Send_NoneAll != null && System.Label.AutoNoti_Send_NoneAll.equalsIgnoreCase('False'))
            noneOrAll=false;
        // System.debug('noneOrAll:'+noneOrAll);   
        
        if(!blnIsAutoNotificationCalled)
        {
            NotifyHelper helper = new NotifyHelper(mapNewCases, mapTriggerOld, blnIsInsert);
        
            try 
            {
                if(helper.notifications != null && helper.notifications.size() > 0)
                {
                    // system.debug('#### helper.notifications returned to trigger:: '+helper.notifications);

                    Messaging.reserveSingleEmailCapacity(helper.notifications.size());
                    
                    //Messaging.sendEmail(helper.notifications);

                    List<Messaging.SendEmailResult> lstSendEmailResult = Messaging.sendEmail(helper.notifications,noneOrAll );

                    // Printing the Errors (if any) to the debug log
                    for (Messaging.SendEmailResult emailResult: lstSendEmailResult) 
                    {
                        if(!emailResult.isSuccess())
                        {
                            // system.debug('#### Email Error/Exception :: '+emailResult.getErrors());
                        }
                    }
                }
            }
            catch(Exception e) 
            {
                // TO DO - gracefully handle exceptions
                // log notifications that should have been sent
                // notify administrator

                // system.debug('#### Exception :: '+e);
            }
        }
    }



    /////
    /**
    New method added to add case team members email ids against the case id in a map to avoid duplicate emails when 2 case team mebers have same email ids
    **/
    private void addCTMMemberEmailForCase(CaseTeamMember ctm)
    {
        
        if(mapCaseIdSetCTMEmails.get(ctm.ParentId) == null)
            mapCaseIdSetCTMEmails.put(ctm.ParentId, new Set<String>());
        mapCaseIdSetCTMEmails.get(ctm.ParentId).add(ctm.Member.Email);
    }

    /////
}