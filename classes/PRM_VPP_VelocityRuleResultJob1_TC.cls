/*========================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER                WR       DESCRIPTION                               
 |  ====          =========                ==       =========== 
 |  25/03/2011    Suman B                           Unit Test for PRM_VPP_VelocityRuleResultJob1 class
 |  28/03/2011    Anand Sharma                      Created account record data in test class  
 |  06/12/2011    Anand Sharma                      Update test class according to new theater
 |  06/12/2011    Anil                              Used Custom Setting Data Halper
 |  17/05/2012    Kaustav                           Increased the code coverage
 |  22/01/2015    Karan                             Updated code with best practices under platform innovation project
 +=======================================================================================================*/
@isTest
private class PRM_VPP_VelocityRuleResultJob1_TC {
    
    //created static lists to access in different test methods, Karan,22 Jan 2015:  Platform innovation project
    static List<Account> accList = new List<Account>();
    private static void createTestData(){
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser)
        {   //used CustomSettingDataHelper class instead of PRM VPP data helper
            CustomSettingDataHelper.dataValueMapCSData();
        }
        Account_Groupings__c grouping = new Account_Groupings__c (Name = 'UNITTESTGrp');
        insert grouping;
        accList=AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        for(Integer i=0;i<accList.size();i++){
        
            accList[i].Country_Local__c='Japan';
            accList[i].Country_Local__c='APJ';
            accList[i].Specialty_Rule_Type__c='LA1';
            accList[i].Grouping__c=grouping.Id;
            accList[i].profiled_Account_flag__c=true;
            accList[i].Cluster__c = 'NA';
            if(i==0) {
                
                accList[i].Batch_Job_Operation__c='Clustor/Theather Updated';
            }
            if(i==1){
                accList[i].Batch_Job_Operation__c='New Profiled Account';
            }
            if(i==2){
                accList[i].Batch_Job_Operation__c='Porifled Account Unchecked';
            }
            
        }
        if(!accList.isEmpty()){
            insert accList;
        }
        //creation of 5 Velocity Rule Record 
        List<Velocity_Rules__c> velocityRulesRecords = new List<Velocity_Rules__c>();                                     
        list <Velocity_Rules__c> listVR = new list <Velocity_Rules__c>();
        velocityRulesRecords.add(new Velocity_Rules__c(Theater__c='APJ',cluster__c='NA',  Specialty_Rule_Type__c='LA1' , Tier__c='Affiliate Elite', recordtypeid='01270000000Q6iXAAS', VS_Section__c = 'QSS Implement', Evaluation_At__c = 'Country Grouping Level'));
        velocityRulesRecords.add(new Velocity_Rules__c(Theater__c='APJ',cluster__c='NA',  Specialty_Rule_Type__c='LA1' , Tier__c='Affiliate Elite', recordtypeid='01270000000Q933AAC', VS_Section__c = 'QSS Implement', Evaluation_At__c = 'Country Grouping Level', Any_Value_Acceptable__c = true));
        velocityRulesRecords.add(new Velocity_Rules__c(Theater__c='APJ',cluster__c='NA',  Specialty_Rule_Type__c='LA1' , Tier__c='Affiliate Elite', recordtypeid='01270000000Q8iDAAS', VS_Section__c = 'QSS Implement', Evaluation_At__c = 'Country Grouping Level'));
        velocityRulesRecords.add(new Velocity_Rules__c(Theater__c='APJ',cluster__c='NA',  Specialty_Rule_Type__c='LA1' , Tier__c='Affiliate Elite'));
        
        Insert velocityRulesRecords;  

          List<Velocity_Rule_Results__c> lstVelocityRuleResulttemp=new List<Velocity_Rule_Results__c>();
           for(Integer iCounter=0;iCounter<velocityRulesRecords.size();iCounter++)
           {
                Velocity_Rule_Results__c tempVeloRuleResultObj=new Velocity_Rule_Results__c();
                tempVeloRuleResultObj.Speciality_RuleID__c= velocityRulesRecords[iCounter].id;//; lstVelocityRules[iCounter].Id
                tempVeloRuleResultObj.AccountID__c=accList[0].Id;
                tempVeloRuleResultObj.Grouping__c = grouping.Id;
                lstVelocityRuleResulttemp.add(tempVeloRuleResultObj);               
           }
           insert lstVelocityRuleResulttemp;        
           
       }
    static testMethod void  PRM_VPP_VelocityRuleResultJob1_Test   () {
    //create custom setting, Account & Account grouping test data
        PRM_VPP_VelocityRuleResultJob1_TC.createTestData();
               
        Test.StartTest();           

        // run batch 
       String ruleResultQuery = 'Select Id, Grouping__c,Velocity_Solution_Provider_Tier__c,Batch_Job_Operation__c,Specialty_Rule_Type__c,Cluster__c '
                               + 'from Account e '
                               + 'where Batch_Job_Operation__c!=null Limit 4' ;
        Id batchProcessId = Database.executeBatch(new PRM_VPP_VelocityRuleResultJob1(ruleResultQuery));  
        Id batchProcessId1 = Database.executeBatch(new PRM_VPP_VelocityRuleResultJob1());
        
        Test.StopTest();
    }
    
   static testMethod void PRM_VPP_VelocityRuleResultJob1Scheduler_Test() {
   //create custom setting, Account & Account grouping test data
       // PRM_VPP_VelocityRuleResultJob1_TC.createTestData();        
        Test.StartTest();           
        // run batch 
        String strScheduleTime ='0 0 0 3 9 ? ';
        strScheduleTime = strScheduleTime + Datetime.now().addYears(1).year();
        String ruleResultQuery = 'Select Id, Grouping__c,Velocity_Solution_Provider_Tier__c,Batch_Job_Operation__c,Specialty_Rule_Type__c,Cluster__c '
                               + 'from Account e '
                               + 'where Batch_Job_Operation__c!=null Limit 4' ;

        //String jobId = System.schedule('testBasicScheduledApex',strScheduleTime, new PRM_VPP_VelocityRuleResultJob1());
        List<SequencialBatchJob>LstSeqJobs = new List<SequencialBatchJob>(); 
        PRM_VPP_VelocityRuleResultJob1  job1= new PRM_VPP_VelocityRuleResultJob1();       
        LstSeqJobs.add(new SequencialBatchJob('PRM_VPP_VelocityRuleResultJob1' ,job1,20));   
        SequencialBatchJob seqBatchJob=new SequencialBatchJob('PRM_VPP_VelocityRuleResultJob1' ,job1);
        seqBatchJob.setStatus('testing method');
        SequencialBatchJobScheduler seqBatchJobSchedulerObj=new SequencialBatchJobScheduler();
        seqBatchJobSchedulerObj.addSequencialBatchjob(seqBatchJob);
        seqBatchJobSchedulerObj.addSequencialBatchjob(seqBatchJob);
        seqBatchJobSchedulerObj.getPreviousSequencialBatchjob();
        System.debug('#### seqBatchJobSchedulerObj.getPreviousSequencialBatchjob()'+seqBatchJobSchedulerObj.getPreviousSequencialBatchjob());
        System.debug('#### seqBatchJobSchedulerObj.getNextSequencialBatchjob()'+seqBatchJobSchedulerObj.getNextSequencialBatchjob());
        SequencialBatchJobScheduler.executeSequence(LstSeqJobs);
        Test.StopTest();
    }
}