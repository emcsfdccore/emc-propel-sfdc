@isTest
public Class R2R_LinkAssetsViewController_TC{
    public static testmethod void testsLeadAssetController()
    {    
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        } 
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='testR2Ruser1@emailR2RTest.com', alias='tuser1', CommunityNickName='tuser1', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' ,Forecast_Group__c = 'Maintenance Renewals'); 
            
        insert insertUser;
        
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where DeveloperName like 'EMC_Install' or DeveloperName Like 'Competitive_Install'];

        List<Account> acc = new list<Account>();
        acc.add(new Account(Name = 'TestAcount0', Status__c='A'));
        insert acc;

        Opportunity opp = new Opportunity ( Name = 'Test Oppty', StageName = 'Pipeline', CloseDate = System.today()+2, Sales_Force__c = 'EMC', Sales_Channel__c = 'Direct', VMWare_Related__c = 'Not VMware Related', AccountId = acc[0].Id  ) ;
        insert opp;
         List<Lead> lstLeadToInsert = new list<Lead>();

        //Creating Lead
        Lead lead1= new Lead();
        lead1.Company='TestLead';
        lead1.LastName='Test123';
        lead1.Related_Account__c=acc[0].id;
        //lead1.Related_Opportunity__c=opp.id;
        lead1.Status='Swap';
        lead1.Sales_Force__c='EMC';
        lead1.Lead_Originator__c='Customer Intelligence';
        lead1.Lead_Type_Based_on_Linking__c='Refresh';
        lstLeadToInsert.add(lead1);

        //Creating other Lead
        Lead lead2= new Lead();
        lead2.Company='TestLead1';
        lead2.LastName='Test1231';
        lead2.Related_Account__c=acc[0].id;
        //lead2.Related_Opportunity__c=opp.id;
        lead2.Status='Refresh';
        lead2.Lead_Type_Based_on_Linking__c='Renewals';
        lead2.Sales_Force__c='EMC';
        lead2.Lead_Originator__c='Customer Intelligence';
        lstLeadToInsert.add(lead2);
        insert lstLeadToInsert;


        //Creating list type asset
        List<Asset__c> listSelectedAssets=new List<Asset__c>();
           
        //Creating Comp asset
        Asset__c asset1= new Asset__c();
        asset1.Name='TestassetComp';
        asset1.Customer_Name__c=acc[0].id;
        asset1.RecordTypeId=recordType[0].id;
        asset1.Product_Name_Vendor__c='Dell';

        //Creating EMC asset
        Asset__c asset2= new Asset__c();
        asset2.Name='TestassetEMC';
        asset2.Customer_Name__c=acc[0].id;
        asset2.RecordTypeId=recordType[1].id;

        listSelectedAssets.add(asset1);
        listSelectedAssets.add(asset2);
        insert listSelectedAssets;

        lead2.Lead_Type_Based_on_Linking__c='Renewals';
        update lead2;

        //listSelectedAssets= controller.getselected();
        string rendersection = Apexpages.currentPage().getParameters().put('rendersection','lead');
        string accdist = Apexpages.currentPage().getParameters().put('accDist','AUSTRALIA AND NEW ZEALAND BANKING GROUP LIMITED: ADP District');
        string assetId = Apexpages.currentPage().getParameters().put('assetid',listSelectedAssets[0].id);
        string leadToFetch= Apexpages.currentPage().getParameters().put('selLead',lstLeadToInsert[0].id);
        string leadIdFromURL= Apexpages.currentPage().getParameters().put('leadId',lstLeadToInsert[0].id); 
        string assetaccid=Apexpages.currentPage().getParameters().put('accid',acc[0].id);
        String selectedLead=Apexpages.currentPage().getParameters().put('selectedLead',lstLeadToInsert[0].id);


        //Apexpages.currentPage().getParameters().put('assetid',asset1.id);
        //Creating Lead asset junction with lead status =Swap and EMC asset attached
        List<Lead_Asset_Junction__c> listlaj= new List<Lead_Asset_Junction__c>();
        Lead_Asset_Junction__c laj1= new Lead_Asset_Junction__c();
        laj1.Related_Asset__c=listSelectedAssets[0].id;
        laj1.Related_Lead__c=lead1.id;


        listlaj.add(laj1);


        //Creating Lead asset junction with Lead status= Refresh and COmp asset attached
        /*Lead_Asset_Junction__c laj2= new Lead_Asset_Junction__c();
        laj2.Related_Asset__c=listSelectedAssets[1].id;
        laj2.Related_Lead__c=lead2.id;

        listlaj.add(laj2);*/
        //insert listlaj;

         
        Test.startTest();
        //Creating StandardSetController Object
        if(listSelectedAssets !=null)
        {
            ApexPages.StandardSetController con = new ApexPages.StandardSetController(listSelectedAssets);
            con.SetSelected(listSelectedAssets);
            
            R2R_LinkAssetsViewController obj=new R2R_LinkAssetsViewController(con); 
            
            Boolean showClearList;
            showClearList=true;
            obj.strinputcode= 'testLead'; 
            obj.accdistrictName ='AUSTRALIA AND NEW ZEALAND BANKING GROUP LIMITED: ADP District';
            obj.SearchLead();
            obj.cancelLink();
            System.runAs(insertUser)
            {
                obj.linkAssestToLead();
            }   

        }
        Test.stopTest();

    }


}