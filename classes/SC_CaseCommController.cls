/*==================================================================================================================+
 | This Class is used for Community Case Creation
 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        ===========       
 
 | 15/10/2014     Aagesh             This Class is used for SC Webform Case logic
 | 15/01/2015 Akash Rastogi    1528  Commented few lines of code
 +==================================================================================================================**/

public class SC_CaseCommController{

public String languageVar { get; set; }
public String myUserLanguage;
public String getLang() {
        Cookie lang = ApexPages.currentPage().getCookies().get('lang');
        if(lang == null) {
            return 'en';
        }
        return lang.getValue();
    }
    public String getMyUserLanguage() {
        return system.Userinfo.getLanguage();
    }
    private final Case cs;
    public Boolean caseSaved {get; set;} {caseSaved=false;}
    public Boolean caseInserted{get; set;} {caseInserted=false;}
    public Attachment caseAttachment {get; set;}
    public list<Attachment> caseAttachments {get; set;}
    public Case caseObj {get; set;}
    public User loggedInUser;
    public String recordTypeValue {get; set;}
    public String recordType {get; set;}
    
    public String caseTypeName{get;set;}
    public String caseSubTypeName{get;set;}
    


    //Language

    public String selectedLanguage{get; set;}
    public Map<String,String> mapLang = new Map<String,String>();



   //Attachment
    public static final integer attachmentsToAdd = 4;

    public List<Attachment> newAttachments{get;set;}
    
    //Recordtype Field Mapping
    public List<String> fldString{get; set;}
    public Map<String,SC_RecordType_Mapping__c> fldMappingMap{get; set;}
    public String recSubTypeMapping {get; set;}
    public RecordType rtObj = new RecordType();

    //Case ID
    public Id caseid {get;set;}
    public PageReference caseUrl;

    //Rendering Fields
    public Boolean addAtt {get; set;}

    public Boolean isMCOTheater {get; set;}
    public Boolean isMCOPriority {get; set;}

    public Boolean stdType {get; set;}
    
    //Hidding Submit Button On Page Load
    
    public Boolean showSubmit {get; set;}
    
    public Boolean isCaseSubTypeDes {get;set;}
    public String caseSubTypeDes {get;set;}
    public String val {get;set;}
    //Record Type Setting

    public String caseRecordType{get; set;}
 

    //Rerender Output Panel for Case Sub Type Description
     /* @Method <This method is used to rerender Output Panel for Case Sub Type Description>
    @param <This method takes no parameter>
    @return void - <Not returning anything>
    @throws exception - <No Exception>
    */
    public pagereference ischanged()
    { 
      system.debug('onchange event::::');
      if(caseTypeName!=null)
        system.debug('CaseTypeValue:::'+caseTypeName);
        val = ApexPages.currentPage().getParameters().get('caseType');
        system.debug('val:::'+val);
        
      return null;
    } 
    public void displayDescription(){
        if(!stdType)
        {
            caseObj.Type = caseObj.SC_Webform_Case_Sub_Type__c;
            
        }
        System.debug('caseObj.Type----'+caseObj.Type);

        
            
        if (caseObj.Type =='--None--' || caseObj.Type =='' ){
           System.debug('caseObj.Type--->'+caseObj.Type);
           caseSubTypeDes = '';
           isCaseSubTypeDes = false;
        }else{
              isCaseSubTypeDes = true;
              Map<String, SC_IBG_CaseSubType_Desc__c> mapCaseDescCS = SC_IBG_CaseSubType_Desc__c.getAll();
              if(mapCaseDescCS.containsKey(caseObj.Type)){
                  SC_IBG_CaseSubType_Desc__c objCS = mapCaseDescCS.get(caseObj.Type);
                  caseSubTypeDes=objCS.Label_Name__c;
              }
              
        }
        System.debug('isCaseSubTypeDes'+isCaseSubTypeDes );
        System.debug('caseSubTypeDes'+caseSubTypeDes);
        renderFields();
    }
         
   //Success Page

    public String caseNumber {get; set;}


      public SC_CaseCommController() {
          caseid = ApexPages.currentPage().getParameters().get('id');
          caseNumber = ApexPages.currentPage().getParameters().get('caseNmber');
          newAttachments=new List<Attachment>();           
          
          selectedLanguage = ApexPages.currentPage().getParameters().get('langForPage');
          if(selectedLanguage==null || selectedLanguage=='' )
          { 
              selectedLanguage =UserInfo.getLanguage(); 
          }
          
          //Added For Record Type Mapping from Sucess PAge
        recordTypeValue = ApexPages.currentPage().getParameters().get('recordType');

      }
 
/* @Method <This Constructor execute is used to Perfoem Initial level of logic on SC Case Page>
@param <This method takes StandardController as parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/

    public SC_CaseCommController(ApexPages.StandardController controller) {
        this.cs = (Case)controller.getRecord();
        caseAttachment = new Attachment();
        caseAttachments = new List<Attachment>();
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        
        //Attachment Button

        addAtt = true;

        //Standard Type Logic

        stdType = true;
        
        //Hidding Submit Button On Intial Page Load
           
         showSubmit = false;
        
        //Language


        selectedLanguage = ApexPages.currentPage().getParameters().get('langForPage');
        if(selectedLanguage==null || selectedLanguage=='' )
          { 
            selectedLanguage =UserInfo.getLanguage();  
          }
        
        recordTypeValue = ApexPages.currentPage().getParameters().get('recordType');
        
        caseSubTypeName = ApexPages.currentPage().getParameters().get('caseType');
        
        //Added by Jaypal for GBS Com
        
        
     
        
        if((selectedLanguage!='en_US') && (recordTypeValue=='Global Revenue Operations' || recordTypeValue=='Maintenance Contract Operations')){
            selectedLanguage='en_US';
        }
        System.debug('selectedLanguage'+selectedLanguage);

          Map<String,SC13_Language__c> mapLangauage = SC13_Language__c.getall();


            recSubTypeMapping = '';


          for(SC13_Language__c langSetting : mapLangauage.values()){


              mapLang.put(langSetting.Language_Code__c , langSetting.Language__c);

          }




     caseObj = (Case)controller.getRecord();  
        newAttachments=new List<Attachment>();  

     
       
        fldString = new List<String>();
            fldMappingMap = new Map<String,SC_RecordType_Mapping__c>();
        
           

                                    System.debug('Thiss is recordTypeValue---->'+recordTypeValue);


            if(recordTypeValue!=null){
          
            

            //Getting Detault Type for Record Type



            Map<String,SC13_RecordType_Type__c> rtTypeMap = SC13_RecordType_Type__c.getall();

            if(rtTypeMap.containsKey(recordTypeValue.trim())){
            
                recordType = rtTypeMap.get(recordTypeValue.trim()).SC_Record_Type__c;

                 if(!rtTypeMap.get(recordTypeValue.trim()).isStandardType__c){
                        stdType = false;
                        System.debug('inside if looop------>'+stdType);
                    
                }
            }else{
                recordType = 'Presales Performance Analysis';
            }
            System.debug('Thiss is recordType---->'+recordType);
            System.debug('Thiss is caseTypeName---->'+caseTypeName);            
             rtObj  = [Select id,Name from RecordType where sObjectType='Case' and DeveloperName like 'Presales%' and name=:recordType.trim()];
            caseObj.recordtypeid = rtObj.id;
            
            if(!stdType){
                caseObj.SC_Webform_Case_Sub_Type__c = caseSubTypeName;
            }
            else
            {
                caseObj.Type = caseSubTypeName;
            }
            System.debug('caseObj.Type----1>'+caseObj.Type);
            
            if(rtObj.Name==Label.SC13_MaintenanceContractOperations_Donot_Translate){

                    isMCOTheater = true;
                    isMCOPriority = true;



            }

            
            
            renderFields();

          

            System.debug('Thiss is recSubTypeMapping---->'+recSubTypeMapping);

          
       }
            

    
   
      
   }

/* @Method <This is Action Method fot Add more Button>
@param <This method takes No parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/
    public void addMore(){
        
        for (Integer i=0; i<attachmentsToAdd; i++)
        {
            system.debug('first attmt--->'+newAttachments);
            newAttachments.add(new Attachment());
            system.debug('first attmt after adding--->'+newAttachments);
   
        }

        addAtt = false;
    } 
    
   /* @Method <This is Action Method for Case Save logic (Submit Button)>
@param <This method takes No parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/ 

public PageReference saveCaseOld()
{
    try
    {
    List<Attachment> attList =new List<Attachment>();
    ApexPages.Message myMsg;
    myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.SC_Error_Message);
    String email = caseObj.contact_Email1__c ; 
    Boolean createCase = true;
    boolean emcEmailValidation=true;
    
    List<String> nameList= email.split('@');

    if (!(nameList[0].contains('.')) && email.contains('@emc.com'))
    {
    createCase = false;
    emcEmailValidation=false;
    }
//Added to show email validation once if any of the email field is not in proper format
    if(emcEmailValidation==false) {ApexPages.addMessage(myMsg);}

    if((ApexPages.getMessages()).size()>0)
    {
    createCase = false;
    }

    if(createCase)
    {
    //Inserting Case
        try
        {
        caseObj.Webform_Language__c = mapLang.get(selectedLanguage);
        insert caseObj;
        }
        catch(Exception e)
        {
        ApexPages.addMessages(e);
        }

        if(!newAttachments.isEmpty())
        {
            for (Attachment newAtt : newAttachments)
            {
            system.debug('attmt body--->'+newAtt.Body);
                if (newAtt.Body!=null)
                {
                newAtt.parentId=caseObj.id;
                attList.add(newAtt);
                }
            }
        }

        Case newCaseRecord =[select id,CaseNumber from Case where id = :caseObj.id];

        //Inserting Attachment
        if(!attList.isEmpty())
            try
            {
            insert attList;
            }
            catch(Exception e)
            {
            ApexPages.addMessages(e);
            }




        if(newCaseRecord != Null)
        {
            caseUrl = new PageReference('/apex/SC_CaseComm_Success?caseNmber='+newCaseRecord.CaseNumber);
            caseUrl.setredirect(true);
        }

    }
   
    return caseUrl;
}
catch(Exception e)
{
ApexPages.addMessages(e);
return caseUrl;
}
}
 


        public String getApplicationID(){

            return null;
        
    }

/* @Method <This  Action Method  is used for Select Button Used Next To Case Type Selection>
@param <This method takes No parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/

     public void renderFields(){


         fldString.clear();
         fldMappingMap.clear();
         recSubTypeMapping = '';
           
           if(!stdType){
            caseObj.Type = caseObj.SC_Webform_Case_Sub_Type__c;
           }
        System.debug('caseObj.Type---- Render'+caseObj.Type);

         String caseTypeFld = String.valueof(caseObj.Type);
            
            if (caseObj.Type =='--None--' || caseObj.Type =='' ){
            System.debug('caseObj.Type------>'+caseObj.Type);
                return;
            }
            recSubTypeMapping = recordType.trim()+'+'+caseTypeFld;

            System.debug('recSubTypeMapping------>'+recSubTypeMapping);
           
              List<SC_RecordType_Mapping__c> lstRecord = [Select FieldName__c,Section_Name__c,isMandatory__c,isRendered__c,IsRenderedForCommunity__c,SortOrder__c,RecordType_Subtype__c from SC_RecordType_Mapping__c where RecordType_Subtype__c=:recSubTypeMapping and IsRenderedForCommunity__c=true and isPSC_field__c = false order by SortOrder__c ];

                for(SC_RecordType_Mapping__c scRecObj: lstRecord){
        
                    fldString.add(scRecObj.FieldName__c);
                    fldMappingMap.put(scRecObj.FieldName__c,scRecObj);
                }
                
                //Displaying Submit Button
                showSubmit = true;
        
       
    } 

/* @Method <This Action Method will be called on page load for intializing attachment>
@param <This method takes No parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/
        public void createInstance(){
        
    
        newAttachments=new List<Attachment>{new Attachment()};
        
        for (Integer i=0; i<attachmentsToAdd; i++)
        {
            system.debug('first attmt--->'+newAttachments);
            newAttachments.add(new Attachment());
            system.debug('first attmt after adding--->'+newAttachments);
   
        }
       
    }

     public PageReference saveAttachment()  {
         PageReference pageRef=null;
         try{
                List <Attachment> AttachmentsToInsert = new List <Attachment> (); 
                
                if (caseSaved==true) {
                    
                    //clear assets those are in
                    for( Attachment caseAttachment: caseAttachments){
                        if(caseAttachment.body!=null  ){
                             
                             System.debug('cs.id '+cs.id);
                             System.debug('caseAttachment.parentId '+caseAttachment.parentId);
                             System.debug('caseAttachment.name'+caseAttachment.name);
                            if(caseAttachment.parentId == null ){
                                caseAttachment.parentId = cs.id;
                                AttachmentsToInsert.add(caseAttachment);
                            }
                        }
                    }
                    
                    if(AttachmentsToInsert.size()>0){
                        system.debug('AttachmentsToInsert '+AttachmentsToInsert);
                        insert AttachmentsToInsert;
                        caseAttachment = null;
                        AttachmentsToInsert=null;
                        caseAttachments = new List<Attachment>();
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                    }
                    Case newCaseRecord =[select id,CaseNumber from Case where id = :cs.id];
                    if((ApexPages.getMessages()).size()>0 ) {
                        try{
                            List<Attachment> Att_List =[SELECT Id FROM Attachment where ParentId=:cs.id];
                            if(Att_List != null && Att_List.size()>0  ){
                                delete Att_List;
                            }
                        }catch(exception e){
                            system.debug(e);
                        }
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info,Label.SC_FileAttachHelpText));
                        pageRef = null;
                        caseId=cs.id;
                        caseSaved=true;
                        cs.id=null;
                        
                        
                    }else{
                        System.Debug('Before Success Case Id:'+newCaseRecord.Id+':'+newCaseRecord.CaseNumber+':selectedLanguage:'+selectedLanguage+':recordTypeValue:'+EncodingUtil.urlEncode(recordTypeValue, 'UTF-8'));
                        pageRef = new PageReference('/apex/SC_CaseComm_Success?id='+newCaseRecord.Id+'&caseNmber='+newCaseRecord.CaseNumber+'&langForPage='+selectedLanguage+'&recordType='+EncodingUtil.urlEncode(recordTypeValue, 'UTF-8') );
                        pageRef.setRedirect(true);  
                    }
                           
                }
               
                
         }catch(Exception e){
             ApexPages.addMessages(e);
             pageRef=null;
             System.debug(e);
         }
         return pageRef;
    }
    public void saveCase() {
       try{ 
                     
                    validateEmail();
                    if(recordTypeValue.equalsIgnoreCase(Label.SC_Track_Resource_Hierarchy_Donot_Translate)){
                        trackFields();
                    }
                     if(caseSaved ){
                       
                         if(caseId==null){
                             cs.Webform_Language__c = mapLang.get(selectedLanguage);
                             cs.Origin = 'Community';
                             insert cs;    
                           }else{
                            cs.id=caseId;   
                            update(cs);
                          }
                          system.debug('Value:::::'+cs);
                        caseInserted=true;
                        caseSaved=true;
                        
                    }
                
        }catch(exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
             caseSaved=false;
        }
    } 
    
     public void validateEmail(){
        System.debug('validate email....');      
        ApexPages.Message myMsg;
        myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter your EMC email address in the firstname.lastname@emc.com format');
        loggedInUser = [Select Email, LastName, Phone,Contact_Account_Name__c from User where id=:Userinfo.getuserid()];
        system.debug('Loggedin user email::::'+loggedInUser.email);
        String email = loggedInUser.email;
        cs.Contact_Email1__c = loggedInUser.email;
        
      
        List<String> nameList= email.split('@');
        System.debug('email  '+email );
        if ( email.contains('@emc.com')){
                if(nameList[0].contains('.')) {
                     caseSaved = true;  
                 }else{
                     caseSaved = false;  
                     ApexPages.addMessage(myMsg);
                 }
             
        }
         else{
             caseSaved = true;        
       }
 
    }

    public void trackFields(){
        System.debug('inside Track--------------------->');
      
         //WR 1528 Commenting the following code 
         /*String resourceMirror = String.valueof(cs.EN_Resource_to_Mirror__c);
         String managerName = String.valueof(cs.PWID_Manager_Name__c);
         String resourceName = String.valueof(cs.PWID_Resource_Name__c);
         if(resourceMirror!=null){
            resourceMirror = resourceMirror.leftPad(6);
            resourceMirror = resourceMirror.Replace(' ','0');
            try{
            User resourceUser = [Select id from User where isactive = true and Employee_Number__c=:resourceMirror and Profile.UserType = 'Standard'  limit 1];
            System.debug('resourceUser-------->'+resourceUser);
            cs.Resource_to_Mirror__c = resourceUser.id;
            }
            catch(Exception ex){
                 caseSaved = false; 
                   ApexPages.Message myMsg;
                 myMsg = new ApexPages.Message(ApexPages.Severity.Warning,Label.SC_Error_Message2);
                 ApexPages.addMessage(myMsg);

            }
            


        }

        if(managerName!=null || resourceName!=null){
            System.debug('managerName----------->'+managerName+'resourceName-------------->'+resourceName);
            
            List<Contact> contLst = [SELECT Id,PowerlinkID_Badge__c FROM Contact  where Contact.recordtype.name = 'EMC Internal Contact'  and Active__c = true and PowerlinkID_Badge__c!=null and (PowerlinkID_Badge__c=:managerName or  PowerlinkID_Badge__c=:resourceName)];
            Map<String , Id> contactMap = new Map<String , Id>();

            for(Contact cnt : contLst){

                contactMap.put(cnt.PowerlinkID_Badge__c , cnt.id);


            }

            System.debug('contactMap----------->'+contactMap);

            if(contactMap.size()>0){

                if(managerName!=null){

                    System.debug('Inside IF1--->');

                        if(contactMap.containsKey(managerName)){
                            System.debug('Inside IF2--->');
                        cs.Manager_Name__c = contactMap.get(managerName);
                        }
                        else{
                            System.debug('Inside else1--->');
                                 caseSaved = false; 
                              ApexPages.Message myMsg;
                              myMsg = new ApexPages.Message(ApexPages.Severity.Warning,Label.SC_Error_Message3);
                             ApexPages.addMessage(myMsg);
    
                        }


                }
                if(resourceName!=null){
                    System.debug('Inside IF3--->');
                        if(contactMap.containsKey(resourceName)){
                            System.debug('Inside IF4--->');
                           cs.Resource_Name__c = contactMap.get(resourceName);

                        }
                        else{
                            System.debug('Inside else2--->');
                                 caseSaved = false; 
                              ApexPages.Message myMsg;
                              myMsg = new ApexPages.Message(ApexPages.Severity.Warning,Label.SC_Error_Message4);
                             ApexPages.addMessage(myMsg);
    
                        }


                }

                


            }

            else if(contactMap.size()<=0){
                         caseSaved = false; 
                      ApexPages.Message myMsg;
                      myMsg = new ApexPages.Message(ApexPages.Severity.Warning,Label.SC_Error_Message5);
                      ApexPages.addMessage(myMsg);

            }


        }*/
        //WR#1528 Changes ends here
    }

//New Method to set Record type and Lang from Thank you page
         public PageReference backtoCase()  {

                    PageReference pageRef = new PageReference('/apex/CommunitiesHomePage?sfdc.tabName=01rn00000000C3s');
                    pageRef.setRedirect(true);  
                    return pageRef;


         }

}