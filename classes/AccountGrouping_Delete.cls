/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 |  Sept/9/2013     Leonard V   257406     This Class is used for updating reporting fields when Grouping is updated to deleted                                       
 +==================================================================================================================**/

public class AccountGrouping_Delete{

//Method for Updating Reporting Field
 public void reportingFieldUpdation(Map<Id,Account_Groupings__c> oldGrpMap , Map<Id,Account_Groupings__c> newGrpMap){

     Map<Id , Account_Groupings__c> inactiveGrpMap = new Map<Id , Account_Groupings__c>();
     List<Account> accToUpdate = new List<Account>();

     for(Account_Groupings__c groupObj : newGrpMap.values()){

         System.debug('groupObj.Active__c-->' +groupObj.Active__c );
         System.debug('groupObj.Inactivation_Reason__c-->' +groupObj.Inactivation_Reason__c );


         if((groupObj.Active__c!= oldGrpMap.get(groupObj.id).Active__c) && (groupObj.Inactivation_Reason__c!= oldGrpMap.get(groupObj.id).Inactivation_Reason__c && groupObj.Inactivation_Reason__c!=null) ){

             inactiveGrpMap.put(groupObj.Profiled_Account__c,groupObj);
             System.debug('inside if-->' +inactiveGrpMap );

         }


     }

     Map<Id,Account> mapAcct = new Map<Id,Account>([Select id,Grouping_End_Date__c,Grouping_Inactivation_Reason__c from Account where id in :inactiveGrpMap.keySet()]);

     for(Account accObj : mapAcct.values()){
        
            if(inactiveGrpMap.ContainsKey(accObj.id)){

                    //accObj.Grouping_End_Date__c =inactiveGrpMap.get(accObj.id).End_Date__c;
                    accObj.Grouping_End_Date__c =System.today();
                    accObj.Grouping_Inactivation_Reason__c =inactiveGrpMap.get(accObj.id).Inactivation_Reason__c;
                    accToUpdate.add(accObj);


            }


     }
          System.debug('accToUpdate------>'+accToUpdate);


     try{

         update accToUpdate;

     }
     catch(Exception ex){

     }


 }

 //End OF Method


}