/*================================================================================================+

|  HISTORY  |                                                                           

|  DATE          DEVELOPER                WR        DESCRIPTION                               

|  ====          =========                ==        =========== 

| 12/09/2011     Anil Sure              #3656      This test class is used to
                                                   test the Search Criteria for Lead Deal Search.
|  14/12/2011       Anil                           Used Custom setting Data Helper
|  07/05/2012    Anand Sharma                       Fixed test class failure issue 
|  10/12/2013   Jaypal Nimesh       Backward Arrow  Optimized the class & improved code coverage
======================================================================================================*/
@isTest
public class PRM_DealLeadSearchController_TC {
    
    /* @Method <This method is used for testing the controller methods>.   
    @param - <void>  
    @return <Lead> - <Lead record>   
    @throws exception - <No Exception> 
*/    
    private static testMethod void startTest(){

        System.runAs(new user(Id = UserInfo.getUserId())){
        
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
        }        
        
        Contact cont = new Contact();
        cont.LastName = 'testContact';
        cont.Active__c = true ;
        cont.Email = 'test12@xyx.com' ;

        insert cont;
        
        List<Account> account1 =  AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        List<Account> account2 =  AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        List<Account> AccountList = new List<Account>();
        List<Account> lstDistributorAccount = new List<Account>();
        
        //Modified by Jaypal Nimesh for Backward Arrow Project Dated: 25-10-2013
        AccountList.addAll(account1);
        AccountList.addAll(account2);

        insert AccountList;
        for(account distiacc: AccountList){
            if(distiacc.Partner_Type__c=='Distributor'&& distiacc.Lead_Oppty_Enabled__c==true){
               distiacc.Ispartner=true;
            }
            lstDistributorAccount.add(distiacc);
        }
        Update lstDistributorAccount;
        
        List<Lead> leadlist=LeadTestClassDataHelper.createDealReg(AccountList[4], null, lstDistributorAccount[0], AccountList[3]);
        
        insert leadlist;
        
        for(Lead lead : leadlist){
            lead.DealReg_Deal_Submitter_Contact__c=cont.id;
            lead.DealReg_Expiration_Date__c = system.today() + 30;
            lead.DealReg_Expected_Close_Date__c = system.today() + 60;
            lead.Contract_End_Date__c = system.today() + 15;
            lead.DealReg_Partner_Expected_Close_Date__c = system.today() + 10;
            lead.DealReg_Related_Account_lookup__c = 'TestRelatedAccountLookup';
        }        

        update leadlist;
        
        ApexPages.StandardController ctlr = new ApexPages.standardController(leadlist[0]);
        
        PRM_DealLeadSearchController LeadCtrl = new PRM_DealLeadSearchController(ctlr);
        
        ApexPages.currentPage().getParameters().put('sbstr', '1778294');        
        
        LeadCtrl.getCountryList();
        LeadCtrl.getStatusList();
        LeadCtrl.getLeadList();
        LeadCtrl.leads = leadlist[0];
        LeadCtrl.RAccount = 'TestRAccount';
        LeadCtrl.DistVAR = 'TestDistVAR';
        LeadCtrl.Downer = 'TestDowner';
        LeadCtrl.OppNumber='1111';
        LeadCtrl.DealRegNumber='2222';
        LeadCtrl.isAll = false;
        LeadCtrl.isSearch();
        LeadCtrl.SearchResults();
        LeadCtrl.sortListLeads('name', TRUE);
        LeadCtrl.sortField1 = 'name';
        LeadCtrl.sortAscending1 = true;
        LeadCtrl.runSortAction1();
        LeadCtrl.sortAscending1 = false;
        LeadCtrl.runSortAction1();
        LeadCtrl.sortListLeads('name', FALSE);
        LeadCtrl.searchCriteria = 'TestSearch';
        LeadCtrl.ComponentSearch();
        LeadCtrl.openLink();
        LeadCtrl.BackPage();

        PRM_DealLeadSearchController LeadCtrl2 = new PRM_DealLeadSearchController(ctlr);        
        
        LeadCtrl2.leads = leadlist[0];
        LeadCtrl2.RAccount = 'TestRAccount';
        LeadCtrl2.DistVAR = 'TestDistVAR';
        LeadCtrl2.Downer = 'TestDowner';
        LeadCtrl2.OppNumber='1111';
        LeadCtrl2.DealRegNumber='';
        LeadCtrl2.isAll = false;
        LeadCtrl2.isSearch();
        LeadCtrl2.SearchResults();        
        
        PRM_DealLeadSearchController LeadCtrl3 = new PRM_DealLeadSearchController(ctlr);    
        
        LeadCtrl3.leads = leadlist[0];
        LeadCtrl3.RAccount = 'TestRAccount';
        LeadCtrl3.DistVAR = 'TestDistVAR';
        LeadCtrl3.Downer = 'TestDowner';
        LeadCtrl3.OppNumber='';
        LeadCtrl3.DealRegNumber='2222';
        LeadCtrl3.isAll = false;
        LeadCtrl3.isSearch();
        LeadCtrl3.SearchResults();        
    }
         
    Private static testMethod void startTest1(){
        
        System.runAs(new user(Id = UserInfo.getUserId())){
        
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
        }
        
        Contact cont = new Contact();
        cont.LastName = 'testContact';
        cont.Active__c = true ;
        cont.Email = 'test13@xyz.com' ;
        insert cont;
         
        List<Account> account1 =  AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        List<Account> account2 =  AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        List<Account> AccountList = new List<Account>();
        List<Account> lstDistributorAccount = new List<Account>();
        
        //Modified by Jaypal Nimesh for Backward Arrow Project Dated: 25-10-2013
        AccountList.addAll(account1);
        AccountList.addAll(account2);
        
        insert AccountList;
        
       for(account distiacc: AccountList){
            if(distiacc.Partner_Type__c=='Distributor'&& distiacc.Lead_Oppty_Enabled__c==true){
               distiacc.Ispartner=true;
            }
            lstDistributorAccount.add(distiacc);
        }
        
        Update lstDistributorAccount;
        List<Lead> leadlist=LeadTestClassDataHelper.createDealReg(AccountList[4], null, lstDistributorAccount[0], AccountList[3]);
        
        insert leadlist;
        
        for(Lead lead : leadlist){
            lead.DealReg_Deal_Submitter_Contact__c=cont.id;
            lead.DealReg_Expiration_Date__c = system.today() + 30;
            lead.DealReg_Expected_Close_Date__c = system.today() + 60;
            lead.Contract_End_Date__c = system.today() + 15;
            lead.DealReg_Partner_Expected_Close_Date__c = system.today() + 10;
            lead.DealReg_Related_Account_lookup__c = 'TestRelatedAccountLookup';
        } 
        
        ApexPages.StandardController ctlr = new ApexPages.standardController(leadlist[0]);
        
        PRM_DealLeadSearchController LeadCtrl = new PRM_DealLeadSearchController(ctlr);  
        
        LeadCtrl.leads = leadlist[0];
        LeadCtrl.OppNumber='';
        LeadCtrl.DealRegNumber='';
        LeadCtrl.Country='--None--';
        LeadCtrl.Status='--None--';       
        leadlist[0].DealReg_Expiration_Date__c=null;
        leadlist[0].DealReg_Expected_Close_Date__c=null;
        leadlist[0].Contract_End_Date__c=null;
        leadlist[0].DealReg_Partner_Expected_Close_Date__c=null;
        leadlist[0].Tier_2_Partner__c=null;
        leadlist[0].DealReg_Related_Account_lookup__c=null;
        leadlist[0].DealReg_Deal_Submitter_Contact__c=null;
        leadlist[0].Partner__c=null;
        leadlist[0].phone='2324567';
        update leadlist;
        
        LeadCtrl.isSearch();
        LeadCtrl.SearchResults();
        LeadCtrl.getCountryList();
        LeadCtrl.getStatusList();
        LeadCtrl.getLeadList();
        LeadCtrl.ComponentSearch();
        LeadCtrl.openLink();
        LeadCtrl.BackPage();
        LeadCtrl.getLeadDetails();
        LeadCtrl.setLeadList(leadlist); 
        
        ApexPages.StandardController ctlr1 = new ApexPages.standardController(leadlist[1]);

        PRM_DealLeadSearchController LeadCtrl2 = new PRM_DealLeadSearchController(ctlr1);        
        
        LeadCtrl2.leads = leadlist[1];
        LeadCtrl2.RAccount = 'TestRAccount';
        LeadCtrl2.DistVAR = 'TestDistVAR';
        LeadCtrl2.Downer = 'TestDowner';
        LeadCtrl2.isAll = false;
        LeadCtrl2.isSearch();
        LeadCtrl2.SearchResults();
        LeadCtrl2.OppNumber='1111';
        LeadCtrl2.DealRegNumber='';
        
        ApexPages.StandardController ctlr2 = new ApexPages.standardController(leadlist[1]);
        
        PRM_DealLeadSearchController LeadCtrl3 = new PRM_DealLeadSearchController(ctlr2);    
        
        LeadCtrl3.leads = leadlist[1];
        LeadCtrl3.RAccount = 'TestRAccount';
        LeadCtrl3.DistVAR = 'TestDistVAR';
        LeadCtrl3.Downer = 'TestDowner';
        LeadCtrl3.isAll = false;
        LeadCtrl3.isSearch();
        LeadCtrl3.SearchResults();
        LeadCtrl3.OppNumber='';
        LeadCtrl3.DealRegNumber='2222';
    }
}