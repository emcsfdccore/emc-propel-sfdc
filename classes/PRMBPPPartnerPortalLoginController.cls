/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
|  16/Sep/2014      EMC             BPP WR379763    Created controller class for Partner central login page.
|
+=====================================================================================================*/

global with sharing class PRMBPPPartnerPortalLoginController{
    
    //set username/ password variables via page
    global String username {get; set;}
    public transient String password {get; set;}
    public String selectedLanguage{get; set;}
    public List<SelectOption> selectLanguage { get; set; }
     
    public PRMBPPPartnerPortalLoginController() {

        //Intially Setting Lang to English
        selectedLanguage = 'en_US';

        selectLanguage = new List<SelectOption>();
        //selectLanguage.add(new SelectOption('','--NONE--'));
        
        Map<String,BPP_Login_Language__c> mapLangauage = BPP_Login_Language__c.getall();

        //Creating and adding value to List for sorted Language display
        List<String> lstLangSorted = new List<String>();
        lstLangSorted.addall(mapLangauage.keyset());
        lstLangSorted.sort();

        for(String langName : lstLangSorted){          
            selectLanguage.add(new SelectOption(mapLangauage.get(langName).Language_Code__c , langName));
        }
    }
 
    global PageReference login() {
        
        BusinessPartnerProgram__c bppPortalId = BusinessPartnerProgram__c.getValues('PartnerPortalId');
        BusinessPartnerProgram__c bppPortalUrlPrefix = BusinessPartnerProgram__c.getValues('PartnerPortalUrlPrefix');
        
        //static org-id and portal id      
        String strOrgID = UserInfo.getOrganizationId();     
        String strPortalID = bppPortalId.APInames__c; 
        String strURL = bppPortalUrlPrefix.APInames__c;
        
        //start url of the page
        String startUrl = strUrl + '/secur/login_portal.jsp?orgId=' + strOrgID + '&portalId=' + strPortalID;
 
        startUrl += '&un=' + username;
        startUrl += '&pw='+ password;
 
        //set reference and attempt login
        PageReference portalPage = new PageReference(startUrl);
        portalPage.setRedirect(true);
        
        PageReference p = Site.login(username, password, startUrl);
        
        //if p==null, no login
        if(p == null){
            return Site.login(username, password, null);
        } 
        else{
            return portalPage;
        }
    } 
}