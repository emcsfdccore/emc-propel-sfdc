@istest
public class Case_CompetitorAndPrcingDetails_TC {
    
    
    static Testmethod void compAndpricingDet(){
        
        try{
            CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
            custset.By_Pass_Account_Triggers__c = true;
            custset.By_Pass_Account_Validation_Rules__c = true;
            custset.By_Pass_Opportunity_Triggers__c = true;
            custset.By_Pass_Opportunity_Validation_Rules__c = true;
            insert custset;
            
            Account acc = new Account();
            acc.name = 'test';
            insert acc;
            
            Competitor__c comp = new Competitor__c();
            comp.name='Test';
            comp.Active__c = true;
            comp.Pricing_Calculator__c = 'test';
            
            insert comp;
            
            Competitor_Product__c comprod = new Competitor_Product__c();
            comprod.name='ttest';
            comprod.Active__c = true;
            comprod.Competitor__c = comp.id;
            
            comprod.Competitive_One_Pager__c = 'test';
            comprod.Competitive_One_Pager__c='test';
            comprod.Pricing_Calculator__c = 'test';
            //comprod.CI_Wiki_Link__c = '';
            insert comprod;
            
            
            Opportunity opp = new Opportunity();
            opp.name='test';
            opp.Closed_Reason__c = 'Project Cancelled';
            opp.Competitor__c = 'Actifio';
            opp.Close_Comments__c = 'Test';
            opp.Duplicate_Opportunity__c = opp.ID;
            opp.Competitor_Lost_To__c = 'Amazon';
            opp.Competitor_Product__c = '18000 Series';
            opp.Product_Model__c = '18800F';
            opp.Sales_Channel__c = 'Direct';
            opp.Renewals_Close_Details__c = '3rd Party';
            opp.Closed_Reason_Action__c = 'IB to De-Install';
            opp.Opportunity_Type__c = 'IB to De-Install';
            opp.AccountId= acc.id; 
            //opp.Account.name = acc.id;
            opp.stagename= 'pipeline';
            opp.closedate = system.today();
            insert opp;
            
            case cs = new case();
            cs.Competitor_New__c = comp.ID;
            cs.Competitor_Product_New__c = comprod.ID;
            cs.Opportunity_Name__c = opp.ID;
            insert cs;
            
            
            CaseCompetitorProduct__c casecompprod = new CaseCompetitorProduct__c();
            casecompprod.Competitor__c=comp.ID;
            casecompprod.case__c = cs.id;
            casecompprod.Competitor_Product__c = comprod.ID;
            casecompprod.Competitor_Not_Listed__c='Test';  
            // casecomprod.Is_Created_from_Case__c = true;   
            insert casecompprod;
            
            Test.starttest();
            
            pagereference pageref = page.CaseCompetitorAndProductsPage;
            Test.setCurrentPageReference(pageref);
            //Test.setCurrentPage(pageRef);
            pageref.getParameters().put('delId', casecompprod.id);
            ApexPages.StandardController std = new ApexPages.StandardController(cs) ;
            Case_CompetitorAndPrcingDetails css = new Case_CompetitorAndPrcingDetails(std);
            css.getOppty();
            css.loadWrapperData();
            css.addCaseCompProd();
            //css.deleteCaseCompProd();
            css.editCaseCompProd();
            css.saveCaseCompProd();
            css.caseCancel();
            css.updateOppty( comp.ID , comprod.ID);
            test.stoptest();
            
        }catch(exception ex){}
        
    }  
    
    static testmethod void compAndpricingDelete1(){
        
        try{
            CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
            custset.By_Pass_Account_Triggers__c = true;
            custset.By_Pass_Account_Validation_Rules__c = true;
            custset.By_Pass_Opportunity_Triggers__c = true;
            custset.By_Pass_Opportunity_Validation_Rules__c = true;
            insert custset;
            
            Account acc = new Account();
            acc.name = 'test';
            insert acc;
            
            Competitor__c comp = new Competitor__c();
            comp.name='Test';
            comp.Active__c = true;
            comp.Pricing_Calculator__c = 'test';
            
            insert comp;
            
            Competitor_Product__c comprod = new Competitor_Product__c();
            comprod.name='ttest';
            comprod.Active__c = true;
            comprod.Competitor__c = comp.id;        
            comprod.Competitive_One_Pager__c = 'test';
            comprod.Competitive_One_Pager__c='test';
            comprod.Pricing_Calculator__c = 'test';
            //comprod.CI_Wiki_Link__c = '';
            insert comprod;
            
            
            
            Opportunity opp = new Opportunity();
            opp.name='test';
            opp.Closed_Reason__c = 'Project Cancelled';
            opp.Competitor__c = 'Actifio';
            opp.Close_Comments__c = 'Test';
            opp.Duplicate_Opportunity__c = opp.ID;
            opp.Competitor_Lost_To__c = 'Amazon';
            opp.Competitor_Product__c = '18000 Series';
            opp.Product_Model__c = '18800F';
            opp.Sales_Channel__c = 'Direct';
            opp.Renewals_Close_Details__c = '3rd Party';
            opp.Closed_Reason_Action__c = 'IB to De-Install';
            opp.Opportunity_Type__c = 'IB to De-Install';
            opp.AccountId= acc.id; 
            //opp.Account.name = acc.id;
            opp.stagename= 'pipeline';
            opp.closedate = system.today();
            insert opp;
            
            case cs = new case();
            cs.Competitor_New__c = comp.ID;
            //cs.Competitor_Product_New__c = comprod.ID;
            cs.Opportunity_Name__c = opp.ID;
            insert cs;
            
            
            CaseCompetitorProduct__c casecompprod = new CaseCompetitorProduct__c();
            casecompprod.Competitor__c=comp.ID;
            casecompprod.case__c = cs.id;
            casecompprod.Competitor_Product__c = comprod.ID;
            casecompprod.Competitor_Not_Listed__c='Test';  
            // casecomprod.Is_Created_from_Case__c = true;   
            insert casecompprod;
            
            Test.starttest();
            
            pagereference pageref = page.CaseCompetitorAndProductsPage;
            Test.setCurrentPage(pageRef);
            pageref.getParameters().put('delId', casecompprod.id);
            ApexPages.StandardController std = new ApexPages.StandardController(cs) ;
            Case_CompetitorAndPrcingDetails css = new Case_CompetitorAndPrcingDetails(std);
            css.loadWrapperData();
            Id delId_TEST = ApexPages.currentPage().getParameters().get('delId');
            system.debug('The CaseCompetitorProduct__c to be deleted:::'+casecompprod);
            system.debug('The paramter for delete :::'+delId_TEST);
            system.debug('The loaded values are:::'+css.cPWrapList);
            css.deleteCaseCompProd();
            
            test.stoptest();
        }catch(exception ex){}            
    }
    
    static testmethod void compAndpricingDelete_M3(){
        
        try{
            CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
            custset.By_Pass_Account_Triggers__c = true;
            custset.By_Pass_Account_Validation_Rules__c = true;
            custset.By_Pass_Opportunity_Triggers__c = true;
            custset.By_Pass_Opportunity_Validation_Rules__c = true;
            insert custset;
            
            Account acc = new Account();
            acc.name = 'test';
            insert acc;
            
            Competitor__c comp = new Competitor__c();
            comp.name='Test';
            comp.Active__c = true;
            comp.Pricing_Calculator__c = 'test';
            
            insert comp;
            
            Competitor_Product__c comprod = new Competitor_Product__c();
            comprod.name='ttest';
            comprod.Active__c = true;
            comprod.Competitor__c = comp.id;        
            comprod.Competitive_One_Pager__c = 'test';
            comprod.Competitive_One_Pager__c='test';
            comprod.Pricing_Calculator__c = 'test';
            //comprod.CI_Wiki_Link__c = '';
            insert comprod;
            
            
            
            Opportunity opp = new Opportunity();
            opp.name='test';
            opp.Closed_Reason__c = 'Project Cancelled';
            opp.Competitor__c = 'Actifio';
            opp.Close_Comments__c = 'Test';
            opp.Duplicate_Opportunity__c = opp.ID;
            opp.Competitor_Lost_To__c = 'Amazon';
            opp.Competitor_Product__c = '18000 Series';
            opp.Product_Model__c = '18800F';
            opp.Sales_Channel__c = 'Direct';
            opp.Renewals_Close_Details__c = '3rd Party';
            opp.Closed_Reason_Action__c = 'IB to De-Install';
            opp.Opportunity_Type__c = 'IB to De-Install';
            opp.AccountId= acc.id; 
            //opp.Account.name = acc.id;
            opp.stagename= 'pipeline';
            opp.closedate = system.today();
            insert opp;
            
            case cs = new case();
            cs.Competitor_New__c = comp.ID;
            //cs.Competitor_Product_New__c = comprod.ID;
            cs.Opportunity_Name__c = opp.ID;
            insert cs;
            
            List<CaseCompetitorProduct__c> caseCompList = new List<CaseCompetitorProduct__c>();
            CaseCompetitorProduct__c casecompprod = new CaseCompetitorProduct__c();
            casecompprod.Competitor__c=comp.ID;
            casecompprod.case__c = cs.id;
            //casecompprod.Competitor_Product__c = comprod.ID;
            casecompprod.Competitor_Not_Listed__c='Test';  
            // casecomprod.Is_Created_from_Case__c = true;   
            //insert casecompprod;
            caseCompList.add(casecompprod);
            
            CaseCompetitorProduct__c casecompprod2 = new CaseCompetitorProduct__c();
            casecompprod2.Competitor__c=comp.ID;
            casecompprod2.case__c = cs.id;
            casecompprod2.Competitor_Product__c = comprod.ID;
            casecompprod2.Competitor_Not_Listed__c='Test';  
            // casecomprod.Is_Created_from_Case__c = true;   
            //insert casecompprod;     
            caseCompList.add(casecompprod2);
            system.Database.insert(caseCompList);
            
            
            
            Test.starttest();
            
            pagereference pageref = page.CaseCompetitorAndProductsPage;
            Test.setCurrentPage(pageRef);
            pageref.getParameters().put('delId', casecompprod.id);
            ApexPages.StandardController std = new ApexPages.StandardController(cs) ;
            Case_CompetitorAndPrcingDetails css = new Case_CompetitorAndPrcingDetails(std);
            css.loadWrapperData();
            Id delId_TEST = ApexPages.currentPage().getParameters().get('delId');
            system.debug('The CaseCompetitorProduct__c to be deleted:::'+casecompprod);
            system.debug('The paramter for delete :::'+delId_TEST);
            system.debug('The loaded values are:::'+css.cPWrapList);
            css.deleteCaseCompProd();
            
            test.stoptest();
        }catch(exception ex){}
    }
    
    
    static Testmethod void compAndpricingDet_M4(){
        try{
            CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
            custset.By_Pass_Account_Triggers__c = true;
            custset.By_Pass_Account_Validation_Rules__c = true;
            custset.By_Pass_Opportunity_Triggers__c = true;
            custset.By_Pass_Opportunity_Validation_Rules__c = true;
            insert custset;
            
            Account acc = new Account();
            acc.name = 'test';
            insert acc;
            
            Competitor__c comp = new Competitor__c();
            comp.name='Test';
            comp.Active__c = true;
            comp.Pricing_Calculator__c = 'test';
            
            insert comp;
            
            List<Competitor_Product__c> competitorProdList = new List<Competitor_Product__c>();
            Competitor_Product__c comprod = new Competitor_Product__c();
            comprod.name='ttest';
            comprod.Active__c = true;
            comprod.Competitor__c = comp.id;        
            comprod.Competitive_One_Pager__c = 'test';
            comprod.Competitive_One_Pager__c='test';
            comprod.Pricing_Calculator__c = 'test';
            //comprod.CI_Wiki_Link__c = '';
            //insert comprod;
            competitorProdList.add(comprod);
            
            Competitor_Product__c comprod_NotListed = new Competitor_Product__c();
            comprod_NotListed.name='Not Listed';
            comprod_NotListed.Active__c = true;
            comprod_NotListed.Competitor__c = comp.id;
            
            comprod_NotListed.Competitive_One_Pager__c = 'Not Listed';
            comprod_NotListed.Competitive_One_Pager__c='Not Listed';
            comprod_NotListed.Pricing_Calculator__c = 'Not Listed';
            //comprod.CI_Wiki_Link__c = '';
            //insert comprod_NotListed;
            competitorProdList.add(comprod_NotListed);
            System.Database.insert(competitorProdList);
            
            
            Opportunity opp = new Opportunity();
            opp.name='test';
            opp.Closed_Reason__c = 'Project Cancelled';
            opp.Competitor__c = 'Actifio';
            opp.Close_Comments__c = 'Test';
            opp.Duplicate_Opportunity__c = opp.ID;
            opp.Competitor_Lost_To__c = 'Amazon';
            opp.Competitor_Product__c = '18000 Series';
            opp.Product_Model__c = '18800F';
            opp.Sales_Channel__c = 'Direct';
            opp.Renewals_Close_Details__c = '3rd Party';
            opp.Closed_Reason_Action__c = 'IB to De-Install';
            opp.Opportunity_Type__c = 'IB to De-Install';
            opp.AccountId= acc.id; 
            //opp.Account.name = acc.id;
            opp.stagename= 'pipeline';
            opp.closedate = system.today();
            insert opp;
            
            case cs = new case();
            cs.Competitor_New__c = comp.ID;
            cs.Competitor_Product_New__c = comprod.ID;
            cs.Opportunity_Name__c = opp.ID;
            insert cs;
            
            
            CaseCompetitorProduct__c casecompprod = new CaseCompetitorProduct__c();
            casecompprod.Competitor__c=comp.ID;
            casecompprod.case__c = cs.id;
            casecompprod.Competitor_Product__c = comprod.ID;
            casecompprod.Competitor_Not_Listed__c='Test';  
            // casecomprod.Is_Created_from_Case__c = true;   
            insert casecompprod;
            
            pagereference pageref = page.CaseCompetitorAndProductsPage;
            pageref.getParameters().put('delId', casecompprod.id);
            
            Test.starttest();
            ApexPages.StandardController std = new ApexPages.StandardController(cs) ;
            Case_CompetitorAndPrcingDetails css = new Case_CompetitorAndPrcingDetails(std);
            
            css.getOppty();
            css.loadWrapperData();
            css.addCaseCompProd();
            for(Case_CompetitorAndPrcingDetails.compProdWrap x : css.cPWrapList){
                system.debug('The Case_CompetitorAndPrcingDetails.compProdWrap is ::::'+x);
                if(x.cpid == null){
                    //x.cp.Competitor_Not_Listed__c = 'Not Listed'; 
                    x.cp.Competitor__c = comprod_NotListed.id; 
                }
            }
            css.saveCaseCompProd();
            
            css.loadWrapperData();
            css.addCaseCompProd();
            for(Case_CompetitorAndPrcingDetails.compProdWrap x : css.cPWrapList){
                system.debug('The Case_CompetitorAndPrcingDetails.compProdWrap is ::::'+x);
                if(x.cpid == null){
                    x.cp.Competitor_Not_Listed__c = 'Not Listed'; 
                    //x.cp.Competitor__c = comprod_NotListed.id; 
                }
            }
            css.saveCaseCompProd();            
            
            css.isFirstRecord = false;
            test.stoptest();
        }catch(exception ex){}
        
    }  
    
}