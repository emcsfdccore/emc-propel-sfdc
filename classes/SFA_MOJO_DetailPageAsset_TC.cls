/*==================================================================================================================+

 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        =========== 
 |  19/03/2014    Srikrishna SM   		This Test class is used for Pricing Requests Sharing(Modified this test class to implement
 | 										Best Practices)
 +==================================================================================================================**/
@isTest
private class SFA_MOJO_DetailPageAsset_TC {

      static testMethod void RelatedAssetTest(){
      	//CustomSettingDataHelper
        try{
        	
        	System.runAs(new user(Id = UserInfo.getUserId())){
	            CustomSettingDataHelper.dataValueMapCSData();
	            CustomSettingDataHelper.bypassLogicCSData();
	            CustomSettingDataHelper.eBizSFDCIntCSData();
	            CustomSettingDataHelper.profilesCSData();
	            CustomSettingDataHelper.countryTheaterMapCSData();
			}
			test.startTest();
            //Insert Account
            
			Account acc = AccountAndProfileTestClassDataHelper.createSimpleAccount();
			insert acc;
			                   
			Opportunity opp = new Opportunity(Name = 'Test Opp', Closed_Reason__c = 'Loss', StageName = 'Upside', CloseDate = Date.today()+10);
			insert opp;
			
            List<Opportunity> lstOpportunityUpside = [Select id, name, Closed_Reason__c, Close_Comments__c, CloseDate 
                From Opportunity
                Where Id =: opp.Id limit 1];

            Opportunity oppNormal;
            Asset__c asset1 = new Asset__c(name = 'Test 1', Customer_Name__c = acc.id);
            insert asset1; 

			if (lstOpportunityUpside != null && lstOpportunityUpside.size() > 0){
                oppNormal = lstOpportunityUpside.get(0);
            }

            Opportunity_Asset_Junction__c oaj = new Opportunity_Asset_Junction__c(Related_Asset__c = asset1.id, Related_Opportunity__c = oppNormal.id);
            insert oaj;

            ApexPages.currentpage().getParameters().put('Id',oppNormal.id);
            ApexPages.StandardController con = new ApexPages.StandardController(oppNormal);
            DetailPageAsset relatedController = new DetailPageAsset(con);
            List<Asset__c> lstAsts = relatedController.getOppz();
            test.stopTest();
        }
        catch(Exception e)
        {
            
        }
    }
}