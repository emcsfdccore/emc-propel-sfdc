/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE       DEVELOPER       WR       DESCRIPTION                                
|
   ====       =========       ==       ===========  
|  18-Apr-2013  Prachi                 Created 
+===========================================================================*/ 
@isTest(SeeAllData=true)
private Class beforeUpdatePartnerLocation_Trigger_TC{

    public static testmethod void beforeUpdatePartnerLocation_Trigger(){
        Partner_Location__c objPar= new Partner_Location__c();

            List<Account> accList = new List<Account>();
            for(integer i=0 ; i< 2; i++){
                Account acc = new Account();
                  if(i==0){
                acc.Name='ABC'+i;
                accList.add(acc);
                }
                if(i==1){
                acc.Name='PQR';
                accList.add(acc);
                }
            }
            insert accList;

            Contact Con = new Contact();     
            Con.Salutation = 'Mr';
            Con.FirstName = 'Jayaraju';
            Con.LastName = 'Nulakachandanam';
            Con.AccountId = accList[1].id;
            insert Con;

            objPar.Account__c=accList[0].id;
            objPar.eBus_Location_Enabled__c=TRUE;
            objPar.Country__c='NETHERLANDS (NL)';
            insert objPar;
            
            objPar.eBus_Location_Enabled__c=FALSE;
            update objPar;
            
            objPar.eBus_Location_Enabled__c=TRUE;
            update objPar;
            
        }  
    }