/*==================================================================================================================+                                                                          
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        ===========  
 |  Feb/11/2014    Bisna V P    CI 185  Created this batch class to archive list of articles by executing batch job   
 |  Jul/10/2014    Bisna V P    CI 974  Updated code to Productionalize Archive/Delete solution 
 |  Sep/15/2014    Bisna V P    CI 1263 Updated to Do a cleanup of all the associated objects to an article when the article is deleted                                                     
+==================================================================================================================**/
global class KBbatchdeleteArticle implements Database.Batchable<sObject>
{
    public string query='';
    
    Global List<KnowledgeArticleVersion> listKAV ;   
    Global List<KnowledgeArticleVersion> listKAVArchive ;   
    public List<KB_Batch_Delete_Article__c> listBatchListArticle;   
    public List<String> articleNumbers;
    public Map<String,KnowledgeArticleVersion>  updateKBBatchArchive;    
    public List<KB_Batch_Delete_Article__c> updateBatchListArticle;
    //////
    Global List<Article_Summary__c> listArtSum ;
    Global List<Article_Rating__c> listArtRating ;
    Global List<OPT_Bug__c> listOPTBug ;
    public Map<String,KB_Batch_Delete_Article__c> batchdelete; 
    global List<KB_Batch_Delete_Article__c> listBatchListArticletoDelete;
    public Map<String,KnowledgeArticleVersion>  updateKBBatchDelete;
    ///////////

    global KBbatchdeleteArticle(){ 
    
            articleNumbers = new List<String>(); 
             updateKBBatchArchive = new Map<String,KnowledgeArticleVersion>(); 
            updateBatchListArticle = new List<KB_Batch_Delete_Article__c>();
            ///////
            batchdelete = new Map<String,KB_Batch_Delete_Article__c> ();
            updateKBBatchDelete = new Map<String,KnowledgeArticleVersion>();
            ////////
            // updated for CI WR#974
            listBatchListArticle = [SELECT Id,Article_Number_Padded__c,Article_Number__c,Status__c from KB_Batch_Delete_Article__c where (Status__c != 'Archived' AND Status__c != 'Deleted')];
            //
        listBatchListArticletoDelete = [SELECT Id,Article_Number_Padded__c,Article_Number__c,Status__c from KB_Batch_Delete_Article__c where ( Status__c = 'Waiting for deletion')];
            
            if(listBatchListArticle!= null && listBatchListArticle.size()>0)
            for(KB_Batch_Delete_Article__c deleteArt:listBatchListArticle){         
            articleNumbers.add(deleteArt.Article_Number_Padded__c);
            }
        ///
            if(listBatchListArticletoDelete!= null && listBatchListArticletoDelete.size()>0)
            for(KB_Batch_Delete_Article__c deleteArt:listBatchListArticletoDelete){         
            batchdelete.put(deleteArt.Article_Number_Padded__c,deleteArt);
            }
            System.debug('batchdelete------->'+batchdelete);
        ///
            listKAVArchive =[SELECT id,KnowledgeArticleId,ArticleNumber,LastModifiedDate FROM KnowledgeArticleVersion WHERE PublishStatus ='Archived' and Language ='en_US' and (ArticleNumber in : articleNumbers)];      
                    
            listKAV =[SELECT id,KnowledgeArticleId,ArticleNumber,LastModifiedDate FROM KnowledgeArticleVersion WHERE PublishStatus ='Draft' and Language ='en_US' and (ArticleNumber in : articleNumbers)];        
            
                List<ProcessInstanceWorkitem> wrkItemLst = [Select Id, ProcessInstanceId, ProcessInstance.Status,ProcessInstance.TargetObjectId from ProcessInstanceWorkitem
                 where ProcessInstance.TargetObjectId in:listKAV and ProcessInstance.Status='Pending']; 
                
                System.debug('wrkItemLst--->'+wrkItemLst);
    
              for(ProcessInstanceWorkitem wrkItemObj :wrkItemLst){
                
                 Approval.ProcessWorkitemRequest itemReq = new Approval.ProcessWorkitemRequest();
                itemReq.setComments('Rejected from batch');
                itemReq.setAction('Reject');
                itemReq.setWorkitemId(wrkItemObj.id);
                Approval.ProcessResult result = Approval.process(itemReq);
            }
                                            
            try{
            for(KnowledgeArticleVersion draft : listKAV){                            
                  KbManagement.PublishingService.deleteDraftArticle(draft.KnowledgeArticleId);
                   //updateKBBatchDelete.put(draft.ArticleNumber,draft);
             } 
             //Changes for WR 1263
            listArtSum = [select id,Article_Number__c from Article_Summary__c where Article_Number__c in : articleNumbers];
            listArtRating = [select id, Article_Number__c from Article_Rating__c where Article_Number__c in : articleNumbers];
            listOPTBug = [select id, Article_Number__c from OPT_Bug__c where Article_Number__c in : articleNumbers];
                                             
                         
            }
            catch(Exception e){
            System.debug(e);
            }   
            
            query ='SELECT id,ArticleNumber,KnowledgeArticleId,LastModifiedDate FROM KnowledgeArticleVersion WHERE   PublishStatus =\'Online\' and Language =\'en_US\' and (ArticleNumber in : articleNumbers)';           
    }        
    global Database.QueryLocator start(Database.BatchableContext BC){                         
        Database.QueryLocator db = Database.getQueryLocator(query);
        return db;        
    }   
    global void execute(Database.BatchableContext BC, List<KnowledgeArticleVersion> scope){ 
         try{                     
             for(KnowledgeArticleVersion a : scope){               
                  KbManagement.PublishingService.archiveOnlineArticle(a.KnowledgeArticleId,null);
                  
                  if(batchdelete.get(String.valueOf(a.ArticleNumber))!= null)
                  {
                    KbManagement.PublishingService.deleteArchivedArticle(a.KnowledgeArticleId);
                    updateKBBatchDelete.put(a.ArticleNumber,a);
                  }
                  else
                     updateKBBatchArchive.put(a.ArticleNumber,a);                  
             } 
             for(KnowledgeArticleVersion arch : listKAVArchive){                            
                  KbManagement.PublishingService.deleteArchivedArticle(arch.KnowledgeArticleId);
                   updateKBBatchDelete.put(arch.ArticleNumber,arch);
             } 
             
                for(KB_Batch_Delete_Article__c artNo: listBatchListArticle){
                if( updateKBBatchArchive.get(String.valueOf(artNo.Article_Number_Padded__c))!= null && (updateKBBatchDelete.get(String.valueOf(artNo.Article_Number_Padded__c))== null)){
                  KB_Batch_Delete_Article__c kbd = new KB_Batch_Delete_Article__c();
                  kbd.id= artNo.id;
                  kbd.Article_Number__c = artNo.Article_Number__c;
                  kbd.Status__c= 'Archived';                                   
                  updateBatchListArticle.add(kbd);
                  }
             }
                Update updateBatchListArticle;
                
              for(KB_Batch_Delete_Article__c artNo: listBatchListArticletoDelete){
                if( updateKBBatchDelete.get(String.valueOf(artNo.Article_Number_Padded__c))!= null){
                  KB_Batch_Delete_Article__c kbd = new KB_Batch_Delete_Article__c();
                  kbd.id= artNo.id;
                  kbd.Article_Number__c = artNo.Article_Number__c;
                  kbd.Status__c= 'Deleted';                                   
                  updateBatchListArticle.add(kbd);
                  }
             }
             
             
                    
             Update updateBatchListArticle;          
             delete listArtSum;
             delete listArtRating;
             delete listOPTBug;
             //////////////////////////
         }
         catch(Exception e){
            System.debug(e);
         }
    } 
    global void finish(Database.BatchableContext BC){
    
    
    }
}