/*==================================================================================================================+

 |  HISTORY  |                                                                            

 |  DATE          DEVELOPER      WR        DESCRIPTION                              

 |  ====          =========      ==        =========== 
    27/11/2013    Garima Shukla  R2R-R4    Link Unlink Validations Line 146 
 |  24/06/2013    Anirudh Singh  R2R       To Link Assets To Lead                       
 |  24/02/2014    Srikrishna SM  FR 9639   Filter Logic is now replaced with GU  
 |  04/03/2014    Srikrishna SM  FR-9639   Added Pagination functionality on results
 |  07/03/2014    Srikrishna SM  FR - #    Added restriction for 'EMC Contract Renewals User' profile user from linking a Lead to asset
 |  29/04/2014    Sneha Jain     R2R-May14 Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
 |  09/07/2014    Vivek Barange  WR#1006   Modified the code to implement Asset filter functionality in page
 |  09/13/2014    Sneha Jain     CDC Demand Req - 1345 - R2R Nov'14 Rel - Added a limit of 2K (as required) to the queryLocator as Salesforce cannot return more than 10K records
 |  19/02/2015    Vinod Jetti    #1649      Replaced field 'Global_DUNS_Entity__c'to 'Global_DUNS_Entity_Hub__c'
 +==================================================================================================================*/

public with sharing class R2R_LeadLink_Controller {
    public String retURL='/a3S/o';
    public Account accountObj {get;set;}
    public Account CustomerProfileAccount {get;set;}
    public lead leadObj {get;set;}
    public lead_Asset_Junction__c leadlink {get;set;}
    public List<AssetListItem > AssetList {get;set;} 
    public List<Asset__c> selectedAssets {get;set;} 
    public Boolean isSelected {get;set;}
    public Boolean isListEmpty {get;set;}
    public Boolean renderAssetSection {get;set;}
    public Boolean renderLeadSection {get;set;}
    public List<Account> listAccount   {get;set;}
    public List<Asset__c> listAsset   {get;set;} 
    public List<Lead> leadSearchResults {get;set;}     
    public id selectedAccountId {get;set;}
    public List<string> lstSelectedAccount{get;set;}
    public List<string> lstSelectedAccId{get;set;}
    public set<id> setSelectedAssetId{get;set;}
    public Integer intErrored {get;set;} {intErrored = 0;}
    public List<Lead_Asset_Junction__c> lstLeadJnObj = new List<Lead_Asset_Junction__c>();  
    public List<Opportunity_Asset_Junction__c> lstOpptyJnObj = new List<Opportunity_Asset_Junction__c>();
    Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
    public Boolean flag{set;get;}
    
    //Properties for Lead Lookup
    public String strInputCode { get; set; }
    public string accdistrictName {get; set;}
    public string accdistrictNameencoded {get; set;}
    public string accGlobalDUNS;
    public String selectedLead;
    public string leadId{get;set;}
    public List<Lead> list_MasterLeadSearchResults { get; set; } 
    public List<Asset__c> list_leadofsameaccdistrict{get; set;}
    public Boolean showClearList{get;set;}
    string str1 ='' ;   
    public string assetaccid{get;set;}
    asset__c assetobj = new asset__c();
    public  map<Id,Lead> mapLead{ get; set; } 
   
    //Commented existing Pagination implementation for R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
    /*
    //Added for Pagination - Start - FR-9639
    public PageManager objPageManager {get;set;}
    //Added for Pagination - End
    
    //StandardSetController Constructor - Added for Pagination - FR-9639
    public R2R_LeadLink_Controller(ApexPages.StandardSetController controller) { 
    objPageManager = new PageManager(25);
   }*/
  
    //Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
    public List<AssetListItem> assetWrapperlist = new List<AssetListItem>();
    public List<List<AssetListItem>> listOfAssetWrapperlist = new List<List<AssetListItem>>();
    public List<Asset__c> assetsToAddList = new List<Asset__c>();
    
    /*WR#1006 - To store customer asset name*/
    public String customerAssetName {get;set;}
    
    public R2R_LeadLink_Controller() {
        flag = false;     
        //objPageManager = new PageManager(25);   
        string deletedStatus = 'Deleted;Displaced by EMC;Displaced by Competitor';
        List<Account> accountrelatedToAsset = new List<Account>();
        isListEmpty = false;
        renderLeadSection = false;
        renderAssetSection =false; 
        string nonactionablestatus = data.get('R2R Non-Actionable Asset Status').datavalue__c;
        string actionablestatus= data.get('R2R Actionable Status').datavalue__c;
        string rendersection = Apexpages.currentPage().getParameters().get('rendersection');        
        string accdist = Apexpages.currentPage().getParameters().get('accDist');
        string assetId = Apexpages.currentPage().getParameters().get('assetid');
        string leadToFetch= Apexpages.currentPage().getParameters().get('selLead');
        string leadIdFromURL= Apexpages.currentPage().getParameters().get('leadId');
        System.Debug('accdist ->' +accdist );
        
        //Added for restricting access to 'EMC Contract Renewals User' Profile from linking an Lead to Asset
        User u = [select Id,Name, ProfileID from User WHERE id = :UserInfo.getUserId()];
        Profile pr = [SELECT Id, Name FROM Profile WHERE id = :u.ProfileID];
        if(pr.Name == System.Label.R2R_Restricted_Profile_Name){
          flag = true;  
        }
        if(flag){
            ApexPages.Message myMsg;
            myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.R2R_Insufficient_Privilege_Error);
            ApexPages.addMessage(myMsg);            
        }
       //Added for restricting access to Profile from linking an Lead to assets
        
        if(accdist !=null){
            accdist = EncodingUtil.urlDecode(accdist,'UTF-8');
        }
        System.Debug('accdist ->' +accdist );
        if(assetId !=null){
            assetobj = [select id,name,Install_Base_Status__c,Customer_Name__r.Account_District__c from asset__c where id =:assetId];
        }    
        AssetList = new List<AssetListItem>();
        listAsset = new List<Asset__c>();
        System.debug('rendersection'+rendersection);
        if(rendersection =='lead'){
            renderLeadSection = true;
        }
        else if(rendersection =='asset'){
            renderAssetSection = true; 
        }
        assetaccid=Apexpages.currentPage().getParameters().get('accid');
       
        if(assetobj !=null){
            if(assetobj.Install_Base_Status__c !=null){
                if(deletedStatus.contains(assetobj.Install_Base_Status__c)){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Deleted_Assets);
                    ApexPages.addMessage(msg);
                    intErrored++; 
                    renderLeadSection =false;
                }
                if(!actionablestatus.contains(assetobj.Install_Base_Status__c)){                        
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.R2R_Non_Actionable_Asset);
                    ApexPages.addMessage(msg);
                    intErrored++; 
                    renderLeadSection =false;
                    isListEmpty=true;
                }
                //FR-9639
                /*
                if(assetobj.Customer_Name__r.Account_District__c == null || assetobj.Customer_Name__r.Account_District__c == ''){                        
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.R2R_Assets_Without_Account_District);
                    ApexPages.addMessage(msg);
                    intErrored++; 
                    renderLeadSection =false;
                    isListEmpty=true;
                }
                */
            }
        }
        if(renderAssetSection && (assetaccid==null || assetaccid =='')){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_No_Account_On_Lead);
            ApexPages.addMessage(msg);
            intErrored++; 
        }
        if(assetaccid !=null && assetaccid !=''){
            accountrelatedToAsset = [Select Id,Account_District__c,Global_DUNS_Entity_Hub__c from account where id =: assetaccid];
            if(!accountrelatedToAsset.isEmpty()){
                accdistrictName = accountrelatedToAsset[0].Account_District__c;
                //FR-9639
                accGlobalDUNS = accountrelatedToAsset[0].Global_DUNS_Entity_Hub__c;  
           }
           
            if(accdistrictName!=null){
                accdistrictNameencoded = EncodingUtil.urlEncode(accdistrictName,'UTF-8');
            }    
        }
        //FR-9639
        //if(accdist!=null || accdistrictName!=null){
        if(accdist!=null || accGlobalDUNS!=null){
            /*if(accdist!=null){
                accdistrictName = accdist;
            } 
            */   
            //FR-9639
            //if(leadToFetch!=null && accdistrictName !=null){
            if(leadToFetch!=null && accGlobalDUNS !=null){
                if(leadToFetch.length()>0){
                    leadToFetch = '%'+leadToFetch+'%';
                    //FR-9639
                    //leadSearchResults  = R2R_Utility.getLeads(accdistrictName,leadToFetch);
                    leadSearchResults  = R2R_Utility.getLeads(accGlobalDUNS,leadToFetch);
                }   
                else{
                    /*if(accdistrictName !=null) { 
                        leadSearchResults  = R2R_Utility.getLeads(accdistrictName); 
                    } */
                    //FR-9639   
                    if(accGlobalDUNS !=null) { 
                        leadSearchResults  = R2R_Utility.getLeads(accGlobalDUNS); 
                    }    
                }  
            }
           
            /*if(accdistrictName !=null) { 
                listAsset = R2R_Utility.getAssets(accdistrictName);
            }*/
            //Commented existing Pagination implementation for R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
            //FR-9639
            /*
            if(accGlobalDUNS !=null) { 
                listAsset = R2R_Utility.getAssets(accGlobalDUNS);
            }*/
        }
      
        // Get the account district of lead and check if it is empty.......
        Lead listoflead=(Lead)[Select id,Related_Account__c,Related_Account__r.Account_District__c from Lead where id=:leadIdFromURL];
        String acconlead=listoflead.Related_Account__c;
        String accdistrictoflead=listoflead.Related_Account__r.Account_District__c;
        if(acconlead!=null){
            //FR-9639
            /*
            if (accdistrictoflead==null || accdistrictoflead==''){      
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Account_District_Cannot_Be_Blank);
                ApexPages.addMessage(msg);
                intErrored++;
                renderAssetSection = false;
                islistEmpty=true;
            }
           */
        }
        // End of account district check on lead.............................

        /*System.Debug('Accountdistrict***************' +accdistrictName);
        System.Debug('accdist***************' +accdist);
        if(accdistrictName==null){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Account_District_Cannot_Be_Blank);
            ApexPages.addMessage(msg);
            intErrored++;
            renderAssetSection = false;
            islistEmpty=true;
        }*/

        //added so that the error is only thrown when account is populated on lead.
       
        //Commented existing Pagination implementation for R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
        /*
        if(listAsset.isempty() && renderAssetSection){
            renderAssetSection = false;
            islistEmpty=true;
            ApexPages.Message myMsg;
            myMsg = new ApexPages.Message(ApexPages.Severity.INFO,System.Label.R2R_No_Assets_On_Lead);
            ApexPages.addMessage(myMsg);
        }
       
        for(Asset__c ast: listAsset)
        {
            AssetList.add(new AssetListItem(ast));   
        }
        System.Debug('assetObj--->' +assetObj);
        if(assetObj!=null && assetId !='' && rendersection=='lead'){
            AssetListItem wrapper = new AssetListItem(assetObj);
            wrapper.selected= true;
            AssetList.add(wrapper);
        }    
        System.debug('AssetList--->'+AssetList);
        */
        leadlink = new lead_Asset_Junction__c();             
        String strLead = ApexPages.currentPage().getParameters().get('retURL');
        System.debug('strOpp  :'+strLead);        
        //  String OpportunityId ='';       
        leadId ='';
        if(strLead !=null){
            if (strLead.indexof('leadId')!=-1){
                integer index =strLead.indexof('leadId');
                System.debug('index '+index);
                leadId=strLead.substring(index +14);
                leadlink.Related_Lead__c=leadId;
            }
        }     
        else if(leadIdFromURL !=null){
            leadlink.Related_Lead__c=leadIdFromURL;
        }   
            
        System.Debug('leadIdFromURL--->' +leadIdFromURL );
        System.debug('renderLeadSection '+renderLeadSection);
        System.debug('renderLeadSection '+renderAssetSection);
        System.Debug('list_LeadSearchResults'+leadSearchResults);
       
        //Commented existing Pagination implementation for R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
        //Added for Pagination - Start - FR-9639
        //objPageManager.numberOfRows = AssetList.size();
        //Added for Pagination - End
   
        // mapLead = new map<Id,Lead>([Select Id,Name,Related_Account__r.Account_District__c from Lead where Related_Account__r.Account_District__c =:assetaccid limit 100]);
        //SearchLead();
         
        //R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
        setCon.setPageSize(25);
        Integer reminder = math.mod(setCon.getResultSize(),setCon.getPageSize());
        Integer listToCreateSize = (reminder == 0?(setCon.getResultSize()/setCon.getPageSize()):(setCon.getResultSize()/setCon.getPageSize())+1);
        for(Integer i=0; i < listToCreateSize ; i++){
            listOfAssetWrapperlist.add(new List<AssetListItem>());
        }       
    } 
    
    //R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {
            
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id ,Target_Zone__c,Red_Zone_Priority__c,Install_City__c,Install_Base_Status__c,Contract_End_Date__c,Sales_Plan__c,Maintenance_Engagement_Status1__c,Lease_Exp_Date__c,Model__c,Install_Date__c,Customer_Name__r.Party_Number__c,Customer_Name__r.Customer_Profiled_Account_Lookup__c,Custom_Asset_Name__c,Serial_Number__c,Name,Disposition_Status__c ,Account_Record_Type__c ,RecordType.Name ,Product_Name_Vendor__c,Product_Family__c,Customer_Name__c,Contract_Number__c 
                                        from Asset__c where Customer_Name__r.Global_DUNS_Entity_Hub__c=: accGlobalDUNS and Install_Base_Status__c not in('Deleted','Displaced by EMC','Displaced by Competitor','Deinstall - Cust Site') and RecordType.DeveloperName!='EMC_Install_Inactive' Order by Name limit 2000]));
            }
            return setCon;
        }
        set;
    }
    
    //Commented existing Pagination implementation for R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
    /*
    //Added for Pagination - Start
    public List<AssetListItem> getListOfAssets(){
    List<AssetListItem> data = new List<AssetListItem>();   
        for(integer i=objPageManager.startIndex;i<objPageManager.endIndex&&i<AssetList.size();i++)
        {
            data.add(AssetList.get(i));
        }
        return data;
    }
    //Added for Pagination - End
    */
    
    /* This Method is to clear the List when clear search items link is clicked*/  
    /*public PageReference clearData(){     
           list_LeadSearchResults.clear();   
           showClearList = false;        
           list_LeadSearchResults = list_MasterLeadSearchResults;    
           return null; 
         }    
    
    /* This is the search Functionlity which will fetch lead based on the value entered in the input Box */   
     
    public PageReference SearchLead() {
        leadSearchResults = new List<Lead>();                
        if((strInputCode != null && !strInputCode.equalsIgnoreCase('')) )     
        {         
            showClearList = true;    
            if(strInputCode.endsWith('*'))     
            {              
                strInputCode = strInputCode.substring(0,strInputCode.length()-1);  
                selectedLead= strInputCode +'%';       
            }         
            else     
            {          
                selectedLead= '%'+strInputCode +'%';     
            }        
            selectedLead= '%'+strInputCode +'%';  
            if(str1 != ''){       
                // list_LeadSearchResults  = [SELECT Id, Name,Company,Status,Related_Account__c,Related_Opportunity__c,Lead_Number__c,LeadSource  FROM Lead where name Like: selectedLead and Status not in ('Closed','Converted to Opportunity')  order by LastModifiedDate Desc limit 25 ]; 
                //FR-9639
                //leadSearchResults = R2R_Utility.getLeads(accdistrictName ,selectedLead);       
                leadSearchResults = R2R_Utility.getLeads(accGlobalDUNS ,selectedLead);       
            }
            else{
                System.debug('selectedLead--->'+selectedLead);
                //list_LeadSearchResults  = [SELECT Id, Name,Company,Status,Related_Account__c,Related_Opportunity__c,Lead_Number__c,LeadSource  FROM Lead where name Like: selectedLead and Status not in ('Closed','Converted to Opportunity') order by LastModifiedDate Desc limit 25 ];           
                //FR-9639
                //leadSearchResults = R2R_Utility.getLeads(accdistrictName); 
                leadSearchResults = R2R_Utility.getLeads(accGlobalDUNS); 
            }
        }    
        else if(strInputCode == '' ||strInputCode == null  ){  
            showClearList = true; 
            leadSearchResults   = [SELECT Id, Name,Company,Status,Related_Account__c,Related_Opportunity__c,Lead_Number__c,LeadSource  FROM Lead where name Like: selectedLead and Status not in ('Closed','Converted to Opportunity')  order by LastModifiedDate Desc limit 25 ];                                  
        }
        return null;  
    }
    //end of method

    public void validateLinking(){
        Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
        string LeadOpenStatus = data.get('R2R_lead_Status').datavalue__c;
        string LeadClosedStatus = data.get('R2R_Closed_lead_Staus').datavalue__c;
        string R2R_Admin_Profiles = data.get('R2R_Admin_Profiles').datavalue__c;
        string OpptyClosedLostStatus = data.get('OpptyClosedStatus').datavalue__c;
        String R2R_UserIdsToByPass = data.get('R2R_UserIdsToByPass').datavalue__c;
        boolean bypassRules=false;
        if(R2R_Admin_Profiles.contains(userinfo.getProfileId()))
        {
            bypassRules=true;
        }
        else if(R2R_UserIdsToByPass.contains(userinfo.getUserId()))
        {
            bypassRules=true;
        }
        else
        {
            bypassRules=false;
        }

        if(setSelectedAssetId ==null)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.Transformation_NoRecordsOnLink);
            ApexPages.addMessage(msg);
            intErrored++;  
                     
        }
        
        else if(setSelectedAssetId.size()>0){
            lstLeadJnObj = [Select Id, Related_Lead__c from Lead_Asset_Junction__c where Related_Asset__c in :setSelectedAssetId and Related_lead__r.Status not in ('Closed','Converted To Opportunity')];     
            lstOpptyJnObj = [select id,Asset_Record_Type_ID__c,Opportunity_Forecast_Status__c,Opportunity_Close_Date__c,Related_Asset__c,Related_Asset__r.id,Related_Opportunity__c,
                            Related_Opportunity__r.Opportunity_Type__c,Related_Opportunity__r.StageName,Record_Type_Name__c from Opportunity_Asset_Junction__c where Related_Asset__c in:setSelectedAssetId limit 50000];
        }
        
        Map<Id,Asset__c> mapAsset = new Map<Id,Asset__c>([Select Id,Install_Base_Status__c,RecordType.DeveloperName,Customer_Name__r.Account_District__c from Asset__c where id in :setSelectedAssetId and RecordType.DeveloperName!='EMC_Install_Inactive']);
        if(leadlink.Related_lead__c !=null){
            leadObj = [Select Id,Status,Lead_Type_Based_on_Linking__c from lead where id =:leadlink.Related_lead__c];
        }
        System.debug('bypassRules.................' +bypassRules);
        System.debug('leadObj.................' +leadObj);
        System.debug('lstLeadJnObj .................' +lstLeadJnObj );
        system.debug('**** LeadClosedStatus '+LeadClosedStatus);
        if(lstLeadJnObj !=null && !lstLeadJnObj.isempty())
        {
            System.debug('lstLeadJnObj................'+lstLeadJnObj);
            if(!LeadClosedStatus.contains(leadObj.status))
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Link_To_Lead);
                ApexPages.addMessage(msg);
                intErrored++;
            }
            else if(!bypassRules)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Link_To_Lead);
                ApexPages.addMessage(msg);
                intErrored++;
            }
        }     
        
        for(Opportunity_Asset_Junction__c OpptyJunction :lstOpptyJnObj){
            if(!bypassRules && !OpptyClosedLostStatus.contains(OpptyJunction.Related_Opportunity__r.StageName) && OpptyJunction.Related_Opportunity__r.Opportunity_Type__c.contains('Swap') && mapAsset.get(OpptyJunction.Related_Asset__c).RecordType.DeveloperName=='Competitive_Install'){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Linked_To_Oppty);
                ApexPages.addMessage(msg);
                intErrored++;
                break;
            }
            system.debug('**** bypassRules '+bypassRules);
            system.debug('**** Stage  '+OpptyJunction.Related_Opportunity__r.StageName);
            system.debug('**** leadObj.status.contains '+leadObj.status);
            system.debug('**** LeadClosedStatus '+LeadClosedStatus);
            system.debug('**** developer name '+mapAsset.get(OpptyJunction.Related_Asset__c).RecordType.DeveloperName);
            
            if(!OpptyClosedLostStatus.contains(OpptyJunction.Related_Opportunity__r.StageName) && !LeadClosedStatus.contains(leadObj.status) && !leadObj.status.contains('Renewals') && OpptyJunction.Related_Opportunity__r.Opportunity_Type__c.contains('Refresh') && mapAsset.get(OpptyJunction.Related_Asset__c).RecordType.DeveloperName=='EMC_Install'){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Linked_To_Oppty);
                ApexPages.addMessage(msg);
                intErrored++;                 
                break;
            }
            
            if(!bypassRules && !OpptyClosedLostStatus.contains(OpptyJunction.Related_Opportunity__r.StageName) && LeadClosedStatus.contains(leadObj.status) && !leadObj.status.contains('Renewals') &&    OpptyJunction.Related_Opportunity__r.Opportunity_Type__c.contains('Refresh') && mapAsset.get(OpptyJunction.Related_Asset__c).RecordType.DeveloperName=='EMC_Install'){
                System.debug('**** lead and oppty open hence failed');
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Linked_To_Oppty);
                ApexPages.addMessage(msg);
                intErrored++;                 
                break;
            }

            if(leadObj !=null){
                if(!OpptyClosedLostStatus.contains(OpptyJunction.Related_Opportunity__r.StageName) && OpptyJunction.Related_Opportunity__r.Opportunity_Type__c.contains('Renewals') && mapAsset.get(OpptyJunction.Related_Asset__c).RecordType.DeveloperName=='Competitive_Install' && leadObj.status.contains('Renewals')){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Linked_To_Oppty);
                    ApexPages.addMessage(msg);
                    intErrored++;                 
                    break;
                }
            }
            
            if(leadObj !=null){
                if(!OpptyClosedLostStatus.contains(OpptyJunction.Related_Opportunity__r.StageName) && OpptyJunction.Related_Opportunity__r.Opportunity_Type__c.contains('Renewals') && mapAsset.get(OpptyJunction.Related_Asset__c).RecordType.DeveloperName=='EMC_Install' && leadObj.status.contains('Renewals')){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Linked_To_Oppty);
                    ApexPages.addMessage(msg);
                    intErrored++;                 
                    break;
                }
            }
        }  
         
           
        if(leadObj !=null){
            system.debug('!R2R_UserIdsToByPass.contains(userinfo.getUserId()).........'+ !R2R_UserIdsToByPass.contains(userinfo.getUserId()));
            system.debug('LeadClosedStatus.contains(leadObj.status).........'+LeadClosedStatus.contains(leadObj.status));
            if(LeadClosedStatus.contains(leadObj.status) && !bypassRules)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Assets_To_Open_Lead);
                ApexPages.addMessage(msg);
                intErrored++;
            }
        }
        
        if(leadObj !=null && leadObj.Lead_Type_Based_on_Linking__c !=null){
            if(leadObj.Lead_Type_Based_on_Linking__c.contains('Renewals')){
                for(Asset__c assetObj :mapAsset.values()){
                    if(assetObj.RecordType.DeveloperName=='Competitive_Install'){
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Comp_Assets_Renewals_Lead);
                        ApexPages.addMessage(msg);
                        intErrored++;
                        break;
                    }
                }
            }
        }
    }
    /* public list<lead> getlist_LeadSearchResults(){
        System.debug('list_LeadSearchResults-->' +list_LeadSearchResults.size());
        return list_LeadSearchResults;
    }*/

    public PageReference linkAssestToLead(){
        List<Asset__c> selectedAssetsList = new List<Asset__c>();
        selectedAssetsList = SelectedRecord();
        set<String> seterrormessage= new set<string>();
        string leadIdFromURL= Apexpages.currentPage().getParameters().get('leadId');
        setSelectedAssetId = new set<Id>();
        List<Id> allSelectedAssetIdList = new List<Id>();
        if(leadlink.Related_lead__c==null){
            leadlink.Related_lead__c = leadIdFromURL;
        }  
        List<Id> selectedAssetId = new List<id>();
        //Commented existing Pagination implementation for R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
        /*system.debug('AssetList--->'+AssetList);
        for(AssetListItem aAss: AssetList){
            if(aAss.selected && aAss.assetObj.id!=null){               
                selectedAssetId.add(aAss.assetObj.Id);
                setSelectedAssetId.add(aAss.assetObj.Id);
            }                
        } */
        if(selectedAssetsList !=null && selectedAssetsList.size()>0){
            for(Asset__c aSel: selectedAssetsList)
            {
                setSelectedAssetId.add(aSel.Id);
                allSelectedAssetIdList.add(aSel.Id);
            }
            validateLinking();
            if(intErrored >0){
                return null;
            }
            system.debug('leadlink.Related_lead__c-----'+leadlink.Related_lead__c);   
            system.debug('selectedAssetId-----'+selectedAssetId);
            system.debug('setSelectedAssetId----'+setSelectedAssetId);
            system.debug('leadId----'+leadId);
            system.debug('allSelectedAssetIdList----'+allSelectedAssetIdList);
        
            if(intErrored <=0){
                if(leadlink.Related_lead__c!=null){     
                    //seterrormessage = R2R_Lead_Management.insertAssetLeadLinks(leadlink.Related_lead__c,selectedAssetId);
                    seterrormessage = R2R_Lead_Management.insertAssetLeadLinks(leadlink.Related_lead__c,allSelectedAssetIdList);
               } 
               if(seterrormessage !=null){
                    //errormessage= errormessage.substring(errormessage.indexof('Exception')+1,errormessage.indexof('[]')-1);
                    system.debug(seterrormessage );
                    for(string errormessage :seterrormessage ){
                        ApexPages.Message linkingerrormessage= new ApexPages.Message(ApexPages.Severity.Error,errormessage);
                        ApexPages.addMessage(linkingerrormessage);
                        return null;  
                    }               
                }
                if(leadIdFromURL !=null) {
                    retURL = '/'+leadIdFromURL;
                }
                if(leadIdFromURL ==null) {
                    retURL = '/'+selectedAssetId[0];
                }         
            }   
            return new pagereference(retURL);
        }
        else{
            ApexPages.Message myMsg;
            myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No Asset selected. Please confirm by selecting the Asset record and then click on the Link Button');
            ApexPages.addMessage(myMsg);
            return null;
        }       
    }
    
    // Wrapper Class
    public class AssetListItem { 
        public Asset__c assetObj {get;set;}
        public Boolean selected {get;set;}
       // public string patyNumber {get;set;}
        public AssetListItem (Asset__c a)
        {
            assetObj = a;
            selected = false;
        }
    }   
    
    //R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
    public List<Asset__c> SelectedRecord(){
        //for(AssetListItem aAss: AssetList)
        for(List<AssetListItem> aAss: listOfAssetWrapperlist)
        {
            for(AssetListItem aWrapper : aAss)
            {
                if(aWrapper.selected == true)
                {
                    assetsToAddList.add(aWrapper.assetObj);   
                }
            }
        }
        if((assetsToAddList!=null)&&(assetsToAddList.size()>0))
        {
           
            return assetsToAddList;                   
        }
        else
        {
            return null;
        }   
    }
    
    //R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
    public Boolean hasResults 
    {
        get{    
            if(setCon != null){             
                return (setCon.getResultSize()>0);
            }   
            return false;
        }
        set;
    }   
    
    public List<Asset__c> getselectedAssets()
    {
        //R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
        //return selectedAssets;
        return (List<Asset__c>) setCon.getRecords();
    }
    
    //R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
    public List<AssetListItem> getAssetWrapperlist(){
        //If there are records to be displayed then show the records else display message
        system.debug('setCon.getResultSize() : '+setCon.getResultSize());
        if((setCon.getResultSize()) > 0)
        {
            if(listOfAssetWrapperlist[setCon.getPageNumber()-1].size() == 0) {
                assetWrapperlist = new List<AssetListItem>();
                for(Asset__c a : (List<Asset__c>)setCon.getRecords()) {
                    assetWrapperlist.add(new AssetListItem(a));
                }
                listOfAssetWrapperlist[setCon.getPageNumber()-1] = assetWrapperlist;
            }
            return listOfAssetWrapperlist[setCon.getPageNumber()-1];
        }
        else
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_No_Assets_On_Lead);
            ApexPages.addMessage(msg);
            isListEmpty=true;
            return null;
        }
    }
    
    public PageReference cancelLink() {
        if(assetObj!=null && assetObj.Id !=null){
            return new PageReference('/'+assetObj.Id);
        }
        else{
            retURL= Apexpages.currentPage().getParameters().get('retURL');
            return new pagereference(retURL);
        } 
    }
    
    /*
        WR#1006 - Method to search Assets using Customer asset name
    */
    public void searchCustomerAsset() {
        
        if(customerAssetName != null && customerAssetName != '') {
      setCon = new ApexPages.StandardSetController(Database.query('Select Id ,Target_Zone__c,Red_Zone_Priority__c,Install_City__c,'+
                                        'Install_Base_Status__c,Contract_End_Date__c,Sales_Plan__c,Maintenance_Engagement_Status1__c,'+
                                        'Lease_Exp_Date__c,Model__c,Install_Date__c,Customer_Name__r.Party_Number__c,'+
                                        'Customer_Name__r.Customer_Profiled_Account_Lookup__c,Custom_Asset_Name__c,'+
                                        'Serial_Number__c,Name,Disposition_Status__c ,Account_Record_Type__c ,'+
                                        'RecordType.Name ,Product_Name_Vendor__c,Product_Family__c,Customer_Name__c,'+
                                        'Contract_Number__c from Asset__c where Customer_Name__r.Global_DUNS_Entity_Hub__c=: accGlobalDUNS '+
                                        'and Install_Base_Status__c not in(\'Deleted\',\'Displaced by EMC\',\'Displaced by Competitor\',\'Deinstall - Cust Site\') '+
                                        'and RecordType.DeveloperName!=\'EMC_Install_Inactive\' and Name like \'%'+customerAssetName+'%\' Order by Name limit 2000'));
        } else {
             setCon = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id ,Target_Zone__c,Red_Zone_Priority__c,Install_City__c,
                                        Install_Base_Status__c,Contract_End_Date__c,Sales_Plan__c,Maintenance_Engagement_Status1__c,
                                        Lease_Exp_Date__c,Model__c,Install_Date__c,Customer_Name__r.Party_Number__c,
                                        Customer_Name__r.Customer_Profiled_Account_Lookup__c,Custom_Asset_Name__c,
                                        Serial_Number__c,Name,Disposition_Status__c ,Account_Record_Type__c ,
                                        RecordType.Name ,Product_Name_Vendor__c,Product_Family__c,Customer_Name__c,
                                        Contract_Number__c from Asset__c where Customer_Name__r.Global_DUNS_Entity_Hub__c=: accGlobalDUNS 
                                        and Install_Base_Status__c not in('Deleted','Displaced by EMC','Displaced by Competitor','Deinstall - Cust Site') 
                                        and RecordType.DeveloperName!='EMC_Install_Inactive' Order by Name limit 9999]));
        }   
        
        //R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
        listOfAssetWrapperlist.clear();
        setCon.setPageSize(25);
        Integer reminder = math.mod(setCon.getResultSize(),setCon.getPageSize());
        Integer listToCreateSize = (reminder == 0?(setCon.getResultSize()/setCon.getPageSize()):(setCon.getResultSize()/setCon.getPageSize())+1);
        for(Integer i=0; i < listToCreateSize ; i++){
            listOfAssetWrapperlist.add(new List<AssetListItem>());
        } 
    }
    
    //R2R-May14 : Modified the code to implement Salesforce standard pagination logic : R2R Stabilization
    /*public PageReference processSelected()
    {
        // for(aAsset aAss: getAssets())
        for(AssetListItem aAss: AssetList){
            if(aAss.selected == true)
            {
                selectedAssets.add(aAss.assetObj);   
            }            
        }
        if((selectedAssets!=null)&&(selectedAssets.size()>0))
        {
            system.debug('selectedAssets in Process--->'+selectedAssets);  
        }       
        return null;
    } */
    
}//End  Class;