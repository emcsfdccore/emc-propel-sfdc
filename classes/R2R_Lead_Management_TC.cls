@isTest
public Class R2R_Lead_Management_TC
{

    static R2R_Lead_Management R2Rleadmangmnt= new R2R_Lead_Management();
    
        @testSetup static void TestData()
        {
            System.runAs(new user(Id = UserInfo.getUserId()))
            {
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
                CustomSettingDataHelper.countryTheaterMapCSData();
                CustomSettingDataHelper.dealRegistrationCSData();
                CustomSettingDataHelper.VCEStaticCSData();
            }
            
            // create account record.
            List<Account> acc = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();  
            insert acc; 
                
            Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
            String recordtypeEMC = data.get('Asset EMC Install Record Type Id').DataValue__c;
            String recordtypeComp = data.get('Asset Competitive Record Type Id').DataValue__c;
            
            // create asset
            List<Asset__c> listAssets=new List<Asset__c>();
            listAssets.add( new Asset__c(Name='TestassetComp0',Customer_Name__c=acc[0].id, RecordTypeId = recordtypeComp,Product_Name_Vendor__c='Dell' ));
            listAssets.add( new Asset__c(Name='TestassetComp1',Customer_Name__c=acc[0].id, RecordTypeId = recordtypeComp,Product_Name_Vendor__c='Dell'));
            listAssets.add( new Asset__c(Name='TestassetEMC0',Customer_Name__c=acc[0].id, RecordTypeId = recordtypeEMC,Product_Name_Vendor__c='EMC'));
            listAssets.add( new Asset__c(Name='TestassetEMC1',Customer_Name__c=acc[0].id, RecordTypeId = recordtypeEMC,Product_Name_Vendor__c='EMC'));
            insert listAssets ;
            
             //Creating Lead
            list<Lead> lstLeadToInsert = new list<Lead>();
            lstLeadToInsert.add(new lead (Company='TestLead' ,LastName='Test0', Status='Working',Sales_Force__c='EMC',Lead_Originator__c='Customer Intelligence'));
            lstLeadToInsert.add(new lead (Company='TestLead' ,LastName='Test1', Status='Pass to Renewals',Sales_Force__c='EMC',Lead_Originator__c='Customer Intelligence'));
            insert lstLeadToInsert;
            
            //Creating Opportunity records
            List<Opportunity> oppList = new List<Opportunity>();
            oppList.add(new Opportunity(Name = 'TestOpp0',AccountId = acc[0].Id, CloseDate = system.today() + 5, StageName = 'Pipeline',Sales_Channel__c='Direct',Sales_Force__c='EMC'));
            oppList.add(new Opportunity(Name = 'TestOpp1',AccountId = acc[0].Id, CloseDate = system.today() + 7, StageName = 'Pipeline',Opportunity_type__c = 'Refresh'));
            insert oppList;
            
        }

    static testmethod void testsetLeadType()
    {
        List<Account> acc = [ select id, name, Grouping__c from account where name in ('TestPartnerAcc', 'TestPartnerAcc1')]; 
        
        list<Lead> lstLeadToInsert = [select LastName,Company, Status, Sales_Force__c, Lead_Originator__c, Lead_Type_Based_on_Linking__c from lead where LastName like 'Test%' limit 2];
        
        List<Asset__c> listAssets=[select id, name, Customer_Name__c, Product_Name_Vendor__c from Asset__c where name in ('TestassetComp0', 'TestassetComp1','TestassetEMC0', 'TestassetEMC1') limit 4 ];
        
         //Creating Lead asset junction with lead status =Swap and EMC asset attached
        List<Lead_Asset_Junction__c> listlaj= new List<Lead_Asset_Junction__c>();
        listlaj.add(new Lead_Asset_Junction__c ( Related_Asset__c = listAssets[0].id, Related_Lead__c=lstLeadToInsert[0].id ));
        listlaj.add(new Lead_Asset_Junction__c ( Related_Asset__c = listAssets[2].id, Related_Lead__c=lstLeadToInsert[1].id ));
        OpportunityHelperClass.run=false;
        insert(listlaj);
        
        Test.startTest();
        R2Rleadmangmnt.setLeadType(listlaj);
        R2Rleadmangmnt.ProfileValidate(listlaj);
        
        String leadrecord = null;
        R2R_Lead_Management obj = new R2R_Lead_Management();
        obj.updateLeadAssetCount(listlaj);
        obj.Assetlinkedtopenlead(listAssets);
        
        List<Id> assetIdList = new List<Id>();
        assetIdList.add(listAssets[0].id);
        assetIdList.add(listAssets[1].id);
        assetIdList.add(listAssets[2].id);
       Set<String> strSet = R2R_Lead_Management.insertAssetLeadLinks(lstLeadToInsert[0].id,assetIdList); 
        
        //String leadrecord = null;
        Map<Id,List<Id>> mapleadIdwithasset= new Map<Id,List<Id>>();
        mapleadIdwithasset.put(lstLeadToInsert[1].id,assetIdList);
        leadrecord = obj.AssetvalidatelinkedLead(mapleadIdwithasset); 
        
        Map<Lead,List<Id>> mapleadWithAssetId= new map<Lead,List<Id>>();
        mapleadWithAssetId.put(lstLeadToInsert[0],assetIdList);
        R2R_Lead_Management.insertLeadAssetJunction(mapleadWithAssetId);
        
        Map<id,Lead> PassTorenewalsLead= new Map<id,Lead>();
        PassTorenewalsLead.put(lstLeadToInsert[1].id,lstLeadToInsert[1]);
        R2R_Lead_Management.validatePassTorenewalsLead(PassTorenewalsLead);
        
        Test.stopTest();
    }
    
    static testmethod void testAssetUnlinking()
    
    {
    
        List<Account> acc = [ select id, name, Grouping__c from account where name in ('TestPartnerAcc', 'TestPartnerAcc1')]; 
        
        //Creating Opportunity records
        List<Opportunity> oppList = [select Name, AccountId,StageName, Opportunity_Type__c from Opportunity where Name in ('TestOpp0', 'TestOpp1') limit 2];
        
         //Creating Lead
        list<Lead> lstLeadToInsert = [select LastName,Company, Status, Sales_Force__c, Lead_Type_Based_on_Linking__c, Lead_Originator__c from lead where LastName in('Test0', 'Test1') limit 2];
        
        // create asset
        List<Asset__c> listAssets=[select id, name, Customer_Name__c, Product_Name_Vendor__c from Asset__c where name in ('TestassetComp0', 'TestassetComp1','TestassetEMC0', 'TestassetEMC1') limit 4];
        
        Test.startTest();
        
                
         //Creating Lead asset junction with lead status =Swap and EMC asset attached
        List<Lead_Asset_Junction__c> listlaj= new List<Lead_Asset_Junction__c>();
        listlaj.add(new Lead_Asset_Junction__c ( Related_Asset__c = listAssets[0].id, Related_Lead__c=lstLeadToInsert[0].id ));
        listlaj.add(new Lead_Asset_Junction__c ( Related_Asset__c = listAssets[2].id, Related_Lead__c=lstLeadToInsert[1].id ));
        OpportunityHelperClass.run=false;
        insert(listlaj);
        
        //Insert Opportunity Asset Junction records
        List<Opportunity_Asset_Junction__c> oajList = new List<Opportunity_Asset_Junction__c>();
        oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = listAssets[1].Id,Related_Opportunity__c = oppList[0].Id));
        insert  oajList ;
        
        
        list<id> lstlinkid = new list<id>();
        lstlinkid.add(listlaj[0].id);
        lstlinkid.add(listlaj [1].id);
        
        list<id> lstOppid = new list<id>();
        lstOppid.add(oppList[1].id);
        
        Map<Lead,List<Opportunity>> leadwithOppt = new Map<Lead,List<Opportunity>> ();
        leadwithOppt.put(lstLeadToInsert[0], oppList );
        
        R2R_Lead_Management obj = new R2R_Lead_Management();
        String leadrecord = null;
        String successmessage = 'success';
        R2R_Lead_Management.deferAsset(listlaj);
        leadrecord =R2R_Lead_Management.AssetUnlinking(lstLeadToInsert[0].id,lstlinkid);
        leadrecord = R2R_Lead_Management.assetDeferUnliking(lstLeadToInsert[0].id,lstlinkid);
        leadrecord = obj.Assetvalidatelinked(lstLeadToInsert[0].id,lstOppid);
        leadrecord = obj.validateleadOpprtSameType(leadwithOppt);
        Test.stopTest();
    }
    
    static testmethod void testvalidateOpportunityConversion()
    {
    
        List<Account> acc = [ select id, name, Grouping__c from account where name in ('TestPartnerAcc', 'TestPartnerAcc1')]; 
        
        //Creating Opportunity records
        List<Opportunity> oppList = [select Name, AccountId,StageName,Opportunity_Type__c from Opportunity where Name in ('TestOpp0', 'TestOpp1') limit 2];
        
         //Creating Lead
        list<Lead> lstLeadToInsert = [select LastName,Company, Status, Sales_Force__c, Lead_Originator__c, Lead_Type_Based_on_Linking__c from lead where LastName in('Test0', 'Test1') limit 2];
        
        // create asset
        List<Asset__c> listAssets=[select id, name, Customer_Name__c, Product_Name_Vendor__c from Asset__c where name in ('TestassetComp0', 'TestassetComp1','TestassetEMC0', 'TestassetEMC1') limit 4 ];
        
         Test.startTest();
         
         //Creating Lead asset junction with lead status =Swap and EMC asset attached
        List<Lead_Asset_Junction__c> listlaj= new List<Lead_Asset_Junction__c>();
        listlaj.add(new Lead_Asset_Junction__c ( Related_Asset__c = listAssets[0].id, Related_Lead__c=lstLeadToInsert[0].id ));
        listlaj.add(new Lead_Asset_Junction__c ( Related_Asset__c = listAssets[2].id, Related_Lead__c=lstLeadToInsert[1].id ));
        OpportunityHelperClass.run=false;
        insert(listlaj);
        
        //Insert Opportunity Asset Junction records
        List<Opportunity_Asset_Junction__c> oajList = new List<Opportunity_Asset_Junction__c>();
        oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = listAssets[1].Id,Related_Opportunity__c = oppList[0].Id));
        insert  oajList ;
        
        try
        {
        lstLeadToInsert[0].Related_Opportunity__c = oppList[0].id;
        update lstLeadToInsert ;
        }
        catch(exception e){
            }
        
        map<id, lead> mapfromTrigger = new map<id,lead> ();
        mapfromTrigger.put(lstLeadToInsert[0].id, lstLeadToInsert[0]);
        
        R2R_Lead_Management obj = new R2R_Lead_Management();
        R2R_Lead_Management.validateOpportunityConversion(mapfromTrigger);
        R2R_Lead_Management.mapAssetsToOppty(mapfromTrigger);
        
        Test.stopTest();
    }
}