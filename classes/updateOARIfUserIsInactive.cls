/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE        DEVELOPER     DESCRIPTION                               
 |  ====        =========     =========== 
 | 22.12.2011   Shipra Misra  Oppty OAR: Trigger when any user record is made inactive.It should Deactivate all the OAR's where these User's are present as Resource.
 | 17.08.2013   Sneha Jain    Modified the existing class running on trigger invocation to Batch Class which is scheduled to run daily. 
+===========================================================================*/

global class updateOARIfUserIsInactive implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        List<User> inactiveUserList = [Select Id,Name,isActive from User where isActive = false];
        //Test query
        if(test.isRunningTest())
        {
            return Database.getQueryLocator([Select id, name,Resource_Name__c,Active__c from Opportunity_Assignment_Rule__c where Resource_Name__c in: inactiveUserList limit 5]);
        }
        else
        {   
            //Querying the records which are currently active but associated with inactive Resource
            return Database.getQueryLocator([Select id, name,Resource_Name__c,Active__c from Opportunity_Assignment_Rule__c where Resource_Name__c in: inactiveUserList AND Active__c = true]);
        }
    }
    
    global void execute(Database.BatchableContext BC, SObject[] scope)
    {
        //Deactivating the OAR records
        List<Opportunity_Assignment_Rule__c> lstOARUpdate= new List<Opportunity_Assignment_Rule__c>();
        for(Opportunity_Assignment_Rule__c OARobj:(List<Opportunity_Assignment_Rule__c>)scope)
        {
            OARobj.Active__c=false;
            lstOARUpdate.add(OARobj);
        }
        if(lstOARUpdate!=null && lstOARUpdate.size()>0)
        {
            database.update(lstOARUpdate,false);
        }
    }
    
    global void finish(Database.BatchableContext BC){}      
}