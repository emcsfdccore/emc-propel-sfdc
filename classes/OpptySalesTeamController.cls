/**
    * Class Name: OpptySalesTeamController
    * Author: Accenture
    * Date: 05-Jun-2008
    * Requirement/Project Name: <EMC_SFDC>
    * Requirement/Project Description: <Controller class for visual force page 'saleTeamInformation'>
    
    Update on: 12-Jan-2011
    Update By: Sunil Bansal
    Updated for: WR 156054 - Fix Labeling on Opportuntiy Sales Team Access so it is consistent
  Updated for: WR-339361 - If Sales team member from _TC profile split shoub be hide in Sales Team Related list
                             If sales tem member added through other than OAR,Manual,Advanced Opportunity Search, User Assignmnet value should be other.
*/

/*
    *------Revision------
    *   Version 1.0: Pilot Release
    *   By: <Developer � Gajanand k chalkapure>
    *          Version 2.0: CR 1.0 - Not to delete the access 
    *   By: <Developer � Gajanand k chalkapure> 
    *           
*/


public class OpptySalesTeamController { 

    Public Integer Pageheight{get;set;} 
    
    Public String OpportunityId=ApexPages.currentPage().getParameters().get('Id');
            
    public List<OpportunitySalesTeam> OpportunitySalesTeam =new List<OpportunitySalesTeam>(); 
    
    public List<OpportunitySalesTeam> getOpportunitySalesTeam(){
    
        return this.OpportunitySalesTeam;
    }
    
    List <OpportunityTeamMember> salesTeamMember=null;
    
    Map<String,String> DetailLineMap=new Map<String,String>(); 
    
    Map<String,String> OppTeamMap=new Map<String,String>();
    public Boolean addedThroughFlg {get;set;} {addedThroughFlg = false;}//WR-339361
    public OpptySalesTeamController(ApexPages.StandardController Controller){     
        Pageheight=0;
        //WR-339361 Added logic to show Added Through filed to set of profiles.
        map<string,customsettingdatavaluemap__c> datavaluemap = customsettingdatavaluemap__c.getall();
              List<Profile> loginProfileList = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
              String loginProfileName = loginProfileList[0].Name;
              if(datavaluemap.get('OppAddedThroughVisibilityProfiles').datavalue__c.contains(loginProfileName)){
                  this.addedThroughFlg= true;
              } else{
                this.addedThroughFlg= false;
              }
        //Added_Through__c added in query by Naga WR-339361
        //Adding column for Sales Role - Jaspreet
        salesTeamMember = [  Select o.User.Name, o.UserId,o.TeamMemberRole,o.User.Profile.Name, o.OpportunityAccessLevel,o.User.Forecast_Group__c,Added_Through__c, o.User.Role__c,o.Reason_for_Addition__c  From OpportunityTeamMember o where o.OpportunityId=:OpportunityId order by o.User.Forecast_Group__c desc];
        
        for(Detail_Line__c dt:[Select d.Opportunity__c, d.OpportunityTeamMember__c, d.Split__c from Detail_Line__c d  where d.Opportunity__r.Id=:OpportunityId]){
        
             DetailLineMap.put(dt.OpportunityTeamMember__c,''+dt.Split__c);
        }
        
        String SplitPercentage='';

        for(OpportunityTeamMember teammem:salesTeamMember){
           if(DetailLineMap.size()>0)
              SplitPercentage=DetailLineMap.get(teammem.Id);
        OpportunitySalesTeam.add(new OpportunitySalesTeam(teammem,SplitPercentage,teammem.User.Forecast_Group__c));
           
        if(salesTeamMember.size()==1){
                Pageheight=Pageheight+30;
        }
            Pageheight=Pageheight+42;
        }
    }    
        
    public class OpportunitySalesTeam{
    
    public OpportunityTeamMember OppTeamMem{get;set;}
    
    public String split{get;set;} 
    
    public String accessLevel{get;set;} 
    public String addedThrough{get;set;} //WR-339361
    public Boolean SplitFlg {get;set;} {SplitFlg = false;}
    
    public string userSalesRole {get;set;}
//public String forecastgroup{get;set;}   
       
    public OpportunitySalesTeam(OpportunityTeamMember member,String split,String forecastgroup){
            this.OppTeamMem=member;
            this.split= split;
            if(member.OpportunityAccessLevel == 'Private' || member.OpportunityAccessLevel == 'None')
                accessLevel = 'Full Access';
            else
                accessLevel = member.OpportunityAccessLevel;
            //WR-339361
            map<string,customsettingdatavaluemap__c> datavaluemap = customsettingdatavaluemap__c.getall();
                if(datavaluemap.get('OppotunitySalesTeamProfiles').datavalue__c.contains(member.User.Profile.Name)){
                  this.SplitFlg= true;
                }             
            if(member.Added_Through__c == 'OAR' ||  member.Added_Through__c == 'Manual' || member.Added_Through__c == 'Advanced Opportunity Search'|| member.Added_Through__c == 'User Assignment' || member.Added_Through__c == 'Mass Reassignment' ){
                this.addedThrough = member.Added_Through__c;
            }else{
                this.addedThrough = '';
            }
           
            this.userSalesRole = member.Reason_for_Addition__c;
            //Ends here WR-339361
//      this.forecastGroup=forecastgroup;
        }
       }
       
       
}