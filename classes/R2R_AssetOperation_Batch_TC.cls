/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  09 Dec 2013      Sneha Jain                 Optimized the test class according to coding standards, removed seeAllData
 |  27 Dec 2013      Bhanuprakash   WR319143    Implemented helper classes for Asset and Lead creation
 +=========================================================================================================================*/
@isTest
public Class R2R_AssetOperation_Batch_TC
{
    
    public static testmethod void testsAssetOperationbatch()
    {
        //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        }   
        //Creating Account
        Account acc= R2R_datahelper.createAccount();
        insert acc;

        //Creating Query from recordtype
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where Name like 'EMC_Install' or Name Like 'Competitive_Install'];
        List<Asset__c> listAssets=new List<Asset__c>();

        //Creating Comp asset
        List<Asset__c> assetList = new List<Asset__c>();
        //Calling helper class method by passing account object
        assetList = AssetTestClassDataHelper.createAsset(acc);
        //Change for asset1
         if(recordType[0].Name == 'Competitive_Install')
        {
            assetList[0].RecordTypeId=recordType[0].id;
            assetList[1].RecordTypeId=recordType[0].id;
        }
        else
        {
            assetList[0].RecordTypeId=recordType[1].id;
            assetList[1].RecordTypeId=recordType[1].id;
        }
        //Change for asset3
        assetList[2].RecordTypeId=recordType[0].id;
        
        insert assetList;


        //Creating Lead
        List<Lead> leadList = new List<Lead>();
        
        leadList = LeadTestClassDataHelper.createLead();
        insert leadList;
        
        //creating list lead asset junction object
        List<Lead_Asset_Junction__c> listlaj= new List<Lead_Asset_Junction__c>();

        //Creating Lead asset junction with Lead status= Swap and COmp asset attached
        Lead_Asset_Junction__c laj1= new Lead_Asset_Junction__c();
        laj1.Related_Asset__c=assetList[0].id;
        laj1.Related_Lead__c=leadList[0].id;
        listlaj.add(laj1);


        //Creating Lead asset junction with Lead status= Refresh and COmp asset attached
        Lead_Asset_Junction__c laj2= new Lead_Asset_Junction__c();
        laj2.Related_Asset__c=assetList[0].id;
        laj2.Related_Lead__c=leadList[1].id;  
        listlaj.add(laj2);
        
        insert listlaj;
        
        Database.BatchableContext bc;
        Test.startTest();
            R2R_AssetOperation_Batch Assobj=new R2R_AssetOperation_Batch(); 
            Assobj.batchQuery= Assobj.batchQuery+'Limit 10' ;
            ID batchprocessid = Database.executeBatch(Assobj);
            Assobj.execute(bc,assetList);
            Assobj.finish(bc);
        Test.stopTest();
    }
}