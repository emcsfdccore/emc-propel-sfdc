/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE       DEVELOPER        WR       DESCRIPTION                               
 |  ====       =========        ==       =========== 
 |  24-Dec-2014 Bhanuprakash  PROPEL     Created to test Oppty_CreateQuoteController class           
  +===========================================================================*/

    @isTest
    public class Oppty_CreateQuoteController_TC {
        
        public static  void createTestData(){
        
        }
        public static testmethod void performCreateQuote(){
         System.runAs(new user(Id = UserInfo.getUserId()))
            {
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
            }
     //Creating Custom Setting data ****************************** END *****************************
        //Create District_Lookup__c : ********************* START ***********************
        District_Lookup__c dislokup = new District_Lookup__c(name='DisLUp 1');
        insert dislokup;
        System.debug('*** dislokup Id = ' + dislokup.Id);
        //Create District_Lookup__c : ********************* END ***********************
            //Create User & Account List : ********************* START ***********************
             List<Account> accList = new List<Account>();
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator - API Only' limit 1]; 
            List<User> lstUser = new List<User>();
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, 
                  TimeZoneSidKey='America/Los_Angeles', UserName='TestUser@some.com');
                  
            User u2 = new User(Alias = 'stan34', Email='standarduser234@testorg.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, 
                  TimeZoneSidKey='America/Los_Angeles', UserName='TestUser234@some.com');
                  
                  lstUser.add(u);
                  lstUser.add(u2);
                insert lstUser;  
                System.debug('**** new user id = '+ u.Id);
                
                accList = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
                System.runAs(lstUser[0]) {
                    accList[0].party_number__c='11331015';
                    accList[0].District_Lookup__c=dislokup.Id;
                    accList[1].District_Lookup__c=dislokup.Id;
                    accList[0].UCID__c ='1234567';
                    accList[0].Lead_Oppty_Enabled__c = true;
                    accList[1].Lead_Oppty_Enabled__c = true;
                    accList[0].UCID__c = '123131';
                    accList[1].UCID__c = '123131';
                    //accList[1].Account_District__c=dislokup.Id;//Not writeable field
                    insert accList;
                 }
                System.debug('**** accList size = '+ accList.size());
                System.debug('**** accList size = '+ accList[0].Lead_Oppty_Enabled__c);
            //Create User & Account List : ********************* END ***********************
        
            //Create Opportunity List : ********************* START ***********************
            List<Opportunity> lstTestOpps = new List<Opportunity>();
            List<Opportunity> lstopps = new List<Opportunity>(); 
            String accId = accList[0].Id;
            Opportunity opps = new Opportunity(Name = 'Test Oppty ' ,
                                              StageName = 'Pipeline',
                                              Amount=100.5,
                                              CloseDate = System.today()+2,
                                              Sales_Force__c = 'EMC', 
                                              Sales_Channel__c = 'Direct',
                                              VMWare_Related__c = 'Not VMware Related', 
                                              AccountId = accList[0].Id,
                                              Partner__c = accList[0].Id,
                                              Tier_2_Partner__c =accList[1].Id);
            //System.debug('**** opps sise = ' + opps.size());
             System.runAs(lstUser[0]) {
                insert opps;
                }
            //CREATE PROPEL GENERAL SETTING CUSTOM SETTING
            
            PROPEL_General_Settings__c pGS = new PROPEL_General_Settings__c(Create_Quote_Encrypt_Key__c='9nH4lwh1ImzndDwy1ISgMQ==',Direct_Distribution_Channels__c ='Direct',Hybris_URL__c='https://pstore-qav.emc.com/propelstorefront/createQuote/createQuoteFromSFDC?site=propel');
            System.runAs(lstUser[0]) { insert pGS; }
            
            
            Test.startTest();
             PageReference pageRef = Page.Oppty_CreateQuote;
            pageRef.getParameters().put('id', String.valueOf(opps.Id));
            Test.setCurrentPage(pageRef);
            ApexPages.standardcontroller sController = new apexpages.standardcontroller(opps);
            
            
            System.runAs(lstUser[0]) {
            Oppty_CreateQuoteController createQuote = new Oppty_CreateQuoteController(sController);
            System.debug('*** calling valieate()');
            createQuote.validate();
            createQuote.redirectToOppty();
            }
            //RUN AS UNACCESSIBLE USER
            System.runAs(lstUser[1]){
                Oppty_CreateQuoteController createQuote2 = new Oppty_CreateQuoteController(sController);
                createQuote2.validate();
            }
            
            
            opps.Quote_Cart_Number__c ='123456';
            //update opps;
            
            sController = new apexpages.standardcontroller(opps);
            System.runAs(lstUser[0]) {
                Oppty_CreateQuoteController createQuote = new Oppty_CreateQuoteController(sController);
            System.debug('*** calling valieate()');
            createQuote.validate();
            }
            //QUOTE NO NOT NULL TEST
            
            //Quote_Cart_Number__c
            Test.stopTest();
        }
    }