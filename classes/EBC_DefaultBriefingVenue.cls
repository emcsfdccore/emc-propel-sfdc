/*=====================================================================================+
 | HISTORY                                                                             |
 |                                                                                     |
 | DATE        DEVELOPER   WR      DESCRIPTION                                         |
 | ==========  ==========  ======  ===========                                         |
 | 06/10/2013  Y. Salazar  268708  Initial creation. Methods to assign corresponding   |
 |                                 venue for each Briefing Event according to location.|
 | 15/12/2014 Akash Rastogi 1431   Updated to add new briefing center Shanghai  
 | 20/02/2014 Bisna V P     1719   Updated to Durham to pivotal      |
 +=====================================================================================*/

public without sharing class EBC_DefaultBriefingVenue{

  Map<Id,Id> bcLocationVenue = new Map<Id,Id>();
  
  public EBC_DefaultBriefingVenue(){
    /*Briefing Center Location, Briefing Venue*/
    bcLocationVenue.put('a0j70000000Tbk1', 'a0l70000000UIUZ'); //Hopkinton
    bcLocationVenue.put('a0j70000000Ts9d', 'a0l70000000UeR0'); //Cork
    bcLocationVenue.put('a0j70000000Tbjz', 'a0l70000000UIUc'); //Singapore
    bcLocationVenue.put('a0j700000028GWH', 'a0l70000002WQdn'); //Washington, D.C.
    bcLocationVenue.put('a0j70000000Tbk0', 'a0l70000000UIUf'); //RSA-Bedford, MA.
    bcLocationVenue.put('a0j70000000Tbk2', 'a0l70000000UQA6'); //Santa Clara
    bcLocationVenue.put('a0j70000000U4sX', 'a0l70000000WrRK'); //Bangalore
    //bcLocationVenue.put('a0j70000000V4iW', 'a0lS00000022pJW'); //Durham
    bcLocationVenue.put('a0j70000000V4ib', 'a0l70000002Xo5E'); //Rio de Janeiro
    bcLocationVenue.put('a0j70000000V4iR', 'a0l70000000XKoe'); //Beijing
    bcLocationVenue.put('a0j70000000Tbk4', 'a0l70000000Vk2R'); //Tele-Briefings
    bcLocationVenue.put('a0j70000000Tbk8', 'a0l70000000Ubhc'); //Tokyo
    bcLocationVenue.put('a0j70000000Tbk5', 'a0l70000000UJMP'); //Tour Only
    //WR#1431 Starts
    bcLocationVenue.put(system.label.Shanghai, system.label.Shanghai_Venue); //Shanghai
    
    //WR#1431 Endss
    //1719 change
    bcLocationVenue.put(system.label.Pivotal_Briefing_Center, system.label.Pivotal_Briefing_Venue); //Durham
    //Leave blank for Field Americas, APJ and EMEA
  }

  public void setBriefingVenue(List<EBC_Briefing_Event__c> requestedBE){
    for(EBC_Briefing_Event__c be:requestedBE){
      if(bcLocationVenue.keySet().contains(be.Briefing_Center__c)){
          be.Briefing_Venue__c = bcLocationVenue.get(be.Briefing_Center__c);
      }
    }
  }
}