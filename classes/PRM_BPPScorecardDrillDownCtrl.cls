/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req     DESCRIPTION                               
 |  ====            =========       ======     ===========  
 |  20/11/2014    Sneha Jain        393254      Controller class Scorecard Drilldown
 |  31/12/2014    Anand Sharma      BPP2015     Added logic to include LB/PAN drildown (Few more changes)
+=============================================================================================================================*/
public class PRM_BPPScorecardDrillDownCtrl{
    
    public List<Velocity_Rule_Results__c> vrrList = new List<Velocity_Rule_Results__c>();
    public Velocity_Rules__c ruleRecord = new Velocity_Rules__c();
    
    public List<Education__c> educationList = new List<Education__c>();
    public Boolean isSuperUser{get;set;}
    public Boolean isStandarduser{get;set;}
    
    public List<Education__c> getEducationList(){
        return educationList;
    }
    public Boolean displayText{set;get;}
    
    public PRM_BPPScorecardDrillDownCtrl(){
        
        PRM_PAN_VPP_VelocityRuleResultsUtils PANVPPVRRUtils = new PRM_PAN_VPP_VelocityRuleResultsUtils();
        Map<Id,List<Account_Groupings__c>> mapGrouping = new Map<Id,List<Account_Groupings__c>>();
        
        isSuperUser=false;
        isStandarduser=false;
        User u = [select Id,Name, Profile.Name from User WHERE id = :UserInfo.getUserId()];
        if(u.Profile.Name.containsIgnoreCase('Direct Reseller Super User') || u.Profile.Name.containsIgnoreCase('VAR Super User') || u.Profile.Name.containsIgnoreCase('Distributor Super User') ){
            isSuperUser=true;
        }
        else if (u.Profile.Name.containsIgnoreCase('Direct Reseller Partner User') || u.Profile.Name.containsIgnoreCase('VAR Partner User') || u.Profile.Name.containsIgnoreCase('Distributor Partner User')){
            isStandarduser=true;    
        }       
    
    //Boolean flag to decide whether to display the static text message on page or not. It defines the case of Federation Tier  
        displayText = false;
        //Parameters passed in URL from Scorecard page when training contact count is clicked
        Id vrrId = ApexPages.currentPage().getParameters().get('vrrId');
        Id ruleId = ApexPages.currentPage().getParameters().get('vrId');
        Id accId = ApexPages.currentPage().getParameters().get('Id');
        String selectedTier = ApexPages.currentPage().getParameters().get('selectedTier');
      
        //Querying Velocity Rule Result record details
        vrrList = [select id,Speciality_RuleID__c,Grouping__c,AccountID__c,Speciality_RuleID__r.Velocity_Rule_1__c,Speciality_RuleID__r.Group_Rule_3__c, Speciality_RuleID__r.Group_Rule_4__c,Speciality_RuleID__r.Group_Rule_5__c,Speciality_RuleID__r.Velocity_Rule_2__c from Velocity_Rule_Results__c where Id =: vrrId limit 1];
        
        //Querying Velocity Rule record details
        ruleRecord = [Select Id, Name, AND_OR__c,Display_Name__c, Evaluation_at__c from Velocity_Rules__c where Id =: ruleId limit 1];
        
        //Custom setting details to display static text - considering Federation Tier value
        List<FederationprogramTier__c> fedPgmTierList = FederationprogramTier__c.getall().values();
        if(fedPgmTierList != null && fedPgmTierList.size()>0){  
            for(FederationprogramTier__c fed:fedPgmTierList){
                if(fed.Training__c == ruleRecord.Display_Name__c && fed.Tier__c == selectedTier){
                    displayText = true;
                }
            }
        }
        
        //Querying Account and its Partner Grouping details
        List<Account> accList = [Select Id, Name, Grouping__c,Account_Level__c, Grouping__r.Logical_Block_Level_Grouping__c, Grouping__r.Logical_Block_Level_Grouping__r.Pan_Theater_Level_Grouping__c from Account where Id =: accId];
        String partnerId = accList[0].Grouping__c;
        if(ruleRecord.Evaluation_at__c=='Logical Block Grouping Level' ){
            partnerId = accList[0].Grouping__r.Logical_Block_Level_Grouping__c;
            List<Id> lstGroupingIds = new List<Id>();
            lstGroupingIds.add(partnerId);
            mapGrouping = PANVPPVRRUtils.getGroupingsUnderLogicalBlock(lstGroupingIds);
            
            vrrList = [select id,Speciality_RuleID__c,Grouping__c,AccountID__c,Speciality_RuleID__r.Velocity_Rule_1__c,Speciality_RuleID__r.Group_Rule_3__c, Speciality_RuleID__r.Group_Rule_4__c,Speciality_RuleID__r.Group_Rule_5__c,Speciality_RuleID__r.Velocity_Rule_2__c from Velocity_Rule_Results__c where Grouping__c in:mapGrouping.get(partnerId)];
        }
        if(ruleRecord.Evaluation_at__c=='PAN Theater Grouping Level' ){
            partnerId = accList[0].Grouping__r.Logical_Block_Level_Grouping__r.Pan_Theater_Level_Grouping__c;
            List<Id> lstGroupingIds = new List<Id>();
            lstGroupingIds.add(partnerId);
            mapGrouping = PANVPPVRRUtils.getGroupingsUnderPANLevel(lstGroupingIds);
            vrrList = [select id,Speciality_RuleID__c,Grouping__c,AccountID__c,Speciality_RuleID__r.Velocity_Rule_1__c,Speciality_RuleID__r.Group_Rule_3__c, Speciality_RuleID__r.Group_Rule_4__c,Speciality_RuleID__r.Group_Rule_5__c,Speciality_RuleID__r.Velocity_Rule_2__c from Velocity_Rule_Results__c where Grouping__c in:mapGrouping.get(partnerId)];
        }
        system.debug('mapGrouping--> ' + mapGrouping.values());
        system.debug('vrrList--> ' + vrrList);
        //partnerId = partnerId.substring(0,15);
        //Method call
        PRM_VPP_RuleEvaluator prmVppEvaluator = new PRM_VPP_RuleEvaluator(vrrList);

        Set<Id> contactIdSet = new Set<Id>();
        for(Velocity_Rule_Results__c objVRR :vrrList){
            contactIdSet.addAll(prmVppEvaluator.evaluateGroupRule(ruleRecord,objVRR));
        }
        system.debug('contactIdSet ' + contactIdSet);
        
        //Changing 18 digit contact ID to 15 digit
        Set<String> contact15IdSet = new Set<String>();
        if(contactIdSet != null && contactIdSet.size() > 0){
            for(Id con: contactIdSet){
                contact15IdSet.add((String.valueOf(con)).substring(0,15));
            }
            
            List<Velocity_Rule_Member__c> vrmList = [select id,Group__c,Speciality_Rule__c from Velocity_Rule_Member__c where Speciality_Rule__c =: ruleRecord.Id];
            
            List<Education_Groups__c> egList = new List<Education_Groups__c>();
            
            //Querying Education Group record from the Display Name of the training record clicked on scorecard
            //egList = [select id,Mentoring__c from Education_Groups__c where Education_Group_Name__c =: ruleRecord.Display_Name__c];
            if(vrmList!= null && vrmList.size()>0){
                Set<Id> egIdSet = new Set<Id>();
                for(Velocity_Rule_Member__c vrm: vrmList){
                    egIdSet.add(vrm.Group__c);
                }
                if(egIdSet !=null && egIdSet.size()>0){
                    List<Education_EducationGroup_Mapping__c> eegmList = new List<Education_EducationGroup_Mapping__c>();
                    
                    //Querying Education Education Group Mapping records based on Mentoring value
                    //if(egList[0].Mentoring__c){
                    //    eegmList = [select id,ContactId__c,Education_Mentoring_Completed__c,Education__c from Education_EducationGroup_Mapping__c where PartnerId__c =: partnerId and Education_Group__c IN: egIdSet and ContactId__c IN : contact15IdSet and Education_Mentoring_Completed__c =: 'Yes' and Contact_Status__c = 'Yes' ];
                    //}
                   // else{
                   if(ruleRecord.Evaluation_at__c=='Country Grouping Level'){
                        eegmList = [select id,ContactId__c,Education_Mentoring_Completed__c,Education__c from Education_EducationGroup_Mapping__c where Education__r.Contact__r.Account.Grouping__c =:partnerId and Education_Group__c IN: egIdSet and ContactId__c IN : contact15IdSet and Contact_Status__c = 'Yes' ];
                   }else{
                        eegmList = [select id,ContactId__c,Education_Mentoring_Completed__c,Education__c from Education_EducationGroup_Mapping__c where Education__r.Contact__r.Account.Grouping__c in:mapGrouping.get(partnerId) and Education_Group__c IN: egIdSet and ContactId__c IN : contact15IdSet and Contact_Status__c = 'Yes' ];
                    }
                  // }
                    
                    
                    
                    if(eegmList !=null && eegmList.size()>0){
                        Set<Id> eduIdSet = new Set<Id>();
                        for(Education_EducationGroup_Mapping__c eegm: eegmList){
                            eduIdSet.add(eegm.Education__c);
                        }
                        if(eduIdSet !=null && eduIdSet.size()>0){
                        
                            //Querying Education records to display the details on the VF page sorted by First Name of Contact
                            educationList = [Select e.Name, e.First_Name__c, e.Email__c, e.Date_Achieved__c, e.ESBI_Name__c, e.Contact__c, e.Last_Name__c, e.Cert_ID__c From Education__c e where Id IN:eduIdSet order by First_Name__c ];
                        }
                    }
                }
            }
        }
    }
}