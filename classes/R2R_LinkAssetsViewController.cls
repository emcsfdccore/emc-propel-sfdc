/*==================================================================================================================+

 |  HISTORY  |                                                                            

 |  DATE          DEVELOPER      WR        DESCRIPTION                              

 |  ====          =========      ==        =========== 

 |  24/06/2013    Anirudh Singh  R2R       To Link Assets To Lead                       
 |  24/02/2014    Srikrishna SM  FR-9639   Commneted error conditions to avoid displaying error when asset is selected
 										   different Account distrinct and when Account District field is blank                       
 +==================================================================================================================*/


public class R2R_LinkAssetsViewController {   
    public String retURL='/a3S/o';
    public Account accountObj {get;set;}
    public Account CustomerProfileAccount {get;set;}
    public lead leadObj {get;set;}
    public lead_Asset_Junction__c leadlink {get;set;}    
    public List<Asset__c> selectedAssets {get;set;} 
    public Boolean isSelected {get;set;}
    public Boolean isListEmpty {get;set;}
    public Boolean renderAssetSection {get;set;}
    public Boolean renderLeadSection {get;set;}
    public List<Account> listAccount   {get;set;}
    public List<Asset__c> listAsset   {get;set;} 
    public List<Lead> leadSearchResults {get;set;}     
    public id selectedAccountId {get;set;}
    public List<string> lstSelectedAccount{get;set;}
    public List<string> lstSelectedAccId{get;set;}
    public List<Lead_Asset_Junction__c> lstLeadJnObj = new List<Lead_Asset_Junction__c>();  
    public List<Opportunity_Asset_Junction__c> lstOpptyJnObj = new List<Opportunity_Asset_Junction__c>();
   //Properties for Lead Lookup
   public String strInputCode { get; set; }
   public string accdistrictName {get; set;}
   public String selectedLead;
   public string leadId{get;set;}
   public List<Lead> list_MasterLeadSearchResults { get; set; } 
   public List<Asset__c> list_leadofsameaccdistrict{get; set;}
   public Boolean showClearList{get;set;}
   public Boolean isLinkdisabled{get;set;}
   string str1 ='' ;   
   public string assetaccid{get;set;}
   public  map<Id,Lead> mapLead{ get; set; } 
   public Integer intDiffAccount {get;set;} {intDiffAccount = 0;}
   public Integer intLinkToOpenLead {get;set;} {intLinkToOpenLead = 0;}
   public Integer intLinkToRenewalsLead {get;set;} {intLinkToRenewalsLead = 0;}
   public Integer intLinkToOpenOpportunity {get;set;} {intLinkToOpenOpportunity = 0;}
   public Integer intErrored {get;set;} {intErrored = 0;}
   public Integer intNoRec {get;set;} {intNoRec = 0;}  
   public Integer intNoAccDistrict {get;set;} {intNoAccDistrict = 0;}
   Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
   string OpptyClosedLostStatus = data.get('OpptyClosedStatus').datavalue__c;
   public Map<Id,Asset__c> mapAsset; 
    //End of lead lookupproperties
    
    public R2R_LinkAssetsViewController(ApexPages.StandardSetController controller) {        
        Account accountrelatedToAsset;
        isListEmpty = false;
        isLinkdisabled = false;
        renderLeadSection = false;
        renderAssetSection =false; 
        selectedAssets = controller.getselected();
        
        string rendersection = Apexpages.currentPage().getParameters().get('rendersection');
        string accdist = Apexpages.currentPage().getParameters().get('accDist');
        string assetId = Apexpages.currentPage().getParameters().get('assetid');
        string leadToFetch= Apexpages.currentPage().getParameters().get('selLead');
        string leadIdFromURL= Apexpages.currentPage().getParameters().get('leadId');          
        
         mapAsset = new Map<Id,Asset__c>([Select Id,Install_Base_Status__c,RecordType.DeveloperName,Customer_Name__r.Account_District__c from Asset__c where id in :selectedAssets]);
        if(selectedAssets.size()>0){
            lstLeadJnObj = [Select Id, Related_Lead__c from Lead_Asset_Junction__c where Related_Asset__c in :selectedAssets and Related_lead__r.Status not in ('Closed','Converted To Opportunity')];     
            lstOpptyJnObj = [select id,Asset_Record_Type_ID__c,Opportunity_Forecast_Status__c,Opportunity_Close_Date__c,Related_Asset__c,Related_Asset__r.id,Related_Opportunity__c,
                            Related_Opportunity__r.Opportunity_Type__c,Related_Opportunity__r.StageName,Record_Type_Name__c from Opportunity_Asset_Junction__c where Related_Asset__c in:selectedAssets limit 50000];
        }
        if(selectedAssets==null || selectedAssets.isempty()){
           ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.Transformation_Go_back);
           isLinkdisabled = true;
           ApexPages.addMessage(msg);
           intNoRec++;  
        }
        if(lstLeadJnObj !=null && !lstLeadJnObj.isempty()){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Link_To_Lead);
            ApexPages.addMessage(msg);
            intLinkToOpenLead++;
        }    
        for(Asset__c assetObj :mapAsset.values()){
                accdist = assetObj.Customer_Name__r.Account_District__c;
                break;
        }
        for(Asset__c assetObj :mapAsset.values()){
            //FR-9639
            /*
            if(accdist != assetObj.Customer_Name__r.Account_District__c){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Lead_Asset_not_in_same_CPA);
                ApexPages.addMessage(msg);
                intDiffAccount++;
                break;
            }
            */
        }
       if(accdist!=null || accdistrictName!=null){
           if(accdist!=null){
               accdistrictName = accdist;
           }               
           
       }
       //FR-9639
       /*
       if(accdistrictName == null || accdistrictName == ''){
           intNoAccDistrict++;
        }   
        
      
      if(intNoAccDistrict!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Account_District_Cannot_Be_Blank);
            ApexPages.addMessage(msg);
        }
        */
       leadlink = new lead_Asset_Junction__c();             
       String strLead = ApexPages.currentPage().getParameters().get('retURL');
         System.debug('strOpp  :'+strLead);        
        //  String OpportunityId ='';       
          leadId ='';
          if(strLead !=null){
              if (strLead.indexof('leadId')!=-1){
                     integer index =strLead.indexof('leadId');
                     System.debug('index '+index);
                     leadId=strLead.substring(index +14);
                     leadlink.Related_Lead__c=leadId;
                  leadObj = [Select Id,Lead_Type_Based_on_Linking__c from lead where id =:leadId];
                    
                }
                
           } 
          
           else if(leadIdFromURL !=null){
               leadlink.Related_Lead__c=leadIdFromURL;
               leadObj = [Select Id,Lead_Type_Based_on_Linking__c from lead where id =:leadIdFromURL];
           } 
          if(leadObj !=null){
             if(leadObj.Lead_Type_Based_on_Linking__c.contains('Renewals')){
                for(Asset__c assetObj :mapAsset.values()){
                    if(assetObj.RecordType.DeveloperName=='Competitive_Install'){
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Comp_Assets_Renewals_Lead);
                        ApexPages.addMessage(msg);
                        intLinkToRenewalsLead++;
                        break;
                    }
                }
             }
          }
          for(Opportunity_Asset_Junction__c OpptyJunction :lstOpptyJnObj){
               if(mapAsset.containskey(OpptyJunction.Related_Asset__c) && OpptyJunction.Related_Opportunity__r.Opportunity_Type__c !=null){
                  if(!OpptyClosedLostStatus.contains(OpptyJunction.Related_Opportunity__r.StageName)
                     && OpptyJunction.Related_Opportunity__r.Opportunity_Type__c.contains('Swap') && 
                     mapAsset.get(OpptyJunction.Related_Asset__c).RecordType.DeveloperName=='Competitive_Install'){
                     ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Linked_To_Oppty);
                     ApexPages.addMessage(msg);
                     intLinkToOpenOpportunity++;
                     break;
                  }  
                }   
         }
         intErrored = intLinkToOpenOpportunity+intLinkToRenewalsLead+intNoRec+intDiffAccount+intLinkToOpenLead+intNoAccDistrict;
         system.debug('intErrored ' +intErrored );
         if(intErrored >0){
            renderLeadSection = false;
            isLinkdisabled= true;
         }
         else{
             renderLeadSection = true;
         }
       System.Debug('leadIdFromURL--->' +leadIdFromURL );
       System.debug('renderLeadSection '+renderLeadSection);
       System.debug('renderLeadSection '+renderAssetSection);
       System.Debug('list_LeadSearchResults'+leadSearchResults);
       // mapLead = new map<Id,Lead>([Select Id,Name,Related_Account__r.Account_District__c from Lead where Related_Account__r.Account_District__c =:assetaccid limit 100]);
         //SearchLead();
             
    } 
    public void validateLinking(){
        if(intNoRec!= 0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.No_Record_Selected_for_Create_Opportunity);
            ApexPages.addMessage(msg);
        }
        if(intLinkToOpenLead !=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Link_To_Lead);
            ApexPages.addMessage(msg);
        }
        //FR-9639
        /*
        if(intDiffAccount!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Lead_Asset_not_in_same_CPA);
            ApexPages.addMessage(msg);
        }
        */
        if(intLinkToOpenOpportunity!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Linked_To_Oppty);
            ApexPages.addMessage(msg);
        }
        if(intLinkToRenewalsLead!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Comp_Assets_Renewals_Lead);
            ApexPages.addMessage(msg);
        }
         
        
    }
    /* This Method is to clear the List when clear search items link is clicked*/  
   /*public PageReference clearData(){     
           list_LeadSearchResults.clear();   
           showClearList = false;        
           list_LeadSearchResults = list_MasterLeadSearchResults;    
           return null; 
         }    
    
     /* This is the search Functionlity which will fetch lead based on the value entered in the input Box */   
     
 public PageReference SearchLead() {
    
     
    
        leadSearchResults = new List<Lead>(); 
         
                 
           if((strInputCode != null && !strInputCode.equalsIgnoreCase('')) )     
                 {         
                         showClearList = true;    
                         if(strInputCode.endsWith('*'))     
                         {              
                         strInputCode = strInputCode.substring(0,strInputCode.length()-1);  
                         selectedLead= strInputCode +'%';       
                         }         
                         else     
                         {          
                         selectedLead= '%'+strInputCode +'%';     
                         }        
                         selectedLead= '%'+strInputCode +'%';  
                         if(str1 != ''){       
                               // list_LeadSearchResults  = [SELECT Id, Name,Company,Status,Related_Account__c,Related_Opportunity__c,Lead_Number__c,LeadSource  FROM Lead where name Like: selectedLead and Status not in ('Closed','Converted to Opportunity')  order by LastModifiedDate Desc limit 25 ]; 
                                  leadSearchResults = R2R_Utility.getLeads(accdistrictName ,selectedLead);       
                         }
                         else{
                                System.debug('selectedLead--->'+selectedLead);
                                //list_LeadSearchResults  = [SELECT Id, Name,Company,Status,Related_Account__c,Related_Opportunity__c,Lead_Number__c,LeadSource  FROM Lead where name Like: selectedLead and Status not in ('Closed','Converted to Opportunity') order by LastModifiedDate Desc limit 25 ];           
                              leadSearchResults = R2R_Utility.getLeads(accdistrictName); 
                         }
                 }    
                 else if(strInputCode == '' ||strInputCode == null  ){  
                         showClearList = true; 
                         leadSearchResults   = [SELECT Id, Name,Company,Status,Related_Account__c,Related_Opportunity__c,Lead_Number__c,LeadSource  FROM Lead where name Like: selectedLead and Status not in ('Closed','Converted to Opportunity')  order by LastModifiedDate Desc limit 25 ];           
                 
                                
                                           
 }
 return null;  
 }
 //end of method

    
   /* public list<lead> getlist_LeadSearchResults(){
        System.debug('list_LeadSearchResults-->' +list_LeadSearchResults.size());
        return list_LeadSearchResults;
    }*/

   public PageReference linkAssestToLead(){
            validateLinking();
            if(intErrored<=0){
               set<String> seterrormessage=new set<string>();
               string leadIdFromURL= Apexpages.currentPage().getParameters().get('leadId');
               if(leadlink.Related_lead__c==null){
                  leadlink.Related_lead__c = leadIdFromURL;
                }
                lead leadObj = [Select id,Status,Lead_Type_Based_on_Linking__c from lead where id =:leadlink.Related_lead__c];
                System.Debug('++'+lstOpptyJnObj);
                for(Opportunity_Asset_Junction__c OpptyJunction :lstOpptyJnObj){
                    if(!OpptyClosedLostStatus.contains(OpptyJunction.Related_Opportunity__r.StageName)
                     && OpptyJunction.Related_Opportunity__r.Opportunity_Type__c.contains('Refresh') && 
                     mapAsset.get(OpptyJunction.Related_Asset__c).RecordType.DeveloperName=='EMC_Install' && (leadObj.Lead_Type_Based_on_Linking__c ==null || leadObj.Lead_Type_Based_on_Linking__c=='' || leadObj.Lead_Type_Based_on_Linking__c=='Refresh Lead') && leadObj.Status !='Pass to Renewals' ){
                     ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Linked_To_Oppty);
                     ApexPages.addMessage(msg);                     
                     return null;
                    }
                    if(!OpptyClosedLostStatus.contains(OpptyJunction.Related_Opportunity__r.StageName)
                     && OpptyJunction.Related_Opportunity__r.Opportunity_Type__c.contains('Renewals') && 
                     mapAsset.get(OpptyJunction.Related_Asset__c).RecordType.DeveloperName=='EMC_Install' && leadObj.Status =='Pass to Renewals' ){
                     ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Linked_To_Oppty);
                     ApexPages.addMessage(msg);                     
                     return null;
                    }
                }   
               List<Id> selectedAssetId = new List<id>();
               system.debug('AssetList--->'+selectedAssets);
               for(Asset__c assetObj: selectedAssets)
                {                     
                            selectedAssetId.add(assetObj.Id);
                               
                     
                             
                } 
                system.debug('leadlink.Related_lead__c'+leadlink.Related_lead__c);   
                system.debug('leadlink.Related_lead__c'+selectedAssetId);
                system.debug('leadlink.Related_lead__c'+leadId);
                
               if(leadlink.Related_lead__c!=null){     
                  seterrormessage = R2R_Lead_Management.insertAssetLeadLinks(leadlink.Related_lead__c,selectedAssetId);
               } 
               if(seterrormessage !=null){
                  //errormessage= errormessage.substring(errormessage.indexof('Exception')+1,errormessage.indexof('[]')-1);
                  system.debug(seterrormessage );
                  for(string errormessage :seterrormessage ){
                   ApexPages.Message linkingerrormessage= new ApexPages.Message(ApexPages.Severity.Error,errormessage);
                   ApexPages.addMessage(linkingerrormessage);
                    return null;  
                  }               
               }
            }   
               
          return new pagereference(retURL);
        
    }
    
    
    
    
   
    
    
    public PageReference cancelLink() {
        return new PageReference(retURL);
}



 
}//End of Class;