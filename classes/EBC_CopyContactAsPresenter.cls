/*=======================================================================================+
 | HISTORY                                                                               |
 |                                                                                       |
 | DATE        DEVELOPER   WR      DESCRIPTION                                           |
 | ==========  ==========  ======  ===========                                           |
 | 07/11/2013  Y. Salazar  273539  Initial creation. Methods to copy Presenter (Contact) |
 |                                 from Topic Presenter in Session Presenter records, on |
 |                                 insert and update operations. This implementation     |
 |                                 avoids losing Presenter when Topic Presenter is deleted
 |                                 from EMC Internal Contact.                            |
 +======================================================================================*/

public without sharing class EBC_CopyContactAsPresenter{
  Set<Id> tpIds = new Set<Id>();
  Map<Id,EBC_Topic_Presenters__c> topicPres;
  
  public void copyOnInsert(List<EBC_Session_Presenter__c> sessionPres){
    for(EBC_Session_Presenter__c sp:sessionPres){
      if(sp.Topic_Presenters__c != null){
        tpIds.add(sp.Topic_Presenters__c);
      }
    }
    
    copyPresenter(sessionPres);
  }
  
  public void copyOnUpdate(Map<Id,EBC_Session_Presenter__c> newPres, Map<Id,EBC_Session_Presenter__c> oldPres){
    List<EBC_Session_Presenter__c> sessionPres = new List<EBC_Session_Presenter__c>();
    
    for(EBC_Session_Presenter__c sp:newPres.values()){
      if(sp.Topic_Presenters__c != oldPres.get(sp.Id).Topic_Presenters__c && sp.Topic_Presenters__c != null){
        tpIds.add(sp.Topic_Presenters__c);
        sessionPres.add(sp);
      }
    }
    
    if(tpIds.size() > 0){
      copyPresenter(sessionPres);
    }
  }
  
  public void copyPresenter(List<EBC_Session_Presenter__c> sessionPres){
    topicPres = new Map<Id,EBC_Topic_Presenters__c>([SELECT Id, Presenter__c FROM EBC_Topic_Presenters__c WHERE Id IN :tpIds]);
    
    for(EBC_Session_Presenter__c sp:sessionPres){
      if(topicPres.containsKey(sp.Topic_Presenters__c)){
        sp.Presenter__c = topicPres.get(sp.Topic_Presenters__c).Presenter__c;
      }
    }
  }
}