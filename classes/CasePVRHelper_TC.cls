/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE           DEVELOPER          WR       DESCRIPTION                                
|
   ====           =========          ==       ===========  
|  20-June-2014   Partha Baruah      PVR      Created test class for "CasePVRHelper" class
+===========================================================================*/ 
@isTest
private class CasePVRHelper_TC {
      
    public static testMethod void InsertCasePVR(){
        
        //Using below to avoid Mixed DML Exception while calling the  methods of CustomSettingDataHelper class        
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
                    
        System.runAs(insertUser)
        {
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
                CustomSettingDataHelper.dealRegistrationCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.partnerBoardingCSData();
                CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
                CustomSettingDataHelper.VCEStaticCSData();
                CustomSettingDataHelper.PSCCaseTypeImageData();
                CustomSettingDataHelper.PSCFieldMappingData();
                CustomSettingDataHelper.SC13LanguageData();
        }
            
        List<case> caseObjList = new List<case>();
        
        //Getting the PVR Case Record Type Id using Describe Method
        Map<String,Schema.RecordTypeInfo> recordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName(); 
        final String casePvrRecordTypeId = recordTypes.get('Renewals Policy Variation Request (PVR)').getRecordTypeId();
        
        //Creating PVR case test data for Insert scenario 
        caseObjList.add(new case(Concession_End_Date__c = System.today(),STATUS = 'Open', Origin ='System', parentid = null ,SUBJECT='PVR case : 1' ,DESCRIPTION = 'Testing Insert scenario of case :1', RecordTypeId = casePvrRecordTypeId, New_Deal_SO_3__c ='90', New_Deal_SO_4__c ='23' ,New_Deal_SO_1__c = '1234567',New_Deal_SO_2__c = '1231',Sales_Order_Number__c = '123')); 
        
         // Adding case records.
       /* list<case> listcase = new list<case>();
        listcase.add(new case( Origin ='Community', STATUS = 'New', RecordTypeId = casePvrRecordTypeId, parentid = null, SUBJECT='testcase0', DESCRIPTION = 'Testing description'));
        listcase.add(new case( Origin ='Community', STATUS = 'New', RecordTypeId = casePvrRecordTypeId, parentid = null, SUBJECT='testcase1', DESCRIPTION = 'Testing description', PSC_Requested_Preferred_Distributor__c = 'Test', PSC_Current_Distributor__c = 'Test', Deal_Registration_Number__c = LeadNum));
        insert listcase; */
        
        
        caseObjList.add(new case(Concession_End_Date__c = System.today() +3,STATUS = 'Open', Origin ='Community', SUBJECT='PVR case : 2' ,DESCRIPTION = 'Testing Insert scenario of case :2', RecordTypeId = casePvrRecordTypeId, New_Deal_SO_1__c = '23423423425234',New_Deal_SO_2__c = '11231231',Sales_Order_Number__c = '2342342344',New_Deal_SO_3__c ='30', New_Deal_SO_4__c ='45'  )); 
        
        /*caseObjList.add(new case(Concession_End_Date__c = System.today(),STATUS = 'Open', Processing_Status__c = 'open' ,parentid = null, TYPE = 'Data Migration',Origin ='System',PVR_INQUIRY_TYPE__C ='Within Policy Duration2', CONCESSION_TYPE__C = 'Field',PVR_BUSINESS_UNIT__C='Core',SUBJECT='PVR case : 3' ,DESCRIPTION = 'Testing Insert scenario of case :3', RecordTypeId = casePvrRecordTypeId,New_Deal_SO_1__c = '',New_Deal_SO_2__c = '',Sales_Order_Number__c = '0000' ));
        
        caseObjList.add(new case(Concession_End_Date__c = System.today(),STATUS = 'Open', Processing_Status__c = 'open' , TYPE = 'Data Migration',Origin ='System',PVR_INQUIRY_TYPE__C ='Within Policy Duration3', CONCESSION_TYPE__C = 'Field', PVR_BUSINESS_UNIT__C='Core',SUBJECT='PVR case : 4' ,DESCRIPTION = 'Testing Insert scenario of case :4', RecordTypeId = casePvrRecordTypeId,New_Deal_SO_1__c = '1234567',New_Deal_SO_2__c = '1212313231',Sales_Order_Number__c = '12322222222222'));
        
        caseObjList.add(new case(Concession_End_Date__c = System.today(),STATUS = 'Open', Processing_Status__c = 'open' , TYPE = 'Data Migration',Origin ='System',PVR_INQUIRY_TYPE__C ='Within Policy Duration4', CONCESSION_TYPE__C = 'Field', PVR_BUSINESS_UNIT__C='Core',SUBJECT='PVR case : 5' ,parentid = null , DESCRIPTION = 'Testing Insert scenario of case :5', RecordTypeId = casePvrRecordTypeId,New_Deal_SO_1__c = '',New_Deal_SO_2__c = '',Sales_Order_Number__c ='' ));       
        system.debug(caseobjList);   */   

        insert caseObjList;                
                               
       
        //Test data for covering update scenario   
        /*caseObjList[0].New_Deal_SO_1__c = '1234567123123';
        caseObjList[0].New_Deal_SO_2__c = '11232323';
        caseObjList[0].Sales_Order_Number__c = '';
        caseObjList[0].Concession_End_Date__c= System.today()+5;
  
        caseObjList[1].New_Deal_SO_1__c = '';
        caseObjList[1].New_Deal_SO_2__c = '';
        caseObjList[1].Sales_Order_Number__c = ''; 
          
        caseObjList[2].New_Deal_SO_1__c = '';
        caseObjList[2].New_Deal_SO_2__c = '';
        caseObjList[2].Sales_Order_Number__c = '865462323423424'; */
        //Update caseObjList; 
        
        //Calling method to improve code coverage
        casePVRHelper csPVR = new casePVRHelper();
        csPVR.caseNullifyPVRLogic(caseObjList);
    }   
 }