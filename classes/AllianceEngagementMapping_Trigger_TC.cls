/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE       DEVELOPER       WR       DESCRIPTION                                
|
   ====       =========       ==       ===========  
|  18-Apr-2013  Prachi                 Created 
+===========================================================================*/ 
@isTest(SeeAllData=true)
private Class AllianceEngagementMapping_Trigger_TC{

    public static testmethod void AllianceEngagementMapping_Trigger(){
        Alliance_Engagement_Mapping__c objAll= new Alliance_Engagement_Mapping__c();

                Account acc = new Account();
                acc.Name='ABC';

            insert acc;
            
            acc.IsPartner = true;            
            acc.Lead_Oppty_Enabled__c = true;
            acc.Partner_Type__c = 'Direct Reseller';
            
            update acc;

            objAll.Primary_Alliance_Partner__c=acc.id;
            objAll.Alliance_Engagement_Document_Name__c='Document name';
            insert objAll;

            objAll.Alliance_Engagement_Document_Name__c='Changed name';
            update objAll;
            
        }  
    }