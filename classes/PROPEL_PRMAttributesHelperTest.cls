/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  17 Jun 2014      Sneha Jain                 PROPEL PRM Attributes I.002 - Initial Creation
 |  20 Oct 2014      Bhanuprakash   PROPEL      Changed Partner_Service_Enabled_Product__c test as per Dennis update
 +=========================================================================================================================*/

@isTest
private class PROPEL_PRMAttributesHelperTest
{
    
    static testMethod void accountChildrecords()
    {      
        //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
        }
        
        //Insert a user record
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        User insertUser = new user(email='test-user1@emailTest.com',profileId = sysid ,  UserName='test-user1@emailTest.com', alias='tuser11', CommunityNickName='tuser11', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User'); 
        
        insert insertUser;
        
        System.runAs(insertUser)
        {
            Test.startTest();
            //Insert Account records
            List<Account> accList = new List<Account>();
            accList.add(new Account(Name = 'TestAcount0',Partner_Quoting_Account__c = true));
            accList.add(new Account(Name = 'TestAcount1',Partner_Quoting_Account__c = true,Party_Number__c= '123'));
            accList.add(new Account(Name = 'TestAcount2',Partner_Quoting_Account__c = true,Party_Number__c= '234'));
            accList.add(new Account(Name = 'TestAcount3',Partner_Quoting_Account__c = true,Party_Number__c= '345'));
            insert accList;
            
            //Update Account record
            accList[0].Partner_Quoting_Account__c = false;
            accList[0].Partner_Quoting_Account_Name__c=accList[1].Id;
            accList[3].Partner_Quoting_Account__c = false;
            accList[3].Partner_Quoting_Account_Name__c=accList[2].Id;
            update accList;
            
            
            //Insert Partner Type Attribute records
            List<Partner_Type_Attribute__c> ptaList = new List<Partner_Type_Attribute__c>();
            ptaList.add(new Partner_Type_Attribute__c(Partner_Account__c = accList[1].Id,Partner_Price_List_Type__c = 'Tier 1- Reseller',Business_Partner_Program_Tier__c='Affiliate') );
            insert ptaList;
            
            //Update Partner Service Enabled Product records
            TriggerContextUtility.firstRun = true;
            ptaList[0].Business_Partner_Program_Tier__c = 'Signature';
            update ptaList;
            
            //Delete Partner Service Enabled Product records
            delete ptaList;
            
            //Insert Partner Quoting Relationship records
            List<Partner_Quoting_Relationship__c> pqrList = new List<Partner_Quoting_Relationship__c>();
            pqrList.add(new Partner_Quoting_Relationship__c(Partner_Account__c = accList[1].Id,Related_Account__c = accList[2].Id,Relationship_Type__c ='Bill To'));
            pqrList.add(new Partner_Quoting_Relationship__c(Partner_Account__c = accList[0].Id,Related_Account__c = accList[2].Id,Relationship_Type__c ='Bill To'));
            insert pqrList;
                        
            //Delete Partner Quoting Relationship records
            delete pqrList[0];
            delete pqrList[1];
            
            //Insert Partner Service Enabled Product records
            List<Partner_Service_Enabled_Product__c> psepList = new List<Partner_Service_Enabled_Product__c>();
            psepList.add(new Partner_Service_Enabled_Product__c(Partner_Account__c = accList[1].Id,Service_Enabled_Product__c ='VNX-HW', Service_Level__c='1'));
            insert psepList;
            
            //Update Partner Service Enabled Product records
            TriggerContextUtility.firstRun = true;
            //Bhanu - Commenting as we can't change Service_Level__c' value as per new update in validation rule and commented Update trigger
            //psepList[0].Service_Level__c='2';
            //update psepList;
            
            //Delete Partner Service Enabled Product records
            delete psepList;
            
            
            //Insert Account Association Records
            List<APPR_MTV__RecordAssociation__c> accAList = new List<APPR_MTV__RecordAssociation__c>();
            accAList.add(new APPR_MTV__RecordAssociation__c(APPR_MTV__Account__c = accList[1].Id,APPR_MTV__Account_Role__c='Distributor',APPR_MTV__Associated_Account__c = accList[2].Id));
            accAList.add(new APPR_MTV__RecordAssociation__c(APPR_MTV__Account__c = accList[1].Id,APPR_MTV__Account_Role__c='Distributor',APPR_MTV__Associated_Account__c = accList[3].Id));
            insert accAList;
            
            //Delete Account Association Records
            delete accAList[0];
            delete accAList[1];
            Test.stopTest();
        }   
    }    
}