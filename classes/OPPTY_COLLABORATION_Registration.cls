/*======+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER          WR        DESCRIPTION                               

 |  ====          =========          ==        =========== 

 |  02/27/2014    Shalabh Sharma               This class is a controller for OpptyCollab_OpptyRegPage page. 
 |  03/18/2014    Bhanu Prakash                Replaced Sales_Collaboration_Comments__c with Next_Step__c 
 |  03/21/2014    Bhanu Prakash                Added Comments for review
 |  22 July 2014    Avinash K                   Updated the code to include the 
 |                                              field "Is_This_a_VSPEX_Solution_Opportunity__c" in the Partner
 |                                              portal
 |  11/11/2014   Akash Rastogi     1435        Updated the code to get the values of two fields from the partner
 |                                             portal 
 |  12/04/2014    Vinod Jetti      1340         Updated the code to include the 
 |                                              fields "Is_This_a_Cross_Border_Deal__c" & "In_Which_Countries__c" in Partner
 |                                              portal
 |  13-Jan-2015   Vinod Jetti      #1641        added field 'Distributor_In_Origin_Country__c' in Partner portal
 |  04-Mar-2015    Altaf      CDC Req# 1576    Set Shared to partner value on Opportunity to YES to allow integration log creation
 |  07-April-2015  Vinod Jetti     #1825       added field 'Is_this_an_EHC_deal__c' in Partner portal
 +==================================================================================================================**/
public class OPPTY_COLLABORATION_Registration
{
    public Opportunity_Registration__c objOpptyReg{get;set;}    
    public String oppId{get;set;}
    public Opportunity opp{get;set;}
    public Boolean noRecords{get;set;}    
    public String CustomerName{get;set;}
    public String CustomerEmail{get;set;}
    public String CustomerPhoneNumber{get;set;}
    public String DealDescription{get;set;}
    public Decimal ExpectedDealValue{get;set;}
    public Date ExpectedCloseDate{get;set;}
    public Boolean MultipleShipTo{get;set;}
    public String LocationType{get;set;}
    public String isVSPEX{get;set;}
    public String EngineerAccredited{get;set;}
    public String isHosting{get;set;}
    public String isServiceDelivery{get;set;}
    public String endUser{get;set;}
    public String enggEmail{get;set;}
    public String enggPhone{get;set;}
    public String enggName{get;set;}
    public String PartnerComments{get;set;}   
    public String EMCEmail{get;set;}   
    public String EMCName{get;set;}   
    public String IsThisVSPEX{get;set;}
    public String IsThisCross{get;set;}
    public String InWhich{get;set;}
    public String VMWareRelated{get;set;}
    public String SalesCollaComments{get;set;} 
    public String PartnerRepName{get;set;}
    public String PartnerRepEmail{get;set;}
    public Lead objLead{get;set;}
 
    public static Boolean blnOpptyApproval = false;

    //Changes for In Which Country

    Public List<string> leftselected{get;set;}
    Public List<string> rightselected{get;set;}
    Set<string> leftvalues = new Set<string>();
    Set<string> rightvalues = new Set<string>();
    //public List<SelectOption> SelectedValues ;

    //End of In Which Country


    /* @Method <getLocation(): Setting pick list options for Location.> 
       @param - <void>
       @return - <List<SelectOption>
       @throws exception - <No Exception>
    */
    public List<SelectOption> getLocation() {            
        if(LocationType== null)
        {
            LocationType='Office';
        }

        return getPicklistFieldValues('Opportunity_Registration__c','Location_Type__c');
    
    }
    /* @Method <getHosting(): Setting picklist options for Is this a hosting opportunity field.> 
       @param - <void>
       @return - <List<SelectOption>
       @throws exception - <No Exception>
    */   
    public List<SelectOption> getHosting() {  
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--')); 

        options.addAll(getPicklistFieldValues('Opportunity_Registration__c','Is_this_a_hosting_Opportunity__c'));

        return options;
    }
    /* @Method <getServiceType(): Setting picklist options for Service Delivery field.> 
       @param - <void>
       @return - <List<SelectOption>
       @throws exception - <No Exception>
    */   
    public List<SelectOption> getServiceDelivery() {  
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--')); 

        options.addAll(getPicklistFieldValues('Opportunity_Registration__c','Service_Delivery__c'));

        return options;
    }
    /* @Method <getEngineer(): Setting picklist options for Presales Engineer field.> 
       @param - <void>
       @return - <List<SelectOption>
       @throws exception - <No Exception>
    */
    public List<SelectOption> getEngineer() {   
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--')); 

        options.addAll(getPicklistFieldValues('Opportunity_Registration__c','Is_Presales_Engineer_Accredited__c'));

        return options;
    }
    /* @Method <getVSPEX(): Setting picklist options for Is VSPEX opportunity field.> 
       @param - <void>
       @return - <List<SelectOption>
       @throws exception - <No Exception>
    */
    public List<SelectOption> getVSPEX() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--')); 

        options.addAll(getPicklistFieldValues('Opportunity','Is_this_a_VSPEX_Opportunity_Partner__c'));

        return options;
    } 
    /* @Method <getCross(): Setting picklist options for Is this Cross Border Deal field.> 
       @param - <void>
       @return - <List<SelectOption>
       @throws exception - <No Exception>
    */
    public List<SelectOption> getCross() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--')); 

        options.addAll(getPicklistFieldValues('Opportunity','Is_this_a_Cross_Border_Deal__c'));

        return options; 
        
    } 
    /* @Method <getCountry(): Setting picklist options for In Which Countries.> 
       @param - <void>
       @return - <List<SelectOption>
       @throws exception - <No Exception>
    */
    public List<SelectOption> getCountry() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--')); 

        options.addAll(getPicklistFieldValues('Opportunity','In_Which_Countries__c'));

        return options; 
        
    } 
   
    public String ApprovalStatus{get;set;}//= new String[]{};
    
    /* @Method <getStatus(): Setting picklist options for Approval Status field.> 
       @param - <void>
       @return - <List<SelectOption>
       @throws exception - <No Exception>
    */
    public List<SelectOption> getStatus() {        
        if(ApprovalStatus== null)
        {
            ApprovalStatus='Awaiting Response';
        }

        return getPicklistFieldValues('Opportunity_Registration__c','Partner_Approval_Status__c');
        }

     public String RejectionReason{get;set;}
     
     /* @Method <getReason(): Setting picklist options for Rejection Reasons field.> 
       @param - <void>
       @return - <List<SelectOption>
       @throws exception - <No Exception>
    */
     public List<SelectOption> getReason() {
        List<SelectOption> options = new List<SelectOption>();
        if(ApprovalStatus.equals('Rejected'))
        {
            options = getPicklistFieldValues('Opportunity_Registration__c','Partner_Rejection_Reason__c');
        }      
        
        return options;
        }
    
     /* @Method <getVMWare(): Setting picklist options for VMWARE related field.> 
       @param - <void>
       @return - <List<SelectOption>
       @throws exception - <No Exception>
    */
     public List<SelectOption> getVMWare() {             
        return getPicklistFieldValues('Opportunity','VMWare_Related__c');
        }    

        
    /* @Method : Construstor 
       @param - Instance of Standard Set Controller
       @return - N/A
       @throws exception - <No Exception>
    */
    public OPPTY_COLLABORATION_Registration(ApexPages.StandardController controller){
        
    }
    
     /*@Method : Construstor 
       @param - No Param
       @return - N/A
       @throws exception - <No Exception>
    */
    public OPPTY_COLLABORATION_Registration() {
        objOpptyReg = new Opportunity_Registration__c();
        objLead = new Lead();
        //Fetch opportunity id from page
        oppId = System.currentPageReference().getParameters().get('Id');
        // initialize variables
        noRecords = true;
        // Fetch logged in user details
        User usr = [Select ContactId, AccountId from User where Id=:UserInfo.getUserId() LIMIT 1];
        if(oppId != null && oppId!=''){
            try{
                if(UserInfo.getProfileId().contains(Profiles__c.getInstance().System_Administrator__c)){
                    objOpptyReg = [Select Id,Status__c,Partner_Approval_Status__c,Customer_Name__c,Partner_Rejection_Reason__c,Is_this_a_hosting_Opportunity__c,Presales_Engineer_Phone__c,Presales_Engineer_Email__c,
                            Partner_Comments__c,Service_Delivery__c,Location_Type__c,Multiple_Ship_To__c,Who_is_the_end_user__c ,Is_Presales_Engineer_Accredited__c,Partner_Sales_Rep_Email__c, Partner_Sales_Rep_Name__c,
                            Expected_Close_Date__c,Expected_Deal_Value__c,Related_Opportunity__r.Expected_Close_Date__c,Presales_Engineer_Name__c,Deal_Description__c,Customer_Phone_Number__c,
                            Customer_Email__c,EMC_Contact_Email__c,EMC_Contact_Name__c,Related_Opportunity__r.Expected_Deal_Value_del__c,Related_Opportunity__r.Is_this_a_VSPEX_Opportunity_Partner__c,Next_Steps__c,Related_Opportunity__r.Is_this_a_VSPEX_Opportunity1__c,Related_Opportunity__r.VMWare_Related__c from Opportunity_Registration__c where Related_Opportunity__c=:oppId AND
                            (Status__c='Submitted to Partner' OR Status__c='Accepted by Partner') Limit 1];  
                }
                else{
                    //#1340 - added 'Is_this_a_Cross_Border_Deal__c, In_Which_Countries__c' into query
                    //#1641 - added 'Distributor_In_Origin_Country__c' into query
                    //#1825 - added 'Is_this_an_EHC_deal__c' into query
                    objOpptyReg = [Select Id,Status__c,Partner_Approval_Status__c,Customer_Name__c,Partner_Rejection_Reason__c,
                            Partner_Comments__c,Presales_Engineer_Phone__c,Presales_Engineer_Name__c,Presales_Engineer_Email__c,Is_this_a_hosting_Opportunity__c,Is_Presales_Engineer_Accredited__c,Service_Delivery__c,Location_Type__c,Multiple_Ship_To__c,
                            Expected_Close_Date__c,Expected_Deal_Value__c,Deal_Description__c,Customer_Phone_Number__c,Who_is_the_end_user__c,Partner_Sales_Rep_Email__c, Partner_Sales_Rep_Name__c,
                            Customer_Email__c,EMC_Contact_Email__c,Related_Opportunity__r.Expected_Close_Date__c,EMC_Contact_Name__c,Related_Opportunity__r.Expected_Deal_Value_del__c,Next_Steps__c,Related_Opportunity__r.Is_this_a_VSPEX_Opportunity_Partner__c,Related_Opportunity__r.Is_this_a_VSPEX_Opportunity1__c,Related_Opportunity__r.VMWare_Related__c, Is_This_a_VSPEX_Solution_Opportunity__c,Is_this_a_Cross_Border_Deal__c, In_Which_Countries__c,Distributor_In_Origin_Country__c,Is_this_an_EHC_deal__c from Opportunity_Registration__c where Related_Opportunity__c=:oppId AND
                            (Status__c='Submitted to Partner' OR Status__c='Accepted by Partner') AND (Distributor_Direct_Reseller_lookup__c =:usr.AccountId OR Distribution_VAR_lookup__c =:usr.AccountId) Limit 1];
                }
            //Setting values on page
            objLead.DealReg_Expected_Close_Date__c = objOpptyReg.Expected_Close_Date__c;
            objLead.DealReg_Expected_Deal_Value__c = objOpptyReg.Expected_Deal_Value__c;
            CustomerName= objOpptyReg.Customer_Name__c;
            CustomerEmail= objOpptyReg.Customer_Email__c;
            CustomerPhoneNumber=objOpptyReg.Customer_Phone_Number__c;
            DealDescription=objOpptyReg.Deal_Description__c;
            //ExpectedCloseDate=objOpptyReg.Related_Opportunity__r.Expected_Close_Date__c;
            MultipleShipTo=objOpptyReg.Multiple_Ship_To__c;
            LocationType=objOpptyReg.Location_Type__c;
            ApprovalStatus=objOpptyReg.Partner_Approval_Status__c;
            RejectionReason=objOpptyReg.Partner_Rejection_Reason__c;
            PartnerComments=objOpptyReg.Partner_Comments__c;
            //ExpectedDealValue=objOpptyReg.Related_Opportunity__r.Expected_Deal_Value_del__c;
            EMCEmail=objOpptyReg.EMC_Contact_Email__c;
            EMCName=objOpptyReg.EMC_Contact_Name__c;
            PartnerRepEmail=objOpptyReg.Partner_Sales_Rep_Email__c;
            PartnerRepName=objOpptyReg.Partner_Sales_Rep_Name__c;
            isHosting = objOpptyReg.Is_this_a_hosting_Opportunity__c;
            isServiceDelivery = objOpptyReg.Service_Delivery__c;
            endUser = objOpptyReg.Who_is_the_end_user__c;
            EngineerAccredited = objOpptyReg.Is_Presales_Engineer_Accredited__c;
            enggEmail = objOpptyReg.Presales_Engineer_Email__c;
            enggPhone = objOpptyReg.Presales_Engineer_Phone__c;
            enggName = objOpptyReg.Presales_Engineer_Name__c;
            IsTHisVSPEX= objOpptyReg.Related_Opportunity__r.Is_this_a_VSPEX_Opportunity1__c;
            VMWareRelated = objOpptyReg.Related_Opportunity__r.VMWare_Related__c;
            SalesCollaComments = objOpptyReg.Next_Steps__c;
            isVSPEX = objOpptyReg.Is_This_a_VSPEX_Solution_Opportunity__c;
            // Added for 1340 Start
            IsThisCross = objOpptyReg.Is_this_a_Cross_Border_Deal__c;
            //InWhich = objOpptyReg.In_Which_Countries__c;

            //added for 1641
            objLead.DealReg_Distributor_In_Origin_Country__c = objopptyReg.Distributor_In_Origin_Country__c;
            // End for 1340
            
            //added for #1825
            objLead.Is_this_an_EHC_deal__c = objopptyReg.Is_this_an_EHC_deal__c;
            //#1825 - End
            
            //Changes for In Which Country

            leftselected = new List<String>();
            rightselected = new List<String>();

            //Setting Value to Right Side

            if(objOpptyReg.In_Which_Countries__c!=null){
            for(String inWhichSelected : (objOpptyReg.In_Which_Countries__c).split(';')){
                System.debug('inWhichSelected--->'+inWhichSelected);
                rightvalues.add(inWhichSelected.trim());
                rightselected.add(inWhichSelected.trim());

            }
            }
            //End of Right Side

            //Setting value to Left Side of mutiseelct picklist
            List<SelectOption> countryList = new List<SelectOption>();
            countryList.addAll(getPicklistFieldValues('Opportunity','In_Which_Countries__c'));
            for(SelectOption opt : countryList ){
                if(!rightvalues.contains(String.valueof(opt.getValue())))
                    leftvalues.add(String.valueof(opt.getValue()).trim());
            }

            //End of Left Side Value
            

            
            //End of In Which Country

            opp=[Select Id,VMWare_Related__c,Partner_Approval_Date_Time__c,Partner_Rejection_Reason__c,Is_this_a_VSPEX_Opportunity_Partner__c,Partner_Expected_Deal_Value__c,Partner_Expected_Close_Date__c from Opportunity where id =:oppId limit 1];
                    
            }
            catch(Exception e){
                system.debug('e--->'+e);
                ApexPages.Message msg1 = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollab_PartnerError);
                ApexPages.addMessage(msg1);
                noRecords = false;
            }
        }
    }
    /* @Method <saveReg(): method executes on click of Save button on Partner Acceptance Page.> 
       @param - <void>
       @return - <PageReference>
    */
    public PageReference saveReg(){
        Boolean isSuccess;
        try{
            objOpptyReg.Partner_Approval_Date_Time__c = System.Now();
            objOpptyReg.Partner_Approving_User__c = UserInfo.getUserId();
            objOpptyReg.Expected_Close_Date__c= objLead.DealReg_Expected_Close_Date__c;
            objOpptyReg.Multiple_Ship_To__c=MultipleShipTo;
            objOpptyReg.Location_Type__c=LocationType;
            objOpptyReg.Partner_Approval_Status__c=ApprovalStatus;
            objOpptyReg.Partner_Rejection_Reason__c= RejectionReason;
            objOpptyReg.Partner_Comments__c= PartnerComments;
            objOpptyReg.Expected_Deal_Value__c= objLead.DealReg_Expected_Deal_Value__c;
            objOpptyReg.Next_Steps__c=SalesCollaComments ;
            objOpptyReg.Is_this_a_hosting_Opportunity__c = isHosting;
            objOpptyReg.Service_Delivery__c = isServiceDelivery;
            objOpptyReg.Who_is_the_end_user__c = endUser;
            objOpptyReg.Is_Presales_Engineer_Accredited__c = EngineerAccredited;
            objOpptyReg.Presales_Engineer_Email__c = enggEmail;
            objOpptyReg.Presales_Engineer_Phone__c = enggPhone;
            objOpptyReg.Presales_Engineer_Name__c = enggName;
            //Update for WR#1435 Start
            objOpptyReg.Partner_Sales_Rep_Email__c = PartnerRepEmail;
            objOpptyReg.Partner_Sales_Rep_Name__c = PartnerRepName; 
            //Update for WR#1435 End
            opp.VMWare_Related__c=VMWareRelated ;
            opp.Is_this_a_VSPEX_Opportunity_Partner__c = isVSPEX;
           // Added for 1340 start
            objOpptyReg.Is_this_a_Cross_Border_Deal__c = IsThisCross;
            String selectCountry = '';
            System.debug('rightselected--->'+rightselected);
            System.debug('rightvalues--->'+rightvalues);
            for(String selectedCountry :rightvalues )
                    selectCountry += selectedCountry.trim()+';';

            objOpptyReg.In_Which_Countries__c = selectCountry.removeend(';');
            //added for 1641
             objopptyReg.Distributor_In_Origin_Country__c = objLead.DealReg_Distributor_In_Origin_Country__c;
           //End for 1340
            //added for #1825
            objopptyReg.Is_this_an_EHC_deal__c = objLead.Is_this_an_EHC_deal__c;
            //#1825 - End
            opp.Partner_Expected_Deal_Value__c = objLead.DealReg_Expected_Deal_Value__c;
            opp.Partner_Expected_Close_Date__c = objLead.DealReg_Expected_Close_Date__c;
           
            if(UserInfo.getFirstName() != null){
                opp.Partner_Approving_User__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
            }
            else {
                opp.Partner_Approving_User__c = UserInfo.getLastName();
            }
            
            if(objOpptyReg.Partner_Approval_Status__c!= null && objOpptyReg.Partner_Approval_Status__c.equals('Accepted')){
                if(opp.Partner_Expected_Deal_Value__c != null && opp.Partner_Expected_Deal_Value__c != 0.00 && opp.Partner_Expected_Close_Date__c != null){
                    objOpptyReg.Status__c = 'Accepted by Partner';                
                    opp.Shared_to_Partner__c = 'YES';// added the below line for CDC Req# 1576
                    blnOpptyApproval = true;
                    Database.SaveResult srOpp = Database.Update(opp);
                    Database.SaveResult srObjOpptyRegp = Database.update(objOpptyReg);                
                    
                    isSuccess = true;
                }
                else{
                    ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollab_PartnerAcceptance);
                    ApexPages.addMessage(msg3);
                    isSuccess = false;  
                }
            }
            else if(objOpptyReg.Partner_Approval_Status__c!= null && objOpptyReg.Partner_Approval_Status__c.equals('Rejected')){
                if(objOpptyReg.Partner_Rejection_Reason__c != null && (objOpptyReg.Partner_Comments__c != '' && objOpptyReg.Partner_Comments__c != null)){
                    objOpptyReg.Status__c = 'Rejected by Partner';
                    opp.Partner_Approval_Date_Time__c = System.Now();
                    opp.Partner_Rejection_Reason__c = RejectionReason;   
                    
                    blnOpptyApproval = true;

                    Database.SaveResult srObjOpptyRegp = Database.Update(objOpptyReg);
                    Database.SaveResult srOpp = Database.Update(opp);
                    isSuccess = true;
                }
                else{
                    ApexPages.Message msg2 = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollab_PartnerRejection);
                    ApexPages.addMessage(msg2);
                    isSuccess = false;
                }
            }
            else{
                blnOpptyApproval = true;

                Database.SaveResult srOpp = Database.update(opp);
                Database.SaveResult srObjOpptyRegp = Database.update(objOpptyReg);
                isSuccess = true;   
            }
           
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        if (objOpptyReg.Partner_Approval_Status__c!= null && objOpptyReg.Partner_Approval_Status__c.equals('Rejected') && isSuccess == true) 
        {
            return new PageReference('/006/o');
        }
        else if(isSuccess == true)
        {
            return new PageReference('/'+oppId); 
        }
        else{
            return null;
        }
    }
    /* @Method <cancelReg(): method executes on click of Cancel button on Partner Acceptance Page.> 
       @param - <void>
       @return - <PageReference>
    */
    public PageReference cancelReg(){
        if(oppId != null) {
            return new PageReference('/'+oppId);
        } else {
            return new PageReference('/006/o');
        }
    }

    /* @Method <getPicklistFieldValues(): Private methos to get Picklist values from Objects.> 
       @param - String Object and String Field API Names
       @return - List<SelectOption> i.e. List of Picklist values
    */
    private List<SelectOption> getPicklistFieldValues(String strObjectAPIName, String strFieldAPIName)
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(strObjectAPIName);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType objOppReg = Object_name.getSObjectType(); 
        Schema.DescribeSObjectResult objDescribe = objOppReg.getDescribe();       
        Map<String, Schema.SObjectField> mapFieldAndSchemaObject = objDescribe.fields.getMap(); 
        List<Schema.PicklistEntry> values = mapFieldAndSchemaObject.get(strFieldAPIName).getDescribe().getPickListValues();
        system.debug('values--->'+values);
        for (Schema.PicklistEntry a : values)
        { 
            options.add(new SelectOption(a.getLabel(), a.getValue())); 
        }
        
        return options;
    }





  public PageReference selectclick(){
        rightselected.clear();
        for(String s : leftselected){
            leftvalues.remove(s);
            rightvalues.add(s);
            rightselected.add(s);
        }
        return null;
    }
     
    public PageReference unselectclick(){
        leftselected.clear();
        for(String s : rightselected){
            rightvalues.remove(s);
            leftvalues.add(s);
            leftselected.add(s);
        }
        return null;
    }
 
    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(leftvalues);
        tempList.sort();
        for(string s : tempList)
            options.add(new SelectOption(s,s));
        return options;
    }
 
    public List<SelectOption> getSelectedValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(rightvalues);
        tempList.sort();
        for(String s : tempList)
            options1.add(new SelectOption(s,s));
        return options1;
    }



}