/*
DATE         Developer Name      WR No              Description
-----        --------------      ----------         -----------------------------------------------------------  
17-07-2014   Sayan Choudhury     CI WR 1093         Created this extension controller to implement the logic

*/
public with sharing class customExtension {
    public List<Custom_Content__c> CustmCont{get;set;}
    public customExtension(ApexPages.StandardController controller) {
     CustmCont=[Select id,Content__c,name from Custom_Content__c where name = 'RMSA Agreement' limit 1 ];
    }

}