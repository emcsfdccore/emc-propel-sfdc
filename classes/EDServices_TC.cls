/*==================================================================================================================+

|  HISTORY  |                                                                           

|  DATE          DEVELOPER      WR        DESCRIPTION        

|  04/03/2014     Abinaya        WR:567  call saveAttachment() method from EDServices_Controller
|  05/30/2014     Abinaya        WR:922  call EDServices_Emailmsg class from EDServices_Controller
|  07/14/2014     Abinaya        WR:847  updated Origin field values as 'OBC' to increase the coverage.
|  09/17/2014     Bindu          WR:1294 call renderFields() method from EDServices_Controller
|  10/11/2014     Vinod          CI:1488 Added record type "EMC Proven Professional Support"
|  1/15/2015      Bindu          CI:1690 Added record type "Certificate Support"
|  2/17/2015      Bindu          CI:1697 Added record type "Director Development Support"
+==================================================================================================================**/


@isTest(SeeAllData=true)
public class EDServices_TC{
    public static TestMethod void edServices()
    {   
        system.runAs(new user(Id = UserInfo.getUserId())){    
            
            List<GBS_Config__c>  GBSConfigList =  new List<GBS_Config__c>();
        
            GBSConfigList = GBS_Config__c.getAll().Values();
            
            if(GBSConfigList != null && GBSConfigList.size() > 0){
                delete GBSConfigList;
            }
            
            CustomSettingDataHelper.GBSConfigData();
        }
        
        Education_Services_Contact__c edserv=new Education_Services_Contact__c();
        edserv.Name='test@test.com';
        //edserv.Id=''';
        String rtName = System.label.EDServices_RecordType;
       // List<RecordType> rt = [Select Id, Name from RecordType where SobjectType = 'case' and developername  in ('Education_Services_Proven','Education_Services_Record_Type')];
        RecordType rt = [Select Id, Name from RecordType where DeveloperName=:rtName];
        Case c= new Case();
        //c.Id='';
        c.EDS_Contact_Email__c=edserv.Id;
        c.RecordTypeId = rt.Id;
        c.EDS_Email__c = 'test@test.com';
        c.Origin='OBC';
        //insert c;
        
        //Creating New Attachment Object and creating a reocrd for attachment object
        Attachment att = new Attachment();     
        List<Attachment> attLst = new List<Attachment>();  
        String myString = 'StringToBlob';
        Blob myBlob = Blob.valueof(myString);
        //List<Case> lst_Case = [Select ID,Escalation__c,IsEscalated,CaseNumber,Record_Type_Hidden__c from Case where Id =:lstCase];
         //System.debug('CASELIST__>'+caseLst);
        att.ParentId=c.Id;
        att.Body=myBlob;
        att.Name='File1';
        att.ContentType = 'doc,txt,jpeg';
        attLst.add(att);
        //Test.StartTest();
        try{
            Database.insert(edserv);
        }
        catch(DmlException e){
            //String s = (String)e;
            system.assert(e.getMessage().contains('An active Education Services Contact with this email address already exists in Salesforce.com.'));
        }
        Database.insert(c);
        EmailMessage emsg= new EmailMessage();
        emsg.ParentId=c.Id;
        emsg.FromAddress = 'test1@test.com';
        emsg.Incoming = true;
        Database.insert(emsg);
        c.Status = 'Resolved';
        Database.update(c);
        
        system.debug('edserv--->'+edserv);
        Education_Services_Contact__c edserv1 = new Education_Services_Contact__c();
        edserv1.Name = 'test1@test.com';
        try{
            Database.insert(edserv1);
        }
        catch(DmlException e){
            //String s = (String)e;
            system.assert(e.getMessage().contains('An active Education Services Contact with this email address already exists in Salesforce.com.'));
        }
        Education_Services_Contact__c edserv2 = new Education_Services_Contact__c();
        edserv2.Name = 'test2@test.com';
        Database.insert(edserv2);
        edserv2.Name = 'test3@test.com';
        Database.update(edserv2);
        
        ApexPages.currentpage().getParameters().put('Id',c.id); 
        //ApexPages.currentpage().getParameters().put('eDServicesContact.Name',edserv);
        ApexPages.StandardController controller = new ApexPages.StandardController(c);  
        //EDServices_Controller caseObj= new EDServices_Controller(controller);
        EDServices_Controller caseObj1= new EDServices_Controller();
        caseObj1.eDServicesContact=edserv;
        caseObj1.newAttachments=attLst;         
        //caseObj1.addMore();
        caseObj1.caseRecord.Type= 'Certificate Inquiry';
       caseObj1.Save();
       // added as part of 1294
       caseObj1.renderFields(); 
        //Call below method to increase the coverage.
        caseObj1.saveAttachment();
        //caseObj1.getApplicationID();
        caseObj1.createInstance();
        
        ApexPages.currentpage().getParameters().put('Id',c.id);
        ApexPages.currentpage().getParameters().put('eDServicesContact.Name','test2@test.com');
        ApexPages.StandardController controller1 = new ApexPages.StandardController(c);  
        //EDServices_Controller caseObj= new EDServices_Controller(controller);
        EDServices_Controller caseObj= new EDServices_Controller();
        caseObj.eDServicesContact.Name='test2@test.com';
        //caseObj.newAttachments=attLst;         
        //caseObj.addMore();
        caseObj.caseRecord.Type= 'EMC Proven Professional';
        caseObj.Save();
         // added as part of 1294
       caseObj.renderFields(); 
        //Call below method to increase the coverage.
        caseObj.saveAttachment();
        //caseObj.getApplicationID();
        caseObj.createInstance();
         //Called EDServices_Emailmsg class and its method to increase the coverage.
        // added for 1488
        EDServices_Controller caseObj2= new EDServices_Controller();
        caseObj2.eDServicesContact.Name='test3@test.com';
        caseObj2.caseRecord.Type= 'EMC Proven Professional Support';
        caseObj2.Save();
        // End of 1488
        //Added for 1690
        EDServices_Controller caseObj3= new EDServices_Controller();
        caseObj3.eDServicesContact.Name='test4@test.com';
        caseObj3.caseRecord.Type= 'Certificate Support';
         caseObj3.Save();
         //End of 1690
         //Added for 1697
        EDServices_Controller caseObj4= new EDServices_Controller();
        caseObj4.eDServicesContact.Name='test5@test.com';
        caseObj4.caseRecord.Type= 'Director Development Support';
         caseObj4.Save();
         //End of 1697
        List<Case> caseList = new List<Case>();
        caseList.add(c);
        EDServices_Emailmsg edService = new EDServices_Emailmsg();
        edService.autoAssignmentToQueue(caseList);
        
        Map<Id,FeedItem> mapFeed = new Map<Id,FeedItem>();
        FeedItem feedI = new FeedItem();
        feedI.ParentId = c.id;
        feedI.Body = 'Test body content';
        Test.starttest();
        insert feedI;
        List<FeedItem> FeedData = [SELECT ID, CreatedDate, CreatedById, CreatedBy.FirstName, CreatedBy.LastName, ParentId, Parent.Name, Body FROM FeedItem where id =:feedI.id];
        if(FeedData.size()>0){
            for(FeedItem feed:FeedData){
              String s = feed.ParentId;
              if(s.substring(0,3)=='500'){
                mapFeed.put(feed.Id,feed);  
              }  
            }
        }
        if(mapFeed.size()>0){
            edService.disableAutoClosureFeedItem(mapFeed);
        }
        Test.StopTest();
        
    } 
}