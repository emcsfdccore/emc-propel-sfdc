/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE           DEVELOPER     WR        DESCRIPTION                               
 |  ====           =========     ==         =========== 
 |  13 Dec 2011    Shipra       177094    SFA - Remove inactive users from Account Team.
 |  15 OCT 2012    Vivek       204934    SFA - Bug Fix for Batch Job that Removes Inactive Account Team Members
 |  18 Dec 2013     Avinash K   319143      Optimized the query on AccountTeamMember so that it does not time out giving an error 
 |  17 June 2014   Bhanu Prakash PROPEL   Replaced "AccountId__c" with "Account__c" for AccountTeamDelete__c instance 
  +===========================================================================*/
global class To_DeleteInactiveAccountTeamMemb implements Database.Batchable<Sobject> 
{
    public String Query;
    String PROC_EXC='TA_InsertInactiveUser';
    List <EMCException> errors_Lst = new List <EMCException>();
    String UserID = Userinfo.getUserId();
    boolean Flag;
    RunInactiveATbatch__c RIA;  

    final static String strlmt =  ATMLimit__c.getInstance().LimitNumber__c;
    //Main Query

    static String Default_AccountTeamMember_Query='Select AccountId, Id, UserId from AccountTeamMember where User.IsActive = false ';



    global Database.Querylocator start(Database.BatchableContext BC)
    {    
        if(Query==null|| Query.length()==0)
        {
            
            //Code added by Avinash for WR#319143 begins below

            Integer intStartDateVariable = 0, intEndDateVariable = 0;

            Date dtStartDate, dtEndDate = System.TODAY();

            Map<String,CustomSettingDataValueMap__c> mapData = CustomSettingDataValueMap__c.getAll();
                
            if(mapData.get('Start Date Number') != null && mapData.get('Start Date Number').Datavalue__c != null)
                intStartDateVariable = Integer.valueOf(mapData.get('Start Date Number').Datavalue__c);

            if(mapData.get('End Date Number') != null && mapData.get('End Date Number').Datavalue__c != null)
                intEndDateVariable = Integer.valueOf(mapData.get('End Date Number').Datavalue__c);

            if (intStartDateVariable != 0) 
                dtStartDate = System.Today() - intStartDateVariable;

            if (intEndDateVariable != 0) 
                dtEndDate = System.Today() - intEndDateVariable;


            //Updating the Where clause based on Start Day and End Day numbers from the Custom Settings
            if (intStartDateVariable == 0 && intEndDateVariable == 0)
                Default_AccountTeamMember_Query += ' and User.LastModifiedDate <= TODAY ';

            else if (dtStartDate != null) 
                Default_AccountTeamMember_Query += ' and User.LastModifiedDate > :dtStartDate and User.LastModifiedDate <= :dtEndDate ';
            else
                Default_AccountTeamMember_Query += ' and User.LastModifiedDate <= :dtEndDate ';


            //Code added by Avinash for WR#319143 ends above

            Default_AccountTeamMember_Query += ' LIMIT ' + strlmt + '';

            Query = Default_AccountTeamMember_Query;      
            
            System.debug('#### Query----------'+Query);
        }  
        return Database.getQueryLocator(Query);
    }
    

    global void execute(Database.BatchableContext BC,List<sObject> scope)
    {  
        system.debug('Testing limit'+ATMLimit__c.getInstance().LimitNumber__c);
        
        RIA = RunInactiveATbatch__c.getValues('FlagStatus');
        System.debug('***** RIA = ' + RIA);
        if(RIA.StatusFlag__c !=null)
        {
            Flag = RIA.StatusFlag__c;
        }
        
        
        //Start:Inserting Data into AccountTeamDelete Object
        try
        {
            if (Flag !=false)
            {              
                Database.SaveResult[] saveResults = null;//This array is used here to get delete results.
                List<AccountTeamDelete__c> lstATD = new List<AccountTeamDelete__c>();
                List<AccountTeamMember> objATM = scope;
                for(AccountTeamMember a:objATM)
                {
                    AccountTeamDelete__c objATD = new AccountTeamDelete__c();
                    objATD.UserID__c = a.UserId;
                    objATD.Account__c = a.AccountId;              
                    lstATD.add(objATD);     
                }    
                System.debug('*** before insert ATD records *****');
                saveResults=Database.Insert(lstATD);
                System.debug('**** saveResults.size = '+ saveResults.size());
                InsertAccntDel(saveResults);
                lstATD.clear();
            }
        }
        catch (Exception Ex)
        {
        //  for (Integer i = 0; i < Ex.getNumDml(); i++) { System.DmlException  
        System.debug('---To_DeleteInactiveAccountTeamMemb----'+Ex); 
        // }
        } 
        //End   

    }
    
    public void InsertAccntDel(Database.SaveResult[] SaveResults)
    {
        for(Integer i=0;i<SaveResults.size();i++)
        {
            String dataError='';
            Database.SaveResult result=SaveResults[i];
            if(!result.isSuccess())
            {
                for(Database.Error dr:result.getErrors())
                {
                    dataError+=dr.getMessage();  
                }
                errors_Lst.add(new EMCException(dataError,'To_InsertInactiveAccountTeamMemb',new String[] {SaveResults[i].id}));
                EMC_UTILITY.logErrors(errors_Lst);
                errors_Lst.clear();
            }
        }
    }

    global void finish(Database.BatchableContext BC)
    {        
        //WR204934 finally Set to false the statusFlag
        RIA = RunInactiveATbatch__c.getValues('FlagStatus');
        if(RIA.StatusFlag__c !=null)
        {
            RIA.StatusFlag__c = false;
            update RIA;
        }
        system.debug('Flag'+RIA.StatusFlag__c);
    }

}