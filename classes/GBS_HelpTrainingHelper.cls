public class GBS_HelpTrainingHelper {
    
    public static void helpTrainGroupContentValidation(List<FeedItem> newFeedItemList){
        
        List<String> chatterGrpNameList = new List<String>();
        
        GBS_Config__c gbsConfigExt = GBS_Config__c.getValues('GBSHelpTrainingGroupExternal');
        GBS_Config__c gbsConfigEmp = GBS_Config__c.getValues('GBSHelpTrainingGroupEmployee');
        
        chatterGrpNameList.add(gbsConfigExt.ParamValue__c);
        chatterGrpNameList.add(gbsConfigEmp.ParamValue__c);
        
        Profile userProfile = [Select p.PermissionsModifyAllData, p.Name, p.Id From Profile p where Id =: UserInfo.getProfileId()];
        
        if(chatterGrpNameList != null && chatterGrpNameList.size() > 0){
            
            Map<Id, CollaborationGroup> gbsHelpTrainGrpMap = new Map<Id, CollaborationGroup>([Select Id, Name from CollaborationGroup where Name IN: chatterGrpNameList]);
            
            List<CollaborationGroupMember> collabGrpMemList = [Select c.Id, c.CollaborationRole, c.MemberId, c.CollaborationGroupId From CollaborationGroupMember c where CollaborationGroupId IN: gbsHelpTrainGrpMap.keySet() AND CollaborationRole = 'Admin' AND MemberId =: UserInfo.getUserId()];
            
            if(collabGrpMemList.isEmpty() || collabGrpMemList.size() == 0){
                
                for(FeedItem newFeed : newFeedItemList){
        
                    if(gbsHelpTrainGrpMap.containsKey(newFeed.ParentId) && !userProfile.PermissionsModifyAllData){
                    
                        newFeed.addError(System.Label.GBSChatterContentPostAccessMsg);                    
                    }
                }
            }           
        }     
    }
}