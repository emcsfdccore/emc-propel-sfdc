/*====================================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER                WR       DESCRIPTION                               
 |  ====          =========                ==       =========== 
 |  22-Nov-2011   Prasad                            This class has been scheduled for Matching and VPP jobs run. 
                                                    This has been taken out from ESBI webserive. 
                                                    
 | 28-Dec-2011    Anand Sharma                      Added  Theater Job to run getStagingTheaterJob
 | 02-Feb-2012    Anand Sharma                      Added Partner Lev Jobs  
 | 03-May-2012    Anirudh Singh                     Added Functional Area Job    
   05/08/2012    Prasad                             VPP improvement 
   09/05/2012    Prasad                             VPP PAN Delta   
 +==================================================================================================================*/
global class PRM_ESBI_VPP_Jobs_Theater_Tier_Scheduler implements Schedulable {       
    
     Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll(); 
     Map<String,BusinessPartnerProgram__c> bppCSMap = BusinessPartnerProgram__c.getAll();
     PRM_JobFactory_Theater_Tier JobFactory = new PRM_JobFactory_Theater_Tier();
        
    global void execute(SchedulableContext sc){
        
        //Code added for Feb release by Srikrishna Starts here
        //CustomSettingDataValueMap__c ESBI_VPP_Scheduled_Theaters2 = DataValueMap.get('ESBI_VPP_Scheduled_Theaters2');
        //String theatersJob2=ESBI_VPP_Scheduled_Theaters2.datavalue__c;
        BusinessPartnerProgram__c ESBI_VPP_Scheduled_Theaters2 = bppCSMap.get('ESBI_VPP_Scheduled_Theaters2');
        String theatersJob2=ESBI_VPP_Scheduled_Theaters2.APInames__c;
        /*CustomSettingDataValueMap__c ESBI_VPP_Scheduled_Tiers_Job1 = DataValueMap.get('ESBI_VPP_Scheduled_Tiers_Job1');
        String tiersJob1=ESBI_VPP_Scheduled_Tiers_Job1.datavalue__c;
        CustomSettingDataValueMap__c ESBI_VPP_Scheduled_Tiers_Job2 = DataValueMap.get('ESBI_VPP_Scheduled_Tiers_Job2');
        String tiersJob2=ESBI_VPP_Scheduled_Tiers_Job2.datavalue__c;*/        
        //Code added for Feb release by Srikrishna Ends here
        List<SequencialBatchJob>LstSeqJobs=new List<SequencialBatchJob>();
         
        // Verify Grouping theaters
        LstSeqJobs.add(JobFactory.getStagingTheaterJob());
        //Create Education Master
        LstSeqJobs.add(JobFactory.getEducationMasterJob());    
              
           if(theatersJob2.indexOf('All')!=-1){ 
               LstSeqJobs.addALL(getJobs('All'));
               SequencialBatchJobScheduler.executeSequence(LstSeqJobs);
               
               System.debug(LstSeqJobs.size());
               for(SequencialBatchJob job :LstSeqJobs){
                   System.debug('name  ' +job.getjobName());
               }                
               return;
            }
           if(theatersJob2.indexOf('APJ')!=-1){ 
               LstSeqJobs.addALL(getJobs('APJ'));
               
            }
           if(theatersJob2.indexOf('EMEA')!=-1){ 
              LstSeqJobs.addALL(getJobs('EMEA'));
          } 
           if(theatersJob2.indexOf('Americas')!=-1){ 
               LstSeqJobs.addAll(getJobs('Americas'));
          }            
       System.debug(LstSeqJobs.size());
       for(SequencialBatchJob job :LstSeqJobs){
           System.debug('name  ' +job.getjobName());
       }           
            
       SequencialBatchJobScheduler.executeSequence(LstSeqJobs);

    } 
    
    private List<SequencialBatchJob> getJobs(String Theater){
    
         List<SequencialBatchJob>LstSeqJobs=new List<SequencialBatchJob>();
         
        
         CustomSettingDataValueMap__c ESBI_VPP_Scheduled_Jobs = DataValueMap.get('ESBI_VPP_Scheduled_Jobs');
         String jobs = ESBI_VPP_Scheduled_Jobs.datavalue__c;
          
          LstSeqJobs.addAll(JobFactory.getEducationMatchingJobs(Theater));
          
          //Added by Jaypal for BPP - 05 Mar 2015
          LstSeqJobs.addAll(JobFactory.getEducationEducationMasterJob(Theater));
          
          //Commented below Level = All and added new condition for BPP Feb release.
          /*if(jobs.indexOf('VPP - Country Level,VPP - Logical Block Level,VPP - PAN Theater Level')!=-1){ 
             LstSeqJobs.addAll(JobFactory.getVPPJobs(Theater,'All')); 
          
          }else {
              if(jobs.indexOf('VPP - Country Level')!=-1){ 
                  LstSeqJobs.addAll(JobFactory.getVPPJobs(Theater,'Country')); 
              
              }if(jobs.indexOf('VPP - Logical Block Level')!=-1){ 
                  LstSeqJobs.addAll(JobFactory.getVPPJobs(Theater,'Logical Block')); 
              
              }if(jobs.indexOf('VPP - PAN Theater Level')!=-1){ 
                  LstSeqJobs.addAll(JobFactory.getVPPJobs(Theater,'PAN Theater')); 
              }
          }
          */
          if(jobs.indexOf('VPP - Country Level')!=-1){ 
              LstSeqJobs.addAll(JobFactory.getVPPJobs(Theater,'Country')); 
          
          }if(jobs.indexOf('VPP - Logical Block Level')!=-1){ 
              LstSeqJobs.addAll(JobFactory.getVPPJobs(Theater,'Logical Block')); 
          
          }if(jobs.indexOf('VPP - PAN Theater Level')!=-1){ 
              LstSeqJobs.addAll(JobFactory.getVPPJobs(Theater,'PAN Theater')); 
          }
             /* commented as part of partner leverage code cleanup under platform innovation project  
          if(jobs.indexOf('Partner Leverage')!=-1){ 
              LstSeqJobs.addAll(JobFactory.getPartnerLevrageJobs(Theater)); 
          } */
          if(jobs.indexOf('Funcational Area')!=-1){ 
              LstSeqJobs.add( JobFactory.getFunctionalAreaJob(Theater));
          } 
         
         return LstSeqJobs; 
    }
}