@istest

public class Cases_PSC_Portal_Success_Controller_TC
{
    public static testmethod void PSCPortalCntrlTest()
    {
        System.runAs(new user(Id = UserInfo.getUserId()))
            {    
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
                CustomSettingDataHelper.dealRegistrationCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.partnerBoardingCSData();
                CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
                CustomSettingDataHelper.VCEStaticCSData();
                CustomSettingDataHelper.PSCCaseTypeImageData();
                CustomSettingDataHelper.PSCCaseTypeImageSiteData();
                CustomSettingDataHelper.PSCFieldMappingData();
                CustomSettingDataHelper.PSCCasesLanguageData();
           }
        
        Cases_PSC_Portal_Success_Controller PSCCntrl = new Cases_PSC_Portal_Success_Controller();
        PSCCntrl.backtoCase();
    
    }
}