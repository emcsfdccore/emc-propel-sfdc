/*==================================================================================================================+

 |  HISTORY  |                                                                            

 |  DATE          DEVELOPER      WR        DESCRIPTION                              

 |  ====          =========      ==        =========== 
 |  06/18/2014    Sneha Jain    PROPEL      FD.I.002 PRM Attributes  Helper Class Initial Creation
 |  20 Oct 2014   Bhanuprakash  PROPEL      Commented 'updatePartnerTypeAttr' method,
 +==================================================================================================================*/

 public class PROPEL_PRMAttributesHelper{
 
    //When an Account is changed to no longer be a Partner Quoting Account, the Quoting Attributes need to be nullified
    public static void setQuotingAttributes(List <Account> accountList,Map<Id,Account> accOldMap){
        List<Account> accList = new List<Account>();
        for(Account acc : accountList){
            if(!acc.Partner_Quoting_Account__c && accOldMap.get(acc.Id).Partner_Quoting_Account__c){
                acc.Quoting_EPOA__c = false;
                acc.Quoting_Autobook_Enabled__c = false;
                acc.Quoting_Payment_Terms__c = '';
                acc.Quoting_Freight_Terms__c = '';
                acc.Quoting_FOBIncoterms__c = '';
            }
        }
    }
    

    //For all new Partner Attribute Type records, insert a record in Outbound Message Log object
    public static void insertPartnerTypeAttr(List <Partner_Type_Attribute__c> ptaList){
    
        List<Partner_Type_Attribute__c> insertPTAList = new List<Partner_Type_Attribute__c>();
    
        for (Partner_Type_Attribute__c pta : ptaList){
            insertPTAList.add(pta);
        }
        insertOMLrecord(insertPTAList,'Upsert Partner Type Attributes');
    }
    
    
    //For all updated Partner Attribute Type records, insert a record in Outbound Message Log object
    public static void updatePartnerTypeAttr(List <Partner_Type_Attribute__c> ptaList,Map<Id,Partner_Type_Attribute__c> ptaOldMap){
    
        List<Partner_Type_Attribute__c> updatePTAList = new List<Partner_Type_Attribute__c>();
    
        for (Partner_Type_Attribute__c pta : ptaList){
            if((pta.Partner_Account__c != ptaOldMap.get(pta.id).Partner_Account__c) || (pta.Business_Partner_Program_Tier__c != ptaOldMap.get(pta.id).Business_Partner_Program_Tier__c)){
            
                updatePTAList.add(pta);
            }   
        }
        insertOMLrecord(updatePTAList,'Upsert Partner Type Attributes');
    }
    
    
    //For all deleted Partner Attribute Type records, insert a record in Outbound Message Log object
    public static void deletePartnerTypeAttr(List <Partner_Type_Attribute__c> ptaList){
    
        List<String> partnerAccList = new List<String>();
        Map<Id,List<String>> ptaKeyMap = new Map<Id,List<String>>();
        Map<Id,String> partnerAccUcidMap = new Map<Id,String>();
        
        //Collect the Ids of Partner Accounts on all the Partner Type Attribute records
        for (Partner_Type_Attribute__c pta : ptaList){
            partnerAccList.add(pta.Partner_Account__c);
        }
        
        //Query the Partner Quoting Account UCID for all the Partner Accounts collected
        if(partnerAccList != null && partnerAccList.size()>0)
        {
            List<Account> ptaUcidList = [select Id,Partner_Quoting_Account_UCID__c from Account where Id IN: partnerAccList];
            
            if(ptaUcidList != null && ptaUcidList.size()>0){
                for(Account acc: ptaUcidList){
                    partnerAccUcidMap.put(acc.Id,acc.Partner_Quoting_Account_UCID__c);
                }
            }
        }
        
        //Populate the map with the details of each record being deleted. keyList contains values of Partner Account UCID, Partner Type Price List and NULL in order
        for (Partner_Type_Attribute__c pta : ptaList){
            List<String> keyList = new List<String>();
            if(partnerAccUcidMap !=null && partnerAccUcidMap.containsKey(pta.Partner_Account__c)){
                keyList.add(partnerAccUcidMap.get(pta.Partner_Account__c));
            }   
            else{
                keyList.add(null);
            }
            keyList.add(pta.Partner_Price_List_Type__c);
            keyList.add(null);
            ptaKeyMap.put(pta.Id,keyList);
        }   
        
        //Call the method to make an extry in OML object
        insertOMLforDeleteRecords(ptaKeyMap,'Delete Partner Type Attributes');
    }
    
    
    //For all new Partner Quoting Relationships records, insert a record in Outbound Message Log object
    public static void insertPartnerQuotingRel(List <Partner_Quoting_Relationship__c> pqrList){
    
        List<Partner_Quoting_Relationship__c> insertPQRList = new List<Partner_Quoting_Relationship__c>();
    
        for (Partner_Quoting_Relationship__c pqr : pqrList){
            insertPQRList.add(pqr);
        }
        insertOMLrecord(insertPQRList,'Upsert Partner Quoting Relationships');
    }
    
    
    //Make an entry in Outbound message Log object for Delete PQR only when the combination of "PQA UCID - Related Acc ID - Rel Type" is unique in the database
    public static void deletePartnerQuotingRel(List <Partner_Quoting_Relationship__c> deletdPQRList){
    
        List<Partner_Quoting_Relationship__c> deletePQRList = new List<Partner_Quoting_Relationship__c>();
        Set<String> quotingAccUcidSet = new Set<String>();
        Map<String,List<Partner_Quoting_Relationship__c>> keyRelMap = new Map<String,List<Partner_Quoting_Relationship__c>>();
        Map<Id,List<String>> pqrKeyMap = new Map<Id,List<String>>();
        
        //Collect the record's Unique key formed with combination of "PQA UCID - Related Acc ID - Rel Type"
        for (Partner_Quoting_Relationship__c pqr : deletdPQRList){
            quotingAccUcidSet.add(pqr.Deletion_UniqueKey__c);
        }
        
        if(quotingAccUcidSet != null && quotingAccUcidSet.size()>0){
            List<Partner_Quoting_Relationship__c> pqrList = [select Id, Deletion_UniqueKey__c,Partner_Account__r.Partner_Quoting_Account_UCID__c,Related_Account_UCID__c ,Relationship_Type__c from Partner_Quoting_Relationship__c where Deletion_UniqueKey__c IN: quotingAccUcidSet];
            
            if(pqrList != null && pqrList.size()>0){
                //Create a map of Unique String as Key and list of associated Partner Quoting records as the value
                for(Partner_Quoting_Relationship__c pqr :pqrList){
                    if(keyRelMap.containsKey(pqr.Deletion_UniqueKey__c)){
                        List<Partner_Quoting_Relationship__c> tempRelList = keyRelMap.get(pqr.Deletion_UniqueKey__c);
                        tempRelList.add(pqr);
                        keyRelMap.put(pqr.Deletion_UniqueKey__c,tempRelList);
                    }
                    else{
                        List<Partner_Quoting_Relationship__c> tempRelList = new List<Partner_Quoting_Relationship__c>();
                        tempRelList.add(pqr);
                        keyRelMap.put(pqr.Deletion_UniqueKey__c,tempRelList);
                    }
                }
                
                for(String uniqKey :keyRelMap.keySet()){
                    //If the record being deleted is the only one in db with the unique combination, then add an entry to OML objects
                    if(keyRelMap.get(uniqKey).size() == 1){
                        
                        //Populate the map with the details of each record being deleted. keyList contains values of Partner Account UCID, Related Account UCID and Relationship Type in order
                        List<String> keyList = new List<String>();
                        Partner_Quoting_Relationship__c pqr = keyRelMap.get(uniqKey)[0];
                        keyList.add(pqr.Partner_Account__r.Partner_Quoting_Account_UCID__c);
                        keyList.add(pqr.Related_Account_UCID__c);
                        keyList.add(pqr.Relationship_Type__c);
                        pqrKeyMap.put(pqr.Id,keyList);
                    }
                }
            }
        }
        //Call the method to make an entry in OML object
        insertOMLforDeleteRecords(pqrKeyMap,'Delete Partner Quoting Relationships');
    }
    
    
    //For all new Partner Quoting Relationships records, insert a record in Outbound Message Log object
    public static void insertPartnerServiceEnabledProd(List <Partner_Service_Enabled_Product__c> psepList){
    
        List<Partner_Service_Enabled_Product__c> insertPSEPList = new List<Partner_Service_Enabled_Product__c>();
        
        for (Partner_Service_Enabled_Product__c psep : psepList){
            insertPSEPList.add(psep);
        }
        insertOMLrecord(insertPSEPList,'Upsert Partner Service Enabled Products');
    }
    
    
    //When a PSEP record is updated with either Account or Service Level Value,insert a record in Outbound Message Log object
    public static void updatePartnerServiceEnabledProd(List <Partner_Service_Enabled_Product__c> psepList, Map<Id,Partner_Service_Enabled_Product__c> psepOldMap){
    
        List<Partner_Service_Enabled_Product__c> updatePSEPList = new List<Partner_Service_Enabled_Product__c>();
        
        for (Partner_Service_Enabled_Product__c psep : psepList){
            if((psep.Partner_Account__c != psepOldMap.get(psep.id).Partner_Account__c) ){//|| (psep.Service_Level__c != psepOldMap.get(psep.id).Service_Level__c)){//Bhanu - Removed
                updatePSEPList.add(psep);
            }   
        }   
        insertOMLrecord(updatePSEPList,'Upsert Partner Service Enabled Products');
    }
    
    
    //For all deleted Partner Service Enabled Products records, insert a record in Outbound Message Log object
    public static void deletePartnerServiceEnabledProd(List <Partner_Service_Enabled_Product__c> psepList){
    
        List<String> partnerAccList = new List<String>();
        Map<Id,List<String>> psepKeyMap = new Map<Id,List<String>>();
        Map<Id,String> partnerAccUcidMap = new Map<Id,String>();
        
        //Collect the Ids of Partner Accounts on all the Partner Service Enabled Products records
        for (Partner_Service_Enabled_Product__c psep : psepList){
            partnerAccList.add(psep.Partner_Account__c);
        }
        
        //Query the Partner Quoting Account UCID for all the Partner Accounts collected
        if(partnerAccList != null && partnerAccList.size()>0)
        {
            List<Account> psepUcidList = [select Id,Partner_Quoting_Account_UCID__c from Account where Id IN: partnerAccList];
            
            if(psepUcidList != null && psepUcidList.size()>0){
                for(Account acc: psepUcidList){
                    partnerAccUcidMap.put(acc.Id,acc.Partner_Quoting_Account_UCID__c);
                }
            }
        }
        
        //Populate the map with the details of each record being deleted. keyList contains values of Partner Account UCID, Service Enabled Product and NULL in order
        for (Partner_Service_Enabled_Product__c psep : psepList){
            List<String> keyList = new List<String>();
            if(partnerAccUcidMap !=null && partnerAccUcidMap.containsKey(psep.Partner_Account__c)){
                keyList.add(partnerAccUcidMap.get(psep.Partner_Account__c));
            }   
            else{
                keyList.add(null);
            }
            keyList.add(psep.Service_Enabled_Product__c);
            keyList.add(psep.Service_Level__c);//Bhanu - changed passing 'null' value to 'Service_Level__c'
            psepKeyMap.put(psep.Id,keyList);
        }   
        //Call the method to make an entry in OML object
        insertOMLforDeleteRecords(psepKeyMap,'Delete Partner Service Enabled Products');
    }
    
    
    //Make an entry in Outbound message Log object for Delete Account Association only when the combination of "Account - Associated Account - Account Role" is unique in the database
    public static void deleteAccAssociation(List <APPR_MTV__RecordAssociation__c> deletdaccAList){
    
        Set<String> accAssociateSet =  new Set<String>();
        Set<String> accIdSet = new Set<String>();
        Map<String,List<APPR_MTV__RecordAssociation__c>> keyAccMap = new Map<String,List<APPR_MTV__RecordAssociation__c>>();
        Map<Id,List<String>> accAKeyMap = new Map<Id,List<String>>();
        
        //If the Account Role = Distributor, collect the record's Unique key formed with combination of "Account - Associated Account - Account Role"
        for(APPR_MTV__RecordAssociation__c accAsscn : deletdaccAList ){
            if(accAsscn.APPR_MTV__Account_Role__c == 'Distributor'){
                accAssociateSet.add(accAsscn.PROPEL_Deletion_UniqueKey__c);
                accIdSet.add(accAsscn.APPR_MTV__Account__c);
            }   
        }   
        if(accAssociateSet !=null && accAssociateSet.size() > 0 && accIdSet !=null && accIdSet.size()>0 ){

            List<APPR_MTV__RecordAssociation__c> accAssociateList = [select Id,PROPEL_Deletion_UniqueKey__c,APPR_MTV__Account__c,APPR_MTV__Account__r.Party_Number__c,APPR_MTV__Associated_Account__r.Partner_Quoting_Account_UCID__c,APPR_MTV__Account_Role__c from APPR_MTV__RecordAssociation__c where PROPEL_Deletion_UniqueKey__c IN: accAssociateSet AND APPR_MTV__Account__c IN: accIdSet];
            if(accAssociateList !=  null && accAssociateList.size()>0)
            {
                //Create a map of Unique String as Key and list of associated Account Association records as the value
                for(APPR_MTV__RecordAssociation__c recA : accAssociateList){
                    if(keyAccMap.containsKey(recA.PROPEL_Deletion_UniqueKey__c)){
                        List<APPR_MTV__RecordAssociation__c> tempAccList = keyAccMap.get(recA.PROPEL_Deletion_UniqueKey__c);
                        tempAccList.add(recA);
                        keyAccMap.put(recA.PROPEL_Deletion_UniqueKey__c,tempAccList);
                    }
                    else{
                        List<APPR_MTV__RecordAssociation__c> tempAccList = new List<APPR_MTV__RecordAssociation__c>();
                        tempAccList.add(recA);
                        keyAccMap.put(recA.PROPEL_Deletion_UniqueKey__c,tempAccList);
                    }
                }
                
                for(String uniqKey : keyAccMap.keySet()){
                    //If the record being deleted is the only one in db with the unique combination, then add an entry to OML objects
                    if(keyAccMap.get(uniqKey).size() == 1){
                        //Populate the map with the details of each record being deleted. keyList contains values of Account Partner Number, Associated Account's PQA UCID and Account Role in order
                        List<String> keyList = new List<String>();
                        APPR_MTV__RecordAssociation__c accA = keyAccMap.get(uniqKey)[0];
                        keyList.add(accA.APPR_MTV__Account__r.Party_Number__c);
                        keyList.add(accA.APPR_MTV__Associated_Account__r.Partner_Quoting_Account_UCID__c);
                        keyList.add(accA.APPR_MTV__Account_Role__c);
                        accAKeyMap.put(accA.Id,keyList);
                    }
                }
            }
        }   
        
        //Call the method to make an entry in OML object
        insertOMLforDeleteRecords(accAKeyMap,'Delete Account Association');
    }
    
    //Insert OML records for records with upsert action
    public static void insertOMLrecord(List<sObject> childList, String integrationOp){
        
        //Populate the value of RecordId and Integration Operation fields
        List<Outbound_Message_Log__c> omlList = new List<Outbound_Message_Log__c>();
        if(childList!= null && childList.size()>0){
            for(sObject rec : childList){
                Outbound_Message_Log__c oml= new Outbound_Message_Log__c();            
                oml.RecordId__c = rec.Id ;
                oml.Integration_Operation__c = integrationOp;
                omlList.add(oml);
            }   
        }
        
        if(omlList!= null && omlList.size()>0){
            try{
                //Setting the flag to false so as to avoid recursive Trigger run
                TriggerContextUtility.firstRun = false;
                insert omlList; 
            }
            catch(exception e){}
        }
    }
    
    
    //Insert OML records for records with delete action
    public static void  insertOMLforDeleteRecords(Map<Id,List<String>> childKeyMap, String integrationOp){
        
        List<Outbound_Message_Log__c> omlList = new List<Outbound_Message_Log__c>();
        
        //Populate the value of RecordId,Integration Operation and Key1, Key2 and Key3 fields
        if(childKeyMap !=null && childKeyMap.size()>0){
            for(Id rec : childKeyMap.keySet()){
                Outbound_Message_Log__c oml= new Outbound_Message_Log__c();            
                oml.RecordId__c = rec;
                oml.Integration_Operation__c = integrationOp;
                oml.Key1__c = childKeyMap.get(rec)[0];
                oml.Key2__c = childKeyMap.get(rec)[1];
                oml.Key3__c = childKeyMap.get(rec)[2];
                omlList.add(oml);
            }
        }
        
        if(omlList!= null && omlList.size()>0){
            try{
                //Setting the flag to false so as to avoid recursive Trigger run
                TriggerContextUtility.firstRun = false;
                insert omlList; 
            }
            catch(exception e){}
        }
    }       
}