/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 |  Sept/8/2013     Leonard V               This Class is used for sending mail to POC groups based on solution centers   
 |  Jan/14/2014     Bisna V P    CI 296     Updated the code to change the to addresses for the email notification                                                                        
 +==================================================================================================================**/

public class Presales_POC_MailNotification{

    public void sendPOCMail(List<Case> lstCase){

        List<Case> caseLSt = new List<Case>();
        Id templateId;
        List<Messaging.SingleEmailMessage> lstMsg = new List<Messaging.SingleEmailMessage>(); 
        Map<String,Id> contactMap = new Map<String,String>();

        for(Case caseObj : lstCase){

                if(caseObj.Record_Type_Developer_Name__c==Label.Presales_POC_Contact && caseObj.Solutions_Center__c!=null){

                        caseLSt.add(caseObj);

                }

        }

        //Getting Mail Temaplate

        templateId = [select id from EmailTemplate where Name = 'PreSales POC Case Created Queue Members'].id;

        //Gettng Lsit OF Solution Centers

        Map<String , Presales_POC_SolutionCenter__c> mapSolution= Presales_POC_SolutionCenter__c.getAll();

        

        List<Contact> contactLSt = [Select id,name,email from Contact where Presales_POC_Contact__c  = true and email!=null and name in :mapSolution.keySet() ];

        //Setting Values To Map 

        for(Contact contObj : contactLSt ){

            contactMap.put(contObj.name , contObj.id);


        }
        //Updated for WR CI 296
        
       Map<String,Presales_POC_Solution_Mails__c> solCentMails= Presales_POC_Solution_Mails__c.getall();      
       List<String> toAddresses = new List<String>();
       
       

        if(!caseLSt.isEmpty()){

            for(Case caseMailObj : caseLSt){

                Messaging.SingleEmailMessage sndMailObj = new Messaging.SingleEmailMessage(); 

                  sndMailObj.setTargetObjectId(contactMap.get(String.Valueof(caseMailObj.Solutions_Center__c)));
                 
                 sndMailObj.setWhatId(caseMailObj.Id); 
                 sndMailObj.setTemplateId(templateId); 
                 sndMailObj.SaveAsActivity = false;
                //
                 String emailsSolutionCenter = solCentMails.get(caseMailObj.Solutions_Center__c).Solution_Center_Emails__c; 
                 if(emailsSolutionCenter!=null)
                 toAddresses = emailsSolutionCenter.split(',');                                 
                 sndMailObj.setToAddresses(toAddresses);
                //Update ends for WR CI 296
                 
                lstMsg.add(sndMailObj);


            }

           try{
            System.debug('lstMsg--->'+lstMsg);
           if (!Test.isRunningTest()){
             Messaging.sendEmail(lstMsg);
                }

            }
            catch (Exception ex){

            System.debug('Inside Exception--->'+ex);


            }

        }
        
    }

}