public class OpportunityTestClassDataHelper{
    public static List<Opportunity> createOpptys(Account acc, Lead lea){
     List<Opportunity> opptyList = new List<Opportunity>();
     Opportunity Oppty1 = new Opportunity();
        Oppty1.Name = 'testoppty1';
        if (acc != null)
        Oppty1.AccountId = acc.id;
        Oppty1.Sales_Channel__c = 'Direct';
        Oppty1.Sales_Force__c = 'EMC';
        Oppty1.StageName='Pipeline';
        Oppty1.CloseDate=system.today();
        if (lea != null)
        Oppty1.Related_Deal_Registration__c= lea.Id;
        opptyList.add(Oppty1);
        
        Opportunity Oppty2 = new Opportunity();
        Oppty2.Name = 'testoppty2';
        if (acc != null)
        Oppty2.AccountId = acc.id;
        Oppty2.Sales_Channel__c = 'Direct';
        Oppty2.Sales_Force__c = 'EMC';
        Oppty2.StageName='Upside';
        Oppty2.CloseDate=system.today();
        if (lea != null)
        Oppty1.Related_Deal_Registration__c= lea.Id;
        opptyList.add(Oppty2);
        return opptyList;
    }
    
    public static List<Product2> createProducts(){
        
         List<Product2> productList = new List<Product2>();
         Product2 prodObj = new Product2 (Name= 'Test Product',IsActive = true);
         productList.add(prodObj);
         Product2 prodObj1 = new Product2 (Name= 'Test Product1',IsActive = true);
         productList.add(prodObj1);
         return productList;
         
    }
    public static List<Pricebook2> createPriceBook() {
        
        List<Pricebook2> pbList = new List<Pricebook2>();
        Pricebook2 pbObj = new Pricebook2 (Name='test',isActive=true);
        pbList.add(pbObj);
        return pbList ;
    }
    public static List<PricebookEntry> createPriceBookEntry(Id pb, List<Product2> prod) {
        
        
        
        List<PricebookEntry > pbEntryList = new List<PricebookEntry >();
        PricebookEntry pbEntryObj = new PricebookEntry (Product2Id=prod[0].Id,Pricebook2Id=pb,UnitPrice=100,IsActive = true, UseStandardPrice = false);
        pbEntryList.add(pbEntryObj);
        PricebookEntry pbEntryObj1 = new PricebookEntry (Product2Id=prod[1].Id,Pricebook2Id=pb,UnitPrice=100,IsActive = true, UseStandardPrice = false);
        pbEntryList.add(pbEntryObj1);
        return pbEntryList;
    }
    public static List<OpportunityLineItem> createOpptyLineItems(List<Opportunity> oppList, List<PricebookEntry> pbEntryList){
         List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
         OpportunityLineItem OppLineItemObj = new OpportunityLineItem (OpportunityId=oppList[0].Id,PricebookEntryId=pbEntryList[0].Id);
         oppLineItemList.add(OppLineItemObj);
         OpportunityLineItem OppLineItemObj1 = new OpportunityLineItem (OpportunityId=oppList[0].Id,PricebookEntryId=pbEntryList[1].Id );
         oppLineItemList.add(OppLineItemObj1);
         OpportunityLineItem OppLineItemObj2 = new OpportunityLineItem (OpportunityId=oppList[1].Id,PricebookEntryId=pbEntryList[0].Id);
         oppLineItemList.add(OppLineItemObj2);
         OpportunityLineItem OppLineItemObj3 = new OpportunityLineItem (OpportunityId=oppList[1].Id,PricebookEntryId=pbEntryList[1].Id );
         oppLineItemList.add(OppLineItemObj3);

         return oppLineItemList;
    }
}