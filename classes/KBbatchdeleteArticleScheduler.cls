/*==================================================================================================================+                                                                          
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        ===========  
 |  Jul/10/2014    Bisna V P    CI 974  Created this batch scheduler class to archive list of articles by executing batch job   
 +==================================================================================================================**/

global class KBbatchdeleteArticleScheduler implements Schedulable 
{
    global void execute(SchedulableContext ctx)
    { 
    
     Map<String,CustomSettingDataValueMap__c> DataValueMap= CustomSettingDataValueMap__c.getall();
     String batchSize= DataValueMap.get('KBbatchdeleteArticleBatchSize').DataValue__c;
     KBbatchdeleteArticle k1= new KBbatchdeleteArticle();
     Database.executeBatch(k1, integer.ValueOf(batchSize));
    }
    
      
        
}