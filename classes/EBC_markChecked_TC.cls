/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
    05-Jun-2014      Abinaya M S                 Assigned 'Internal meeting' value to Briefing_Type__c field to satisfy validation rule.
    19 Feb 2015     Vinod Jetti   #1649         created Hub_Info__c objects data instead of Account 'Emc_classification__c' field
 */
@isTest
private class EBC_markChecked_TC {

    static testMethod void EBC_markChecked() {
        //#1649 - Start
        Hub_Info__c objHubInfo = new Hub_Info__c();
        objHubInfo.Global_DUNS_Entity__c = '12';
        objHubInfo.Golden_Site_Identifier__c = 305001;
        insert objHubInfo;
        //#1649 - End
        Account acc=new Account(name='testAccount',Hub_Info__c = objHubInfo.Id);//Global_DUNS_Entity__c='12');
        insert acc;
        EBC_Global_Strategic_Executive_Account__c gseAccount =new EBC_Global_Strategic_Executive_Account__c(Global_DUNS__c='12');
        insert gseAccount;
        EBC_Briefing_Event__c bEvent=new EBC_Briefing_Event__c(Customer_Name__c=acc.Id,Name='xyz',Briefing_Type__c = 'Internal meeting',Start_Date__c=date.today(),End_Date__c=date.today(),Briefing_Status__c='Pending');
        insert bEvent;
    }
}