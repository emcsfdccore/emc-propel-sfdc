// Wrapper Class
    public class warapperUserAssignment{
    
        public Boolean selected {get; set;}
        public Opportunity objOpp {get; set;}
        public User_Assignment__c  objUserAss {get;set;}
        
        public warapperUserAssignment(Opportunity Opp){
            objOpp = opp;
            selected = false;
        }
        
        public warapperUserAssignment(User_Assignment__c objUser){
            selected = false;
            objUserAss = objUser;
        }   
        
    }