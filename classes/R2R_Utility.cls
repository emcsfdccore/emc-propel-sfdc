/*==================================================================================================================+

 |  HISTORY  |                                                                            

 |  DATE          DEVELOPER      WR        DESCRIPTION   strInputCode                            

 |  ====          =========      ==        =========== 

 |  12/02/2014    Sneha Jain    FR 9639   Changing the Asset search logic to be based on GU field rather than Account District                      
 |  24/02/2014    Srikrishna SM FR 9639   Modified getAsset method to fit search logic to return based on GU                    
 |  18/02/2015    Vinod Jetti   #1649     Replaced field 'Global_Duns_Entity__c' to 'Global_Duns_Entity_Hub__c'
 |  10/04/2015    Vinod Jetti   #1876     Modified code 'Estimated Raw Capacity (TB)' gets update on asset obj, 
                                                 the 'Re-Registration Reason'&'Re-Registration Required' fields on linked Opptys should not update for 'Booked/Closed' Opptys.
 +==================================================================================================================**/
global Class R2R_Utility{
    
    
/* @Method <This method is used to get Opportunity values on Asset>
    @param <It is taking ID as Parameter>
    @returntype - No return type >
    @throws exception - <No Exception>
    */  
 public static List<Asset__c> getAssetOnOpportunity(ID OpportunityId){
        List<Asset__c> l; 
        List<ID> lstRlatedAssets = new List<ID>();
        for( Opportunity_Asset_Junction__c rAs: [Select Id, Name,Record_Type_Name__c,Asset_Disposition_Status__c,Related_Asset__c from Opportunity_Asset_Junction__c where Related_Opportunity__c =:OpportunityId])
            {
               lstRlatedAssets.add(rAs.Related_Asset__c);
            }
         if(lstRlatedAssets!=Null)
            {
               l=[Select Id from Asset__c where id in :lstRlatedAssets];
            }
               return l;
             
            }

/* @Method <This method is uded to  get Lead values On Asset>
    @param <It is taking ID as Parameter
    @returntype - No return type >
    @throws exception - <No Exception>
    */  
 public static  List<Asset__c> getAssetsOnLead(ID LeadId){
        List<Asset__c> lst;
        List<ID> lstRlatedAssets = new List<ID>();
         for( Lead_Asset_Junction__c rAs: [Select Id,Related_Asset__c from Lead_Asset_Junction__c where Related_Lead__c =:LeadId])
            {
               lstRlatedAssets.add(rAs.Related_Asset__c);
            }
         if(lstRlatedAssets!=Null)
            {
                lst=[Select Id from Asset__c where id in :lstRlatedAssets];
            }
               return lst;
            }
/* @Method <This method is used fetch the accounts in case of customer porifiled account you need to find all accounts under it >
    @param <It is taking List<ID> as Parameter>
    @returntype - No return type >
    @throws exception - <No Exception>
    */  
  Public static List<Asset__c> getAsset(List<ID> AccountIds){
        List<Asset__c> lstAss2;
        List<String> lstName= new List<String>();
        List<String> lstIds = new List<String>();
        List<Account> lstAccounts = [select id,Name,Customer_Profiled_Account__c from Account where Id in: AccountIds]; 
          for(Account acc : lstAccounts){
            lstName.add(acc.Name);
            lstIds.add(acc.id);
                }
            if(lstIds != null){
                     lstAss2=[Select Id,Name from Asset__c where Customer_Name__c in : AccountIds];
                        }
                         return lstAss2;
   }
/* @Method <This method is used  return values list of those assets that are linked to Customer Profiled Account >
    @param <It is taking ID as Parameter>
    @returntype - No return type >
    @throws exception - <No Exception>
    */  
    public static List<Asset__c> getAssetOnProfile(ID ProfileAccountId) {
        List<Asset__c> lstAss3;
        List<String> lstName= new List<String>();
        List<String> lstIds = new List<String>();
        List<Account> lstAccounts = [select id,Name,Customer_Profiled_Account__c from Account where Customer_Profiled_Account__c =: false]; 
            for(Account acc : lstAccounts){
                    lstName.add(acc.Name);
                    lstIds.add(acc.id);
                                }
                if(lstIds != null){
                           lstAss3=[Select Id,Name from Asset__c where Customer_Name__c in : lstIds ];
                                 }
                            return lstAss3;
     }
   /* @Method <This method is used get customerProfiledAccountId values on lead  >
    @param <It is taking ID as Parameter>
    @returntype - No return type >
    @throws exception - <No Exception>
    */             
    public static list<Lead> getLeads(string AccountdistName){
        List<Lead> lstLeadRelatedToProfiledAccount = new list<Lead>();
           lstLeadRelatedToProfiledAccount  =[Select Id,Name,Company,Status,Related_Account__c,Related_Opportunity__c,Lead_Number__c,LeadSource from Lead where 
                                              Related_Account__r.Account_District__c=: AccountdistName and status not in('Closed','Converted to Opportunity')];
              
                
        return lstLeadRelatedToProfiledAccount;
    }
     /* @Method <This method is used get customerProfiledAccountId values on lead  >
    @param <It is taking ID as Parameter>
    @returntype - No return type >
    @throws exception - <No Exception>
    */             
    //Logic is now replaced with GU - 9639
    //public static list<Asset__c> getAssets(string AccountdistName){
    public static list<Asset__c> getAssets(string accGlobalDUNS){
    
        List<Asset__c> lstAssetRelatedToAccount = new list<Asset__c>();
        //lstAssetRelatedToAccount =[Select Id ,Target_Zone__c,Red_Zone_Priority__c,Install_City__c,Install_Base_Status__c,Contract_End_Date__c,Sales_Plan__c,Maintenance_Engagement_Status1__c,Lease_Exp_Date__c,Model__c,Install_Date__c,Customer_Name__r.Party_Number__c,Customer_Name__r.Customer_Profiled_Account_Lookup__c,Custom_Asset_Name__c,Serial_Number__c,Name,Disposition_Status__c ,Account_Record_Type__c ,RecordType.Name ,Product_Name_Vendor__c,Product_Family__c,Customer_Name__c,Contract_Number__c 
        //                                from Asset__c where Customer_Name__r.Account_District__c=: AccountdistName and Install_Base_Status__c not in('Deleted','Displaced by EMC','Displaced by Competitor','Deinstall - Cust Site') and RecordType.DeveloperName!='EMC_Install_Inactive'];
        lstAssetRelatedToAccount =[Select Id ,Target_Zone__c,Red_Zone_Priority__c,Install_City__c,Install_Base_Status__c,Contract_End_Date__c,Sales_Plan__c,Maintenance_Engagement_Status1__c,Lease_Exp_Date__c,Model__c,Install_Date__c,Customer_Name__r.Party_Number__c,Customer_Name__r.Customer_Profiled_Account_Lookup__c,Custom_Asset_Name__c,Serial_Number__c,Name,Disposition_Status__c ,Account_Record_Type__c ,RecordType.Name ,Product_Name_Vendor__c,Product_Family__c,Customer_Name__c,Contract_Number__c 
                                        from Asset__c where Customer_Name__r.Global_DUNS_Entity_Hub__c=: accGlobalDUNS and Install_Base_Status__c not in('Deleted','Displaced by EMC','Displaced by Competitor','Deinstall - Cust Site') and RecordType.DeveloperName!='EMC_Install_Inactive'];
              
                
        return lstAssetRelatedToAccount;
    }
    /* @Method <This method is used get customerProfiledAccountId values on lead  >
    @param <It is taking ID as Parameter>
    @returntype - No return type >
    @throws exception - <No Exception>
    */  
    //Logic is now replaced with GU - 9639           
    //public static list<Lead> getLeads(string AccountdistName,string leadName){
    public static list<Lead> getLeads(string accGlobalDUNS,string leadName){
        List<Lead> lstLeadRelatedToProfiledAccount = new list<Lead>();
           /* lstLeadRelatedToProfiledAccount =[SELECT Id, Name,Company,Status,Related_Account__c,Related_Opportunity__c,Lead_Number__c,LeadSource  FROM Lead where name Like: leadName and 
                              Related_Account__r.Account_District__c=: AccountdistName and status not in('Closed','Converted to Opportunity')];*/
            lstLeadRelatedToProfiledAccount =[SELECT Id, Name,Company,Status,Related_Account__c,Related_Opportunity__c,Lead_Number__c,LeadSource  FROM Lead where name Like: leadName and 
                              Related_Account__r.Global_DUNS_Entity_Hub__c=: accGlobalDUNS and status not in('Closed','Converted to Opportunity')];
              
                
        return lstLeadRelatedToProfiledAccount;
    }
    public static void validateLeadLinking(List<Lead_Asset_Junction__c> lstFromTrigger){
        Map<Id,List<Lead_Asset_Junction__c>> mapAssetWithLeadLinks = new map<Id,list<Lead_Asset_Junction__c>>();        
        map<Id,list<Opportunity_Asset_Junction__c>> mapAssetWithJunction = new map<Id,list<Opportunity_Asset_Junction__c>>();
        map<Id,list<Lead_Asset_Junction__c>> mapAssetWithLeadJunction = new map<Id,list<Lead_Asset_Junction__c>>();
        Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
        string LeadOpenStatus = data.get('R2R_lead_Status').datavalue__c;
        string LeadClosedStatus = data.get('R2R_Closed_lead_Staus').datavalue__c;
        string OpptyClosedLostStatus = data.get('OpptyClosedStatus').datavalue__c;
        string R2RAdminProfiles = data.get('R2R_Admin_Profiles').datavalue__c;
        set<id> setLeadIds = new set<id>();
        for(Lead_Asset_Junction__c junctionObj : lstFromTrigger){
            list<Lead_Asset_Junction__c> lstLeadAssetJunction = mapAssetWithLeadLinks.get(junctionObj.Related_Asset__c);
            if(lstLeadAssetJunction == null){
                lstLeadAssetJunction = new list<Lead_Asset_Junction__c>();
            }
            lstLeadAssetJunction.add(junctionObj);
            mapAssetWithLeadLinks.put(junctionObj.Related_Asset__c,lstLeadAssetJunction);
            setLeadIds.add(junctionObj.Related_Lead__c);
        }
        Map<Id,Asset__c> mapAssetWithrecTypeInfo = new Map<Id,Asset__c>([Select Id,recordType.DeveloperName from Asset__c where id in : mapAssetWithLeadLinks.keyset()]);
        
        list<Lead_Asset_Junction__c> listLeadAssetJunction = [Select Id,Related_Asset__c from Lead_Asset_Junction__c
                                                              where  Related_Asset__c in :mapAssetWithLeadLinks.keyset() and related_lead__r.status not in ('Closed','Converted To Opportunity')];
        List<Opportunity_Asset_Junction__c> lstOpptyJnObj = [select id,Asset_Record_Type_ID__c,Opportunity_Forecast_Status__c,Opportunity_Close_Date__c,Related_Asset__c,Related_Asset__r.id,Related_Opportunity__c,
                        Related_Opportunity__r.Opportunity_Type__c,Related_Opportunity__r.StageName,Record_Type_Name__c from Opportunity_Asset_Junction__c where Related_Asset__r.id in:mapAssetWithLeadLinks.keyset()];

        map<id,Lead> mapLeadToLink = new map<Id,Lead>([Select Id,Status from Lead where id in :setLeadIds]);
        for(Opportunity_Asset_Junction__c junctionObj :lstOpptyJnObj){
            list<Opportunity_Asset_Junction__c> lstJunction = mapAssetWithJunction.get(junctionObj.Related_Asset__c);
            if(lstJunction == null){
               lstJunction = new list<Opportunity_Asset_Junction__c>(); 
            }
            lstJunction.add(junctionObj);
            mapAssetWithJunction.put(junctionObj.Related_Asset__c,lstJunction);            
         } 
        for(Lead_Asset_Junction__c leadjunctionObj :listLeadAssetJunction){
            list<Lead_Asset_Junction__c> lstJunction = mapAssetWithLeadJunction.get(leadjunctionObj.Related_Asset__c);
            if(lstJunction == null){
               lstJunction = new list<Lead_Asset_Junction__c>(); 
            }
            lstJunction.add(leadjunctionObj);
            mapAssetWithLeadJunction.put(leadjunctionObj.Related_Asset__c,lstJunction);         
        }
        for(Lead_Asset_Junction__c junctionLink :lstFromTrigger){
            if(mapAssetWithLeadJunction.containskey(junctionLink.Related_Asset__c) && !R2RAdminProfiles.contains(userInfo.getProfileId()) && LeadClosedStatus.contains(mapLeadToLink.get(junctionLink.related_lead__c).status)){
               junctionLink.adderror(System.Label.R2R_Link_To_Lead);
            }
            if(!R2RAdminProfiles.contains(userInfo.getProfileId()) && LeadClosedStatus.contains(mapLeadToLink.get(junctionLink.related_lead__c).status) && mapLeadToLink.get(junctionLink.Related_Lead__c).status.contains('Renewals') && 
               mapAssetWithrecTypeInfo.get(junctionLink.Related_Asset__c).RecordType.DeveloperName =='Competitive_Install'){
               junctionLink.adderror(System.Label.R2R_Comp_Assets_Renewals_Lead);
            
            }
            if(mapAssetWithJunction !=null){
               if(mapAssetWithJunction.containskey(junctionLink.Related_Asset__c)){
                  for(Opportunity_Asset_Junction__c opptyJunction :mapAssetWithJunction.get(junctionLink.Related_Asset__c)){
                    if(!OpptyClosedLostStatus.contains(opptyJunction.Related_Opportunity__r.StageName) && opptyJunction.Related_Opportunity__r.Opportunity_Type__c.contains('Renewals') &&
                    mapLeadToLink.get(junctionLink.Related_Lead__c).status.contains('Renewals') && !R2RAdminProfiles.contains(userInfo.getProfileId()) && LeadClosedStatus.contains(mapLeadToLink.get(junctionLink.related_lead__c).status)){
                     junctionLink.adderror(System.Label.R2R_Asset_Linked_To_Oppty);
                    }
                    if(!OpptyClosedLostStatus.contains(opptyJunction.Related_Opportunity__r.StageName) && opptyJunction.Related_Opportunity__r.Opportunity_Type__c.contains('Refresh') &&
                    mapLeadToLink.get(junctionLink.Related_Lead__c).status !='Pass To Renewals' && !R2RAdminProfiles.contains(userInfo.getProfileId()) && LeadClosedStatus.contains(mapLeadToLink.get(junctionLink.related_lead__c).status)){
                     junctionLink.adderror(System.Label.R2R_Asset_Linked_To_Oppty);
                    }
                  }
               }
            }
        }
    }
/* @Method <This method is used to get Opportunity List and invoke the updateReRegistrationDetails() method of SFA_MOJO_RegisterWithGFSController>
    @param <It is List<Asset__c> as parameter>
    @returntype - No return type >
    @throws exception - <No Exception>
    */  
 public void getOpportunityReregistration(List<Asset__c> lstAsset, Boolean flag){
        List<Opportunity_Asset_Junction__c> lstOpptyJnObj = [select id,Related_Opportunity__c from Opportunity_Asset_Junction__c where Related_Asset__c in:lstAsset];
        set<id> setOpptyId = new set<id>();
        for(Opportunity_Asset_Junction__c junctionObj :lstOpptyJnObj){
            setOpptyId.add(junctionObj.Related_Opportunity__c);
        }
        List<Trade_Ins_Competitive_Swap__c> lstTradeInAndSwap=[Select Swap_Value__c, Related_Opportunity__c,Registration_ID__c, Name, Id 
                                                               From Trade_Ins_Competitive_Swap__c where Related_Opportunity__c in: setOpptyId];
        if(!lstTradeInAndSwap.isempty()){
           setOpptyId.clear();
           for(Trade_Ins_Competitive_Swap__c tradeinSwapObj :lstTradeInAndSwap){
               setOpptyId.add(tradeinSwapObj.Related_Opportunity__c);
           } 
        //#1876 Changes - Start
          if(!setOpptyId.isempty() && flag){
               SFA_MOJO_RegisterWithGFSController.updateReRegistrationDetails([select id,Re_Registration_Reason__c,Re_Registration_Required_on__c from opportunity where id in:setOpptyId and StageName NOT IN ('Booked','Closed')],'Ports/Capacity Changed',true);
           }
           else{
               SFA_MOJO_RegisterWithGFSController.updateReRegistrationDetails([select id,Re_Registration_Reason__c,Re_Registration_Required_on__c from opportunity where id in:setOpptyId],'Ports/Capacity Changed',true);
           }
         //#1876 Changes - End  
        }                                                 
    }
    /* @Method <This method is used to fetch the Related Opportunity of Assets>
    @param <It is taking set<id> as parameter>
    @returntype - No return type >
    @throws exception - <No Exception>
    */  
 public void getOpptyRelatedToAsset(set<Id> setId){
    set<id> setOpptyId = new set<Id>();
    
    List<Opportunity_Asset_Junction__c> lstOpptyJnObj = [select id,Related_Opportunity__c,Related_Asset__c,Related_Asset__r.RecordType.DeveloperName,Related_Asset__r.Product_Name_Vendor__c,Related_Asset__r.Product_Family__c,
                                                          Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c from Opportunity_Asset_Junction__c where Related_Asset__c in :setId];
    if(lstOpptyJnObj !=null){
        for(Opportunity_Asset_Junction__c junctionObj :lstOpptyJnObj){
            setOpptyId.add(junctionObj.Related_Opportunity__c);
        }
    }   
    if(!setOpptyId.isempty()){
       setTotalSwapValueOnTradeIn(setOpptyId);
    }
 }
    /* @Method <This method is used to update the TotalSwapValue on GFS Trade Ins>
    @param <It is taking List<Asset__c> as parameter>
    @returntype - No return type >
    @throws exception - <No Exception>
    */  
 public void setTotalSwapValueOnTradeIn(set<Id> setId){
        Map<Id,map<string,integer>> mapOpptyWithRegTypeSwapValue = new Map<Id,map<string,integer>>();
        set<string> setRegistrationType = new set<string>();
        List<Opportunity_Asset_Junction__c> lstOpptyJnObj = [select id,Related_Opportunity__c,Related_Asset__c,Related_Asset__r.RecordType.DeveloperName,Related_Asset__r.Product_Name_Vendor__c,Related_Asset__r.Product_Family__c,
                                                             Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c from Opportunity_Asset_Junction__c where Related_Opportunity__c in :setId];
        for(Opportunity_Asset_Junction__c junctionObj :lstOpptyJnObj){            
               map<string,integer> mapRegTypeWithValue = mapOpptyWithRegTypeSwapValue.get(junctionObj.Related_Opportunity__c);
               setRegistrationType.add('Connectrix Trade In');
               setRegistrationType.add('EMC Trade In');
               setRegistrationType.add('Competitive Swaps');
               if(mapRegTypeWithValue==null){
                  mapRegTypeWithValue = new map<string,integer>();
                  mapRegTypeWithValue.put('Connectrix Trade In',0);
                  mapRegTypeWithValue.put('EMC Trade In',0);
                  mapRegTypeWithValue.put('Competitive Swaps',0);
                }              
               integer totalSwapValue = 0;
               if(junctionObj.Related_Asset__r.Product_Family__c =='CONNECTRIX'){
                  totalSwapValue = mapRegTypeWithValue.get('Connectrix Trade In');
                  if(junctionObj.Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c !=null){
                      totalSwapValue = totalSwapValue + integer.valueof(junctionObj.Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c);
                  }    
                  mapRegTypeWithValue.put('Connectrix Trade In',totalSwapValue);
               }
               if(junctionObj.Related_Asset__r.RecordType.DeveloperName=='Competitive_Install' && junctionObj.Related_Asset__r.Product_Family__c !='CONNECTRIX'){
                  totalSwapValue = mapRegTypeWithValue.get('Competitive Swaps');
                  if(junctionObj.Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c !=null){
                      totalSwapValue = totalSwapValue + integer.valueof(junctionObj.Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c);
                  }    
                  mapRegTypeWithValue.put('Competitive Swaps',totalSwapValue);
               }
               if(junctionObj.Related_Asset__r.RecordType.DeveloperName=='EMC_Install' && junctionObj.Related_Asset__r.Product_Family__c !='CONNECTRIX'){
                  totalSwapValue = mapRegTypeWithValue.get('EMC Trade In');
                  if(junctionObj.Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c !=null){
                      totalSwapValue = totalSwapValue + integer.valueof(junctionObj.Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c);
                  }    
                  mapRegTypeWithValue.put('EMC Trade In',totalSwapValue);
               }
               mapOpptyWithRegTypeSwapValue.put(junctionObj.Related_Opportunity__c,mapRegTypeWithValue);
        }
        List<Trade_Ins_Competitive_Swap__c> lstGFSObj = [select id,name,Swap_Value__c,Registration_ID__c,RecordTypeid,Registration_Type__c,Related_Opportunity__c, Related_Opportunity__r.Account_Name1__c, Related_Opportunity__r.id from 
                                                         Trade_Ins_Competitive_Swap__c where Related_Opportunity__c in :mapOpptyWithRegTypeSwapValue.keyset() and Registration_Type__c in :setRegistrationType]; 
        if(!lstGFSObj.isempty()){
           for(Trade_Ins_Competitive_Swap__c swapObj :lstGFSObj){
                if(mapOpptyWithRegTypeSwapValue.containskey(swapObj.Related_Opportunity__c)){
                   map<string,integer> mapRegTypeWithValue = mapOpptyWithRegTypeSwapValue.get(swapObj.Related_Opportunity__c);
                   if(mapRegTypeWithValue != null){
                      if(swapObj.Registration_Type__c=='Connectrix Trade In'){
                         swapObj.Uplift__c =mapRegTypeWithValue.get(swapObj.Registration_Type__c);
                         swapObj.Swap_Value__c =null;
                      }
                      else{
                          swapObj.Swap_Value__c = mapRegTypeWithValue.get(swapObj.Registration_Type__c);
                      }    
                    }  
                }
           }
        }   
        
        update  lstGFSObj;
    }
}