/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
|  17/Sep/2014  Jaypal Nimesh       BPP Project      Test Class for PRM_BPP_PortalSearchController
|
+=====================================================================================================*/

@isTest
private class PRM_BPP_PortalSearchController_TC{

    public static testmethod void searchctrlmethod(){
    
	    //Initializing variables
	    String objName;
	    PRM_BPP_PortalSearchController objbpp = new PRM_BPP_PortalSearchController();
	    
	    List<SelectOption> selOpts=objbpp.getsearchObjList();
	    objbpp.objectName = 'Lead';
	    objbpp.objectName = 'opportunity';
	    PageReference testPage = objbpp.searchRecords();
	    system.Test.setCurrentPage(testPage);
  
	    if(objName=='Lead'){
	       testpage= new pagereference('/search/SearchResults');
	    }
	    if(objName=='opportunity'){
	        testpage= new pagereference('/search/SearchResults');
	    }
    }
}