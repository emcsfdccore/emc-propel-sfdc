/*=======================================================================================================+
|  HISTORY  |                                                                           
|  DATE          DEVELOPER        WR                    DESCRIPTION                               
|  ====          =========        ==                    =========== 
|  16-Sep-2014   Sneha Jain      WR# 291(b)     R2R Nov'14 Rel - Initial creation of class to post chatter messages on records           
+=====================================================================================================*/

public class R2R_ChatterPost{

    //Method is used for doing chatter post on Opportunity records where there are assets attached and the Oppty status is changed to COMMIT
    public void afterUpdateFeedItems(Set<Opportunity> oppIds){     
        if(oppIds != null && oppIds.size() > 0){
            List<Opportunity> oppList = new List<Opportunity>();
            oppList = [Select Id,Account.Core_Quota_Rep__c from Opportunity where Id IN: oppIds];
            for(Opportunity opp : oppList){
                String currentUserName = UserInfo.getName();
                //@Mention the Opportunity's Account' Core Quota Rep
                Id userPostId = opp.Account.Core_Quota_Rep__c;
                Id oppId = opp.Id;
                String chatterPostMsg = System.Label.R2R_Opp_COMMIT_Chatter_Post; 
                insertChatterFeedApi(oppId,userPostId,chatterPostMsg);
            }
        }
    }

    //Method is used for doing chatter post on Asst records where the Sales Strategy changes to 'Deferred – Non-renewable' Or 'Refresh Deferred'
    public void afterDeferred(Map<Id,String> assetIdChatterMap){
        if(assetIdChatterMap !=null && assetIdChatterMap.size()>0){
            //List for feed items to be inserted
            List<FeedItem> lstFeedItemToPost = new List<FeedItem>();
            for(Id assetId: assetIdChatterMap.keySet()){
                FeedItem chatterpost = new FeedItem();                        
                chatterpost.ParentId = assetId;
                // The Non-Renewable reason field on Asset record is not blank
                if(assetIdChatterMap.get(assetId) == 'NonBlank'){
                    chatterpost.Body = System.Label.R2R_Non_Renewable_Chatter_Post; 
                }
                // The Non-Renewable reason field on Asset record is blank
                else if(assetIdChatterMap.get(assetId) == 'Blank'){
                    chatterpost.Body = System.Label.R2R_Refresh_Deferred_Chatter_Post; 
                }
                lstFeedItemToPost.add(chatterpost);
            }
            if(lstFeedItemToPost !=null && lstFeedItemToPost.size()>0){
                try{
                    insert lstFeedItemToPost;
                }
                catch(Exception e){}
            }
        }
    }

    
    //Method is used to post chatter feed on any record using Connect Api
    public static void insertChatterFeedApi(Id oppId,Id userPostId,String chatterPostMsg ){
        ConnectApi.FeedType feedType = ConnectApi.FeedType.Record;
        String subjectId = oppId;
        
        ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
        messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();

        ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
        mentionSegment.id = userPostId;
        messageInput.messageSegments.add(mentionSegment);

        textSegment = new ConnectApi.TextSegmentInput();
        textSegment.text =chatterPostMsg;
        messageInput.messageSegments.add(textSegment);

        ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
        input.body = messageInput;

        ConnectApi.FeedItem feedItemRep = ConnectApi.ChatterFeeds.postFeedItem(null, feedType, subjectId, input, null); 
    }
}