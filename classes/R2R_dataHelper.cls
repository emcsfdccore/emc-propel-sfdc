@istest
public Class R2R_dataHelper{
public static Account acc= new Account();
public static Opportunity opp=new Opportunity();
public static Asset__c a= new Asset__c();
public static Opportunity_Asset_Junction__c oaj1=new Opportunity_Asset_Junction__c(); 
public static Lead_Asset_Junction__c laj1= new Lead_Asset_Junction__c();
public static Lead lead= new Lead();
public static Asset__c asst= new Asset__c();

public static Account createAccount(){
//Creating Customer Profiled Account

acc.Name='Testacc';
acc.Customer_Profiled_Account__c=true;
return acc;

}


public static Opportunity createOppty(id accid){

//Creating Oppty

opp.Name='Testoppty';
opp.AccountId = accid;
Date closeDate =  System.today()+5;
opp.CloseDate=closeDate;
opp.Sales_Channel__c='Direct';
opp.Sales_Force__c='EMC';
opp.StageName='Pipeline';

return opp;

}

public static void creatAssetforLead(id accid){


asst.Name='assetforlead';
asst.Customer_Name__c=accid;
insert asst;

}
public static Lead createLead(){
//Creating Lead

lead.Company='TestLead';
lead.LastName='Test123';
lead.Status='New';
lead.Sales_Force__c='EMC';
lead.Lead_Originator__c='Customer Intelligence';
return lead;

}

public static Lead_Asset_Junction__c leadAssetJunction(id assetId, id leadId){

//Creating lead asset junction

laj1.Related_Asset__c=asst.id;
laj1.Related_Lead__c=lead.id;
return laj1;

}
}