@isTest
public class ctrlCommunitiesCasesListViewTest{
    public static testMethod void testMyController() {
    //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        } 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Community User']; 
        Profile p2 = [SELECT Id FROM Profile WHERE Name='Community Power User']; 
        Profile p3 = [SELECT Id FROM Profile WHERE Name='GBS Collaborator Profile']; 
        Account a = new Account(name='TestAccount');
        insert a;
        Contact c = new Contact(Email='haytham.lotfy@emc.com',AccountId=a.id,LastName='Haytham');
        insert c;
        Contact c2 = new Contact(Email='haytham.lotfy2@emc.com',AccountId=a.id,LastName='Haytham');
        insert c2;
        User u = new User(Alias = 'standt', Email='haytham.lotfy@emc.com', 
                          EmailEncodingKey='UTF-8', 
                          LastName='Haytham', 
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', 
                          ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName='standarduser@testorg.com',
                          ContactId=c.Id,
                          CommunityNickname = 'testUser'
                         );
        User u2 = new User(Alias = 'standt2', Email='haytham.lotfy2@emc.com', 
                           EmailEncodingKey='UTF-8', 
                           LastName='Haytham', 
                           LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', 
                           ProfileId = p2.Id, 
                           TimeZoneSidKey='America/Los_Angeles',
                           UserName='standarduser2@testorg.com',
                           ContactId=c2.Id,
                           CommunityNickname = 'testUser2'
                          );
        User u3 = new User(Alias = 'standt3', Email='haytham.lotfy3@emc.com', 
                           EmailEncodingKey='UTF-8', 
                           LastName='Haytham3', 
                           LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', 
                           ProfileId = p3.Id, 
                           TimeZoneSidKey='America/Los_Angeles',
                           UserName='standarduser34@testorg.com'
                           //ContactId=c2.Id,
                           //CommunityNickname = 'testUser3'
                          );
        insert u;
        insert u2;   
        insert u3;
        System.runAs(u){
            ctrlCommunitiesCasesListView controller = new ctrlCommunitiesCasesListView();
            PageReference pageRef = Page.CommunitiesCasesListView;
            pageRef.getParameters().put('mySearchString','012');
            Test.setCurrentPage(pageRef);
            controller.getViewOptions();
            controller.refreshCases();
            controller.getSSCCases();
            //Boolean cs = controller.hasCases();
            //controller.getlstCases();
            controller.setSortDirection('ASC');
            controller.sortExpression = 'Subject';
            controller.SearchString = '0123';
            //controller.setSortExpression('CreatedBy');
            controller.getSortDirection();
            controller.AlphaFilter ='A';
            controller.refreshCases();
            controller.AlphaFilter ='Other';
            controller.refreshCases();
            controller.runSearch();
        }
        System.runAs(u2){
            ctrlCommunitiesCasesListView controller2 = new ctrlCommunitiesCasesListView();    
            PageReference pageRef = Page.CommunitiesCasesListView;
            Test.setCurrentPage(pageRef);    
            controller2.getViewOptions();  
            controller2.refreshCases();
        }
        System.runAs(u3){
            ctrlCommunitiesCasesListView controller3 = new ctrlCommunitiesCasesListView();    
            PageReference pageRef = Page.CommunitiesCasesListView;
            Test.setCurrentPage(pageRef);    
            controller3.getViewOptions();  
            controller3.refreshCases();
            //controller3.getlstCases();
            controller3.selectedViewOption = '6GRO';
            controller3.refreshCases();
            controller3.selectedViewOption = '5Resolved Last 48 Hours';
            controller3.refreshCases();
            list<Case> cs = controller3.lstCases;
            boolean cst = controller3.hasCases;
            //controller3.
        }
        
    }
}