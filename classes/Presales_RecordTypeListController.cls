/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 |  14/09/2011    Shalabh Sharma           This class is used to allow requestor to select record types on click of Clone and Copy Parent Details buttons on case.  
 |  30/11/2011    Leonard Victor           Changes made to include two new fields for case clone
 |  26/04/2012    Leonard Victor           Changes made to include Partner Related Fields for case clone  
 |  01/10/2012    Ganesh Soma              Added filter condition to pick only active record types
 |  26/12/2012    Srinivas Pinnamaneni     Added filter in query to get the record type names in alphabetical order in line no 107
 |  01/02/2013    John Thomson             Added fields for eservices to checkParentCase
 |  09/10/2013    Jaspreet Singh           Added field for Federal Case types and Priority
 |  07/01/2014    Shalabh Sharma           Added PVR fields to copy parent details
 | 13-Oct-2014    Bisna V P    CI:1399     Remove code referencing unused fields 
 
 +==================================================================================================================**/

public class Presales_RecordTypeListController{
    public static Id value{get;set;}
    private List<SelectOption> items;
    public ApexPages.standardController stdCon;
    public Case caseRecord;
    public String caseURL;
    public String parentId;
    public Id caseId;
    String lstRecordTypeIds;
    List<String> lstRecordTypeIdsForProfile = new List<String>();
    
    public Presales_RecordTypeListController() {

    }   
    public Presales_RecordTypeListController(ApexPages.StandardController stdController){
        stdCon = stdController;
        caseRecord = (Case)stdController.getRecord();
        caseId = ApexPages.currentPage().getParameters().get('id');       
        caseURL = ApexPages.currentPage().getURL();
        system.debug('case url--->'+caseURL);
        /*Checking index of case id in case of clone button*/
        integer checkIFCaseId = caseURL.indexOF('CaseID');
        system.debug('index of case id'+checkIFCaseId);
        parentId = caseURL.substring(checkIFCaseId+7,checkIFCaseId+22);
        system.debug('case idppppp--->'+parentId);
    }
// property that reads the value from the Component attribute
    public string sObjectType
    {
        get;
        set;
    }   
    public List<SelectOption> getItems() {
        List<SelectOption> items = new List<SelectOption>();
 //default value
        items.add(new SelectOption('','--Select Case Type --'));
 //query force.com database to get the record type of the requested object.
        Map<String,String> mapPresales_ProfileRecordTypeMapping = new Map<String,String>();
        List<Presales_ProfileRecordTypeMappingObject__c> lstPresales_ProfileRecordTypeMapping = [select Id,Name,Record_Type_Id__c from Presales_ProfileRecordTypeMappingObject__c];
        system.debug('lstPresales_ProfileRecordTypeMapping--->'+lstPresales_ProfileRecordTypeMapping);
        for(Presales_ProfileRecordTypeMappingObject__c profileRecordTypeMappingRecord:lstPresales_ProfileRecordTypeMapping){
            mapPresales_ProfileRecordTypeMapping.put(profileRecordTypeMappingRecord.Name,profileRecordTypeMappingRecord.Record_Type_Id__c); 
        }
        system.debug('mapPresales_ProfileRecordTypeMapping--->'+mapPresales_ProfileRecordTypeMapping);
        Id userProfileId = UserInfo.getProfileId();
        system.debug('profile Id'+userProfileId);
        Profile userProfile = [select Id, Name from Profile where Id =:userProfileId];
        system.debug('profile--->'+userProfile);       
            if(mapPresales_ProfileRecordTypeMapping.containsKey(userProfile.Name)){
                lstRecordTypeIds = mapPresales_ProfileRecordTypeMapping.get(userProfile.Name);
                system.debug('list of record type ids--->'+lstRecordTypeIds);
            }

            lstRecordTypeIdsForProfile=lstRecordTypeIds.split(',');
            system.debug('list of record type ids--->'+lstRecordTypeIdsForProfile);
            system.debug('list of record type ids--->'+lstRecordTypeIds);              
            List<RecordType> rt= [select id,name from recordtype where Id = :lstRecordTypeIdsForProfile and isactive=true order by name];
                for(RecordType loopVariable: rt){
                    system.debug('Key----------->' +loopVariable.name);
                    system.debug('Value----------->' +loopVariable.Id);
                    items.add(new SelectOption(loopVariable.id,loopVariable.name));
                }        
        system.debug('record types--->'+items);
             return items;
    }
     
    public void checkParentCase(){  
        system.debug('case record1111111111--->'+ApexPages.currentPage().getParameters().get('id'));
        //1399 removing unwanted fields from query
        caseRecord = [select Id,Validation_check_for_TC__c,AccountId,Description,Subject,Applications__c,Engineering_Contact__c,Area__c,Federal_Case__c, Priority, 
                    Business_Unit__c,Competitor_Models__c,Competitors__c,ContactId,Contact_Email1__c,
                    Contact_Phone1__c,Country__c,Division__c,HW_List_Price__c,HW_Discounted_Price__c,SW_List_Price__c,SW_Discounted_Price__c,
                    Customer_Account_Name__c,Discount_Product_Class__c,Record_Type_Developer_Name__c,District__c,Concession_Start_Date__c,Concession_End_Date__c,Total_Concession__c,Credit_Amount__c,
                    Host_Server_Environment__c,RecordTypeId,SW_Concession__c,HW_Concession__c,
                    Opportunity_Name__c,Order_Number__c,
                    PAS_Number__c,QA_Comments__c,RPQ_Deal_Value__c,SoW_Document_Number__c,
                    T_E_Price__c,Theatre__c,Case_Completed_STD_Process__c,Response_Format__c,
                    //Added two new Case field  RPQ_Status__c,Assigned_Service_Group__c field
                    //Commented to Exclude the Assigned_Service_Group__c field
                    //Response_Within_SLA__c,Required_Fields_Populated__c,RPQ_Status__c,Assigned_Service_Group__c from Case where Id = :parentId or Id=:caseId];
                     //Added New Fields Related To Partner
                    Response_Within_SLA__c,Required_Fields_Populated__c,RPQ_Status__c,Partner_Case__c,Partner_Country__c,Partner_Grouping_Name__c,Partner_Theater__c,
                    Inquiry_Type__c, EDS_Contact_Email__c,Case_Resolution_Detail__c,SuppliedEmail
                     from Case where Id = :parentId or Id=:caseId];                       
    }
    public PageReference createChildCase(){       
        integer checkIFClone = caseURL.indexOf('flag=1');
        Boolean errorRecord = false;
        system.debug('index value--->'+checkIFClone);
       // setvalue(value);
        Case childCase = new Case();
        childCase = caseRecord.clone(false);
        system.debug('record type id--->'+value);
        childCase.RecordTypeId = value;
        childCase.Validation_check_for_TC__c = true;
        


        if(checkIFClone==-1){
            childCase.ParentId = caseRecord.Id; 
        }
        else{
            childCase.ParentId = null;
        }    
        
         if(!errorRecord){
        try {
        
        insert childCase;
        system.debug('inserted case--->'+childCase);
        }
        catch(System.DmlException e){
            System.debug('DML exception while Inserting Child Case: ' + e.getDmlMessage(0));   
        } 
             
        PageReference childCaseURL = new PageReference('/'+childCase.id+'/e?retURL=%2F'+childCase.id);
        return childCaseURL;        
        }
        else
            return null;
    }

}