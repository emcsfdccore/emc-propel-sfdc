/*========================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER                WR       DESCRIPTION                               
 |  ====          =========                ==       =========== 
 |  04/20/2015    Sneha Jain    Cases for PSC -May'15   Controller for PSC_Request_Escalation 
 +=======================================================================================================*/
 
public class PSC_Request_Escalation_Ctrl{
  
    public Case caseObj {get; set;}
    public boolean displayJustification {get;set;}
    public string escJustify {get;set;}
    
    public PSC_Request_Escalation_Ctrl(ApexPages.StandardController controller){
        
        caseObj =(Case)controller.getRecord();
        displayJustification = true;
        caseObj =[select id, Escalation_Request__c,Escalation_Justification__c from Case where Id =: caseObj.Id ];
        
        if(caseObj !=null){
            if(caseObj.Escalation_Request__c == true){
                displayJustification = false;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,System.Label.PSC_Esc_Request_Received);
                ApexPages.addMessage(myMsg); 
            }
            else{
                caseObj.Escalation_Request__c = true;
            }
        }   
    }
    public pageReference SaveAction(){  
        
        Pagereference returnToCase = null;
        
        try{
            caseObj.Escalation_Justification__c=escJustify;
            update caseObj;
            returnToCase = new pagereference('/'+caseObj.Id);
            return returnToCase;
            
        }catch(DMLException ex){
           ApexPages.addMessages(ex);
           return null;
        }
    }
}