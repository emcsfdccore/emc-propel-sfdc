/**
 * @author: Srikrishna SM
 * @Date: 11/03/2014
 * @Description: Test class to cover AssetCampaignJncAfterInsertUpdateDelete trigger
 */
@isTest
private class R2R_AssetCampJncAfterInsertUpdateDel_TC {
    static testMethod void onDeleteUpdateAssetsTest() {
        CustomSettingDataHelper.eBizSFDCIntCSData();
        CustomSettingDataHelper.dataValueMapCSData();
        String campaignName = '';
        //Insert Asset Based Campaign
        Asset_Based_Campaigns__c assetBasedCamp = new Asset_Based_Campaigns__c();
        assetBasedCamp.Name = 'Year 2014';
        assetBasedCamp.Campaign_Eligibility__c = 'Eligible';
        insert assetBasedCamp;
        
        //Insert Account
        Account account = AccountAndProfileTestClassDataHelper.createSimpleAccount();
        insert account;
        
        //Insert Asset
        Asset__c testAsset = new Asset__c();
        testAsset.Name = 'Test1';
        testAsset.Customer_Name__c = account.Id;
        insert testAsset;
        
        //Insert Junction Objects
        List<Asset_Campaign_Junction__c> acJunctionList = new List<Asset_Campaign_Junction__c>();
        
        Asset_Campaign_Junction__c acjunction1 = new Asset_Campaign_Junction__c();
        acjunction1.Related_Asset__c = testAsset.Id;
        acjunction1.Related_Asset_Campaign__c = assetBasedCamp.Id;
        acJunctionList.add(acjunction1);
        
        Asset_Campaign_Junction__c acjunction2 = new Asset_Campaign_Junction__c();
        acjunction2.Related_Asset__c = testAsset.Id;
        acjunction2.Related_Asset_Campaign__c = assetBasedCamp.Id;
        acJunctionList.add(acjunction2);
        
        insert acJunctionList;
        
        List<Asset_Campaign_Junction__c> assetCampJunList = [Select Id, name from Asset_Campaign_Junction__c where id in :acJunctionList];
        if(!assetCampJunList.isEmpty())
        delete assetCampJunList;
    }
    
}