/*=================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE           DEVELOPER        WR           DESCRIPTION                               
 |  ====           ==========       ==           =========== 
 |  17-JUNE-13     Y. Salazar       268708       Initial Creation.
 |  CI FEB'14 Release change req id 508 
 |  16 Sep 2014   Akash Rastogi     0659         Updated 'Sales Associate User' to 'Sales Associate User APJ' and corresponding 'Profile Id' too
 +=================================================================================*/

public class EBC_BypassNewBriefingEvent {

    private final ApexPages.standardController controller;
    public Profile p {get;set;}
    public Account a {get;set;}
    public String accountId {get;set;}
    public String accountName {get;set;}
    public Map<Id,String> masterLayout = new Map<Id,String>();
    public Map<Id,String> limitedLayout = new Map<Id,String>();
    
    public EBC_BypassNewBriefingEvent(ApexPages.StandardController controller) {
      this.controller = controller;
      accountId = ApexPages.currentPage().getParameters().get('aid');
      accountName = ApexPages.currentPage().getParameters().get('acc');
      
      masterLayout.put('00e70000001FhQQ', 'AMER Direct Reseller Partner User');
      masterLayout.put('00e70000001FhQV', 'AMER Direct Reseller Super User');
      masterLayout.put('00e70000001FhPm', 'AMER Distribution VAR Partner User');
      masterLayout.put('00e70000001FhPr', 'AMER Distribution VAR Super User');
      masterLayout.put('00e70000001FhQH', 'AMER Distributor Partner User');
      masterLayout.put('00e70000001FhQa', 'AMER Distributor Super User');
      masterLayout.put('00e70000001FhQf', 'AMER Non Reseller Partner User');
      masterLayout.put('00e70000001FhQk', 'AMER Non Reseller Super User');
      masterLayout.put('00e70000001FbTO', 'AMER PSC User');
      masterLayout.put('00e70000001Fhdj', 'APJ CRM/PRM User - No Reporting');
      masterLayout.put('00e70000001FhPw', 'APJ Direct Reseller Partner User');
      masterLayout.put('00e70000001FhQ1', 'APJ Direct Reseller Super User');
      masterLayout.put('00e70000001FhQ6', 'APJ Distribution VAR Partner User');
      masterLayout.put('00e70000001FhQB', 'APJ Distribution VAR Super User');
      masterLayout.put('00e70000001FhQG', 'APJ Distributor Partner User');
      masterLayout.put('00e70000001FhQL', 'APJ Distributor Super User');
      masterLayout.put('00e70000001FhQp', 'APJ Non Reseller Partner User');
      masterLayout.put('00e70000001FhQu', 'APJ Non Reseller Super User');
      masterLayout.put('00e70000001FbTY', 'APJ PSC User');
      masterLayout.put('00e70000001FbTd', 'APJ PSC User (Double Byte)');
      masterLayout.put('00e70000001Fh9U', 'Aprimo Integration Administrator');
      masterLayout.put('00e70000001FacU', 'Business Admin');
      masterLayout.put('00e70000001Fa7U', 'Business Operations');
      masterLayout.put('00e70000001Fgeb', 'Business Ops & Contract Manager');
      masterLayout.put('00e70000001FhFN', 'Channels Velocity Auditor');
      masterLayout.put('00e70000001FifH', 'Cloud9 Read Only');
      masterLayout.put('00e70000001Fb0l', 'CMA Channels User');
      masterLayout.put('00e70000000wUfL', 'CMA Standard User');
      masterLayout.put('00e70000000wBzg', 'Contract Manager');
      masterLayout.put('00e70000001FhdP', 'Deal Desk');
      masterLayout.put('00e70000001FhV1', 'EBC Integration - API Only');
      masterLayout.put('00e70000001FbAv', 'EBC Power User');
      masterLayout.put('00e70000001FjG3', 'Education Services Manager');
      masterLayout.put('00e70000001FjFy', 'Education Services Rep');
      masterLayout.put('00e70000001FaZu', 'Education Services User');
      masterLayout.put('00e70000001FaZz', 'EMC Allocations User');
      masterLayout.put('00e70000001FhRn', 'EMC Channel Marketing');
      masterLayout.put('00e70000001Faa4', 'EMC Consulting User');
      masterLayout.put('00e70000001FiRo', 'EMC Consulting User - Read Only');
      masterLayout.put('00e70000001Fbrf', 'EMC Contract & Grouping Administrator');
      masterLayout.put('00e70000001FbrV', 'EMC Contract Manager');
      masterLayout.put('00e70000001Faa9', 'EMC Contract Renewals User');
      masterLayout.put('00e70000001FhRd', 'EMC District Coordinator');
      masterLayout.put('00e70000001FgoR', 'EMC ESBI User');
      masterLayout.put('00e70000001FaaE', 'EMC GFS User');
      masterLayout.put('00e70000001Fbra', 'EMC Grouping Administrator');
      masterLayout.put('00e70000001Fiz8', 'EMC Partner User Admin');
      masterLayout.put('00e70000001Fj0j', 'EMC Read Only User');
      masterLayout.put('00e70000001FcTe', 'EMC Reference Manager User');
      masterLayout.put('00e70000001Fb0R', 'EMC SSF User');
      masterLayout.put('00e70000001FgeW', 'EMC SSF User (Double Byte)');
      masterLayout.put('00e70000001FaZR', 'EMC Tech Alliances User');
      masterLayout.put('00e70000001Fbqm', 'EMEA Direct Reseller Partner User');
      masterLayout.put('00e70000001FbrB', 'EMEA Direct Reseller Super User');
      masterLayout.put('00e70000001Fbqr', 'EMEA Distribution VAR Partner User');
      masterLayout.put('00e70000001FbrG', 'EMEA Distribution VAR Super User');
      masterLayout.put('00e70000001Fbqw', 'EMEA Distributor Partner User');
      masterLayout.put('00e70000001Fbr1', 'EMEA Distributor Super User');
      masterLayout.put('00e70000001Fbr6', 'EMEA Non Reseller Partner User');
      masterLayout.put('00e70000001FbrL', 'EMEA Non Reseller Super User');
      masterLayout.put('00e70000001FbTT', 'EMEA PSC User');
      masterLayout.put('00e70000001FifM', 'Field Partner Marketing Mgr');
      masterLayout.put('00e70000000wyXC', 'Gold Partner User');
      masterLayout.put('00e70000000weNT', 'Ideas Only User');
      masterLayout.put('00e70000001Fin1', 'Isilon-read only');
      masterLayout.put('00e70000001FjWu', 'Knowledge Author User');
      masterLayout.put('00e70000001FjWx', 'Knowledge KCS User');
      masterLayout.put('00e70000001Fixk', 'Knowledge Only User');
      masterLayout.put('00e70000001FjX1', 'Knowledge Read-Only User');
      masterLayout.put('00e70000001FjX2', 'Knowledge Reviewer User');
      masterLayout.put('00e70000001Fbv3', 'Lead Routing Admin');
      masterLayout.put('00e70000000x19i', 'Marketing Standard User');
      masterLayout.put('00e70000000wBzf', 'Marketing User');
      masterLayout.put('00e70000001Fhdt', 'Oracle Deal Reg');
      masterLayout.put('00e70000001Fi9z', 'Presales Service Manager');
      masterLayout.put('00e70000001FiA4', 'Presales Service Rep');
      masterLayout.put('00eV0000000QMj3', 'Presales Solutions Support Manager');
      masterLayout.put('00e70000001Fizg', 'Pricing User');
      masterLayout.put('00e70000001FbAM', 'PRM Project Toolkit User');
      masterLayout.put('00e70000000wBzd', 'Read Only');
      masterLayout.put('00e70000001FhR9', 'ROInnovations Admin'); 
      //masterLayout.put('00e70000000wUfU', 'Sales Ops');
      masterLayout.put('00e70000001FhFI', 'Service Provider Admin');
      masterLayout.put('00e70000001Fj5o', 'SFDC TEST AMER Inside Sales/SMB User');
      masterLayout.put('00e70000001Fj71', 'SFDC Test Touch AMER Commercial User');
      masterLayout.put('00e70000000wBze', 'Solution Manager');
      masterLayout.put('00e70000001FaHM', 'Standard Platform User');
      masterLayout.put('00e70000000wBzc', 'Standard User');
      masterLayout.put('00e70000000wBza', 'System Administrator');
      masterLayout.put('00e70000000wRaB', 'System Administrator - API Only');
      masterLayout.put('00e70000001Fimm', 'System Administrator - API Only VLAB');
      masterLayout.put('00e70000001FiPn', 'System Administrator � BTG');
      masterLayout.put('00e70000001FgoT', 'System Administrator - TA Sync');
      masterLayout.put('00e70000000wztu', 'User Management');
      masterLayout.put('00e70000000x1Zv', 'User Management + Reporting');
      masterLayout.put('00e70000001Fbeq', 'VCE Reporting User');
      masterLayout.put('00e70000001Fber', 'VCE Support User');
      masterLayout.put('00e70000001FhAh', 'VSPM Channels');
      
      limitedLayout.put('00e70000001FbBK', 'EBC View Only');
      limitedLayout.put('00e70000001Fby7', 'AMER Channels User');
      limitedLayout.put('00e70000001FaZ6', 'AMER Commercial User');
      limitedLayout.put('00e70000001FhRO', 'AMER Commercial User + FIMS');
      limitedLayout.put('00e70000001Fi9f', 'AMER CORE TC');
      limitedLayout.put('00e70000001FaZB', 'AMER Enterprise User');
      limitedLayout.put('00e70000001FaZG', 'AMER Inside Sales/SMB User');
      limitedLayout.put('00e70000001FiJ6', 'AMER Partner TC');
      limitedLayout.put('00e70000001FbyH', 'APJ Channels User');
      limitedLayout.put('00e70000001FbyM', 'APJ Channels User (Double Byte)');
      limitedLayout.put('00e70000001FaZQ', 'APJ Commercial User');
      limitedLayout.put('00e70000001FaZV', 'APJ Commercial User (Double Byte)');
      limitedLayout.put('00e70000001Fi9p', 'APJ Core TC');
      limitedLayout.put('00e70000001FaZa', 'APJ Enterprise User');
      limitedLayout.put('00e70000001FaZf', 'APJ Enterprise User (Double Byte)');
      limitedLayout.put('00e70000001FaZk', 'APJ Inside Sales/SMB User');
      limitedLayout.put('00e70000001FaZp', 'APJ Inside Sales/SMB User (Double Byte)');
      limitedLayout.put('00e70000001FiRj', 'APJ Partner TC');
      limitedLayout.put('00e70000001FbBP', 'EBC User');
      limitedLayout.put('00e70000001FbgI', 'EMC GAM User');
      limitedLayout.put('00e70000001Fiz7', 'EMC GAM User (Double Byte)');
      limitedLayout.put('00e70000001FbyC', 'EMEA Channels User');
      limitedLayout.put('00e70000001FaaJ', 'EMEA Commercial User');
      limitedLayout.put('00e70000001Fi9k', 'EMEA CORE TC');
      limitedLayout.put('00e70000001FaaO', 'EMEA Enterprise User');
      limitedLayout.put('00e70000001FaaT', 'EMEA Inside Sales/SMB User');
      limitedLayout.put('00e70000001FiRe', 'EMEA Partner TC');
      // WR # 0659 Updated 'Sales Associate User' to 'Sales Associate User APJ' and corresponding 'Profile Id' too
      Map<String,CustomSettingDataValueMap__c> DataValueMap= CustomSettingDataValueMap__c.getall();
     String proId= DataValueMap.get('Sales Associate User APJ').DataValue__c;
      limitedLayout.put(proId, 'Sales Associate User APJ'); 
    }
    
    public pageReference newBriefingEvent(){
      PageReference pr;
      
      if(masterLayout.containsKey(UserInfo.getProfileId())){
        pr = Page.EBC_Request_Master_Briefing_E_Layout;
      }
      
      else{
        pr = Page.EBC_Request_Limited_Briefing_E_Layout;
      }
      
      pr.getParameters().put('aid', accountId);
      pr.getParameters().put('acc', accountName);
      return pr;
    }
}