/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  02 Dec 2013      Sneha Jain                 Optimized the test class according to coding standards, removed seeAllData
 |  02.04.2014       Sneha Jain        R2R      Modified the class to increase the code coverage           
 |  08.27.2014       Partha Baruah     R2R      Modified the class to increase the code coverage
 |  9.04.2015        Bindu             CI 1901  Modified for code coverage
 +=========================================================================================================================*/

@isTest
private class R2R_Opportunity_Management_TC
{
    
    public static testmethod void TestsetOpportunityType()
    {
        //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
        } 
        
        //Insert user record where Profile is Sys Admin and Forecast Group is Maintenance Renewals
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='testR2Ruser2@emailR2R2Test.com', alias='tuser1', CommunityNickName='tuser1', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' ,Forecast_Group__c = 'Maintenance Renewals'); 
        
        insert insertUser;
        

        R2R_Opportunity_Management R2rOppManagement=new R2R_Opportunity_Management();
         
        //Creating Account record
        Account objAct = new Account();
        objAct.Name='Account1';
        List <Account> listAccount= new List<Account>();
        listAccount.add(objAct);
        insert listAccount;
        
        //Creating Opportunity record
        List <Opportunity> listOpportunity = new List<Opportunity>();
        for(Integer i=0; i<2;i++)
        {   
            Opportunity opp = new Opportunity(Name='Opt'+i,AccountId=listAccount[0].Id,StageName='Pipeline',
            CloseDate=system.today() + 10,Sales_Channel__c='Direct',Sales_Force__c='EMC');
            listOpportunity.add(opp);
        }
        system.runAs(insertUser)
        {   
            insert listOpportunity;
        }
        
        Test.startTest();
        R2rOppManagement.setOpportunityType(listOpportunity);     
        Test.stopTest();
    }
    
    public static testmethod void setOpportunityType()
    {
        //Creating Custom setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
        }
        
        List<Account> lstAccountToInsert = new list<Account>();
        List<Lead> lstLeadToInsert = new list<Lead>();
        List<Asset__c> listAssets=new List<Asset__c>();
        List<Opportunity> listOpportunity = new list<Opportunity>();
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where DeveloperName like 'EMC_Install' or DeveloperName Like 'Competitive_Install'];
        
        R2R_Opportunity_Management R2rOppManagement=new R2R_Opportunity_Management();
        
        //Create Opportunity records
        Opportunity opp=testclassUtils.getOppty ();
        opp.Opportunity_Type__c='Refresh/Swap';
        listOpportunity.add(opp);
 
        Opportunity opp1=testclassUtils.getOppty ();
        opp1.Opportunity_Type__c='Refresh';
        listOpportunity.add(opp1);
          
        try{
            insert listOpportunity;
        }catch(Exception e){System.debug(+e);}    
    
        //creating account records
        Account acc=testclassUtils.getAccount () ;
        insert acc;
   
        //Creating Asset records
        List<Asset__c> listAsset=new List<Asset__c>();
        Asset__c asset1= new Asset__c();
        asset1.Name='Testasset1';
        asset1.Customer_Name__c=acc.id;
        asset1.RecordTypeId=recordType[0].id;
        asset1.Red_Zone_Priority__c=null;
        asset1.Deffered_to_Renewals__c= false;
        listAsset.add(asset1);
        
        Asset__c asset2= new Asset__c();
        asset2.Name='Testasset2';
        asset2.Customer_Name__c=acc.id;
        asset2.RecordTypeId=recordType[1].id;
        asset2.Red_Zone_Priority__c=null;
        asset2.Deffered_to_Renewals__c= false;
        listAsset.add(asset2);
         
        Asset__c asset3= new Asset__c();
        asset3.Name='Testasset3';
        asset3.Customer_Name__c=acc.id;
        asset3.RecordTypeId=recordType[0].id;
        asset3.Red_Zone_Priority__c=null;
        asset3.Deffered_to_Renewals__c= false;
        listAsset.add(asset3);
                 
        Asset__c asset4= new Asset__c();
        asset4.Name='Testasset4';
        asset4.Customer_Name__c=acc.id;
        asset4.RecordTypeId=recordType[1].id;
        asset4.Red_Zone_Priority__c=null;
        asset4.Deffered_to_Renewals__c= false;
        listAsset.add(asset4);
        
        insert listAsset;
           
        //Creating Opportunity Asset Junction records
        List<Opportunity_Asset_Junction__c> listOAJ = new list<Opportunity_Asset_Junction__c>();
        Opportunity_Asset_Junction__c oaj1=new Opportunity_Asset_Junction__c();
        oaj1.Related_Asset__c=listAsset[0].id;
        oaj1.Related_Account__c=acc.id;
        oaj1.Related_Opportunity__c=opp.id;
        listOAJ.add(oaj1);
               
        Opportunity_Asset_Junction__c oaj2=new Opportunity_Asset_Junction__c();
        oaj2.Related_Asset__c=listAsset[1].id;
        oaj2.Related_Account__c=acc.id;
        oaj2.Related_Opportunity__c=opp1.id;
        listOAJ.add(oaj2);
        
        Opportunity_Asset_Junction__c oaj3=new Opportunity_Asset_Junction__c();
        oaj3.Related_Asset__c=listAsset[2].id;
        oaj3.Related_Account__c=acc.id;
        oaj3.Related_Opportunity__c=opp1.id;
        listOAJ.add(oaj3);
        
        Opportunity_Asset_Junction__c oaj4=new Opportunity_Asset_Junction__c();
        oaj4.Related_Asset__c=listAsset[3].id;
        oaj4.Related_Account__c=acc.id;
        oaj4.Related_Opportunity__c=opp.id;
        listOAJ.add(oaj4);
        
        try{
        insert listOAJ;
        }
        catch(Exception e){}
        
        boolean isBoolean=true;
        Test.startTest();
        R2rOppManagement.setOpportunityType(listOAJ,isBoolean);
        Test.stopTest();
    }
    
    public static testmethod void testAssetUnlinking()
    {
        try
        {
            //Creating Custom setting data
            System.runAs(new user(Id = UserInfo.getUserId()))
            {
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
                CustomSettingDataHelper.countryTheaterMapCSData();
            }
            
            //Insert user record where Profile is EMC Contract Renewal User and/or APJ Commercial USer and Forecast Group is Maintenance Renewals
            String emcContractRenewalUser = '';
            String APJCommercialUser = '';
            List<Profile> profileIDList = [ Select id,name from Profile where name ='EMC Contract Renewals User' OR name = 'APJ Commercial User'];
            if(profileIDList[0].name == 'EMC Contract Renewals User')
            {
                emcContractRenewalUser = profileIDList[0].Id;
                APJCommercialUser = profileIDList[1].Id;
            }
            if(profileIDList[0].name == 'APJ Commercial User')
            {
                emcContractRenewalUser = profileIDList[1].Id;
                APJCommercialUser = profileIDList[0].Id;
            }
            
            List<User> userList = new List<User>();
            User insertUser = new user(email='testRenewalUser@emailTest.com',profileId = emcContractRenewalUser, UserName='testRenewalR2R1User@emailTest.com', alias='tuser2', CommunityNickName='tuser2', 
            TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
            LanguageLocaleKey='en_US', FirstName = 'Test1', LastName = 'User1' ,Forecast_Group__c = 'Maintenance Renewals'); 
            userList.add(insertUser);
            User insertUser1 = new user(email='testAPJUser@emailTest.com',profileId = APJCommercialUser, UserName='testAPJUser@emailTest.com', alias='tuser3', CommunityNickName='tuser3', 
            TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
            LanguageLocaleKey='en_US', FirstName = 'Test2', LastName = 'User2' ,Forecast_Group__c = 'Maintenance Renewals'); 
            userList.add(insertUser1);
            
            insert userList;
            
            List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where DeveloperName like 'EMC_Install' or DeveloperName Like 'Competitive_Install'];
            List<Asset__c> lstAssetToInsert = new list<Asset__c>();
            List<Opportunity_Asset_Junction__c> lstOptyAssetJunction = new list<Opportunity_Asset_Junction__c>();
            List<Account> lstAccountToInsert = new list<Account>();
            List<Lead> lstLeadToInsert = new list<Lead>();
            List<Opportunity> listOpportunity = new list<Opportunity>();

            // Creating Account records
            Account acc=testclassUtils.getAccount () ;
            insert acc;

            //Creating Opportunity records
            Opportunity opp=new Opportunity();
            opp.Name='Testoppty12';
            opp.AccountId = acc.Id;
            Date closeDate =  System.today()+5;
            opp.CloseDate=closeDate;
            opp.Sales_Channel__c='Direct';
            opp.Sales_Force__c='EMC';
            opp.StageName='Pipline';
            //CI 1901
            opp.T15_Notification__c=TRUE;
            opp.T45_Notification__c=TRUE;
            opp.T90_Notification__c=TRUE;
            opp.Term_Notification__c=TRUE;
            opp.Booking_Certificate__c=TRUE;
            //End of CI 1901
            listOpportunity.add(opp);
            try{
            insert listOpportunity;
            listOpportunity[0].Opportunity_Type__c='refresh';
            update listOpportunity;}
            catch(exception e)
            {
            }
            //creating assets records
            Asset__c asset1= new Asset__c();
            asset1.Name='Testasset1';
            asset1.Customer_Name__c=acc.id;
            asset1.RecordTypeId=recordType[0].id;
            lstAssetToInsert.add(asset1);
                
            Asset__c asset2= new Asset__c();
            asset2.Name='Testasset2';
            asset2.Customer_Name__c=acc.id;
            asset2.RecordTypeId=recordType[1].id;      
            lstAssetToInsert.add(asset2);
            
            Asset__c asset3= new Asset__c();
            asset3.Name='Testasset3';
            asset3.Customer_Name__c=acc.id;
            asset3.RecordTypeId=recordType[1].id;      
            lstAssetToInsert.add(asset3);
            
            Asset__c asset4= new Asset__c();
            asset4.Name='Testasset4';
            asset4.Customer_Name__c=acc.id;
            asset4.RecordTypeId=recordType[1].id;      
            lstAssetToInsert.add(asset4);
            
            insert lstAssetToInsert;
                
            //creating Opportunity Asset Junction records
            Opportunity_Asset_Junction__c Oaj1=new Opportunity_Asset_Junction__c();
            Oaj1.Related_Asset__c=lstAssetToInsert[0].id;
            Oaj1.Related_Account__c=acc.id;
            Oaj1.Related_Opportunity__c=opp.id;
            lstOptyAssetJunction.add(Oaj1);
                
            Opportunity_Asset_Junction__c Oaj2=new Opportunity_Asset_Junction__c();
            Oaj2.Related_Asset__c=lstAssetToInsert[1].id;
            Oaj2.Related_Account__c=acc.id;
            Oaj2.Related_Opportunity__c=opp.id;
            lstOptyAssetJunction.add(Oaj2); 
            
            Opportunity_Asset_Junction__c Oaj3=new Opportunity_Asset_Junction__c();
            Oaj3.Related_Asset__c=lstAssetToInsert[2].id;
            Oaj3.Related_Account__c=acc.id;
            Oaj3.Related_Opportunity__c=opp.id;
            lstOptyAssetJunction.add(Oaj3); 
            
            Opportunity_Asset_Junction__c Oaj4=new Opportunity_Asset_Junction__c();
            Oaj4.Related_Asset__c=lstAssetToInsert[3].id;
            Oaj4.Related_Account__c=acc.id;
            Oaj4.Related_Opportunity__c=opp.id;
            lstOptyAssetJunction.add(Oaj4); 
            
            
            try{
                insert lstOptyAssetJunction;
            }
            catch(exception e){
            }

            //Collecting Ids of Opportunity Asset Junction records
            list<id> lstlinkid = new list<id>();
            lstlinkid.add(lstOptyAssetJunction[0].id);
            lstlinkid.add(lstOptyAssetJunction[1].id);
            
            list<id> lstlinkidForRenewal = new list<id>();
            lstlinkidForRenewal.add(lstOptyAssetJunction[2].id);
            
            list<id> lstlinkidForAPJ = new list<id>();
            lstlinkidForAPJ.add(lstOptyAssetJunction[3].id);
            
            Test.startTest();
            String optyrecord = null;
            //Added a new string to hold the message returned by assetDeferUnlinking function
            String optyrecord1 = null;
            String successmessage = 'failure';
            optyrecord = R2R_Opportunity_Management.AssetUnlinking(opp.id,lstlinkid); 
            //Calling these functions to increase the coverage
            optyrecord1= R2R_Opportunity_Management.assetDeferUnlinking(opp.id,lstlinkidForRenewal);
            R2R_Opportunity_Management.deferAsset(lstOptyAssetJunction);    
            system.runAs(userList[0])
            {
                optyrecord = R2R_Opportunity_Management.AssetUnlinking(opp.id,lstlinkidForRenewal);
                //Calling these functions to increase the coverage
                optyrecord1= R2R_Opportunity_Management.assetDeferUnlinking(opp.id,lstlinkidForRenewal);
                R2R_Opportunity_Management.deferAsset(lstOptyAssetJunction);
            }
            
            system.runAs(userList[1])
            {
                optyrecord = R2R_Opportunity_Management.AssetUnlinking(opp.id,lstlinkidForAPJ);
                //Calling these functions to increase the coverage
                optyrecord1= R2R_Opportunity_Management.assetDeferUnlinking(opp.id,lstlinkidForRenewal);
                R2R_Opportunity_Management.deferAsset(lstOptyAssetJunction);
            }
            Test.stopTest();
        }
        catch(Exception e){  }
    }
    
    private static testmethod void testAssetLinking()
    {
        //Creating custom setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
        }
        
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where DeveloperName like 'EMC_Install' or DeveloperName Like 'Competitive_Install']; 

        //Creating Account records
        Account acc=testclassUtils.getAccount () ;
        insert acc;
        
        //Creating Asset records
        String competitiveRecordType = '';
        String emcInstallRecordType = '';
        if(recordType[0].DeveloperName == 'Competitive_Install')
        {
            competitiveRecordType = recordType[0].id;
            emcInstallRecordType = recordType[1].id;
        }
        if(recordType[0].DeveloperName == 'EMC_Install')
        {
            competitiveRecordType = recordType[1].id;
            emcInstallRecordType = recordType[0].id;
        }
        
        List<Asset__c> lstAssetToInsert = new list<Asset__c>();
        //Competitive Asset
        lstAssetToInsert.add(new Asset__c(Name='Testasset1',Customer_Name__c=acc.id,RecordTypeId=competitiveRecordType));
        lstAssetToInsert.add(new Asset__c(Name='Testasset2',Customer_Name__c=acc.id,RecordTypeId=competitiveRecordType));
        //EMC Install Asset
        lstAssetToInsert.add(new Asset__c(Name='Testasset3',Customer_Name__c=acc.id,RecordTypeId=emcInstallRecordType));
        lstAssetToInsert.add(new Asset__c(Name='Testasset4',Customer_Name__c=acc.id,RecordTypeId=emcInstallRecordType));
            
        insert lstAssetToInsert; 
        
        //Creating Opportunity records
        List<Opportunity> listOpp=new List<Opportunity>();
        listOpp.add(new Opportunity(Name='Testoppty1231',AccountId = acc.Id,Opportunity_Type__c='Renewals',closeDate = System.today()+5,Sales_Channel__c='Direct',Sales_Force__c='EMC',StageName='Pipeline'));
        listOpp.add(new Opportunity(Name='Testoppty1231',AccountId = acc.Id,Opportunity_Type__c='Refresh/Swap',closeDate=System.today()+6,Sales_Channel__c='Direct',Sales_Force__c='EMC',StageName='Pipeline'));
        
        try{
        insert listOpp;
        listOpp[0].Opportunity_Type__c='Renewals';
        update listOpp;
        }
        catch(Exception e){}
  
        //Creating Opportunity asset junction records
        List<Opportunity_Asset_Junction__c> listOaj= new List<Opportunity_Asset_Junction__c>();
        listOaj.add(new Opportunity_Asset_Junction__c(Related_Asset__c=lstAssetToInsert[0].id,Related_Opportunity__c=listOpp[0].id,Related_Account__c=acc.id));
        listOaj.add(new Opportunity_Asset_Junction__c(Related_Asset__c=lstAssetToInsert[0].id,Related_Opportunity__c=listOpp[1].id,Related_Account__c=acc.id));
        listOaj.add(new Opportunity_Asset_Junction__c(Related_Asset__c=lstAssetToInsert[2].id,Related_Opportunity__c=listOpp[0].id,Related_Account__c=acc.id));
        listOaj.add(new Opportunity_Asset_Junction__c(Related_Asset__c=lstAssetToInsert[2].id,Related_Opportunity__c=listOpp[1].id,Related_Account__c=acc.id));
        try{
            insert listOaj;
        }catch(Exception e){System.debug(+e);}
        
                
        List<Id> listAsset=new List<Id>();
        listAsset.add(lstAssetToInsert[0].id);
        listAsset.add(lstAssetToInsert[1].id);
        listAsset.add(lstAssetToInsert[2].id);
        listAsset.add(lstAssetToInsert[3].id);
        
        Map<Id,List<Id>> mapOpptyIdWithAssets = new Map<Id,List<Id>>();
        mapOpptyIdWithAssets.put(listOpp[0].Id,listAsset);
        R2R_Opportunity_Management R2rOppManagement=new R2R_Opportunity_Management();
        Map<Id,List<Id>> mapOpptyIdWithAssets1 = new Map<Id,List<Id>>();
        mapOpptyIdWithAssets1.put(listOpp[1].Id,listAsset);
        R2R_Opportunity_Management R2rOppManagement1=new R2R_Opportunity_Management();
        
        Test.startTest();
        R2rOppManagement.validateAssetLinking(mapOpptyIdWithAssets);
        R2rOppManagement1.validateAssetLinking(mapOpptyIdWithAssets1);
        Test.stopTest();
    }
    
    private static testmethod void testValidateLinkings()
    {
        //Creating custom setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        }
        
        List<Asset__c> lstAssetToInsert = new list<Asset__c>();
        List<Opportunity_Asset_Junction__c> lstOptyAssetJunction = new list<Opportunity_Asset_Junction__c>();
        List<Account> lstAccountToInsert = new list<Account>();
        List<Lead> lstLeadToInsert = new list<Lead>();
        List<Opportunity> listOpportunity = new list<Opportunity>();
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where DeveloperName like 'EMC_Install' or DeveloperName Like 'Competitive_Install']; 
        
        //Creating account records
        Account acc=testclassUtils.getAccount () ;
        insert acc;
        
        //Creating opportunity records
        Opportunity opp1=testclassUtils.getOppty ();
        listOpportunity.add(opp1);
        
        //Creating another Opportunity with type='Refresh'
        Opportunity opp=new Opportunity();
        opp.Name='Testoppty1241';
        opp.AccountId = acc.Id;
        opp.Opportunity_Type__c='Refresh';
        Date closeDate =  System.today()+5;
        opp.CloseDate=closeDate;
        opp.Sales_Channel__c='Direct';
        opp.Sales_Force__c='EMC';
        opp.StageName='Pipeline';
        listOpportunity.add(opp);
        
        //Creating another Opportunity with type='Renewals'
        Opportunity opp2=new Opportunity();
        opp2.Name='Testoppty1242';
        opp2.AccountId = acc.Id;
        opp2.Opportunity_Type__c='Renewals';
        Date closeDate1 =  System.today()+5;
        opp2.CloseDate=closeDate1;
        opp2.Sales_Channel__c='Direct';
        opp2.Sales_Force__c='EMC';
        opp2.StageName='Pipeline';
        listOpportunity.add(opp2);
        try{
           insert listOpportunity;
           }catch(Exception e){System.debug(+e);}

        
        //creating Competitive asset records
        Asset__c asset1= new Asset__c();
        asset1.Name='Testasset1';
        asset1.Install_Base_Status__c='Displaced by EMC';
        asset1.Customer_Name__c=acc.id;
        asset1.RecordTypeId=recordType[0].id;
        lstAssetToInsert.add(asset1);
       
        //Creating EMC asset records
        Asset__c asset2= new Asset__c();
        asset2.Name='TestassetEMC';
        asset2.Install_Base_Status__c='Displaced by EMC';
        asset2.Customer_Name__c=acc.id;
        asset2.RecordTypeId=recordType[1].id;
        asset2.Product_Name_Vendor__c='EMC';
        lstAssetToInsert.add(asset2);
        
        insert lstAssetToInsert;
       
        List<Id> listAsset=new List<Id>();
        listAsset.add(asset1.id);
        listAsset.add(asset2.id);
    
        //Creating Lead records
        Lead lead1= new Lead();
        lead1.Company='TestLead';
        lead1.LastName='Test1231';
        lead1.Status='New';
        lead1.Lead_Type_Based_on_Linking__c='Refresh';
        lead1.Sales_Force__c='EMC';
        lead1.Lead_Originator__c='Customer Intelligence';
        lstLeadToInsert.add(lead1);
   
        //Creating another Lead
        Lead lead2= new Lead();
        lead2.Company='TestLead2';
        lead2.LastName='Test121';
        lead2.Status='New';
        lead2.Lead_Type_Based_on_Linking__c='Renewals';
        lead2.Sales_Force__c='EMC';
        lead2.Lead_Originator__c='Customer Intelligence';
        lstLeadToInsert.add(lead2);
        
        insert lstLeadToInsert;

        //Creating Opportunity asset junction records
        Opportunity_Asset_Junction__c Oaj= new Opportunity_Asset_Junction__c();
        Oaj.Related_Asset__c=lstAssetToInsert[0].id;
        Oaj.Related_Opportunity__c=opp.id;
        Oaj.Related_Account__c=acc.id;
        lstOptyAssetJunction.add(Oaj);
         
        Opportunity_Asset_Junction__c Oaj1= new Opportunity_Asset_Junction__c();
        Oaj1.Related_Asset__c=lstAssetToInsert[1].id;
        Oaj1.Related_Opportunity__c=opp2.id;
        Oaj1.Related_Account__c=acc.id;
        lstOptyAssetJunction.add(Oaj1);
        try{
        insert lstOptyAssetJunction;
        }
        catch(Exception e){System.debug(+e);}
       
       
        //Creating Lead asset junction records
        Lead_Asset_Junction__c laj2= new Lead_Asset_Junction__c();
        laj2.Related_Asset__c=lstAssetToInsert[0].id;
        laj2.Related_Lead__c=lead2.id;
        try{
            insert laj2;
        }catch(Exception e){}
    
        R2R_Opportunity_Management R2rOppManagement=new R2R_Opportunity_Management();
        Test.startTest();
        User nonadmin=[Select id,name,profileId from User where profileId='00e70000001Fjc9' and IsActive=TRUE limit 1];
        System.RunAs(nonadmin){
        R2rOppManagement.validateAssetOpportunityLinking(lstOptyAssetJunction);
        }
        R2rOppManagement.validateAssetOpportunityLinking(lstOptyAssetJunction);
        Test.stopTest();
    }
    private static testmethod void testTransferDuplicateOpty()
    {
        //Creating custom setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        }
        List<Asset__c> lstAssetToInsert = new list<Asset__c>();
        List<Opportunity_Asset_Junction__c> lstOptyAssetJunction = new list<Opportunity_Asset_Junction__c>();
        List<Account> lstAccountToInsert = new list<Account>();
        List<Opportunity> listOpportunity = new list<Opportunity>();
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where DeveloperName like 'EMC_Install' or DeveloperName Like 'Competitive_Install']; 
        
        //Creating account records
        Account acc=testclassUtils.getAccount () ;
        insert acc;
        
        //Creating opportunity records
        Opportunity opp1=testclassUtils.getOppty ();
        listOpportunity.add(opp1);
        
        //Creating another Opportunity with type='Refresh'
        Opportunity opp=new Opportunity();
        opp.Name='Testoppty1241';
        opp.AccountId = acc.Id;
        opp.Opportunity_Type__c='Refresh';
        Date closeDate =  System.today()+5;
        opp.CloseDate=closeDate;
        opp.Sales_Channel__c='Direct';
        opp.Sales_Force__c='EMC';
        opp.StageName='Pipeline';
        listOpportunity.add(opp);
        
        //Creating another Opportunity with type='Refresh'
        Opportunity opp3=new Opportunity();
        opp3.Name='Testoppty1243';
        opp3.AccountId = acc.Id;
        opp3.Opportunity_Type__c='Refresh';
        Date closeDate2 =  System.today()+5;
        opp3.CloseDate=closeDate2;
        opp3.Sales_Channel__c='Direct';
        opp3.Sales_Force__c='EMC';
        opp3.StageName='Pipeline';
        listOpportunity.add(opp3);
    
        
        //Creating another Opportunity with type='Renewals'
        Opportunity opp2=new Opportunity();
        opp2.Name='Testoppty1242';
        opp2.AccountId = acc.Id;
        opp2.Opportunity_Type__c='Renewals';
        Date closeDate1 =  System.today()+5;
        opp2.CloseDate=closeDate1;
        opp2.Sales_Channel__c='Direct';
        opp2.Sales_Force__c='EMC';
        opp2.StageName='Pipeline';
        listOpportunity.add(opp2);
        try{
           insert listOpportunity;
           listOpportunity[1].StageName='Closed';
           update listOpportunity;
           }catch(Exception e){System.debug(+e);}

        
        //creating Competitive asset records
        Asset__c asset1= new Asset__c();
        asset1.Name='Testasset1';
        asset1.Install_Base_Status__c='Displaced by EMC';
        asset1.Customer_Name__c=acc.id;
        asset1.RecordTypeId=recordType[0].id;
        lstAssetToInsert.add(asset1);
       
        //Creating EMC asset records
        Asset__c asset2= new Asset__c();
        asset2.Name='TestassetEMC';
        asset2.Install_Base_Status__c='Displaced by EMC';
        asset2.Customer_Name__c=acc.id;
        asset2.RecordTypeId=recordType[1].id;
        asset2.Product_Name_Vendor__c='EMC';
        lstAssetToInsert.add(asset2);
        
        insert lstAssetToInsert;
        //Creating Opportunity asset junction records
        Opportunity_Asset_Junction__c Oaj= new Opportunity_Asset_Junction__c();
        Oaj.Related_Asset__c=lstAssetToInsert[0].id;
        Oaj.Related_Opportunity__c=opp.id;
        Oaj.Related_Account__c=acc.id;
        lstOptyAssetJunction.add(Oaj);
        
        Opportunity_Asset_Junction__c Oaj2= new Opportunity_Asset_Junction__c();
        Oaj2.Related_Asset__c=lstAssetToInsert[0].id;
        Oaj2.Related_Opportunity__c=opp3.id;
        Oaj2.Related_Account__c=acc.id;
        lstOptyAssetJunction.add(Oaj2);
        
        Opportunity_Asset_Junction__c Oaj1= new Opportunity_Asset_Junction__c();
        Oaj1.Related_Asset__c=lstAssetToInsert[1].id;
        Oaj1.Related_Opportunity__c=opp2.id;
        Oaj1.Related_Account__c=acc.id;
        lstOptyAssetJunction.add(Oaj1);
        
        try{
        insert lstOptyAssetJunction;
        }
        catch(Exception e){System.debug(+e);}
        
        Map<Id,Id> DupOptyId = new Map<Id,Id>();
        DupOptyId.put(listOpportunity[1].id,listOpportunity[1].duplicate_opportunity__r.id);
        set<Opportunity>setOpty=new set<Opportunity>();
        for(Opportunity Opty:listOpportunity )
        {
        setOpty.add(Opty);
        }
        R2R_Opportunity_Management R2rOppManagement=new R2R_Opportunity_Management();
        Test.startTest();
        R2rOppManagement.transferAssetToDuplicateOpp(setOpty,DupOptyId);
        Test.stopTest();
        
       } 
}