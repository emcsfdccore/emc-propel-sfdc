public class KB_CalculateCountOfSR {


    private static boolean chkShareFlag  = false;

    public static boolean hasAlreadyChangedShare() {
    return chkShareFlag  ;
}

// By setting the variable to true, it maintains this new value throughout the duration of the request

public static void setAlreadyChangedShare() {
    chkShareFlag   = true;
 }
    
    public void calculateCountofSROnInsert(List<Linked_SR__c> lstLinkedSR, Boolean isTrigger){
        
        List<Article_Summary__c> lstTestArt = new List<Article_Summary__c>();
        List<Linked_SR__c> lstTestSR = new List<Linked_SR__c>();
        List<Linked_SR__c> lstTestSRToUpdate = new List<Linked_SR__c>();
        Map<String,List<Linked_SR__c>> mapArticleLinkedSR = new Map<String,List<Linked_SR__c>>(); 
        List<String> setOldArticle = new List<String>();
        List<String> setNewArticle = new List<String>();
        Map<String,Article_Summary__c> mapTestArt = new Map<String,Article_Summary__c>();
        
        system.debug('lstLinkedSR--->'+lstLinkedSR);
        for(Linked_SR__c linkedSR: lstLinkedSR){
            String srNumber = linkedSR.Article_Number__c;
            srNumber = srNumber.leftPad(9);
            srNumber = srNumber.Replace(' ','0');
            if(mapArticleLinkedSR.containsKey(srNumber)){
                mapArticleLinkedSR.get(srNumber).add(linkedSR);   
            }
            else{
                List<Linked_SR__c> lstSR = new List<Linked_SR__c>();
                lstSR.add(linkedSR);
                mapArticleLinkedSR.put(srNumber,lstSR);
            }
        }
        system.debug('mapArticleLinkedSR--->'+mapArticleLinkedSR);
            lstTestArt = [Select Id,Name,Article_Number__c from Article_Summary__c where Article_Number__c in:mapArticleLinkedSR.keyset()];
            for(Article_Summary__c artSumm:lstTestArt){
                mapTestArt.put(artSumm.Article_Number__c,artSumm);
            }
            /*if(lstTestArt.size()==0){
                for(String s: mapArticleLinkedSR.keyset()){
                    Article_Summary__c testArt = new Article_Summary__c();
                    testArt.Name = s;
                    testArt.Article_Number__c = s;
                    insert testArt;
                    
                    for(Linked_SR__c objSR :mapArticleLinkedSR.get(s)){
                        //Linked_SR__c testSR = new Linked_SR__c();
                        objSR.SR_Number__c = objSR.SR_Number__c;
                        objSR.Article_Summary__c = testArt.Id;
                        objSR.Article_Solved_My_Problem__c = objSR.Article_Solved_My_Problem__c;
                        
                        objSR.Article_Number__c = objSR.Article_Number__c;
                        
                        objSR.Originally_Created_By__c = objSR.Originally_Created_By__c;
                        objSR.Article_ID__c = objSR.Article_ID__c;   
                        objSR.Article_Version_ID__c = objSR.Article_Version_ID__c;
                        lstTestSR.add(objSR);
                        
                    }
                }
                if(isTrigger)
                insert lstTestSR;
                
            }
            else{
                for(Article_Summary__c objTestArt :lstTestArt){
                    if(mapArticleLinkedSR.containsKey(objTestArt.Article_Number__c)){
                        for(Linked_SR__c objSR :mapArticleLinkedSR.get(objTestArt.Article_Number__c)){
                            //Linked_SR__c testSR = new Linked_SR__c();
                            objSR.SR_Number__c = objSR.SR_Number__c;
                            objSR.Article_Summary__c = objTestArt.Id;
                            objSR.Article_Solved_My_Problem__c = objSR.Article_Solved_My_Problem__c;
                            
                            objSR.Article_Number__c = objSR.Article_Number__c;
                            
                            objSR.Originally_Created_By__c = objSR.Originally_Created_By__c;
                            objSR.Article_ID__c = objSR.Article_ID__c;   
                            objSR.Article_Version_ID__c = objSR.Article_Version_ID__c;
                                       
                            lstTestSR.add(objSR);
                            system.debug('lstTestSR---->'+lstTestSR);
                        }
                   }
                }
                if(isTrigger)
                insert lstTestSR;
                
            }*/
            for(String s: mapArticleLinkedSR.keyset()){
                if(mapTestArt.containsKey(s)){
                    for(Linked_SR__c objSR :mapArticleLinkedSR.get(s)){
                        //Linked_SR__c testSR = new Linked_SR__c();
                        objSR.SR_Number__c = objSR.SR_Number__c;
                        objSR.Article_Summary__c = mapTestArt.get(s).Id;
                        objSR.Article_Solved_My_Problem__c = objSR.Article_Solved_My_Problem__c;
                        
                        objSR.Article_Number__c = objSR.Article_Number__c;
                        
                        objSR.Originally_Created_By__c = objSR.Originally_Created_By__c;
                        objSR.Article_ID__c = objSR.Article_ID__c;   
                        objSR.Article_Version_ID__c = objSR.Article_Version_ID__c;
                                   
                        lstTestSR.add(objSR);
                        system.debug('lstTestSR---->'+lstTestSR);
                   }
                }
                else{
                    Article_Summary__c testArt = new Article_Summary__c();
                    testArt.Name = s;
                    testArt.Article_Number__c = s;
                    insert testArt; 

                    for(Linked_SR__c objSR :mapArticleLinkedSR.get(s)){
                        //Linked_SR__c testSR = new Linked_SR__c();
                        objSR.SR_Number__c = objSR.SR_Number__c;
                        objSR.Article_Summary__c = testArt.Id;
                        objSR.Article_Solved_My_Problem__c = objSR.Article_Solved_My_Problem__c;
                        
                        objSR.Article_Number__c = objSR.Article_Number__c;
                        
                        objSR.Originally_Created_By__c = objSR.Originally_Created_By__c;
                        objSR.Article_ID__c = objSR.Article_ID__c;   
                        objSR.Article_Version_ID__c = objSR.Article_Version_ID__c;
                        lstTestSR.add(objSR);
                        
                    }
                }
            }
            if(isTrigger)
            insert lstTestSR;
    }

@future
public static void getKnowledgeId(Set<Id> setOPTBug, Boolean isSR){

Map<String,KnowledgeArticleVersion> mapKnowArt = new Map<String,KnowledgeArticleVersion>();
List<KnowledgeArticleVersion> lstKnowArtPub = new List<KnowledgeArticleVersion>();
List<KnowledgeArticleVersion> lstKnowArtDraft = new List<KnowledgeArticleVersion>();
List<OPT_Bug__c> lstOPTToUpdate = new List<OPT_Bug__c>();
List<OPT_Bug__c> lstOPTBug = new List<OPT_Bug__c>();
Set<String> setArticleNo = new Set<String>();
Map<String,List<OPT_Bug__c>> mapOPT = new Map<String,List<OPT_Bug__c>>();
List<Linked_SR__c> lstSRToUpdate = new List<Linked_SR__c>();
List<Linked_SR__c> lstLnkSR = new List<Linked_SR__c>();

KB_CalculateCountOfSR.setAlreadyChangedShare();


if(!isSR){
    lstOPTToUpdate = [Select Id,Originally_Created_By__c,Article_Version_ID__c,Article_Number__c,Bug_Tracking_Number__c from OPT_Bug__c where Id in:setOPTBug];
}
else {
    lstSRToUpdate = [Select Id,Article_ID__c,Article_Version_ID__c,Originally_Created_By__c,Article_Number__c from Linked_SR__c where Id in : setOPTBug];   
}
for(OPT_Bug__c objOPTBug: lstOPTToUpdate){
      if(objOPTBug.Article_Number__c!=null){
            String srNumber = objOPTBug.Article_Number__c;
            srNumber = srNumber.leftPad(9);
            srNumber = srNumber.Replace(' ','0');
             setArticleNo.add(srNumber);
             if(mapOPT.containsKey(srNumber)){
                 mapOPT.get(srNumber).add(objOPTBug);
             }
             else{
                List<OPT_Bug__c> lst = new List<OPT_Bug__c>();
                lst.add(objOPTBug);
                mapOPT.put(srNumber,lst);
             }
        }

   
}
for(Linked_SR__c objSR: lstSRToUpdate){
     if(objSR.Article_Number__c!=null){
            String srNumber = objSR.Article_Number__c;
            srNumber = srNumber.leftPad(9);
            srNumber = srNumber.Replace(' ','0');
             setArticleNo.add(srNumber);
        }
}
system.debug('setArticleNo---'+setArticleNo);
lstKnowArtPub = [Select id,KnowledgeArticleId,ArticleType,ArticleNumber,PublishStatus,CreatedById from KnowledgeArticleVersion where ArticleNumber in:setArticleNo and PublishStatus = 'Online' and Language = 'en_US' ];
//if(lstKnowArtPub.size()==0)
lstKnowArtDraft = [Select id,KnowledgeArticleId,ArticleType,ArticleNumber,PublishStatus,CreatedById from KnowledgeArticleVersion where ArticleNumber in:setArticleNo and PublishStatus = 'Draft' and Language = 'en_US' ];

if(lstKnowArtPub.size()>0 && lstKnowArtDraft.size()==0 && !(isSR)){
    for(KnowledgeArticleVersion knowArtObj: lstKnowArtPub){
        mapKnowArt.put(knowArtObj.ArticleNumber,knowArtObj);
        String id = KbManagement.PublishingService.editOnlineArticle (knowArtObj.KnowledgeArticleId, false);
        system.debug('id--->'+id);
        KnowledgeArticleVersion knowArticleVersion = new KnowledgeArticleVersion();
        knowArticleVersion = [Select Id,ArticleNumber,ValidationStatus,ArticleType,KnowledgeArticleId from KnowledgeArticleVersion where Id=: id and PublishStatus = 'Draft' and Language = 'en_US'];
        KB_SubmitArticleForApproval obj = new KB_SubmitArticleForApproval();          
        sObject sourceObj = obj.getArticleRecord(knowArticleVersion.ArticleType,knowArticleVersion.Id);
        Object OPTBugNumber = sourceObj.get('Bug_Tracking_Number__c');
        String bugNumber = (String)OPTBugNumber;
        system.debug('bugNumber--->'+bugNumber);
        if(mapOPT.containsKey(knowArticleVersion.ArticleNumber)){
            for(OPT_Bug__c objOPT:mapOPT.get(knowArticleVersion.ArticleNumber)){
                if(bugNumber!=null)              
                    bugNumber+=','+objOPT.Bug_Tracking_Number__c;   
                else{
                    bugNumber = objOPT.Bug_Tracking_Number__c;
                }
            }
        }
        sourceObj.put('Bug_Tracking_Number__c',bugNumber);
        try{
            update sourceObj;
        }
        catch(Exception e){
            system.debug('e--->'+e);
        }
        KbManagement.PublishingService.publishArticle(knowArticleVersion.KnowledgeArticleId, false);
    }
}
else if(lstKnowArtDraft.size()>0 && !(isSR)){
    for(KnowledgeArticleVersion knowArtObj: lstKnowArtDraft){
        mapKnowArt.put(knowArtObj.ArticleNumber,knowArtObj);
        /*String id = KbManagement.PublishingService.editOnlineArticle (knowArtObj.KnowledgeArticleId, true);
        system.debug('id--->'+id);
        KnowledgeArticleVersion knowArticleVersion = new KnowledgeArticleVersion();
        knowArticleVersion = [Select Id,ArticleNumber,ValidationStatus,ArticleType,KnowledgeArticleId from KnowledgeArticleVersion where Id=: id and PublishStatus = 'Draft' and Language = 'en_US'];*/
        KB_SubmitArticleForApproval obj1 = new KB_SubmitArticleForApproval();         
        sObject sourceObj = obj1.getArticleRecord(knowArtObj.ArticleType,knowArtObj.Id);
        Object OPTBugNumber = sourceObj.get('Bug_Tracking_Number__c');
        String bugNumber = (String)OPTBugNumber;
        if(mapOPT.containsKey(knowArtObj.ArticleNumber)){
            for(OPT_Bug__c objOPT:mapOPT.get(knowArtObj.ArticleNumber)){
                if(bugNumber!=null)
                    
                    bugNumber+=','+objOPT.Bug_Tracking_Number__c;   
                else{
                    bugNumber = objOPT.Bug_Tracking_Number__c;
                }   
            }
        }
        sourceObj.put('Bug_Tracking_Number__c',bugNumber);
        try{
            update sourceObj;
        }
        catch(Exception e){
            system.debug('e--->'+e);
        }
    }
}

system.debug('mapKnowArt--->'+mapKnowArt);
for(OPT_Bug__c lnkObj :lstOPTToUpdate){
    if(mapKnowArt.size()>0){
        String srNumber = lnkObj.Article_Number__c;
            srNumber = srNumber.leftPad(9);
            srNumber = srNumber.Replace(' ','0');

        lnkObj.Originally_Created_By__c = mapKnowArt.get(srNumber).CreatedById;
        lnkObj.Article_Version_ID__c = mapKnowArt.get(srNumber).Id;

        lstOPTBug.add(lnkObj);
    }
}
if(lstOPTBug.size()>0)
update lstOPTBug;

for(Linked_SR__c lnkObj1 :lstSRToUpdate){
    if(mapKnowArt.size()>0){
        String srNumber = lnkObj1.Article_Number__c;
            srNumber = srNumber.leftPad(9);
            srNumber = srNumber.Replace(' ','0');

        lnkObj1.Originally_Created_By__c = mapKnowArt.get(srNumber).CreatedById;
        lnkObj1.Article_Version_ID__c = mapKnowArt.get(srNumber).Id;
        lnkObj1.Article_ID__c = mapKnowArt.get(srNumber).KnowledgeArticleId;

        lstLnkSR.add(lnkObj1);
    }
}
if(lstLnkSR.size()>0)
update lstLnkSR;

    }
}