@isTest
private class AP_Customer_Profiling_Account_Distric_TC 
{
    static testMethod void test()
    {	
    	User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
    	//Create custom settings data required for the test class
    	System.runAs(insertUser){
	    	CustomSettingDataHelper.dataValueMapCSData();
	    	CustomSettingDataHelper.eBizSFDCIntCSData();
    	}
    	//Insert District Lookup records
    	District_Lookup__c objDistrictLookup = new District_Lookup__c();
        objDistrictLookup.Name ='TestLookup';
        objDistrictLookup.Area__c='EDWARDS';
        insert objDistrictLookup;
        
        //Insert Account records
        Account accObj1 = AccountAndProfileTestClassDataHelper.createSimpleAccount();
        accObj1.District_Lookup__c = objDistrictLookup.Id; 
        accObj1.Customer_Profiled_Account__c=true;
        insert accObj1; 
    	
    	//Insert Customer Profile Audit Master records
    	Customer_Profile_Audit_Master__c masterRec = new Customer_Profile_Audit_Master__c();
    	masterRec.Account_District__c = 'GENCORP INC.:Sacramento District';
        masterRec.District_Lookup__c = accObj1.District_Lookup__c;
        masterRec.name = 'GENCORP INC.:Sacramento District';
        //masterRec.Profiled_Account_Total__c = 2.0;
        insert masterRec;
    	
    	//Insert Customer Profile Detail records
    	List<Customer_Profile_Audit_Detail__c> cpadList = new List<Customer_Profile_Audit_Detail__c>();
    	
    	Customer_Profile_Audit_Detail__c detailRec1 = new Customer_Profile_Audit_Detail__c();
    	detailRec1.Name = 'Test1';
    	detailRec1.Profile_Value__c = 1.8;
    	detailRec1.Customer_Profile_Audit_Master__c = masterRec.Id;
    	detailRec1.Account_Lookup__c = accObj1.Id; 
    	cpadList.add(detailRec1);

    	Customer_Profile_Audit_Detail__c detailRec2 = new Customer_Profile_Audit_Detail__c();
    	detailRec2.Name = 'Test1';
    	detailRec2.Profile_Value__c = 2.1;
    	detailRec2.Customer_Profile_Audit_Master__c = masterRec.Id;
    	detailRec2.Account_Lookup__c = accObj1.Id;
    	cpadList.add(detailRec2);
    	
    	if(!cpadList.isEmpty())
    	insert cpadList;
    	
    	Test.startTest();
        AP_Customer_Profiling_Account_District testObj = new AP_Customer_Profiling_Account_District();
        Database.executeBatch(testObj);
        Test.stopTest();
    }
}