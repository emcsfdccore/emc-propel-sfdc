/*=======================================================================================================+
|  HISTORY  |                                                                           
|  DATE          DEVELOPER        WR                    DESCRIPTION                               
|  ====          =========        ==                    =========== 
|  10-Mar-2014   Jaspreet Singh                         This class handles After Insert trigger Methods for Opportunity Object
|  12-Sep-2014   Aagesh Jose     CI 1167                Modified to not add chatter post on opportunity if user is partner
+=====================================================================================================*/

public class Visibility_ChatterPost{

    /* @ Method           - <This method is used to filters Opportunities for which we need to do Chatter Post>
       @ Params           - <Set of Opportunity Ids>
       @ Returns          - <void>
       @ throwsException  - <No Exception>
    */
    public void afterInsertFeedItems(Set<Id> oppIds){
        Map<Id, String> mapRecordChatter = new Map<Id, String>();
        
        //Query Related Account Owner Id and Core Quota Rep
        List<Opportunity> lstOpps;
        if(oppIds.size() != 0){
            try{
                lstOpps = [SELECT Id, CreatedBy.Id, Account.Core_Quota_Rep__c, Name, Account.Name, Account.Core_Quota_Rep__r.Name,LeadSource
                                  FROM Opportunity WHERE Id IN :oppIds];
            }catch(Exception e){
                System.debug('##QueryException-->'+e.getMessage());
            }
        }
        
        /*Iterate through Opps 
          Call the insertChatterFeedApi method to post chatter feed
          If User creating opportunity is different from core quota rep of the associated account.*/
        if(lstOpps != null && lstOpps.size() != 0){
            Boolean insertedOnePost = false;
            for(Opportunity tempObj : lstOpps){
                String currentUserName = UserInfo.getName();
                //1167 : adding "UserInfo.getUserType()!='PowerPartner'" in if condition
                if( tempObj.Account.Core_Quota_Rep__c != null && tempObj.CreatedBy.Id != tempObj.Account.Core_Quota_Rep__c && tempObj.LeadSource != 'Contract Renewal' && UserInfo.getUserType()!='PowerPartner' ){
                    Id coreRepId = tempObj.Account.Core_Quota_Rep__c;
                    Id oppId = tempObj.Id;
                    string chatterPostMsg = ' ' +Label.VisibilityChatterOpp + ' ' + tempObj.Name + ' ' + Label.VisibilityChatterCreated + ' ' + currentUserName + ' ' + Label.VisibilityChatterAccount + ' ' + tempObj.Account.Name;
                    insertChatterFeedApi(oppId,coreRepId,chatterPostMsg);
                    insertedOnePost = true;
                    }
                if(insertedOnePost)
                    break;
            }
        }
        System.debug('##mapRecordChatter-->'+mapRecordChatter);       
    }
    
    /* @ Method           - <This method is used to post chatter feed on any record using Connect Api>
       @ Params           - <Record Id on which you want to post feed Item>
                            <Mention's Id -- User or Record you want to metion in the post @User>
                            <text Message which you want to post>
       @ Returns          - <void>
       @ throwsException  - <No Exception>
    */
    public static void insertChatterFeedApi(Id oppId,Id coreRepId,String chatterPostMsg ){
        ConnectApi.FeedType feedType = ConnectApi.FeedType.Record;
        String subjectId = oppId;
        
        ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
        messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();

        ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
        mentionSegment.id = coreRepId;
        messageInput.messageSegments.add(mentionSegment);

        textSegment = new ConnectApi.TextSegmentInput();
        textSegment.text =chatterPostMsg;
        messageInput.messageSegments.add(textSegment);

        ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
        input.body = messageInput;

        ConnectApi.FeedItem feedItemRep = ConnectApi.ChatterFeeds.postFeedItem(null, feedType, subjectId, input, null); 
    }

}