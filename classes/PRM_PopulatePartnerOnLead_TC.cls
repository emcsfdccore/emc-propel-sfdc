/*HISTORY 

| 

| DATE DEVELOPER WR DESCRIPTION 

| ==== ========= == =========== 


| 06.11.2013 Aagesh 311032 Re-written test class to improve performance.*/

@isTest
private Class PRM_PopulatePartnerOnLead_TC{

 private static testMethod void startTest(){
          
            list<lead> leadtest = new list<lead> ();
            list<lead> lstLeadPullback = new list<lead>();
            list<lead> totlead = new list<lead>();
            list<User> inactiveUser = [Select id from User where isActive=false and usertype like '%Standard%' limit 1];
            
    //Bhanu
            list<User> inactiveUser2 = [Select id, name from User where isActive=true and usertype like '%Standard%' limit 1];
            System.debug('***22, inactiveUser2[0].Name= '+ inactiveUser2[0].name);
    //Bhanu
            User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];    
            System.runAs(insertUser)        // Creating test data for custom settings 
            {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            }
              System.runAs(insertUser)        // Creating test data for custom settings 
            {
            List<Account> acc = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();      
            insert acc;
            for(Account acc1 : acc)
               {
                acc1.Type='Partner';
                acc1.IsPartner=true;
               }
           update acc;
           
           Contact cont = UserProfileTestClassDataHelper.createContact();
           cont.AccountId=acc[0].Id;
           insert cont;
           User partner;
           Profile amerUserProf = [select Id from Profile where Name=: 'EMEA Distributor Super User'];
           partner = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.id,null,cont.Id);
           insert partner;
            
           Lead tLead1 = new Lead(LastName='testlm1', firstname='testfm1', company='test1', LeadOwnerchek__c = true,Last_EMC_Owner__c=inactiveUser[0].id,
                                  email='test1@test.com',ownerid=inactiveUser2[0].id,EMC_Pull_Back__c = true,Previous_Owner__c =partner.Id);                         
           Lead LeadPullback = new Lead(LastName='testlmp', firstname='testfmp', company='testp', DealRegOwner__c = true,
                                  email='test2@test.com',ownerid=partner.Id,LeadOwnerchek__c=true,Last_EMC_Owner__c =partner.Id,LastEMCOwnerIds__c='test12345',Previous_Owner__c =partner.Id); 
           Lead tLead2 = new Lead(LastName='testlm2', firstname='testfm2', company='test2', 
                                  email='test3@test.com',ownerid=partner.Id ,Lead_Originator__c = 'Partner',
                                  Originator_Details__c = 'Partner Created' ,Last_EMC_Owner__c = null);
           
           lstLeadPullback.add(LeadPullback);
           leadtest.add(tLead1);
           leadtest.add(tLead2);
           totlead.add(LeadPullback);
           totlead.add(tLead1);
           totlead.add(tLead2);
           insert totlead;
           
           Test.startTest();
            PRM_PopulatePartnerOnLead obj = new PRM_PopulatePartnerOnLead();  
            obj.populatePartneronLead(leadtest);

            obj.UpdateOwnerforLeadPullback(lstLeadPullback);
            obj.UpdateOwnerforLeadPullback(leadtest);
            obj.UpdateOwnerforDealReg(lstLeadPullback);
            PRM_PopulatePartnerOnLead.checkDistributorPartnerForDealReg('Distributor','Distribution VAR');
            PRM_PopulatePartnerOnLead.checkDistributionVarPartnerForDealReg('Distributor','Distribution VAR');
            PRM_PopulatePartnerOnLead.createLastEMCOwnerLeadShare(lstLeadPullback);
            PRM_PopulatePartnerOnLead.setPassToPartnerFlagOnLeadwithpartner(leadtest, null);
            QueueSobject Queue = [Select SobjectType, QueueId From QueueSobject where SobjectType = 'Lead' limit 1];
            RecordType rtype = [SELECT id FROM RecordType WHERE sObjectType='Lead' and Name='Deal Registration'];  
            for(lead a : leadtest){
              a.ownerId = Queue.QueueId;
              a.recordtypeId = rtype.id;
            }
            update leadtest;
            List<Lead> LeadsToProcess = New List<Lead>();
            LeadsToProcess = [SELECT id, ownerId, Reject_Lead__c, Accept_Lead__c,Tier_2_Partner__c, Partner__c, Previous_CAM_Email__c, CAM_Email__c, Channel__c,
                                  Last_Inside_Sales_Owner__c, Last_EMC_Owner__c,RecordTypeId FROM Lead 
                                  where (Partner__c = NULL AND Tier_2_Partner__c = NULL ) and (Reporting_Owner__r.ContactId<> null or owner.Name like 'Partner%')limit 5];
            List<Database.SaveResult> dsrs = Database.update(LeadsToProcess, false);
            if(dsrs.size()>0){
                  PRM_PopulatePartnerOnLead.logFailedRecords(dsrs);
            } 
            
            PRM_PopulatePartnerOnLead obj1 = new PRM_PopulatePartnerOnLead(); 
                        
            obj1.populatePartneronLead(leadtest); 
            obj1.setPassToPartnerFlagOnLead(leadtest);
            obj1.leadAssignedDistiorEMCuser();
            obj1.updatePartnerAccount(leadtest[1],'DistributionVAR');
//Bhanu 
            PRM_PopulatePartnerOnLead.LeadRoutingExecution = false;
             obj.populatePartneronLead(leadtest);
            tLead1.ownerId=inactiveUser[0].id;
              obj.UpdateOwnerforLeadPullback(totlead);
            //PRM_PopulatePartnerOnLead.LeadRoutingExecution = true;
//Bhanu;
            }
            Test.stopTest();
   }  
  
 
}