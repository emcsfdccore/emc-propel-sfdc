/*==================================================================================================================+

 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        =========== 
 |  16/07/2014    Srikrishna SM           This Test controller is used to Create/Update Article Metadata Records from Article Page
 +==================================================================================================================**/
public class ESKB_ArticleMetadataHandler{
    public Article_Metadata__c articleMetadata  {get;set;} {articleMetadata = new Article_Metadata__c();}
    public Id articleId{get;set;}
    public String articleNumber{get;set;}
    public String Title{get;set;}
    public String comments{get;set;}
    public String archiveStatus{get;set;}
    List<Article_Metadata__c> articleMetadataList;
    
    public ESKB_ArticleMetadataHandler(){
    	system.debug('---------------------------------------------------------------------1');
        try{
        	articleId = ApexPages.currentPage().getParameters().get('id');
        }catch(Exception msg){
        	ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Id Used');
        	ApexPages.addMessage(errorMessage);
        }
        articleNumber = ApexPages.currentPage().getParameters().get('articleNumber');
        Title=ApexPages.currentPage().getParameters().get('Title');
        articleMetadataList = [Select Id, Article_Title__c, Archive_Candidate__c, Archive_Comments__c, Article_Number__c from Article_Metadata__c where Article_Number__c =: articleNumber AND Article_Title__c=:Title];
        if(!articleMetadataList.isEmpty()){
            articleMetadata.Archive_Candidate__c = articleMetadataList[0].Archive_Candidate__c;
            articleMetadata.Archive_Comments__c = articleMetadataList[0].Archive_Comments__c;    
        }
        system.debug('---------------------------------------------------------------------2');
    }
    
    public PageReference quickSave(){        
        if(articleMetadataList.isEmpty()){
            Article_Metadata__c articleData = new Article_Metadata__c();
            articleData.Archive_Candidate__c = articleMetadata.Archive_Candidate__c;
            articleData.Archive_Comments__c = articleMetadata.Archive_Comments__c;
            articleData.Article_Title__c = Title;
            articleData.Article_Number__c = articleNumber;
            insert articleData;    
        }else{
            articleMetadataList[0].Archive_Candidate__c = articleMetadata.Archive_Candidate__c;
            articleMetadataList[0].Archive_Comments__c = articleMetadata.Archive_Comments__c;
            update articleMetadataList;
        }
        return new pageReference('/'+articleId);                
    }
    
    public PageReference cancel(){
        return new pageReference('/'+articleId);
    }
}