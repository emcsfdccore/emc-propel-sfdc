/*+===========================================================================================================
|   Date         Developer    WR                Comments
|   -----        ---------    --                --------
| 20-Jan-2015 Akash Rastogi  1528     Commented few lines of Code as per the requirement
| 19-Feb-2015 Vinod Jetti    #1649    created Hub_Info__c objects data instead of Account 'Emc_classification__c' field 
+============================================================================================================*/
@isTest
        public class SC_CaseCommController_TC
        {
          public Static List<case> lstCase = new List<case>();
          public Static List<Account> lstAccount = new List<Account>();
         // public Static List<Asset__c> lstAsset = new List<Asset__c>();
          public Static List<Contact> contLst = new List<Contact>();
          public Static List<User> userLst = [Select Id,email,Country__c,UserRoleId  from user where Profile_Name__c like 'Presales%' and  UserRoleId!=null and isActive = true Limit 2];
          User userObj= [Select Email, LastName, Phone,Contact_Account_Name__c from User where id=:Userinfo.getuserid()];
      
    //Method to create test Account records     
          public void createAccount(){
              //#1649 - start
              Hub_Info__c objHubInfo = new Hub_Info__c();
              objHubInfo.Global_DUNS_Entity__c = 'global duns entity';
              objHubInfo.EMC_Classification__c='emc classification';
              objHubInfo.Golden_Site_Identifier__c = 305014;
              insert objHubInfo;
              //#1649 - End
              Account acctObj = new Account();  
              acctObj.Name = 'Testing';   
              acctObj.Synergy_Account_Number__c = '123456';   
              acctObj.Customer_Profiled_Account__c=true;      
              acctObj.Customer_Service_Requirements__c='test';  
              acctObj.Hub_Info__c = objHubInfo.ID;            
              //acctObj.EMC_Classification__c='emc classification';      
              acctObj.Party_Number__c='123';  
              //acctObj.Global_DUNS_Entity__c ='global duns entity';
              acctObj.Core_Quota_Rep__c =userLst[0].Id;
              lstAccount.add(acctObj);
              
              if(!lstAccount.isEmpty()){
              insert lstAccount;
              }

          }

    //Method to create test Asset records 
        /*  public void createAsset(){ 
            RecordType rectype = [Select r.Id, r.name from RecordType r where r.name =: 'EMC Install' limit 1];
            for(Integer i=0; i<2;i++){
            Asset__c asset_Obj = new Asset__c();
            asset_Obj.Customer_Name__c = lstAccount[0].id;   
            asset_Obj.Disposition_Status__c ='Installed';  
            asset_Obj.Serial_Number__c ='SA12345'+ i ; 
            asset_Obj.Install_Base_Status__c ='Competitor Maintained'; 
            asset_Obj.Product_Family__c ='SYMMETRIX'; 
            asset_Obj.Model__c ='DMX2000-3'; 
            asset_Obj.Product_Name_Vendor__c ='EMC'; 
            asset_Obj.RecordTypeId = rectype.Id;
             

            lstAsset.add(asset_Obj);
            }
            if(!lstAsset.isEmpty()){
            insert lstAsset;
            }
          }*/

    //Method to create test Contact records 
          public void createContact(){
            List<RecordType> recList = [ Select Id, Name from RecordType where sObjectType ='Contact' AND Name ='EMC Internal Contact' limit 1];  
                for(Integer c=0;c<2;c++){
                    Contact contObj = new Contact();
                    contObj.LastName = 'Tester';
                    contObj.Phone = '999999999';
                    contObj.Email = 'test.'+c+'@emc.com';
                    contObj.RecordTypeId = recList[0].id;
                    contObj.AccountId = lstAccount[0].id;   
                    contObj.PowerlinkID_Badge__c = '78987'+c;
                    contObj.Active__c=true;
                    contLst.add(contObj);
                }
                
                    if(!contLst.isEmpty()){
                    insert contLst;
                    }
           }

           
    //Method to create test Case records   
              public void createCase( Boolean leadRequired){
                Map<String , Id> recid = new Map<String , Id>(); 
                List<RecordType> recList = [Select id,name from recordtype where name in ('Global Revenue Operations','Accounts Payable','TRACK - Customer Master') ];
                
              
                for(Integer i=0;i<2;i++){       
                    Case caseObj_Insert = new Case();            
                    caseObj_Insert.Description = 'TEST1';  
                    caseObj_Insert.Subject = 'TEST1';     
                    caseObj_Insert.Origin = 'System';     
                    caseObj_Insert.Type = 'GRO Order Inquiries';  
                    caseObj_Insert.Quote_Number__c='1023';
                    //caseObj_Insert.Record_Type_Hidden__c = recList[i].name ;  
                    caseObj_Insert.RecordTypeId = recList[i].id;
                    caseObj_Insert.Customer_Account_Name__c = null;   
                    //caseObj_Insert.Customer_Asset_Name__c = lstAsset[0].id;   
                    //WR#1528 Changes
                    //caseObj_Insert.Global_Ultimate__c = lstAccount[0].id; 
                    //caseObj_Insert.Business_Unit_Ultimate__c = lstAccount[0].id; 
                    caseObj_Insert.contact_Email1__c ='abc.d@emc.com';
                    caseObj_Insert.Bill_To_Country__c='India';
                    caseObj_Insert.Country__c='United States';
                    if(leadRequired== true)
                    {
                    
                    caseObj_Insert.Field_Rep__c = contLst[0].Id;
                    caseObj_Insert.Party_Number__c = lstAccount[0].Party_Number__c;
                    //WR#1528 Changes
                    //caseObj_Insert.PWID_Manager_Name__c = contLst[i].PowerlinkID_Badge__c;
                    //caseObj_Insert.PWID_Resource_Name__c=contLst[i].PowerlinkID_Badge__c;
                    //caseObj_Insert.EN_Resource_to_Mirror__c =  contLst[1].PowerlinkID_Badge__c;
                    }
                    lstCase.add(caseObj_Insert);
                 }  
  
                    if(!lstCase.isEmpty()){
                    insert lstCase;
                    }
                   
              }
              
              
     public static User createUser(Id contactId){
       
        User tempUsr = new User(
            Username='test1234'+Math.random()+'@emc.com.test',
            TimeZoneSidKey='America/New_York',
            ProfileId= '00e70000001Fbqw',
            LocaleSidKey='en_US',
            FirstName='Direct',
            LastName='Rep',
            email='john@acme.com',
            Alias='test',
            EmailEncodingKey='ISO-8859-1',
            LanguageLocaleKey='en_US',
            Forecast_Group__c='Direct',
            BU__c='NA',
            Employee_Number__c='9323782000',
            IsActive=true,
            ContactId = contactId
        );
        insert tempUsr;
        return tempUsr;
    }
              
              
               public static testMethod void scCaseController(){
                   
                User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
               
            System.runAs(insertUser)        // Creating test data for custom settings 
            {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.VCEStaticCSData();
            }
            
                SC_CaseCommController_TC testObj = new SC_CaseCommController_TC();
                SC_CaseCommController controllObj1 = new SC_CaseCommController();
                testObj.createAccount();
                testObj.createContact();
                test.starttest();
                testobj.createcase(true);
                user contactUser = createUser(contLst[0].Id);
               
                List<Attachment> lstAtt = new List<attachment>();
                String str = 'my string for attachment';
                Blob blobVal = Blob.ValueOf(str);   
                Attachment attObj =  new Attachment();
                attObj.ParentId=lstCase[0].Id;           
                attObj.Body= blobVal;
                attObj.Name='File1';            
                attObj.ContentType = 'doc,txt,jpeg';           
                lstAtt.add(attObj);
                List<SC13_RecordType_Type__c> lstcustomSettingObj = new List<SC13_RecordType_Type__c>();
                for(Integer cusSet =0 ; cusSet<=3 ; cusSet++){
                    SC13_RecordType_Type__c customSettingObj = new SC13_RecordType_Type__c();
                    
                    if(cusSet==0){
                        customSettingObj.isStandardType__c = true;
                        customSettingObj.SC_Record_Type__c = 'Global Revenue Operations';
                        customSettingObj.Name = 'Global Revenue Operations';
                    }
                    else if(cusSet==1){
                        customSettingObj.isStandardType__c = true;
                        customSettingObj.SC_Record_Type__c = 'Accounts Payable';
                        customSettingObj.Name = 'Accounts Payable';
                    }
                    else if(cusSet==2){
                        customSettingObj.isStandardType__c = true;
                        customSettingObj.SC_Record_Type__c = 'Credit Collections';
                        customSettingObj.Name = 'Credit & Collections';
                    }
                    else 
                    {
                        customSettingObj.isStandardType__c = false;
                        customSettingObj.SC_Record_Type__c = 'Maintenance Contract Operations';
                        customSettingObj.Name = 'Maintenance Contract Operations';
                        
                    }
                    lstcustomSettingObj.add(customSettingObj);
            }
        
            
            lstCase[0].Additonal_Notification_Email_1__c ='abcd.efgh@emc.com';
            lstCase[1].contact_Email1__c ='abcd@emc.com';
            
            for(Integer i =0 ; i<lstCase.size();i++){
            
           
            ApexPages.Currentpage().getParameters().put('recordType','Global Revenue Operations');
            ApexPages.Currentpage().getParameters().put('caseType','GRO Order Inquiries');
            ApexPages.Currentpage().getParameters().put('langForPage','en_US');
            
          
            
            ApexPages.StandardController controller = new ApexPages.StandardController(lstCase[i]); 
            SC_CaseCommController controllObj = new SC_CaseCommController(controller);
            
            
            
            
            ApexPages.Currentpage().getParameters().put('id',lstCase[i].Id);
            ApexPages.Currentpage().getParameters().put('CaseNumber',lstCase[i].CaseNumber);
            
            controllObj.recordType ='Global Revenue Operations';
            controllObj.ischanged();
            controllObj.addMore();
            controllObj.createInstance();    
            controllObj.newAttachments = lstAtt;
            controllObj.saveCase();
            controllObj.renderFields(); 
            controllObj.getApplicationID();  
            string lang= controllObj.getLang();
            controllObj.getMyUserLanguage();
            controllObj.ischanged();
           
            
            
            controllObj.displayDescription();
            controllObj.addMore();
            controllObj.renderFields();
            controllObj.newAttachments = lstAtt;
            controllObj.caseSaved = true;
            controllObj.caseAttachments = lstAtt;
            controllObj.saveAttachment();
            controllObj.caseSaved = true;
            controllObj.saveCase();
            controllObj.trackFields();
            controllObj.backtoCase();
            controllObj.saveCaseOld();
            
            List<Attachment> lstAtt1 = new List<attachment>();
            String str1 = 'my string for attachment1';
            Blob blobVal1 = Blob.ValueOf(str1);   
            Attachment attObj1=  new Attachment();
            attObj1.Body= blobVal1;
            attObj1.Name='File12';            
            attObj1.ContentType = 'doc,txt,jpeg';           
            lstAtt1.add(attObj1);
            controllObj.newAttachments = lstAtt1;
            controllObj.caseSaved = true;
            controllObj.caseAttachments = lstAtt1;
            controllObj.saveAttachment();
            
            }
             
               Test.stopTest();
               
           }
              
              }