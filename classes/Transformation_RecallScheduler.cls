//**************************************************************************************************************
// Name          : Transformation_RecallScheduler.cls
// Description    : Class is to schedule the Pricing Trasnformation_RecallBatch which needs to run every hour
// Created By      : Jaypal Nimesh (EMC)
// Created Date  : 07/05/2013

// ************************Version Updates**********************************************************************
// Modified By: CI October
// WR No: 1072
// Modified Date 17/Sep/2014
// Description: Added "Holding" as a filter in the query against AsyncApexJob
// *************************************************************************************************************

global with sharing class Transformation_RecallScheduler implements Schedulable{
    
    global void execute(SchedulableContext SC){
        
        //Get the Id of the batch Apex class        
        Id batchClassId = [Select Id, Name From ApexClass where Name = 'Trasnformation_RecallBatch'].Id;
        
        //List for Pending Apex jobs executing
        //1072 adding 'Holding' in filter
        List<AsyncApexJob> pendingApexJob = [Select Id, Status, ApexClass.Name From AsyncApexJob where ApexClassId =: batchClassId AND (Status = 'Queued' OR Status = 'Processing' OR Status = 'Preparing' OR Status ='Holding')];
        
        //Checks if there is no job currently running
        if(pendingApexJob.isEmpty()) {

            //Creating instance of batch class and assigning the query
            Trasnformation_RecallBatch transRecallBatch = new Trasnformation_RecallBatch(); 
            if (!Test.isRunningTest()){       
            transRecallBatch.query = 'Select Name, Owner.Name, Recall_Comments__c from Pricing_Requests__c where Recalled_Flag__c = true';
            }
            else{
            
            transRecallBatch.query = 'Select Name, Owner.Name, Recall_Comments__c from Pricing_Requests__c where Recalled_Flag__c = true limit 5';
            
            }
            
            //Initiating Batch job
            Id batchProcessId = Database.executebatch(transRecallBatch, 200);                    
        }
    }
    
    //Method to schedule the batch process for every 1 hour
    public static void runScheduler(){
        
        //Defining CRON Expression
        String CRON_EXP;
        
        //This Scheduler runs every 1 hour
        CRON_EXP = '0 0 */1 * * ?';
        
        system.schedule('Transformation Recall', CRON_EXP, new Transformation_RecallScheduler());  
 
    }
}