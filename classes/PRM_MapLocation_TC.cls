@isTest
public class PRM_MapLocation_TC{

Public static testmethod void Method1(){

Set<Id> locationIds=new set<Id>();

Partner_Location__c part1=new Partner_Location__C();
part1.name='test name';
part1.city__c='test city1';
part1.Postal_code__c='test code1';
part1.State_Province__c='test state1';
part1.street__c='test street1';
part1.Latitude__c=90;
part1.Longitude__c=60;

Partner_Location__c part2=new Partner_Location__c();
part2.name='test name1';
part2.city__c='test city';
part2.Postal_code__c='test code';
part2.State_Province__c='test state';
part2.street__c='test street';
part2.Latitude__c=80;
part2.Longitude__c=45;



List<Partner_Location__c> lst=new List<Partner_Location__c>();

lst.add(part1);
lst.add(part2);
Insert lst;

locationIds.add(lst[0].id);
locationIds.add(lst[1].id);

String add='test address';
String jsonresponse='test response';

StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
mock.setStaticResource('NameOfStaticResourceContainingResponseBodyString');
mock.setStatusCode(200); // Or other appropriate HTTP status code
mock.setHeader('Content-Type', 'application/json'); // Or other appropriate MIME type like application/xml
Test.setMock(HttpCalloutMock.class, mock);

Test.StartTest();
PRM_MapLocation obj1=new PRM_MapLocation();
PRM_MapLocation.getLatitudenLongitude(locationIds);
try{
PRM_MapLocation.getLocationResponse(add);
}catch(exception e){
System.debug('exception'+e);
}
PRM_MapLocation.getLatLong(jsonresponse);
Test.StopTest();

}

}