/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        ===========       

 | 14-Jan-2014 Shalabh Sharma : Added method createLead to create test Lead records to cover MCO requirement to populate Lead on Case.
 | 20 Jan 2014 Bisna VP      CI :196  Updated to cover method backtoCase 
 | 28-May-2014 Sayan Choudhury CI:562 Updated to cover the logic for CI WR 562
 | 13-Oct-2014 Bisna V P    CI:1399   Remove code referencing unused fields
 | 18-Feb-2015 Vinod Jetti   CI:1649  created Hub_Info__c objects record for test

=======================================================================================*/
@isTest(seealldata = true) 
    public class SC_CaseOperation_TC
    {
      public Static List<case> lstCase = new List<case>();
      public Static List<Account> lstAccount = new List<Account>();
      public Static List<Asset__c> lstAsset = new List<Asset__c>();
      public Static List<Contact> contLst = new List<Contact>();
      public Static List<User> userLst = [Select Id,email,Country__c,UserRoleId  from user where Profile_Name__c like 'Presales%' and  UserRoleId!=null and isActive = true Limit 2];
      public Static List<Lead> lstLead = new List<Lead>();

//Method to create test Account records     
      public void createAccount(){
          //#1649 - Start
          Hub_Info__c objHubInfo = new Hub_Info__c();
          objHubInfo.EMC_Classification__c = 'emc classification';
          objHubInfo.Global_DUNS_Entity__c ='global duns entity';
          objHubInfo.Golden_Site_Identifier__c = 623548;
          Insert objHubInfo;
          //#1649 - End
         Account acctObj = new Account();  
          acctObj.Name = 'Testing';   
          acctObj.Synergy_Account_Number__c = '123456';   
          acctObj.Customer_Profiled_Account__c=true;      
          acctObj.Customer_Service_Requirements__c='test';  
          acctObj.Hub_Info__c = objHubInfo.Id;
          //acctObj.EMC_Classification__c='emc classification';      
          acctObj.Party_Number__c='123';  
          //acctObj.Global_DUNS_Entity__c ='global duns entity';
          acctObj.Core_Quota_Rep__c =userLst[0].Id;
          lstAccount.add(acctObj);
          
          if(!lstAccount.isEmpty()){
          insert lstAccount;
          }

      }

//Method to create test Asset records 
      public void createAsset(){ 
        RecordType rectype = [Select r.Id, r.name from RecordType r where r.name =: 'EMC Install' limit 1];
        for(Integer i=0; i<2;i++){
        Asset__c asset_Obj = new Asset__c();
        asset_Obj.Customer_Name__c = lstAccount[0].id;   
        asset_Obj.Disposition_Status__c ='Installed';  
        asset_Obj.Serial_Number__c ='SA12345'+ i ; 
        asset_Obj.Install_Base_Status__c ='Competitor Maintained'; 
        asset_Obj.Product_Family__c ='SYMMETRIX'; 
        asset_Obj.Model__c ='DMX2000-3'; 
        asset_Obj.Product_Name_Vendor__c ='EMC'; 
        asset_Obj.RecordTypeId = rectype.Id;
         

        lstAsset.add(asset_Obj);
        }
        if(!lstAsset.isEmpty()){
        insert lstAsset;
        }
      }

//Method to create test Contact records 
      public void createContact(){
        List<RecordType> recList = [ Select Id, Name from RecordType where sObjectType ='Contact' AND Name ='EMC Internal Contact' limit 1];  
            for(Integer c=0;c<2;c++){
                Contact contObj = new Contact();
                contObj.LastName = 'Tester';
                contObj.Phone = '999999999';
                contObj.Email = 'test.'+c+'@emc.com';
                contObj.RecordTypeId = recList[0].id;
                contObj.AccountId = lstAccount[0].id;   
                contObj.PowerlinkID_Badge__c = '78987'+c;
                contObj.Active__c=true;
                contLst.add(contObj);
            }
            
            
                    
            //contLst[0].PowerlinkID_Badge__c = 'aeiou';
            
                if(!contLst.isEmpty()){
                insert contLst;
                }
       }

//Method to create test Lead records 
        public void createLead(){
            List<RecordType> recList = [ Select Id, Name from RecordType where sObjectType ='Lead' AND Name ='Tech Refresh Lead Record Type' limit 1];
            for(Integer i=0;i<2;i++){
                Lead leadObj = new Lead();
                leadObj.Related_Account__c = lstAccount[0].id;
                leadObj.Status = 'New';
                leadObj.Sales_Force__c = 'EMC';
                leadObj.lastname = 'Test Lead';
                leadObj.Company = 'Test';
                leadObj.RecordTypeId = recList[0].id;

                lstLead.add(leadObj);
            }
            if(!lstLead.isEmpty()){
            insert lstLead;
            }
          }
    
//Method to create test Case records   
          public void createCase( Boolean leadRequired){
            Map<String , Id> recid = new Map<String , Id>(); 
            List<RecordType> recList = [Select id,name from recordtype where name in ( 'Global Revenue Operations','Maintenance Contract Operations','TRACK - Customer Master')];
            
            
            /*List<Contact> contLst11;
            
                try{
                contLst11 = [SELECT Id,PowerlinkID_Badge__c FROM Contact  
                    where Contact.recordtype.name = 'EMC Internal Contact'  
                    and Active__c = true and PowerlinkID_Badge__c!=null limit 1]
                    ;                                   
                
                }
                
                Catch(Exception e){
                contLst11 = contLst;
                }
            */          
            
            for(Integer i=0;i<2;i++){       
                Case caseObj_Insert = new Case();            
                caseObj_Insert.Description = 'TEST1';  
                caseObj_Insert.Subject = 'TEST1';     
                caseObj_Insert.Origin = 'System';     
                caseObj_Insert.Type = 'Contract Update Request';  
                caseObj_Insert.Record_Type_Hidden__c = recList[i].name ;              
                caseObj_Insert.RecordTypeId = recList[i].id;
                caseObj_Insert.Customer_Account_Name__c = null;   
                caseObj_Insert.Customer_Asset_Name__c = lstAsset[0].id; 
                //1399 commenting the following             
                //caseObj_Insert.Global_Ultimate__c = lstAccount[0].id;                 
                //caseObj_Insert.Business_Unit_Ultimate__c = lstAccount[0].id; 
                caseObj_Insert.contact_Email1__c ='abc.d@emc.com';
                if(leadRequired== true)
                caseObj_Insert.Lead_Name__c = lstLead[0].Id;
                caseObj_Insert.Field_Rep__c = contLst[0].Id;
                caseObj_Insert.Party_Number__c = lstAccount[0].Party_Number__c;
                
                /*caseObj_Insert.PWID_Manager_Name__c = contLst11[0].PowerlinkID_Badge__c;
                caseObj_Insert.PWID_Resource_Name__c=contLst11[0].PowerlinkID_Badge__c;
                caseObj_Insert.EN_Resource_to_Mirror__c =  contLst11[0].PowerlinkID_Badge__c;       
                */
                //commented for 1399
                /*caseObj_Insert.PWID_Manager_Name__c = contLst[i].PowerlinkID_Badge__c;
                caseObj_Insert.PWID_Resource_Name__c=contLst[i].PowerlinkID_Badge__c;
                caseObj_Insert.EN_Resource_to_Mirror__c =  contLst[1].PowerlinkID_Badge__c;
                */
                if(i==0){
                caseObj_Insert.take_ownership__c=false;
                }
                if(i==1){
                caseObj_Insert.take_ownership__c=true;
                }
                
                lstCase.add(caseObj_Insert);
             }  

                
                
                if(!lstCase.isEmpty()){
                insert lstCase;
                }
                //Create Intenal Case Comment
                Presales_Internal_Case_Comment__c  comment = new Presales_Internal_Case_Comment__c ();
                comment.Number_of_Transactions__c = 2;
                comment.Case_Comment__c = 'TEST COMMENT';
                comment.Case__c = lstCase[0].Id;
                insert comment;
          }
//Method to test SC_CaseOperation      
       public static testMethod void  scCaseOperation(){
           try{
           
            //User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
            System.RunAs(new User(Id = Userinfo.getUserId()))
            {
            PRM_VPP_JobDataHelper.createVPPCustomSettingData();   
            } 
            List<Case> caseLst = new List<Case>();
            List<Id> cntIdLst = new List<Id>();
            SC_CaseOperation_TC testObj = new SC_CaseOperation_TC();
            testObj.createAccount();
            testObj.createContact();
            testObj.createLead();
            testObj.createAsset();
            Test.startTest();
            testObj.createCase(true);
            Test.stopTest();
           //Update Case value 
           for( Case caseObj : lstCase){
               caseObj.Customer_Asset_Name__c = lstAsset[1].id;
               caseObj.Customer_Account_Name__c = lstAccount[0].id; 
               //1399 commenting the following
               //caseObj.Global_Ultimate__c = null;             
               //caseObj.Business_Unit_Ultimate__c = null;
               caseObj.OwnerId = userLst[1].Id;
               caseObj.contactid = contLst[0].Id;
               caseObj.Lead_Name__c = null;
               caseObj.Lead_Number__c = null;
               caseObj.Party_Number__c = null;
               cntIdLst.add(contLst[0].Id);         
               caseLst.add(caseObj);
           }
           update caseLst;
            SC_CaseOperation objCase = new SC_CaseOperation();
           for(Case caseObj : lstCase){
               caseObj.Party_Number__c = null;
               //caseObj.Customer_Account_Name__c = null;
               caseLst.add(caseObj);
           }
           objCase.populateCustomerAccountName(caseLst);
           //update caseLst;
           // Creating Additional Asset record related to the test case record
           Additional_Assets__c addObj =new Additional_Assets__c();
           addObj.Case__c = caseLst[0].id;
           addObj.Customer_Asset__c = lstAsset[0].id;
           insert addObj;

           Presales_Account_Split splitClass = new Presales_Account_Split();
           splitClass.split(caseLst , cntIdLst);
           }

           catch(Exception ex){
               System.debug('Inside Exception 1');
            Case casObj = new Case();
            casObj = [Select id from Case where status = 'Open' limit 1];
            RecordType rectype1 = [Select r.Id, r.name from RecordType r where r.name =: 'EMC Install' limit 1];
            Asset__c asstObj = [Select id from Asset__c Where RecordTypeId =: rectype1.Id  limit 1];

            Additional_Assets__c addObj =new Additional_Assets__c();
            addObj.Case__c = casObj.id;
            addObj.Customer_Asset__c = asstObj.id;
            insert addObj;
           }
       }

//Method to test SC_CaseController 
       public static testMethod void scCaseController(){
           try{        
            //User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
            System.RunAs(new User(Id = Userinfo.getUserId()))
            {
                PRM_VPP_JobDataHelper.createVPPCustomSettingData(); 

            }       
            SC_CaseOperation_TC testObj = new SC_CaseOperation_TC();
            SC_CaseFormController controllObj1 = new SC_CaseFormController();
            testObj.createAccount();
            testObj.createContact();
            testObj.createAsset();
            testobj.createcase(false);
            List<Attachment> lstAtt = new List<attachment>();
            String str = 'my string for attachment';
            Blob blobVal = Blob.ValueOf(str);   
            Attachment attObj =  new Attachment();
            attObj.ParentId=lstCase[0].Id;           
            attObj.Body= blobVal;
            attObj.Name='File1';            
            attObj.ContentType = 'doc,txt,jpeg';           
            lstAtt.add(attObj);
            List<SC13_RecordType_Type__c> lstcustomSettingObj = new List<SC13_RecordType_Type__c>();
            for(Integer cusSet =0 ; cusSet<=3 ; cusSet++){
                SC13_RecordType_Type__c customSettingObj = new SC13_RecordType_Type__c();
                customSettingObj.isStandardType__c = true;
                if(cusSet==0){
                    customSettingObj.SC_Record_Type__c = 'Global Revenue Operations';
                    customSettingObj.Name = 'Global Revenue Operations';
                }
                else if(cusSet==1){
                    customSettingObj.SC_Record_Type__c = 'Accounts Payable';
                    customSettingObj.Name = 'Accounts Payable';
                }
                else if(cusSet==2){
                    customSettingObj.SC_Record_Type__c = 'Credit Collections';
                    customSettingObj.Name = 'Credit & Collections';
                }
                else 
                {
                    customSettingObj.SC_Record_Type__c = 'Maintenance Contract Operations';
                    customSettingObj.Name = 'Maintenance Contract Operations';
                }
                lstcustomSettingObj.add(customSettingObj);
        }
    /* Map<String,SC13_RecordType_Type__c> rtTypeMapSetting = SC13_RecordType_Type__c.getall();

     if(rtTypeMapSetting.isEmpty()){
        try{
            insert lstcustomSettingObj;
        }
        catch(Exception e){
            system.debug('e--->'+e);
        }

     }*/    
        lstCase[0].Additonal_Notification_Email_1__c ='abcd.efgh@emc.com';
        lstCase[1].contact_Email1__c ='abcd@emc.com';
                    Test.startTest();

        for(Integer i =0 ; i<lstCase.size();i++){
        
        
        ApexPages.Currentpage().getParameters().put('recordType','Global Revenue Operations');
        ApexPages.StandardController controller = new ApexPages.StandardController(lstCase[i]); 
        SC_CaseFormController controllObj = new SC_CaseFormController(controller);
        
        
        
        
        ApexPages.Currentpage().getParameters().put('id',lstCase[i].Id);
        ApexPages.Currentpage().getParameters().put('CaseNumber',lstCase[i].CaseNumber);
        
        controllObj.recordType ='Maintenance Contract Operations';
        controllObj.addMore();
        controllObj.createInstance();    
        controllObj.newAttachments = lstAtt;
        controllObj.saveCase();
        controllObj.renderFields(); 
        controllObj.getApplicationID();  
        
        
        controllObj.displayDescription();
        controllObj.addMore();
       
        controllObj.renderFields();
        controllObj.newAttachments = lstAtt;
        controllObj.caseSaved = true;
        controllObj.caseAttachments = lstAtt;
        controllObj.saveAttachment();
        controllObj.caseSaved = true;
        controllObj.saveCase();
        controllObj.trackFields();
        controllObj.backtoCase();
        controllObj.saveCaseOld();
        
        lstAtt[0].ParentId=null;
        controllObj.caseAttachments = lstAtt;
        controllObj.saveAttachment();
                     Test.stopTest();

        
        }
        }
           catch(Exception ex){
               System.debug('Inside Exception 2'+ex);
           }
       }

 //Method to test SC_CaseSearchController 
       public static testMethod void scCaseSearchController(){
           try
           {           
            //User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
            System.RunAs(new User(Id = Userinfo.getUserId()))
            {
                PRM_VPP_JobDataHelper.createVPPCustomSettingData();   
            } 
                   Test.startTest();
 
       SC_CaseOperation_TC testObj = new SC_CaseOperation_TC();
        testObj.createAccount();
        testObj.createContact();
        testObj.createAsset();
        testObj.createCase(false);
       // List<Case> lstCases = new List<case>();
       // lstCases = [Select Id, Contact_Email1__c,casenumber from case where id in: lstCase];
        for(Integer i =0 ; i<lstCase.size();i++){
            ApexPages.StandardController controller = new ApexPages.StandardController(lstCase[i]); 
            SC_CaseSearchController controllObj = new SC_CaseSearchController(controller);
            controllObj.caseObj.Contact_Email1__c='';
            controllObj.searchCase();
            controllObj.getListCase();
            controllObj.gotoMain();
         }
                     Test.stopTest();

       }
           catch(Exception ex){
               System.debug('Inside Exception 3');
           } 
    }
    }