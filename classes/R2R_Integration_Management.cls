/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  
 |  15.04.2013      Anirudh                    Added code for R2R integration.                                              
+=====================================================================================================================*/
public class R2R_Integration_Management{
 public static void mapAssetsWithAccount(List<Asset__c> lstAsset,boolean isInsert){
     map<string,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getall();
     List<Asset__c> lstNotMappedAssets = new list<Asset__c>();
     Map<String,Integration_error_log__c> mapIntegrationErrorLog= new Map<String,Integration_error_log__c>();
     Map<String,List<Asset__c>> mapAssetWithPartyNumber= new Map<String,List<Asset__c>>();
     System.Debug('mapAssetWithPartyNumber-->' +mapAssetWithPartyNumber);
     for (Asset__c assetObj :lstAsset){
          List<Asset__c> lstAssetForPartyNumber = mapAssetWithPartyNumber.get(assetObj.Party_Number__c);
          if(lstAssetForPartyNumber ==null){
             lstAssetForPartyNumber = new List<Asset__c>();      
          }
          lstAssetForPartyNumber.add(assetObj );
          mapAssetWithPartyNumber.put(assetObj.Party_Number__c,lstAssetForPartyNumber);
      }   
     
     List<Account> lstAccount=[Select Id,Party_Number__c,name from Account where Party_Number__c in :mapAssetWithPartyNumber.keyset()];
     map<string,Account> mapAccoountWithPartyNumber = new map<string,Account>();
   
     for(Account accObj : lstAccount){
         mapAccoountWithPartyNumber.put(accObj.Party_Number__c,accObj);
     }
       System.Debug('mapAccoountWithPartyNumber--->' +mapAccoountWithPartyNumber );
     for(string partyNumber :mapAssetWithPartyNumber.keyset()){
         for(Asset__c assetObj :mapAssetWithPartyNumber.get(partyNumber)){
             System.Debug('mapAccoountWithPartyNumber--->' +mapAccoountWithPartyNumber.get(partyNumber) );              
                if(mapAccoountWithPartyNumber.get(partyNumber)!=null){
                    assetObj.Customer_Name__c = mapAccoountWithPartyNumber.get(partyNumber).Id;
                 if(assetObj.RecordTypeId == null){
                    assetObj.RecordTypeId = DataValueMap.get('EMC Install Record Type').DataValue__c;
                    
                 }
                 if(isInsert){  
                    assetObj.Name = assetObj.Serial_Number__c;
                 }  
             //Id;
             }            
            else if(!mapAccoountWithPartyNumber.containskey(partyNumber) && !mapIntegrationErrorLog.containskey(partyNumber)){                  
                    Integration_error_log__c IntegrationError = new Integration_error_log__c(); 
                    IntegrationError.Integration_Name__c = 'Sam Asset Inetgration Error';
                    IntegrationError.Error_Message__c = 'SAM assets are not getting Mapped due to invalid Party Number i.e.' + partyNumber; 
                    mapIntegrationErrorLog.put(partyNumber,IntegrationError);  
              }      
               System.Debug('1---->' +assetObj);        
            }
     
            
             
     }   
     if(!mapIntegrationErrorLog.isempty()){
        Database.Insert(mapIntegrationErrorLog.values(),false);
     } 
     
   }
 
    
}