/*========================================================================================================================+
 |  HISTORY                                                               
 |                                                                         
 |  DATE            DEVELOPER      WR/Req                 DESCRIPTION                              
 |  ====            =========      ======                 =========== 
 |  11-02-2013    Anand Sharma  System Administration   This class is used as a Utility class for System Admin App 
 |  17/09/2014    CI          1072  Added "Holding" as a filter in the query against AsyncApexJob 
 +=========================================================================================================================*/

global class SystemAdministration_Utility{
    public Boolean isJobRunning {get; set;}
    public String inputstr {get; set;}
    
    public SystemAdministration_Utility(){
        Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
        CustomSettingDataValueMap__c  seqeunceId = DataValueMap.get('SystemAdminJobStatus');
        if(seqeunceId != null && seqeunceId.DataValue__c != null && seqeunceId.DataValue__c == 'true'){
            isJobRunning = true;         
        }else{
            isJobRunning = false;
        }
    }
    
    /* @Method <This method is used to validate System Admin Privilege Request>  
    @param <It is taking User Id as parameters> 
    @return String of validated Result>   
    @throws exception - <No Exception>  
    */
    webservice static string ValidateSystemAdminRequest (Id UserId) {
        string isSysAdminRequestEnabled = 'No';
        System.Debug('UserId--->' +UserId); 
        List<System_Admin_Users__c> sysAdminUser = new List<System_Admin_Users__c>(); 
        try{ 
            //Anand sysAdminUser= [Select Id,User_Name__c,  Is_a_System_Administrator__c,End_Date__c from System_Admin_User__c where User_Name__c =: UserId and Can_Request_Initiated__c = 'Yes'];
            sysAdminUser= [Select Id,User_Name__c,End_Date_Time__c, Is_a_Temp_System_Admin__c from System_Admin_Users__c where User_Name__c =: UserId and Active__c = true and End_Date_Time__c >: System.now()];
        }
        catch (exception e){ 
        }
        System.Debug('sysAdminUser--->' +sysAdminUser);
        if(sysAdminUser != null  && sysAdminUser.size() >0){ 
            if(sysAdminUser != null && sysAdminUser.get(0).Is_a_Temp_System_Admin__c ==true){
                isSysAdminRequestEnabled = System.Label.SystemAdmin_AlreadySystemAdmin;
                return isSysAdminRequestEnabled;
            }           
        }else{
            isSysAdminRequestEnabled = System.Label.SystemAdmin_NoActiveUser;
            return isSysAdminRequestEnabled; 
        } 
        try{
            List<System_Admin_Request_Logs__c> lstAdminLog = [Select Id From System_Admin_Request_Logs__c where User_Name__c =: UserId and Status__c ='In-Progress'];
            if(lstAdminLog != null && lstAdminLog.size() > 0){            
                isSysAdminRequestEnabled = System.Label.SystemAdmin_AlreadyRequested;
                return isSysAdminRequestEnabled;
            }
        }catch (exception e){
        }
        return isSysAdminRequestEnabled;
    }
    
    @future
    public static void grantSysAdminAccess(Set<Id> userId){
        SystemAdministration_Utility.grantAccessToUsers(userId);
    }
    
    public static void updateSystemAdminUserAndReqLog(Set<Id> setUserIds){
        if(setUserIds != null && setUserIds.size() >0){
            List<System_Admin_Request_Logs__c> lstSystemAdminReqLogs =[Select Id, Status__c,User_Name__c from System_Admin_Request_Logs__c where Status__c ='In-Progress' and User_Name__c in:setUserIds];
            if(lstSystemAdminReqLogs != null && lstSystemAdminReqLogs.size() >0){
                for(System_Admin_Request_Logs__c objSysAdmReqLog : lstSystemAdminReqLogs) {
                    objSysAdmReqLog.Status__c ='Completed';
                }
                update lstSystemAdminReqLogs;
            }
            List<System_Admin_Users__c> lstSystemAdminUsers =[Select Id, User_Name__c,Is_a_Temp_System_Admin__c from System_Admin_Users__c  where User_Name__c in:setUserIds];
            if(lstSystemAdminReqLogs != null && lstSystemAdminUsers.size() >0){
                for(System_Admin_Users__c objSysAdmUser : lstSystemAdminUsers) {
                    objSysAdmUser.Is_a_Temp_System_Admin__c =true;
                }
                update lstSystemAdminUsers;
            }
        }      
    }  
   
    public static void populateValuesOnLog(List<System_Admin_Request_Logs__c> lstSysAdmLog){
        Integer intBatchIb = 72;
        System_Admin_Users__c objAdminUser;
        Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
        CustomSettingDataValueMap__c  seqeunceId = DataValueMap.get('SystemAdminNoOfHours');
        if(seqeunceId != null && seqeunceId.DataValue__c != null && seqeunceId.DataValue__c != ''){
            intBatchIb = Integer.valueOf(seqeunceId.DataValue__c);           
        }          
        for(System_Admin_Request_Logs__c objSysAdmreqLog : lstSysAdmLog) {
            // Record and Attachments to be inserted            
            objSysAdmreqLog.End_Date__c = System.now().addHours(intBatchIb); 
            objSysAdmreqLog.Start_Date__c =System.now(); 
            //record.Reason__c ='Test';
            System.Debug('UserInfo.getUserType()-->' + UserInfo.getUserType());
            if(UserInfo.getUserType() =='Guest'){
                objSysAdmreqLog.User_Name__c =objSysAdmreqLog.UserId__c;
            }          
            objSysAdmreqLog.Status__c ='In-Progress';
        }
    }
    
    /* @Method <This method is used to update System Admin User Record on System Admin Request Logged>
        @param <List<System_Admin_Request_Logs__c>>
        @return String of validated Result>
        @throws exception - <No Exception>
    */
    public void syncSystemAdminLogWithSystemAdminUser (List<System_Admin_Request_Logs__c> lstSystemAdminRequests) {
        Map<Id,System_Admin_Request_Logs__c> MapUserWithSystemAdminLog = new Map<Id,System_Admin_Request_Logs__c>();
        Map<Id,System_Admin_Users__c> MapUserWithSystemAdminUser = new Map<Id,System_Admin_Users__c>();
        for(System_Admin_Request_Logs__c logRequest :lstSystemAdminRequests){
            MapUserWithSystemAdminLog.put(logRequest.User_Name__c,logRequest );
        }
        List<System_Admin_Users__c> lstSystemAdminUser = new list<System_Admin_Users__c>();
        lstSystemAdminUser = [Select Id,End_Date_Time__c, User_Name__c, User_Name__r.IsActive , Reason__c,Temp_End_Date_Time__c, ProfileID__c from System_Admin_Users__c where User_Name__c in :MapUserWithSystemAdminLog.keyset()];
        System.debug('lstSystemAdminUser -->' + lstSystemAdminUser);
        if(!lstSystemAdminUser.isEmpty() && lstSystemAdminUser.size()>0){
           for(System_Admin_Users__c sysAdminUser :lstSystemAdminUser){
               MapUserWithSystemAdminUser.put(sysAdminUser.User_Name__c,sysAdminUser);
               System.debug('sysAdminUser -->' + sysAdminUser.User_Name__r.IsActive); 
           }
        }
        for(Id userId :MapUserWithSystemAdminLog.keyset()){
            if(MapUserWithSystemAdminUser.containsKey(userId)){
               MapUserWithSystemAdminUser.get(userId).Temp_End_Date_Time__c = MapUserWithSystemAdminLog.get(userId).End_Date__c;
               MapUserWithSystemAdminUser.get(userId).ProfileId__c = MapUserWithSystemAdminLog.get(userId).Previous_Profile__c;
               MapUserWithSystemAdminUser.get(userId).StatusOfUser__c = MapUserWithSystemAdminUser.get(userId).User_Name__r.IsActive; 
               System.debug('MapUserWithSystemAdminUser.get(userId).StatusOfUser__c) -->' + MapUserWithSystemAdminUser.get(userId).StatusOfUser__c);
               System.debug('MapUserWithSystemAdminLog.get(userId).User_Name__r.IsActive -->' + MapUserWithSystemAdminLog.get(userId).User_Name__r.IsActive);
            }
        }
        if(MapUserWithSystemAdminUser.size()>0){
           update MapUserWithSystemAdminUser.values(); 
           System.debug('MapUserWithSystemAdminUser.values()' + MapUserWithSystemAdminUser.values()); 
        }       
    } 
    
    public static void populatedProfileId(List<System_Admin_Users__c> lstSysAdmUser){ 
        
        if(lstSysAdmUser != null && lstSysAdmUser.size() >0){
            Set<String> setUserIds= new Set<String>();
            for(System_Admin_Users__c objSysAdmUser :lstSysAdmUser){
                if(objSysAdmUser.User_Name__c != null){
                    setUserIds.add(objSysAdmUser.User_Name__c);
                } 
            } 
            if(setUserIds.size() >0){
                Map<Id, User> mapUserProfile = new Map<Id,User>([Select Id, ProfileId, IsActive   from User where Id in: setUserIds]);
                for(System_Admin_Users__c objSysAdmUser :lstSysAdmUser){
                    if(mapUserProfile.containsKey(objSysAdmUser.User_Name__c) ){
                        objSysAdmUser.ProfileID__c = mapUserProfile.get(objSysAdmUser.User_Name__c).ProfileId;
                        objSysAdmUser.StatusOfUser__c = mapUserProfile.get(objSysAdmUser.User_Name__c).IsActive;
                    }
                    objSysAdmUser.UserId__c = objSysAdmUser.User_Name__c;
                }
            }
        }
    }
    
    public static void sendEmailOnNewModifiedDeleteUser(Set<Id> setUserIds){
        if(setUserIds != null && setUserIds.size() >0){
            String strUserDetails = '';
            Map<Id, User> mapUserDetails = new Map<Id, User>([Select Id, Name From User where Id in :setUserIds]);
            for(User objUser :mapUserDetails.values()){
                strUserDetails = strUserDetails + objUser.Name + ' (' + objUser.Id + '), </br>' ;
            }
            if(strUserDetails.endsWith(', ') || strUserDetails.endsWith(',')){
                strUserDetails = strUserDetails.substring(0, strUserDetails.length() -7);
            }
            
            List<String> lstRecipients = new List<String>();
            lstRecipients.add('anand.sharma2@emc.com'); 
            lstRecipients.add('sajeed.sayed@emc.com');   
            lstRecipients.add('Michael.Gram@emc.com');
                    
            String emailSubject = System.Label.SystemAdminEmailSubject; 
            String htmlBody = System.Label.SystemAdminEmailText1 + strUserDetails + System.Label.SystemAdminEmailText2;
            EmailUtils.sendHTMLEmail(lstRecipients, emailSubject, htmlBody) ;
        }
    }
    
    /////////////   Batch Job Methods ////////////////////////
    public static Boolean getEnviromentAsProduction(){
        boolean isProduction = false;
        Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
        CustomSettingDataValueMap__c jobObjectDetails = DataValueMap.get('SystemAdminProdEnviroment');
        if(jobObjectDetails != null && jobObjectDetails.DataValue__c != null && jobObjectDetails.DataValue__c != ''){
            Id orgId = jobObjectDetails.DataValue__c ;            
            if(orgId == Userinfo.getOrganizationId()){
                isProduction = true;
            }
        }
        return isProduction;
    }

    public static Set<Id> deactivateSysAdminUsers(){
        Set<Id> setRemoveSysAdminAsscess = new Set<Id>();
        String strQuery='';
        String ScheduleAfterTime = String.valueOfGmt(DateTime.Now().addMinutes(1));
        ScheduleAfterTime  = ScheduleAfterTime.replace(' ', 'T');
        ScheduleAfterTime = ScheduleAfterTime + '.000Z';       
         
        strQuery = 'Select User_Name__c from System_Admin_Users__c where Active__c = true and End_Date_Time__c != null and End_Date_Time__c < '+ ScheduleAfterTime  ; 
        system.debug('strQuery: ' + strQuery );
        List<System_Admin_Users__c> lstSysAdminUsers = Database.query(strQuery);
        if(lstSysAdminUsers != null && lstSysAdminUsers.size() > 0){
            for(System_Admin_Users__c  objSysAdmUser: lstSysAdminUsers){
                objSysAdmUser.Active__c = false;
                setRemoveSysAdminAsscess.add(objSysAdmUser.User_Name__c);
            }
            update lstSysAdminUsers;
        }
        return setRemoveSysAdminAsscess;
    }
    
    public static Set<Id> revokeSysAdminAccess(Boolean flagForAllUserRemoveAccess, Set<Id> setUpdateAdminUserForEndDatePass) {
        Set<Id> setRemoveSysAdminAsscess = new Set<Id>();
        Map<Id,System_Admin_Users__c> mapUpdateSystemAdminUser = new Map<Id,System_Admin_Users__c>();
        String strQuery='';
        String ScheduleAfterTime = String.valueOfGmt(DateTime.Now());
        ScheduleAfterTime  = ScheduleAfterTime.replace(' ', 'T');
        ScheduleAfterTime = ScheduleAfterTime + '.000Z';
        //test
        //Id SystemAdminProfileId ='00eV0000000QMhW';
        //Production
        Id SystemAdminProfileId ='00e70000001Fjc9';
        system.debug('flagForAllUserRemoveAccess #### :' + flagForAllUserRemoveAccess );

        
        if(flagForAllUserRemoveAccess){
            strQuery = 'Select Id, User_Name__c from System_Admin_Users__c  where Is_a_Temp_System_Admin__c = true OR User_Name__r.Profileid =: SystemAdminProfileId';
        }else{
            strQuery = 'Select Id, User_Name__c from System_Admin_Users__c  where (Is_a_Temp_System_Admin__c = true OR User_Name__r.Profileid =: SystemAdminProfileId)and Temp_End_Date_Time__c < '+ ScheduleAfterTime; 
        }system.debug('strQuery: ' + strQuery );
        
        List<System_Admin_Users__c> lstRemoveSysAdmin = Database.query(strQuery);
        if(!lstRemoveSysAdmin.isEmpty() && lstRemoveSysAdmin.size()>0){
            for(System_Admin_Users__c sysAdminUser :lstRemoveSysAdmin){ 
                sysAdminUser.Is_a_Temp_System_Admin__c = false;
                setRemoveSysAdminAsscess.add(sysAdminUser.User_Name__c);  
                mapUpdateSystemAdminUser.put(sysAdminUser.Id, sysAdminUser);                
            } 
        } 
        
        if(setUpdateAdminUserForEndDatePass != null && setUpdateAdminUserForEndDatePass.size() >0){
            List<System_Admin_Users__c> lstInActiveSysAdminUser = [Select Id, User_Name__c from System_Admin_Users__c  where User_Name__c in:setUpdateAdminUserForEndDatePass ];
            for(System_Admin_Users__c sysAdminUser :lstInActiveSysAdminUser){ 
                sysAdminUser.Is_a_Temp_System_Admin__c = false;
                setRemoveSysAdminAsscess.add(sysAdminUser.User_Name__c);  
                mapUpdateSystemAdminUser.put(sysAdminUser.Id, sysAdminUser);                
            }          
        }
        if(mapUpdateSystemAdminUser != null && mapUpdateSystemAdminUser.size() >0){
            update mapUpdateSystemAdminUser.values();
        }
        return setRemoveSysAdminAsscess;
    }
    
    public static void grantAndRevokeAccessToUser(Set<Id> setRemoveSysAdminAsscess, set<Id> setGrantSysAdminAccess, Boolean flagForAllUserRemoveAccess){
        Set<Id> setUserIds = new Set<Id>();
        //Revoke Access  
        if(setRemoveSysAdminAsscess != null && setRemoveSysAdminAsscess.size() >0){
            Map<String, System_Admin_Users__c> mapUserNameWithSysAdminUsers = new Map<String, System_Admin_Users__c>();
            Map<Id, System_Admin_Users__c> mapSystemAdminUsers = new Map<Id, System_Admin_Users__c>([Select Id, User_Name__c,StatusOfUser__c, ProfileId__c from System_Admin_Users__c where User_Name__c in :setRemoveSysAdminAsscess]);
            if(mapSystemAdminUsers != null && mapSystemAdminUsers.size() >0){
                for(System_Admin_Users__c objSysAdmUser : mapSystemAdminUsers.values()){
                    mapUserNameWithSysAdminUsers.put(objSysAdmUser.User_Name__c, objSysAdmUser);
                }
            }
            List<User> lstAdminUser = new List<User>([select Id, ProfileId, isActive from User where Id in :setRemoveSysAdminAsscess and isActive= true]);
            //system.debug('SIZE OF mapIdUser: ' + mapIdUser.size() );
            if(lstAdminUser != null && lstAdminUser.size() >0){
                for(user objUser : lstAdminUser){
                    if(mapUserNameWithSysAdminUsers.containsKey(objUser.Id)){
                        objUser.ProfileId = mapUserNameWithSysAdminUsers.get(objUser.Id).ProfileId__c;
                        objUser.isActive = mapUserNameWithSysAdminUsers.get(objUser.Id).StatusOfUser__c;                    
                    }                  
                }
                update lstAdminUser;
            }
        }      
        if(flagForAllUserRemoveAccess){ 
            return;
        }
         
        //Grant Access
        if(setGrantSysAdminAccess != null && setGrantSysAdminAccess.size() >0){
            SystemAdministration_Utility.grantAccessToUsers(setGrantSysAdminAccess);
        }
    }
    
    public static Set<Id> getUsersForSystemAdminAccessFromLog(){
        Set<Id> setUserIds = new Set<Id>();
        List<System_Admin_Request_Logs__c> lstSystemAdminReqLogs =[Select Id, User_Name__c from System_Admin_Request_Logs__c where Status__c ='In-Progress'];
        if(lstSystemAdminReqLogs != null && lstSystemAdminReqLogs.size() >0){
            for(System_Admin_Request_Logs__c objSysAdmLog : lstSystemAdminReqLogs){
                setUserIds.add(objSysAdmLog.User_Name__c);            
            }
        }
        return setUserIds;
    }
    
    public static void grantAccessToAllEligibleUsers(){
        Set<Id> userIds = new Set<Id>();
        Map<Id, System_Admin_Users__c> mapSystemAdminUsers = new Map<Id, System_Admin_Users__c>([Select Id, User_Name__c,StatusOfUser__c, ProfileId__c from System_Admin_Users__c where Is_a_Temp_System_Admin__c = false]);
        if(mapSystemAdminUsers != null && mapSystemAdminUsers.size() >0){
            for(System_Admin_Users__c objSysAdminUser: mapSystemAdminUsers.values()){
                userIds.add(objSysAdminUser.User_Name__c);
            }
            if(userIds.size() >0){
                SystemAdministration_Utility.grantAccessToUsers(userIds);
            }
        }
    }
    
    public static void grantAccessToUsers(Set<Id> userIds){
        if(userIds != null && userIds.size() >0){
            Integer intBatchIb = 72;
            Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
            CustomSettingDataValueMap__c  seqeunceId = DataValueMap.get('SystemAdminNoOfHours');
            if(seqeunceId != null && seqeunceId.DataValue__c != null && seqeunceId.DataValue__c != ''){
                intBatchIb = Integer.valueOf(seqeunceId.DataValue__c);           
            }
            List<User> lstUsers = new List<User>([Select Id, isActive, profileid from user where id in: userIds]);
            if(lstUsers != null && lstUsers.size() >0){
                for(User objUser : lstUsers){
                    // Development profile id
                    //objUser.ProfileId ='00e70000000wBza';
                    
                    // Test profile id Need to update for production after testing
                    //objUser.ProfileId ='00eV0000000QMhW';
                    
                    //Production profile id Need to update for production after testing
                    objUser.ProfileId ='00e70000001Fjc9';
                    
                    objUser.IsActive = true;
                    objUser.Sys_Admin_Exp_Date_Time__c = System.now().addHours(intBatchIb);
                    objUser.Sys_Admin_Start_Date_Time__c = System.now();
                }
                update lstUsers;
            }
        }      
    }
     
    public void updateCustomSetting(){
        Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
        CustomSettingDataValueMap__c  objDVM = DataValueMap.get('SystemAdminNoOfHours'); 
        if(inputstr== null || inputstr=='')  {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.Error, System.Label.SystemAdminNoOfHoursMessage);
            ApexPages.addMessage(errorMessage);
        } else{ 
            if(objDVM != null){
                objDVM.DataValue__c = inputstr;
                update objDVM;
            }
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.Info, System.Label.SystemAdminSuccesOfHoursMessage);
            ApexPages.addMessage(errorMessage);
        }
    }
     
    public void runBatchJob(){
        
        Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
        CustomSettingDataValueMap__c  objDVM = DataValueMap.get('SystemAdminJobStatus'); 
        if(objDVM != null){  
            objDVM.DataValue__c = 'true';
            update objDVM;
        }
    
        try{
        //1072 adding 'Holding' in filter
            List<AsyncApexJob> activejobs = [SELECT Id, Status,ApexClassId,ApexClass.Name from AsyncApexJob WHERE ApexClass.Name like'%AdminAccessJob%' and (status='Queued' or status='Processing' or status='Preparing' or status='Holding')];
            system.debug('--------------'+activejobs.size());
            if(activejobs!= null && activejobs.size()==0){
                if(activejobs.size()>0){
                    //Abort the existing Batch job 
                    for(AsyncApexJob a:activejobs){
                        system.abortJob(a.id);
                    }
                }
                system.debug('----------------'+activejobs.size());
                Database.executeBatch(new SystemAdminAccessJob(true));
            }
            
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.Info, System.Label.SystemAdminSuccesOfRemoveJobMessage);
            ApexPages.addMessage(errorMessage);
        }catch(exception ex){ 
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.Info, ex.getMessage());
            ApexPages.addMessage(errorMessage);
        }
    
    }
    
    
}