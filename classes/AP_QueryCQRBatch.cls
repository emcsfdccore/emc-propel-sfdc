/*===================================================================================================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE           DEVELOPER                WR                DESCRIPTION                               
 |  ====           =========                ==                =========== 
 |  03 March 2015  Uday Annapareddy      CDC #1917        Initial Creation : CQR Delete batch job is deleting more records if 11i sent less records into CQR Table, to restrict that 
 |                                                        in this batch job we are quering the CQR records which weren't updated from 11i based on the count
 |                                                        we will start the BatchDeleteCQRData batch job.
 +===================================================================================================================================================================================*/


public class AP_QueryCQRBatch implements Database.Batchable<sObject>, Database.Stateful  {
   public final string Query;
   public Decimal batchID;
   public Decimal maxCQRDeleteValue;
   public Integer maxCQRDeleteValueInt;
   public Decimal maxBulkInsertValue;
   public Integer maxBulkInsertValueInt;
   public string dataValue;
   public Id jsId;
   public Job_Scheduler__c Js;
   Integer total = 0;
   public Map<String,CustomSettingDataValueMap__c> mapCustomSettingDataValueMap = CustomSettingDataValueMap__c.getall();
    CustomSettingDataValueMap__c maxBulkInsert = (mapCustomSettingDataValueMap.get('Max Bulk Insert'));
    CustomSettingDataValueMap__c maxCQRDelete =  (mapCustomSettingDataValueMap.get('Max CQR Delete'));
   
   
   public AP_QueryCQRBatch(Job_Scheduler__c js,Decimal batchID) {
     System.debug('-------------------------********* INSIDE CONSTRUCTOR PARAMETER *********-----------------------------');
     
         this.Js = js;
         System.debug('js-in constructor--->'+ Js);
        this.batchID = batchID;
        system.debug('batchId--->'+batchId);   
        system.debug('maxBulkInsert --->'+maxBulkInsert); 
        String CQRUpdateQuery = '';
        try{
            
       
            if(Test.isRunningTest()){
            CQRUpdateQuery = 'SELECT Id FROM Core_Quota_Rep__c where Batch_ID__c != null and Batch_ID__c < :batchID limit 10'; 
            }
            else
            {
                CQRUpdateQuery = 'SELECT Id FROM Core_Quota_Rep__c where Batch_ID__c != null and Batch_ID__c = :batchID' ;
            }
        }
        catch(QueryException qe){
            System.debug('Exception for query Inside Constructor' + qe);
        }
            this.Query = CQRUpdateQuery;
         
   system.debug('Query'+cqrUpdateQuery);
   }

    
   // Method : To Start the batch job          
    public Database.QueryLocator start(Database.BatchableContext BC){
       
        return Database.getQueryLocator(Query);
    }
    
    //Mehotd : To execute Batch Job and update the custom setting Max Bulk Insert with count where batch id < new batch id pushed from 11i today.
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        system.debug('in Execute');
        total += scope.size();
        system.debug('total++++++++'+total);
        
    } ////END : execute
  
   /*Method :Finish Batch Job - This method is used Kick start BatchDeleteCQRData job only if the CQR delete records are less than custom setting value  maxCQRDelete
    If expected count is not met mail will be sent*/  
    public void finish(Database.BatchableContext BC){
        System.debug('-------------------------********* INSIDE FINAL METHOD *********-----------------------------');
        System.debug('------------------Run Batch Job AP_BatchDeleteCQRData--------------------');
        dataValue = string.valueof(total);
        maxBulkInsert.DataValue__c = dataValue;
        update maxBulkInsert;
          if(maxCQRDelete.DataValue__c!=null)
          maxCQRDeleteValue = Decimal.valueOf(maxCQRDelete.DataValue__c);
          if(maxCQRDeleteValue != null)
            maxCQRDeleteValueInt = maxCQRDeleteValue.intValue();  
          system.debug('maxCQRDeleteValue--->'+maxCQRDeleteValue);   
               
          if(maxBulkInsert.DataValue__c!=null)
            maxBulkInsertValue = Decimal.valueOf(maxBulkInsert.DataValue__c);
         if(maxBulkInsertValue != null)
            maxBulkInsertValueInt = maxBulkInsertValue.intValue();  
            
            
                  
          If( maxBulkInsertValueInt > maxCQRDeleteValueInt ){
          
             AP_BatchDeleteCQRData batchDeleteCQRObj =  new AP_BatchDeleteCQRData(js,batchID);  
             ID batchProcessId = Database.executeBatch(batchDeleteCQRObj);
          } 
          
    }
}