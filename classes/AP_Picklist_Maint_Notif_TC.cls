/* =====================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  31.12.2012      Avinash K       213868      Initial Creation.  Test class for AP_Picklist_Maint_Notif class.
 |  19.12.2013      Jaypal Nimesh   Backward    Optimized class per best practices and improved code coverage.
                                    Arrow
 +=====================================================================================================================*/
@isTest
public class AP_Picklist_Maint_Notif_TC 
{  
    public static testMethod void TestAP_Picklist_Maint_Notif()
    {   
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1]; 
    
        //Calling helper class method to create custom setting Data
        System.runAs(insertUser){        
            CustomSettingDataHelper.dataValueMapCSData();
        }
        
        Map<String,Set<String>> mapExistingFieldValues = new Map<String,Set<String>>();

        Set<String> setTemp = new Set<String>();
        
        //Initializing object
        District_Lookup__c objDL = new District_Lookup__c();

        // Get the object type of the SObject.
        Schema.sObjectType objType = objDL.getSObjectType(); 

        // Describe the SObject using District_Lookup__c type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       

        // Get a map of fields for District_Lookup__c
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        
        // Get the list of picklist values for all District Lookup Picklist fields.
        list<Schema.PicklistEntry> lstAreaValues = fieldMap.get('Area__c').getDescribe().getPickListValues();
        
        for(Integer i = 0 ; i < lstAreaValues.size() ; i++){
            
            if(i != 0){
                setTemp.add(lstAreaValues[i].getValue());
            }
        }
        
        mapExistingFieldValues.put('Area__c',setTemp);
        
        setTemp = new Set<String>();

        list<Schema.PicklistEntry> lstBRMAreaValues = fieldMap.get('BRM_Area__c').getDescribe().getPickListValues();
        
        //Filling map with picklist field values
        for(Integer i = 0 ; i < lstBRMAreaValues.size() ; i++){
            
            if(i != 0){
                setTemp.add(lstBRMAreaValues[i].getValue());
            }
        }
        
        mapExistingFieldValues.put('BRM_Area__c',setTemp);
        
        setTemp = new Set<String>();            
        
        //Getting picklist field values and filling map 
        list<Schema.PicklistEntry> lstDivisionValues = fieldMap.get('Division__c').getDescribe().getPickListValues();
        
        for(Integer i = 0 ; i < lstDivisionValues.size() ; i++){
            
            if(i != 0){
                setTemp.add(lstDivisionValues[i].getValue());
            }
        }
        
        mapExistingFieldValues.put('Division__c',setTemp);
        
        setTemp = new Set<String>();
        
        list<Schema.PicklistEntry> lstIntDomesticValues = fieldMap.get('International_Domestic__c').getDescribe().getPickListValues();
        
        for(Integer i = 0 ; i < lstIntDomesticValues.size() ; i++){
            
            if(i != 0){
                setTemp.add(lstIntDomesticValues[i].getValue());
            }
        }
        //Putting values in map to prepare data
        mapExistingFieldValues.put('International_Domestic__c',setTemp);
        
        setTemp = new Set<String>();

        list<Schema.PicklistEntry> lstRegionValues = fieldMap.get('Region__c').getDescribe().getPickListValues();
        
        for(Integer i = 0 ; i < lstRegionValues.size() ; i++){
            
            if(i != 0){
                setTemp.add(lstRegionValues[i].getValue());
            }
        }
        
        mapExistingFieldValues.put('Region__c',setTemp);
        
        setTemp = new Set<String>();

        //Getting picklist field values and filling map 
        list<Schema.PicklistEntry> lstReportingLevelValues = fieldMap.get('Reporting_Level__c').getDescribe().getPickListValues();
        
        for(Integer i = 0 ; i < lstReportingLevelValues.size() ; i++){
            
            if(i != 0){
                setTemp.add(lstReportingLevelValues[i].getValue());
            }
        }

        mapExistingFieldValues.put('Reporting_Level__c',setTemp);
    
        setTemp = new Set<String>();

        list<Schema.PicklistEntry> lstSuperAreaValues = fieldMap.get('Super_Area__c').getDescribe().getPickListValues();
        
        for(Integer i = 0 ; i < lstSuperAreaValues.size() ; i++){
            
            if(i != 0){
                setTemp.add(lstSuperAreaValues[i].getValue());
            }
        }

        mapExistingFieldValues.put('Super_Area__c',setTemp);
        
        setTemp = new Set<String>();
        
        //Getting picklist field values and filling map 
        list<Schema.PicklistEntry> lstSuperRegionValues = fieldMap.get('Super_Region__c').getDescribe().getPickListValues();
        
        for(Integer i = 0 ; i < lstSuperRegionValues.size() ; i++){
            
            if(i != 0){
                setTemp.add(lstSuperRegionValues[i].getValue());
            }
        }

        mapExistingFieldValues.put('Super_Region__c',setTemp);
        
        setTemp = new Set<String>();
        
        //Getting picklist values for different fields and filling map 
        list<Schema.PicklistEntry> lstSuperReportingLevelValues = fieldMap.get('Super_Reporting_Level__c').getDescribe().getPickListValues();
        
        for(Integer i = 0 ; i < lstSuperReportingLevelValues.size() ; i++){
            
            if(i != 0){
                setTemp.add(lstSuperReportingLevelValues[i].getValue());
            }
        }
        
        mapExistingFieldValues.put('Super_Reporting_Level__c',setTemp);
        
        //List to hold District_Lookup__c data
        List<District_Lookup__c> distLookupList = new List<District_Lookup__c>();
        
        //Iterating and creating test data for District_Lookup__c
        for(Integer i=0 ; i < 10 ; i++){
        
            District_Lookup__c objDistrictLookup = new District_Lookup__c();
            
            objDistrictLookup.Name ='TestLookup' + i;
            objDistrictLookup.Area__c = lstAreaValues[0].getValue();
            objDistrictLookup.BRM_Area__c = lstBRMAreaValues[0].getValue();
            objDistrictLookup.Division__c = lstDivisionValues[0].getValue();
            objDistrictLookup.International_Domestic__c = lstIntDomesticValues[0].getValue();
            objDistrictLookup.Region__c = lstRegionValues[0].getValue();
            objDistrictLookup.Reporting_Level__c = lstReportingLevelValues[0].getValue();
            objDistrictLookup.Super_Area__c = lstSuperAreaValues[0].getValue();
            objDistrictLookup.Super_Region__c = lstSuperRegionValues[0].getValue();
            objDistrictLookup.Super_Reporting_Level__c = lstSuperReportingLevelValues[0].getValue();
            objDistrictLookup.Oracle_District_ID__c = '3213243240' + i;           
            
            distLookupList.add(objDistrictLookup);
        }
        
        //Insert District_Lookup__c list
        insert distLookupList;

        //Send the Map of Existing Picklist Values to the Batch Class
        AP_Picklist_Maint_Notif batch = new AP_Picklist_Maint_Notif('SELECT Name, Area__c, BRM_Area__c, Division__c, International_Domestic__c, Oracle_District_ID__c, Region__c, Reporting_Level__c, Super_Area__c, Super_Region__c, Super_Reporting_Level__c FROM District_Lookup__c limit 10', mapExistingFieldValues) ;
        
        Integer intNoOfRecordsPerBatch = 200;
        
        //Test started with Batch execution
        Test.startTest();
        Database.executeBatch(batch, intNoOfRecordsPerBatch);
        Test.stopTest();
    }
}