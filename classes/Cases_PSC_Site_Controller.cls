public class Cases_PSC_Site_Controller{
    
   
    private final Case cs;
    //Case ID
    public Id caseid;
    public Boolean caseSaved {get; set;} {caseSaved=false;}
    public Attachment caseAttachment {get; set;}
    public List<Attachment> caseAttachments{get; set;}
     //Attachment
    public static final integer attachmentsToAdd = 4;
    public List<Attachment> newAttachments{get;set;}
    public Boolean addAtt {get; set;}
    
    
    public String imgLink{get;set;}
    //public String depValues{get;set;}
    public String recordTypeValue{get; set;}
    public RecordType rtObj = new RecordType();
    public Case caseObj {get; set;}
    public String selectedLanguage{get; set;}
    public List<SelectOption> selectLanguage {get; set;}
    public List<PSC_Subtype_Labels_wrapper> dependentListwrapper{get;set;}
public List<List<PSC_Subtype_Labels_wrapper>> listdependentListwrapper{get;set;}
    public String labName;
    public String subTypeValue{get; set;}
    public List<String> fldString{get; set;}
    public String recSubTypeMapping {get; set;}
    public Map<String,SC_RecordType_Mapping__c> pscFldMappingMap{get; set;}
    public Map<String,List<SC_RecordType_Mapping__c>> pscSecMappingMap{get; set;}
    public List<String> pscSecLst {get; set;}
    public String caseNumber {get; set;}
    public Integer countInf=0;
    public Integer countDisp=0;
    public Integer infoIndex=100;
    public Integer dispIndex=100;
    public List<SC_RecordType_Mapping__c> infoLst {get;set;}
    public List<SC_RecordType_Mapping__c> dispLst {get;set;}
    public boolean showPicklist{get;set;}
    public boolean inquiryTypeRequired{get;set;}
    public boolean showReqType{get;set;}
    public PSC_FieldMapping__c fieldMapConfig = null;
    public list<selectOption> inquiryValues{get; set;}
    public list<selectOption> partnerTypeValues{get; set;}
    public list<selectOption> reqTypeValues{get; set;}
    public string subTypeDependents{get; set;}
    public List<string> finalInquireyValues {get; set;}
    public List<string> finalReqTypeValues {get; set;}
    public List<string> finalPartTypeValues {get;set;}
    public string selectedInquiry{get; set;}
    public string selectedReqType{get; set;}
    public string selectedPartnerType{get;set;}
    public boolean allowInsert{get; set;}
    public String partnerAccName{get;set;}
    public boolean isDealRegMsg{get;set;}
    public boolean isContractMgmtMsg{get;set;}
    Public String PSC_Current_Distributor{get;set;}
    Public String Current_Distributor_or_VAR_Text{get;set;}
    Public String New_Distributor_or_VAR_Text{get;set;}
    Public String PSC_Requested_Preferred_Distributor{get;set;}
    public String PSC_Partner_Org{get;set;}            
    
    public Cases_PSC_Site_Controller(){
        
        showPicklist = false;
        showReqType = false;
        inquiryTypeRequired = true;
        //Fetching Record Type name from the URL parameter passed from Main page
        recordTypeValue = ApexPages.currentPage().getParameters().get('recordType');
        //recordTypeValue='PSC - '+recordTypeValue;
        //Fetching Custom setting data for the image details and Sub Type values for a particular Record Type
        Map<String,PSC_CaseType_Image_Site__c> mapPscTypeImage = PSC_CaseType_Image_Site__c.getAll();
        if( recordTypeValue!='' && recordTypeValue != null && mapPscTypeImage !=null && mapPscTypeImage.size()>0){
            //Fetching image details for the RT
            PSC_CaseType_Image_Site__c subTypeImageObj = mapPscTypeImage.get(recordTypeValue);
            //Fetching the list of Subtypes for a given case type
            List<String> depValues = subTypeImageObj.Dependent_Values__c.split(',');
       List<PSC_Subtype_Labels_wrapper> miniLst=new List<PSC_Subtype_Labels_wrapper>();
            dependentListwrapper=new List<PSC_Subtype_Labels_wrapper>();
 listdependentListwrapper=new List<List<PSC_Subtype_Labels_wrapper>>();
            imgLink = subTypeImageObj.ImgValue__c;
            
             for(String subVal:depValues){
                  String subVal1=subVal;
                   String labelforHeading=subVal1;
                  if(subVal.contains(' / ')||subVal.contains('/')||subVal.contains(' & ')||subVal.contains('&')||subVal.contains(' - ')||subVal.contains('-')){
                                if(subVal=='Configuration Support(EMEA/APJ)'){
                                subVal='Configuration Support_EMEA_APJ';
                                }
                                
                                if(subVal.contains(' / ')){
                                subVal=subVal.replaceAll(' / ','_');    
                                }
                                if(subVal.contains('/')){
                                subVal=subVal.replaceAll('/','_');  
                                }
                                if(subVal.contains(' & ')){
                                subVal=subVal.replaceAll(' & ','_');    
                                }
                                if(subVal.contains('&')){
                                subVal=subVal.replaceAll('&','_');
                                }
                                if(subVal.contains(' - ')){
                                subVal=subVal.replaceAll(' - ','_');
                                }
                                if(subVal.contains('-')){
                                subVal=subVal.replaceAll('-','_');
                                }
                        
                            }
                        
                labName='PSC_'+subVal.replaceAll(' ','_');
                labelforHeading=labName+'_heading';
                dependentListwrapper.add(new PSC_Subtype_Labels_wrapper(subVal1,labelforHeading,labName));
            }
Integer counter=0;
            for(PSC_Subtype_Labels_wrapper wrapLstRec:dependentListwrapper){
                miniLst.add(wrapLstRec);
                counter++;
                if(counter==3){
                    listdependentListwrapper.add(miniLst);
                    miniLst=new List<PSC_Subtype_Labels_wrapper>();
                    counter=0;
                }   
            }
            if(!miniLst.isEmpty()){
                    listdependentListwrapper.add(miniLst);  
            }
           System.debug('listdependentListwrapper--->'+listdependentListwrapper);
        }
        
        
        //Language Selection made by the user on the first page - Passed as a parameter in the URL
        selectedLanguage= ApexPages.currentPage().getParameters().get('langForPage');
        system.debug('---selectedLanguage---'+selectedLanguage);
        //Initially defaulting the language to English
        if(selectedLanguage==null)
        {   
            selectedLanguage = 'en_US';
        }
        selectLanguage = new List<SelectOption>();
        Map<String,PSC_Cases_Language__c> mapLangauage = PSC_Cases_Language__c.getall();
        //Creating and adding value to List for sorted Language display
        List<String> lstLangSorted = new List<String>();
        lstLangSorted.addall(mapLangauage.keyset());
        lstLangSorted.sort();
        
        for(String langName : lstLangSorted){
            selectLanguage.add(new SelectOption(mapLangauage.get(langName).Language_Code__c , langName));
        }
    }
    
    //Fetching the Case record details from the  user input
    public Cases_PSC_Site_Controller(ApexPages.StandardController controller){
    
        fieldMapConfig = PSC_FieldMapping__c.getValues('DealInquirySubTypeNonMandatory');
        isDealRegMsg = false;
        isContractMgmtMsg = false;
        
        this.cs = (Case)controller.getRecord();
        caseObj =(Case)controller.getRecord();
        
        caseAttachment = new Attachment();
        caseAttachments = new List<Attachment>();
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        
        //Attachment Button

        addAtt = true;
        
        recordTypeValue = ApexPages.currentPage().getParameters().get('recordType');
        recordTypeValue='PSC - '+recordTypeValue;
        fldString = new List<String>();
        pscSecLst=new List<String>();
        infoLst=new List<SC_RecordType_Mapping__c>();
        dispLst=new List<SC_RecordType_Mapping__c>();
        pscFldMappingMap = new Map<String,SC_RecordType_Mapping__c>();
        subTypeValue = ApexPages.currentPage().getParameters().get('subType');
        selectedLanguage=ApexPages.currentPage().getParameters().get('langForPage');
        System.debug('----->recordTypeValue-->'+recordTypeValue);
        System.debug('Select id,Name from RecordType where sObjectType=\''+'Case'+'\'and name=\''+recordTypeValue+'\'');
        
         selectedInquiry = '';
         selectedReqType = '';
         selectedPartnerType = '';
         inquiryTypeRequired = true;
         finalInquireyValues = new List<string>();
         finalReqTypeValues = new List<string>();
         finalPartTypeValues= new List<string>();
         inquiryValues = new List<selectOption>();
         inquiryValues.add(new SelectOption('None','--None--'));
         reqTypeValues = new List<selectOption>();
         reqTypeValues.add(new SelectOption('None','--None--'));
         partnerTypeValues = new List<selectOption>();
        
        if(fieldMapConfig != null && fieldMapConfig.DependentValues__c.containsIgnoreCase(subTypeValue)){
            
            inquiryTypeRequired = false;
        }
         
        map<string,PSC_FieldMapping__c> customAll = PSC_FieldMapping__c.getAll();
         
        for(PSC_FieldMapping__c csConfig: customAll.values())
        {
            if(csConfig.UniqueControlling__c == subTypeValue && csConfig.FieldName__c == 'Inquiry Type')
            {
                showPicklist = true;
                finalInquireyValues = csConfig.DependentValues__c.split(',');
                for(string st: finalInquireyValues)
                {
                    inquiryValues.add(new SelectOption(st,st));
                    
                }
            }
            if(csConfig.UniqueControlling__c == subTypeValue && csConfig.FieldName__c == 'Request Type'){
                showReqType = true;
                finalReqTypeValues = csConfig.DependentValues__c.split(',');
                for(string st: finalReqTypeValues)
                {
                    reqTypeValues.add(new SelectOption(st,st));
                    
                }
            }
            if(csConfig.UniqueControlling__c == 'Web Partner Type' && csConfig.FieldName__c == 'Partner Type'){
            finalPartTypeValues=csConfig.DependentValues__c.split(',');
            for(string st: finalPartTypeValues)
                {
                    partnerTypeValues.add(new SelectOption(st,st));
                    
                }
            }
        }
        
        if(recordTypeValue=='PSC - Channel System Support' && subTypeValue=='Partner Portal'){
        
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,System.Label.PSC_Partner_Portal_Message);
            ApexPages.addMessage(myMsg); 
        
        }
        if(recordTypeValue=='PSC - Contract Management'){
            isContractMgmtMsg = true;
        }
        
        if(recordTypeValue=='PSC - Deal Registration and Lead Management'){
            isDealRegMsg = true;
        }
        
        if(recordTypeValue != ''){
            rtObj  = [Select id,Name from RecordType where sObjectType='Case' and name=:recordTypeValue];
            
            if(rtObj !=null){
                caseObj.recordtypeid = rtObj.id;
                caseObj.Record_Type_Hidden__c = recordTypeValue;
                caseObj.Type = subTypeValue;
                caseObj.Case_Sub_Type__c = subTypeValue;
                caseObj.BU__c ='testBU';
                System.debug('----->subType-->'+caseObj.Case_Sub_Type__c);
            }
        }
        
         recSubTypeMapping = '';
         fldString.clear();
         pscFldMappingMap.clear();
        
        recSubTypeMapping = recordTypeValue.trim()+'+'+subTypeValue;
        //recSubTypeMapping = recordTypeValue.trim();
        system.debug('-----recSubTypeMapping----' + recSubTypeMapping);
        
        List<SC_RecordType_Mapping__c> lstRecord = [Select FieldName__c,Section_Name__c,Section_Label_Name__c,On_webform__c,isMandatory__c,isRendered__c,SortOrder__c,RecordType_Subtype__c from SC_RecordType_Mapping__c where RecordType_Subtype__c=:recSubTypeMapping AND On_webform__c=true order by SortOrder__c ];
        pscSecMappingMap = new Map<String,List<SC_RecordType_Mapping__c>>();
            
            for(SC_RecordType_Mapping__c pscRecObj: lstRecord){
            fldString.add(pscRecObj.FieldName__c);
            pscFldMappingMap.put(pscRecObj.FieldName__c,pscRecObj);
            
                if(pscSecMappingMap.containsKey(pscRecObj.Section_Name__c)){
                pscSecMappingMap.get(pscRecObj.Section_Name__c).add(pscRecObj);
                }
                else{
                pscSecMappingMap.put(pscRecObj.Section_Name__c,new List<SC_RecordType_Mapping__c>{pscRecObj});
                }
                
            }
            
            if(pscSecMappingMap != null && pscSecMappingMap.size() > 0){
                
                pscSecLst.addAll(pscSecMappingMap.keySet());
                for(String str:pscSecLst){
                    countInf++;
                    countDisp++;
                    if(str=='Case Information'){
                        infoLst.addAll(pscSecMappingMap.get('Case Information'));
                        infoIndex=countInf-1;
                    }
                    if(str=='Description Information'){
                        dispLst.addAll(pscSecMappingMap.get('Description Information'));
                        dispIndex=countDisp-1;
                    }
                }
                if(infoIndex !=100){
                    pscSecLst.remove(infoIndex);
                }
                if(infoIndex !=100 && dispIndex !=100){
                    pscSecLst.remove(dispIndex-1);
                }
                if(dispIndex !=100 && infoIndex==100){
                    pscSecLst.remove(dispIndex);
                }
                System.debug('pscSecLst--->'+pscSecLst);
                
            }
                             
    }
    
    //Getting Image links
    public string getImgLogo(){
    
        return imgLink;   
    }
    
    //Method to decide whether to display language options to user or not. It would be displayed on Web-form.
    public Boolean getisRenderLang() {  
        
        Boolean isRenderLang = true;  
        if(UserInfo.getUsertype()!='Guest'){
          isRenderLang = false;
        }
        return isRenderLang;
    }
    
     //Wrapper class to combine the header and description text to be displayed on the VF page
    public class PSC_Subtype_Labels_wrapper {
    
        public String displayName {get;set;}
         public String headingLabel {get;set;}
        public String desclabelName {get;set;}
       
        public PSC_Subtype_Labels_wrapper(String subVal,String headLabName,String labName){
            displayName=subVal;
            headingLabel=headLabName;
            desclabelName=labName;   
           
        }
    } 
    
    
    //Method to save the Case record in the database
     public void SaveAction(){  
        
        try{
            if(selectedInquiry != 'None'){
                caseObj.Inquiry_Type_1__c = selectedInquiry;
                allowInsert = true;
            }
            else if(fieldMapConfig != null && !fieldMapConfig.DependentValues__c.containsIgnoreCase(subTypeValue)){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.PSC_Inquiry_Type_Mandatory_Error_Msg);
                ApexPages.addMessage(myMsg);
                caseSaved=false;
                allowInsert = false;
            }
            
            if(selectedReqType != 'None'){
                caseObj.Request_Type__c = selectedReqType;
                caseSaved = true;
            }
            if(recordTypeValue=='PSC - Deal Registration and Lead Management'){
                if(New_Distributor_or_VAR_Text!=null && New_Distributor_or_VAR_Text!=''){
                
                    caseObj.description=caseObj.description+'\n'+'Requested Preferred Distributor : '+New_Distributor_or_VAR_Text +'     ';
                } 
                if(Current_Distributor_or_VAR_Text!=null && Current_Distributor_or_VAR_Text!='' ){
                    caseObj.description=caseObj.description+'\n'+'Current Distributor : '+Current_Distributor_or_VAR_Text +'    ';
                
                }
            }
            if(PSC_Partner_Org!=null && PSC_Partner_Org!=''){
                    caseObj.description=caseObj.description+'\n'+'Partner Organization : '+ PSC_Partner_Org+'   ';
            
            }
            if(subTypeValue=='Preferred Distributor Changes'){
                if(PSC_Current_Distributor==null || PSC_Current_Distributor==''){
                      ApexPages.Message Error_Msg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.PSC_Curr_Disti_Req);
                      ApexPages.addMessage(Error_Msg);
                      caseSaved=false;
                      allowInsert = false;      
                      
                }
                if(PSC_Requested_Preferred_Distributor==null || PSC_Requested_Preferred_Distributor==''){
                       ApexPages.Message Error_Msg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.PSC_Preff_Disti_Req);
                       ApexPages.addMessage(Error_Msg);
                       caseSaved=false;
                       allowInsert = false;      
                       
                }
            
            
            }
            
            
           /* if(partnerAccName!=null && partnerAccName!='' && !((caseObj.description).contains('Partner Account Name : ')) ){
                caseObj.description=caseObj.description+'\n\n'+'Partner Account Name : '+partnerAccName;
            }
            if(selectedPartnerType!=null && selectedPartnerType!='' && !((caseObj.description).contains('Partner Type : '))){
                caseObj.description=caseObj.description+'\n'+'Partner Type : '+selectedPartnerType+'\n\n';
            }*/
            /*if(caseObj.Deal_Registration_Number__c!=null && caseObj.Deal_Registration_Number__c!=''){
                List<Lead> LeadList=[SELECT id,name FROM Lead WHERE Lead_Number__c = : caseObj.Deal_Registration_Number__c];
                if(LeadList.size()>0){
                    caseObj.Deal_Registration_URL__c=URL.getSalesforceBaseUrl().toExternalForm() + '/' + LeadList[0].Id;
                }
            }*/
            /*if(caseObj.Opp_Number__c!=null && caseObj.Opp_Number__c!=''){
                List<opportunity> OpptyList=[SELECT id,name,Opportunity_Number__c FROM Opportunity WHERE Opportunity_Number__c = : caseObj.Opp_Number__c];
                if(OpptyList.size()>0){
                    caseObj.Opportunity_URL__c=URL.getSalesforceBaseUrl().toExternalForm() + '/' + OpptyList[0].Id;
                }
            }*/
            if(allowInsert != false){
            if(caseId==null)
                {   
                 
                  Insert cs;
                  caseSaved=true;
                  List<Case_Helper__c> csHelpLst=new List<Case_Helper__c>();
                  Case_Helper__c CsHelp = new Case_Helper__c();
                  //Case helper record insertion 
                      if((PSC_Partner_Org!=null && PSC_Partner_Org!='')||(PSC_Current_Distributor!=null && PSC_Current_Distributor!='')||(Current_Distributor_or_VAR_Text!=null && Current_Distributor_or_VAR_Text!='')|| (New_Distributor_or_VAR_Text!=null && New_Distributor_or_VAR_Text!='')|| (PSC_Requested_Preferred_Distributor!=null && PSC_Requested_Preferred_Distributor!='')){
                      
                      csHelp.Case__c = cs.id;
                      csHelp.PSC_Current_Distributor__c = PSC_Current_Distributor;
                      csHelp.Current_Distributor_or_VAR_Text__c = Current_Distributor_or_VAR_Text;
                      csHelp.New_Distributor_or_VAR_Text__c = New_Distributor_or_VAR_Text;
                      csHelp.PSC_Requested_Preferred_Distributor__c = PSC_Requested_Preferred_Distributor;
                      CsHelp.Partner_Organization__c=PSC_Partner_Org;
                      csHelpLst.add(CsHelp);
                      }
                    if(csHelpLst.size()>0){
                        insert csHelpLst;
                    }
                      
                     
               }else{
                            cs.id=caseId;   
                            update(cs);
                            caseSaved=true;
                          }
            }
            
            else{
            
              caseSaved=false;
            }
         }catch(DMLException ex){
           ApexPages.addMessages(ex);
           caseSaved=false;
        }
            
    }
     
    //Method to cancel the transaction
    public Pagereference CancelAction(){
        Pagereference canPageRef=new Pagereference ('/apex/Cases_PSC_Site_MainPage');
        return canPageRef;
    }
    
    /* @Method <This Action Method will be called on page load for intializing attachment>
    @param <This method takes No parameter>
    @return void - <Not returning anything>
    @throws exception - <No Exception>
    */
        public void createInstance(){
        
    
        newAttachments=new List<Attachment>{new Attachment()};
        
        for (Integer i=0; i<attachmentsToAdd; i++)
        {
            system.debug('first attmt--->'+newAttachments);
            newAttachments.add(new Attachment());
            system.debug('first attmt after adding--->'+newAttachments);
   
        }
       
    }
    public PageReference saveAttachment()  {
         PageReference pageRef=null;
         try{
                List <Attachment> AttachmentsToInsert = new List <Attachment> (); 
                system.debug('-----caseSaved----' +caseSaved);
                if (caseSaved==true) {
                    
                    //clear assets those are in
                    for( Attachment caseAttachment: caseAttachments){
                        if(caseAttachment.body!=null  ){
                             //caseAttachment.id=null;
                             System.debug('caseObj.id '+cs.id);
                             System.debug('caseAttachment.parentId '+caseAttachment.parentId);
                             System.debug('caseAttachment.name'+caseAttachment.name);
                            if(caseAttachment.parentId == null ){
                                caseAttachment.parentId = cs.id;
                                AttachmentsToInsert.add(caseAttachment);
                            }
                        }
                    }
                    
                    if(AttachmentsToInsert.size()>0){
                        system.debug('AttachmentsToInsert '+AttachmentsToInsert);
                        insert AttachmentsToInsert;
                        caseAttachment = null;
                        AttachmentsToInsert=null;
                        caseAttachments = new List<Attachment>();
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                    }
                    //Case newCaseRecord =[select id,CaseNumber from Case where id = :cs.id];
                    if((ApexPages.getMessages()).size()>0 ) {
                        try{
                            List<Attachment> Att_List =[SELECT Id FROM Attachment where ParentId=:cs.id];
                            if(Att_List != null && Att_List.size()>0  ){
                                delete Att_List;
                            }
                        }catch(exception e){
                            system.debug(e);
                        }
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info,Label.SC_FileAttachHelpText));
                        pageRef = null;
                        caseId=cs.id;
                        caseSaved=true;
                        cs.id=null;
                        
                        // delete newCaseRecord;
                    }else{
                        Case newCaseRecord =[select id,CaseNumber from Case where id = :cs.id];
                        pageRef=new PageReference('/apex/PSCsuccessPage?caseNum='+newCaseRecord.CaseNumber+'&langForPage='+selectedLanguage+'&recordType='+recordTypeValue);
                        pageRef.setRedirect(true);    
                    }
                           
                }
               
                
         }catch(Exception e){
             ApexPages.addMessages(e);
             pageRef=null;
             System.debug(e);
         }
         return pageRef;
    }
}