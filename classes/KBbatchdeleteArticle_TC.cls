/*==================================================================================================================+                                                                          
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        ===========  
 |  Feb/13/2014    Bisna V P    CI 185     Test class for batch class, KBbatchdeleteArticle  
 |  Jul/10/2014    Bisna V P    CI 974  Updated code to cover update   
 |  Sep/15/2014    Bisna V P    CI 1263 Updated to cover class changes for WR 1263
+==================================================================================================================**/

@isTest(seeAllData= true)
Private Class KBbatchdeleteArticle_TC{

    static testMethod void archiveBatchAticles(){
      
      
      List<Id> idList = new List<Id>();
      String idListBatch ;
      List<ETA__kav> listETA = new  List<ETA__kav>();
      ETA__kav eta = new ETA__kav();  
            
        eta.Title = 'Test Class ETA';
        eta.UrlName = 'Test-Class-eta'; 
        eta.Language = 'en_US';
        eta.ValidationStatus = 'Work In Progress'; 
        eta.Audience__c =' Level 30 = Customers';        
        
        listETA.add(eta);
        
         ETA__kav eta1 = new ETA__kav();  
            
        eta1.Title = '1Test Class ETA';
        eta1.UrlName = '1Test-Class-eta'; 
        eta1.Language = 'en_US';
        eta1.ValidationStatus = 'Work In Progress'; 
        eta1.Audience__c =' Level 30 = Customers';        
        
        listETA.add(eta1);
        
        ETA__kav eta2 = new ETA__kav();  
            
        eta2.Title = '2Test Class ETA';
        eta2.UrlName = '2Test-Class-eta'; 
        eta2.Language = 'en_US';
        eta2.ValidationStatus = 'Work In Progress'; 
        eta2.Audience__c =' Level 30 = Customers';        
        
        listETA.add(eta2);
        
        insert listETA;
        
        idList.add(eta.id); 
        idList.add(eta1.id); 
        idList.add(eta2.id); 
                        
        Test.startTest();
        List<KnowledgeArticleVersion> listKav= [SELECT id,ArticleType, KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE  id in :idList];
        
        KbManagement.PublishingService.publishArticle(listKav[0].KnowledgeArticleId,true); 
        KbManagement.PublishingService.publishArticle(listKav[1].KnowledgeArticleId,true);
        KbManagement.PublishingService.publishArticle(listKav[2].KnowledgeArticleId,true);
                                       
        Article_Summary__c articlesummary= new Article_Summary__c();
        articlesummary.Article_Number__c='22222';
        articlesummary.Name= '22222';               
        insert articlesummary; 
        
        List<Linked_SR__c> linkedSR= new List<Linked_SR__c>();       
        
        Linked_SR__c ls= new Linked_SR__c();
        ls.Article_Number__c=listKav[2].ArticleNumber;
        ls.Article_Summary__c = articlesummary.Id;
        ls.Article_ID__c=listKav[2].KnowledgeArticleId;
        ls.Article_Version_ID__c=listKav[2].id;
        ls.SR_Number__c=1000000;
        linkedSR.add(ls);   
        
        Linked_SR__c ls1= new Linked_SR__c();
        ls1.Article_Number__c=listKav[2].ArticleNumber;
        ls1.Article_Summary__c = articlesummary.Id;
        ls1.Article_ID__c=listKav[2].KnowledgeArticleId;
        ls1.Article_Version_ID__c=listKav[2].id;
        ls1.SR_Number__c=0000000;
        linkedSR.add(ls1);   
        
        Linked_SR__c ls2= new Linked_SR__c();
        ls2.Article_Number__c=listKav[2].ArticleNumber;
        ls2.Article_Summary__c = articlesummary.Id;
        ls2.Article_ID__c=listKav[2].KnowledgeArticleId;
        ls2.Article_Version_ID__c=listKav[2].id;
        ls2.SR_Number__c=00050000;                   
        linkedSR.add(ls2);                          
        
        insert linkedSR; 
        
        //Changes for WR 1263
        Article_Rating__c artRate = new Article_Rating__c();
        artRate.Article_Number__c = listKav[2].ArticleNumber;
        artRate.Raters_Email_Address__c = 'aa.aa@aa.aa';
        
        insert artRate;
        //Changes end for WR 1263
        
                       
        KbManagement.PublishingService.editOnlineArticle (listKav[1].KnowledgeArticleId,true);          
        /*
        idListBatch=listKav[0].ArticleNumber+','+listKav[1].ArticleNumber+','+listKav[2].ArticleNumber;
        */   
        List<KB_Batch_Delete_Article__c> listKbd =  new List<KB_Batch_Delete_Article__c>();
        
        KB_Batch_Delete_Article__c kbd =  new KB_Batch_Delete_Article__c();
        kbd.Article_Number__c = Decimal.ValueOf(listKav[0].ArticleNumber);
        kbd.Status__c = 'Waiting for archive';
        listKbd.add(kbd);
        
        KB_Batch_Delete_Article__c kbd1 =  new KB_Batch_Delete_Article__c();
        kbd1.Article_Number__c = Decimal.ValueOf(listKav[1].ArticleNumber);
        kbd1.Status__c = 'Waiting for archive';
        listKbd.add(kbd1);
        
        KB_Batch_Delete_Article__c kbd2 =  new KB_Batch_Delete_Article__c();
        kbd2.Article_Number__c = Decimal.ValueOf(listKav[2].ArticleNumber);
        kbd2.Status__c = 'Waiting for deletion';
        listKbd.add(kbd2);
        
        insert listKbd;
        
        Map<String,CustomSettingDataValueMap__c> DataValueMap= CustomSettingDataValueMap__c.getall();
         System.debug(  'aaaaaaa---->'+DataValueMap.get('KBbatchdeleteArticleBatchSize')); 
        if(DataValueMap.get('KBbatchdeleteArticleBatchSize')==null)
        {
        List<CustomSettingDataValueMap__c>  csDataList =  new List<CustomSettingDataValueMap__c>();
        csDataList.add(new CustomSettingDataValueMap__c(Name = 'KBbatchdeleteArticleBatchSize', DataValue__c = '100')); 
        insert csDataList;
        }
         
                
        KBbatchdeleteArticle k= new KBbatchdeleteArticle();
        /*Database.executeBatch(k);
        // updated for CI WR#974
        KBbatchdeleteArticleScheduler myClass = new KBbatchdeleteArticleScheduler();   
         String chron = '0 0 23 * * ?';        
         system.schedule('Test Sched', chron, myClass);
        */
        Test.stopTest();
        
    }

}