@isTest
public class PRM_BPPScorecardDrillDownCtrl_TC{
    
    public static testMethod void testPRM_BPPScorecardDrillDownCtrl(){
     
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser)
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();  
            CustomSettingDataHelper.federationTierData(); 
            CustomSettingDataHelper.BusinessPartnerProgramData();
        }
        
        PageReference pageref =Page.PRM_BPPScorecardDrillDown;
        Test.setCurrentPage(pageref);
    
        /*Creating account record*/
        List<Account> objAccount = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        insert objAccount;
       
        /*Creation of Grouping record */ 
        Account_Groupings__c grouping = new Account_Groupings__c (Name = 'UNITTESTGrp',Profiled_Account__c=objAccount[0].Id);
        insert grouping;
        
        Contact con = new Contact();
        con.AccountId=objAccount[0].Id;
        con.Email='test23@emc.com';
        con.LastName = 'testContact';
        insert con;
                
        Map<String,Schema.RecordTypeInfo> recordTypes1 = Velocity_Rules__c.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id veloRecordTypeId = recordTypes1.get('Tier Rule').getRecordTypeId();
       
        Velocity_Rules__c velocityRuleObj= new Velocity_Rules__c();
        velocityRuleObj.Display_Name__c='test data';
        velocityRuleObj.Required__c='2';
        velocityRuleObj.Evaluation_at__c='Country Grouping Level';
        velocityRuleObj.Cluster__c='Group A';
        velocityRuleObj.AND_OR__c='OR';
        velocityRuleObj.Sort__c=1;
        velocityRuleObj.Field_Name__c='Solution_Center_Implemented__c';
        velocityRuleObj.recordtypeid=veloRecordTypeId;
        velocityRuleObj.Bucket__c='Solution Provider Program';
        velocityRuleObj.Tier__c= 'Platinum';
       
        insert velocityRuleObj;
       
        Velocity_Rule_Results__c veloRuleResultObj=new Velocity_Rule_Results__c();
        veloRuleResultObj.Speciality_RuleID__c=  velocityRuleObj.Id; 
        veloRuleResultObj.AccountID__c=objAccount[0].Id;
        veloRuleResultObj.Grouping__c = grouping.Id;              
           
        insert veloRuleResultObj;
        Education_Master__c eduMaster = new Education_Master__c();
        eduMaster.ESBI_Type__c = 'ACCREDITATION_EXAMS';
        insert eduMaster ; 
        
        Education__c edu= new Education__c();
        edu.Contact__c= con.id;
        edu.Education_Master__c = eduMaster.id;
        insert edu; 
        
        FederationprogramTier__c fed= new FederationprogramTier__c();
        fed.Federation_Tier__c='Federation_Tier_Testing';
       
        Education_Groups__c eg = new Education_Groups__c();
        eg.Education_Group_Name__c=velocityRuleObj.Display_Name__c;
        eg.Mentoring__c = true;
        insert eg;
        
        Velocity_Rule_Member__c vrm = new Velocity_Rule_Member__c();
        vrm.Group__c = eg.id;
        vrm.Speciality_Rule__c = velocityRuleObj.Id;
        insert vrm;
        
        Education_Group_Member__c eduGM = new Education_Group_Member__c();
        eduGM.Education_Group__c = eg.id;
        eduGM.Education_Master__c = eduMaster.id ;
        insert eduGM ;
        
        Education_EducationGroup_Mapping__c eegmList = new Education_EducationGroup_Mapping__c();
        //eegmList.Education_Mentoring_Completed__c = 'Yes';
        eegmList.Education__c = edu.id;
        //eegmList.PartnerId__c = objAccount[0].Grouping__c;
        eegmList.Education_Group__c = eg.id;
        //eegmList.ContactId__c = 
        insert eegmList;
        
        Services__c Ser = new Services__c();
        Ser.Services_Program_Designation__c = 'Assembly';
        Ser.Partner_Account__c = objAccount[0].id;
        insert Ser;
        
        Test.startTest();
        
        pageref.getParameters().put('vrId',velocityRuleObj.id);
        pageref.getParameters().put('vrrId',veloRuleResultObj.id);
        pageref.getParameters().put('Id',objAccount[0].id);
        pageref.getParameters().put('selectedTier','Platinum');
         
        PRM_BPPScorecardDrillDownCtrl bpp = new PRM_BPPScorecardDrillDownCtrl();
        bpp.getEducationList();
        Test.stopTest();
    }
}