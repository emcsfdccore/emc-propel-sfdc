/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  17/04/2015      Garima Bansal   RMA May,15      This Class handles all Case RMA Tracking Statuses operation and value derivation. 
 +=======================================================================================================================*/

public Class CaseRMA_Operations{

     public static List <Case_RMA__c> caseRMA= new list<Case_RMA__c>();
     public static List <Case_RMA__c> caseRMA1= new list<Case_RMA__c>();
     public static Boolean chkFlag = False ;
     public static Id CurrentUser= userinfo.getUserId();
     public static Map<string,RMA_IRO_Public_Group__c> RmaIroPGrp= RMA_IRO_Public_Group__c.getAll();
     public static Map<string,RMA_Status_Values__c> rmaStatusMap= RMA_Status_Values__c.getAll();
     public static String rmaCrtnPnd=rmaStatusMap.get('RMA Creation Pending').RMA_Status__c;
     public static String InRecltn=rmaStatusMap.get('In Reconciliation').RMA_Status__c;
     public static String rslvd=rmaStatusMap.get('Resolved').RMA_Status__c;
     public static List<Group> RTTqueue =[select Id from Group where Name = 'RTT Queue' and Type = 'Queue'];
     public static List<Group> IROqueue =[select Id from Group where Name = 'IRO Queue' and Type = 'Queue'];
     public void updateOwner(map<id,Case_RMA__c> mapNewIdCaseRMA,map<id,Case_RMA__c> mapOldIdCaseRMA)
    {   
        caseRMA= [select id,ownerId,RMA_Case_Status__c from Case_RMA__c where id in : mapNewIdCaseRMA.keyset()];
   
       for(Case_RMA__c rmaObj : caseRMA){
   
           if (InRecltn!=null &&rmaObj.RMA_Case_Status__c== InRecltn && rslvd !=null && mapOldIdCaseRMA.get(rmaObj.id).RMA_Case_Status__c==rslvd)
           {
            rmaObj.ownerId=IROqueue[0].id;
            caseRMA1.add(rmaObj);
            chkFlag = true ;
           }
         }
        if(caseRMA1.size()>0){
            update caseRMA1;
            } 
    }
    
    public void createChildRecod( map<id,Case_RMA__c> mapCRMA)
    {
        list<Cases_RMA_Sla_Tracking__c> listRMASlaTrack = new list<Cases_RMA_Sla_Tracking__c>();
        listRMASlaTrack = [select id, Case_Duration__c ,Resolved__c, RMA_Case_End_Time__c , RMA_Case_Start_Date__c, RMA_Case_Status__c , Cases_RMA__c from Cases_RMA_Sla_Tracking__c where Cases_RMA__c in : mapCRMA.keyset() and RMA_Case_End_Time__c = null ];
        
        if(listRMASlaTrack.size()>0)
            {
             for(Cases_RMA_Sla_Tracking__c ST : listRMASlaTrack)
                {
                    ST.RMA_Case_End_Time__c = mapCRMA.get(ST.Cases_RMA__c).LastModifiedDate;
                }
            }
        for(Case_RMA__c CR : mapCRMA.values())
            {
                Cases_RMA_Sla_Tracking__c SlaTrack = new Cases_RMA_Sla_Tracking__c ();
                SlaTrack.Cases_RMA__c = CR.id;
                SlaTrack.RMA_Case_Status__c = CR.RMA_Case_Status__c; 
                SlaTrack.RMA_Case_Start_Date__c = CR.LastModifiedDate;
                listRMASlaTrack.add(SlaTrack);
            }
            
            upsert listRMASlaTrack ;
    
    }
    
    public void populateOwnerOnInsertion(map<id,Case_RMA__c> mapIdCaseRMA)
    {
         caseRMA= [select id,ownerId, RMA_Case_Status__c from Case_RMA__c where id in : mapIdCaseRMA.keyset()];
        
         set<ID> queueID = new set<ID>();
         queueID.add(IROqueue[0].id);
        
         set<id> grpMemberID = GetUserIdsFromGroup(queueID);

         for(Case_RMA__c rmaObj : caseRMA)
         {
         if(rmaCrtnPnd!=null&&(rmaObj.ownerId==IROqueue[0].id || grpMemberID.contains(rmaObj.ownerId) ) )
            {
                rmaObj.ownerId = RTTqueue[0].id ;
                rmaObj.RMA_Case_Status__c = rmaCrtnPnd ;
                caseRMA1.add(rmaObj);
                chkFlag = true ;
            }
        }
        
        if(caseRMA1.size()>0){
            update caseRMA1;
            } 
        
    }
    
    public static Set<id> GetUserIdsFromGroup(Set<Id> groupIds)
    {
        // store the results in a set so we don't get duplicates
        Set<Id> result=new Set<Id>();
        String userType = Schema.SObjectType.User.getKeyPrefix();
        String groupType = Schema.SObjectType.Group.getKeyPrefix();
        Set<Id> groupIdProxys = new Set<Id>();
        // Loop through all group members in a group
        for(GroupMember m : [Select Id, UserOrGroupId From GroupMember Where GroupId in : groupIds])
        {
            // If the user or group id is a user
            if(((String)m.UserOrGroupId).startsWith(userType))
            {
                result.add(m.UserOrGroupId);
            }
            // If the user or group id is a group
           else if (((String)m.UserOrGroupId).startsWith(groupType))
            {
                // Call this function again but pass in the group found within this group
                groupIdProxys.add(m.UserOrGroupId);
            } 
        }
        if(groupIdProxys.size() > 0)
        {
            result.addAll(GetUSerIdsFromGroup(groupIdProxys));
        } 
        return result;  
    }
    
    public void ownershipCheck (map<id,Case_RMA__c > newMap , map<id,Case_RMA__c >oldMap )
    {
        String str =RmaIroPGrp.get('IRO_Reps').Group_ID__c; 
        String Str2 = RmaIroPGrp.get('RTT_Specialist').Group_ID__c; 
        set<id> grpMemberID =new set<ID>();
        set<id> RTTgrpMemberID=new set<ID>();
        Set<ID> grpId = new set<ID>();
        Set<ID> grpId2 = new set<ID>();
        set<Id> grpMgr= new set<Id>();
        set<id> mgrGrpMemberID= new set<Id>();
        if(str!=null && Str2!=null){
        grpId.add(str);
        grpId2.add(str2);
        }
        grpMemberID=GetUserIdsFromGroup(grpId);
        system.debug('grpMemberID are:' + grpMemberID);
        RTTgrpMemberID= GetUserIdsFromGroup(grpId2);
        system.debug('RTTgrpMemberID are:+++' + RTTgrpMemberID);
        grpMgr.add(RmaIroPGrp.get('IRO_Managers').Group_ID__c);
        grpMgr.add(RmaIroPGrp.get('RTT_Managers').Group_ID__c);
        mgrGrpMemberID = GetUserIdsFromGroup(grpMgr);
        system.debug('mgrGrpMemberID--are:+++' + mgrGrpMemberID);
        List<Case_RMA__c> CsRmaList = new List <Case_RMA__c>();
        for(Case_RMA__c rmaObj : newMap.values())
        {
            system.debug('NewOwner++'+ rmaObj.ownerId);
            system.debug('OldOwner++'+oldMap.get(rmaObj.id).ownerId);
            system.debug('chkFlag++forQueueAutomatic++'+chkFlag);
            
            If(mgrGrpMemberID!=null && !mgrGrpMemberID.contains(CurrentUser) ){
         
               if(grpMemberID.contains(CurrentUser)&&!chkFlag &&( oldMap.get(rmaObj.id).ownerId==CurrentUser|| (oldMap.get(rmaObj.id).ownerId!=CurrentUser && rmaObj.ownerId !=CurrentUser )||
               (rmaObj.ownerId ==CurrentUser&& oldMap.get(rmaObj.id).ownerId!=IROqueue[0].id)))
                {
                    CsRmaList.add(rmaObj);
                    rmaObj.addError(system.label.IRO_Rep_OwnerValidate);
                    system.debug('IROOwnerCheckError++');
                }
           
              if(RTTgrpMemberID.contains(CurrentUser)&&!chkFlag && (oldMap.get(rmaObj.id).ownerId==CurrentUser||
              (oldMap.get(rmaObj.id).ownerId!=CurrentUser && rmaObj.ownerId !=CurrentUser )||
               (rmaObj.ownerId ==CurrentUser&& oldMap.get(rmaObj.id).ownerId!=RTTqueue[0].id)))
              {
                    CsRmaList.add(rmaObj);
                    rmaObj.addError(system.label.RMA_Rep_OwnerValidate);
                    system.debug('RTTOwnerCheckError++');
            
            }
           
         }      
        }
        
    }

}