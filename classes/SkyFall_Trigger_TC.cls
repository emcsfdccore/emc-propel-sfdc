/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE       DEVELOPER       WR       DESCRIPTION                                
|
   ====       =========       ==       ===========  
|  Dec 8, 2014  Mung Fan    SkyFall Innovation     Created 
+===========================================================================*/ 
@isTest(SeeAllData=false) 
private class SkyFall_Trigger_TC{

static testMethod void validateUpdate() {


Community cmty = [SELECT id FROM Community WHERE name = 'Enterprise Salesforce Solution'];

Idea idea = new Idea(Body='Test Body', Title='Test Title', status='New', CommunityId=cmty.id, Category='Platform Architect');
System.debug('Idea before inserting: ' + idea.Body);

insert idea;

idea = [SELECT status FROM Idea WHERE Id =:idea.Id];
idea.status = 'Present to Leadership';
update idea;

idea = [SELECT status FROM Idea WHERE Id =:idea.Id];
System.assertEquals('Present to Leadership', idea.status);
}
}