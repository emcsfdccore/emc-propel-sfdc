/* =====================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |
 | 23.12.2013      Jaypal Nimesh   Backward    Optimized class per best practices and improved code coverage.
                                    Arrow
  03/30/2014       validation Errorfix for March'14 release
 +=====================================================================================================================*/
@isTest
class TC_OpportunityBatch{

    static testmethod void test() {
    
        //User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1]; 
        
        //Creating custom setting in different context
        
        //System.runAs(insertUser){ 
        System.RunAs(new User(Id = Userinfo.getUserId())){
            CustomSettingDataHelper.adminConversionCSData();
            CustomSettingDataHelper.houseAccountCSData();
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            
            CustomSettingDataHelper.profilesCSData();
        }
        
        //Inserting Account & Opportunities
        List<Account> accList = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        insert accList;
        
        List<Opportunity> OppListToInsert = new List<Opportunity>();
        
        for(Integer i = 0 ; i < 10 ; i++){
            
            Opportunity Oppty = new Opportunity();
            Oppty.Name = 'TestOpp' + i;
            Oppty.AccountId = accList[0].Id;
            Oppty.Sales_Channel__c = 'Direct';
            Oppty.Sales_Force__c = 'EMC';
            Oppty.StageName = 'Pipeline';
            Oppty.Closed_Reason__c = 'Loss';
            Oppty.Close_Comments__c = 'Lost';
            Oppty.CloseDate =  system.today() + 30;
            Oppty.Sell_Relationship__c = 'Direct';
            Oppty.Quote_Version__c='v13';
            Oppty.Quote_Type__c='New';
            Oppty.CurrencyIsoCode = 'USD';
            Oppty.Approval_Date__c= system.today() ;
            Oppty.Expiration_Date__c = system.today() + 30;
            Oppty.Primary_Outsourcer_System_Integrator__c=accList[1].Id;
            Oppty.Primary_ISV_Infrastructure__c=accList[2].Id;
            
            OppListToInsert.add(Oppty);
        }
        
        insert OppListToInsert;

        System.RunAs(new User(Id = Userinfo.getUserId())){

            //Setting Opportunity Bypass to true to avoid validation error thrown when setting date to backward date.
             CustomSettingBypassLogic__c byPassCSData = CustomSettingBypassLogic__c.getOrgDefaults();

            byPassCSData.By_Pass_Opportunity_Validation_Rules__c = true;
            upsert byPassCSData;



        }
        
        List<OpportunityIntegration__c> houseAcct = OpportunityIntegration__c.getall().Values();
        
        //Updating opportunities closedate to a back date for coverage
        for(Integer i = 0 ; i < OppListToInsert.size() ; i++){
            
            if(i < 5){
                OppListToInsert[i].CloseDate =  system.today() - 30;
                OppListToInsert[i].OwnerId = houseAcct[0].House_Account_User__c;
                OppListToInsert[i].bypass_validation__c = true;
            }
        }
        
        update OppListToInsert;
        
        //Inserting Product needed to insert OLI.
        Product2 prod = new Product2(Name = 'Test Product',CurrencyIsoCode = 'USD',productCode = 'ABC', IsActive = true);
        insert prod;
        
        String standardPriceBookId = '01s70000000EkOZ';
        PricebookEntry pricebook = new PricebookEntry(Pricebook2Id = standardPriceBookId, Product2Id = prod.Id, UnitPrice = 10000,IsActive = true);
        insert pricebook; 
        
        List<OpportunityLineItem> opptyLineItemList= new List<OpportunityLineItem>();
        
        //Preparing OLI data
        for(Integer i = 0 ; i < 10 ; i++){
            
            OpportunityLineItem opptyLineItem = new OpportunityLineItem();
            opptyLineItem.OpportunityId=OppListToInsert[i].Id;
            opptyLineItem.Quantity=5;
            opptyLineItem.Quote_Amount__c=5000;
            opptyLineItem.PricebookEntryId=pricebook.Id;
            opptyLineItemList.add(opptyLineItem);
        }
        
        insert opptyLineItemList;
        
        //Creating Forecast map & Group product data
        Forecast_Mapping__c forecastMap = new Forecast_Mapping__c();
        
        forecastMap.Create_User_Assignment__c = true;
        forecastMap.Forecast_Group__c = 'Atmos';
        forecastMap.Default_Sales_Team_User__c = UserInfo.getUserId();
        
        insert forecastMap;
        
        Forecast_Group_Product__c fgProd = new Forecast_Group_Product__c();
        
        fgProd.Product__c = prod.Id;
        fgProd.Forecast_Mapping__c = forecastMap.Id;
        
        insert fgProd;
      
        String query = 'Select o.AccountId ,o.CloseDate, o.IsClosed, o.Id, o.Partner__c,o.Tier_2_Partner__c, o.Name, o.Opportunity_Owner__c ,o.OwnerId from Opportunity o where o.IsClosed = false order by createdDate DESC limit 10';
        //Added LIMIT by Jaypal Nimesh for Backward Arrow Project - 20 Dec 2013
        
        Test.StartTest();
        
        //Calling batch class
        OpportunityBatch opptyBatch = new OpportunityBatch(query);
        opptyBatch.Query = query;
        ID batchprocessid = Database.executeBatch(opptyBatch);
        
        Test.StopTest();
    }
}