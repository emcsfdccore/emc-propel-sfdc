/*===========================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER            WR         DESCRIPTION                               
 | 17-Feb-2015   Vinod Jetti           #1649      replaced fields 'Site_DUNS_Entity__c,EMC_Classification__c' to Site_DUNS_Entity_Hub__c,EMC_Classification_Hub__c
 +===========================================================================*/
public with sharing class AH_DuplicateInfo_WithSharing {
    
    // function to retrieve the count of accounts that match a specified DUNS
    public static Integer getDuplicateCount(String DUNS) {
        System.debug('Received DUNS: ' + DUNS);
        if(Test.IsRunningTest()) {
            return [select count() from Account where Site_DUNS_Entity_Hub__c = :DUNS limit 200];
        }else{
            return [select count() from Account where Site_DUNS_Entity_Hub__c = :DUNS];
        }
    }

    // function to retrieve the accounts that match a specified DUNS
    public static List <Account> getDuplicates(String DUNS) {
        System.debug('Received DUNS: ' + DUNS);
        return [select Id, Name, Party_ID__c, Party_Number__c, EMC_Classification_Hub__c,
            BillingStreet, BillingCity, BillingState, BillingPostalCode from Account where Site_DUNS_Entity_Hub__c = :DUNS];
    }
 }