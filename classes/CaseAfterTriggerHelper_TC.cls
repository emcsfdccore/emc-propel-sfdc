@istest
public class CaseAfterTriggerHelper_TC {
    
    static testMethod void CaseInitialCompetitorProductCreator(){
        
        System.runAs(new user(Id = UserInfo.getUserId())) 
            {    
                CustomSettingDataHelper.VCEStaticCSData(); 
        }
        Map<string, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosByname();
        String rType = rtMap.get(System.Label.CompetitiveIntelligence_RecordLabel).getRecordTypeId();
        
        CustomSettingDataValueMap__c cd = new CustomSettingDataValueMap__c ();
        cd.name='ClosedCaseStatus';
        insert cd;
        
        CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
        custset.By_Pass_Account_Triggers__c = true;
        custset.By_Pass_Account_Validation_Rules__c = true;
        custset.By_Pass_Opportunity_Triggers__c = true;
        custset.By_Pass_Opportunity_Validation_Rules__c = true;
        insert custset;       
        
        List<Competitor__c> comp_List = new List<Competitor__c>();
        
        Competitor__c competitor =new Competitor__c();
        competitor.active__c =true;
        competitor.name='test';
        insert competitor;
        comp_List.add(competitor);
        Competitor__c competitor1 =new Competitor__c();
        competitor1.active__c =true;
        competitor1.name='test comp';
        insert competitor1;  
        comp_List.add(competitor1);
        //insert comp_List;
        // System.Database.insert(comp_List);
        
        // CaseCompetitorProduct__c caseCompetitor
        Account acc = new Account();
        acc.Name = 'Test Acc';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.AccountId = acc.Id;
        opp.StageName = 'Pipeline';
        opp.CloseDate = System.Today();
        opp.Sales_Channel__c = 'Indirect';
        opp.Sales_Force__c = 'EMC'; 
        opp.Business_Need__c ='Archiving';
        insert opp;         
        
        
        case c = new case();            
        c.RecordTypeId= rType;
        c.Competitor_New__c=competitor.id;
        //c.Opportunity_Name__c=opp.Id;
        
        insert c;
        test.startTest();
        system.debug('The case information before update::'+c);
        //CaseAfterTriggerHelper.isInitialCompetitorUpdate = false;
        c.Opportunity_Name__c=opp.Id;
        c.Competitor_New__c=competitor1.id;
        update c;
        system.debug('The case information after update::'+c);
        test.stopTest();
        
        OpportunityCompetitorProduct__c oppcompprod = new OpportunityCompetitorProduct__c();
        oppcompprod.Auto_Create_Competitor_Asset__c = true;
        oppcompprod.Competitor_Not_Listed__c = 'test';
        //oppcompprod.Competitor__c = comp.id;
        oppcompprod.Opportunity__c = opp.id;
        //oppcompprod.Competitor_Product__c = comprod.id;
        //oppcomprod.Competitor_Lost_To__c = false;
        //oppcomprod.Competitive_One_Pager__c = 'test';
        //casecomprod.Is_Created_from_Case__c = true;   
        insert oppcompprod;   
        
        
    }
    
    static testMethod void CaseInitialCompetitorProductCreator_M2(){
        
        System.runAs(new user(Id = UserInfo.getUserId())) 
            {    
                CustomSettingDataHelper.VCEStaticCSData(); 
        }
        Map<string, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosByname();
        String rType = rtMap.get(System.Label.CompetitiveIntelligence_RecordLabel).getRecordTypeId();
        
        CustomSettingDataValueMap__c cd = new CustomSettingDataValueMap__c ();
        cd.name='ClosedCaseStatus';
        insert cd;
        
        CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
        custset.By_Pass_Account_Triggers__c = true;
        custset.By_Pass_Account_Validation_Rules__c = true;
        custset.By_Pass_Opportunity_Triggers__c = true;
        custset.By_Pass_Opportunity_Validation_Rules__c = true;
        insert custset;       
        
        List<Competitor__c> comp_List = new List<Competitor__c>();
        
        Competitor__c competitor =new Competitor__c();
        competitor.active__c =true;
        competitor.name='test';
        insert competitor;
        comp_List.add(competitor);
        Competitor__c competitor1 =new Competitor__c();
        competitor1.active__c =true;
        competitor1.name='test comp';
        insert competitor1;  
        comp_List.add(competitor1);
        //insert comp_List;
        // System.Database.insert(comp_List);
        
        // CaseCompetitorProduct__c caseCompetitor
        Account acc = new Account();
        acc.Name = 'Test Acc';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.AccountId = acc.Id;
        opp.StageName = 'Pipeline';
        opp.CloseDate = System.Today();
        opp.Sales_Channel__c = 'Indirect';
        opp.Sales_Force__c = 'EMC'; 
        opp.Business_Need__c ='Archiving';
        insert opp;         
        
        
        case c = new case();            
        c.RecordTypeId= rType;
        //c.Competitor_New__c=competitor.id;
        //c.Opportunity_Name__c=opp.Id;
        
        insert c;
        CaseAfterTriggerHelper.isCaseAfterTriggerRunning = false;
        CaseCompetitorProduct__c ccp1 = new CaseCompetitorProduct__c();
        ccp1.case__c = c.id;
        ccp1.competitor__c = c.Competitor_New__c;
        ccp1.Competitor_Product__c = c.Competitor_Product_New__c;
        ccp1.Is_Created_from_Case__c = true;
        insert ccp1;
        c.Competitor_New__c=competitor.id;
        CaseAfterTriggerHelper.isCaseAfterTriggerRunning = false;
        update c;
        test.startTest();
        system.debug('The case information before update::'+c);
        //CaseAfterTriggerHelper.isInitialCompetitorUpdate = false;
        c.Opportunity_Name__c=opp.Id;
        c.Competitor_New__c=competitor1.id;
        update c;
        system.debug('The case information after update::'+c);
        test.stopTest();
        
    }
    
    static testMethod void CaseInitialCompetitorProductCreator_M3(){
        System.runAs(new user(Id = UserInfo.getUserId())) 
            {    
                CustomSettingDataHelper.VCEStaticCSData(); 
        }
        
        Map<string, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosByname();
        String rType = rtMap.get(System.Label.CompetitiveIntelligence_RecordLabel).getRecordTypeId();
        
        CustomSettingDataValueMap__c cd = new CustomSettingDataValueMap__c ();
        cd.name='ClosedCaseStatus';
        insert cd;
        
        CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
        custset.By_Pass_Account_Triggers__c = true;
        custset.By_Pass_Account_Validation_Rules__c = true;
        custset.By_Pass_Opportunity_Triggers__c = true;
        custset.By_Pass_Opportunity_Validation_Rules__c = true;
        insert custset;       
        
        List<Competitor__c> comp_List = new List<Competitor__c>();
        
        Competitor__c competitor =new Competitor__c();
        competitor.active__c =true;
        competitor.name='test';
        insert competitor;
        comp_List.add(competitor);
        Competitor__c competitor1 =new Competitor__c();
        competitor1.active__c =true;
        competitor1.name='test comp';
        insert competitor1;  
        comp_List.add(competitor1);
        //insert comp_List;
        // System.Database.insert(comp_List);
        
        // CaseCompetitorProduct__c caseCompetitor
        Account acc = new Account();
        acc.Name = 'Test Acc';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.AccountId = acc.Id;
        opp.StageName = 'Pipeline';
        opp.CloseDate = System.Today();
        opp.Sales_Channel__c = 'Indirect';
        opp.Sales_Force__c = 'EMC'; 
        opp.Business_Need__c ='Archiving';
        insert opp;         
        
        
        case c = new case();            
        c.RecordTypeId= rType;
        //c.Competitor_New__c=competitor.id;
        c.Opportunity_Name__c=opp.Id;
        
        insert c;
        CaseAfterTriggerHelper.isCaseAfterTriggerRunning = false;
        CaseCompetitorProduct__c ccp1 = new CaseCompetitorProduct__c();
        ccp1.case__c = c.id;
        ccp1.competitor__c = c.Competitor_New__c;
        ccp1.Competitor_Product__c = c.Competitor_Product_New__c;
        ccp1.Is_Created_from_Case__c = true;
        insert ccp1;
        c.Competitor_New__c=competitor.id;
        CaseAfterTriggerHelper.isCaseAfterTriggerRunning = false;
        update c;
        test.startTest();
        system.debug('The case information before update::'+c);
        //CaseAfterTriggerHelper.isInitialCompetitorUpdate = false;
        c.Opportunity_Name__c=opp.Id;
        c.Competitor_New__c=competitor1.id;
        update c;
        system.debug('The case information after update::'+c);
        test.stopTest();
        
    }
    
}