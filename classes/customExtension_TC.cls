/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE           DEVELOPER          WR       DESCRIPTION                                
|
   ====           =========          ==       ===========  
|  21-07-2014     Akash Rastogi   CI WR 1093  Created test class for "customExtension" class
+===========================================================================*/ 
@isTest
public class customExtension_TC  {
   public static testmethod void unitTest() {
       Custom_Content__c cc = new Custom_Content__c();

       ApexPages.StandardController controller = new ApexPages.StandardController(cc);
       customExtension cExt = new customExtension(controller);
  }
 
}