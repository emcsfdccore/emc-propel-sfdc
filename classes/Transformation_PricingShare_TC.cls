/*==================================================================================================================+
 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 |  23/09/2012    Hemavathi N M   204033     This Test class is used for Pricing Requests Sharing for Accoount owner and parent child Sharing     
                      
 
 |   12 Dec 2013  Avinash K     319046      Fixed the "TRANSFER REQUIRES READ" error from the pricingShare test method
 +==================================================================================================================**/

 @isTest(SeeAllData=true) 
private class Transformation_PricingShare_TC {

 public Static List<Account> lstAccount = new List<Account>();
 public Static List<Opportunity> lstOpp= new List<Opportunity>();
 public Static List<Pricing_Requests__c> lstPricing = new List<Pricing_Requests__c>();
 public Static List<Pricing_Requests__c> lstPricingRecall = new List<Pricing_Requests__c>();
 public Static List<Pricing_Requests__c> lstPricingAcceptanceOwner = new List<Pricing_Requests__c>();
 public static Integer loopLimit= 4;
 public void createAccount(){
        List<User> lstUser = [Select id,Theater__c ,Division__c from user where IsActive = true and Theater__c in ('APJ') and Division__c  in ('Korea','UKI','ANZ','Greater China','India','Japan','South East Asia' ) and Division__c Not in ('N/A') limit :loopLimit];
        List<User> lstUser1 = [Select id,Theater__c ,Division__c from user where IsActive = true and Theater__c in ('EMEA') and Division__c  in ('France','UKI') and Division__c Not in ('N/A') limit :loopLimit];
         System.debug('lstUser1---->' + lstUser);
        for(Integer k=0;k<loopLimit;k++){
        Account accObj1 = new Account();
        accObj1.ownerId =lstUser[k].Id;
        if(k==1){
        accObj1.ownerId =lstUser1[k].Id;
        }
        accObj1.name = 'TestAccount'+ k;
        accObj1.BillingCountry ='United States';
        accObj1.BillingCity = 'EDWARDS AFB'; 
        lstAccount.add(accObj1);
        }
        
        Database.insert(lstAccount);
        lstAccount = [Select Id,Name,ownerId FROM Account Where id in: lstAccount];
        System.debug('lstAccount---->' + lstAccount);
      }//END METHOD - CreatecustomProfiledAccount
      
       //Method for Creation Of Opportunity
    public void createOpportunity(){
      List<User> lstUser = [Select id,Theater__c ,Division__c from user where IsActive = true and Theater__c in ('APJ','EMEA') and Division__c  in ('India','Japan') and Division__c Not in ('N/A') limit :loopLimit];
      List<User> lstUser1 = [Select id,Theater__c ,Division__c from user where IsActive = true and Theater__c in ('Americas') limit :loopLimit];
      List<User> lstUser2 = [Select id,Theater__c ,Division__c from user where IsActive = true and Theater__c in ('EMEA') and Division__c  in ('UKI') and Division__c Not in ('N/A') limit :loopLimit];
       
       System.debug('lstUser---->' + lstUser);
        for(Integer k=0;k<loopLimit;k++){
        Opportunity opp_Obj = new Opportunity();
          opp_Obj.AccountId =lstAccount[k].id;
           
          
          opp_Obj.Name = 'TEST'+ k;
          opp_Obj.StageName = 'Strong Upside';
          opp_Obj.CloseDate = Date.today()+10;
          opp_Obj.Sales_Force__c = 'EMC';
          opp_Obj.Quote_Cart_Number__c='123';
          opp_Obj.Quote_Type__c = 'DxP';  
          opp_Obj.Opportunity_Number__c = '0'+ k ;
          Opp_Obj.Sales_Channel__c = 'Direct';
          if(k==0){
          opp_Obj.Quote_Type__c = 'CxP'; 
           opp_Obj.Opportunity_Owner__c = lstUser[k].id;
           opp_Obj.ownerId=lstUser[k].id;
          }
          if(k==1){
            opp_Obj.Opportunity_Owner__c = lstUser[k].id;
            opp_Obj.ownerId=lstUser[k].id;
           }
           if(k==2){
            opp_Obj.Opportunity_Owner__c = lstUser1[k].id;
            opp_Obj.ownerId=lstUser1[k].id;
           }
           if(k==3){
            opp_Obj.Opportunity_Owner__c = lstUser2[k].id;
            opp_Obj.ownerId=lstUser2[k].id;
           }
           
          opp_Obj.Amount = 222.00;
          lstOpp.add(opp_Obj);
        }
        System.debug('lstOpp#######'+lstOpp);
       Database.insert(lstOpp);
     
       lstOpp =[Select ID,Name,Opportunity_Owner__c ,Quote_Type__c, Account_Owner_Theater__c ,Account_Owner_Division__c, Opportunity_Owner__r.Theater__c from Opportunity where id in:lstOpp ];
       System.debug('lstOpp--->' + lstOpp.size() + lstOpp);
        
        }

//Method for creation of Pricing Requests    
    public void createPricingRequest(){
    
    List<Pricing_Requests__c> lstPric = new List<Pricing_Requests__c>();
    //Select id from Opportunity
     for(Integer k=0;k<lstOpp.size();k++){
      Pricing_Requests__c priReq_Obj = new Pricing_Requests__c();
      priReq_Obj.Opportunity_Name__c = lstOpp[k].id;
      priReq_Obj.Price_Floor_Level__c = 'Floor 1';
      priReq_Obj.How_can_we_mask_actual_discount__c = 'TEST 1234';
      priReq_Obj.Approval_Status__c ='New';
      priReq_Obj.Recalled_Flag__c = false;
      priReq_Obj.APJ_Request_Escalation__c = false;
      priReq_Obj.Current_price_point_justification__c ='test';
      priReq_Obj.EMC_PRODUCTS__c='test';
     
      lstPric.add(priReq_Obj);
     }
     Database.insert(lstPric);
     
     lstPricing =[select id,Approval_Status__c,createdById,Tier2_Access__c,Tier_1_ApproverId__c,Tier_2_ApproverId__c ,Recalled_Flag__c,PR_Account_Owner_Divison__c,PR_Account_Owner_Theater__c,Opportunity_Name__r.Opportunity_Owner__c  from Pricing_Requests__c  where id in:lstPric];
     System.debug('lstPricing------>'+ lstPricing);
 } ///end of method Creation Of Pricing Requests 
  
  
  // Submit for Approval
  public void submitForApproval()
        {
          System.debug('####Inside--->');
            // Create an approval request for the Opportunity
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval automatically using Trigger');
            for(Pricing_Requests__c prObj:lstPricing){ 
            req1.setObjectId(prObj.id);
            Approval.ProcessResult result = Approval.process(req1);
            }
          //  req1.setNextApproverIds(new Id[] {opp.Next_Approver__c});
     
            // Submit the approval request for the Opportunity
            
            System.debug('#### Submitted--->');
     
        }
        //Get ProcessInstanceWorkItemId using SOQL
        public Id getWorkItemId(Id targetObjectId)
        {
            Id retVal = null;
     
            for(ProcessInstanceWorkitem workItem  : [Select p.Id from ProcessInstanceWorkitem p
                where p.ProcessInstance.TargetObjectId =: targetObjectId])
            {
                retVal  =  workItem.Id;
            }
     
            return retVal;
        }
   public void approveRecord()
        {
             System.debug('####Inside--- Approve REcord>');
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setComments('Approving request using Trigger');
            req.setAction('Approve');
            for(Pricing_Requests__c prObj:lstPricing){ 
            if(prObj.Recalled_Flag__c == false){
             Id workItemId = getWorkItemId(prObj.id);
             if(workItemId != null)
               {
                   req.setWorkitemId(workItemId);
                // Submit the request for approval
                Approval.ProcessResult result = Approval.process(req);
                
               }
            }
           }
            
            System.debug('#### Approved--- Approve REcord>');
        }

//Method to test Pricing Request Share
    static testMethod void pricingShare() { 
      Transformation_PricingShare_TC testObj = new Transformation_PricingShare_TC();
      Transformation_PricingShare_Class priceshrObj = new Transformation_PricingShare_Class();
      
        CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c(); 
        custset.By_Pass_Opportunity_Triggers__c = true;
        custset.SetupOwnerId = UserInfo.getUserId();
        insert custset;
      
      //Create Account and Opportunity      
        testObj.createAccount();
        testObj.createOpportunity();
                     
    
    List<Id> lstPrl = new List<Id>();
      
      List<User> lstUser1 = [Select id,Theater__c ,Division__c from user where IsActive = true and Theater__c in ('Americas')  limit 2];
      

      Map<String,CustomSettingDataValueMap__c> data =  CustomSettingDataValueMap__c.getall();    
      String strPricingProfile = data.get('Pricing Test Class Profile').DataValue__c;

//Avinash K - Added the condition "and Profile.Name = :strPricingProfile" in where clause of the query below
      List<User> lstUser = [Select id,Theater__c ,Division__c from user where IsActive = true and Theater__c in ('EMEA') and Division__c in ('Germany','France') and Division__c Not in ('N/A') and Profile.Name = :strPricingProfile limit 2];
      List<id> idList = new List<id>();
      // Create Pricing Request
      User usrData = [Select id from User where Id = :UserInfo.getUserId()];
     System.RunAs(usrData)
     {
      //  Test.startTest();
        testObj.createPricingRequest();
       //  Test.stopTest();
     }


    //  Test.startTest();
       
      // Update Oppty owner with acitve user
      System.debug('lstOpp size----->'+lstOpp.size());
  
        
        
    //Update PR with oppty name

    for(Integer j=0;j<lstPricing.size();j++){
           if(j==0){
           lstPricing[j].Opportunity_Name__c = lstOpp[3].id;
            lstPricing[j].Tier_1_ApproverId__c=lstUser1[1].Id;
            lstPricing[j].Tier_2_ApproverId__c=lstUser[1].Id;
           }
           if(j==1){
            lstPricing[j].Opportunity_Name__c = lstOpp[j].id;
            lstPricing[j].Tier_1_ApproverId__c=lstUser1[1].Id;
            lstPricing[j].Tier_2_ApproverId__c=lstUser[1].Id;
           }
           if(j==2){
            lstPricing[j].Opportunity_Name__c = lstOpp[j].id;
           lstPricing[j].Approval_Expiration_Date__c = Date.today();
            
           }
           if(j==3){
            lstPricing[j].Opportunity_Name__c = lstOpp[1].id;
            lstPricing[j].Tier_1_ApproverId__c=lstUser1[1].Id;
            lstPricing[j].Tier_2_ApproverId__c=lstUser[1].Id;
            
           }
         }
     
     DataBase.update(lstPricing);
     
     
    //Submitting for approval
     testObj.submitForApproval();
     

     for(Integer j=0;j<lstPricing.size();j++){
        if(j==0){
            lstPricing[j].Approval_Expiration_Date__c = Date.today(); 
           lstPricing[j].Recalled_Flag__c =true;
    
        }
           if(j==1){
            lstPricing[j].Approval_Expiration_Date__c = Date.today()+2; 
           lstPricing[j].APJ_Request_Escalation__c =true;
           lstPricing[j].Recalled_Flag__c =false;
        
          
           
           }
           if(j==2){
            lstPricing[j].Approval_Expiration_Date__c = Date.today()+1; 
           lstPricing[j].APJ_Request_Escalation__c =true;
           lstPricing[j].Recalled_Flag__c =true;

                   }
           if(j==3){
           lstPricing[j].APJ_Request_Escalation__c =true;
           lstPricing[j].Recalled_Flag__c =false;
           lstPricing[j].Tier2_Access__c = true;
           lstPricing[j].OwnerId = lstUser[0].id;
            
           }
         }
         DataBase.update(lstPricing);
     
    User usr = [Select id from User where Id = :UserInfo.getUserId()];
     System.RunAs(usr)

     {

        Test.startTest();

         testObj.approveRecord();

         Test.stopTest();

     }
    if(lstOpp.size() >0){
          lstOpp[1].Opportunity_Owner__c = lstUser[0].id;
          lstOpp[2].Opportunity_Owner__c = lstUser1[0].id;       
        }
        DataBase.update(lstOpp);
        System.debug('lstOpp--1-->' + lstOpp);
        
        
    /* if(lstOpp.size() >0){
          lstOpp[1].Quote_Cart_Number__c = '424535';
      }
     DataBase.update(lstOpp); */
     List<Pricing_Requests__c> lstPrice = [Select Id,Approval_Status__c ,Price_Floor_Level__c from Pricing_Requests__c where id in: lstPricing];
     if(lstPrice.size()>0){
         for(Integer j=0;j<lstPrice.size();j++){
            if(lstPrice[j].Approval_Status__c == 'Approved'){
                lstPrice[j].Price_Floor_Level__c = 'Floor 5';
                lstPrice[j].Tier_1_ApproverId__c=lstUser1[1].Id;
            lstPrice[j].Tier_2_ApproverId__c=lstUser[1].Id;
            
            }
         }
     }
     DataBase.update(lstPrice); 
     lstPricing.clear();
  priceshrObj.updateSalesChannel(lstOpp);
   } // end of method
   
   
 //Method to test Pricing Request Acceptance
    static testMethod void pricingAcceptance() { 
      Transformation_PricingShare_TC testObj = new Transformation_PricingShare_TC();
      Transformation_PricingShare_Class priceshrObj = new Transformation_PricingShare_Class();
       
        CustomSettingBypassLogic__c custset1 =  new CustomSettingBypassLogic__c(); 
        custset1.By_Pass_Opportunity_Triggers__c = true;
        custset1.SetupOwnerId = UserInfo.getUserId();
        insert custset1;
 
      //Create Account and Opportunity      
        testObj.createAccount();
        testObj.createOpportunity();
                     
          
     User usrData = [Select id from User where Id = :UserInfo.getUserId()];
     System.RunAs(usrData)
     {
       testObj.createPricingRequest();
       
     Test.startTest();
    
       User usrAcceptanceData = [Select id from User where Id = :UserInfo.getUserId()];
       List<User> lstUser = [Select id,Theater__c ,Division__c  from user where IsActive = true and Theater__c in ('APJ') and Division__c in ('Korea','UKI','ANZ','Greater China','India','Japan','South East Asia' ) and Division__c Not in ('N/A') limit 4];
       system.debug('lstUser ***'+lstUser);
       System.RunAs(usrAcceptanceData)
        {  
        //  testObj.submitForApproval();
          lstPricingAcceptanceOwner = [select id,ownerId ,Tier2_Access__c,Approval_Status__c,APJ_Request_Escalation__c from Pricing_Requests__c where id in:lstPricing];
         System.debug('lstPricingAcceptanceOwner------>'+lstPricingAcceptanceOwner);
          for(Integer j=0;j<lstPricingAcceptanceOwner.size();j++){
            lstPricingAcceptanceOwner[j].OwnerId = lstUser[0].id;
            lstPricingAcceptanceOwner[j].APJ_Request_Escalation__c = true;
            lstPricingAcceptanceOwner[j].Approval_Status__c = 'In Process';
            lstPricingAcceptanceOwner[j].Tier2_Access__c = true;
            lstPricingAcceptanceOwner[j].Tier_1_ApproverId__c=lstUser[1].Id;
            lstPricingAcceptanceOwner[j].Tier_2_ApproverId__c=lstUser[0].Id;
            }
           DataBase.update(lstPricingAcceptanceOwner);
        }
       Test.stopTest();

     }
  }//End Method pricingAcceptance
  
}//end of class