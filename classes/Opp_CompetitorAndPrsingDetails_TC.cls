@istest
public class Opp_CompetitorAndPrsingDetails_TC {
    
    static Testmethod void oppcompAndpricDet(){
        try{
            CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
            custset.By_Pass_Account_Triggers__c = true;
            custset.By_Pass_Account_Validation_Rules__c = true;
            custset.By_Pass_Opportunity_Triggers__c = true;
            custset.By_Pass_Opportunity_Validation_Rules__c = true;
            insert custset;
            
            Account acc = new Account();
            
            Competitor__c comp = new Competitor__c();
            comp.name='Test';
            comp.Active__c= true;
            
            //comp.Pricing_Calculator_Link__c = 'test';
            //comp.Competitive_One_Pager_Link__c = 'test';
            insert comp;
            
            Competitor_Product__c comprod = new Competitor_Product__c();
            comprod.name='ttest';
            comprod.Active__c = true;
            comprod.Competitor__c = comp.id;
            
            comprod.Competitive_One_Pager__c = 'test';
            comprod.Competitive_One_Pager__c='test';
            comprod.Pricing_Calculator__c = 'test';
            //comprod.CI_Wiki_Link__c = '';
            insert comprod;
            
            Opportunity opp = new Opportunity();
            opp.name='test';
            opp.Closed_Reason__c = 'Project Cancelled';
            opp.Competitor__c = 'Actifio';
            opp.Close_Comments__c = 'Test';
            opp.Duplicate_Opportunity__c = opp.ID;
            opp.Competitor_Lost_To__c = 'Amazon';
            opp.Competitor_Product__c = '18000 Series';
            opp.Product_Model__c = '18800F';
            opp.Sales_Channel__c = 'Direct';
            opp.Renewals_Close_Details__c = '3rd Party';
            opp.Closed_Reason_Action__c = 'IB to De-Install';
            opp.Opportunity_Type__c = 'IB to De-Install';
            opp.AccountId= acc.id; 
            //opp.Account.name = acc.id;
            opp.stagename= 'pipeline';
            opp.closedate = system.today();
            
            insert opp;
            
            case cs = new case();
            cs.Competitor_New__c = comp.ID;
            cs.Competitor_Product_New__c = comprod.ID;
            cs.Opportunity_Name__c = opp.ID;
            insert cs;
            
            OpportunityCompetitorProduct__c oppcompprod = new OpportunityCompetitorProduct__c();
            oppcompprod.Auto_Create_Competitor_Asset__c = true;
            // oppcompprod.Competitor_Not_Listed__c = 'test';
            oppcompprod.Competitor__c = comp.id;
            oppcompprod.Opportunity__c = opp.id;
            oppcompprod.Competitor_Product__c = comprod.id;
            //oppcomprod.Competitor_Lost_To__c = false;
            //oppcomprod.Competitive_One_Pager__c = 'test';
            //casecomprod.Is_Created_from_Case__c = true;   
            insert oppcompprod;
            
            
            test.starttest();
            pagereference pageref = page.CaseCompetitorAndProductsPage;
            Test.setCurrentPage(pageRef);
            pageref.getParameters().put('delId', oppcompprod.id);
            
            ApexPages.StandardController std = new ApexPages.StandardController(opp) ;
            Opp_CompetitorAndPrsingDetails opprdet = new Opp_CompetitorAndPrsingDetails(std);
            opprdet.getOppty();
            opprdet.loadWrapperData();
            
            opprdet.addOppCompProd();
            opprdet.editOppCompProd();
            opprdet.saveOppCompProd();
            opprdet.createAsset(comp,oppcompprod);
            opprdet.oppCompCancel();
            opprdet.updateOppty(comp.name,comprod.Name,comprod.id);
            opprdet.updateCompetitorLost();
            opprdet.initialize();
            opprdet.deleteOppCompProd();
            
            test.stoptest();
        }
        catch(Exception ex){}
        
    }
    
    static Testmethod void oppcompAndpricDet_M2(){
        try{
            CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
            custset.By_Pass_Account_Triggers__c = true;
            custset.By_Pass_Account_Validation_Rules__c = true;
            custset.By_Pass_Opportunity_Triggers__c = true;
            custset.By_Pass_Opportunity_Validation_Rules__c = true;
            insert custset;
            
            Account acc = new Account();
            
            Competitor__c comp = new Competitor__c();
            comp.name='Test';
            comp.Active__c= true;
            
            //comp.Pricing_Calculator_Link__c = 'test';
            //comp.Competitive_One_Pager_Link__c = 'test';
            insert comp;
            
            Competitor_Product__c comprod = new Competitor_Product__c();
            comprod.name='ttest';
            comprod.Active__c = true;
            comprod.Competitor__c = comp.id;
            
            comprod.Competitive_One_Pager__c = 'test';
            comprod.Competitive_One_Pager__c='test';
            comprod.Pricing_Calculator__c = 'test';
            //comprod.CI_Wiki_Link__c = '';
            insert comprod;
            
            Opportunity opp = new Opportunity();
            opp.name='test';
            opp.Closed_Reason__c = 'Project Cancelled';
            opp.Competitor__c = 'Actifio';
            opp.Close_Comments__c = 'Test';
            opp.Duplicate_Opportunity__c = opp.ID;
            opp.Competitor_Lost_To__c = 'Amazon';
            opp.Competitor_Product__c = '18000 Series';
            opp.Product_Model__c = '18800F';
            opp.Sales_Channel__c = 'Direct';
            opp.Renewals_Close_Details__c = '3rd Party';
            opp.Closed_Reason_Action__c = 'IB to De-Install';
            opp.Opportunity_Type__c = 'IB to De-Install';
            opp.AccountId= acc.id; 
            //opp.Account.name = acc.id;
            opp.stagename= 'pipeline';
            opp.closedate = system.today();
            
            insert opp;
            
            case cs = new case();
            cs.Competitor_New__c = comp.ID;
            cs.Competitor_Product_New__c = comprod.ID;
            cs.Opportunity_Name__c = opp.ID;
            insert cs;
            
            OpportunityCompetitorProduct__c oppcompprod = new OpportunityCompetitorProduct__c();
            oppcompprod.Auto_Create_Competitor_Asset__c = true;
            // oppcompprod.Competitor_Not_Listed__c = 'test';
            oppcompprod.Competitor__c = comp.id;
            oppcompprod.Opportunity__c = opp.id;
            oppcompprod.Competitor_Product__c = comprod.id;
            //oppcomprod.Competitor_Lost_To__c = false;
            //oppcomprod.Competitive_One_Pager__c = 'test';
            //casecomprod.Is_Created_from_Case__c = true;   
            insert oppcompprod;
            
            OpportunityCompetitorProduct__c oppcompprod2 = new OpportunityCompetitorProduct__c();
            oppcompprod2.Auto_Create_Competitor_Asset__c = true;
            // oppcompprod2.Competitor_Not_Listed__c = 'test';
            oppcompprod2.Competitor__c = comp.id;
            oppcompprod2.Opportunity__c = opp.id;
            oppcompprod2.Competitor_Product__c = comprod.id;
            //oppcompprod2.Competitor_Lost_To__c = false;
            //oppcompprod2.Competitive_One_Pager__c = 'test';
            //oppcompprod2.Is_Created_from_Case__c = true;   
            insert oppcompprod2;
            
            
            test.starttest();
            pagereference pageref = page.CaseCompetitorAndProductsPage;
            Test.setCurrentPage(pageRef);
            pageref.getParameters().put('delId', oppcompprod2.id);
            
            ApexPages.StandardController std = new ApexPages.StandardController(opp) ;
            Opp_CompetitorAndPrsingDetails opprdet = new Opp_CompetitorAndPrsingDetails(std);
            opprdet.getOppty();
            opprdet.loadWrapperData();
            opprdet.updateCompetitorLost();
            opprdet.initialize();
            opprdet.deleteOppCompProd();
            
            test.stoptest();
        }catch(Exception ex){}
        
    }    
    
    /*
static Testmethod void oppcompAndpricDet1(){

CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
custset.By_Pass_Account_Triggers__c = true;
custset.By_Pass_Account_Validation_Rules__c = true;
custset.By_Pass_Opportunity_Triggers__c = true;
custset.By_Pass_Opportunity_Validation_Rules__c = true;
insert custset;

Account acc = new Account();

Competitor__c comp = new Competitor__c();
comp.name='abc';
comp.Active__c= true;
insert comp;

Competitor_Product__c comprod = new Competitor_Product__c();
comprod.name='ttest';
comprod.Active__c = true;
comprod.Competitor__c = comp.id;

comprod.Competitive_One_Pager__c = 'test';
comprod.Competitive_One_Pager__c='test';
comprod.Pricing_Calculator__c = 'test';
//comprod.CI_Wiki_Link__c = '';

comp.name='NotListed';
update comp;

Opportunity opp = new Opportunity();
opp.name='test';
opp.Closed_Reason__c = 'Project Cancelled';
opp.Competitor__c = 'Actifio';
opp.Close_Comments__c = 'Test';
opp.Duplicate_Opportunity__c = opp.ID;
opp.Competitor_Lost_To__c = 'Amazon';
opp.Competitor_Product__c = '18000 Series';
opp.Product_Model__c = '18800F';
opp.Sales_Channel__c = 'Direct';
opp.Renewals_Close_Details__c = '3rd Party';
opp.Closed_Reason_Action__c = 'IB to De-Install';
opp.Opportunity_Type__c = 'IB to De-Install';
opp.AccountId= acc.id; 
//opp.Account.name = acc.id;
opp.stagename= 'pipeline';
opp.closedate = system.today();

insert opp;

case cs = new case();
cs.Competitor_New__c = comp.ID;
cs.Competitor_Product_New__c = comprod.ID;
cs.Opportunity_Name__c = opp.ID;
insert cs;

OpportunityCompetitorProduct__c oppcompprod = new OpportunityCompetitorProduct__c();
oppcompprod.Auto_Create_Competitor_Asset__c = true;
oppcompprod.Competitor_Not_Listed__c = 'test';
oppcompprod.Competitor__c = comp.id;
oppcompprod.Opportunity__c = opp.id;
oppcompprod.Competitor_Product__c = comprod.id;
//oppcomprod.Competitor_Lost_To__c = false;
//oppcomprod.Competitive_One_Pager__c = 'test';
//casecomprod.Is_Created_from_Case__c = true;   
insert oppcompprod;

test.starttest();
ApexPages.StandardController std = new ApexPages.StandardController(opp) ;
Opp_CompetitorAndPrsingDetails opprdet = new Opp_CompetitorAndPrsingDetails(std);
opprdet.getOppty();
opprdet.loadWrapperData();
opprdet.deleteOppCompProd();
//opprdet.saveOppCompProd();

test.stoptest();
}
static Testmethod void oppcompAndpricDet2(){

Account acc = new Account();

Competitor__c comp = new Competitor__c();

Competitor_Product__c comprod = new Competitor_Product__c();
comprod.name='ttest';
comprod.Active__c = true;
comprod.Competitor__c = comp.id;

comprod.Competitive_One_Pager__c = 'test';
comprod.Competitive_One_Pager__c='test';
comprod.Pricing_Calculator__c = 'test';
//comprod.CI_Wiki_Link__c = '';
insert comprod;

Opportunity opp = new Opportunity();

OpportunityCompetitorProduct__c oppcompprod = new OpportunityCompetitorProduct__c();
oppcompprod.Auto_Create_Competitor_Asset__c = true;
oppcompprod.Competitor_Not_Listed__c = 'test';
oppcompprod.Competitor__c = comp.id;
oppcompprod.Opportunity__c = opp.id;
oppcompprod.Competitor_Product__c = comprod.id;
//oppcomprod.Competitor_Lost_To__c = false;
//oppcomprod.Competitive_One_Pager__c = 'test';
//casecomprod.Is_Created_from_Case__c = true;   
insert oppcompprod;

test.starttest();
ApexPages.StandardController std = new ApexPages.StandardController(opp) ;
Opp_CompetitorAndPrsingDetails opprdet = new Opp_CompetitorAndPrsingDetails(std);
opprdet.getOppty();
opprdet.loadWrapperData();
//opprdet.deleteOppCompProd();
opprdet.saveOppCompProd();

test.stoptest();
}
*/
}