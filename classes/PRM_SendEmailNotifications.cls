/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER          WR       DESCRIPTION                               

 |  ====          =========          ==       =========== 

 |  15/11/2010    Ashwini Gowda               This class will would send Notification to 
                                              Designated List of users for the selected
                                              Template.
    24/11/2010    Suman B                     Change the method signature of sendEmailNotification() 
                                               for userList from List<Id> to List<User>.  
   23-Dec-2013     Prasad Kothawade           Updated to cover 100 To email addresses            
                                     
 +==================================================================================================================**/

public class PRM_SendEmailNotifications {
    /* @Method <sendEmailNotification(): This is method would send Notification to the
                                          Designated List of users for the selected
                                          Template.>
       @param - <This method will take List of Users along with the Template Name as arguments>
       @return <void> - <returning nothing>
       @throws exception - <No Exception>
    */ 
     public void sendEmailNotification(List<User> usersList,String emailTemplateName){
               
        List<String> toAddresses = new List<String>();
    /** List<User> usersEmailIdLlist = [Select u.Email, u.Id 
                                        from User u
                                        where Id in:usersList]; **/
        
        List<List<String>> EmailRecepeints= new List<List<String>>();
        List<String> UserList =new List<String>();
        
        integer count=1;                                         
        for(User user : usersList){
              // String stremail=user.email;
               //stremail.replace('emc.com.sfdc02','@emc1.com');
               
               UserList.add(user.email);
               system.debug('mod(count,100) '+ Math.mod(count,100));
               if( math.mod(count,100)==0){
                   EmailRecepeints.add(UserList);
                   UserList=new List<String>();
               }
               
            count=count+1;
        }
        if(UserList.size()<100){
            EmailRecepeints.add(UserList);
        }
       List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
       EmailTemplate Template = [Select e.Body,e.Name, e.Subject 
                                      From EmailTemplate e
                                      where name =:emailTemplateName];   
        
         System.debug('EmailRecepeints.size '+EmailRecepeints.size());                              
        for( List<String> toemailAddresses :EmailRecepeints){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
            System.debug('toemailAddresses  '+toemailAddresses );
            mail.setToAddresses(toemailAddresses );
            mail.setSubject(Template.Subject);
            mail.setPlainTextBody(Template.body); 
           // mail.setCcAddresses(new String[]{'prasad.s.kothawade@accenture.com'});
            emails.add(mail); 
            
        }
          System.debug('emails.size() '+emails.size() );
        Messaging.sendEmail(emails );      //new Messaging.SingleEmailMessage[]{ mail }                                
     }
}