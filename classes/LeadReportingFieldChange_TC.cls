/*==============================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER                WR       DESCRIPTION                               
 |  ====          =========                ==       =========== 
 | 5 May 2013   Prachi Bhasin           246408      Creation
 | 15 Apr 2014  Sayan Choudhury         CI 389      Added method to increase the coverage for the class
 | 11/6/2014    Bindu                   CI 1450     Checking if initial lead level and initial owner fields are populated
 +==============================================================================================================*/
@IsTest (SeeAllData=true)
private class LeadReportingFieldChange_TC {
        static testmethod void test() {       
                List <String> profileNames = new List <String> {'AMER Inside Sales/SMB User','APJ Core TC'};
                List <Profile> p =[Select id from Profile where Profile.Name IN :profileNames ];
                User usr = new User();
                usr.Username = 'username@xyz.com';
                usr.Email = 'emai@xyz.com';
                usr.CommunityNickname = 'communityNickname';
                usr.Emailencodingkey='UTF-8';
                usr.Languagelocalekey='en_US';
                usr.Localesidkey='en_US';
                usr.Timezonesidkey='America/Los_Angeles';
                usr.LastName='lastname1';
                usr.Alias='alias1';
                usr.ProfileId =p[0].id;
                insert usr;
     
                 
                User usr1 = new User();
                usr1.Username = 'username1@xyz.com';
                usr1.Email = 'emai1@xyz.com';
                usr1.CommunityNickname = 'communityNickname1';
                usr1.Emailencodingkey='UTF-8';
                usr1.Languagelocalekey='en_US';
                usr1.Localesidkey='en_US';
                usr1.Timezonesidkey='America/Los_Angeles';
                usr1.LastName='lastname11';
                usr1.Alias='alias11';
                usr1.ProfileId =p[1].id; 
                insert usr1;
               
                System.RunAs(usr){
                Test.startTest();
                Lead objLead = new Lead();
                objLead.Company = 'Company 1';
                objLead.LastName= 'Name 1';
                objLead.Sales_Force__c = 'EMC';
                objLead.Lead_Rank__c = 'NPQ';
                objLead.Lead_Originator__c = 'Program';
                objLead.OwnerId = usr.Id;
                objLead.Originator_Details__c='details1';
                //objLead.Initial_Lead_Level__c=objLead.Lead_Rank__c;
                //objLead.Initial_Lead_Owner__c=objLead.OwnerId;
                insert objLead;
                //CI WR 1450
                Lead ld=[select Initial_Lead_Level__c,Initial_Lead_Owner__c from Lead where Id= :objLead.Id];
                //system.assertEquals(ld.Initial_Lead_Level__c,'NPQ');
                //system.assertEquals(ld.Initial_Lead_Owner__c,usr.Id);
                //CI WR 1450
                Map<Id,Lead> leadNewRecordsMap = new Map<Id,Lead>();
                Map<Id,Lead> leadOldRecordsMap = new Map<Id,Lead>(); 
                List<Id> newOwnerId = new List<Id>();
                List<Id> oldOwnerId = new List<Id>();
                
                leadOldRecordsMap.put(objLead.id,objLead);
                oldOwnerId.add(usr.Id);
                                
                objLead.OwnerId = usr1.Id;
                update objLead;
                newOwnerId.add(usr1.Id);
                leadNewRecordsMap.put(objLead.id,objLead); 
                Test.stopTest();              
                           }      
        
        }  
        //Test method added for CI WR 389//
         static testmethod void test1(){
    map<id,Lead> Map_Oldlead = new map<id,Lead>();
    boolean triggercondition = false;
     list<Account> accList = new list<Account>();
     List<Id> idList= new List<Id>();
    Account acc= new Account();
    acc.Name = 'abcd';
    acc.PROFILED_ACCOUNT_FLAG__c = true;
    acc.Type = 'Partner';
    acc.Status__c = 'A';
   accList.add(acc);
    Account acc1= new Account();
    acc1.Name = 'abcd1';
    acc1.PROFILED_ACCOUNT_FLAG__c = true;
    acc1.Type = 'Partner';
    acc1.Status__c = 'A';
     accList.add(acc1);  
     insert accList;
     idList.add(acc.id);
     idList.add(acc1.id);
    list<Lead> leadList = new list<Lead>(); 
    Lead lead1 = new lead(
        Company =  'Test Company', Status = 'New', LastName = 'Last Name',
                  Sales_Force__c = 'EMC', Lead_Originator__c = 'eStore', LeadSource = 'Manual', Originator_Details__c='RFQ',eBus_Requested_Partner__c=acc1.id);     
    leadList .add(lead1); 
    Lead lead2 = new lead(
        Company =  'Test Company', Status = 'New', LastName = 'Last Name1',
                  Sales_Force__c = 'EMC', Lead_Originator__c = 'eStore', LeadSource = 'Manual', Originator_Details__c='RFQ',eBus_Requested_Partner_Write_In__c='kahsdfkh');     
     leadList .add(lead2);   
     insert leadList ;      
     Map_Oldlead.put(lead1.id,lead1);
    Map_Oldlead.put(lead2.id,lead2);
     test.startTest();
     
     LeadReportingFieldChange ebizReqPar = new LeadReportingFieldChange();
     ebizReqPar.Ebusinessreqparinsert(leadList,Map_Oldlead,triggercondition);
               Test.stoptest();
     }    
     //END of test method for CI WR 389//    
        
        }