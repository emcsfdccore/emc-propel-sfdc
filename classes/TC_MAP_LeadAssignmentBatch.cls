/*=====================================================================================================+
 |  HISTORY  |
 |  DATE          DEVELOPER               WR         DESCRIPTION 
 |  ====          =========               ==         =========== 
 |
 | 30/12/2013   Jaypal Nimesh      Backward Arrow  Optimized class and improved code coverage.
 +=====================================================================================================*/

@isTest
Private class TC_MAP_LeadAssignmentBatch{

    static testmethod void test() {
    
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
            
        //Inserting custom setting data in different context
        System.runAs(insertUser)
        {   
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.adminConversionCSData();
            CustomSettingDataHelper.houseAccountCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.mapProfileOptCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        }
              
        //Creating accounts with specific data
        List<Account> lstAccount = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();    
        List<Account> ListAccPartner =  AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        List<Account> AccountList = new List<Account>();
        List<Account> lstDistributorAccount = new List<Account>();
        
        AccountList.addAll(ListAccPartner);
        AccountList.addAll(lstAccount);
        
        //Inserting Accounts
        insert AccountList;
        
        for(account distiacc: AccountList){
            if(distiacc.Partner_Type__c=='Distributor'&& distiacc.Lead_Oppty_Enabled__c==true){
               distiacc.Ispartner=true;
            }
            lstDistributorAccount.add(distiacc);
        }
        
        Update lstDistributorAccount;       
        
        //Calling helper class to create Lead data
        List<Lead> leadlist=LeadTestClassDataHelper.createDealReg(AccountList[4], null, lstDistributorAccount[0], AccountList[3]);
        
        insert leadlist;
        
        for(Lead lead : leadlist){
            
            lead.OwnerId = MAP_LeadAssignmentUtils.getProfileOptionValue('Routing_Batch_Queue_Id');
            lead.BU__c = 'IIG';
        }
        //updating leads with specific data
        update leadlist;
        
        //Preparind Lead Routing field Mapping object data
        List<Lead_Routing_Field_Mapping__c> LRFMlist= new List<Lead_Routing_Field_Mapping__c>();
        
        LRFMlist.add(new Lead_Routing_Field_Mapping__c (Rule_Object_Field_API_Name__c ='BU__c', Lead_Object_Field_API_Name__c = 'BU__c', Comparison_Operator__c = 'Equals'));
        insert LRFMlist;
    
        Test.startTest();
        
        //Creating instance of batch and assigning variables
        MAP_LeadAssignmentBatch LeadAssignment = new MAP_LeadAssignmentBatch ();
        
        LeadAssignment.ruleFieldToLeadFieldMap = MAP_LeadAssignmentUtils.getRuleFieldToLeadFieldMappings();
        LeadAssignment.ruleFieldToOperatorMap = MAP_LeadAssignmentUtils.getRuleFieldToOperatorMappings();
        
        LeadAssignment.batchQuery = 'SELECT Name, Related_Account__c, ownerid, Routing_Exception_Code__c, ';        
        
        //Adding field attributes in query
        List<string> leadAttributes = MAP_LeadAssignmentUtils.getLeadAttributes();
        Iterator<string> iter = leadAttributes.iterator();
        String attrName = null;

        do{
            attrName = iter.next();            
            LeadAssignment.batchQuery += attrName;            
            if(iter.hasNext()){                
                LeadAssignment.batchQuery += ',';            
            }
        }
        while(iter.hasNext());  
    
        LeadAssignment.batchQuery += ' FROM Lead WHERE ownerId = \'' + MAP_LeadAssignmentUtils.getProfileOptionValue('Routing_Batch_Queue_Id') + '\'';        
        LeadAssignment.batchQuery += ' LIMIT 10';
        //Added LIMIT by Jaypal Nimesh for Backward Arrow Project - 17 Dec 2013
        
        //Executing batch
        ID batchprocessid = Database.executeBatch(LeadAssignment);

        Test.stopTest();
    }
}