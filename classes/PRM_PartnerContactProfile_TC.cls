@isTest
/*=========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  14/12/2011      Anil                        Removed Query for fetching Account and contacts and used Custom setting Data Helper
 |  19-Feb-2015     Vinod Jetti     #1649       created Hub_Info__c objects data instead of Account 'Site_Duns_Entity__c' field 
 +=========================================================================================================================*/
Private Class PRM_PartnerContactProfile_TC{
     
    Private static testMethod void startTest(){
        
        System.runAs(new User(Id = UserInfo.getUserId())){
        
        CustomSettingDataHelper.dataValueMapCSData();
        CustomSettingDataHelper.eBizSFDCIntCSData();
        CustomSettingDataHelper.dealRegistrationCSData();
        CustomSettingDataHelper.bypassLogicCSData();
        CustomSettingDataHelper.countryTheaterMapCSData();
   
        Profile p = [select id from profile where name='System Administrator' limit 1];
    
    User sysAdminUser = new User(alias = 'utest', email='testSample1@test.com',
                      emailencodingkey='UTF-8', lastname='Unit Test', 
                      languagelocalekey='en_US',
                      localesidkey='en_GB', profileid = p.Id,
                      timezonesidkey='Europe/London', 
                      username='sysAdminTest@emc.com');
                      
    insert sysAdminUser;
    
    User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
     User user01;   
        
        
        /*System.runAs(insertUser){
        PRM_VPP_JobDataHelper.createVPPCustomSettingData();
        }*/
        System.runAs(insertUser){
         Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
         String distributorSuperUser = data.get('EMEA Distributor Super User').DataValue__c ;
         user01 = createUser(distributorSuperUser);
        } 
        
        System.runAs(sysAdminUser){
       
        List<Account> accList = new List<Account>();
        accList = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();       
        insert accList;
        
        for(Account accounts : accList){
            accounts.IsPartner=true;
            accounts.PROFILED_ACCOUNT_FLAG__c =true;
            accounts.Cluster__c='APJ 2&3';
            accounts.Hub_Info__c = accList[0].Hub_Info__c;
            //accounts.Site_DUNS_Entity__c = '234561234';
            
        }
        update accList;
        
       
        
        Contact contact = UserProfileTestClassDataHelper.createContact();
        contact.email = 'test12456@emc.com';
        insert contact;
        contact.AccountId = accList[0].id;
        update contact; 
        
       }
        
        
         Test.startTest();
         System.runAs(user01){
         PRM_PartnerContactProfile cls = new PRM_PartnerContactProfile();
         cls.init();
         cls.contact.email ='asd@asd.com';
         cls.contact.Language_Preference__c ='English';       
         cls.saveAndReturnToHome();
         cls.getLanguage_PreferenceList();
         cls.getPrimary_Program_Contact_for_List();
         cls.getPreferred_Contact_MethodList();
         cls.getFunctional_AreaList();
      }
         Test.stopTest();
      
    }
}   
    private static User createUser(String Profilename){          

        Profile distributorSuperUserProfile = [select Id from Profile where Name=:Profilename];
        System.debug('distributorSuperUserProfile---->'+distributorSuperUserProfile);
        List<Account> lstAccounts=AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        insert lstAccounts;
        for(Account acc : lstAccounts)
        {
            acc.IsPartner=true;
            acc.PROFILED_ACCOUNT_FLAG__c =true;
            acc.Cluster__c='EMEA 1B';
        }
        update lstAccounts;
        Contact con = UserProfileTestClassDataHelper.createContact();
        insert con;
        con.AccountId=lstAccounts[0].Id;
        con.Email='test23@emc.com';
        update con;
        
        System.debug('contact----->'+con);
        User tempUsr = new User(
             Username='test12341'+Math.random()+'@acme.com.test',
             TimeZoneSidKey='America/New_York',
             ProfileId=distributorSuperUserProfile.Id,
             LocaleSidKey='en_US',
             FirstName='Direct',
             LastName='Rep',
             email='john@acme.com',
             Alias='test',
             EmailEncodingKey='ISO-8859-1',
             LanguageLocaleKey='en_US',
             Forecast_Group__c='Direct',
             BU__c='NA',
             Employee_Number__c='9323782000',
             IsActive=true,
             ContactId = con.id
        );
        insert tempUsr;
        System.debug('insert tempUsr--->'+tempUsr);
        return tempUsr;
    }

}