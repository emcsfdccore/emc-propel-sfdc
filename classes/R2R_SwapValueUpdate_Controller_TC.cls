/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  01 July 2014    Akash Rastogi    991        To cover the corresponding Class.   
 +=========================================================================================================================*/



@isTest (seeAllData = true)
private class R2R_SwapValueUpdate_Controller_TC
{
    
    public static testmethod void unitTest1()
    {
        Opportunity Opp = testclassUtils.getOppty();
        insert Opp;
        Apexpages.currentPage().getParameters().put('retURL','google.com');
        ApexPages.StandardController stdCon = new ApexPages.StandardController(Opp);
        R2R_SwapValueUpdate_Controller r2rCtrl = new R2R_SwapValueUpdate_Controller(stdCon);
        r2rCtrl.cancelupdate();
        r2rCtrl.updateswapvalue();
    }
}