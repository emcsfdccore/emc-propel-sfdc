@isTest (SeeAllData=true)
private class GFS_Email_Template_Controller_TC {

    static testMethod  void gfsEmailTemplate(){
        
        test.startTest();
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser)
        {
          PRM_VPP_JobDataHelper.createVPPCustomSettingData();   
        }
        Opportunity opp=testclassUtils.getOppty ();
        insert opp;
                
        Account acc=testclassUtils.getAccount () ;
        insert acc;
    
        Asset__c asset= new Asset__c();
        asset.Name='Testasset1';
        asset.Customer_Name__c=acc.id;
        asset.Red_Zone_Priority__c='RedZone';
        asset.Deffered_to_Renewals__c= false;
        asset.Total_Raw_Capacity_GB__c='4';
        insert asset;
        
        
        
        Opportunity_Asset_Junction__c oaj1=new Opportunity_Asset_Junction__c();
        oaj1.Related_Asset__c=asset.id;
        oaj1.Related_Account__c=acc.id;
        oaj1.Related_Opportunity__c=opp.id;
    
        insert oaj1;
        
        GFS_Email_Template_Controller gfs= new GFS_Email_Template_Controller();
        gfs.OpportunityId=opp.id;
        gfs.AssetTypeToFetch='Yes';
        gfs.getassetToDisplay();
        gfs.AssetTypeToFetch='No';
        gfs.getassetToDisplay();
        
        test.stopTest();
        
    }
}