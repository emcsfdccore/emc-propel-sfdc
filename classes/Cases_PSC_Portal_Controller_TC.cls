/**
 * This class contains unit tests for validating the behaviour of Apex classes
 */
 
@IsTest
 Public class Cases_PSC_Portal_Controller_TC
{
    public static testMethod void CasePScPortalTest()
    {
        
      // User insertUser = [Select id,ContactId from User where isActive=true and profile.Name like'%Distribution VAR%' limit 1];         
      // System.debug('ContactId : '+insertUser.ContactId  );
        System.runAs(new user(Id = UserInfo.getUserId()))
            {
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
                CustomSettingDataHelper.dealRegistrationCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.partnerBoardingCSData();
                CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
                CustomSettingDataHelper.VCEStaticCSData();
                CustomSettingDataHelper.PSCCaseTypeImageData();
                CustomSettingDataHelper.PSCCaseTypeImageSiteData();
                CustomSettingDataHelper.PSCFieldMappingData();
                CustomSettingDataHelper.SC13LanguageData();
            }
             
             // create account record.
            List<Account> acc = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();  
            insert acc; 
            
           /* list<Contact> listCon = new list<Contact>();
            listCon.add(new Contact(accountID = acc[0].id, email = 'Test_PSC@test.com', LastName = 'Test Contact0'));
            listCon.add(new Contact(accountID = acc[0].id, email = 'Test_PSC1@test.com',     LastName = 'Test Contact1'));
            insert listCon;
            
            
            ID sysid = [ Select id from Profile where name like '%Distribution VAR%' limit 1].Id;
            User insertUser1 = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='testR2Ruser1@emailR2RTest.com', alias='tuser1', CommunityNickName='tuser1', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ContactId = listCon[0].id , 
            LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' ,Forecast_Group__c = 'Maintenance Renewals'); 
            System.runAs(new user(Id = UserInfo.getUserId()))
            {
            insert insertUser1;
            } 
            
            system.runAs(insertUser1)
            {
                ApexPages.currentPage().getParameters().put('recordType', 'Channel System Support');
                ApexPages.currentPage().getParameters().put('langForPage', '');
                Cases_PSC_Portal_Controller csPSCPortal= new Cases_PSC_Portal_Controller();
            }*/ 
            
           
            
            //Create Lead records.
            list<Lead> listLead = new list<lead> ();
            listLead.add(new lead(Company='TestLead' ,LastName='Test0', Status='Working',Sales_Force__c='EMC',Lead_Originator__c='Customer Intelligence', DealReg_Deal_Registration__c = true)); 
            insert listLead;
            String LeadNum = [ select id, Lead_Number__c from lead where id = : listLead limit 1].Lead_Number__c ;
            system.debug('-----LeadNum---' + LeadNum);
            
            // Create Opportunity Record.
            list<Opportunity>  listOpt = new list<Opportunity>();
            listOpt.add(new Opportunity(Name = 'TestOpp0',AccountId = acc[0].Id, CloseDate = system.today() + 5, StageName = 'Pipeline',Sales_Channel__c='Direct',Sales_Force__c='EMC'));
            insert listOpt;
            
            // Adding case records.
            list<case> listcases= new list<case>();
            listcases.add(new case( Origin ='Community', STATUS = 'New', parentid = null, SUBJECT='test case2', DESCRIPTION = 'Testing description2',Legal_Company_Name__c='test' ));
            listcases.add(new case( Origin ='Community', STATUS = 'New', parentid = null, SUBJECT='test case2', DESCRIPTION = 'Testing description2',Legal_Company_Name__c='test', Lead_Name__c = listLead[0].id));
            listcases.add(new case( Origin ='Community', STATUS = 'New', parentid = null, SUBJECT='testcase1', DESCRIPTION = 'Testing description',Deal_Registration_Number__c = LeadNum));
            insert listcases;
            
             // create SC_RecordType_Mapping__c records.
            list<SC_RecordType_Mapping__c> listSCRTMap = new list<SC_RecordType_Mapping__c>();
            listSCRTMap.add(new SC_RecordType_Mapping__c(On_Portal__c = true, RecordType_Subtype__c = 'PSC - Channel System Support+Channel Express', Section_Name__c ='Case Information', FieldName__c = 'test01'));
            listSCRTMap.add(new SC_RecordType_Mapping__c(On_Portal__c = true, RecordType_Subtype__c = 'PSC - Channel System Support+Channel Express', Section_Name__c ='Case Information', FieldName__c = 'test01'));
            listSCRTMap.add(new SC_RecordType_Mapping__c(On_Portal__c = true, RecordType_Subtype__c = 'PSC - Channel System Support+Channel Express', Section_Name__c ='Description Information', FieldName__c = 'Test02'));
            
            insert listSCRTMap;
            
            Test.Starttest();     
            ApexPages.currentPage().getParameters().put('recordType', 'Channel System Support');
            ApexPages.currentPage().getParameters().put('subType', 'Channel Express');
            apexpages.standardcontroller Case1 = new apexpages.standardcontroller(listcases[2]);
            Cases_PSC_Portal_Controller csPSCPortal1= new Cases_PSC_Portal_Controller (Case1);
            csPSCPortal1.SaveAction();
            
            ApexPages.currentPage().getParameters().put('recordType', 'Deal Registration and Lead Management');
            ApexPages.currentPage().getParameters().put('subType', 'Partner Onboarding');
            ApexPages.currentPage().getParameters().put('DealId', +listLead[0].id);
            ApexPages.currentPage().getParameters().put('OpptyId', + listOpt[0].id);
            apexpages.standardcontroller Case2 = new apexpages.standardcontroller(listcases[1]);
            Cases_PSC_Portal_Controller csPSCPortal2= new Cases_PSC_Portal_Controller (Case2);
            csPSCPortal2.getImgLogo();
            csPSCPortal2.getisRenderLang();
            csPSCPortal2.SaveAction();
            csPSCPortal2.CancelAction();
            csPSCPortal2.caseSaved = true;
            csPSCPortal2.saveAttachment();
        
        try{    
          Cases_PSC_Portal_Controller PSCPortalNonPara = new Cases_PSC_Portal_Controller();
        }catch(NullPointerexception np)
        {
            
        }
            
            ApexPages.currentPage().getParameters().put('recordType', 'Channel System Support');
            ApexPages.currentPage().getParameters().put('subType', 'Partner Portal');
            apexpages.standardcontroller Case3 = new apexpages.standardcontroller(listcases[1]);
            Cases_PSC_Portal_Controller csPSCPortal3= new Cases_PSC_Portal_Controller (Case3);
            
            ApexPages.currentPage().getParameters().put('recordType', 'Order Management');
            ApexPages.currentPage().getParameters().put('subType', 'Renewal Status Request');
            apexpages.standardcontroller Case4 = new apexpages.standardcontroller(listcases[2]);
            Cases_PSC_Portal_Controller csPSCPortal4= new Cases_PSC_Portal_Controller (Case4);
            
            ApexPages.currentPage().getParameters().put('recordType', 'Channel System Support');
            ApexPages.currentPage().getParameters().put('subType', 'Program and Process Queries,Technical Queries');
            apexpages.standardcontroller Case5 = new apexpages.standardcontroller(listcases[0]);
            Cases_PSC_Portal_Controller csPSCPortal5= new Cases_PSC_Portal_Controller (Case5);
            
             ApexPages.currentPage().getParameters().put('recordType', 'Contract Management');
            ApexPages.currentPage().getParameters().put('subType', 'Partner Onboarding');        
            apexpages.standardcontroller Case6 = new apexpages.standardcontroller(listcases[2]);
            Cases_PSC_Portal_Controller csPSCPortal6= new Cases_PSC_Portal_Controller (Case6);
        
            ApexPages.currentPage().getParameters().put('recordType', 'Quoting');
            apexpages.standardcontroller Case9 = new apexpages.standardcontroller(listcases[2]);
            Cases_PSC_Portal_Controller csPSCPortal9= new Cases_PSC_Portal_Controller (Case9);
        
            ApexPages.currentPage().getParameters().put('recordType', 'Finders Fees');
            apexpages.standardcontroller Case7 = new apexpages.standardcontroller(listcases[2]);
            //Cases_PSC_Portal_Controller csPSCPortal7= new Cases_PSC_Portal_Controller (Case9);
            
            ApexPages.currentPage().getParameters().put('recordType', 'Credit');
            apexpages.standardcontroller Case8 = new apexpages.standardcontroller(listcases[2]);
            Cases_PSC_Portal_Controller csPSCPortal8= new Cases_PSC_Portal_Controller (Case8);

            
            test.stoptest();

    }
  
  }