/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req     DESCRIPTION                               
 |  ====            =========       ======     ===========  
 |  05/11/2014    Karan Shekhar    375032      Controller class for Distributor scorecard list VF page
 |  29/12/2014    Karan Shekhar    375032      Added limit to fetch 10000 associations from query.
 |  04/01/2015    Keith Deglialberti  393254   Added criteria to query to only return account associations who are Profiled Accounts
 |  20/02/2015    Jaypal Nimesh	  Mar Release  Modifed query as part of March release
+=============================================================================================================================*/
public class PRM_BPP_Disti_Scorecard_List_Controller {
    
    public ID profileAccountId;
    List<APPR_MTV__RecordAssociation__c> varScoreCardList;
    public String varId{get;set;}
    
    public PRM_BPP_Disti_Scorecard_List_Controller () {
        profileAccountId = ApexPages.currentPage().getParameters().get('id');//fetch 
    }
    
    public List<APPR_MTV__RecordAssociation__c> getVarAccounts() {
        varScoreCardList = new List<APPR_MTV__RecordAssociation__c>();
        for (APPR_MTV__RecordAssociation__c accAsso : [Select APPR_MTV__Account__r.Name,APPR_MTV__Account__r.BillingCountry,APPR_MTV__Account__c FROM APPR_MTV__RecordAssociation__c where APPR_MTV__Account__r.PROFILED_ACCOUNT_FLAG__c = true and APPR_MTV__Primary__c = true and APPR_MTV__Associated_Account__c =: profileAccountId Order By APPR_MTV__Account__r.Name limit 10000]){
            varScoreCardList.add(accAsso);
        }
        return varScoreCardList;
    }
    
    public PageReference solProviderScore() {
        
        PageReference pr; 
        system.debug('**varId****'+varId);      
        if(varId!='') {
            pr = new PageReference('/apex/PRM_BPP_Scorecard_v1?id='+varId);
        }
        pr.setRedirect(true);
        return pr; 
    }
}