/*==================================================================================================================+

 |  HISTORY  |                                                                            

 |  DATE          DEVELOPER      WR        DESCRIPTION                              

 |  ====          =========      ==        =========== 
 |  24/02/2014    Srikrishna SM  FR 9786   Added update code for fields 'Estimated Raw Capacity (TB)' and 'Number of Ports/Nodes'
 |  25/06/2014    Vivek          #991      Added new field GFS_Rep_Incentive_Num__c in query                        
 |  06-Nov-2014    Vivek          #1483      Added new field DPAD_Isilon_Rep_Name__c in query                        
 +==================================================================================================================*/
public class R2R_SwapValueUpdate_Controller{
    LIST<Opportunity_Asset_Junction__c> listOfOpptyJunction= new LIST<Opportunity_Asset_Junction__c>(); 
    Id OpptyId;
    public list<Asset__c> lstAssetToDisplay {get;set;}
    public boolean isUpdateVisible {get;set;}
    public R2R_SwapValueUpdate_Controller (ApexPages.StandardController controller){
        isUpdateVisible = false; 
        set<id> setAssetId = new set<id>();
        OpptyId=ApexPages.currentPage().getParameters().get('opportunityId');
        
        List<Opportunity_Asset_Junction__c>listofoppjunction=[Select id,Related_Asset__c,Related_Opportunity__c,Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c,
                                                          Related_Asset__r.Product_Family__c,Related_Asset__r.RecordType.DeveloperName,Related_Asset__r.Name, Related_Asset__r.GFS_Rep_Incentive_Num__c, Related_Opportunity__r.Name from Opportunity_Asset_Junction__c where Related_Opportunity__c =: OpptyId];    
        for(Opportunity_Asset_Junction__c oaj:listofoppjunction){
            setAssetId.add(oaj.Related_Asset__c);
        }
        
        /*@991 - Updated query due to new field GFS Rep Incentive */
        //#1483 - Added new field DPAD_Isilon_Rep_Name__c in query 
        
        lstAssetToDisplay = [Select Id,Name,Swap_Trade_In_Cost_Relief_Value__c,Product_Name_Vendor__c,Product_Family__c,RecordType.Name,Configuration_Details__c, GFS_Rep_Incentive_Num__c,
                             Estimated_Raw_Capacity_TB__c,Total_Raw_Capacity_TB__c,Number_of_Ports__c,DPAD_Isilon_Rep_Name__c,Total_Raw_Capacity__c from Asset__c where id 
        in :setAssetId];
        if(lstAssetToDisplay.isempty()){
           isUpdateVisible = true;
           ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info,System.Label.R2R_No_Asset_To_Update_Swap_Value);
           ApexPages.addMessage(msg); 
        }
    }
    public pagereference updateswapvalue(){
        string retURL;
        for(Asset__c assetObj :lstAssetToDisplay){
            if(assetObj.Swap_Trade_In_Cost_Relief_Value__c !=null){
                assetObj.Swap_Trade_In_Cost_Relief_Value__c = assetObj.Swap_Trade_In_Cost_Relief_Value__c ;
                //FR- 9786
                assetObj.Estimated_Raw_Capacity_TB__c = assetObj.Estimated_Raw_Capacity_TB__c; 
                assetObj.Number_of_Ports__c = assetObj.Number_of_Ports__c;
            }
        }
        update lstAssetToDisplay; 
        retURL= Apexpages.currentPage().getParameters().get('retURL');
         return new pagereference(retURL);
    }
    public PageReference cancelupdate() {        
            return new PageReference('/'+OpptyId); 
            
 
 
        
    }
}