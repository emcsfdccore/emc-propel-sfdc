/*========================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE           DEVELOPER                WR                DESCRIPTION                               
 |  ====           =========                ==                =========== 
 |  18 Dec 2012   Hemavathi N M         Account Presentation  To Schedule Batch Job to Delete Audit Reporting Data from Master Object 
 +======================================================================================================================================*/

global  class AP_BatchDeleteReportScheduler implements Database.Batchable<sObject>,Schedulable
{
    public Boolean isReportRunTest = false;
    public String ScheduleNowTime = String.valueOf(Date.Today());
    
    global Database.Querylocator start(Database.BatchableContext BC)
    {
        System.debug('------------------Run Batch Audit Reporting Delete Master--------------------');
        ScheduleNowTime = ScheduleNowTime +'T00:00:00.000Z';
        string query='';
        //if(Util.isTestCoverage)
        if(test.IsRunningTest())
        {
            query='Select Id, Refresh_Date__c, LastModifiedDate from Customer_Profile_Audit_Detail__c where  LastModifiedDate <'+  ScheduleNowTime +'  limit 10' ;
        
        }else
        {
            query='Select Id, Refresh_Date__c, LastModifiedDate from Customer_Profile_Audit_Detail__c where  LastModifiedDate <'+  ScheduleNowTime ;
        
        }
        system.debug('query===> '+query);
        return Database.getQueryLocator(query);
    }
    global void execute(SchedulableContext SC)
    {
        AP_BatchDeleteReportScheduler batch = new AP_BatchDeleteReportScheduler(); 
        database.executebatch(batch);
    }
     global void execute(Database.BatchableContext BC, List<SObject> scope)
    { 
        List<Customer_Profile_Audit_Detail__c> lstDeleteAuditDetail = (Customer_Profile_Audit_Detail__c[]) scope;  
        if(lstDeleteAuditDetail.size()>0)
        {
           delete(lstDeleteAuditDetail);
        }
    }
    global void finish(Database.BatchableContext BC)
    {
        System.debug(LoggingLevel.WARN,'Batch Process 2 Finished');
        //Build the system time of now + 20 seconds to schedule the batch apex.
        Datetime sysTime = System.now();
        sysTime = sysTime.addSeconds(20);
        String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        system.debug(chron_exp);
        Schedule_AP_For_Account_District apForAccountDistrict = new Schedule_AP_For_Account_District();
        //Schedule the next job, and give it the system time so name is unique
        System.schedule('AP_Batch Multiple CPA Account District' + sysTime.getTime(),chron_exp,apForAccountDistrict);
        
    }

}