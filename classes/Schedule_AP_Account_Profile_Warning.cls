global class Schedule_AP_Account_Profile_Warning implements Schedulable
{
	public boolean isRunTest=false;
	global void execute(SchedulableContext sc)
	{
		//string query='Select ID, Name, Account_District__c, District_Lookup__c, Customer_Profiled_Account__c, Account_Profiling_Warning_Counts__c From Account WHERE  Account_District__c != null and Account_District__c = \'2E2 HOLDINGS LTD: UK Mid Markets South District\' Order by Account_District__c asc , Customer_Profile_Value__c desc ';
		AP_for_Account_With_Profile_Warning clsBatchAccWarn = new AP_for_Account_With_Profile_Warning();
		//type != '+'Partner'+'and
		ID idBatch = Database.executeBatch(clsBatchAccWarn, 100);
	}
}