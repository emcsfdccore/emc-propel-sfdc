@isTest
private class OpptyCommentsUtils_TC 
{
    static testMethod void myUnitTest()
    {
    
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        
        //Create User
        
        User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='test-user2015@emailTest.com', alias='tuser1', CommunityNickName='tuser1', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User', BU_Attribute__c = 'Core', Forecast_Group__c = 'Maintenance Renewals'); 
        
        //Insert User
        insert insertUser;    
                          
        System.runAs(new user(Id = UserInfo.getUserId())){        
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.adminConversionCSData();
            CustomSettingDataHelper.houseAccountCSData();
        }  
        
        //Insert Account
        List<Account> accList = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        insert accList;
        
        // Create Record Type
        Id rectypIdNextStep = Schema.SObjectType.Opportunity_Comments_History__c.RecordTypeInfosByName.get('Sales – Next Steps').RecordTypeId;
        Id rectypIdSolution = Schema.SObjectType.Opportunity_Comments_History__c.RecordTypeInfosByName.get('Presales – Solution Win').RecordTypeId;
        
        //Insert Opportunity Record for Next_Steps_new coverage
        List<Opportunity> oppList1 = new List<Opportunity>();
        oppList1.add(new Opportunity(AccountId = accList[0].Id, Next_Steps_new__c = 'opportunity1',Most_Recent_5_Solutions_Win_Comments__c=null,Solutions_Win_Comments__c='Test Comment 3',Next_Steps__c='Test 4',Name='TestOpp',StageName='Submitted',CloseDate=system.today()));
        oppList1.add(new Opportunity(AccountId = accList[0].Id,Most_Recent_5_Solutions_Win_Comments__c='Test Comment 1',Solutions_Win_Comments__c='Test Comment 3',Next_Steps_new__c = 'No opportunity1',Next_Steps__c='Test 4',Name='TestOpp',StageName='Submitted',CloseDate=system.today()));
        insert oppList1;
                
        //Insert Opportunity Comment History
        List<Opportunity_Comments_History__c> oppCmntList1 = new List<Opportunity_Comments_History__c>();
        oppCmntList1.add(new Opportunity_Comments_History__c(recordtypeid=rectypIdSolution,Comments__c ='Test 1',OpportunityId__c = oppList1[0].id));
        oppCmntList1.add(new Opportunity_Comments_History__c(recordtypeid=rectypIdSolution,Comments__c ='Test 2',OpportunityId__c = oppList1[0].id));
        oppCmntList1.add(new Opportunity_Comments_History__c(recordtypeid=rectypIdNextStep,Comments__c ='Test 3',OpportunityId__c = oppList1[0].id));
        oppCmntList1.add(new Opportunity_Comments_History__c(recordtypeid=rectypIdNextStep,Comments__c ='Test 4',OpportunityId__c = oppList1[0].id));
        Insert oppCmntList1;
        
        Map<Id,Opportunity> OldopptyMap = new Map<Id,Opportunity>();
        OldopptyMap.put(oppList1[0].id,oppList1[0]); 
              
        Test.startTest();
        OpptyCommentsUtils oppUtils= new OpptyCommentsUtils();
        OpptyCommentsUtils.commentsHistory(oppList1,OldopptyMap,true);
        Test.stopTest();
    }       
}