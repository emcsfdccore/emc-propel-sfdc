/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
 |  ====          =========               ==            =========== 
 |  13/Jun/2014   Jaypal Nimesh   DM PVR Project     Created class for Duplicate PVR logic.
|
+=====================================================================================================*/
public class CasePVRHelper {
    
    //Getting the PVR Case Record Type Id using Describe Method
    public Map<String,Schema.RecordTypeInfo> recordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName(); 
    public final String casePvrRecordTypeId = recordTypes.get('Renewals Policy Variation Request (PVR)').getRecordTypeId();
    
    //Method to check PVR cases for both Insert & update scenario
    public void caseDuplicatePVRCheck(List<Case> newCaseList, Map<Id, Case> oldIdCaseMap){
        
        //As trigger old map is not null, its update scenario
        if(oldIdCaseMap != null && oldIdCaseMap.size() > 0){
            
            List<Case> newPVRCaseList = new List<Case>();
            
            for(Case newCase : newCaseList){
                
                //Filtering PVR cases, if Old value & New value for SO# fields have been changed
                if(newCase.RecordTypeId == casePvrRecordTypeId && newCase.ParentId == null && (newCase.New_Deal_SO_1__c != oldIdCaseMap.get(newCase.Id).New_Deal_SO_1__c
                    || newCase.New_Deal_SO_2__c != oldIdCaseMap.get(newCase.Id).New_Deal_SO_2__c || newCase.New_Deal_SO_3__c != oldIdCaseMap.get(newCase.Id).New_Deal_SO_3__c
                    || newCase.New_Deal_SO_4__c != oldIdCaseMap.get(newCase.Id).New_Deal_SO_4__c || newCase.Sales_Order_Number__c != oldIdCaseMap.get(newCase.Id).Sales_Order_Number__c)){
                    
                    newPVRCaseList.add(newCase);
                }
            }
            //If list is not null, call the method for PVR logic           
            if(newPVRCaseList != null && newPVRCaseList.size() > 0){            
                caseDuplicatePVRLogic(newPVRCaseList);
            }
        }
        //Else works for Insert Scenario
        else{           
            List<Case> newPVRCaseList = new List<Case>();
            
            for(Case newCase : newCaseList){
                
                //Checks for PVR Parent cases only, Child cases are not considered in Duplicate PVR logic 
                if(newCase.RecordTypeId == casePvrRecordTypeId && newCase.ParentId == null){            
                    newPVRCaseList.add(newCase);
                }
            }
            //If list is not null, call the method for PVR logic    
            if(newPVRCaseList != null && newPVRCaseList.size() > 0){            
                caseDuplicatePVRLogic(newPVRCaseList);
            }
        }
    }
    
    //Method to identify Duplicate PVR cases for given list
    public void caseDuplicatePVRLogic(List<Case> caseObjList){
        
        //Storing field API names in a list for looping purpose
        final List<String> soFieldApiList = new List<String>{'New_Deal_SO_1__c','New_Deal_SO_2__c','New_Deal_SO_3__c','New_Deal_SO_4__c','Sales_Order_Number__c'};
        
        //Set to store all SO Numbers
        Set<String> soNumberStrSet = new Set<String>();
        
        for(Case caseObj : caseObjList){
        
            for(String soFieldApi : soFieldApiList){
                
                //If SO# fields are not null, add to set
                if(caseObj.get(soFieldApi) != null){                    
                    soNumberStrSet.add(String.valueOf(caseObj.get(soFieldApi)));
                }
            }           
        }
        //If set is not null, query all duplicate PVRs
        if(soNumberStrSet != null && soNumberStrSet.size() > 0){
            
            List<Case> pvrDupeCaseList = [Select Id, CaseNumber, Sales_Order_Number__c, New_Deal_SO_1__c, New_Deal_SO_2__c, New_Deal_SO_3__c, New_Deal_SO_4__c, AllPVRSONumbers__c from 
                                            Case where RecordTypeId =: casePvrRecordTypeId AND ParentId = null AND (New_Deal_SO_1__c IN: soNumberStrSet OR New_Deal_SO_2__c IN: soNumberStrSet 
                                            OR New_Deal_SO_3__c IN: soNumberStrSet OR New_Deal_SO_4__c IN: soNumberStrSet OR Sales_Order_Number__c IN: soNumberStrSet) limit 49999];
                                         
            if(pvrDupeCaseList != null && pvrDupeCaseList.size() > 0){
                
                //Map to store set of duplicate PVRs for each case
                Map<Id, Set<Case>> pvrIdCaseSetMap = new Map<Id, Set<Case>>();
                
                //Iterate over each case, then every duplicate PVR case
                for(Case newCase : caseObjList){
                    
                    Set<Case> caseSubSet = new Set<Case>();
                    
                    for(Case pvrCase : pvrDupeCaseList){
                        
                        //Checks if different case comes in duplicate list and size has not reached 300, then add dupe case to set
                        if(newCase.Id != pvrCase.Id && caseSubSet.size() < 300){
                            
                            //Looping for every So# field
                            for(String soFieldStr : soFieldApiList){
                                //Checks if SO# is present in AllPVRSONumbers__c formula field, then put the value in Map
                                if(newCase.get(soFieldStr) != null && pvrCase.AllPVRSONumbers__c.contains(String.valueOf(newCase.get(soFieldStr)))){
                                    
                                    //Adds duplicate cases for each PVR in a set, then put into Map
                                    if(pvrIdCaseSetMap != null && pvrIdCaseSetMap.size() > 0 && pvrIdCaseSetMap.containsKey(newCase.Id)){
                                        
                                        caseSubSet = pvrIdCaseSetMap.get(newCase.Id);
                                        caseSubSet.add(pvrCase);                                    
                                        pvrIdCaseSetMap.put(newCase.Id, caseSubSet);
                                    }
                                    else{
                                        pvrIdCaseSetMap.put(newCase.Id, new Set<Case>{pvrCase});
                                    }
                                }
                            }
                        }
                    }
                }
                //If Map is not null, that means duplicates are found for PVR case
                if(pvrIdCaseSetMap != null && pvrIdCaseSetMap.size() > 0){
                
                    List<Case> pvrCaseToUpdateList = new List<Case>();
                    
                    //Querying same records, as it is After trigger call
                    List<Case> insertedCaseList = [Select Id, CaseNumber, Business_Rule_Exception__c, Header_Message__c, Business_Rule_Exception_Message__c from Case where Id IN: caseObjList];
                    
                    for(Case caseRec : insertedCaseList){
                        
                        //Initializing dupeCase String for each case in current transaction
                        String dupeCaseNumIdString = '';
                        
                        //If Map has a key, means duplicate cases are present
                        if(pvrIdCaseSetMap.containsKey(caseRec.Id)){
                            
                            //Iterate over dupe cases
                            for(Case cs : pvrIdCaseSetMap.get(caseRec.Id)){
                                
                                //Create Case Hyperlinks and add into same String for Rick Text Area field
                                if(dupeCaseNumIdString == '' || dupeCaseNumIdString == null){       
                                    dupeCaseNumIdString = '<a href=/' + cs.Id + '>Case: ' + cs.CaseNumber + '</a>';
                                }
                                else{
                                    dupeCaseNumIdString = dupeCaseNumIdString + '<br/>' + '<a href=/' + cs.Id + '>Case: ' + cs.CaseNumber + '</a>';
                                }
                            }
                            //Checks of duplicate string is formed, then only update the PVR message and checkbox fields
                            if(dupeCaseNumIdString != null && dupeCaseNumIdString != ''){

                                String pvrDetailMsg = '<b><font face="verdana" color="red">' + System.Label.CasePVRMessage + '</font> </b>' + '<br/> <br/>' + '<b>Duplicate Cases Below: </b>' + '<br/> <br/>';
                                caseRec.Business_Rule_Exception__c = true;
                                caseRec.Header_Message__c = '<b><font face="verdana" color="red">' + System.Label.CasePVRHeaderMessage + '</font> </b>';
                                caseRec.Business_Rule_Exception_Message__c = pvrDetailMsg + dupeCaseNumIdString;
                                
                                //Adding case to update list
                                pvrCaseToUpdateList.add(caseRec);
                            }                       
                        }
                        //Nullify the field values - covers the update non duplicate scenario
                        else{
                            caseRec.Business_Rule_Exception__c = false;
                            caseRec.Header_Message__c = '';
                            caseRec.Business_Rule_Exception_Message__c = '';
                            
                            //Add case to update list 
                            pvrCaseToUpdateList.add(caseRec);                           
                        }
                    }
                    //If list is not null, update using Database method                   
                    if(pvrCaseToUpdateList != null && pvrCaseToUpdateList.size() > 0){                                          
                        Database.SaveResult[] srList = Database.update(pvrCaseToUpdateList, false);
                    }
                }
                else{
                    //If dupe map is null, then nullify the PVR fields
                    caseNullifyPVRLogic(caseObjList);
                }
            }
            else{
                //If duplicate cases are not found in database, then nullify the PVR fields
                caseNullifyPVRLogic(caseObjList);
            }
        }
        else{
            //If duplicate SO# not found or All SO# are nullified, then nullify all PVR fields 
            caseNullifyPVRLogic(caseObjList);
        }
    }
    
    //Method to blank out PVR Fields, in case duplicates are not found in update call
    public void caseNullifyPVRLogic(List<Case> caseList){
        
        //Creating a copy of same records, as it is After trigger call
        List<Case> updatedCaseList = [Select Id, CaseNumber, Business_Rule_Exception__c, Header_Message__c, Business_Rule_Exception_Message__c from Case where Id IN: caseList];
        
        for(Case caseRec: updatedCaseList){
            
            //Blank out all PVR field values
            caseRec.Business_Rule_Exception__c = false;
            caseRec.Header_Message__c = '';
            caseRec.Business_Rule_Exception_Message__c = '';
        }
        //Upadte the case PVRs
        Database.SaveResult[] srList = Database.update(updatedCaseList, false);
    }
}