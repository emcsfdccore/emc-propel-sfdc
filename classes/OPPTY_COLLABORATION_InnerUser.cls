/**
===========================================================================+
 |  Created History                                                                  
                                                                 
 |  DATE        DEVELOPER       WORK REQUEST            DESCRIPTION                               
 |  ====        =========       ============            =========== 
 |  4 Mar 2014  Avinash K       Oppty Collaboration     Acts as a base class for the custom iteratble classes to list the Partner Users along
                                                        with checkboxes to be selected, navigated to next/previous pages and still retaining 
                                                        their selection
=============================================================================+
**/

global Class OPPTY_COLLABORATION_InnerUser
{
    public boolean blnIsSelected {get;set;}
    public User usr {get;set;}
    public String strAccName {get;set;}
    
/**
Constructor assigns the arguments' vales to the appropriate variables within the class
**/
    public OPPTY_COLLABORATION_InnerUser(boolean blnIsSelected, User objUser, String strAccName)
    {
        this.blnIsSelected = blnIsSelected;
        this.usr= objUser ;
        this.strAccName = strAccName;
    }
}