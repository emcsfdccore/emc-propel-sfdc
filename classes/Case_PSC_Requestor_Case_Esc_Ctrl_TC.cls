@istest
public class Case_PSC_Requestor_Case_Esc_Ctrl_TC
{
    public static testmethod void RequestorCaseEscalationTest()
    {
        System.runAs(new user(Id = UserInfo.getUserId()))
            {    
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.partnerBoardingCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
            CustomSettingDataHelper.VCEStaticCSData();
            CustomSettingDataHelper.PSCCaseTypeImageData();
            CustomSettingDataHelper.PSCCaseTypeImageSiteData();
            CustomSettingDataHelper.PSCFieldMappingData();
            CustomSettingDataHelper.PSCCasesLanguageData();
           }
           
        Case_PSC_Requestor_Case_Escalation_Ctrl ReqEscCtrl = new Case_PSC_Requestor_Case_Escalation_Ctrl();
        ReqEscCtrl.CancelAction();
        ReqEscCtrl.CheckCase();
        
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where RecordType.Name = 'PSC - Channel System Support'];
        
        // Adding case records.
        list<case> listcase = new list<case>();
        if(recordType !=null && recordType.size()>0){
            listcase.add(new case( Origin ='Community', STATUS = 'New', parentid = null, SUBJECT='testcase0', DESCRIPTION = 'Testing description',  RecordTypeId = recordType[0].id));
            listcase.add(new case( Origin ='Community', STATUS = 'New', parentid = null, SUBJECT='testcase1', DESCRIPTION = 'Testing description',RecordTypeId = recordType[0].id));
        }
        if(listcase !=null && listcase.size()>0){
            insert listcase;
            String CaseNo = [Select id, CaseNumber from case where id = : listcase limit 1 ].CaseNumber;
            Case_PSC_Requestor_Case_Escalation_Ctrl ReqEscCtr2 = new Case_PSC_Requestor_Case_Escalation_Ctrl();
            ReqEscCtr2.ConName = 'Test';
            ReqEscCtr2.ConMail = 'qwert@test.com';
            ReqEscCtr2.ConPhone = '12345' ; 
            ReqEscCtr2.comment =  'qwerty';
            ReqEscCtr2.CaseNum = CaseNo;
            ReqEscCtr2.CheckCase();
        }
    }
}