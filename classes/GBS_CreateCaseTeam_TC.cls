/*======================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
| 25/Nov/2014  Garima Bansal   GBS Comm Project     Created TC to insert Case Team.
|
+=======================================================================================*/

@istest
 private class GBS_CreateCaseTeam_TC{
 
    static testmethod void GBS_CreateCaseTeam_TM() {
        
        //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.VCEStaticCSData();
        } 
        // Query User Role
        List<UserRole> userRoleList = [Select Id, Name, DeveloperName from UserRole where DeveloperName = 'Basic_Employee' limit 1];
        // Query to fetch profile
        List<Profile> gbsProfileList = [Select Id, Name from Profile where Name ='GBS Collaborator Profile' limit 1];
        // Check if userRoleList and gbsProfileList is empty to avoid Null pointer exception
        if(userRoleList != null && userRoleList.size() > 0 && gbsProfileList != null && gbsProfileList.size() > 0){
            // Query for user in database 
            List<User> insertUserList = [Select username, FirstName from user where UserRoleid =: userRoleList[0].Id and profileId =: gbsProfileList[0].Id and isActive = true limit 1];
 
            Test.startTest();
            // Check if insertUserList is empty to avoid Null pointer exception
            if(insertUserList != null && insertUserList.size() > 0)
            {
                system.runAs(insertUserList[0]) 
                { 
                // Query for Case record type
                List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where RecordType.DeveloperName = 'Presales_Accounts_Payable_T_E_Vendor_Master_Treasury'];
                // Adding case records.
                list <case> listcase = new list<case>();
                listcase.add(new case (STATUS = 'Open',Processing_Status__c = 'open' , TYPE = 'Data Migration',Origin ='System',PVR_INQUIRY_TYPE__C ='Within Policy Duration', CONCESSION_TYPE__C = 'Field', PVR_BUSINESS_UNIT__C='Core',parentid = null ,SUBJECT='PVR case :' ,DESCRIPTION = 'Testing Insert scenario of case :1', RecordTypeId = recordType[0].id, New_Deal_SO_1__c = '1234567',New_Deal_SO_2__c = '1231',Sales_Order_Number__c = '123', VCE_Transfer__c = true));
                insert listcase;       
                }
            }
            Test.stopTest();            
        }
    }
}