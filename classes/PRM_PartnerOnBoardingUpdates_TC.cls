/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 |  29/04/2011     Shalabh Sharma      This test class is used to test methods written for 
                                       PRM_PartnerOnboardingUpdates class.
|  11-JAN-2012      Anil                        Removed role Id    
|  07-Jan-2013      Vivek               Test class errors fix    
   16-Feb-2015      Paridhi      PI       Modified  to increase the coverage                                  
 +==================================================================================================================**/


@isTest
private class PRM_PartnerOnBoardingUpdates_TC
{
    private static testMethod void pobTest()
    {   
     User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];   
     System.runAs(insertUser)
     {
       // PRM_VPP_JobDataHelper.createVPPCustomSettingData();   
        CustomSettingDataHelper.dataValueMapCSData();
        CustomSettingDataHelper.eBizSFDCIntCSData();
        CustomSettingDataHelper.profilesCSData();
        CustomSettingDataHelper.dealRegistrationCSData();
        CustomSettingDataHelper.bypassLogicCSData();
        CustomSettingDataHelper.partnerBoardingCSData();
        CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
        CustomSettingDataHelper.partnerOnboardingEmailService();
             
        Map<String,Custom_Settings_Partner_OnBoarding__c>  pobdata =  Custom_Settings_Partner_OnBoarding__c.getall();
        String TestQueue = pobdata.get('STEP1.0Americas').Owner__c ;
        List<GroupMember> lstgroupmambers= [SELECT UserOrGroupId  FROM GroupMember where Group.Id =: TestQueue];
        Set<Id> idset=new Set<Id>();
        for(GroupMember gm : lstgroupmambers)
         {
           idset.add(gm.UserOrGroupId);
         }
        User user1= [SELECT Email FROM User where Id in :idset limit 1];
        List<Account> acc = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();       
        insert acc;
    
            
        Account_Groupings__c grouping = new Account_Groupings__c(Name = 'TestClass3', Active__c = true,
        Profiled_Account__c = acc[0].id,No_Master_Required__c = false);
        //insert grouping;
        
        Contact contact = UserProfileTestClassDataHelper.createContact();
        contact.AccountId=acc[1].Id;
        contact.email = 'test127867@emc.com';
        insert contact;
      
        User user01;
       
        System.runAs(insertUser)
        {              
          Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
          String distributorSuperUser = data.get('EMEA Distributor Super User').DataValue__c ;
          Profile amerUserProf = [select Id from Profile where Name=: distributorSuperUser];
          user01 = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.id,null,contact.Id);
          insert user01;
        }
        Custom_Settings_Partner_OnBoarding__c OnBoarding = new Custom_Settings_Partner_OnBoarding__c ( Name= 'Test', Template__c = '00X70000001Rz1Q');
        insert  OnBoarding;

        Contact con = [select Id,Email from Contact where Id =:user01.contactId];
        con.Email = 'test12456@emc.com';
        update con;
        
        user01.Email = con.Email;
        System.runAs(insertUser)
        {
         update user01;
        }
        
        /* Creation of Partner OnBoarding record.*/
        List<Partner_Onboarding__c> lstPOB = new List<Partner_Onboarding__c>();
            Partner_Onboarding__c pobRecord = new Partner_Onboarding__c();
               pobRecord.Region_you_are_applying_for__c = 'APJ';
                pobRecord.EMC_Sponsoring_Distributor__c = 'UNITTESTdistri';
                pobRecord.First_Name__c = 'UNITTESTFrstName';
                pobRecord.Last_Name__c = 'UNITTESTLastName';
                pobRecord.E_mail_Address__c = 'UNITTESTEmail@test.com' ;
                pobRecord.Title__c = 'UNITTESTTitle';
                pobRecord.Contact_Office_Number__c = '54485871';
                pobRecord.Corporate_URL__c = 'UNITTESTUrl@test.com' ;
                pobRecord.Legal_Parent_Company_Name__c = 'UNITTESTCompany';
                pobRecord.Country__c = 'India';
                pobRecord.Main_Company_Phone_Number__c = '54634656';
                pobRecord.Address_1__c = 'UNITTESTAddress1';
                pobRecord.City__c = 'UNITTESTCity';
                pobRecord.State__c='Andhra Pradesh';
                pobRecord.Zip_Postal_Code__c = '4515';
                pobRecord.Primary_EMC_Business_Line_Interest__c = 'Storage';
                pobRecord.Vertical_Market_Specialization__c = 'Construction';
                pobRecord.Application_Environments__c = 'Oracle';
                pobRecord.Company_Overview_1000_characters_max__c = 'UNITTESTCmpnyOverview';
                pobRecord.Primary_Business_Contact_First_Name__c = 'UNITTESTFrstNameBusiness';
                pobRecord.President_CEO_First_Name__c = 'UNITTESTFrstNameCEO';
                pobRecord.Primary_Business_Contact_Last_Name__c = 'UNITTESTLastNameBusiness';
                pobRecord.President_CEO_Last_Name__c = 'UNITTESTLastNameCEO';
                pobRecord.Primary_Business_Contact_E_mail_Address__c = 'UNITTESTEmail@test.com' ;
                pobRecord.President_CEO_E_mail_Address__c = 'UNITTESTEmail@test.com' ;
                pobRecord.Primary_Business_Contact_Title__c = 'UNITTESTTitleBusiness';
                pobRecord.President_CEO_Phone_Number__c = '16831655';
                pobRecord.Primary_Business_Contact_Phone_Number__c = '15461645';
                pobRecord.I_Accept__c = True;
                pobRecord.Authorized_Reseller_Account__c = acc[0].Id;
                pobRecord.OwnerId=user01.id;
                
                Partner_Onboarding__c pobRecord1 = new Partner_Onboarding__c();
                pobRecord1.Region_you_are_applying_for__c = 'EMEA';
                pobRecord1.EMC_Sponsoring_Distributor__c = 'UNITTESTdistri';
                pobRecord1.First_Name__c = 'UNITTESTFrstName';
                pobRecord1.Last_Name__c = 'UNITTESTLastName';
                pobRecord1.E_mail_Address__c = 'UNITTESTEmail@test.com' ;
                pobRecord1.Title__c = 'UNITTESTTitle';
                pobRecord1.Contact_Office_Number__c = '54485871';
                pobRecord1.Corporate_URL__c = 'UNITTESTUrl@test.com' ;
                pobRecord1.Legal_Parent_Company_Name__c = 'UNITTESTCompany';
                pobRecord1.Country__c = 'India';
                pobRecord1.Main_Company_Phone_Number__c = '54634656';
                pobRecord1.Address_1__c = 'UNITTESTAddress1';
                pobRecord1.City__c = 'UNITTESTCity';
                pobRecord1.State__c='Andhra Pradesh';
                pobRecord1.Zip_Postal_Code__c = '4515';
                pobRecord1.Primary_EMC_Business_Line_Interest__c = 'Storage';
                pobRecord1.Vertical_Market_Specialization__c = 'Construction';
                pobRecord1.Application_Environments__c = 'Oracle';
                pobRecord1.Company_Overview_1000_characters_max__c = 'UNITTESTCmpnyOverview';
                pobRecord1.Primary_Business_Contact_First_Name__c = 'UNITTESTFrstNameBusiness';
                pobRecord1.President_CEO_First_Name__c = 'UNITTESTFrstNameCEO';
                pobRecord1.Primary_Business_Contact_Last_Name__c = 'UNITTESTLastNameBusiness';
                pobRecord1.President_CEO_Last_Name__c = 'UNITTESTLastNameCEO';
                pobRecord1.Primary_Business_Contact_E_mail_Address__c = 'UNITTESTEmail@test.com' ;
                pobRecord1.President_CEO_E_mail_Address__c = 'UNITTESTEmail@test.com' ;
                pobRecord1.Primary_Business_Contact_Title__c = 'UNITTESTTitleBusiness';
                pobRecord1.President_CEO_Phone_Number__c = '16831655';
                pobRecord1.Primary_Business_Contact_Phone_Number__c = '15461645';
                pobRecord1.I_Accept__c = True;
                pobRecord1.OwnerId=user01.id;
                
                Partner_Onboarding__c pobRecord2 = new Partner_Onboarding__c();
                pobRecord2.Region_you_are_applying_for__c = 'Americas';
                pobRecord2.EMC_Sponsoring_Distributor__c = 'UNITTESTdistri';
                pobRecord2.First_Name__c = 'UNITTESTFrstName';
                pobRecord2.Last_Name__c = 'UNITTESTLastName';
                pobRecord2.E_mail_Address__c = 'UNITTESTEmail@test.com' ;
                pobRecord2.Title__c = 'UNITTESTTitle';
                pobRecord2.Contact_Office_Number__c = '54485871';
                pobRecord2.Corporate_URL__c = 'UNITTESTUrl@test.com' ;
                pobRecord2.Legal_Parent_Company_Name__c = 'UNITTESTCompany';
                pobRecord2.Country__c = 'India';
                pobRecord2.Main_Company_Phone_Number__c = '54634656';
                pobRecord2.Address_1__c = 'UNITTESTAddress1';
                pobRecord2.City__c = 'UNITTESTCity';
                pobRecord2.State__c='Andhra Pradesh';
                pobRecord2.Zip_Postal_Code__c = '4515';
                pobRecord2.Primary_EMC_Business_Line_Interest__c = 'Storage';
                pobRecord2.Vertical_Market_Specialization__c = 'Construction';
                pobRecord2.Application_Environments__c = 'Oracle';
                pobRecord2.Company_Overview_1000_characters_max__c = 'UNITTESTCmpnyOverview';
                pobRecord2.Primary_Business_Contact_First_Name__c = 'UNITTESTFrstNameBusiness';
                pobRecord2.President_CEO_First_Name__c = 'UNITTESTFrstNameCEO';
                pobRecord2.Primary_Business_Contact_Last_Name__c = 'UNITTESTLastNameBusiness';
                pobRecord2.President_CEO_Last_Name__c = 'UNITTESTLastNameCEO';
                pobRecord2.Primary_Business_Contact_E_mail_Address__c = 'UNITTESTEmail@test.com' ;
                pobRecord2.President_CEO_E_mail_Address__c = 'UNITTESTEmail@test.com' ;
                pobRecord2.Primary_Business_Contact_Title__c = 'UNITTESTTitleBusiness';
                pobRecord2.President_CEO_Phone_Number__c = '16831655';
                pobRecord2.Primary_Business_Contact_Phone_Number__c = '15461645';
                pobRecord2.I_Accept__c = True;
                pobRecord2.OwnerId=user01.id;
                
                lstPOB.add(pobRecord); 
                lstPOB.add(pobRecord1); 
                lstPOB.add(pobRecord2);  
                   
            insert lstPOB; 
                 
          
        Map<Id,Partner_Onboarding__c> mapOldPOBRecords=new Map<Id,Partner_Onboarding__c>();
        mapOldPOBRecords.put(lstPOB[0].id,lstPOB[0]); 

        List<Authorized_Reseller_Onboarding_Task__c> AuthTask = new List<Authorized_Reseller_Onboarding_Task__c>();
        Authorized_Reseller_Onboarding_Task__c AuthRecord = new Authorized_Reseller_Onboarding_Task__c();
        AuthRecord.name = 'AuthRecord';
        AuthRecord .Subject__c= 'Step 2a - Distributor Approval of Authorized Reseller';  
        AuthTask.add(AuthRecord);  
        insert AuthTask;
            
             /*if(lstPOB != null && lstPOB.size()>0){   
             System.debug('lstPOBlstPOB'+lstPOB);
             Authorized_Reseller_Onboarding_Task__c authTask = [select Id,Status__c,Account_Found_in_SFDC__c,Distributor_Approval_Status__c,EMC_Approval_Status__c from Authorized_Reseller_Onboarding_Task__c where Partner_Onboarding__c in :lstPOB limit 1];
             System.Debug('Value of task--->' +authTask);
              //if(authTask!=null){
             authTask.EMC_Approval_Status__c = 'Approved';
             List<Authorized_Reseller_Onboarding_Task__c> lstTask = new List<Authorized_Reseller_Onboarding_Task__c>();
             lstTask.add(authTask);
             update lstTask;   
                
           }
        // } */
                      
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        ID templateId = OnBoarding.Template__c ;
        mail.setTemplateId(templateId);
        List<RecordType> lstRecordType = [Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Authorized_Reseller_Onboarding_Task__c' LIMIT 1];
        string RecordType =lstRecordType.get(0).Id;
        Map<string,DealRegistration_CustomSetting__c>  queuedata =  DealRegistration_CustomSetting__c.getall();
        ID queueid=queuedata.get('AmerExtensionRqstQueue').PSC_QueueId__c;
        string owner=queuedata.get('AmerPSCDRQueue').PSC_QueueId__c;
           
        Test.startTest();
        PRM_PartnerOnBoardingUpdates objPOB = new PRM_PartnerOnBoardingUpdates();
        objPOB.step1updatePartnerOnBoardingOwner(lstPOB);        
        PRM_PartnerOnBoardingUpdates.executeFlag = false;
        
        Partner_Onboarding__c pobRec = [select id,Distributor_Approver__c,Authorized_Reseller_Account__c,Account_Created_by_TRACK__c from Partner_Onboarding__c where Id in: lstPOB limit 1];
        pobRec.Distributor_Approver__c = user01.Id;
        pobRec.Distributor_Account__c = acc[0].Id;
        pobRec.Authorized_Reseller_Account__c = acc[1].Id;
        pobRec.Account_Created_by_TRACK__c = false;
        pobRec.Step_Name__c ='STEP6';
        List<Partner_Onboarding__c> lstPOBrec = new List<Partner_Onboarding__c>(); 
        lstPOBrec.add(pobRec);
        update lstPOBrec;
        
        Map<Id,Partner_Onboarding__c> mapNewPOBRecords=new Map<Id,Partner_Onboarding__c>();
        mapNewPOBRecords.put(lstPOBrec[0].id,lstPOBrec[0]);
        
        objPOB.createTheaterLists(lstPOB);
        objPOB.step2createTaskforCM(lstPOB);
        objPOB.step6createTaskforExtSystems(lstPOB);
        objPOB.step4sendmailtoCM(lstPOB);
        objPOB.step5sendmailtoTRACK(lstPOB);
        objPOB.step7createTaskforCMforProfileUpdationAndWelcomeEmails(lstPOB);
        objPOB.step3createTaskforDistributor(mapNewPOBRecords,mapOldPOBRecords);
        objPOB.Create_Partner_OnboardingTask(lstPOBrec,'test',RecordType,owner,true);
        objPOB.Create_Partner_OnboardingTask(lstPOBrec,'test',RecordType,'00G70000001lJh7',false);
        objPOB.createTaskAndSendMail(lstPOBrec,false,false);
        objPOB.SendEmail(lstPOBrec,OnBoarding,True);
        objPOB.get_QueueMembers(queueid);
               
              
        Authorized_Reseller_Onboarding_Task__c authTask1 = [select Id,Partner_Onboarding__c,Status__c,Account_Found_in_SFDC__c,Partner_Account__c,Distributor_Approval_Status__c,EMC_Approval_Status__c,Distributor_Rejection_Reason__c from Authorized_Reseller_Onboarding_Task__c where Subject__c = 'Step 2a - Distributor Approval of Authorized Reseller' limit 1];      
        system.debug('------>task after Messi'+authTask1);
        authTask1.Distributor_Approval_Status__c = 'Not Approved';
        authTask1.Distributor_Rejection_Reason__c = 'Not Approved';
        authTask1.Status__c='In progress';
        authTask1.Partner_Account__c = acc[0].Id;
        List<Authorized_Reseller_Onboarding_Task__c> lstTask1 = new List<Authorized_Reseller_Onboarding_Task__c>();
        lstTask1.add(authTask1);
        update lstTask1; 
        system.debug('------>task after update'+lstTask1);
        
        objPOB.UpdateLocalLanguageFields(lstPOB);
        objPOB.updateRelatedProfiledAccount(lstTask1);
        lstTask1[0].Subject__c ='STEP3.1Americas';
        lstTask1[0].Account_Found_in_SFDC__c = 'Not Found';
        update lstTask1;
                
        objPOB.updateTaskStatus(mapNewPOBRecords,mapOldPOBRecords);
        authTask1.Partner_Account__c = acc[1].Id;
        objPOB.updateTaskStatus(lstTask1);
        lstTask1[0].Partner_Account__c = acc[1].Id;
        lstTask1[0].Partner_Onboarding__c = lstPOBrec[0].Id;
        update lstTask1;
        objPOB.updateRelatedProfiledAccount(lstTask1);
        
        Map<Id,Authorized_Reseller_Onboarding_Task__c> mapauth = new Map<Id,Authorized_Reseller_Onboarding_Task__c>();
        for(Authorized_Reseller_Onboarding_Task__c AORloopvariable:AuthTask )
        {
         mapauth.put(AORloopvariable.Id,AORloopvariable);
        }
         Map<Id,Authorized_Reseller_Onboarding_Task__c> mapauthnew = new Map<Id,Authorized_Reseller_Onboarding_Task__c>();
        for(Authorized_Reseller_Onboarding_Task__c AORloopvariable2:lstTask1 )
        {
          AORloopvariable2.EMC_Approval_Status__c='Not Approved';
          mapauthnew.put(AORloopvariable2.Id,AORloopvariable2);
        }
        for(Authorized_Reseller_Onboarding_Task__c AORloopvariable2:lstTask1 )
        {
          AORloopvariable2.EMC_Approval_Status__c='Approved';
          mapauthnew.put(AORloopvariable2.Id,AORloopvariable2);
        }
                  
        objPOB.updatePOBStatus(mapauthnew,mapauth);
        objPOB.updatePOBStatus(mapauth,mapauthnew);
        OnBoarding.Email__c='TestPI@customsetting.com';
        update OnBoarding;
        objPOB.SendEmail(lstPOBrec,OnBoarding,True);
        OnBoarding.Owner__c='00G70000001lJh2';
         OnBoarding.Email__c= null;
        update OnBoarding;
        objPOB.SendEmail(lstPOBrec,OnBoarding,True);
        Test.stopTest();
      }
    }    
        
}