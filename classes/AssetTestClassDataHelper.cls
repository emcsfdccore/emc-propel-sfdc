public class AssetTestClassDataHelper {
    public static List<Asset__c> createAsset(Account acc){
    List<Asset__c> assetList = new List<Asset__c>();
         Asset__c asset1= new Asset__c();
        asset1.Name='TestassetComptitve12';
        if (acc != null)
        asset1.Customer_Name__c=acc.id;
        asset1.Product_Name_Vendor__c='Dell';
        assetList.add(asset1);

        //Creating EMC asset
        Asset__c asset2= new Asset__c();
        asset2.Name='TestassetEMCasst23';
        asset2.Customer_Name__c=acc.id;
        asset2.Product_Name_Vendor__c='EMC';
        assetList.add(asset2);

        Asset__c asset3= new Asset__c();
        asset3.Name='TestCompetetive';
        if (acc != null)
        asset3.Customer_Name__c=acc.id;
        asset3.Sales_Plan__c='Action Required';
        asset3.Product_Name_Vendor__c='Dell';
        assetList.add(asset3);
        return assetList;
    }
}