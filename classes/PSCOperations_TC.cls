@IsTest
public class PSCOperations_TC
{
    public static testmethod void PSCOperationsTest()
    {
     System.runAs(new user(Id = UserInfo.getUserId()))
            {
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
                CustomSettingDataHelper.dealRegistrationCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.partnerBoardingCSData();
                CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
                CustomSettingDataHelper.VCEStaticCSData();
                CustomSettingDataHelper.PSCCaseTypeImageData();
                CustomSettingDataHelper.PSCFieldMappingData();
                CustomSettingDataHelper.SC13LanguageData();
               
            }
            
            ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
            User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='testR2Ruser1@emailR2RTest.com', alias='tuser1', CommunityNickName='tuser1', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
            LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' ,Forecast_Group__c = 'Maintenance Renewals'); 
        
            insert insertUser;
        
            //Getting the PCS Case Record Type Id 
            
             Map<String,Schema.RecordTypeInfo> recordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName(); 
             final String casePCSRecordTypeId = recordTypes.get('PSC - Channel Systems Training').getRecordTypeId();
            
            // create account record.
            List<Account> acc = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();  
            insert acc;
            
            // Create Opportunity Record.
            list<Opportunity>  listOpt = new list<Opportunity>();
            listOpt.add(new Opportunity(Name = 'TestOpp0',AccountId = acc[0].Id, CloseDate = system.today() + 5, StageName = 'Pipeline',Sales_Channel__c='Direct',Sales_Force__c='EMC', Opportunity_Number__c = 'Test-1234'));
            insert listOpt;
            
           
            
             List<RecordType> DealRegRecordType= [select id,DeveloperName,Name from RecordType where RecordType.Name = 'Deal Registration' ];
             
            //Create Lead records.
             list<Lead> listLead = new list<lead> ();
             listLead.add(new lead(Company='TestLead' ,LastName='Test0', Status='Working',Sales_Force__c='EMC',Lead_Originator__c='Customer Intelligence', DealReg_Deal_Registration__c = true, RecordTypeId = DealRegRecordType[0].id, Partner__c = acc[0].id, DealReg_Deal_Description__c = 'Test', DealReg_Deal_Registration_Justification__c = 'Test', Additional_Field_Approver_1__c = 'bansal.garima@emc.com' , 
             Additional_Field_Approver_2__c = 'bansal.garima@emc.com', Field_Approver_Manager__c = 'bansal.garima@emc.com', Field_Approver__c = 'bansal.garima@emc.com', Partner_Manager__c = insertUser.id, Additional_Notification_Email_1__c = insertUser.id)); 
             insert listLead;
             String leadNumber= [Select id, Lead_Number__c from Lead where id = : listLead limit 1].Lead_Number__c;
             
              test.starttest();
             
            // Adding case records.
             list<case> listcases= new list<case>();
             listcases.add(new case( Origin ='Community', STATUS = 'New' , parentid = null, SUBJECT='Field Approval Request for Deal Registration',Type='Program and Process Queries', Deal_Registration_Number__c=leadNumber ,DESCRIPTION = 'Testing description2',Legal_Company_Name__c='test',RecordTypeId = casePCSRecordTypeId));
             listcases.add(new case( Origin ='Community', STATUS = 'New' , parentid = null, SUBJECT='Field Approval Request for Deal Registration',Type='Program and Process Queries', Opp_Number__c= listOpt[0].Opportunity_Number__c ,DESCRIPTION = 'Testing description2',Legal_Company_Name__c='test',RecordTypeId = casePCSRecordTypeId));
             insert listcases;
             
            test.stoptest();
    }
}