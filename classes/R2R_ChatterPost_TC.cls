/*=======================================================================================================+
|  HISTORY  |                                                                           
|  DATE          DEVELOPER        WR                    DESCRIPTION                               
|  ====          =========        ==                    =========== 
|  25-Nov-2014   Garima Bansal           R2R Dec'14 Rel - Modified the TC to cover the Code coverage.          
+=====================================================================================================*/

@IsTest(SeeAllData=true)
Public Class R2R_ChatterPost_TC{
static R2R_ChatterPost r2rChatterpostObj= new R2R_ChatterPost();

public static testmethod void testAfterUpdateFeedItems(){
        // Creating User record (this user will be the "core quota rep" for the account)
        User insertUser = new user(email='test-user@emailTest.com',profileId = '00e70000000wBzaAAE' ,  UserName='testR2Ruser2@emailR2R2Test.com', alias='tuser1', CommunityNickName='tuser1', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' ,Forecast_Group__c = 'Maintenance Renewals'); 
        
        insert insertUser;   

        List<RecordType> recordType1= [select id,DeveloperName,Name from RecordType where RecordType.DeveloperName like 'EMC_Install' or Name Like 'Competitive_Install'limit 2];
         
        //Creating Account record
        List<Account> listAccount= new List<Account>();
          Account objAct = new Account();
          objAct.Name='Account1';
          objAct.Status__c='A';
          objAct.Core_Quota_Rep__c= insertUser.id ;        
        
        listAccount.add(objAct);
        insert listAccount;
        
        //Creating Opportunity record
        List<Opportunity> listOpportunity = new List<Opportunity>();
         for(Integer i=0; i<2;i++)
         {   
            Opportunity opp = new Opportunity(Name='Opt'+i,AccountId=listAccount[0].Id,StageName='Pipeline',
            CloseDate=system.today() + 10,Sales_Channel__c='Direct',Sales_Force__c='EMC');
            listOpportunity .add(opp);
         }
       
         insert listOpportunity ;
            
         // Creating Asset Records
         List <Asset__c> listAssets = new List <Asset__c> ();
             Asset__c assetuser1= new Asset__c();
             assetuser1.Name='Testasset1';
             assetuser1.RecordTypeId=recordType1[1].id;
             assetuser1.Customer_Name__c=listAccount[0].id;
             assetuser1.Contract_Number__c='abcd111qqq';  
             listAssets.add(assetuser1);
            
             Asset__c assetuser2= new Asset__c();
             assetuser2.Name='Testasset1';
             assetuser2.RecordTypeId=recordType1[1].id;
             assetuser2.Customer_Name__c=listAccount[0].id;
             assetuser2.Contract_Number__c='abcd111qqq'; 
             listAssets.add(assetuser2);
             
             // added asset records to check chatter post on Asst records where the Sales Strategy changes to 'Deferred � Non-renewable' Or 'Refresh Deferred'
            Asset__c asset3= new Asset__c();
            asset3.Name='TestassetEMC2';
            asset3.Customer_Name__c= listAccount[0].id;
            if(recordType1[0].Name == 'EMC_Install')
            {
                asset3.RecordTypeId=recordType1[0].id;
            }
            else
            {
                asset3.RecordTypeId=recordType1[1].id;
            }
            asset3.Product_Name_Vendor__c='EMC';
            asset3.SMS_Related_Asset__c = 'Yes';
            listAssets.add(asset3);
            
            Asset__c asset4= new Asset__c();
            asset4.Name='TestassetEMC2';
            asset4.Customer_Name__c= listAccount[0].id;
            if(recordType1[0].Name == 'EMC_Install')
            {
                asset4.RecordTypeId=recordType1[0].id;
            }
            else
            {
                asset4.RecordTypeId=recordType1[1].id;
            }
            asset4.Product_Name_Vendor__c='EMC';
            listAssets.add(asset4);
            
            insert listAssets;
             
            asset3.Deffered_to_Renewals__c = true;
            asset4.Deffered_to_Renewals__c = true;
            update listAssets ;
             
          // Creating OAJ records  
          Opportunity_Asset_Junction__c oaj1=new Opportunity_Asset_Junction__c();
              oaj1.Related_Asset__c=listAssets[0].id;
              oaj1.Related_Account__c=listAccount[0].Id;
              oaj1.Related_Opportunity__c=listOpportunity[0].id;
              insert oaj1;
         
          Opportunity_Asset_Junction__c oaj2=new Opportunity_Asset_Junction__c();
              oaj2.Related_Asset__c=listAssets[1].id;
              oaj2.Related_Account__c=listAccount[0].Id;
              oaj2.Related_Opportunity__c=listOpportunity[1].id;
              insert oaj2;
                
          listOpportunity[0].StageName = 'Commit';
          listOpportunity[1].StageName ='Pipeline';
          Update listOpportunity;
        
        }
}