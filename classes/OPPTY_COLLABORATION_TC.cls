/*=====================================================================================================+
|   DATE          DEVELOPER     WR         DESCRIPTION  |  
|   ====          =========     ==         ===========  |
|  03/April/2014    Bhanuprakash            This class is used to unit test all classes created for OPPORTUNITY COLLABORATION PROJECT
|  18-Sep-2014    Akash Rastogi 1229        Updated test class to increase the code coverage of main class 'OPPTY_COLLABORATION_Util'           
|  04-Dec_2014    Vinod Jetti   1340        Updated Test class for code coverage of added componets in the 'OPPTY_COLLABORATION_Registration'
|  20-Jan-2015    Akash Rastogi 1611        Updated test class to cover "PopulateOpportunityOwner","DealGroupingUpdateTrigger" and "Opp_UpdateOwner" class
|  23-April-2015  Vinod Jetti   #1825       Updated code to coverage of added componets in the 'OPPTY_COLLABORATION_Registration' class
+=====================================================================================================*/
@isTest(seeAllData=true)
private class OPPTY_COLLABORATION_TC{
  static Opportunity_Registration__c oppReg;
  static  Account objAccount;
  static Contact objContact;
  static Opportunity objOpportunity;
  static User useR;
  static List<Opportunity_Registration__c> listOpptyReg = new List<Opportunity_Registration__c>();
  static Profile p = [SELECT Id FROM Profile WHERE Name='APJ Distributor Super User' limit 1];
  static User usrAdmin = [Select Id, Name from User where isActive= true AND Name Like 'Eag%' AND Profile.Name = 'System Administrator' LIMIT 1];
  //WR# 1611 Changes
  Static List<Deal_Grouping__c> lstDealGrouping = new List<Deal_Grouping__c>();
  
  
  
/* @Method <This method is used create Partner accounts for the test class>
@param <Taking string as paramter which contains account name>
@return <void> - <Account record>
@throws exception - <No Exception>
*/

//Added for Fixing party number issue

    public static String generateRandomString(Integer len) {
    final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    String randStr = '';
    while (randStr.length() < len) {
       Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
       randStr += chars.substring(idx, idx+1);
    }
    return randStr; 
}


  static Account createAccount(String accountName){
    Map<String,Schema.RecordTypeInfo> recordTypes = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id accRecordTypeId = recordTypes.get('Partner Account Record Type').getRecordTypeId();
      
        objAccount = new Account(
            name = accountName,
            Party_Number__c = OPPTY_COLLABORATION_TC.generateRandomString(15),
            BillingCountry ='Colombia',
            Lead_Oppty_Enabled__c = true,
            Synergy_Account_Number__c = '10',
            Partner_Type__c ='Distributor',
            Type = 'Partner' ,
            PROFILED_ACCOUNT_FLAG__c = true,
            recordtypeid= accRecordTypeId
        );
        
        insert objAccount;      
        return objAccount;
    }
    /* @Method <This method is used create Partner contacts for the test class>
    @param <Taking string as paramter which contains account name>
    @return <void> - <Account record>
    @throws exception - <No Exception>
    */

  static Contact createContact(String contactName){
    Map<String,Schema.RecordTypeInfo> recordTypes = Contact.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id accRecordTypeId = recordTypes.get('Partner Contact').getRecordTypeId();
        //Account acc =  createAccount('TEST ACCOUNT');      
         objContact = new Contact(
            LastName = contactName,
            accountId = objAccount.Id,
            recordtypeid= accRecordTypeId,
            MobilePhone = '7788562358'
        );
        insert objContact;      
        return objContact;
    }
/* @Method <This method is used create opportunity records for the test class>
@param <Taking string as paramter which contains opportunity name>
@return <void> - <Account record>
@throws exception - <No Exception>
*/

    
  static Opportunity createOpportunity(String oppName){
    Map<String,Schema.RecordTypeInfo> recordTypes = Opportunity.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id accRecordTypeId = recordTypes.get('New Manually Created Record Type').getRecordTypeId();
        //Account acc =  createAccount('TEST ACCOUNT');  
        
         objOpportunity = new Opportunity(
            name = oppName,
            AccountId = objAccount.Id,
            StageName = 'Pipeline',
            CloseDate = System.Today(),
            Sales_Channel__c = 'Indirect',
            Sales_Force__c = 'EMC', 
            Business_Need__c ='Archiving',
            Budget__c = 'Unfunded' ,
            Executive_Sponsor_Key_Players__c = 'TEST',
            recordtypeid= accRecordTypeId,
            Timeline__c = '10+ Months',
            Lead_Level__c = 'Level 4',
            Partner__c = objAccount.Id,
             Is_this_a_VSPEX_Opportunity_Partner__c='YES',
            Partner_Expected_Deal_Value__c = 123,
            Partner_Expected_Close_Date__c = System.Today(),
            //WR# 1611 Changes
            Deal_Grouping_Name__c = lstDealGrouping[0].id
            
        );
        insert objOpportunity;    
        List<Opportunity> lstOpty = [select Id,Deal_Grouping_Next_Steps__c,Deal_Grouping_Name__c  from Opportunity where Deal_Grouping_Name__c=:lstDealGrouping[0].ID];
        System.debug('This is test--->'+lstOpty);
        return objOpportunity;
    }
    static void createTestData(){
        
        Contact con = createContact('TEST CONTACT');
        useR = new User(Alias = 'standt', Email='test@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingUserOpp', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='testOpp@testorg.com',ContactId=con.Id);
        //insert useR;
        // useR = [SELECT Id FROM User WHERE IsActive = true AND UserType = 'PowerPartner' LIMIT 1];
        
        useR = UserProfileTestClassDataHelper.createPortalUser(p.id,null,con.Id);
    
        List<User> lstPartners = [select id, ContactId, name from user where ProfileId = :p.id AND isActive = true AND UserType = 'PowerPartner' limit 25];
        List<Contact> lstPartnerContact = new List<Contact>();
        List<Id> lstId = new List<Id>();

        for (User u : lstPartners) 
        {
            lstId.add(u.ContactId);
        }

        if(lstPartners != null)
        {
            lstPartnerContact = [select AccountId, id from Contact where id = :lstId.get(0) AND Partner_Contact__c = 'True' AND Partner_User2__c = true AND Account.IsPartner = true limit 50000];

            useR = lstPartners.get(0);
            con = lstPartnerContact.get(0);
            objAccount = new Account(id = con.AccountId);
        }

        
        //WR# 1611 Changes starts
        for(Integer i=0 ; i<=2 ; i++){
        Deal_Grouping__c dg = new Deal_Grouping__c();
        dg.Name = 'abc'+i;
        dg.Deal_Grouping_Next_Steps__c = 'xyz'+i;
        dg.Deal_Grouping_Description__c = 'asd'+i;
        lstDealGrouping.add(dg);
        }
        
        insert lstDealGrouping;
        
        
        
        Opp_UpdateOwner.isExecuted = false;
        
        Opportunity opp = createOpportunity('TEST OPPTY');
        opp.Deal_Grouping_Name__c = lstDealGrouping[1].id;
        update opp;
        
        Opp_UpdateOwner.isExecuted = false;
        
        opp.Deal_Grouping_Name__c = null;
        update opp;
        //WR# 1611 Changes ends
        oppReg = new Opportunity_Registration__c();
        oppReg.Deal_Description__c = 'TEST DATA';
        oppReg.Approving_Partner_User__c = useR.Id;
        oppReg.Related_Opportunity__c = opp.Id;
        oppReg.Registration_Type__c = 'Base';
        oppReg.Customer_Name__c = 'TEST';
        oppReg.Customer_Phone_Number__c = '4235235235';
        oppReg.Customer_Email__c = 'test@test.com';
        oppReg.Distributor_Direct_Reseller_lookup__c = objAccount.id;
        oppReg.Customer_Name__c = objAccount.Id;
        oppReg.Partner_Approval_Status__c = 'Rejected';
        oppReg.Partner_Rejection_Reason__c = 'No Opportunity';
        oppReg.Partner_Comments__c = 'Test';
        oppReg.Status__c = 'New - Unsubmitted';
        //added for #1340 
        oppReg.Is_this_a_Cross_Border_Deal__c = 'Yes';
        oppReg.In_Which_Countries__c = 'Afghanistan;Aland Islands';
        //added for #1641
        oppReg.Distributor_In_Origin_Country__c = objAccount.Id;
        //added for #1825
        oppReg.Is_this_an_EHC_deal__c = True;
 
        insert oppReg;   
    }
    



     static testmethod void testUtil_AdditionalEmail(){

        Test.startTest();       
        createAccount('TEST ACCOUNT');
        createTestData();
        
        //WR# 1611 Changes 
        try{
        Opp_UpdateOwner.isExecuted = false;
        lstDealGrouping[0].Deal_Grouping_Next_Steps__c = 'xyz888';
        Opp_UpdateOwner.isFailed = true;
        update lstDealGrouping[0];
        }Catch(Exception e)
        {   
            System.debug('Exception Message-->>>' + e.getMessage());
        }
        //WR# 1611 Changes ends

        Test.stopTest();  
    }
}