/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  20 Aug 2014     Sneha Jain                 PROPEL SAP CRM OB Core SFDC Opportunity I.006 - Initial Creation
 |  01 Oct 2014     Bhanuprakash    PROPEL     Added 'testRenewalOpptyCreation' method
 +=========================================================================================================================*/

@isTest
private class OpportunityQuoteHelperTest
{
    static testMethod void quoteDelinkMethod()
    {      
        //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.opportunityIntegrationCSData();
            CustomSettingDataHelper.VCEStaticCSData();
        }
        //Create CS
        Propel_General_Settings__c propelSetting = new Propel_General_Settings__c();
        propelSetting.Indirect_Distribution_Channels__c = 'Distributor;Tier 1 Reseller;Service Provider';
        propelSetting.Direct_Distribution_Channels__c ='Direct';
        propelSetting.OEM_Distribution_Channels__c = 'OEM';
        insert propelSetting;
        
        //Insert a user record
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='test-user1@emailTest.com', alias='tuser11', CommunityNickName='tuser11', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User'); 
        
        insert insertUser;
        
        System.runAs(insertUser)
        {
            
            //Insert Account records
            List<Account> accList = new List<Account>();
            accList.add(new Account(Name = 'TestAcount0',Partner_Quoting_Account__c = true,Party_Number__c='111',ShippingCountry = 'United States'));
            accList.add(new Account(Name = 'TestAcount1',Partner_Quoting_Account__c = true,Party_Number__c= '222',ShippingCountry = 'United States'));
            accList.add(new Account(Name = 'TestAcount3',Partner_Quoting_Account__c = true,Party_Number__c= '333',ShippingCountry = 'United States'));
            accList.add(new Account(Name = 'TestAcount4',Partner_Quoting_Account__c = true,Party_Number__c= '444',ShippingCountry = 'United States'));
            insert accList;
            
            //Insert Contact records
            List<Contact> contactList = new List<Contact>();
            contactList.add(new Contact(Lastname = 'Test Contact', AccountId = accList[0].Id));
            insert contactList;
            
            //Insert Opportunity records
            List<Opportunity> oppList = new List<Opportunity>();
            oppList.add(new Opportunity (Name = 'Test Opportunity ', AccountId = accList[0].id, Sales_Force__c = 'EMC',CurrencyIsoCode = 'USD', StageName = 'Pipeline',District_Manager__c = contactList[0].id,Products__c = 'Test',CloseDate = System.today(),Amount = 500, Opportunity_Number__c = '9999999',Quote_Cart_Number__c = '1000'));
            oppList.add(new Opportunity (Name = 'Test Opportunity 1', AccountId = accList[1].id, Sales_Force__c = 'EMC',CurrencyIsoCode = 'USD', StageName = 'Pipeline',District_Manager__c = contactList[0].id,Products__c = 'Test1',CloseDate = System.today(),Amount = 600, Opportunity_Number__c = '8888888',Quote_Cart_Number__c = '1000'));
            oppList.add(new Opportunity (Name = 'Test Opportunity 2', AccountId = accList[2].id, Sales_Force__c = 'EMC',CurrencyIsoCode = 'USD', StageName = 'Pipeline',District_Manager__c = contactList[0].id,Products__c = 'Test2',CloseDate = System.today(),Amount = 700, Opportunity_Number__c = '7777777'));
            oppList.add(new Opportunity (Name = 'Test Opportunity 3', AccountId = accList[3].id, Sales_Force__c = 'EMC',CurrencyIsoCode = 'USD', StageName = 'Pipeline',District_Manager__c = contactList[0].id,Products__c = 'Test3',CloseDate = System.today(),Amount = 800, Opportunity_Number__c = '6666666',Quote_Cart_Number__c = '3000'));
            
            insert oppList;
            List<Opportunity> oppListQueried = new List<Opportunity>();
            oppListQueried = [ Select Id,Name,AccountId,Sales_Force__c,CurrencyIsoCode,StageName,District_Manager__c,Products__c,Amount,Opportunity_Number__c,Quote_Cart_Number__c from Opportunity order by Name];

            
            //Prepare Case related data
            OpportunityQuoteMain.CaseData csData = new OpportunityQuoteMain.CaseData();
            csData.distiDirectResellerPartyNumber = '222';
            csData.distributionVARPartyNumber = '222';
            csData.billToParty = '222';
            csData.shipToParty = '222';
            csData.installAtParty = '222';
            csData.orderType = 'Test';
            csData.orderCurrency = 'USD';
            csData.orderValueWoutFreight = 100.00;
            csData.purchaseOrderNumbers = 'Test';
            csData.quoteSubmissionDateTime = system.today();
            csData.contactEmail = 'Test@emailTest.com';
            csData.oracleEmailAlias = 'Test';
            csData.quoteDistrict = 'Test';
            csData.OrderDetailsCompleteABAFlag = false;
            //Data for De-link scenario - No opportunity # passed in payload
            OpportunityQuoteMain.OpportunityUpdateData oppData = new OpportunityQuoteMain.OpportunityUpdateData();
            
            oppData.opportunityNumber = '';
            oppData.accountPartyNumber = '111';
            oppData.tier2PartnerPartyNum = '111';
            oppData.quoteType = 'Test';
            oppData.quoteCartNumber = '1000';
            oppData.salesOrg = 'Test';
            oppData.quoteVersion = 'Test';
            oppData.quoteStatus = 'Quote Submitted';
            oppData.soNumber = '999';
            oppData.closeDate = system.today() + 5;
            oppData.orderCurrency = 'USD';
            oppData.priceFloor = 'Test';
            oppData.HWRevenue = 100.00;
            oppData.SWRevenue = 100.00;
            oppData.servicesRevenue = 100.00;
            oppData.prepaidHWMARevenue = 100.00;
            oppData.prepaidSWMARevenue = 100.00;
            oppData.HWWarrantyUpgradeRevenue = 100.00;
            oppData.partsRetentionRevenue = 100.00;
            oppData.warrantyRevenue = 100.00;
            oppData.HWDiscount = 110.00;
            oppData.SWDiscount = 110.00;
            oppData.servicesDiscount = 110.00;
            oppData.prepaidHWMADiscount = 110.00;
            oppData.prepaidSWMADiscount = 110.00;
            oppData.HWWarrantyUpgradeDiscount = 110.00;
            oppData.partsRetentionDiscount = 110.00;
            oppData.warrantyDiscount = 110.00;
            oppData.distributionChannel = 'Direct';
            oppData.redirectValue = null;
            
            //Case related data
            List<OpportunityQuoteMain.CaseData> lstcaseData = new List<OpportunityQuoteMain.CaseData>();
            lstcaseData.add(csData);
            oppData.caseData = lstcaseData;
            
            Test.startTest();
            
            OpportunityQuoteMain.StatusResponse sr = new OpportunityQuoteMain.StatusResponse();
            sr = OpportunityQuoteMain.updateOpportunityData(oppData);
   
            Test.stopTest();
        }   
    }    
    
   static testMethod void caseUpdateMethod()
    {
        //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.VCEStaticCSData();
        }
        Propel_General_Settings__c propelSetting = new Propel_General_Settings__c();
        propelSetting.Indirect_Distribution_Channels__c = 'Distributor;Tier 1 Reseller;Service Provider';
        propelSetting.Direct_Distribution_Channels__c ='Direct';
        propelSetting.OEM_Distribution_Channels__c = 'OEM';
        insert propelSetting;
        
        //Insert a user record
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='test-user1@emailTest.com', alias='tuser11', CommunityNickName='tuser11', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User'); 
        
        insert insertUser;
        
        System.runAs(insertUser)
        {
            //Insert Account records
            List<Account> accList = new List<Account>();
            accList.add(new Account(Name = 'TestAcount0',Partner_Quoting_Account__c = true,Party_Number__c='111'));
            accList.add(new Account(Name = 'TestAcount1',Partner_Quoting_Account__c = true,Party_Number__c= '222'));
            accList.add(new Account(Name = 'TestAcount3',Partner_Quoting_Account__c = true,Party_Number__c= '101010'));
            insert accList;
            
            //Insert Contact records
            List<Contact> contactList = new List<Contact>();
            contactList.add(new Contact(Lastname = 'Test Contact', AccountId = accList[0].Id));
            insert contactList;
            
            //Insert Opportunity records
            List<Opportunity> oppList = new List<Opportunity>();
            oppList.add(new Opportunity (Name = 'Test Opportunity ', AccountId = accList[0].id, Sales_Force__c = 'EMC',CurrencyIsoCode = 'USD', StageName = 'Pipeline',District_Manager__c = contactList[0].id,Products__c = 'Test',CloseDate = System.today(),Amount = 500, Opportunity_Number__c = '9999999',Quote_Cart_Number__c = '4000'));
            
            oppList.add(new Opportunity (Name = 'Test Opportunity2 ', AccountId = accList[1].id, Sales_Force__c = 'EMC',CurrencyIsoCode = 'USD', StageName = 'Pipeline',District_Manager__c = contactList[0].id,Products__c = 'Test2',CloseDate = System.today(),Amount = 5000, Quote_Cart_Number__c = '5000'));
            
            insert oppList;
            
            List<Opportunity> oppListQueried = new List<Opportunity>();
            oppListQueried = [Select Id,Name,AccountId,Sales_Force__c,CurrencyIsoCode,StageName,District_Manager__c,Products__c,Amount,Opportunity_Number__c,Quote_Cart_Number__c from Opportunity order by Name LIMIT 5];
            //System.debug('**** 
            //Create Case records
            Id caseGROrecTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('Global Revenue Operations').getRecordTypeId();
            List<Case> caseList = new List<Case>();
            caseList.add(new Case(STATUS = 'Open',Processing_Status__c = 'open' , TYPE = 'GRO Orders *GRO Internal Only*',Origin ='System',parentid = null ,SUBJECT='Quote #' ,DESCRIPTION = 'Testing Insert scenario of case :1', RecordTypeId = caseGROrecTypeId,Sales_Order_Number__c = '123',Quote_Number__c = '4000'));
            caseList.add(new Case(STATUS = 'Open',Processing_Status__c = 'open' , TYPE = 'GRO Orders *GRO Internal Only*',Origin ='System',parentid = null ,SUBJECT='Quote #' ,DESCRIPTION = 'Testing Insert scenario of case :2', RecordTypeId = caseGROrecTypeId,Sales_Order_Number__c = '1234',Quote_Number__c = '5000'));
            
            insert caseList;
            
            //Prepare Case related data
            OpportunityQuoteMain.CaseData csData = new OpportunityQuoteMain.CaseData();
            csData.distiDirectResellerPartyNumber = '111';
            
            csData.billToParty = '111';
            csData.shipToParty = '111';
            csData.installAtParty = '111';
            csData.orderType = 'EVAL';
            csData.orderCurrency = 'USD';
            csData.orderValueWoutFreight = 100.00;
            csData.purchaseOrderNumbers = 'Test';
            csData.quoteSubmissionDateTime = system.today();
            csData.contactEmail = 'Test@emailTest.com';
            csData.oracleEmailAlias = 'Test';
            csData.quoteDistrict = 'Test';
            csData.soldToParty = '111';
            
            //Data for update opportunity scenario - Opportunity # passed in payload -- Quote type = EVAL
            OpportunityQuoteMain.OpportunityUpdateData oppData2 = new OpportunityQuoteMain.OpportunityUpdateData();
            system.debug('----oppListQueried[2].Opportunity_Number__c'+oppListQueried[0].Opportunity_Number__c);
            
            
            //Prepare Line Item related Data
            OpportunityQuoteMain.LineItemData oliData2 = new OpportunityQuoteMain.LineItemData();
            oliData2.productName = 'Test Product 3';
            oliData2.quoteAmount = 500.00;
            OpportunityQuoteMain.LineItemData oliData3 = new OpportunityQuoteMain.LineItemData();
            oliData3.productName = 'Test Product 3';
            oliData3.quoteAmount = 500.00;
            OpportunityQuoteMain.LineItemData oliData4 = new OpportunityQuoteMain.LineItemData();
            oliData4.productName = null;
            oliData4.quoteAmount = 200.00;
            OpportunityQuoteMain.LineItemData oliData5 = new OpportunityQuoteMain.LineItemData();
            oliData5.productName = null;
            oliData5.quoteAmount = 220.00;
            
            List<OpportunityQuoteMain.CaseData> lstcaseData = new List<OpportunityQuoteMain.CaseData>();
            lstcaseData.add(csData);
            
            List<OpportunityQuoteMain.LineItemData> lstlineItemData2 = new List<OpportunityQuoteMain.LineItemData>();
            lstlineItemData2.add(oliData2);
            lstlineItemData2.add(oliData3);
            lstlineItemData2.add(oliData4);
            lstlineItemData2.add(oliData5);
            
            //Data for update opportunity scenario - Opportunity # passed in payload -- Quote type <> EVAL or PVAL
            OpportunityQuoteMain.OpportunityUpdateData oppData3 = new OpportunityQuoteMain.OpportunityUpdateData();
            system.debug('----oppListQueried[2].Opportunity_Number__c'+oppListQueried[1].Opportunity_Number__c);
            
            
            Test.startTest();
            //COVER CASE UPDATE METHOD
            oppData2.opportunityNumber = oppListQueried[0].Opportunity_Number__c;
            oppData2.quoteCartNumber = '4000';
            oppData2.redirectValue = 600;
            oppData2.quoteStatus = 'Submit to Order';
            oppData2.orderCurrency = 'EUR';
            oppData2.closeDate = system.today();
            oppData2.quoteType = 'EVAL';
                lstlineItemData2[0].productName = null;
            oppData2.LineItemData = lstlineItemData2;
            oppData2.caseData = lstcaseData;  
            oppData2.distributionChannel = 'Direct';
            OpportunityQuoteMain.StatusResponse sr = new OpportunityQuoteMain.StatusResponse();
            sr = OpportunityQuoteMain.updateOpportunityData(oppData2);
            
            
            // COVER CASE CREATE METUOD
            oppData3.opportunityNumber = oppListQueried[0].Opportunity_Number__c;
            oppData3.quoteCartNumber = '5000';
            oppData3.redirectValue = 600;
            oppData3.orderCurrency = 'EUR';
            oppData3.closeDate = system.today();
            oppData3.quoteType = 'Test';
            csData.soldToParty = '101010';
            oppData3.caseData = lstcaseData;
            oppData3.quoteStatus = 'Order Submitted';
            oppData3.caseData = lstcaseData; 
            oppData3.distributionChannel = 'Direct';
                lstlineItemData2[0].productName = null;
            oppData3.LineItemData = lstlineItemData2;
            
            sr = OpportunityQuoteMain.updateOpportunityData(oppData3);
            
            //Test 'quoteStatus' = Order Submitted
            //Data for update opportunity scenario - Opportunity # passed in payload -- Quote type <> EVAL or PVAL
            OpportunityQuoteMain.OpportunityUpdateData oppData4 = new OpportunityQuoteMain.OpportunityUpdateData();
           oppData4.opportunityNumber = oppListQueried[0].Opportunity_Number__c;
            oppData4.quoteCartNumber = '5000';
            oppData4.redirectValue = 600;
            oppData4.quoteStatus = 'Order Submitted';
            oppData4.orderCurrency = 'EUR';
            oppData4.closeDate = system.today();
            oppData4.quoteType = 'EVAL';
            oppData4.caseData = lstcaseData;
            oppData4.LineItemData = lstlineItemData2;
            oppData4.tier2PartnerPartyNum = '111';
            oppData4.distributionChannel = 'Distributor'; //change
            
            sr = OpportunityQuoteMain.updateOpportunityData(oppData4);
            Test.stopTest();
        }   
    }   
     
    static testMethod void updateOpptyMethod()
    {
        //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.opportunityIntegrationCSData();
        }
        
        //Insert a user record
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='test-user1@emailTest.com', alias='tuser11', CommunityNickName='tuser11', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User'); 
        
        insert insertUser;
        
        System.runAs(insertUser)
        {
            //Insert Account records
            List<Account> accList = new List<Account>();
            accList.add(new Account(Name = 'TestAcount0',Partner_Quoting_Account__c = true,Party_Number__c='111'));
            accList.add(new Account(Name = 'TestAcount1',Partner_Quoting_Account__c = true,Party_Number__c= '222'));
            accList.add(new Account(Name = 'TestAcount3',Partner_Quoting_Account__c = true,Party_Number__c= '333'));
            accList.add(new Account(Name = 'TestAcount4',Partner_Quoting_Account__c = true,Party_Number__c= '444'));
            insert accList;
            
            //Insert Contact records
            List<Contact> contactList = new List<Contact>();
            contactList.add(new Contact(Lastname = 'Test Contact', AccountId = accList[0].Id));
            insert contactList;
            
            //Insert Opportunity records
            List<Opportunity> oppList = new List<Opportunity>();
            oppList.add(new Opportunity (Name = 'Test Opportunity ', AccountId = accList[0].id, Sales_Force__c = 'EMC',CurrencyIsoCode = 'USD', StageName = 'Pipeline',District_Manager__c = contactList[0].id,Products__c = 'Test',CloseDate = System.today(),Amount = 500, Opportunity_Number__c = '9999999'));
           
            insert oppList;
            List<Opportunity> oppListQueried = new List<Opportunity>();
            oppListQueried = [ Select Id,Name,AccountId,Sales_Force__c,CurrencyIsoCode,StageName,District_Manager__c,Products__c,Amount,Opportunity_Number__c,Quote_Cart_Number__c from Opportunity order by Name];
            
            //Create Product
            List<Product2> prodList = new List<Product2>();
            prodList.add(new Product2(Name = 'Test Product 00',CurrencyIsoCode = 'USD',isActive = true));
            prodList.add(new Product2(Name = 'Test Product 01',CurrencyIsoCode = 'USD',isActive = true));
            insert prodList;
            
            //Create PriceBookEntry 
            //Pricebook2 pbId = [Select id from pricebook2 where name ='Standard Price Book'];
            CustomSettingDataValueMap__c sPB = new CustomSettingDataValueMap__c(name='Standard_PriceBook_Id',DataValue__c='01s70000000EkOZAA0');
            insert sPB;
            String pbId = sPB.DataValue__c;//'01s70000000EkOZAA0';
            List<PricebookEntry> pbeList = new List<PricebookEntry>();
           pbeList.add(new PricebookEntry(priceBook2Id = pbId,CurrencyIsoCode = 'USD', isActive = true, UnitPrice = 0, Product2Id=prodList[0].Id));
            pbeList.add(new PricebookEntry(priceBook2Id = pbId,CurrencyIsoCode = 'USD', isActive = true, UnitPrice = 0, Product2Id=prodList[1].Id));
            
            insert pbeList;
            
            //Create Line Item data
            List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
            oliList.add(new OpportunityLineItem(OpportunityId = oppList[0].Id,PricebookEntryId = pbeList[0].Id,Quote_Amount__c = 100));
            oliList.add(new OpportunityLineItem(OpportunityId = oppList[0].Id,PricebookEntryId = pbeList[1].Id,Quote_Amount__c = 200));
            insert oliList;
            
            oppList[0].Quote_Cart_Number__c = '9000';
            update oppList[0];
            
            //Data for De-link scenario - Opportunity # passed in payload
            OpportunityQuoteMain.OpportunityUpdateData oppData1 = new OpportunityQuoteMain.OpportunityUpdateData();
            oppData1.opportunityNumber = oppListQueried[0].Opportunity_Number__c;
            oppData1.quoteCartNumber = '9000';
            oppData1.redirectValue = 500;
            oppData1.quoteStatus = '';
            oppData1.tier2PartnerPartyNum = '333';
            oppData1.HWRevenue = 100.00;
            oppData1.SWRevenue = 100.00;
            oppData1.servicesRevenue = 100.00;
            oppData1.prepaidHWMARevenue = 100.00;
            oppData1.prepaidSWMARevenue = 100.00;
            oppData1.HWWarrantyUpgradeRevenue = 100.00;
            oppData1.partsRetentionRevenue = 100.00;
            oppData1.warrantyRevenue = 100.00;
            oppData1.HWDiscount = 110.00;
            oppData1.SWDiscount = 110.00;
            oppData1.servicesDiscount = 110.00;
            oppData1.prepaidHWMADiscount = 110.00;
            oppData1.prepaidSWMADiscount = 110.00;
            oppData1.HWWarrantyUpgradeDiscount = 110.00;
            oppData1.partsRetentionDiscount = 110.00;
            oppData1.warrantyDiscount = 110.00;
            oppData1.accountPartyNumber = '111';
            //Prepare Line Item related Data
            OpportunityQuoteMain.LineItemData oliData = new OpportunityQuoteMain.LineItemData();
            //oliData.productName = 'Test Product 1';
            oliData.quoteAmount = 200.00;
            OpportunityQuoteMain.LineItemData oliData2 = new OpportunityQuoteMain.LineItemData();
            oliData2.productName = 'Test Product 2';
            oliData2.quoteAmount = 200.00;
            
            OpportunityQuoteMain.LineItemData oliData3 = new OpportunityQuoteMain.LineItemData();
            oliData3.productName = 'Test Product 3';
            oliData3.quoteAmount = 230.00;
            
            List<OpportunityQuoteMain.LineItemData> lstlineItemData = new List<OpportunityQuoteMain.LineItemData>();
            lstlineItemData.add(oliData);
            lstlineItemData.add(oliData2);
            lstlineItemData.add(oliData3);
            oppData1.LineItemData = lstlineItemData;
            System.debug('*** oppData1.lineItemData = ' + oppData1.lineItemData);
            Test.startTest();
            
           OpportunityQuoteMain.StatusResponse sr = new OpportunityQuoteMain.StatusResponse();
            sr = OpportunityQuoteMain.updateOpportunityData(oppData1);
            
            oppData1.quoteStatus = 'Update Pending';
            sr = OpportunityQuoteMain.updateOpportunityData(oppData1);
            
            Test.stopTest();
        }
    }

    //Test Renewal Opportunity creation
    static testMethod void testRenewalOpptyCreation(){
    System.debug('*** testRenewalOpptyCreation start ');
    //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.opportunityIntegrationCSData();
        }
    //insert new CustomSettingBypassLogic__c(By_Pass_Opportunity_Triggers__c = false);
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='test1-user1@emailTest.com', alias='tuser121', CommunityNickName='tuser121', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test2', LastName = 'User2'); 
        insert insertUser;
        
        //Create Accounts
        List<Account> accList = new List<Account>();
        accList.add(new Account(Name = 'TestAcount0',Partner_Quoting_Account__c = true,Party_Number__c='111',ShippingCountry = 'United States'));
        accList.add(new Account(Name = 'TestAcount1',Partner_Quoting_Account__c = true,Party_Number__c='222',ShippingCountry = 'United States'));
        List<Account> lstAccHelp = AccountAndProfileTestClassDataHelper.CreateT2PartnerAccount();
        insert accList;
        
        //Create Product
        List<Product2> prodList = new List<Product2>();
        prodList.add(new Product2(Name = 'Test Product 00',CurrencyIsoCode = 'USD',isActive = true));
        prodList.add(new Product2(Name = 'Test Product 01',CurrencyIsoCode = 'USD',isActive = true));
        prodList.add(new Product2(Name = 'Test Product 02',CurrencyIsoCode = 'USD',isActive = true));
        insert prodList;
            
        //Create CS
        Propel_General_Settings__c propelSetting = new Propel_General_Settings__c();
        propelSetting.Indirect_Distribution_Channels__c = 'Distributor;Tier 1 Reseller;Service Provider';
        propelSetting.Direct_Distribution_Channels__c ='Direct';
        propelSetting.OEM_Distribution_Channels__c = 'OEM';
        insert propelSetting;
    
        //Renewal Opportunity test
        OpportunityQuoteMain.RenewalOpportunityData requestOppData = new OpportunityQuoteMain.RenewalOpportunityData();
        requestOppData.opportunityName = 'Test Oppty';
        requestOppData.accOwnerId = insertUser.Id;
        //requestOppData.salesChannel = 'Direct';//removed
        requestOppData.currencyCode = 'USA';
        requestOppData.leadSource = 'Manual';
        requestOppData.endCustomer = accList[0].Id;
        requestOppData.forecastStatus = 'Pipeline';
        requestOppData.closeDate = System.today();
        //requestOppData.salesForce = 'EMC';
        //requestOppData.districtReseller = accList[1].Id;//
        requestOppData.distributionVAR = accList[1].Id;
        
        List<OpportunityQuoteMain.RenewalOpportunityLineItemData> lineItems = new List<OpportunityQuoteMain.RenewalOpportunityLineItemData>();
        OpportunityQuoteMain.RenewalOpportunityLineItemData item1 = new OpportunityQuoteMain.RenewalOpportunityLineItemData();
        item1.productName = 'Test Product 00';
        item1.forecastAmt = 10.5;
        lineItems.add(item1);
        OpportunityQuoteMain.RenewalOpportunityLineItemData item2 = new OpportunityQuoteMain.RenewalOpportunityLineItemData();
        item2.productName = 'Test Product 01';
        item2.forecastAmt = 100.5;
        lineItems.add(item2);
        
        requestOppData.lstRenewalLineItems = lineItems;
        
    System.debug('*** calling  createRenewalOpportunity');
        OpportunityQuoteMain.StatusResponse res = new OpportunityQuoteMain.StatusResponse();
        res = OpportunityQuoteMain.createRenewalOpportunity(requestOppData);
    System.debug('*** call createRenewalOpportunity done! res = ' + res);
    
    requestOppData.opportunityName = '';
    res = OpportunityQuoteMain.createRenewalOpportunity(requestOppData);
    
    requestOppData.opportunityName = 'Test Oppty';
    requestOppData.closeDate = null;
    res = OpportunityQuoteMain.createRenewalOpportunity(requestOppData);
    
    requestOppData.closeDate = System.today();
        requestOppData.currencyCode = '';
    res = OpportunityQuoteMain.createRenewalOpportunity(requestOppData);
    
    requestOppData.currencyCode = 'USA';
        requestOppData.endCustomer = null;
    res = OpportunityQuoteMain.createRenewalOpportunity(requestOppData);
    
    
    requestOppData.endCustomer = accList[0].Id;
    requestOppData.accOwnerId = '0303030303fd';
    requestOppData.leadSource = 'Contract Renewal';
    res = OpportunityQuoteMain.createRenewalOpportunity(requestOppData);
    
    requestOppData.accOwnerId = insertUser.Id;
    requestOppData.distributionChannel = 'Distributor';
    requestOppData.districtReseller = '112';
    requestOppData.distributionVAR = '113';
    res = OpportunityQuoteMain.createRenewalOpportunity(requestOppData);
    
    
    requestOppData.distributionChannel = 'Direct';
    res = OpportunityQuoteMain.createRenewalOpportunity(requestOppData);
    
    requestOppData.distributionChannel = 'OEM';
    requestOppData.contractNum = '4321';
    //System.AssertNotEquals(res,null);
    }
}