/*=====================================================================================================+
|  HISTORY  |                                                                           

|  DATE          DEVELOPER                WR            DESCRIPTION                               

|  ====          =========                ==            =========== 
| 18-Feb-2014    Jaypal Nimesh		Backward Arrow		Created new Test class & removed test method from base class                                
+=====================================================================================================*/

@isTest
private class SurveyTestingUtil_TC {
	
	//Test method to call main class
    static testMethod void myUnitTest() {

	    Test.startTest();
	    
	    //Invoking constructor
	    SurveyTestingUtil tu = new SurveyTestingUtil();
	    
	    Test.stopTest();
    }
}