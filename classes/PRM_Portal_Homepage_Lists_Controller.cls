/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
|  09/Oct/2014   Srikrishna             BPP WR375032   Created controller class for PRM_Portal_Homepage_StandardUser_Lists page.
|
+=====================================================================================================*/
public with sharing class PRM_Portal_Homepage_Lists_Controller {
        
    //Public Setters and Getters
    public Integer numberOfOptyPages{set;get;}
    public Integer numberOfLeadPages{set;get;}
    public Integer numberOfDealPages{set;get;}
    //Setting default value for limit to avoid errors
    public integer queryLimit = 100;
    public boolean isSuperUser{get; set;}{isSuperUser=false;}
    
    public PRM_Portal_Homepage_Lists_Controller(){
        
        //Getting the query Limit from BPP custom setting
        BusinessPartnerProgram__c bppConfig = BusinessPartnerProgram__c.getValues('PortalHomePageQueryLimit');
        if(bppConfig != null){          
            queryLimit = integer.valueOf(bppConfig.APInames__c);
        }
        
        //Check the logged in user profile and Set the flag accordingly to render proper tables
        Profile p = [Select Id, Name from Profile where Id =: UserInfo.getProfileId()];
        String profileName = p.name;
        if(profileName.containsIgnoreCase('Partner User')){
            isSuperUser = false;
        }else if(profileName.containsIgnoreCase('Super User')){
            isSuperUser = true;
        }
    }
    
    //Initialize the StandardSetController for Opportunities and return with the Query result
    public ApexPages.StandardSetController oppController{
        get{
            if(oppController == null){
                oppController = new ApexPages.StandardSetController([SELECT Id, Opportunity_Number__c, Name, Account.name, Partner__c, Partner__r.Name,
                                                                    Tier_2_Partner__c, Tier_2_Partner__r.Name, PSC_Approval_Date_Time__c, Partner_Approval_Status__c, 
                                                                    Opportunity_Registration_Status__c FROM Opportunity 
                                                                    WHERE Partner_Approval_Status__c = 'Awaiting Response' order by Opportunity_Number__c asc limit :queryLimit]);
                oppController.setPageSize(10);
            }
            return oppController;
        }
        set;
    }
    
    //Pass results to OpportunityList variable
    public List<Opportunity> getOpportunityList(){
    
        if(Math.mod(oppController.getResultSize(), oppController.getPageSize()) > 0){
            numberOfOptyPages = oppController.getResultSize()/oppController.getPageSize() + 1;
        }else if(Math.mod(oppController.getResultSize(), oppController.getPageSize()) == 0){
            numberOfOptyPages = oppController.getResultSize()/oppController.getPageSize();
        }            
        return (List<Opportunity>)oppController.getRecords();
    }
    
    //Initialize the StandardSetController for Leads Pending Accept and return with the Query result
    public ApexPages.StandardSetController leadController{
        get{
            if(leadController == null){
                //Checks if flag is false then run query for Partner User
                if(!isSuperUser){
                    leadController = new ApexPages.StandardSetController([SELECT Name, Company, State, Campaign_Event_Name__c, Status, 
                                                                            Lead_Originator__c,createdDate,  IsUnreadByOwner, 
                                                                            Originator_Details__c, Lead_Number__c, Accept_Lead__c, Days_to_Accept__c 
                                                                            FROM LEAD WHERE Accept_Lead__c = false and isConverted__c = false and 
                                                                            isconverted = false and isClosed__c = false and 
                                                                            DealReg_Deal_Registration__c = false and OwnerId =: UserInfo.getuserId() limit :queryLimit]);
                }
                //Else run query for Super user data table
                else{
                    leadController = new ApexPages.StandardSetController([SELECT Name, Status, Createddate, Lead_Number__c, Accept_Lead__c, Lead_Rank__c,
                                                                            Passed_to_Partner_Date_Time__c, Company, State,  Partner__c, Partner__r.Name, 
                                                                            Tier_2_Partner__c, Tier_2_Partner__r.Name FROM LEAD 
                                                                            where isConverted__c = false AND Status not in ('Rejected to Marketing','Closed') 
                                                                            AND DealReg_Deal_Registration__c = false limit :queryLimit]);
                }

                leadController.setPageSize(10);
            }
            return leadController;
        }
        set;
    }
    
    //Pass results to LeadList variable
    public List<Lead> getLeadList(){
    
        if(Math.mod(leadController.getResultSize(), leadController.getPageSize()) > 0){
            numberOfLeadPages = leadController.getResultSize()/leadController.getPageSize() + 1;
        }else if(Math.mod(leadController.getResultSize(), leadController.getPageSize()) == 0){
            numberOfLeadPages = leadController.getResultSize()/leadController.getPageSize();
        }
        return (List<Lead>)leadController.getRecords();
    }

    //Initialize the StandardSetController for Lead Deal Registrations Expiring and return with the Query result
    public ApexPages.StandardSetController dealsController{
        get{
            if(dealsController == null){
                dealsController = new ApexPages.StandardSetController([SELECT Company, DealReg_Department_Project_Name__c, DealReg_Deal_Registration_Number__c, 
                                                                         Name, Country__c, 
                                                                        DealReg_Deal_Registration_Type__c, DealReg_Expiration_Date__c, Partner__c, Partner__r.Name,
                                                                        Tier_2_Partner__c, Tier_2_Partner__r.Name  
                                                                        FROM LEAD WHERE DealReg_Deal_Registration_Status__c = 'Approved'
                                                                        AND DealReg_Expiration_Date__c <= NEXT_N_DAYS:10
                                                                        AND DealReg_Deal_Registration__c = true 
                                                                        AND DealReg_Expiration_Date__c >= Today 
                                                                        AND isConverted = false 
                                                                        Order by DealReg_Expiration_Date__c limit :queryLimit]);
                dealsController.setPageSize(10);
            }
            return dealsController;
        }
        set;
    }
    
    //Pass results to DealsList variable
    public List<Lead> getDealsList(){
    
        if(Math.mod(dealsController.getResultSize(), dealsController.getPageSize()) > 0){
            numberOfDealPages = dealsController.getResultSize()/dealsController.getPageSize() + 1;
        }else if(Math.mod(dealsController.getResultSize(), dealsController.getPageSize()) == 0){
            numberOfDealPages = dealsController.getResultSize()/dealsController.getPageSize();
        }
        return (List<Lead>)dealsController.getRecords();
    }
}