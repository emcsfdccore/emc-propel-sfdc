@isTest
    public class Oppty_Search_IntegrationOperation_TC {
        //private static Oppty_Search_IntegrationOperation.Oppty_SearchData sData = new Oppty_Search_IntegrationOperation.Oppty_SearchData();
        public static testmethod void performHybrisSearchTest(){
     //Creating Custom Setting data ****************************** START *****************************
            System.runAs(new user(Id = UserInfo.getUserId()))
            {
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
            }
     //Creating Custom Setting data ****************************** END *****************************
        //Create District_Lookup__c : ********************* START ***********************
        District_Lookup__c dislokup = new District_Lookup__c(name='DisLUp 1');
        insert dislokup;
        System.debug('*** dislokup Id = ' + dislokup.Id);
        //Create District_Lookup__c : ********************* END ***********************
            //Create User & Account List : ********************* START ***********************
             List<Account> accList = new List<Account>();
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator - API Only' limit 1]; 
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, 
                  TimeZoneSidKey='America/Los_Angeles', UserName='TestUser@some.com');
                insert u;  
                System.debug('**** new user id = '+ u.Id);
                
                accList = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
                System.runAs(u) {
                    accList[0].UCID__C='11331015';
                    accList[0].District_Lookup__c=dislokup.Id;
                    accList[1].District_Lookup__c=dislokup.Id;
                    //accList[1].Account_District__c=dislokup.Id;//Not writeable field
                    insert accList;
                 }
                System.debug('**** accList size = '+ accList.size());
                System.debug('**** accList size = '+ accList[0].Lead_Oppty_Enabled__c);
            //Create User & Account List : ********************* END ***********************
        
            //Create Opportunity List : ********************* START ***********************
            List<Opportunity> lstTestOpps = new List<Opportunity>();
            List<Opportunity> opps = new List<Opportunity>(); 
            String accId = accList[0].Id;
            opps = createMoreOpportunities(accId);
            Opportunity opp1 = new Opportunity(Name = 'Test Oppty',
                                              StageName = 'Pipeline',
                                              Amount=100.5,
                                              CloseDate = System.today()+2,
                                              Sales_Force__c = 'EMC', 
                                              Sales_Channel__c = 'Distributor',
                                              VMWare_Related__c = 'Not VMware Related', 
                                              AccountId = accList[0].Id,
                                              Partner__c = accList[0].Id,
                                              Tier_2_Partner__c =accList[0].Id
                                              );
                                              
                                               
            Opportunity opp2 = new Opportunity(Name = 'Test Oppty2',
                                              StageName = 'Pipeline',
                                              Amount=100.5,
                                              CloseDate = System.today()+2,
                                              Sales_Force__c = 'EMC', 
                                              Sales_Channel__c = 'Distributor',
                                              VMWare_Related__c = 'Not VMware Related', 
                                              AccountId = accList[0].Id,
                                              Partner__c = accList[0].Id,
                                              Tier_2_Partner__c =accList[0].Id
                                              );
            opps.add(opp1);
            opps.add(opp2);
            System.debug('**** opps sise = ' + opps.size());
             System.runAs(u) {
                insert opps;
                }
            
            //Create Opportunity List : ********************* END ***********************
             // The following code runs as user 'u' 
             System.debug('Current User: ' + UserInfo.getUserName());
             System.debug('Current Profile: ' + UserInfo.getProfileId()); 
         //Create CS  
          Propel_General_Settings__c propelSetting = new Propel_General_Settings__c();
        propelSetting.Indirect_Distribution_Channels__c = 'Distributor;Tier 1 Reseller;Service Provider';
        propelSetting.Direct_Distribution_Channels__c ='Direct';
        propelSetting.OEM_Distribution_Channels__c = 'OEM';
        insert propelSetting;
        
            System.debug('**** Oppty = ' + opps);
            System.debug('**** Oppty.Opportunity_Number__c = ' + opps[0].Opportunity_Number__c);
            List<Opportunity> tempOpptylist1 = new List<Opportunity>();
            List<Opportunity> tempOpptylist2 = new List<Opportunity>();
            
            tempOpptylist1 =[SELECT  Name, Opportunity_Number__c,StageName,Amount,CurrencyIsoCode,Sales_Channel__c,CloseDate,Account_Name1__c,Tier_2_Partner__r.Name, Partner__r.UCID__C, account.District_Lookup__c,Tier_2_Partner__r.UCID__C FROM Opportunity];
            String oppNum= tempOpptylist1[0].Opportunity_Number__c;
            String partyNum = tempOpptylist1[0].Partner__r.UCID__C;
            String name = tempOpptylist1[0].Name;
            String distributionChannel = opps[0].Sales_Channel__c;
            String accDistric = tempOpptylist1[0].account.District_Lookup__c;
            String tier2PartyNum = tempOpptylist1[0].Tier_2_Partner__r.UCID__C;
            
            //System.debug('***oppNum = '+ oppNum + '  partyNum = ' + partyNum + '    name = ' + name + '   Sales_Channel__c = ' + salesChannel +  '    accDistric =' + accDistric + '    tier2PartyNum = ' + tier2PartyNum);
            tempOpptylist2 =[SELECT  Name, Opportunity_Number__c,StageName,Amount,CurrencyIsoCode,Sales_Channel__c,CloseDate,Account_Name1__c,Tier_2_Partner__r.Name FROM Opportunity where Opportunity_Number__c=:oppNum and Partner__r.UCID__C=:partyNum  AND name like :name AND account.District_Lookup__c=:accDistric AND Tier_2_Partner__r.UCID__C=:tier2PartyNum AND (StageName='Pipeline' OR StageName='Strong Upside' OR StageName='Upside' OR StageName='Won' OR StageName='Commit') ORDER BY name LIMIT 200];
            //AND Partner__r.UCID__C='11331015' AND name like 'Test Oppty%' AND (StageName='Pipeline' OR StageName='Strong Upside' OR StageName='Upside' OR StageName='Won' OR StageName='Commit') ORDER BY name LIMIT 200];
            System.debug('*** retrived oppty = ' +tempOpptylist2  );
            
        
        Test.startTest();
        Oppty_Search_IntegrationOperation.Oppty_SearchData sData = new Oppty_Search_IntegrationOperation.Oppty_SearchData();
        Oppty_Search_IntegrationOperation.Oppty_SearchData tempsData = new Oppty_Search_IntegrationOperation.Oppty_SearchData();
            sData.opptyName = 'Test';
            sData.opptyNo = oppNum;
            sData.endCustPartyNo = '11331015';
            sData.soldToPartyNo = accList[0].UCID__C;//'11331015';
            sData.tier2PartyNo = tier2PartyNum;//'';
            sData.distributionChannel = distributionChannel;//the Partner and Tier2 Partner queries are only executed if the distributionChannel is Distributor, Tier 1 Reseller or Service Provider
            sData.primarySalesRep = u.Id;//'00570000002iO2I';
            tempsData = sData;
            Oppty_Search_IntegrationOperation search = new Oppty_Search_IntegrationOperation();
            Oppty_Search_IntegrationOperation.Oppty_SearchResponse response = Oppty_Search_IntegrationOperation.performOpptySearch(sData);
            System.debug('**** result = ' + response);
            //**********************************************
            
            //Test without OpptyNo.
            tempsData.opptyNo='';
            response = Oppty_Search_IntegrationOperation.performOpptySearch(tempsData);
            System.debug('**** result = ' + response);
            //***********************************************
            
            //Test without soldToPartyNo ,endCustPartyNo
            tempsData.soldToPartyNo='';
            tempsData.endCustPartyNo ='';
            response = Oppty_Search_IntegrationOperation.performOpptySearch(tempsData);
            //End
                               
            //Test Mandatory field exception code-coverage.
            tempsData = sData;
            tempsData.distributionChannel='';
            response = Oppty_Search_IntegrationOperation.performOpptySearch(tempsData);
            
            tempsData = sData;
            sData.primarySalesRep='';
            response = Oppty_Search_IntegrationOperation.performOpptySearch(tempsData);
            
            tempsData = sData;
            tempsData.opptyNo=null;
            tempsData.soldToPartyNo=null;
            tempsData.endCustPartyNo=null;
            response = Oppty_Search_IntegrationOperation.performOpptySearch(tempsData);
            
            tempsData = sData;
            tempsData.opptyNo='';
            tempsData.endCustPartyNo='';
            response = Oppty_Search_IntegrationOperation.performOpptySearch(tempsData);
            
            //cover empty oppty list exception
            tempsData = sData;
            sData.primarySalesRep='';
            response = Oppty_Search_IntegrationOperation.performOpptySearch(tempsData);
            
            //Cover (if indirect values are null) condition in helper class
            propelSetting.Indirect_Distribution_Channels__c='';
            update propelSetting;
            tempsData = sData;
            response = Oppty_Search_IntegrationOperation.performOpptySearch(tempsData);
            
            //Cover (No opptyies found and No Oppties with User assess) code in helper class
            User dommyUser = new User(Alias = 'standt1', Email='standarduser1@testorg.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, 
                  TimeZoneSidKey='America/Los_Angeles', UserName='TestUser1@some.com');
                insert dommyUser;  
            
                                              
            tempsData.opptyName = 'Demo Oppty';//'Test';
            response = Oppty_Search_IntegrationOperation.performOpptySearch(tempsData);
            
            //Cover no user access oppties code in Helper class
            tempsData = sData;
            tempsData.primarySalesRep=dommyUser.Id;
            response = Oppty_Search_IntegrationOperation.performOpptySearch(tempsData);
            
            //Cover sales rep. id formate exception code in Helper class
            tempsData = sData;
            tempsData.primarySalesRep='ABC';
            response = Oppty_Search_IntegrationOperation.performOpptySearch(tempsData);
            
            Opportunity oppty1 = null;
            //Create more than 50 Opportunities list
            for(Integer i=0;i<52;i++){
                oppty1 = new Opportunity(Name = 'Test Oppty' + i,
                                              StageName = 'Pipeline',
                                              Amount=100.5,
                                              CloseDate = System.today()+12,
                                              Sales_Force__c = 'EMC', 
                                              Sales_Channel__c = 'Distributor',
                                              VMWare_Related__c = 'Not VMware Related', 
                                              AccountId = accList[0].Id,
                                              Partner__c = accList[0].Id
                                              );
                lstTestOpps.add(oppty1);
            }
             System.runAs(u) {
                insert lstTestOpps;
            }
            tempsData = sData;
            response = Oppty_Search_IntegrationOperation.performOpptySearch(tempsData);
        Test.stopTest();
            
        }
        public static List<Opportunity> createMoreOpportunities(String accId){
            List<Opportunity> opps = new List<Opportunity>();
            Opportunity opp;
            for(Integer i=0; i<= 5;i++){
                 opp = new Opportunity(Name = 'Test Oppty ' + i,
                                              StageName = 'Pipeline',
                                              Amount=100.5,
                                              CloseDate = System.today()+2,
                                              Sales_Force__c = 'EMC', 
                                              Sales_Channel__c = 'Distributor',
                                              VMWare_Related__c = 'Not VMware Related', 
                                              AccountId = accId,
                                              Partner__c = accId,
                                              Tier_2_Partner__c =accId);
                opps.add(opp);
            }
            return opps;
        }
    }