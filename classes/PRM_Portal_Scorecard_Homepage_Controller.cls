/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req     DESCRIPTION                               
 |  ====            =========       ======     ===========  
 |  09/08/2014      Keith Deglialberti         Copied from PRM_PAN_Country_Routing_Controller. Redirects Partner Portal Homepage 
 |                                             component to the VPP Scorecard without passing parameters through URL
 |  10/Dec/2014     Jaypal Nimesh   BPP Jan     Changes done to redirect to new scorecard on home page
 |  12/Feb/15       Jaypal Nimesh   BPP Feb     Changes made for existing issue for LB & Country level scorecard
+=============================================================================================================================*/
public class PRM_Portal_Scorecard_Homepage_Controller{
    public Id profileAccountId;
    User currentUser;
    public PRM_Portal_Scorecard_Homepage_Controller(){
       if(userinfo.getUserType() == 'PowerPartner'){                   
            currentUser = [Select u.ContactId, u.Contact.AccountId,contact.Account.PROFILED_ACCOUNT_FLAG__c,
                           contact.Account.Profiled_Account__c,u.Velocity_Access__c, u.Co_Op_Access__c,
                           contact.Account.Partner_Type__c
                           from User u
                           where id=:userinfo.getUserId()];
            if(ApexPages.currentPage().getParameters().get('Id')!=null){               
                if(ApexPages.currentPage().getParameters().get('Id').startswith('001')){               
                    profileAccountId = ApexPages.currentPage().getParameters().get('Id'); 
                }  
            }    
            if(currentUser.contact.AccountId != profileAccountId && profileAccountId !=null){
               profileAccountId = profileAccountId; 
            }               
            else if(currentUser.contact.Account.PROFILED_ACCOUNT_FLAG__c){
                profileAccountId = currentUser.contact.AccountId;
            } 
            else if(currentUser.contact.Account.Profiled_Account__c != null){
                profileAccountId = currentUser.contact.Account.Profiled_Account__c;
            }    
       }else{
           profileAccountId = ApexPages.currentPage().getParameters().get('Id');
           system.debug('profileAccountId --------->' +profileAccountId );
       }
    }
    public PageReference returnToPANORCountryPage(){
        //PageReference returnURL = new PageReference('/'+ profileAccountId);
        PageReference returnURL = null;
        Account profiledAccount = [Select Id,Account_Level__c from account where Id =: profileAccountId];
        system.debug('profiledAccount --------->' +profiledAccount );
        //Modified If condition - Jaypal BPP Feb 15
        if(userinfo.getuserType()=='PowerPartner' && (currentUser.contact.AccountId != profileAccountId 
           && currentUser.contact.Account.Profiled_Account__c != profileAccountId ) && profiledAccount.Account_Level__c == 'NONE'){
           ApexPages.Message errormessage = new ApexPages.Message(ApexPages.Severity.Error, System.Label.Insufficient_Access_To_VPP_Scorecards);
           ApexPages.addmessage(errormessage); 
           
        }   
        else if(profiledAccount.Account_Level__c != 'NONE'){
            //Changes made for Jan Release. Jaypal Nimesh, Redirect to new scorecard UI
            returnURL = new PageReference('/apex/PRM_VPP_Velocity_Scorecard?id='+profileAccountId);
        }

        else if(profiledAccount.Account_Level__c == 'NONE'){
            ApexPages.Message errormessage = new ApexPages.Message(ApexPages.Severity.Error, System.Label.Insufficient_Access_To_VPP_Scorecards);
           ApexPages.addmessage(errormessage); 
        }
        system.debug('returnURL --------->' +returnURL);
        return returnURL;
    }
}