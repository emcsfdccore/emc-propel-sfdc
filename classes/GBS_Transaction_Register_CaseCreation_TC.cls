@isTest
public class GBS_Transaction_Register_CaseCreation_TC{

  public static testmethod void checkTRInsertAndUpdate(){

      //Creating customer requirement test record
      Customer_Requirements__c testCustomerRecord = new Customer_Requirements__c();
      testCustomerRecord.name = 'test1234';
      testCustomerRecord.Customer_Name__c = 'Test Record';
      testCustomerRecord.Country_Code__c = 'ind111';
      insert testCustomerRecord;
      
      //creating custom setting test data
      CustomSettingDataHelper.VCEStaticCSData();
      
      //creating transaction register test record
      Transaction_Register__c transactionRegister = new Transaction_Register__c();
      transactionRegister.Name = 'Test transaction';
      transactionRegister.Company_Code__c = 'test code';
      transactionRegister.Invoice_Date__c = system.today();
      transactionRegister.CurrencyIsoCode = 'ARS';
      transactionRegister.CUSTOMER_ID__c = testCustomerRecord.name;
      transactionRegister.Cust_Number__c = '1234';
      transactionRegister.Cust_Name__c = 'Alok test record';
      transactionRegister.SO__c = 'test';
      insert transactionRegister ; //after insert trigger will be called
      
      //testing after update trigger
      transactionRegister.TRX_TYPE__c = 'PENDING';
      update transactionRegister;
      
      
      transactionRegister.TRX_TYPE__c = 'PROCESSED';
      update transactionRegister;
      
      transactionRegister.Rejected__c = true;
      update transactionRegister;
      
      transactionRegister.Rejected__c = false;
      transactionRegister.TRX_TYPE__c = null;
      update transactionRegister;
  
  
  
    }


}