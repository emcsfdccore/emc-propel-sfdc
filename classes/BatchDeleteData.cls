/*======================================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER                WR       DESCRIPTION                               
 |  ====          =========                ==       =========== 
 |  05/04/2011    Suman B                      		Delete data from Specified table. 
 |  14/04/2011    Suman B                      		Modified as per the change in requirement
 |  21/03/2013    Krishna Pydavula		241804     	commented the corntrigger code to exectue the scheduler for every weekly
 |												   	AND Aborting the existing batch jobs.
 |	10/04/2013	  Shipra Misra			248315		SFA - Concurrent Batch Job Fix
 |  25/06/2013 		Sravanthi           261008      To perform harddelete on staging table
 +=======================================================================================*/
global class BatchDeleteData implements Database.Batchable<sObject>,  Database.Stateful
{
 	public Boolean isTest=false;
	global String Query;
  	global String objectName ;
  	Map<String,CleanupTableData__c> mapTables = CleanupTableData__c.getall();  
  	global map<String,String> glbMapObjQuery = new map<String,String> ();
  	//261008
  	global map<String,String> glbmapforharddel = new map<String,String> ();
  	    
  	global BatchDeleteData (Map<String , String > mapQryObj )
  	{
	 	this.glbMapObjQuery.putAll(mapQryObj);
	 	system.debug('glbMapObjQuery###1==>'+glbMapObjQuery);
	}
	
    //create batch of records which is passed to execute method for processing
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
     	String currentDOW = Datetime.now().format('EEEE');
    	List<CleanupTableData__c> listActiveTables = new List<CleanupTableData__c>();
      	Map<String,CustomSettingDataValueMap__c> dataValueMap = CustomSettingDataValueMap__c.getall();
      	integer allowedBatchJobs = (integer)dataValueMap.get('AllowedBatchJobsForOneUser').DateValue__c;
      	integer counter = 1;
      	system.debug('mapTables-->'+mapTables);
        for(CleanupTableData__c tablename : mapTables.values()) 
	    {
	      String objName = tablename.ObjectAPIName__c ;
	      String FilterCriteria = tablename.Filter_Criteria__c;
		  Decimal days = tablename.Number_of_Days__c ;
		  //Start-261008
		  glbmapforharddel.put(objName,tablename.isHardDelete__c);
		  //end-261008
		  if(tablename.Active__c && tablename.Day_of_Week__c.equals(currentDOW)) 
	      {
	       if(FilterCriteria != null   )
	       {
	        if(days != Null)
	        {
	        	DateTime delDateforTA = System.now()- days ;
	        	Query = 'Select Id FROM '+ objName + ' where ' + FilterCriteria +' and  LastModifiedDate  < '+ new PRM_CommonUtils().getQueryFormattedDate(delDateforTA);
	    	}
	        else 	
	        {
	        	Query = 'Select Id FROM '+ objName + ' where ' + FilterCriteria;
	        }
	       }
	       else if(days != Null) 
	       {
	          DateTime delDate = System.now()- days ;  
	          Query = 'Select Id FROM '+ objName + ' where LastModifiedDate  < '+ new PRM_CommonUtils().getQueryFormattedDate(delDate);
	       }
	       DateTime delDateforTA = System.now()- days ;
		   glbMapObjQuery.put(objName,Query);
		  }
	    } 
     for(string frstValue:glbMapObjQuery.keySet())
	  {
	  	Query = glbMapObjQuery.get(frstValue);
	    objectName=frstValue;
	    break;
	  }
	  if(Query!=null)
	  {
       return Database.getQueryLocator(Query);
	  }
	  else
	  {
	  	
	  	return null;
	  }
	  
    }
    
    // Deleting the records. 
   global void execute(Database.BatchableContext BC, LIST<SObject> scope) {
      System.debug('INSIDE EXECUTE METHOD'+scope);
      Database.DeleteResult[] deleteResult = Database.Delete(scope) ;
      if(deleteResult != Null)
      {
        system.debug('###objectName  ::: ' + ObjectName );
        createErrorLogs(deleteResult ,objectName ) ;
      }
      //start-261008
      if(glbmapforharddel.get(objectName)=='true'){
      	Database.EmptyRecycleBinResult[] result = Database.emptyRecycleBin(scope);
      }
      //end-261008
    }

   global void finish(Database.BatchableContext BC)
   {
    system.debug('ObjectName ###Inside finish '+ objectName); 
    String currentDOW = Datetime.now().format('EEEE');
	if(glbMapObjQuery!=null)
    {
    	if(objectName != Null)
    	{
     		system.debug('glbMapObjQuery after record removal===>'+glbMapObjQuery);
     		Map<String,CleanupTableData__c> mapTables = CleanupTableData__c.getall();  
  	 		List<CleanupTableData__c> lstclnDataTbl= new List<CleanupTableData__c>();
     		CleanupTableData__c cleanObj = mapTables.get(objectName);
     		system.debug('cleanObj:::####'+cleanObj);
      		if(cleanObj != Null && glbMapObjQuery.size()>1 )
      		{
       			//teken out this condition: && cleanObj.isRecurringExecute__c == False
       			system.debug('ObjectName ###'+ cleanObj.objectAPIName__c); 
       			cleanObj.Active__c = FALSE; 
        		update cleanObj;
      		}
      		if(glbMapObjQuery.size()==1)
      		{
      			System.debug('tableName==>'+glbMapObjQuery);
      			for(CleanupTableData__c tableName : mapTables.values()) 
	        	{
		      		System.debug('tableName==>'+tableName);
      				if(objectName != tablename.objectAPIName__c && tablename.Active__c==false && tableName.isRecurringExecute__c ==true &&  tablename.Day_of_Week__c.equals(currentDOW))
		      		{
		      			tablename.Active__c=true;
		      			lstclnDataTbl.add(tablename);
		      		}
	    	  	}
	    	  	system.debug('lstclnDataTbl===>'+lstclnDataTbl);
	    	  	if(lstclnDataTbl.size()>0)update lstclnDataTbl;
      		}
      		
    	}
    	system.debug('glbMapObjQuery INSIDE FINISH ###'+glbMapObjQuery);
      	if(glbMapObjQuery.size()>1)
      	{
	      	Datetime sysTime = System.now();
	        sysTime = sysTime.addSeconds(10);
	        String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
	        system.debug(chron_exp);
	       	Scheduler_Batch_Delete BatchDeleteDataSched = new Scheduler_Batch_Delete();
	        //Schedule the next job, and give it the system time so name is unique
	     	System.schedule('Batch Delete Data Scheduled' + sysTime.getTime(),chron_exp,BatchDeleteDataSched);
      	}
        
    }
   } // End of finish() method.
  
   Public static void createErrorLogs(Database.DeleteResult[] deleteResult, String objectName ) {
     List <EMCException> errors = new List <EMCException>();
         /* HANDLING EXCEPTION LOG*/
        for (Database.DeleteResult dr : deleteResult) {
            String dataErrs = '';
            if (!dr.isSuccess()) {
                // if the particular record did not get deleted, we log the data error 
                for (Database.Error err : dr.getErrors()) {
                    dataErrs += err.getMessage();
                }
                System.debug('An exception occurred while attempting an update on ' + dr.getId());
                System.debug('ERROR: ' + dataErrs);
                errors.add(new EMCException(dataErrs, 'ERROR_DELETION_'+objectName, new String [] {dr.getId()}));
            }
        }
      // log any errors that occurred
       if (errors.size() > 0) { 
            EMC_UTILITY.logErrors(errors);  
       }
  } 
}