/* ctrlCommunitiesCaseDetailPage 
* Created by:  Matthew Roark
* Created Date: 09/15/2014
* Last Updated by:  Haytham Lotfy
* Last Update Date:  10/05/2014
*
* Description:  This class acts as the controller for the 'CommunitiesCaseDetailPage' Visualforce page
*  
* Change log:
*
* 09/15/2014 - MR - Class created.
* 09/29/2014 - HL - Modified to Add Email Message of the case, Filtered by Case Contact's Email Addresses.
* 09/29/2014 - HL - Modified to add different languages support
*/
public with sharing class ctrlCommunitiesCaseDetailPage {
    public String getCurrentUserProfile() {
        String currentUserProfile;
        List<Profile> userprofile = [select name from Profile where id = :system.Userinfo.getProfileId() ];
        currentUserProfile = userprofile[0].Name;
        return currentUserProfile;
    }
    private final Case mysObject;
    public String selectedLanguage{get; set;}
    public String recSubTypeMapping {get; set;}

    //A variable that will hold the EmailMessages list that will be viewed on the page
    public list<EmailMessage> caseEmails { 
        get; set; 
    }
    //Creating another list to make the query, and at the end we'll equale the reference list variable to this one.
    public Boolean isCaseEscalated
    {
        get; set;
    }
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public ctrlCommunitiesCaseDetailPage(ApexPages.StandardController stdController) {
        this.mysObject = (Case)stdController.getRecord();
        //For localization      
        //isCaseEscalated = this.mysObject.IsEscalated;
        //adding RetrieveEmails to the page loading. 
        if (this.mysObject.Id != null) {
        retrieveEmails();
        }
    }
        public list<EmailMessage> retrieveEmails() {
        //get the contact email address to use in the next filter's list.
        //run the SOQL query that will get all the emails received/Sent on the specified Case filtered by only the emails that were sent/received by the Contact
        String contactEmailWildChar = '%%';
        List<EmailMessage> myFilter;
        if (getCurrentUserProfile() == 'Community User' || getCurrentUserProfile() == 'Community Power User'){
        contactEmailWildChar = '%' + this.mysObject.Contact_Email1__c  + '%';
         myFilter = [select Id,Subject,Status,ToAddress,FromAddress,CreatedDate from EmailMessage where ParentId = :this.mysObject.Id and (ToAddress like :contactEmailWildChar or FromAddress like :contactEmailWildChar  or CcAddress like :contactEmailWildChar  ) order by CreatedDate] ;
        }
        else {
        myFilter = [select Id,Subject,Status,ToAddress,FromAddress,CreatedDate from EmailMessage where ParentId = :this.mysObject.Id order by CreatedDate] ;
        }
        caseEmails = myFilter;
        return myFilter;
    }

    
    public PageReference escalateCase()
    {
        try
        {
            update mysObject;
            isCaseEscalated = true;
        }
        catch (DmlException dex)
        {
            System.debug('Unable to update Case. Error: '+dex.getMessage());
        }
        return null;
    }
    
    public PageReference refreshFields()
    {
        return null;        
    }
}