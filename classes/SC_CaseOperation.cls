/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        ===========       
 
 | 05/07/2013     Leonard Victor       Created Class For SC Realated Field Population on insert and update since we hit foemula limit
 | 30/07/2013     Leonard Victor       Added two Methods for Case Asset Liniking logic
 | 24/12/2013     Shalabh Sharma       Added method to populate Lead fields and contact manager on case record  
 | 13-Oct-2014     Bisna V P    CI:1399   Remove code referencing unused fields 
 |20-Jan-2015    Akash Rastogi   1528  Commented code as per requirement
 |18-Feb-2015    Vinod Jetti     #1649   Replaced fields Site_DUNS_Entity__c, Global_DUNS_Entity__c to Site_DUNS_Entity_Hub__c, Global_DUNS_Entity_Hub__c
 +==================================================================================================================**/

 public class SC_CaseOperation{
     Map<string,CustomSettingDataValueMap__c> dataValueMap = CustomSettingDataValueMap__c.getall();
     String closedCaseStatus = dataValueMap.get('ClosedCaseStatus').Datavalue__c;
     Static Boolean restricDuplicate = false;

/* @Method <This method execute is used to populate Asset Related fields on case record on insert/update>
@param <This method takes List<Case> as parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/
        public void updateCaseAssetFields(List<Case> caseLst){
                List<case> lstCase = new List<case>();
                Map<Id,Id> caseAssetMap = new Map<Id,Id>();
                Set<Id> recId = new Set<Id>();   
                for(Case caseObj : caseLst){
                    caseAssetMap.put(caseObj.id,caseObj.Customer_Asset_Name__c);
                    recId.add(caseObj.RecordTypeId);
                }           
                Map<id,Asset__c> mapAsset = new Map<id,Asset__c>([Select id,Serial_Number__c,Install_Base_Status__c,Product_Family__c,Model__c,Contract_Status__c,Contract_Number__c,Contract_End_Date__c,Product_Name_Vendor__c,
                                Lease_Exp_Date__c,Customer_Name__c,Party_Number__c,Core_Quota_Rep__c,End_of_Service_Life_Date__c,Core_Quota_Rep_from_Account__c,Customer_Name__r.Core_Quota_Rep__c,Customer_Name__r.Global_DUNS_Entity_Hub__c,EMC_Classification__c   from Asset__c where id in:caseAssetMap.values()]);
                 Map<Id,RecordType> recordNameMap = new Map<Id,RecordType>([Select Id , Name from RecordType where Id in: recId]);
                 for(Case caseObjInsert : caseLst){
                        caseObjInsert.Customer_Asset_Serial_Number__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Serial_Number__c;
                        caseObjInsert.Customer_Asset_Install_Base_Status__c  = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Install_Base_Status__c;
                        caseObjInsert.Customer_Asset_Product_Family__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Product_Family__c;
                        caseObjInsert.Customer_Asset_Model_Number__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Model__c;
                        caseObjInsert.Customer_Asset_Vendor__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Product_Name_Vendor__c;
                        caseObjInsert.Customer_Asset_Contract_Status__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Contract_Status__c;
                        caseObjInsert.Customer_Asset_Contract_Number__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Contract_Number__c;
                        caseObjInsert.Customer_Asset_Contract_End_Date__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Contract_End_Date__c;
                        //caseObjInsert.Customer_Asset_Contract_Number__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Serial_Number__c;
                        //1528 Commenting customer_asset_lease_expiration_date__c from the code
                        //caseObjInsert.Customer_Asset_Lease_Expiration_Date__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Lease_Exp_Date__c;
                        caseObjInsert.Customer_Asset_Account_Name__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Customer_Name__c;
                         caseObjInsert.Customer_Asset_Account_Party_Number__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Party_Number__c;
                      //  caseObjInsert.Customer_Asset_Core_Quota_Rep__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Core_Quota_Rep__c;
                        caseObjInsert.Customer_Asset_Core_Quota_Rep__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Customer_Name__r.Core_Quota_Rep__c;
                        caseObjInsert.Customer_Asset_Global_DUNS__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Customer_Name__r.Global_DUNS_Entity_Hub__c;
                        caseObjInsert.Customer_Asset_EMC_Classification__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).EMC_Classification__c;
                        caseObjInsert.Customer_Asset_End_of_Service_Life_Date__c =  mapAsset.get(caseAssetMap.get(caseObjInsert.id)).End_of_Service_Life_Date__c;            
                         if(recordNameMap.containskey(caseObjInsert.recordtypeid) && (recordNameMap.get(caseObjInsert.recordtypeid).Name).equalsIgnoreCase(Label.SC_Maintenance_Contract_Operations) ){                    
                          caseObjInsert.Customer_Account_Name__c = mapAsset.get(caseAssetMap.get(caseObjInsert.id)).Customer_Name__c;                       
                        }                                           
                }
        }
    /* @Method <This method execute is used to populate Account Related fields on case record on insert/update>
    @param <This method takes List<Case> as parameter>
    @return void - <Not returning anything>
    @throws exception - <No Exception>
    */
         public void updateCaseAccountFields(List<Case> caseLst){
                List<case> lstCase = new List<case>();
                Map<Id,list<Id>> caseAccountMap = new Map<Id,list<Id>>();
                Set<Id> custIdSet = new Set<Id>();
                Set<Id> guIdSet = new Set<Id>();
                Set<Id> buuIdSet = new Set<Id>();
                Set<Id> accountIdSet = new Set<Id>();                
                for(Case caseObj : caseLst){
                    List<id> idLst = new List<id>();                     
                    if(caseObj.Customer_Account_Name__c != null){
                     custIdSet.add(caseObj.Customer_Account_Name__c);
                     idLst.add(caseObj.Customer_Account_Name__c);
                     accountIdSet.add(caseObj.Customer_Account_Name__c);
                     }
                     else if(caseObj.Customer_Account_Name__c == null){
                        caseObj.Party_Number__c = null;
                        caseObj.Global_Ultimate_Identifier__c = null;
                        caseObj.Entity_Identifier__c = null;
                        caseObj.Customer_Account_Owner__c = null;
                     } 
                    //1399 commenting this if 
                    /*if(caseObj.Global_Ultimate__c != null){
                     guIdSet.add(caseObj.Global_Ultimate__c);
                     idLst.add(caseObj.Global_Ultimate__c);
                     accountIdSet.add(caseObj.Global_Ultimate__c);
                     }*/
                     //1399 commenting this if
                    /*if(caseObj.Business_Unit_Ultimate__c != null){
                     buuIdSet.add(caseObj.Business_Unit_Ultimate__c);  
                     idLst.add(caseObj.Business_Unit_Ultimate__c);
                     accountIdSet.add(caseObj.Business_Unit_Ultimate__c);                   
                     }*/
                        caseAccountMap.put(caseObj.id,idLst);   
                }
                 System.debug('caseAccountMap-------------->'+ caseAccountMap.values());
                Map<id,Account> mapAccount = new Map<id,Account>([Select id,Customer_Profiled_Account_Lookup__r.name,Name,Site_DUNS_Entity_Hub__c,Global_DUNS_Entity_Hub__c,Party_Number__c,Customer_Profiled_Account__c from Account where id in :accountIdSet]);
                   for(Case caseObjInsert : caseLst){
                      for( Id caseAccountId : caseAccountMap.get(caseObjInsert.id)){                      
                            if(custIdSet.contains(caseAccountId)){
                            caseObjInsert.Customer_Profiled_Account__c = mapAccount.get(caseAccountId).Customer_Profiled_Account__c;
                            caseObjInsert.Party_Number__c = mapAccount.get(caseAccountId).Party_Number__c;
                            caseObjInsert.Customer_Profiled_Account_Apex__c = mapAccount.get(caseAccountId).Customer_Profiled_Account_Lookup__r.name;
                            caseObjInsert.Customer_Account_Owner__c = mapAccount.get(caseAccountId).Name;
                            caseObjInsert.Global_Ultimate_Identifier__c = mapAccount.get(caseAccountId).Global_DUNS_Entity_Hub__c;
                            caseObjInsert.Entity_Identifier__c = mapAccount.get(caseAccountId).Site_DUNS_Entity_Hub__c;
                            }
                            //1399 commenting if conditions
                            /*
                            if(guIdSet.contains(caseAccountId)){
                            caseObjInsert.GU_DUNS_Number__c = mapAccount.get(caseAccountId).Global_DUNS_Entity__c;
                            caseObjInsert.GU_Party_Number__c = mapAccount.get(caseAccountId).Party_Number__c;
                            }                           
                            if(buuIdSet.contains(caseAccountId)){
                            caseObjInsert.BUU_DUNS_Number__c = mapAccount.get(caseAccountId).Site_DUNS_Entity_Hub__c;
                            caseObjInsert.BUU_Party_Number__c = mapAccount.get(caseAccountId).Party_Number__c;
                            }*/
                      }
                 }
        }
    /* @Method <This method execute is used to populate Link to Case Field fields on Asset record on insert/update>
    @param <This method takes List<Case> as parameter>
    @return void - <Not returning anything>
    @throws exception - <No Exception>
    */
        public void setCaseIndicator(List<Case> lstCaseFromTrigger){        
        set<id> setAssetIds = new set<id>();
        map<id,List<id>> mapCaseWithAssetId = new map<id,list<id>>();        
        for(Case caseObj:lstCaseFromTrigger){
            setAssetIds.add(caseObj.Customer_Asset_Name__c);
        }
        Map<Id,Case> mapCaseRelatedToAsset= new map<id,case>([Select id,Customer_Asset_Name__c,Status from 
                                                              case where Customer_Asset_Name__c in :setAssetIds]);
        for(case caseObj:mapCaseRelatedToAsset.values()){
            if(!closedCaseStatus.contains(caseObj.status)){
                list<id> lstcaseIds = mapCaseWithAssetId.get(caseObj.Customer_Asset_Name__c);
                if(lstcaseIds ==null){
                   lstcaseIds = new list<id>();  
                }
            lstcaseIds.add(caseObj.id);
            mapCaseWithAssetId.put(caseObj.Customer_Asset_Name__c,lstcaseIds);
           } 
        }                                                     
        Map<Id,Additional_Assets__c> mapAdditionalAssets = new map<id,Additional_Assets__c>([Select id,Case__r.Status,Customer_Asset__c
                                                                from Additional_Assets__c where Customer_Asset__c in :setAssetIds or Case__c in :lstCaseFromTrigger]);
        for(Additional_Assets__c additionalassetObj:mapAdditionalAssets.values()){
            setAssetIds.add(additionalassetObj.Customer_Asset__c);
            if(!closedCaseStatus.contains(additionalassetObj.Case__r.status)){
                list<id> lstcaseIds = mapCaseWithAssetId.get(additionalassetObj.Customer_Asset__c);
                if(lstcaseIds ==null){
                   lstcaseIds = new list<id>();  
                }
            lstcaseIds.add(additionalassetObj.Case__c);
            mapCaseWithAssetId.put(additionalassetObj.Customer_Asset__c,lstcaseIds);
           } 
        }                                                        
        map<id,Asset__c> mapAssetToUpdate = new map<id,Asset__c>([Select id,Related_To_Open_Case__c from Asset__c where id in :setAssetIds]); 
        for(Asset__c assetObj:mapAssetToUpdate.values()){
            if(mapCaseWithAssetId.get(assetObj.id)!=null){
               assetObj.Related_To_Open_Case__c = true;
            }
            else{
               assetObj.Related_To_Open_Case__c = false; 
            }
        }   
        update mapAssetToUpdate.values();                                                                                            
    }
    /* @Method <This method execute is used to populate Link to Case Field fields on Asset record based in Additional_Assets__c creation insert/update>
    @param <This method takes List<Additional_Assets__c> as parameter>
    @return void - <Not returning anything>
    @throws exception - <No Exception>
    */
    public void setCaseIndicator(List<Additional_Assets__c> lstAdditionalAssetsFromTrigger){
        set<id> setAssetIds= new set<Id>();
        Map<Id,Additional_Assets__c> mapAdditionalAssets = new map<id,Additional_Assets__c>([Select id,Case__r.Status,Customer_Asset__c
                                                                from Additional_Assets__c where id in :lstAdditionalAssetsFromTrigger]);
        for(Additional_Assets__c additionalAssetObj :mapAdditionalAssets.values()){
            if(!closedCaseStatus.contains(additionalAssetObj.Case__r.Status) ){
                setAssetIds.add(additionalAssetObj.Customer_Asset__c);
            }
        }
        map<id,Asset__c> mapAssetToUpdate = new map<id,Asset__c>([Select id,Related_To_Open_Case__c from Asset__c where id in :setAssetIds]); 
        for(Asset__c assetObj:mapAssetToUpdate.values()){            
            assetObj.Related_To_Open_Case__c = true;            
        }   
        update mapAssetToUpdate.values();                                                                                            
    }
/* @Method <This method execute is used to populate Lead Related fields on case record on insert/update>
@param <This method takes List<Case> as parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/
        public void updateCaseLeadFields(List<Case> caseLst){
                List<Case> lstCase = new List<Case>();
                Map<Id,Id> caseLeadMap = new Map<Id,Id>();
                Map<Id,String> caseLeadNumberMap = new Map<Id,String>();
                Map<Id,Lead> mapCaseLead = new Map<Id,Lead>();
                Map<String,Lead> mapLeadNumberLead = new Map<String,Lead>(); 
                for(Case caseObj : caseLst){
                    if(caseObj.Lead_Name__c !=null)
                        caseLeadMap.put(caseObj.id,caseObj.Lead_Name__c);
                    if(caseObj.Lead_Number__c !=null)
                        caseLeadNumberMap.put(caseObj.id,caseObj.Lead_Number__c);
                    if(caseObj.Lead_Name__c == null && caseObj.Lead_Number__c == null){
                        caseObj.Lead_Name__c = null;
                        caseObj.Lead_Number__c = null;
                        caseObj.Lead_Account_Name__c = null;
                        caseObj.Lead_Account_Party_Number__c = null;
                        caseObj.Lead_EMC_Classification__c = null;
                        caseObj.Lead_Country__c = null;
                        caseObj.Lead_Type__c = null;
                        caseObj.Lead_Contract_End_Date__c = null;
                        caseObj.Lead_Source_Program_Detail__c = null;
                        caseObj.Lead_Campaign_Event_Name__c = null;
                        caseObj.Lead_Passed_to_Renewals_Date__c = null;
                        caseObj.Lead_Passed_to_Renewals_Type__c = null;
                        caseObj.Lead_Source__c = null;
                    }
                }
                Map<Id,Lead> mapLead = new Map<Id,Lead>([Select Id,Name,Lead_Number__c,Related_Account__c,Related_Account__r.Name,Related_Account_Party_Number__c,EMC_Classification_Formula__c,
                                            Country__c,Lead_Type__c,Contract_End_Date__c,Originator_Details__c,Campaign_Event_Name__c,Passed_to_Renewals_Date__c,Pass_to_Renewals_Type__c,LeadSource
                                            from Lead where id in:caseLeadMap.values() OR Lead_Number__c in:caseLeadNumberMap.values()]);

                for(Lead leadObj: mapLead.values()){
                    mapLeadNumberLead.put(leadObj.Lead_Number__c,leadObj);
                }
                for(Case caseObj : caseLst){
                    if(caseObj.Lead_Name__c !=null && mapLead.size()>0)
                        mapCaseLead.put(caseObj.id,mapLead.get(caseLeadMap.get(caseObj.id)));
                    else if(caseObj.Lead_Number__c !=null && mapLeadNumberLead.size()>0)
                        mapCaseLead.put(caseObj.id,mapLeadNumberLead.get(caseLeadNumberMap.get(caseObj.id)));
                }
                for(Case caseObjInsert : caseLst){
                    if(mapLead.size()==0 && caseObjInsert.Lead_Number__c!=null){
                        caseObjInsert.addError('The Lead Number is not valid.  Please provide a valid Lead Number');
                    }
                    else if(mapCaseLead.size()>0){
                        String str = mapCaseLead.get(caseObjInsert.Id).Lead_Type__c;  
                        str = str.replaceAll('amp;','');
                        List<String> lstStr = new List<String>();
            if(str != '' && str != '-'){
              for(String s : str.split('"')){
                lstStr.add(s);
              }
            }
                        system.debug('lstStr--->'+lstStr);
                        system.debug('str--->'+str);
                        caseObjInsert.Lead_Name__c = mapCaseLead.get(caseObjInsert.Id).Id;
                        caseObjInsert.Lead_Number__c = mapCaseLead.get(caseObjInsert.Id).Lead_Number__c;
                        caseObjInsert.Lead_Account_Name__c = mapCaseLead.get(caseObjInsert.Id).Related_Account__r.Name;
                        caseObjInsert.Lead_Account_Party_Number__c = mapCaseLead.get(caseObjInsert.Id).Related_Account_Party_Number__c;
                        caseObjInsert.Lead_EMC_Classification__c = mapCaseLead.get(caseObjInsert.Id).EMC_Classification_Formula__c;
                        caseObjInsert.Lead_Country__c = mapCaseLead.get(caseObjInsert.Id).Country__c;
            if(lstStr.size()>0 && lstStr[1] != null)
                        caseObjInsert.Lead_Type__c = lstStr[1];
                        caseObjInsert.Lead_Contract_End_Date__c = mapCaseLead.get(caseObjInsert.Id).Contract_End_Date__c;
                        caseObjInsert.Lead_Source_Program_Detail__c = mapCaseLead.get(caseObjInsert.Id).Originator_Details__c;
                        caseObjInsert.Lead_Campaign_Event_Name__c = mapCaseLead.get(caseObjInsert.Id).Campaign_Event_Name__c;
                        caseObjInsert.Lead_Passed_to_Renewals_Date__c = mapCaseLead.get(caseObjInsert.Id).Passed_to_Renewals_Date__c;
                        caseObjInsert.Lead_Passed_to_Renewals_Type__c = mapCaseLead.get(caseObjInsert.Id).Pass_to_Renewals_Type__c;
                        caseObjInsert.Lead_Source__c = mapCaseLead.get(caseObjInsert.Id).LeadSource;
                    }
                }
        }

/* @Method <This method execute is used to populate Field Rep on case record on insert/update>
@param <This method takes List<Case> as parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/
    public void updateFieldRep(List<Case> lstCase){
        Map<Id,Id> mapCaseContact = new Map<Id,Id>();
        for(Case caseObj : lstCase){
            mapCaseContact.put(caseObj.Id,caseObj.Field_Rep__c);
        }
        Map<Id,Contact> mapContact = new Map<Id,Contact>([Select Id,ReportsToId,ReportsTo.Name from Contact where Id in:mapCaseContact.values()]);

        for(Case caseObj : lstCase){
            caseObj.Field_Rep_Manager__c = mapContact.get(mapCaseContact.get(caseObj.Id)).ReportsTo.Name;
        }
    }
/* @Method <This method execute is used to populate Customer Account Name when Party Number is populated on case record on insert/update>
@param <This method takes List<Case> as parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/
    public void populateCustomerAccountName(List<Case> lstCase){
        Set<String> setPartyNumber = new Set<String>();
        Map<String,Account> mapPartyNumberAccount = new Map<String,Account>();
        for(Case caseObj:lstCase){
            if( caseObj.Party_Number__c != null){
                setPartyNumber.add(caseObj.Party_Number__c);
            }
            else if(caseObj.Party_Number__c == null){
                caseObj.Customer_Account_Name__c = null;
                caseObj.Global_Ultimate_Identifier__c = null;
                caseObj.Entity_Identifier__c = null;
                caseObj.Customer_Account_Owner__c = null;
            }
        }
        Map<Id,Account> mapIdAccount = new Map<Id,Account>([Select id,Name,Party_Number__c,Site_DUNS_Entity_Hub__c,Global_DUNS_Entity_Hub__c from Account where Party_Number__c in :setPartyNumber]);
        for(Account acc: mapIdAccount.values()){
            mapPartyNumberAccount.put(acc.Party_Number__c,acc); 
        }
        if(mapPartyNumberAccount.size()>0){
            for(Case caseObj: lstCase){
                caseObj.Customer_Account_Name__c = mapPartyNumberAccount.get(caseObj.Party_Number__c).Id;
                caseObj.Global_Ultimate_Identifier__c = mapPartyNumberAccount.get(caseObj.Party_Number__c).Global_DUNS_Entity_Hub__c;
                caseObj.Entity_Identifier__c = mapPartyNumberAccount.get(caseObj.Party_Number__c).Site_DUNS_Entity_Hub__c;
                caseObj.Customer_Account_Owner__c = mapPartyNumberAccount.get(caseObj.Party_Number__c).Name;
            }
        }
    }

    
    
}