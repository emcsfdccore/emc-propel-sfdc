/*==============================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER                WR       DESCRIPTION                               
 |  ====          =========                ==       =========== 
 |  23/06/2011    Suman B                         This is a test class for PRM_VPP_VelocityScorecardController.                       
 |  14/12/2011    Anil                            Removed Query for fetching Partner and used Custom setting Data Helper
 |  16/05/2012    Kaustav                         Increased the code coverage         
 |  17/05/2012    Anand Sharma                    Grouping issue exception 
 |  04/12/2012    Anand Sharma                    Fixed for PRM_VPP_VelocityRuleResults constuctor  
 |  22/04/2013    Krishna Pydavula      223956    Increased the code coverage
 |  23/04/2014    Jaspreet Singh                  Code Coverage, BPP
 |  25/05/2014    Aagesh Jose                     Increased code coverage
 |  30/12/2014    Karan Shekhar                   Added test conditions for new Solution provider scorecard
 +==============================================================================================================*/

@isTest
private class PRM_VPP_VelocityScorecardController_TC {

  static testMethod void myUnitTest() {
  
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1]; 
        System.runAs(insertUser)
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();  
            CustomSettingDataHelper.federationTierData();  
            CustomSettingDataHelper.BusinessPartnerProgramData();
        }
       
     /*Creating account record*/
       Map<String,Schema.RecordTypeInfo> recordTypes = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
       List<Account> accountTestList = new List<Account>();
       Id accRecordTypeId = recordTypes.get('Customer Account Record Type').getRecordTypeId();
       List<Account> objAccount = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
      //logic added to change the Party Id because it is unique.
       for(Integer i=0;i<objAccount.size();i++) {
        
            objAccount[i].Party_Number__c = '123'+i;
            objAccount[i].Federation_Program_Tier__c = 'VMWare: Premier;VCE Gold';
            if(i==2) {
                objAccount[i].Hide_Scorecard_Revenue__c=false;
            }
        
       }
            insert objAccount;
       /*Creation of Grouping record */ 
       Account_Groupings__c grouping = new Account_Groupings__c (Name = 'UNITTESTGrp',Profiled_Account__c=objAccount[0].Id);
       insert grouping;
       for(Integer iCounter=0;iCounter<objAccount.size();iCounter++)
       {
           if(iCounter==0)
           {
                objAccount[iCounter].Velocity_Solution_Provider_Tier__c='Silver';
                objAccount[iCounter].Preferred_Distributor__c = objAccount[1].Id;
           }
           else if(iCounter==1)
           {
                objAccount[iCounter].Velocity_Solution_Provider_Tier__c='Silver';
           }
           else if(iCounter==2) {
            
             objAccount[iCounter].DMR_Tier__c='Silver';
           }
           else if(iCounter==3) {
            objAccount[iCounter].DMR_Tier__c=null;
            objAccount[iCounter].Velocity_Solution_Provider_Tier__c=null;
            
            
           }
           objAccount[iCounter].Type = 'Partner';
           objAccount[iCounter].Grouping__c = grouping.Id;
           objAccount[iCounter].Partner_Type__c ='Distributor;Service Provider'; 
           objAccount[iCounter].Lead_Oppty_Enabled__c=true;
           objAccount[iCounter].Cluster__c='LA1';
           
       }
    
        update objAccount;
           Map<String,Schema.RecordTypeInfo> recordTypes1 = Velocity_Rules__c.sObjectType.getDescribe().getRecordTypeInfosByName();
           Id veloRecordTypeId = recordTypes1.get('Tier Rule').getRecordTypeId();
           List<Velocity_Rules__c> lstVelocityRules=new List<Velocity_Rules__c>();
           for(Integer iCounter=0;iCounter<7;iCounter++){
            Velocity_Rules__c velocityRuleObj= new Velocity_Rules__c();
            velocityRuleObj.Display_Name__c='test data';
           velocityRuleObj.Specilaity__c='Backup and Recovery';
           velocityRuleObj.Specialty_Rule_Type__c='LA1';
           velocityRuleObj.Cluster__c='LA1';
           velocityRuleObj.AND_OR__c='OR';
           velocityRuleObj.Sort__c=iCounter+1;
           velocityRuleObj.recordtypeid=veloRecordTypeId;
           if(iCounter == 0){
           velocityRuleObj.Bucket__c='Revenue';
           velocityRuleObj.Tier__c= 'Silver';
           velocityRuleObj.Required__c='2000000';
           velocityRuleObj.Required_Type__c='USD';
           }else if(iCounter == 1){
            velocityRuleObj.Bucket__c='Training';
            velocityRuleObj.Tier__c= 'Silver';
            velocityRuleObj.Required__c='2';  
            velocityRuleObj.Required_Type__c='Number/Count';
           }
           else if(iCounter == 2) {
               velocityRuleObj.Bucket__c='SE Foundation';
               velocityRuleObj.Tier__c= 'Silver';
               velocityRuleObj.Required__c='2';
               velocityRuleObj.Required_Type__c='Number/Count';
           }
           else if(iCounter == 3) {
            
            velocityRuleObj.Bucket__c='Implementation Engineer';
            velocityRuleObj.Tier__c= 'Silver';
            velocityRuleObj.Required__c='2';
            velocityRuleObj.Required_Type__c='Number/Count';
           }
          else if(iCounter == 4) {
            
            velocityRuleObj.Bucket__c='Sales Foundation';
            velocityRuleObj.Tier__c= 'Silver';
            velocityRuleObj.Required__c='2';
            velocityRuleObj.Required_Type__c='Number/Count';
           }
          else if(iCounter == 5) {
            
            velocityRuleObj.Bucket__c='Technology Architect';
            velocityRuleObj.Tier__c= 'Silver';
            velocityRuleObj.Required__c='2';
            velocityRuleObj.Required_Type__c='Number/Count';
           }
          else if(iCounter == 6) {
            
            velocityRuleObj.Bucket__c='Additional Requirements';
            velocityRuleObj.Tier__c= 'Silver';
            velocityRuleObj.Required__c='2';
            velocityRuleObj.Required_Type__c='Number/Count';
           }
           velocityRuleObj.Is_Total_Revenue__c= false;
           //velocityRuleObj.Tier__c= 'Silver';
           velocityRuleObj.Evaluation_at__c ='Country Grouping Level';
           velocityRuleObj.Required_to_Cover_Technology__c=2;
          // velocityRuleObj.Required__c='2';
           //velocityRuleObj.Required_Type__c='Number/Count';
           //insert velocityRuleObj;
            lstVelocityRules.add(velocityRuleObj);
           }
           insert lstVelocityRules;
           
           List<Velocity_Rule_Results__c> lstVelocityRuleResult=new List<Velocity_Rule_Results__c>();
           for(Integer iCounter=0;iCounter<7;iCounter++)
           {
                Velocity_Rule_Results__c veloRuleResultObj=new Velocity_Rule_Results__c();
                veloRuleResultObj.Speciality_RuleID__c=  lstVelocityRules[iCounter].Id; //velocityRuleObj.id;//;
                veloRuleResultObj.AccountID__c=objAccount[0].Id;
                veloRuleResultObj.Grouping__c = grouping.Id;
                if(iCounter==0) {
                    veloRuleResultObj.Current__c='$2,719,480';
                }
                else {
                    
                    veloRuleResultObj.Current__c='4';
                }
                lstVelocityRuleResult.add(veloRuleResultObj);               
           }
          for(Integer iCounter=0;iCounter<7;iCounter++)
           {
                Velocity_Rule_Results__c veloRuleResultObj=new Velocity_Rule_Results__c();
                veloRuleResultObj.Speciality_RuleID__c=  lstVelocityRules[iCounter].Id; //velocityRuleObj.id;//;
                veloRuleResultObj.AccountID__c=objAccount[2].Id;
                veloRuleResultObj.Grouping__c = grouping.Id;
                if(iCounter==0) {
                    veloRuleResultObj.Current__c='$1,719,480';
                }
                else {
                    
                    veloRuleResultObj.Current__c='4';
                }
                lstVelocityRuleResult.add(veloRuleResultObj);               
           }
           insert lstVelocityRuleResult;
           ApexPages.StandardController controller01 = new ApexPages.StandardController(objAccount[0]); 
           ApexPages.currentPage().getParameters().put('id',objAccount[0].Id);
           PRM_VPP_VelocityScorecardController scorecard = new PRM_VPP_VelocityScorecardController() ;           
           scorecard.getItems();
           scorecard.getTierScoreCard();
           scorecard.getComplianceRequirementStatusItems();
           scorecard.getTierScoreCardSingle();
           scorecard.cancelUpdates();
           scorecard.fetchRevenueData();
           scorecard.setSelectedButton();
           scorecard.fetchDistiData();
                      
           ApexPages.StandardController controller02 = new ApexPages.StandardController(objAccount[1]); 
           ApexPages.currentPage().getParameters().put('id',objAccount[1].Id);
           ApexPages.currentPage().getParameters().put('pageTier','Silver');
           PRM_VPP_VelocityScorecardController scorecard1 = new PRM_VPP_VelocityScorecardController() ;           
           scorecard.getItems();
           scorecard.getTierScoreCard();
           scorecard.getComplianceRequirementStatusItems();
           scorecard.getTierScoreCardSingle();
           scorecard.cancelUpdates();
           scorecard.fetchRevenueData();
           scorecard.setSelectedButton();
           scorecard.fetchDistiData();
           

               ApexPages.StandardController controller03 = new ApexPages.StandardController(objAccount[2]); 
               ApexPages.currentPage().getParameters().put('id',objAccount[2].Id);
               system.debug('***objAccount[2].DMR_Tier__c**'+objAccount[2].DMR_Tier__c);
                PRM_VPP_VelocityScorecardController scorecard2 = new PRM_VPP_VelocityScorecardController() ;    
                scorecard2.fetchRevenueData();                             
                scorecard2.preferredDistributorCheck(objAccount[0].Id, objAccount[1].Id);        
                ApexPages.StandardController controller06= new ApexPages.StandardController(objAccount[3]); 
                ApexPages.currentPage().getParameters().put('id',objAccount[3].Id);
                PRM_VPP_VelocityScorecardController scorecard6 = new PRM_VPP_VelocityScorecardController() ;   
       if(scorecard.SpecialityRulesResult == Null || scorecard.SpecialityRulesResult.size()==0){

           List<Account> listAccount = new List<Account>();
           //listAccount.add(partner.contact.Account) ;
           listAccount.addall(objAccount) ;
           PRM_VPP_VelocityRuleResults vrrresults = new PRM_VPP_VelocityRuleResults(listAccount, false) ;
            vrrresults.createVelocityRuleResults();
           scorecard.SpecialityRulesResult = [Select v.AccountID__c, v.AccountID__r.EMC_Speciality__c, v.Current__c, v.Speciality_RuleID__c, v.Speciality_RuleID__r.Bucket__c, 
                                                                 v.Speciality_RuleID__r.Display_Name__c, v.Speciality_RuleID__r.Required__c, 
                                                                 v.Speciality_RuleID__r.Specilaity__c,v.Speciality_RuleID__r.Is_Total_Revenue__c,v.Status__c,
                                                                 v.rule_theater__c,v.Speciality_RuleID__r.Tier__c ,v.Required_Value_Type__c,v.Is_Total_Revenue__c,
                                                                 v.RequiredCount__c,v.Speciality_RuleID__r.Sort__c
                                                           from Velocity_Rule_Results__c v
                                                           where Speciality_RuleID__r.RecordTypeId=: scorecard.specilalityRecordtypeId
                                                            AND v.rule_theater__c =: scorecard.clusterValue
                                                            AND v.AccountID__c=: scorecard.AccountRecord.Id
                                                            ORDER BY v.Speciality_RuleID__r.Sort__c asc
                                                            ];
         }                                                            
           System.debug('###### scorecard.SpecialityRulesResult=>'+scorecard.SpecialityRulesResult);
           scorecard.populateData();
           //scorecard.updateProfileAccount();
           PRM_VPP_VelocityScorecardController.PRM_DisplayNameDataContainer namedatacntnr = new PRM_VPP_VelocityScorecardController.PRM_DisplayNameDataContainer();
                    namedatacntnr.setmapTierNameValueDataContainer('testtier','test'); 
                    
          PRM_VPP_VelocityScorecardController.PRM_BucketNameDataContainer bucketcntnr = new PRM_VPP_VelocityScorecardController.PRM_BucketNameDataContainer();
                    bucketcntnr.setmapBucketNameValueDataContainer(namedatacntnr);   
                   
                Account acc=[select id,name,Type,Grouping__c,Partner_Type__c,Lead_Oppty_Enabled__c,Cluster__c,Preferred_Distributor__c from Account where Preferred_Distributor__c!=null limit 1];  
                if(acc!=null)
                {
                   ApexPages.StandardController controller04 = new ApexPages.StandardController(acc); 
                   ApexPages.currentPage().getParameters().put('id',acc.id);
                  // PRM_VPP_VelocityScorecardController scorecard3 = new PRM_VPP_VelocityScorecardController() ;           
                   /*scorecard2.getItems();
                   scorecard2.getTierScoreCard();
                   scorecard2.getComplianceRequirementStatusItems();
                   scorecard2.getTierScoreCardSingle();
                   scorecard2.cancelUpdates();*/
                }
       //}
           Id veloRecordTypeId1 = recordTypes1.get('Tier Rule').getRecordTypeId(); 
           Velocity_Rules__c tempVelocityRuleObj= new Velocity_Rules__c();
           tempVelocityRuleObj.Display_Name__c='test Data Rule';
           tempVelocityRuleObj.Specilaity__c='Backup and Recovery';
           tempVelocityRuleObj.Specialty_Rule_Type__c='LA1';
           tempVelocityRuleObj.Cluster__c='APJ';
           //tempVelocityRuleObj.AND_OR__c='OR';
           tempVelocityRuleObj.Sort__c=1;
           tempVelocityRuleObj.recordtypeid=veloRecordTypeId1;
           tempVelocityRuleObj.Bucket__c='Solution Provider Program';
           tempVelocityRuleObj.Is_Total_Revenue__c= true;
           tempVelocityRuleObj.Tier__c= 'Platinum';
           tempVelocityRuleObj.Required_Type__c = 'Number/Count';
           tempVelocityRuleObj.Evaluation_at__c ='Country Grouping Level';
           insert tempVelocityRuleObj;
           
           List<Velocity_Rule_Results__c> lstVelocityRuleResulttemp=new List<Velocity_Rule_Results__c>();
           for(Integer iCounter=0;iCounter<4;iCounter++)
           {
                Velocity_Rule_Results__c tempVeloRuleResultObj=new Velocity_Rule_Results__c();
                tempVeloRuleResultObj.Speciality_RuleID__c= tempVelocityRuleObj.id;//; lstVelocityRules[iCounter].Id
                tempVeloRuleResultObj.AccountID__c=objAccount[0].Id;
                tempVeloRuleResultObj.Grouping__c = grouping.Id;
                lstVelocityRuleResulttemp.add(tempVeloRuleResultObj);               
           }
           insert lstVelocityRuleResulttemp;
           ApexPages.StandardController controller04 = new ApexPages.StandardController(objAccount[0]); 
           ApexPages.currentPage().getParameters().put('id',objAccount[0].Id);
           PRM_VPP_VelocityScorecardController scorecard3 = new PRM_VPP_VelocityScorecardController() ;           
           /*scorecard3.getItems();
           scorecard3.getTierScoreCard();
           scorecard3.getComplianceRequirementStatusItems();
           scorecard3.getTierScoreCardSingle();
           scorecard3.cancelUpdates();*/
       
       
         Velocity_Rules__c velocityRuleObj= new Velocity_Rules__c();
            velocityRuleObj.Display_Name__c='test Data1';
           velocityRuleObj.Specilaity__c='Backup and Recovery';
           velocityRuleObj.Specialty_Rule_Type__c='LA1';
           velocityRuleObj.Cluster__c='LA1';
           velocityRuleObj.AND_OR__c='OR';
           velocityRuleObj.Sort__c=1;
           velocityRuleObj.recordtypeid=veloRecordTypeId;
           velocityRuleObj.Bucket__c='Solution Provider Program';
           velocityRuleObj.Required_Type__c = 'USD';
           velocityRuleObj.Is_Total_Revenue__c= true;
           velocityRuleObj.Tier__c= 'Signature';
           velocityRuleObj.Evaluation_at__c ='Country Grouping Level';
           insert velocityRuleObj;
           
           List<Velocity_Rule_Results__c> lstVelocityRuleResult2=new List<Velocity_Rule_Results__c>();
           for(Integer iCounter=0;iCounter<4;iCounter++)
           {
                Velocity_Rule_Results__c veloRuleResultObj=new Velocity_Rule_Results__c();
                veloRuleResultObj.Speciality_RuleID__c=  velocityRuleObj.Id; //velocityRuleObj.id;//;
                veloRuleResultObj.AccountID__c=objAccount[iCounter].Id;
                veloRuleResultObj.Grouping__c = grouping.Id;
                lstVelocityRuleResult2.add(veloRuleResultObj);               
           }
           insert lstVelocityRuleResult2;
           ApexPages.StandardController controller05 = new ApexPages.StandardController(objAccount[0]); 
           ApexPages.currentPage().getParameters().put('id',objAccount[0].Id);
           PRM_VPP_VelocityScorecardController scorecard4 = new PRM_VPP_VelocityScorecardController() ;           
           /*scorecard4.getItems();
           scorecard4.getTierScoreCard();
           scorecard4.getComplianceRequirementStatusItems();
           scorecard4.getTierScoreCardSingle();
           scorecard4.cancelUpdates();
           scorecard4.getComplianceStatusForRevenue('Signature');*/
           objAccount[0].Partner_Type__c ='Distribution, Distribution VAR';
           update objAccount;
           PRM_VPP_VelocityScorecardController scorecard5 = new PRM_VPP_VelocityScorecardController() ;   
    }
        
}