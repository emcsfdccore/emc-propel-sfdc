/*============================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER                WR        DESCRIPTION                               

 |  ====          =========                ==        =========== 

 | 23/06/2011    Suman B                            This is a test class for PRM_VPP_VelocitySpecialities    
 | 10/11/2011    Suman B                            Calling createVPPCustomSettingData() in Test methods.
 | 03/12/2013    Aagesh                             Modified for Back Arrow Project
 | 09/23/2014    Jaypal Nimesh                      Added startTest and stopTest to avoid mixed DML error
 +=============================================================================================*/

@isTest(seeAllData=false)
private class PRM_VPP_VelocitySpecialities_TC {
static testMethod void myUnitTest() {

    User partner;
    User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1]; 
    System.runAs(insertUser)
    {
    CustomSettingDataHelper.dataValueMapCSData();
    CustomSettingDataHelper.eBizSFDCIntCSData();
    CustomSettingDataHelper.bypassLogicCSData();
    CustomSettingDataHelper.countryTheaterMapCSData();
    CustomSettingDataHelper.velocityRulesData(); 
    }
    List<Account> lstAccounts=AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
    insert lstAccounts;
    for(Account acc : lstAccounts)
    {
        acc.IsPartner=true;
        acc.PROFILED_ACCOUNT_FLAG__c =true;
        acc.Cluster__c='EMEA 1B';
    }
    update lstAccounts;
    Contact con = UserProfileTestClassDataHelper.createContact();
    con.AccountId=lstAccounts[0].Id;
    con.Email='test23@emc.com';
    insert con;
    System.runAs(insertUser)
    {
     Profile amerUserProf = [select Id from Profile where Name=: 'AMER Distribution VAR Partner User'];
     partner = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.id,null,con.Id);
     
     //Added Start & Stop Test by Jaypal to avoid mixed DML errors. Date - 16-Sep-2014
     Test.startTest();
     insert partner; 
     Test.stopTest();
     
     partner.IsPortalEnabled=true;
     partner.isActive=True;
     update partner; 
        
     set<Id> setRecordTypeId = new set<Id>();
     Map<String,Schema.RecordTypeInfo> recordTypes = Velocity_Rules__c.sObjectType.getDescribe().getRecordTypeInfosByName(); 
     Id SpeicalityRuleRecTypeId = recordTypes.get('Specialty Rule').getRecordTypeId();
     setRecordTypeId.add(SpeicalityRuleRecTypeId);
     SpeicalityRuleRecTypeId = recordTypes.get('Field Based Rule Type').getRecordTypeId(); 
     setRecordTypeId.add(SpeicalityRuleRecTypeId); 
     system.debug('setRecordTypeId--------->'+setRecordTypeId);
      
     RecordType rectype=[SELECT IsActive, Id,SobjectType, SystemModstamp FROM RecordType where IsActive=true and Name like'%Specialty Rule%' and SobjectType='Velocity_Rules__c' limit 1];
      
     Account_Groupings__c  gp = new Account_Groupings__c();
     gp.name='test grouping';
     gp.account__c='test account';
     insert gp;
     List<Velocity_Rules__c> vrtot = new List<Velocity_Rules__c>();
     Velocity_Rules__c vr = new Velocity_Rules__c();
     vr.Display_Name__c='Cloud Architect';
     vr.RecordTypeId = rectype.id ;
     vr.Evaluation_at__c = 'Country Grouping Level';
     vr.BR_Sub_category__c='Avamar';
     vr.Specilaity__c='Backup and Recovery';
     vr.Specialty_Rule_Type__c = 'APJ-M';
     vrtot.add(vr);
     
     Velocity_Rules__c vr1 = new Velocity_Rules__c();
     vr1.Display_Name__c='Advanced Consolidate';
     vr1.RecordTypeId = rectype.id ;
     vr1.Evaluation_at__c = 'Country Grouping Level';
     vr1.BR_Sub_category__c='Avamar';
     vr1.Specilaity__c='Advanced Consolidate';
     vr1.Specialty_Rule_Type__c = 'APJ-M';
     vrtot.add(vr1);
     Velocity_Rules__c vr2 = new Velocity_Rules__c();
     vr2.Display_Name__c='Governance and Archive';
     vr2.RecordTypeId = rectype.id ;
     vr2.Evaluation_at__c = 'Country Grouping Level';
     vr2.BR_Sub_category__c='Networker';
     vr2.Specilaity__c='Governance and Archive';
     vr2.Specialty_Rule_Type__c = 'APJ-M';
     vrtot.add(vr2);
     Velocity_Rules__c vr3 = new Velocity_Rules__c();
     vr3.Display_Name__c='Consolidate';
     vr3.RecordTypeId = rectype.id ;
     vr3.Evaluation_at__c = 'Country Grouping Level';
     vr3.BR_Sub_category__c='Avamar';
     vr3.Specilaity__c='Consolidate';
     vr3.Specialty_Rule_Type__c = 'APJ-M';
     vrtot.add(vr3);
     Velocity_Rules__c vr4 = new Velocity_Rules__c();
     vr4.Display_Name__c='Cloud Builder Practice';
     vr4.RecordTypeId = rectype.id ;
     vr4.Evaluation_at__c = 'Country Grouping Level';
     vr4.BR_Sub_category__c='Networker';
     vr4.Specilaity__c='Cloud Builder Practice';
     vr4.Specialty_Rule_Type__c = 'APJ-M';
     vrtot.add(vr4);
     
     insert vrtot;
     
     List<Velocity_Rule_Results__c> vrrtot = new List<Velocity_Rule_Results__c>();
     Velocity_Rule_Results__c vrr = new Velocity_Rule_Results__c();
     vrr.AccountID__c =partner.contact.Account.id;
     vrr.Speciality_RuleID__c = vr.id;
     vrr.Grouping__c = gp.id;
     vrrtot.add(vrr);
     Velocity_Rule_Results__c vrr1 = new Velocity_Rule_Results__c();
     vrr1.AccountID__c =partner.contact.Account.id;
     vrr1.Speciality_RuleID__c = vr1.id;
     vrr1.Grouping__c = gp.id;
     vrrtot.add(vrr1);
     Velocity_Rule_Results__c vrr2 = new Velocity_Rule_Results__c();
     vrr2.AccountID__c =partner.contact.Account.id;
     vrr2.Speciality_RuleID__c = vr2.id;
     vrr2.Grouping__c = gp.id;
     vrrtot.add(vrr2);
     Velocity_Rule_Results__c vrr3 = new Velocity_Rule_Results__c();
     vrr3.AccountID__c =partner.contact.Account.id;
     vrr3.Speciality_RuleID__c = vr3.id;
     vrr3.Grouping__c = gp.id;
     vrrtot.add(vrr3);
     Velocity_Rule_Results__c vrr4 = new Velocity_Rule_Results__c();
     vrr4.AccountID__c =partner.contact.Account.id;
     vrr4.Speciality_RuleID__c = vr4.id;
     vrr4.Grouping__c = gp.id;
     vrrtot.add(vrr4);
     
     insert vrrtot;
    }
    
    
    System.runAs(partner){
                                            
    PRM_VPP_VelocitySpecialities velspObj = new PRM_VPP_VelocitySpecialities();
    velspObj.getEMCSpecialityStatusItems();
    velspObj.fetchSpecialityRuleResults(partner.contact.Account.id);
    velspObj.cancelUpdates();
    velspObj.updateProfileAccount();
    velspObj.preferredDistributorCheck(partner.contact.Account.id,partner.contact.Account.id);
    PRM_VPP_VelocitySpecialities.SpecialitySubDataContainer spesubdata=new PRM_VPP_VelocitySpecialities.SpecialitySubDataContainer();
    spesubdata.subCategoryName='Consolidate';
    spesubdata.subSpecialityStatus='Not deployed';
     } 
    
  }
}