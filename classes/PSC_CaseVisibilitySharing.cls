/*===========================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER                WR       DESCRIPTION                               
 |  ====          =========                ==       =========== 
 |  24-Apr-2015   Sneha Jain              Cases for PSC - Class to assign new users or delete users from Cases for PSC public group
 +=====================================================================================================================================================================*/
public with sharing class PSC_CaseVisibilitySharing {
  
    public void userAssignmentToPSCGroup(List<User> newUsrList, List<User> oldUsrList){
    
        List<GroupMember> grpMembrList = new List<GroupMember>();
        Set<Id> userIdToBeRemovedSet = new Set<Id>();
        Map<string,PSC_FieldMapping__c> customAll = PSC_FieldMapping__c.getAll();
        String profileIds = '';
        
        //Profile pro = [Select Id, Name from Profile where Name = 'AMER Channels User'];       
        Group pscGrp = [Select Id, Name, DeveloperName from Group where DeveloperName = 'PSC_Internal_Channel_Users'];
        
        for(PSC_FieldMapping__c csConfig: customAll.values())
        {
            if(csConfig.Name =='Case Visibility Public Group'){
                profileIds = csConfig.DependentValues__c;
            }
        }
        
        //Update scenario
        if(oldUsrList != null){
          
            for(Integer i=0 ; i < newUsrList.size() ; i++){
                if(newUsrList[i].IsActive && newUsrList[i].ProfileId != oldUsrList[i].ProfileId && profileIds != null && profileIds.contains(newUsrList[i].ProfileId)){
                    grpMembrList.add(new GroupMember(GroupId = pscGrp.Id,UserOrGroupId = newUsrList[i].Id));
                }
                else if(newUsrList[i].IsActive && newUsrList[i].ProfileId != oldUsrList[i].ProfileId && profileIds != null && !profileIds.contains(newUsrList[i].ProfileId)){
                    userIdToBeRemovedSet.add(newUsrList[i].Id);
                }
            }
          
            if(userIdToBeRemovedSet != null && userIdToBeRemovedSet.size() > 0){
                //Make a query on Groupmember for these user Ids and delete those. Life is easy 
                List<GroupMember> grpMemberDelList = [select id from GroupMember where GroupId =: pscGrp.Id and UserOrGroupId IN: userIdToBeRemovedSet];
                if(grpMemberDelList !=null && grpMemberDelList.size()>0){
                    delete grpMemberDelList;
                }
            }
          
            if(grpMembrList != null && grpMembrList.size() > 0){
                insert grpMembrList;
            }
        }
        
        //Insert Sceneraio
        else{
        
            for(User usr : newUsrList){
                if(usr.IsActive && profileIds != null && profileIds.contains(usr.ProfileId)){
                    grpMembrList.add(new GroupMember(GroupId = pscGrp.Id,UserOrGroupId = usr.Id));
                }
            }
          
            if(grpMembrList != null && grpMembrList.size() > 0){
                insert grpMembrList;
            }    
        }    
    }
}