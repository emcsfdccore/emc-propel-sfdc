/*==================================================================================================================+
 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 | 16 Mar 15    Aagesh J    GBS Sprint 3   Class called from trigger to prevent post on closed cases                                       
 +==================================================================================================================**/

public class GBS_ChatterOnClosedCases {
    
    public static void ClosedCaseChatter(List<FeedItem> newFeed){
    
        set<Id> feeditemSet = new set<Id>();
        for(FeedItem Feed1 : newFeed)
            {
            feeditemSet.add(Feed1.ParentId);
            }       
        
            Map<Id,case> mpcs = new Map<Id,Case>([select id, Status, Origin from case where Id IN : feeditemSet]);
        
            for(FeedItem newFeed1 : newFeed) {
                if(mpcs.containsKey(newFeed1.ParentId)){
                    if(mpcs.get(newFeed1.ParentId).Status == 'Closed'){
                        string st = URL.getCurrentRequestUrl().getPath();
                        if(st.containsIgnoreCase('EMCBusinessServices')){
                            newFeed1.addError(Label.GBS_Chatter_Message);
                        }
                    }
                    
                }
            }
    }
     
    public static void ClosedCaseChatterComment(List<FeedComment> newFeedComment){
    
        set<Id> feedcommentSet = new set<Id>();
        for(FeedComment Feed1 : newFeedComment)
            {
            feedcommentSet.add(Feed1.ParentId);
            }
    
            Map<Id,case> mpcs = new Map<Id,Case>([select id, Status, Origin from case where Id IN : feedcommentSet]);
        
            for(FeedComment newFeedComment1 : newFeedComment){
                if(mpcs.containsKey(newFeedComment1.ParentId)){
                    if(mpcs.get(newFeedComment1.ParentId).Status == 'Closed'){
                        string st = URL.getCurrentRequestUrl().getPath();
                        if(st.containsIgnoreCase('EMCBusinessServices')) {
                            newFeedComment1.addError(Label.GBS_Chatter_Message);
                        }
                    }
                    
                }
            }
    } 
}