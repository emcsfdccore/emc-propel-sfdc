/*==========================================================================================================================+
 |  HISTORY                                  
 |  DATE      DEVELOPER          WR       DESCRIPTION                 
 |  ====      =========          ==       =========== 
 |  24.Aug.2014   Bhanu Prakash            Initial Creation.  bulk updating Install Base Account Field on Lead. 
 |  19.Feb.2015   Vinod Jetti    #1649     replaced 'EMC_Classification__c' to 'EMC_Classification_Hub__c'' 
 +==========================================================================================================================*/

global class LeadInstallBaseUpdate_Batch implements Database.Batchable<sObject>, Database.Stateful{
  public Map<Id,User> mapUser;
  public List<Lead> leads;

  //global LeadInstallBaseUpdate_Batch(Map<Id,User> mapUser){
  //  this.mapUser = mapUser;
  //}

  global LeadInstallBaseUpdate_Batch(Map<Id,User> mapUser, List<Lead> leads){
    this.mapUser = mapUser;
    this.leads = leads;
  }
  global LeadInstallBaseUpdate_Batch(){
    System.debug('&&&&&&&& in BaseUpdateBatch constructer.... ');
  }
  global Database.QueryLocator start(Database.BatchableContext BC){
    System.debug('&&&&&&&& in BaseUpdateBatch start().... ');
    //return null;
    
    return Database.getQueryLocator([SELECT id,Install_Base_Account__c,Related_Account__c,Related_Account__r.Install_Base_Account__c,
                                 DealReg_Deal_Registration__c,Street,City,State,PostalCode, Country, CompanyLocal, company,
                                 State_Province_Local__c,City_Local__c,Street_Local__c, Country_Local__c,
                                 Zip_Postal_Code_Local__c,DealReg_Account_Category__c ,DealReg_Channel_Account_Manager__c,ownerId,
                                 Related_Account__r.Street_Local__c, Related_Account__r.Zip_Postal_Code_Local__c, 
                                 Related_Account__r.State_Province_Local__c,Related_Account__r.Country_Local__c, 
                                 Related_Account__r.City_Local__c, Related_Account__r.NameLocal, Related_Account__r.BillingCountry, 
                                 Related_Account__r.BillingPostalCode,Related_Account__r.BillingState,Related_Account__r.BillingCity,
                                 Related_Account__r.BillingStreet,Related_Account__r.Name ,Related_Account__r.EMC_Classification_Hub__c  
                              FROM Lead where Id in :leads]);
    
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
  List<Lead> lstLeadInstallBAcc = (List<Lead>) scope;
  System.debug('&&&&&&&& in BaseUpdateBatch execute().... lstLeadInstallBAcc size = '  + lstLeadInstallBAcc.size());
  Acc_updateInstallBaseAccount.performBaseUpdate(lstLeadInstallBAcc, mapUser);
  System.debug('&&&&&&&& in BaseUpdateBatch execute().... after  performBaseUpdate() execution '  + lstLeadInstallBAcc.size());
   
  }

   global void finish(Database.BatchableContext BC){
   }
}