/*==================================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        =========== 
 |  10/Apr/2015   Bisna V P     CI 1655    Created class: R2R - RQF Quote Access in SFDC
 ====================================================================================================================*/
public class QuoteSummaryGeneratorController{

    public List<Asset__c> listAsset {get;set;}
    public User loggedInUser {get;set;}
    public string businessGroup{get;set;}
    public string templateCode{get;set;}
    public string language {get;set;}
    public string quoteId {get;set;}
    public string assetId {get;set;}
    
    public QuoteSummaryGeneratorController(){
    
    }       
    public PageReference redirectPage(){
        
        if(!Test.isRunningTest())
        assetId= ApexPages.currentPage().getparameters().get('assetId');
        quoteId = '';
        if(assetId!= null && assetId!='')
            listAsset = new List<Asset__c>([select id, Quote_Number__c,Next_Quoted_Contract_Status__c from Asset__c where id=:assetId]);
            if(listAsset != null && listAsset.size()>0){
            quoteId =   listAsset[0].Quote_Number__c;
            loggedInUser = [select id, Name,LanguageLocaleKey,Theater__c from User where id=:(UserInfo.getUserId())];
            
            //mapping respective BU's
            if(loggedInUser.Theater__c=='APJ')
                businessGroup = 'EMC-APJK';
            else if(loggedInUser.Theater__c=='EMEA')
                businessGroup = 'EMC-EMEA';
            else if(loggedInUser.Theater__c=='Americas'|| loggedInUser.Theater__c=='N/A'||loggedInUser.Theater__c=='Global'||loggedInUser.Theater__c=='V Specialist Global')
                businessGroup = 'EMC-NALA';
            else 
                businessGroup = 'EMC-NALA';
                
            //mapping language
            if(loggedInUser.LanguageLocaleKey=='en_US')
                language = 'en';
            else if(loggedInUser.LanguageLocaleKey=='fr')
                language = 'fr-FR';
            else if(loggedInUser.LanguageLocaleKey=='de')
                language = 'de-DE';
            else if(loggedInUser.LanguageLocaleKey=='it')
                language = 'it-IT';
            else if(loggedInUser.LanguageLocaleKey=='ja')
                language = 'ja-JP';
            else if(loggedInUser.LanguageLocaleKey=='ko')
                language = 'ko-KR';
            else if(loggedInUser.LanguageLocaleKey=='es')
                language = 'es-ES';
            else if(loggedInUser.LanguageLocaleKey=='pt_BR')
                language = 'pt-PT';
            else 
                language = 'en';
                        
            if(listAsset[0].Next_Quoted_Contract_Status__c == 'Customer Quoted'||listAsset[0].Next_Quoted_Contract_Status__c == 'Active'||listAsset[0].Next_Quoted_Contract_Status__c == 'Signed'||listAsset[0].Next_Quoted_Contract_Status__c == 'Quote Won'){     
                if(businessGroup== 'EMC-APJK' && (language == 'fr-FR' ||language == 'de-DE' ||language == 'it-IT' ||language =='pt-PT'|| language == 'es-ES')){
                language = 'en';                
                }
                if(businessGroup== 'EMC-EMEA' && (language == 'ja-JP' ||language == 'ko-KR')){
                language = 'en';
                }
                if(businessGroup== 'EMC-NALA' && (language == 'fr-FR')){
                language = 'fr-CA';
                }
                if(businessGroup== 'EMC-NALA' && (language == 'ja-JP' ||language == 'ko-KR'||language == 'pt-PT'||language == 'it-IT' || language == 'de-DE')){
                language = 'en';
                }
                
                
                templateCode = 'EMCCS_RQF_ALL_OUTPUTS';                 
            }
            else if(listAsset[0].Next_Quoted_Contract_Status__c == 'Budgetary Estimate'){
                language = 'en';
                if(businessGroup =='EMC-APJK'|| businessGroup =='EMC-NALA')
                    templateCode ='EMCCS_RQF_ESTIMATE_WITHOUTBT_OUTPUT_TEMPLATE';
                else if(businessGroup =='EMC-EMEA')
                    templateCode = 'EMCCS_RQF_ESTIMATE_OUTPUT_TEMPLATE_EMEA';
            }
                
        
        if(quoteId != null && businessGroup != null && templateCode != null && language != null)
        {
        return new PageReference(System.Label.RQFUrl+'?QUOTE_ID='+quoteId+'&BUSINESS_GROUP='+businessGroup+'&TEMPLATE_CODE='+templateCode+'&LANGUAGE='+language+'&GROUP_BY=SFDC&TABS=null');
        }
        else
        {
        ApexPages.Message myMsg;
        myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.RQFerrorMessage);
        ApexPages.addMessage(myMsg);
        return null;
        }
        }
        
        else
        {
        ApexPages.Message myMsg;
        myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.RQFerrorMessage);
        ApexPages.addMessage(myMsg);
        return null;
        }
        
    }

}