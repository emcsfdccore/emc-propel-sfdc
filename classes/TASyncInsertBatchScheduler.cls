/*=====================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  23.11.2012      Avinash K       212738      Initial Creation.  TA Sync Batch job Scheduler to insert TA Sync records.
 |  18.02.2014      Aarti Jindal    Visibility  Added custom setting TASyncInsertBatchExemptProfiles__c to exempt marketing users.
 |  17.12.2014      Alok Kumar                   Added check to exclude top offenders and users with already existing TA sync record 
 +=====================================================================================================================*/

global class TASyncInsertBatchScheduler implements Schedulable
{
  global void execute(SchedulableContext sc) 
    {

        String strBatchQuery = buildBatchQuery();

        if(strBatchQuery != null && strBatchQuery != '')
        {
            TA_Sync_Log_Insert batch = new TA_Sync_Log_Insert(strBatchQuery) ;
            Map<String,CustomSettingDataValueMap__c> mapDataValueMap = CustomSettingDataValueMap__c.getAll();
            Integer intNoOfRecordsPerBatch;

            
            if(mapDataValueMap!= null &&
                        mapDataValueMap.get('Number Of Records per TA Sync Batch') != null &&
                        mapDataValueMap.get('Number Of Records per TA Sync Batch').DataValue__c != null &&
                        Integer.valueOf(mapDataValueMap.get('Number Of Records per TA Sync Batch').DataValue__c) != null &&
                        Integer.valueOf(mapDataValueMap.get('Number Of Records per TA Sync Batch').DataValue__c) <= 200)
            {
                system.debug('#### Entered batch calling area');
                intNoOfRecordsPerBatch = Integer.valueOf(mapDataValueMap.get('Number Of Records per TA Sync Batch').DataValue__c);
                Database.executeBatch(batch, intNoOfRecordsPerBatch);
            }
        }
    }
 
  global String buildBatchQuery()
  {
    Map<String,CustomSettingDataValueMap__c> mapDataValueMap = CustomSettingDataValueMap__c.getAll();
    List<TASyncInsertBatchExemptProfiles__c> prof=TASyncInsertBatchExemptProfiles__c.getAll().values();
    String strProfiles = null;
    set<string> userWithLogRecords = new set<string>();
    string userLog = '';
    List<string> topOffendersId = new List<string>();
    string topOff = '';
    integer noOfUsers =  Integer.valueOf(mapDataValueMap.get('Number of Users Per TA Sync Request').DataValue__c);
    integer openRequestsCount = [select count() from TA_Sync_Log__c where Status__c = 'Open'];
    if(openRequestsCount < noOfUsers)
      {
        integer setLimit = noOfUsers - openRequestsCount;
        List<TA_Sync_Log__c> pendingToOpen = [select id, Sales_Resource__r.name, Row_Count__c,createddate  from TA_Sync_Log__c where Status__c = 'Pending' order by createddate asc limit :setLimit ];
        List<TA_Sync_Log__c> updateToOpen = new List<TA_Sync_Log__c>();
        
        for(TA_Sync_Log__c ta:pendingToOpen )
        {
           ta.Status__c = 'Open';
           updateToOpen.add(ta);
        }
        
        update updateToOpen;
     }
      
     List <TA_Sync_Log__c> existing_TA_Sync_Logs = [Select Sales_Resource__r.id, Sales_Resource__c,status__c, CreatedDate from TA_Sync_Log__c where (CreatedDate >:Date.today()-7 OR status__c=:'Open' OR Status__c =:'EIS Insertions Complete' OR Status__c=:'Rowcount Validated')];
     for(TA_Sync_Log__c ta: existing_TA_Sync_Logs)
     {
       userWithLogRecords.add(ta.Sales_Resource__r.id);
     }
     
     integer userLogSize = userWithLogRecords.size();
     integer incre = 1;
     for(string userLogRecord: userWithLogRecords)
     if(incre < userLogSize)
         {
           userLog = userLog + '\'' + userLogRecord+ '\''+',';
           incre++;
         }
     else
         {
           userLog = userLog + '\'' + userLogRecord + '\'';
         }
  
    List<group> topOffendersGroups = [select id , Name from group where name Like 'TOPUB%' or name Like 'TOPVT%'];
    List<groupmember> groupMemberRecord = [select UserOrGroupId from GroupMember where GroupId IN :topOffendersGroups];
    
    integer listSize = groupMemberRecord.size();
    integer increment = 1;
    for(groupmember gm:groupMemberRecord)
    {
        if(increment<listSize )
        {
          topOff = topOff + '\'' + gm.UserOrGroupId + '\''+',';
          increment++;
        }
        else{
          topOff = topOff + '\'' + gm.UserOrGroupId + '\'';
        }
               
    }
    
   
     
    if(prof!=null)
    {
        for(TASyncInsertBatchExemptProfiles__c objTemp : prof){
            If(strProfiles == null)
            {
            strProfiles = '\'' + objTemp.name;
            }

            Else
            {
            strProfiles = strProfiles  + '\''+','+'\''+objTemp.name;
            }
         }
        strProfiles=strProfiles+ '\'';
     }   
    
    Integer intLimit;
    Integer InsertLimit;
    Date dteThirtyDaysBack = System.Today() - 30;
    String strQuery;
    string strIds = '';
    set<Id> userIdtobeIgnored = new set<Id>();
    set<Id> userInOpenStatus = new set<id>();
    Map<Id,TA_Sync_Log__c> mapWorkingTA= NEW map<Id,TA_Sync_Log__c>([Select id,name,status__c,Sales_Resource__c from TA_Sync_Log__c where status__c=:'Open' OR Status__c =:'EIS Insertions Complete' OR Status__c=:'Rowcount Validated' limit 100]);
    //userIdtobeIgnored=mapWorkingTA.keyset();
    if(mapDataValueMap!= null &&
                    mapDataValueMap.get('Number of Users Per TA Sync Request') != null &&
                    mapDataValueMap.get('Number of Users Per TA Sync Request').DataValue__c != null &&
                    Integer.valueOf(mapDataValueMap.get('Number of Users Per TA Sync Request').DataValue__c) != null &&
                    Integer.valueOf(mapDataValueMap.get('Number of Users Per TA Sync Request').DataValue__c) <= 50000)
      intLimit = Integer.valueOf(mapDataValueMap.get('Number of Users Per TA Sync Request').DataValue__c);
    else
      intLimit = 0;
          ////////////////////////////// 
         
    for(TA_Sync_Log__c ta:mapWorkingTA.values())
    {
      userIdtobeIgnored.add(ta.Sales_Resource__c);
      if(ta.status__c=='Open')
      {
        userInOpenStatus.add(ta.Sales_Resource__c);
      }
    }
    strIds = fn_getStringIds(userIdtobeIgnored);     
    
    if(intLimit>0)
    {
      InsertLimit= intLimit-userInOpenStatus.size();
      if(Util.isTestCoverage==true)
      {
        InsertLimit=1;
      }
    }
    
//////////////////////////////////////////////Logic to count number of request to be inserted per batch.//////////////////////////////////


   if(InsertLimit>0)   
   {  
    // fo String strQuery = 'select name, id, Profile_Name__c, Last_TA_Synch_Date__c from User where isActive = true AND id = \'00570000001LDv8AAG\'';
    // \'00570000001djXQ\',\'00570000001fvfX\',\'00570000001LCzr\'
    strQuery = 'select name, id, Profile_Name__c, Last_TA_Synch_Date__c, ProfileId, UserRoleId from User where isActive = true AND id NOT IN '+'('+topOff +') '+' And id NOT IN '+'('+userLog +')'+
       'And Profile_Name__c NOT IN('+strProfiles +')AND(NOT Profile_Name__c  like \'%VAR%\') AND (NOT Profile_Name__c  like \'%Distributor%\') AND (NOT Profile_Name__c  like \'%Reseller%\') AND(Last_TA_Synch_Date__c = null OR Last_TA_Synch_Date__c < :dteThirtyDaysBack) AND UserType <> \'CSNOnly\' AND UserType  <> \'Guest\' ';       
    system.debug('#### strIds :: '+strIds);
    
    if(strIds != null && strIds.trim() != '')
      strQuery += ' AND id not in  '+strIds ;
    strQuery +=' ORDER BY Last_TA_Synch_Date__c asc limit '+InsertLimit;
    System.debug('strQuery==>'+strQuery);
   }
system.debug('####  strQuery ::'+strQuery);

    return strQuery;
  }
   public static string fn_getStringIds(Set<id> IdsToConcatenate_List){
    string strReturnedIds;
system.debug('#### IdsToConcatenate_List :: '+IdsToConcatenate_List);
    if(IdsToConcatenate_List != null && IdsToConcatenate_List.size() > 0)
      strReturnedIds = '(';
    for(id idItem : IdsToConcatenate_List)
    {
     if(strReturnedIds.length() > 1) //If strReturnedIds is not empty, adds a comma as separator before adding new values.
      strReturnedIds += ','; 
     strReturnedIds += '\'' + idItem + '\'';
    }
    if(IdsToConcatenate_List != null && IdsToConcatenate_List.size() > 0)
      strReturnedIds += ')';
    return strReturnedIds;
 }
}