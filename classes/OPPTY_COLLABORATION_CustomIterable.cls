/**
===========================================================================+
 |  Created History                                                                  
                                                                 
 |  DATE        DEVELOPER       WORK REQUEST            DESCRIPTION                               
 |  ====        =========       ============            =========== 
 |  4 Mar 2014  Avinash K       Oppty Collaboration     Acts as an Iterator class to list the Partner Users along
                                                        with checkboxes to be selected, navigated to next/previous pages and still retaining 
                                                        their selection for Auxiliary sharing of Opportunities with Partner Users
                                                        This class has logic for next/previos and insertion of selected Partner users
=============================================================================+
**/


global class OPPTY_COLLABORATION_CustomIterable implements Iterator<list<OPPTY_COLLABORATION_InnerUser>>
{ 
    list<OPPTY_COLLABORATION_InnerUser> lstInnerUser {get; set;} 
    list<OPPTY_COLLABORATION_InnerUser> lstInnerUsersToShow {get; set;} 
    private OPPTY_COLLABORATION_InnerUser objInnerUser;
    private Integer intPageCursor; 
    public Integer intPageSize {get; set;} 

    
/**
Constructor
Takes in:
lstPartnerUser - Active Partner user list of Distribution VAR & Distributor/Direct Reseller accounts of the Opportunity
mapIdAccountName - Map of Account Id and its Name
setIdExistingPartnerOTM - Set of Existing Partner Users in the Opportunity Sales Team
Constructs a list of Partner Users (wrapper class records) that are not already in the Opportunity Sales Team 
**/
    public OPPTY_COLLABORATION_CustomIterable (List<User> lstPartnerUser, MAP<Id, String> mapIdAccountName, Set<Id> setIdExistingPartnerOTM)
    { 

      lstPartnerUser.sort();

        lstInnerUser = new list<OPPTY_COLLABORATION_InnerUser>(); 
        lstInnerUsersToShow = new list<OPPTY_COLLABORATION_InnerUser>();     
        for(User usr : lstPartnerUser) 
        {
            if(!setIdExistingPartnerOTM.contains(usr.id))
            {
                objInnerUser = new OPPTY_COLLABORATION_InnerUser(false, usr, mapIdAccountName.get(usr.AccountId));
                lstInnerUser.add(objInnerUser);
            }
        }
        
        intPageCursor = 0;
    }

    
/**
hasNext()
Returns a boolean true/false
TRUE - If there are more Partner Users in the list than the ones displayed in current and/or previous pages
FALSE -  If there are NO more Partner Users in the list than the ones displayed in current and/or previous pages
**/
    global boolean hasNext()
    { 
        if(intPageCursor >= lstInnerUser.size()) 
        {
            return false; 
        } 
        else 
        {
            return true; 
        }
    } 

    
/**
hasPrevious()
Returns a boolean true/false
TRUE - If the user is on last page
FALSE -  If the user is on first page
**/
    global boolean hasPrevious()
    { 
        if(intPageCursor <= intPageSize) 
        {
            return false; 
        } 
        else 
        {
            return true; 
        }
    }   

    
/**
next()
Returns a list of Partner Users (wrapper class records) to be shown initially and when the user clicks on "Next" button
**/
    global list<OPPTY_COLLABORATION_InnerUser> next()
    {       
        lstInnerUsersToShow = new list<OPPTY_COLLABORATION_InnerUser>(); 
        integer intStartNumber;
        integer intSize = lstInnerUser.size();
        if(hasNext())
        {  
          if(intSize <= (intPageCursor + intPageSize))
          {
            intStartNumber = intPageCursor;
            intPageCursor = intSize;
          }
          else
          {
            intPageCursor = (intPageCursor + intPageSize);
            intStartNumber = (intPageCursor - intPageSize);
          }


          for(integer start = intStartNumber; start < intPageCursor; start++)
          {
            lstInnerUsersToShow.add(lstInnerUser[start]);
          }
        } 
        return lstInnerUsersToShow;
    } 

    
/**
previous()
Returns a list of Partner Users (wrapper class records) to be shown when the user clicks on "Previous" button
**/
    global list<OPPTY_COLLABORATION_InnerUser> previous()
    {      
        lstInnerUsersToShow = new list<OPPTY_COLLABORATION_InnerUser>(); 
        integer intSize = lstInnerUser.size(); 
        if(intPageCursor == intSize)
        {
          if(math.mod(intSize, intPageSize) > 0)
          {    
              intPageCursor = intSize - math.mod(intSize, intPageSize);
          }
          else
          {
              intPageCursor = (intSize - intPageSize);
          } 
        }
        else
        {
            intPageCursor = (intPageCursor - intPageSize);
        }


        for(integer start = (intPageCursor - intPageSize); start < intPageCursor; ++start)
        {
            lstInnerUsersToShow.add(lstInnerUser[start]);
        } 
        return lstInnerUsersToShow;
    }


/**
updateSalesTeam(...)
Takes in the Opportunity record and the set of existing Partner Users Ids in Sales Team
Inserts the selected partner users (via checkbox) to the Opportunity Sales Team with Role = 'Partner User'
**/
    public void updateSalesTeam(Opportunity objOpp, Set<Id> setIdExistingPartnerOTM)
    {
        List<OpportunityTeamMember> lstOppTeamMember = new List<OpportunityTeamMember>();

        String strReasonForAddition = '', strAddedThrough = '';

        Map<String,OptyCollab_Custom_Setting__c> mapOpptyCollabCustomSetting = OptyCollab_Custom_Setting__c.getAll();
        
        if(mapOpptyCollabCustomSetting != null && mapOpptyCollabCustomSetting.get('opptyTeamValues') != null)
        {
            if(mapOpptyCollabCustomSetting.get('opptyTeamValues').reasonForAddition__c != null)
            {
                strReasonForAddition = mapOpptyCollabCustomSetting.get('opptyTeamValues').reasonForAddition__c;
            }

            if(mapOpptyCollabCustomSetting.get('opptyTeamValues').addedThrough__c != null)
            {
                strAddedThrough = mapOpptyCollabCustomSetting.get('opptyTeamValues').addedThrough__c;
            }
        }

        for (OPPTY_COLLABORATION_InnerUser innerUser : lstInnerUser) 
        {
            
            if (innerUser.blnIsSelected && (setIdExistingPartnerOTM == null || !setIdExistingPartnerOTM.contains(innerUser.usr.id))) 
            {
                lstOppTeamMember.add(new OpportunityTeamMember(
                    OpportunityId = objOpp.id, 
                    UserId = innerUser.usr.id, 
                    TeamMemberRole = 'Partner User', 
                    Reason_for_Addition__c = strReasonForAddition, 
                    Added_Through__c = strAddedThrough));
            }
        }

        if(lstOppTeamMember != null && lstOppTeamMember.size() > 0)
        {
            Database.SaveResult[] lsr = Database.insert(lstOppTeamMember, false);
        }
    }

}