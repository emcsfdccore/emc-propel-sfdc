global class AutomateMaskingBatch implements Database.Batchable<sObject>{
	public String Query;
	public List<String> Fields = new List<String>();
	public String MaskingText;
	private boolean isValueSet;
	private string strObjectName;
	//Ketan Benegal - 07/23/2013 - Code Start.
	public CustomSettingBypassLogic__c LeadContact_bypass_value;

	global AutomateMaskingBatch(String objectName, List<String> maskingFields, String maskText, String citeria ){
		//Ketan Benegal - 07/23/2013 - Code Start.
		isValueSet = false;
		this.strObjectName = objectName;
		LeadContact_bypass_value = CustomSettingBypassLogic__c.getOrgDefaults(); //[ SELECT Id,By_Pass_Lead_Validation_Rules__c FROM CustomSettingBypassLogic__c Limit 1] ;
		MaskingText=maskText;
		Fields.addall(maskingFields);
		Query='Select ' ;
		System.debug('Query'+Query);
		// changes done by sheriff
		for(integer i=0; i<maskingFields.size(); i++){
			Query = Query  + maskingfields[i] + ' , ';
		}

		if(Query != null && Query != '' && Query !='Select ' && query.endsWith(', ')) {
			Query = Query.substring(0,Query.length() -2);
		}
		Query=Query +' '+ 'from' + ' '+ objectName + ' ' + 'where' + '( ';
		for(integer i=0; i<maskingFields.size(); i++){
			Query = Query +'('+maskingfields[i] + ' ' + '!=Null' + ' and (NOT (' +  maskingfields[i] + ' ' + 'like \'%'+MaskingText  + '\' )))' + ' OR ' ;
		}
		if(Query != null && Query != '' && Query !='Select ' && query.endsWith('OR ')) {
			Query = Query.substring(0,Query.length() -3) + ' )';
		}  
		/*
        integer z=maskingfields.size()-1;
        Query = Query +'('+maskingfields[Z] + ' ' + '!=Null' + ' and (NOT (' +  maskingfields[z] + ' ' + 'like \'%'+MaskingText  + '\' ))))'  ;
		 */
		if(citeria != null && citeria !=''){
			Query = Query + ' and (' +citeria +')'; 
		}
		System.debug('Query '+Query);
	}
	global Database.QueryLocator start(Database.BatchableContext BC){
		//Ketan Benegal - 07/23/2013 - Code Start. - Set the values of Custom Setting.
		if(LeadContact_bypass_value != null){
			setBypassValidationRules(true);
			update LeadContact_bypass_value;
			isValueSet = true;
		}
		//Ketan Benegal - 07/23/2013 - Code End.
		return Database.getQueryLocator(query);
	}

	//Ketan Benegal - 07/23/2013 - added Try-Catch-Finally block.

	global void execute(Database.BatchableContext BC, List<sObject> scope){
		try{

			List <EMCException> errors = new List <EMCException>();
			for(sobject s : scope){
				for(String Field: Fields ){
					if(s.get(Field)!=null && s.get(Field)!='')
						s.put(Field,s.get(Field)+ MaskingText ) ;
				}
			}
			Database.SaveResult[] Results=database.update(scope,false);

			for(Database.SaveResult sr: Results)
			{
				String dataErrs = '';
				if(!sr.isSuccess())
				{
					for(Database.Error dbe : sr.getErrors())
					{
						dataErrs += dbe.getMessage();
						system.debug('-------Results----dbe.getMessage()----'+dbe.getMessage());
					}
					errors.add(new EMCException(dataErrs, BC.getjobId(), new String [] {sr.getId()}));
				}
			}
			system.debug('-------errors----errors----'+errors);
			if (errors.size() > 0) { 
				EMC_UTILITY.logErrors(errors); 
			}   
		}catch(Exception Ex){

		}finally{
			//Ketan Benegal - 07/23/2013 - Code Start. - ReSet the values of Custom Setting.
			if(LeadContact_bypass_value != null){
				setBypassValidationRules(false);
				update LeadContact_bypass_value;
				isValueSet ^= true;				
			}
			//Ketan Benegal - 07/23/2013 - Code End. 
		}
	}
	global void finish(Database.BatchableContext BC){
		//Ketan Benegal - 07/23/2013 - Code Start. - ReSet the values of Custom Setting.
		if(isValueSet && LeadContact_bypass_value != null){
			setBypassValidationRules(false);
			update LeadContact_bypass_value;
			isValueSet = false;
		}
		//Ketan Benegal - 07/23/2013 - Code End. 
	}

	//Ketan Benegal - 07/23/2013 - Code Start. - ReSet the values of Custom Setting. XOR operator used to reverse the current value.
	private void setBypassValidationRules(boolean setCSVal){
		if(LeadContact_bypass_value != null){
			if(this.strObjectName == 'Contact')
				LeadContact_bypass_value.By_Pass_Contact_Validation_Rules__c ^= true;
			else{
				if(this.strObjectName == 'Lead')
					LeadContact_bypass_value.By_Pass_Lead_Validation_Rules__c ^= true; 
			}
		}
	}
	//Ketan Benegal - 07/23/2013 - Code End. 
}