/*=====================================================================================================+
|  HISTORY  |                       
|  DATE          DEVELOPER               WR         DESCRIPTION 
  ====          =========               ==         =========== 
|  06/07/2011    Ashwini Gowda                      This class is used to unit testing for 
                                                    PRM_DEALREG_ApprovalRouting Class
|  12/07/2011    Anirudh Singh                      Updated Test Data Creation as per the Validation Rules.   
   11/1/2011     Prasad                             Fix issue 101 SOQL limit     
   2 Dec 2011    Arif                               Updated createLead method to populate some mandatory fields for entering in approval process.
 | 11-JAN-2012    Anil                              Fixed Test Class for Jan'12 Release   
   11 April 2012  Arif                189527        Commented lines where 'DealReg_Extension_Justification__c' field is involved 
                                                    to change the data type of the field.    
   11 April 2012  Arif                189527        Uncommented back all the lines where 'DealReg_Extension_Justification__c' field
                                                    involved after changing data type.
   11 April 2012  Arif                189648        Changed the status of lead to 'Submitted' in createLead method
                                                    for bypassing a new validation rule. 
   02 May 2012    Arif                              Deleting EMEA DR Routing Mapping and EMEA Field Representative object.Commented lines  
   30 Nov 2012    Vivek                             Added custom setting.   
   07 Jan 2013    Krishna                           Test Class erros fix.                                       
 +=====================================================================================================*/
@isTest
private class PRM_DEALREG_ApprovalRouting_TC  
{
    // To cover calculateQuaterDate methods 
    static testmethod void createLeadShareForQueuesTM()
    {
        //Creating custom setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.VisibilityProgramCSData();
            CustomSettingDataHelper.dealApprovalDateCheckCSData();
        }
        
        //Insert user record where Profile is Sys Admin and Forecast Group is Maintenance Renewals
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='testR2Ruser1@emailR2RTest.com', alias='tuser1', CommunityNickName='tuser1', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' ,Forecast_Group__c = 'Maintenance Renewals'); 
        
        insert insertUser;
        
        Map<String,Schema.RecordTypeInfo> recordTypes = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id accRecordTypeId = recordTypes.get('Customer Account Record Type').getRecordTypeId(); 
        /*Creating account record*/
        Account objAccount = new Account();
        objAccount.name = 'UNITTESTAcc';
        objAccount.Party_Number__c = '8235';
        objAccount.recordTypeId = accRecordTypeId;
        objAccount.BillingCountry ='Colombia';
        objAccount.Synergy_Account_Number__c = '10';
        insert objAccount;
        
        Map<String,Deal_Reg_Approval_Date_Check__c> submissiondates = Deal_Reg_Approval_Date_Check__c.getall();
        Map<String,DealRegistration_CustomSetting__c>  data =  DealRegistration_CustomSetting__c.getall();
        String AmerPSCDRQueue = data.get('AmerPSCDRQueue').PSC_QueueId__c;
        String ApjPSCDRQueue = data.get('ApjPSCDRQueue').PSC_QueueId__c;
        String EmeaPSCDRQueue = data.get('EmeaPSCDRQueue').PSC_QueueId__c; 
        
        Map<Id,Lead> newLeadMap = new Map<Id,Lead>();
        Map<Id,Lead> oldLeadMap = new Map<Id,Lead>();
        
        Map<Id,Lead> newDealRegMap = new Map<Id,Lead> ();
        Map<Id,Lead> oldDealRegMap = new Map<Id,Lead> ();
        
        list<lead> lstLead = new list<lead>();
        lstLead.add(new lead ( LastName ='DealregLastName0', Company ='TestDealreg' , DealReg_Theater__c ='EMEA',Email ='Dealreg1@dealreg.com', Original_Submission_Time__c = null, DealReg_Submission_Date__c = System.now(), phone = '6788993', DealReg_Deal_Registration__c = true , DealReg_PSC_Owner__c = insertUser.id, related_account__c = objAccount.id , SendOutboundNotification__c = true ));
        lstLead.add(new lead ( LastName ='DealregLastName1', Company ='TestDealreg' , DealReg_Theater__c ='APJ',Email ='Dealreg1@dealreg.com', Original_Submission_Time__c = null, DealReg_Submission_Date__c = System.now(), phone = '6788993', DealReg_Deal_Registration__c = true , DealReg_PSC_Owner__c = insertUser.id, related_account__c = objAccount.id ));
        lstLead.add(new lead ( LastName ='DealregLastName2', Company ='TestDealreg' , DealReg_Theater__c ='Americas',Email ='Dealreg1@dealreg.com', phone = '6788993', DealReg_Deal_Registration__c = true, related_account__c = objAccount.id));
        lstLead.add(new lead ( LastName ='DealregLastName3', Company ='TestDealreg' , DealReg_Theater__c ='EMEA',Email ='Dealreg1@dealreg.com', phone = '6788993', DealReg_Deal_Registration__c = true, Approval_Status__c ='Approved By Field Rep', related_account__c = objAccount.id ));
        lstLead.add(new lead ( LastName ='DealregLastName4', Company ='TestDealreg' , DealReg_Theater__c ='APJ',Email ='Dealreg1@dealreg.com', phone = '6788993', DealReg_Deal_Registration__c = true, related_account__c = objAccount.id));
        lstLead.add(new lead ( LastName ='DealregLastName5', Company ='TestDealreg' , DealReg_Theater__c ='Americas',Email ='Dealreg1@dealreg.com', DealReg_PSC_Owner__c = insertUser.id));
        lstLead.add(new lead ( LastName ='DealregLastName6', Company ='TestDealreg' , DealReg_Theater__c ='EMEA',Email ='Dealreg1@dealreg.com'));
        lstLead.add(new lead ( LastName ='DealregLastName7', Company ='TestDealreg' , DealReg_Theater__c ='APJ',Email ='Dealreg1@dealreg.com'));
        lstLead.add(new lead ( LastName ='DealregLastName8', Company ='TestDealreg' , DealReg_Theater__c ='APJ',Email ='Dealreg1@dealreg.com'));
        lstLead.add(new lead ( LastName ='DealregLastName9', Company ='TestDealreg' , DealReg_Theater__c ='Americas',Email ='Dealreg1@dealreg.com'));
        lstLead.add(new lead ( LastName ='DealregLastName10', Company ='TestDealreg' , DealReg_Theater__c ='EMEA',Email ='Dealreg1@dealreg.com'));
        lstLead.add(new lead ( LastName ='DealregLastName11', Company ='TestDealreg' , DealReg_Theater__c ='APJ',Email ='Dealreg1@dealreg.com', DealReg_Deal_Registration_Status__c = 'PSC Declined' ));
        lstLead.add(new lead ( LastName ='DealregLastName5', Company ='TestDealreg' , Email ='Dealreg1@dealreg.com' , phone = '6788993', DealReg_Field_SLA_Expire_on__c = system.now()- 2, DealReg_Deal_Registration_Status__c = 'Submitted', DealReg_Theater__c = 'EMEA', Approval_Status__c = 'Submitted By Field Rep',related_account__c = objAccount.id ));
        insert lstLead;
        
        list<Registration_Product__c> listRegProduct = new list<Registration_Product__c> ();
        listRegProduct.add(new Registration_Product__c (Deal_Registration__c = lstLead[11].id ));
        insert listRegProduct;
        
        Map<Id,List<Registration_Product__c>> leadWithProductsMap = new Map<Id,List<Registration_Product__c>>();
        leadWithProductsMap.put(lstLead[11].id, listRegProduct);
        
        oldLeadMap.put(lstLead[0].id,lstLead[0]);
        oldLeadMap.put(lstLead[1].id,lstLead[1]);
        
        oldDealRegMap.put(lstLead[5].id , lstLead[5]); 
        oldDealRegMap.put(lstLead[6].id , lstLead[6]); 
        oldDealRegMap.put(lstLead[7].id , lstLead[7]); 
        oldDealRegMap.put(lstLead[8].id , lstLead[8]); 
        oldDealRegMap.put(lstLead[9].id , lstLead[9]); 
        oldDealRegMap.put(lstLead[10].id , lstLead[10]); 
        
        lstLead[0].DealReg_Deal_Registration_Status__c='Approved';
        lstLead[1].DealReg_Deal_Registration_Status__c='Approved';
        lstLead[2].DealReg_Deal_Registration_Status__c='Submitted';
        lstLead[3].DealReg_Deal_Registration_Status__c='Submitted';
        lstLead[4].DealReg_Deal_Registration_Status__c='Submitted';
        lstLead[5].DealReg_Theater__c='EMEA';
        lstLead[6].DealReg_Theater__c='APJ';
        lstLead[7].DealReg_Theater__c='Americas';
        lstLead[8].DealReg_Theater__c='EMEA';
        lstLead[9].DealReg_Theater__c='APJ';
        lstLead[10].DealReg_Theater__c='Americas';
        update lstLead ; 
        
        lstLead[8].DealReg_Of_Registration_Products__c = 1;
        List<Lead> dealRegsWithProducts = new List<Lead>();
        dealRegsWithProducts.add(lstLead[8]);
        Database.Saveresult[] arrResult = Database.update( dealRegsWithProducts, false);
        
        newLeadMap.put(lstLead[0].id,lstLead[0]); 
        newLeadMap.put(lstLead[1].id,lstLead[1]);
        
        newDealRegMap.put(lstLead[5].id , lstLead[5]); 
        newDealRegMap.put(lstLead[6].id , lstLead[6]); 
        newDealRegMap.put(lstLead[7].id , lstLead[7]);
        newDealRegMap.put(lstLead[8].id , lstLead[8]); 
        newDealRegMap.put(lstLead[9].id , lstLead[9]); 
        newDealRegMap.put(lstLead[10].id , lstLead[10]); 
        
        
        PRM_DEALREG_ApprovalRouting objApprovalRouting = new PRM_DEALREG_ApprovalRouting();
        objApprovalRouting.createLeadShareForQueues(lstLead);   
        objApprovalRouting.autoPopulateExpirationDate(newLeadMap,oldLeadMap);
        objApprovalRouting.createLeadShareOnReAssignmentForQueues(newDealRegMap, oldDealRegMap);
        objApprovalRouting.populateDiffOfOriginalAppRejAndSub(lstLead);
        PRM_DEALREG_ApprovalRouting.addErrorOnRecords( arrResult, dealRegsWithProducts , leadWithProductsMap );
        PRM_DEALREG_ApprovalRouting.updateSLAFieldRepExpirationDateForDR();
        PRM_DEALREG_ApprovalRouting.updateSLAFieldRepExpirationDateForER();
        
        apexpages.currentpage().getparameters().put('ID', lstLead[11].id);
        ApexPages.currentPage().getParameters().put('DealReg' , 'true');
        apexpages.standardcontroller EX1 = new apexpages.standardcontroller(lstLead[11]);
        PRM_DEALREG_ApprovalRouting objEx1 = new PRM_DEALREG_ApprovalRouting(EX1); 
        objEx1.getlead();
        objEx1.SaveRejection();
        
        apexpages.currentpage().getparameters().put('ID', lstLead[5].id);
        ApexPages.currentPage().getParameters().put('DealReg' , 'true');
        apexpages.standardcontroller EX2 = new apexpages.standardcontroller(lstLead[5]);
        PRM_DEALREG_ApprovalRouting objEx2 = new PRM_DEALREG_ApprovalRouting(EX2); 
        objEx2.SaveRejection();
        
        apexpages.currentpage().getparameters().put('ID', lstLead[7].id);
        ApexPages.currentPage().getParameters().put('DealReg' , 'true');
        apexpages.standardcontroller EX3 = new apexpages.standardcontroller(lstLead[7]);
        PRM_DEALREG_ApprovalRouting objEx3 = new PRM_DEALREG_ApprovalRouting(EX3); 
        objEx3.SaveRejection();
        
        apexpages.currentpage().getparameters().put('ID', lstLead[0].id);
        ApexPages.currentPage().getParameters().put('DealReg' , 'true');
        apexpages.standardcontroller EX4 = new apexpages.standardcontroller(lstLead[0]);
        PRM_DEALREG_ApprovalRouting objEx4 = new PRM_DEALREG_ApprovalRouting(EX4); 
        objEx4.SaveRejection();
    } 
    
    static testmethod void createExtensionShareForAMERQueues()
    {
        //Creating custom setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.VisibilityProgramCSData();
            CustomSettingDataHelper.dealApprovalDateCheckCSData();
        }
        Map<String,DealRegistration_CustomSetting__c>  data =  DealRegistration_CustomSetting__c.getall();
        String AmerExtensionRqstQueue = data.get('AmerExtensionRqstQueue').PSC_QueueId__c;
        String ApjExtensionRqstQueue = data.get('ApjExtensionRqstQueue').PSC_QueueId__c;
        String EmeaExtensionRqstQueue = data.get('EmeaExtensionRqstQueue').PSC_QueueId__c;
        
        Map<String,DealRegistration_CustomSetting__c> extensionDateMap = DealRegistration_CustomSetting__c.getall();
        Double ExtensionDate = extensionDateMap.get('Extension Date of America/Emea').Length_of_Extensions__c;
        Double ExtensionDateNew = extensionDateMap.get('Extension Date of America/Emea/APJ').Length_of_Extensions__c;
        Double ExtensionDateForAPJ = extensionDateMap.get('Extension Date of APJ').Length_of_Extensions__c;
        
        date dtTodayDate;
        list<lead> lstLead = new list<lead>();
        lstLead.add(new lead ( LastName ='DealregLastName0', Company ='TestDealreg' , DealReg_Theater__c ='APJ',Email ='Dealreg1@dealreg.com'));
        lstLead.add(new lead ( LastName ='DealregLastName1', Company ='TestDealreg' , DealReg_Theater__c ='Americas',Email ='Dealreg1@dealreg.com'));
        lstLead.add(new lead ( LastName ='DealregLastName2', Company ='TestDealreg' , DealReg_Theater__c ='EMEA',Email ='Dealreg1@dealreg.com'));
        lstLead.add(new lead ( LastName ='DealregLastName3', Company ='TestDealreg' , DealReg_Theater__c ='EMEA',Email ='Dealreg1@dealreg.com' , DealReg_PSC_Approval_Rejection_Date_Time__c = system.now() - 2 , DealReg_of_Extension_Requests__c = 0 , DealReg_Do_Not_Allow_Extension_Request__c = false, DealReg_Expiration_Date__c = system.today() + 2 ));
        lstLead.add(new lead ( LastName ='DealregLastName4', Company ='TestDealreg' , DealReg_Theater__c ='APJ',Email ='Dealreg1@dealreg.com' , DealReg_PSC_Approval_Rejection_Date_Time__c = system.now() - 2 , DealReg_of_Extension_Requests__c = 0 , DealReg_Do_Not_Allow_Extension_Request__c = false ));
        
        insert lstLead;
        
        Set<Id> setLeadids = new Set<Id>();
        setLeadids.add(lstLead[0].id);
        setLeadids.add(lstLead[1].id);
        setLeadids.add(lstLead[2].id);
        
        List<Extension_Request__c> lstEtnReq = new List<Extension_Request__c> ();
        lstEtnReq.add(new Extension_Request__c(Extension_Request_Status__c='New', Expected_Close_Date__c = System.today()+4, DealReg_Extension_Justification__c ='test', Approval_Status__c = 'Submitted By Field Rep', Deal_Registration__c = lstLead[0].id));
        lstEtnReq.add(new Extension_Request__c(Extension_Request_Status__c='New', Expected_Close_Date__c = System.today()+4, DealReg_Extension_Justification__c ='test', Approval_Status__c = 'Submitted By Field Rep', Deal_Registration__c = lstLead[1].id));
        lstEtnReq.add(new Extension_Request__c(Extension_Request_Status__c='New', Expected_Close_Date__c = System.today()+4, DealReg_Extension_Justification__c ='test', Approval_Status__c = 'Submitted By Field Rep', Deal_Registration__c = lstLead[2].id));
           lstEtnReq.add(new Extension_Request__c(Extension_Request_Status__c='New', Expected_Close_Date__c = System.today()+4, DealReg_Extension_Justification__c ='test', Approval_Status__c = 'Submitted By Field Rep', Deal_Registration__c = lstLead[0].id));
        lstEtnReq.add(new Extension_Request__c(Extension_Request_Status__c='New', Expected_Close_Date__c = System.today()+4, DealReg_Extension_Justification__c ='test', Approval_Status__c = 'Submitted By Field Rep', Deal_Registration__c = lstLead[1].id));
        lstEtnReq.add(new Extension_Request__c(Extension_Request_Status__c='New', Expected_Close_Date__c = System.today()+4, DealReg_Extension_Justification__c ='test', Approval_Status__c = 'Submitted By Field Rep', Deal_Registration__c = lstLead[2].id));
        insert lstEtnReq ;
       
        List<Extension_Request__c> lstEtnExpirationReq = new List<Extension_Request__c> ();
        lstEtnExpirationReq.add(new Extension_Request__c(Extension_Request_Status__c='New', Expected_Close_Date__c = System.today()+4, DealReg_Extension_Justification__c ='test', Approval_Status__c = 'Submitted By Field Rep', Deal_Registration__c = lstLead[3].id));
        lstEtnExpirationReq.add(new Extension_Request__c(Extension_Request_Status__c='New', Expected_Close_Date__c = System.today()+4, DealReg_Extension_Justification__c ='test', Approval_Status__c = 'Submitted By Field Rep', Deal_Registration__c = lstLead[4].id));
        lstEtnExpirationReq.add(new Extension_Request__c(Extension_Request_Status__c='New', Expected_Close_Date__c = System.today()+4, DealReg_Extension_Justification__c ='test', Approval_Status__c = 'Submitted By Field Rep', Deal_Registration__c = lstLead[3].id));
        lstEtnExpirationReq.add(new Extension_Request__c(Extension_Request_Status__c='New', Expected_Close_Date__c = System.today()+4, DealReg_Extension_Justification__c ='test', Approval_Status__c = 'Submitted By Field Rep', Deal_Registration__c = lstLead[3].id));
        insert lstEtnExpirationReq ;
        
        lstEtnExpirationReq[0].Extension_Request_Status__c='Approved';
        lstEtnExpirationReq[1].Extension_Request_Status__c='Approved';
        lstEtnExpirationReq[2].Extension_Request_Status__c='Submitted';
        lstEtnExpirationReq[3].Extension_Request_Status__c='Submitted';
        update lstEtnExpirationReq;
        
        Set<Id> setExtnId = new Set<Id>();
        setExtnId.add(lstEtnExpirationReq[2].id);
        setExtnId.add(lstEtnExpirationReq[3].id);
        
        //List<ProcessInstanceWorkitem> lstPIWI = new List<ProcessInstanceWorkitem>();
        //lstPIWI.add();
        
        Map<Id,Extension_Request__c> mapEtnReqOLD = new map<Id,Extension_Request__c>();
        Map<Id,Extension_Request__c> mapEtnReqNEW = new map<Id,Extension_Request__c>();
            
            for (Extension_Request__c dealreg: lstEtnReq )
            {
                mapEtnReqOLD.put(dealreg.Id, dealreg) ;
                dealreg.Extension_Request_Status__c ='Submitted';
            }
            update lstEtnReq ;
            
            for ( Extension_Request__c dealreg: lstEtnReq )
                {
                    mapEtnReqNEW.put(dealreg.Id, dealreg) ;
                }
            
        Test.startTest();                    

            PRM_DEALREG_ApprovalRouting objApprovalRouting = new PRM_DEALREG_ApprovalRouting(); 
            objApprovalRouting.createExtensionShareForQueues(mapEtnReqNEW , mapEtnReqOLD); 
            
            date expiration = date.valueOf('2010-02-02');
            dtTodayDate = expiration ;
            //dtTodayDate = dtTodayDate.addMonths(1);
            PRM_DEALREG_ApprovalRouting.calculateQuaterDate(1,expiration);
            
            dtTodayDate = dtTodayDate.addMonths(4);
            PRM_DEALREG_ApprovalRouting.calculateQuaterDate(4,expiration);
            
            date expirationNull = null ;
            PRM_DEALREG_ApprovalRouting.calculateQuaterDate(5,expirationNull);
            
            dtTodayDate = dtTodayDate.addMonths(9);
            PRM_DEALREG_ApprovalRouting.calculateQuaterDate(9,expiration);
            
            PRM_DEALREG_ApprovalRouting.extendExpirationDate (lstEtnExpirationReq);
            
            //PRM_DEALREG_ApprovalRouting.autoApproveExtensionForAPjEMEA(setExtnId);
            
            PRM_DEALREG_ApprovalRouting.calculateNOOfExtensions(lstEtnExpirationReq) ; 
            
            PRM_DEALREG_ApprovalRouting.updateApprovedDeal(setLeadids);
            
            //PRM_DEALREG_ApprovalRouting.getRelatedAccountMembersForUsers(lstLead);
            //PRM_DEALREG_ApprovalRouting.getRelatedAccountMembersForExtensionForUsers(lstEtnReq);        
        Test.stopTest();
        
    }
    
         static testmethod void testApprovalProcess2()
        {
            //Creating custom setting data
            System.runAs(new user(Id = UserInfo.getUserId()))
            {
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
                CustomSettingDataHelper.countryTheaterMapCSData();
                CustomSettingDataHelper.dealRegistrationCSData();
                CustomSettingDataHelper.VisibilityProgramCSData();
                CustomSettingDataHelper.dealApprovalDateCheckCSData();
            }
              //PRM_VPP_JobDataHelper.createVPPCustomSettingData();      
              Profile profile2 = [SELECT Id FROM profile WHERE name='APJ PSC User']; 
              User usr2   = [SELECT Id from User where ProfileId=:profile2.id and IsActive=True limit 1];
              
             
              Lead apjDR = createLead('APJ',usr2.id);
              
              //Lead emeaDR = createLead('APJ',usr2.id);
              Set<Id> setExtnId = new Set<Id>();
              Set<Id> setDRId = new Set<Id>();       
              Extension_Request__c extensionReq = createExtension(apjDR.id);
              //Extension_Request__c extensionReq2 = createExtension(emeaDR.id);
              setExtnId.add(extensionReq.id);
              
                Approval.ProcessSubmitRequest erRequest = new Approval.ProcessSubmitRequest();
                erRequest.setComments('Submitting request for approval.');
                erRequest.setObjectId(extensionReq.id);
                Approval.ProcessResult extResult = Approval.process(erRequest);      
              
              Test.startTest();
              PRM_DEALREG_ApprovalRouting.autoApproveExtensionForAPjEMEA(setExtnId);
              //PRM_DEALREG_ApprovalRouting.autoApproveFirstStepDRForEMEA(setDRId);
              Test.stopTest();
      
        }
        

        static Lead createLead(String Theater,String UserId){
            Map<String,Schema.RecordTypeInfo> recordTypes = Lead.sObjectType.getDescribe().getRecordTypeInfosByName();
            Id drRecordTypeId = recordTypes.get('Deal Registration').getRecordTypeId();
            Lead newLead = new lead();
            newLead.lastname = 'Test Lead';
            newLead.company = 'EMC';
            newLead.DealReg_PSC_Owner__c = UserId;
            newLead.DealReg_Deal_Registration_Status__c = 'Submitted';
            newLead.DealReg_Of_Registration_Products__c = 3;
            newLead.DealReg_Deal_Description__c = 'test';
            newLead.DealReg_Deal_Registration__c = true;
            newLead.DealReg_Deal_Registration_Justification__c = 'test';
            newLead.DealReg_Department_Project_Name__c = 'ABCL';
            newLead.DealReg_Expected_Deal_Value__c = 12345.5;
            newLead.Email = 'abc@abc.com';
            newLead.DealReg_Partner_Contact_First_Name__c = 'Test';
            newLead.DealReg_Partner_Contact_Last_Name__c = 'Test'; 
            newLead.DealReg_Theater__c = Theater;
            newLead.recordtypeid = drRecordTypeId;
            newLead.DealReg_Create_New_Opportunity__c = True;
            newLead.City ='India';
            newLead.Street ='Bangalore';
            newLead.Channel__c = 'INDIRECT';
            newLead.DealReg_Deal_Registration__c = true;
            newLead.DealReg_of_Extension_Requests__c = 0;
            newLead.phone = '3455677'; 
            newLead.DealReg_PSC_Approval_Rejection_Date_Time__c =System.today().addMonths(1); //DateTime.valueOf('2007-01-01 2:35:21')
            insert newLead;
            system.debug('new lead--->'+newlead);
            return newLead;
        }
    
        static Extension_Request__c createExtension(String leadId){
          Extension_Request__c objExtn = new Extension_Request__c();
            objExtn.Extension_Request_Status__c='Submitted';
            objExtn.Expected_Close_Date__c = System.today().addMonths(1);
            objExtn.DealReg_Extension_Justification__c ='test' ;
            objExtn.Deal_Registration__c = leadId;
            objExtn.DealReg_Field_SLA_Expire_on__c = DateTime.Now().addMinutes(2);        
            objExtn.Approval_Status__c = 'Submitted By Field Rep';
            insert objExtn;
            return objExtn;
        }
     
}