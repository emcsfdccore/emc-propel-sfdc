/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE       DEVELOPER            WR            DESCRIPTION                               
 |  ====       =========        ==========        =========== 
 |  28-July-14  Bhanu Prakash     PROPEL          Initial creation - For PROPEL FD QOT.CTQ.FD.I.038
 |  30-Sept-14  Bhanu Prakash     PROPEL          Revised query preparation logic and added OpptyWrapper implementation
 |  22-Dec-14   Bhanu Prakash     PROPEL          Fixed Order by name issue
 |  27-Mar-15   Bhanu Prakash     PROPEL          Fixed Defect ID : 19089 by replacing 'party_number__c' with 'UCID__c' field
 ===========================================================================+*/

public class Oppty_SearchHelper_IntegrationOperation {
    public static Oppty_Search_IntegrationOperation.Oppty_SearchData searchData = null;
    public static Oppty_Search_IntegrationOperation.Oppty_SearchResponse staticSearchResponse = null;
    public static String exceptionDescription = null;
    static Boolean isSOSL = false;
    //Method to perform all search functionalities 
    public Oppty_Search_IntegrationOperation.Oppty_SearchResponse performOpptyHelperSearch(Oppty_Search_IntegrationOperation.Oppty_SearchData sData){
       searchData = sData;
        String query= null;
        staticSearchResponse = new Oppty_Search_IntegrationOperation.Oppty_SearchResponse();
        try{
            query = prepareQuery(searchData);//Prepare & get query based on criteria
            staticSearchResponse = performSearch(query, searchData);//Perform Oppty search and do salesRep access validation then get list of Opptys
        }catch(Exception e){
            if(exceptionDescription!=null){
                staticSearchResponse.errorMessage = exceptionDescription;
            }else {
                staticSearchResponse.errorMessage = 'Exception : ' + e.getMessage();
            }
            staticSearchResponse.isSuccess = false;
        }
        System.debug('**** final response = '+ staticSearchResponse);
        return staticSearchResponse;
    }
    
    //Method to construct SOQL query based of search criteria.
    private String prepareQuery(Oppty_Search_IntegrationOperation.Oppty_SearchData searcData){
        System.debug('**** in query Method');
        String query;
        String queryWHERE= 'WHERE';
        String fullQuary;
         //Prepare basic query
        query = 'SELECT  Name, Opportunity_Number__c,StageName,Amount,CurrencyIsoCode,Sales_Channel__c,CloseDate,Account_Name1__c,Tier_2_Partner__r.Name FROM Opportunity ';
        
        //if distributionChannel /primarySalesRep null/empty raise an exception as they are mandatory fields.
        if((searcData.distributionChannel ==null || searcData.distributionChannel =='') || (searcData.primarySalesRep  ==null || searcData.primarySalesRep  =='')){
            //staticSearchResponse = new Oppty_Search_IntegrationOperation.Oppty_SearchResponse();
            staticSearchResponse.isSuccess=false;
            
            if(searcData.distributionChannel ==null || searcData.distributionChannel =='')//distributionChannel is mandatory
                exceptionDescription = 'Mandatory field Distribution Channel not found';
                
            if(searcData.primarySalesRep  ==null || searcData.primarySalesRep  =='')//primarySalesRep is mandatory
                exceptionDescription = 'Mandatory field Sales Rep not found';
            
            throw new SearchException('############### Exception : One or more mandatory field values not found...');
        }else if(searcData.opptyNo==null || searcData.opptyNo==''){//If opptyNo is provided, then soldToPartyNo or endCustPartyNo is no longer mandatory
            System.debug('*** opptyNo not found ');
            staticSearchResponse.isSuccess=false;
            //If soldToPartyNo /endCustPartyNo is null/empty raise an exception as they are mandatory fields
                if((searcData.soldToPartyNo ==null || searcData.soldToPartyNo=='') || (searcData.endCustPartyNo  ==null || searcData.endCustPartyNo  =='')){
                    if(searcData.soldToPartyNo ==null || searcData.soldToPartyNo=='')//soldToPartyNo is mandatory if opptyNo is not provided
                        exceptionDescription = 'Mandatory field Sold To Party Number not found';
                    
                    if(searcData.endCustPartyNo  ==null || searcData.endCustPartyNo  =='')//endCustPartyNo is mandatory if opptyNo is not provided
                        exceptionDescription = 'Mandatory field End Customer Party Number not found';
                    throw new SearchException('############### Exception : One or more mandatory field values not found...');
                }
            }
        System.debug('*** After Exceptions');
         String searchSOSLQuery;
         
        //Create Custom setting to check is distribution channel is 'Indirect'
        Propel_General_Settings__c propelSettings = Propel_General_Settings__c.getInstance();
        Set<String> setValues = new Set<String>(propelSettings.Indirect_Distribution_Channels__c.split(';'));
        
        //Add critieria for Oppoprtunity Number
       if(searcData.opptyNo!=null && searcData.opptyNo!=''){
            if(!queryWHERE.endsWith('WHERE')) 
                    queryWHERE += ' AND';
                queryWHERE+=' Opportunity_Number__c=\'' + String.escapeSingleQuotes(searcData.opptyNo) +'\'';
                System.debug('*** added Opportunity_Number__c to query ' + searcData.opptyNo);
                
             //Bhanu : Code to fix defect : 20435 - START
            if (setValues==null || setValues.isEmpty()){
                exceptionDescription ='Please difine CutomSetting for distribution channel';
                System.debug('#### Exception on getting CutomSetting for distribution channel ');
                throw new SearchException('#### Exception on getting CutomSetting for distribution channel ');
            }else {//Add soldToPartyNo criteria to query
                System.debug('****1111 setValues.contains(searcData.distributionChannel) = ' + setValues.contains(searcData.distributionChannel));
                if(searcData.distributionChannel !=null && setValues.contains(searcData.distributionChannel)){
                System.debug('**** in side distributionChannel not null');
                    if(searcData.soldToPartyNo!= null && searcData.soldToPartyNo != ''){
                    System.debug('**** soldToPartyNo is not empty ');
                        if(!queryWHERE.endsWith('WHERE'))
                        queryWHERE += ' AND';
                        queryWHERE +=' (partner__r.UCID__c =\'' + String.escapeSingleQuotes(searcData.soldToPartyNo) + '\'' + ' OR partner__r.Partner_Quoting_Account_Name__r.UCID__c =\'' + String.escapeSingleQuotes(searcData.soldToPartyNo) + '\')';
                    }          
                }
            }
            //Bhanu : Code to fix defect : 20435 - END
        } else {
            //Add Oppty name wild-card search criteria
            if(searcData.opptyName!=null && searcData.opptyName!='') {
                if(!queryWHERE.endsWith('WHERE'))
                        queryWHERE += ' AND';
                        isSOSL = true;
                        System.debug('**** in opp name condition');
                         searchSOSLQuery = 'FIND \''+ String.escapeSingleQuotes(searcData.opptyName) +'*\' IN ALL FIELDS RETURNING Opportunity(Name, Opportunity_Number__c,StageName,Amount,CurrencyIsoCode,Sales_Channel__c,CloseDate,Account_Name1__c,Tier_2_Partner__r.Name '; 
                //String likeName =  searcData.opptyName ;
               //queryWHERE +=' name like \'%' + String.escapeSingleQuotes(searcData.opptyName) + '%\'';
               System.debug('*** adding name to query');
            }
            //The logic for end customer should only apply if opportunity number is null/empty string
            //query Account based on endCustPartyNo and fetch 'District_Lookup__c' and 'Account_District__c' - START
            List<Account> endCustomer= null;
            endCustomer = [SELECT District_Lookup__c, Account_District__c FROM Account WHERE UCID__c=:searcData.endCustPartyNo];
            
            if(endCustomer ==null || endCustomer.isEmpty()){
                exceptionDescription = 'Mandatory field End Customer not found';
                throw new SearchException('############### Exception : Mandatory field End Customer not found...');
            } else {
            //System.debug('***searcData.endCustPartyNo =  '+ searcData.endCustPartyNo + ' : endCustomer size = ' + endCustomer.size());
                //System.debug('*** endCustomer District_Lookup__c = ' + endCustomer[0].District_Lookup__c   +'  %%%%%% Account_District__c = ' + endCustomer[0].Account_District__c);
                if(endCustomer[0].District_Lookup__c != null && endCustomer[0].Account_District__c != null){
                    if(!queryWHERE.endsWith('WHERE'))
                            queryWHERE += ' AND';
                    queryWHERE +=' account.District_Lookup__c=\'' + String.escapeSingleQuotes(endCustomer[0].District_Lookup__c) +'\'';
                    queryWHERE +=' AND account.Account_District__c=\'' + String.escapeSingleQuotes(endCustomer[0].Account_District__c) +'\'';
                    System.debug('*** added account.District_Lookup__c to query ' + endCustomer[0].District_Lookup__c);
                } else {//Added code as per defect 17005 on Cycle0 testing
                    if(!queryWHERE.endsWith('WHERE'))
                                queryWHERE += ' AND';
                    queryWHERE +=' account.UCID__c =\'' + String.escapeSingleQuotes(searcData.endCustPartyNo) +'\'';
                    System.debug('*** account.UCID__c ');
                }
                /*if(endCustomer[0].Account_District__c != null){// Code decommission as per defect 17005 on Cycle0 testing
                    if(!query.endsWith('WHERE')) 
                        query += ' AND';
                    query+=' account.Account_District__c=\'' + String.escapeSingleQuotes(endCustomer[0].Account_District__c) +'\'';
                    System.debug('*** added account.Account_District__c to query');
                }*/
            }
            //query Account based on endCustPartyNo and fetch 'District_Lookup__c' and 'Account_District__c' - END
            
             //NEW CODE
             //Get CustomSetting to check distributionChannel value(Distributor, Tier 1 Reseller or Service Provider)
            if (setValues==null || setValues.isEmpty()){
                exceptionDescription ='Please difine CutomSetting for distribution channel';
                System.debug('#### Exception on getting CutomSetting for distribution channel ');
                throw new SearchException('#### Exception on getting CutomSetting for distribution channel ');
            }
            System.debug('**** setValues.contains(searcData.distributionChannel) = ' + setValues.contains(searcData.distributionChannel));
            if(searcData.distributionChannel !=null && setValues.contains(searcData.distributionChannel)){
            System.debug('**** in side distributionChannel not null');
                if(searcData.soldToPartyNo!= null && searcData.soldToPartyNo != ''){
                System.debug('**** soldToPartyNo is not empty ');
                    if(!queryWHERE.endsWith('WHERE'))
                    queryWHERE += ' AND';
                    queryWHERE +=' (partner__r.UCID__c =\'' + String.escapeSingleQuotes(searcData.soldToPartyNo) + '\'' + ' OR partner__r.Partner_Quoting_Account_Name__r.UCID__c =\'' + String.escapeSingleQuotes(searcData.soldToPartyNo) + '\')';//??? PENDING : EXCEPTION ON RELATION
                    System.debug('*** added Sales_Channel__c to query ' + searcData.distributionChannel);
                }
                if(searcData.tier2PartyNo != null && searcData.tier2PartyNo != ''){
                    if(!queryWHERE.endsWith('WHERE'))
                    queryWHERE += ' AND';
                    queryWHERE +=' Tier_2_Partner__r.UCID__c=\'' + String.escapeSingleQuotes(searcData.tier2PartyNo) + '\'';
                }           
            }
        }//else - end
       
        //Add Oppty Forecast Status filter
        if(!queryWHERE.endsWith('WHERE')) 
                    queryWHERE += ' AND';
        queryWHERE += ' (StageName IN(\'Pipeline\',\'Strong Upside\', \'Upside\',\'Won\', \'Commit\'))';
        
        //Add ORDER BY clouse to query
        queryWHERE += ' ORDER BY name';
        
        //add limit to query to 200
        queryWHERE +=' LIMIT 100';
        
        
        if(isSOSL){
            fullQuary = searchSOSLQuery + queryWHERE + ')';
        } else{
            fullQuary = query + queryWHERE;
        }
        System.debug('*** before returning fullQuary = ' + fullQuary);
        return fullQuary;
    }
    //Method to query the opportunities and filter according to sales rep access
    private Oppty_Search_IntegrationOperation.Oppty_SearchResponse performSearch(String query, Oppty_Search_IntegrationOperation.Oppty_SearchData searData){
       System.debug('*** start performSearch method');
        Oppty_Search_IntegrationOperation.Oppty_SearchResponse searchResponse1 = new Oppty_Search_IntegrationOperation.Oppty_SearchResponse();
        //staticSearchResponse = new Oppty_Search_IntegrationOperation.Oppty_SearchResponse();
        List<Opportunity> lisOppty = null;
        List<Opportunity> lisResultOpptys = null;
     //try{
        try{
            if(isSOSL){
                    List<List<SObject>> objects = search.query(query);
                    lisOppty = (List<Opportunity>)objects[0];
                    System.debug('**** objects = ' + objects);
                    System.debug('***** SOSL returned Opptyels = ' + lisOppty);
            } else {
                 lisOppty = Database.query(query);
            }           
            System.debug('**** Qeury execution success lisOppty = ' + lisOppty);
        }catch(Exception e){
            exceptionDescription = 'Exception on searching for Opportunity';
            System.debug('############ Exception : on querying : ' + e.getMessage());
            throw new SearchException('Exception on searching for Opportunity');
        }
        //if no opportunities are found set isSuccess false and return
        if(lisOppty.isEmpty()){
            exceptionDescription = 'No matching Opportunities found for this Search Criteria';
            System.debug('**** Query not returned any record');
            throw new SearchException('No matching Opportunities found for this Search Criteria');
        } else {//if found some opptys
            System.debug('**** found records size = ' + lisOppty.size());
            System.debug('**** found lisOppty = ' + lisOppty);
            
            //Filter Oppty records according to sales rep access
            lisResultOpptys = filterSalesRepOpptys(lisOppty,searData);
            System.debug('*** alfer filter lisResultOpptys.size() = ' + lisResultOpptys.size());
        }
        
        if(lisResultOpptys== null || lisResultOpptys.isEmpty()){
            exceptionDescription = 'No matching Opportunities found with this Sales Rep access for this Search Criteria';
            throw new SearchException('No matching Opportunities found with this Sales Rep access for this Search Criteria');
        } 
        List<Oppty_Search_IntegrationOperation.OpptyWrapper> lstOpptyWraper = new List<Oppty_Search_IntegrationOperation.OpptyWrapper>();
        //Create OpptyWrapper object from final result of list of Opptys to send back
        if(lisResultOpptys != null && !lisResultOpptys.isEmpty()){
            Oppty_Search_IntegrationOperation.OpptyWrapper oppWrap = null;
            for(Opportunity oppty : lisResultOpptys){
                oppWrap = new Oppty_Search_IntegrationOperation.OpptyWrapper();
                oppWrap.name = oppty.Name;
                oppWrap.opptyNumber = oppty.Opportunity_Number__c;
                oppWrap.stageName = oppty.StageName;
                oppWrap.amount = oppty.Amount; 
                oppWrap.currencyIsoCode = oppty.CurrencyIsoCode;
                oppWrap.distributionChannel = oppty.Sales_Channel__c;
                oppWrap.closeDate = oppty.CloseDate;
                oppWrap.accountName = oppty.Account_Name1__c;
                oppWrap.tier2Partner = oppty.Tier_2_Partner__r.Name;
                lstOpptyWraper.add(oppWrap);
            }   
        }
        //? FIX : IF INPUT VALUES ARE INVALIED RAISING "Exception in Main class Attempt to de-reference a null object" EXCEPTION
        if (lstOpptyWraper != null && !lstOpptyWraper.isEmpty()){
            searchResponse1.isSuccess=true; 
            searchResponse1.lstOppWrapResults = lstOpptyWraper;
            System.debug('*** returning response searchResponse1 = ' + searchResponse1);
            System.debug('*** searchResponse1.lstOppWrapResults = ' + searchResponse1.lstOppWrapResults);
        } else {
            System.debug('**** lstOpptyWraper is null ');
        }
    /*}catch(Exception e){
            exceptionDescription ='Un expected exception ' +  e.getMessage();
            System.debug('###### Exception on Creating Query string');
            throw e;
        }*/
        return searchResponse1;
    }
    //Method to filter opportunities based on SalesRep access
    private List<Opportunity> filterSalesRepOpptys(List<Opportunity> lstOptyToFilter,Oppty_Search_IntegrationOperation.Oppty_SearchData seaData){
        Map<Id,Opportunity> mapOfOptys = new Map<Id,Opportunity>(lstOptyToFilter);
        
        Set<Id> setUnSortedOpptysIds = new Set<Id>();
        List<Opportunity> lstfilteredOpptys = new List<Opportunity>();
        List<Opportunity> limitedOpptys = new List<Opportunity>();
        //List<Opportunity> filteredOpptys = new List<Opportunity>();
        try{
            Id salesRepId = seaData.primarySalesRep;
        }catch(Exception e){
            exceptionDescription = 'Exception : Invalied Sales Rep. ID formate';
            throw new SearchException('Exception : Invalied Sales Rep. ID formate');
        }
        Id uid = seaData.primarySalesRep;
        
        Map<Id,UserRecordAccess> mapURA = new Map<Id,UserRecordAccess>([SELECT RecordId FROM UserRecordAccess WHERE HasReadAccess = true AND UserId =:uid AND RecordId=:mapOfOptys.keyset()]);
        
        Integer count= 0;
        
        for(UserRecordAccess ura : mapURA.values()){
            setUnSortedOpptysIds.add(ura.RecordId);
        }
        
         //Sort the Opportunities on order
        if(mapURA!=null && mapURA.size()>0){
                for(Opportunity opp : lstOptyToFilter){
                    if(setUnSortedOpptysIds.contains(opp.Id)){
                        lstfilteredOpptys.add(opp);
                        System.debug('*** Adding to lstfilteredOpptys mapOfOptys.get(oppId).Name = ' + opp.Name);
                        count++;
                    if(count==50){
                        break;
                    }
                    }
                }
        }
        //return all list of opportunities
        System.debug('*** Sending all Opptyes lstfilteredOpptys.values() = ' + lstfilteredOpptys);
        return lstfilteredOpptys;
    }
    
    public class SearchException extends Exception {}
}