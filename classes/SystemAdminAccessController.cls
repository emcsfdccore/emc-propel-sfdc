/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req                  DESCRIPTION                               
 |  ====            =========       ======                  =========== 
 |  11-02-2013      Anand Sharma	System Administration   This class is used as a Utility class for System Admin App                     
 | 
 +=========================================================================================================================*/

public class SystemAdminAccessController {
	
	public System_Admin_Request_Logs__c record { get; set; }
	public Boolean isSuccess{get;set;}
	public Boolean isGuest{get;set;}
	public String strReason{get; set;}
	public String strEmailId{get; set;}
	public String strReturnUrl{get; set;}

	public SystemAdminAccessController(){
		isSuccess = false;
		isGuest = true;
		strReturnUrl='';
		try{
					
			record = new System_Admin_Request_Logs__c(); 
			strReason ='';
			strEmailId= '';
			if(Userinfo.getUserType() =='Guest'){
				strEmailId = ApexPages.currentPage().getParameters().get('un');	
				if(strEmailId != null && strEmailId !=''){
					isGuest = false;
				}
				strReturnUrl = 	ApexPages.currentPage().getParameters().get('retUrl');				
			}
		}Catch(Exception ex){
			isSuccess = false;
		}
		
    }    
    /* @Method <This Method is used to redirect on user record.>.
        @param <It is not taking any parameters>
        @return <Pagereference>
        @throws exception - <No Exception>
    */
    public PageReference Cancel(){
    	//String strReturnUrl = URL.getSalesforceBaseUrl().toExternalForm() +'/' + strReturnUrl;
    	if(strReturnUrl !=''){
    		strReturnUrl = strReturnUrl + '?noredirect=1';
        	PageReference objPgReturn = new PageReference(strReturnUrl);
        	objPgReturn.setRedirect(true);
        	return objPgReturn;
    	}else{
    		return null;
    	}
    }
    
    public PageReference provideSysAdminAccessAsGuestUser(){
    	
    	Set<Id> setRemoveSysAdminUserIds = new Set<Id>();
		Set<Id> setUpdateSysAdminReqLogIds = new Set<Id>();
		boolean flagForAllUserRemoveAccess = false;
		Id userId ;
    	try{
	    	if((strEmailId ==null || strEmailId =='') &&(strReason ==null || strReason =='')){
    			ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.Error, System.Label.SystemAdmin_UserName_Required);
                ApexPages.addMessage(errorMessage);
                ApexPages.Message errorMessage1 = new ApexPages.Message(ApexPages.Severity.Error, System.Label.SystemAdmin_Reason_Required);
                ApexPages.addMessage(errorMessage1);
                return null;
    		}
    		if(strEmailId ==null || strEmailId ==''){
    			ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.Error, System.Label.SystemAdmin_UserName_Required);
                ApexPages.addMessage(errorMessage);
                return null;
    		}
    		if(strReason ==null || strReason ==''){
    			ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.Error, System.Label.SystemAdmin_Reason_Required);
                ApexPages.addMessage(errorMessage);
                return null;
    		}
    		List<System_Admin_Users__c> lstAdminUser = [Select Id,User_Name__c,User_Name__r.Profileid,User_Name__r.Email , End_Date_Time__c from System_Admin_Users__c where User_Name__r.UserName =: strEmailId and Active__c = true];
    		if(lstAdminUser != null && lstAdminUser.size() >0){
    			System_Admin_Users__c objAdminUser = lstAdminUser.get(0);
    			record.Email__c = objAdminUser.User_Name__r.Email ;
    			record.UserId__c =objAdminUser.User_Name__c;
    			record.Previous_Profile__c = objAdminUser.User_Name__r.Profileid ; 
    			String strValidationMessage = SystemAdministration_Utility.ValidateSystemAdminRequest(objAdminUser.User_Name__c);
    			System.debug('strValidationMessage--->' + strValidationMessage);
    			userId = objAdminUser.User_Name__c;
    			if(strValidationMessage !='No'){
    				ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.Error, strValidationMessage);
                	ApexPages.addMessage(errorMessage);
                	return null;
    			}	    				
    		} else{
    			ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.Error, System.Label.SystemAdmin_NoActiveUser);
                ApexPages.addMessage(errorMessage);
                return null;
    		}   		
	    	
	    	record.Reason__c = strReason;  
	    	insert record;	
	    	
	    	if(isGuest && userId != null){	    		
				SystemAdministration_Utility.grantSysAdminAccess(new set<Id>{userId}); 
				SystemAdministration_Utility.updateSystemAdminUserAndReqLog(new set<Id>{userId});    			
	    	}    	
	    	isSuccess = true;
    	}catch(Exception ex){
    		isSuccess = false;   		
    		return null;
    	}    
        // View the record
        if(isGuest){
        	ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.Info, System.Label.SystemAdminSuccessMessage);
	        ApexPages.addMessage(errorMessage);
	        return null;
        }else{
        	PageReference objPgReturn = new PageReference(strReturnUrl + '?noredirect=1');
        	objPgReturn.setRedirect(true);
        	return objPgReturn;
        }
    }
}