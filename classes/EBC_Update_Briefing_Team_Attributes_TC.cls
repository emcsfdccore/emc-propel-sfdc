/*
Author:     Devi Prasad Bal
Created on: 01-April-2010
Description: This class contains test method for EBC_Update_Briefing_Team_Attributes.cls
Modified by: Emmanuel Cruz
Modified on: August 22th, 2013
Reason: Coverage class code

Modified by :   Abinaya M S 
Modified on :   05-Jun-2014
Reason:     :   Assigned 'Internal meeting' value to Briefing_Type__c field to satisfy validation rule.

*/

@isTest(SeeAllData = true)
private class EBC_Update_Briefing_Team_Attributes_TC {
    
    static testMethod void validateBriefingAttributes1() {

    String emcInternalRT = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND Name = 'EMC Internal Contact' LIMIT 1].Id; 
    Contact ContBA = new Contact(RecordTypeId = emcInternalRT, LastName = 'BATester', AccountId = '0017000000OmdVa', Email = 'batester@xyz.com');
    Contact ContR = new Contact(RecordTypeId = emcInternalRT, LastName = 'RTester', AccountId = '0017000000OmdVa', Email = 'rtester@xyz.com');
    Contact ContS = new Contact(RecordTypeId = emcInternalRT, LastName = 'STester', AccountId = '0017000000OmdVa', Email = 'stester@xyz.com');

    EBC_Briefing_Event__c beRecord = new EBC_Briefing_Event__c(Name = 'testName', Briefing_Status__c = 'Open',Briefing_Type__c = 'Internal meeting', Requestor_Name__c = ContR.Id );
    insert beRecord;
    
    ApexPages.currentPage().getParameters().put('Id',beRecord.Id);
    
    EBC_Invitees__c inviteeBA1 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Briefing Advisor');
    insert inviteeBA1;
    EBC_Invitees__c inviteeBA2 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Briefing Advisor');
    insert inviteeBA2;
    EBC_Invitees__c inviteeLA1 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Logistic Advisor');
    insert inviteeLA1;
    EBC_Invitees__c inviteeLA2 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Logistic Advisor');
    insert inviteeLA2;
    EBC_Invitees__c inviteeR1 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Requestor');
    insert inviteeR1;
    EBC_Invitees__c inviteeR2 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Requestor');
    insert inviteeR2;
    EBC_Invitees__c inviteeS1 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Scheduler');
    insert inviteeS1;
    EBC_Invitees__c inviteeS2 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Scheduler');
    insert inviteeS2;

    EBC_Update_Briefing_Team_Attributes updAtt = new EBC_Update_Briefing_Team_Attributes();
    //updAtt.briefingEventId= beRecord.Id;
    updAtt.retrieveInvitees();
    updAtt.updateAttributes();
    updAtt.cancel();
    }
    
    static testMethod void validateBriefingAttributes2() {
    
    String emcInternalRT = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND Name = 'EMC Internal Contact' LIMIT 1].Id; 
    Contact ContBA = new Contact(RecordTypeId = emcInternalRT, LastName = 'BATester', AccountId = '0017000000OmdVa', Email = 'batester@xyz.com');
    Contact ContR = new Contact(RecordTypeId = emcInternalRT, LastName = 'RTester', AccountId = '0017000000OmdVa', Email = 'rtester@xyz.com');
    Contact ContS = new Contact(RecordTypeId = emcInternalRT, LastName = 'STester', AccountId = '0017000000OmdVa', Email = 'stester@xyz.com');

    EBC_Briefing_Event__c beRecord = new EBC_Briefing_Event__c(Name = 'testName', Briefing_Status__c = 'Open',Briefing_Type__c = 'Internal meeting', Requestor_Name__c = ContR.Id );
    insert beRecord;
    
    ApexPages.currentPage().getParameters().put('Id',beRecord.Id);
    
    EBC_Invitees__c inviteeBA = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='BATester', Attendee_Email__c='batester@xyz.com', Briefing_Team_Role__c='Briefing Advisor');
    insert inviteeBA;
    EBC_Invitees__c inviteeR = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='RTester', Attendee_Email__c='rtester@xyz.com', Briefing_Team_Role__c='Requestor');
    insert inviteeR;
    EBC_Invitees__c inviteeS = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='STester', Attendee_Email__c='stester@xyz.com', Briefing_Team_Role__c='Scheduler');
    insert inviteeS;

    EBC_Update_Briefing_Team_Attributes updAtt2 = new EBC_Update_Briefing_Team_Attributes();
    updAtt2.retrieveInvitees();
    updAtt2.updateAttributes();
    updAtt2.cancel();
    
    }
    
    static testMethod void validateBriefingAttributes3() {
    
    String emcInternalRT = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND Name = 'EMC Internal Contact' LIMIT 1].Id; 
    Contact ContBA = new Contact(RecordTypeId = emcInternalRT, LastName = 'BATester', AccountId = '0017000000OmdVa', Email = 'batester@xyz.com');
    Contact ContR = new Contact(RecordTypeId = emcInternalRT, LastName = 'RTester', AccountId = '0017000000OmdVa', Email = 'rtester@xyz.com');
    Contact ContS = new Contact(RecordTypeId = emcInternalRT, LastName = 'STester', AccountId = '0017000000OmdVa', Email = 'stester@xyz.com');

    EBC_Briefing_Event__c beRecord = new EBC_Briefing_Event__c(Name = 'testName', Briefing_Status__c = 'Open',Briefing_Type__c = 'Internal meeting', Requestor_Name__c = ContR.Id );
    insert beRecord;
    
    ApexPages.currentPage().getParameters().put('Id',beRecord.Id);
    
    EBC_Invitees__c invitee1 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Account Team Member');
    insert invitee1;
    EBC_Invitees__c invitee2 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Customer');
    insert invitee2;
    EBC_Invitees__c invitee3 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='EMC Employee');
    insert invitee3;
    EBC_Invitees__c invitee4 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Other Invitee');
    insert invitee4;
    EBC_Invitees__c invitee5 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Partner');
    insert invitee5;
    EBC_Invitees__c invitee6 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Presenter (EMC Employee)');
    insert invitee6;
    EBC_Invitees__c invitee7 = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='test', Attendee_Email__c='abc.def@xyz.com', Briefing_Team_Role__c='Presenter (Non EMC Employee)');
    insert invitee7;

    EBC_Update_Briefing_Team_Attributes updAtt3 = new EBC_Update_Briefing_Team_Attributes();
    updAtt3.retrieveInvitees();
    updAtt3.updateAttributes();
    updAtt3.cancel();
    
    }
    
    static testMethod void validateBriefingAttributes4() {
    
    String emcInternalRT = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND Name = 'EMC Internal Contact' LIMIT 1].Id; 
    Contact ContBA = new Contact(RecordTypeId = emcInternalRT, LastName = 'BATester', AccountId = '0017000000OmdVa', Email = 'batester@xyz.com');
    Contact ContR = new Contact(RecordTypeId = emcInternalRT, LastName = 'RTester', AccountId = '0017000000OmdVa', Email = 'rtester@xyz.com');
    Contact ContS = new Contact(RecordTypeId = emcInternalRT, LastName = 'STester', AccountId = '0017000000OmdVa', Email = 'stester@xyz.com');

    EBC_Briefing_Event__c beRecord = new EBC_Briefing_Event__c(Name = 'testName', Briefing_Status__c = 'Open', Briefing_Type__c = 'Internal meeting',Requestor_Name__c = ContR.Id );
    insert beRecord;
    
    ApexPages.currentPage().getParameters().put('Id',beRecord.Id);
    
    EBC_Invitees__c inviteeBA = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='', Attendee_Email__c='batester@xyz.com', Briefing_Team_Role__c='Briefing Advisor');
    insert inviteeBA;
    EBC_Invitees__c inviteeR = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='', Attendee_Email__c='rtester@xyz.com', Briefing_Team_Role__c='Requestor');
    insert inviteeR;
    EBC_Invitees__c inviteeS = new EBC_Invitees__c(Briefing_Event__c=beRecord.Id, Last_Name__c='', Attendee_Email__c='stester@xyz.com', Briefing_Team_Role__c='Scheduler');
    insert inviteeS;

    EBC_Update_Briefing_Team_Attributes updAtt2 = new EBC_Update_Briefing_Team_Attributes();
    updAtt2.retrieveInvitees();
    updAtt2.updateAttributes();
    updAtt2.cancel();
    
    }
}