/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE       DEVELOPER     DESCRIPTION                               
 |  ====       =========     =========== 
 | 
    16-Feb-10 Saravanan C    Added SplitRow variable - WR 123622
    
    28-Apr-10 Karthik S      Changed the variable SplitRow to splitExceeding -WR-131541 / Defect 635
    15-Dec-10 Srinivas N     Added new string variable for WR 151992, Used in AddTeamsAddProducts vf page
    Mar-14 Naga           Added Added_Through__c in constructor for WR-339361 if Sales team member added through UI it will be 'Manul'.
    
 +===========================================================================*/
public class SalesTeamMember {

    public Boolean splitExceeding {get; set;} // Added by Karthik S - WR 131541
    
    public String Owner_Eligible {get; set;}{Owner_Eligible = 'No';}  // Addes by Srinivas N - WR 151992. Used in AddTeamsAddProducts vf Page.
    
    public Boolean selected {get; set;}

    public OpportunityTeamMember opptyTeamMember {get; set;}

    public Detail_Line__c detailSplit {get; set;}
    
    public Boolean ProductForecast{get;set;}
    public boolean blnHideSplitForProfile {get;set;}{blnHideSplitForProfile=true;}//Added by Naga to show/hide No products for forecast group in sales team UI
    // Code added by Avinash begins below
    public Boolean blnIsPartner {get; set;} {blnIsPartner = false;}
    public Boolean blnIsOppRegPartner {get; set;} {blnIsOppRegPartner = false;}
    
    public SalesTeamMember(Boolean selected,OpportunityTeamMember mbr){

        this(selected,mbr,null);

    }

    
    public SalesTeamMember(Boolean selected,OpportunityTeamMember mbr,Detail_Line__c split){

        this.selected=selected;

        this.opptyTeamMember=mbr;

        this.detailSplit=split;

        if(mbr != null &&  mbr.TeamMemberRole != null && (mbr.TeamMemberRole == 'Partner User' || mbr.TeamMemberRole == 'Approving Partner User' || mbr.TeamMemberRole == 'Secondary Partner User'))
            this.blnIsPartner = true;
        
        if(mbr != null &&  mbr.TeamMemberRole != null && (mbr.TeamMemberRole == 'Approving Partner User' || mbr.TeamMemberRole == 'Secondary Partner User'))
            this.blnIsOppRegPartner = true;


        if(mbr != null)
            if(mbr.Added_Through__c!= null)
                this.opptyTeamMember.Added_Through__c = mbr.Added_Through__c;
            else
                this.opptyTeamMember.Added_Through__c = 'Manual';
        
    }

}