public class Cases_PSC_Portal_Controller{
    
  
  private final Case cs;
    //Case ID
    public Id caseid;
    public Boolean caseSaved {get; set;} {caseSaved=false;}
    public Attachment caseAttachment {get; set;}
    public List<Attachment> caseAttachments{get; set;}
     //Attachment
    public static final integer attachmentsToAdd = 4;
    public List<Attachment> newAttachments{get;set;}
    public Boolean addAtt {get; set;} 
    public String imgLink{get;set;}
    public String depValues{get;set;}
    public String recordTypeValue{get; set;}
    public RecordType rtObj = new RecordType();
    public Case caseObj {get; set;}
    public String selectedLanguage{get; set;}
    public List<SelectOption> selectLanguage {get; set;}
    public List<PSC_Subtype_Labels_wrapper> dependentListwrapper{get;set;}
    public List<List<PSC_Subtype_Labels_wrapper>> listdependentListwrapper{get;set;}
    public String labName;
    public String subTypeValue{get; set;}
    public List<String> fldString{get; set;}
    public String recSubTypeMapping {get; set;}
    public Map<String,SC_RecordType_Mapping__c> pscFldMappingMap{get; set;}
    public Map<String,List<SC_RecordType_Mapping__c>> pscSecMappingMap{get; set;}
    public List<String> pscSecLst {get; set;}
    public String caseNumber {get; set;}
    public Integer countInf=0;
    public Integer countDisp=0;
    public Integer infoIndex=100;
    public Integer dispIndex=100;
    public List<SC_RecordType_Mapping__c> infoLst {get;set;}
    public List<SC_RecordType_Mapping__c> dispLst {get;set;}
    public boolean showPicklist{get;set;}
    public boolean inquiryTypeRequired{get;set;}
    public boolean credColSubtypeReq{get;set;}
    public boolean showReqType{get;set;}
    public boolean showCredColSubVal{get;set;}
    public PSC_FieldMapping__c fieldMapConfig = null;
    public list<selectOption> inquiryValues{get; set;}
    public list<selectOption> reqTypeValues{get; set;}
    public list<selectOption> credCollSubtypeValues{get; set;}
    //public string subTypeValue {get; set;}
    public string subTypeDependents{get; set;}
    public List<string> finalInquireyValues {get; set;}
    public List<string> finalReqTypeValues {get; set;}
    public List<string> finalcredCollSubtypeValues {get; set;}
    public string selectedInquiry{get; set;}
    public string selectedReqType{get; set;}
    public string selectedcredCollSubtypeValues{get; set;}
    public User user;
    public Profile profileRec;
    public string theaterVal{get;set;}
    public string countryVal{get;set;}
    public string profileName{get;set;}
    public boolean isDistiVar{get;set;}
    public String dealRegId{get; set;}
    public String opptyId{get; set;}
    public boolean allowInsert{get; set;}
    public String finderFee{get; set;}
    public boolean isEduRec{get;set;}
    public boolean isQuoteRec{get;set;}
    public boolean isCredColRec{get;set;}
    public boolean isGRORec{get;set;}
    public boolean isDealRegMsg{get;set;}
    public boolean isContractMgmtMsg{get;set;}
    
    public Cases_PSC_Portal_Controller(){
        
        isDistiVar=false;
        finderFee='Finder\'s Fees';
        user = [Select profileId,name,Theater__c,Country__c,ContactId from User where id=:Userinfo.getuserid()];
        profileRec=[Select id,name from profile where id=:user.profileId ];
        profileName=profileRec.name;
        theaterVal=user.Theater__c;
        countryVal=user.Country__c;
        System.debug('theaterVal--->'+theaterVal);
        if(profileName.contains('Distribution VAR')){
            isDistiVar=true;
        }
        System.debug('profileRec----->'+profileRec);
        showPicklist = false;
        showReqType = false;
        showCredColSubVal=false;
        inquiryTypeRequired = true;
        //Fetching Record Type name from the URL parameter passed from Main page
        recordTypeValue = ApexPages.currentPage().getParameters().get('recordType');
        if(recordTypeValue==Label.Case_PSC_Invoicing_RT_Name_No_Translate){
        recordTypeValue='Finder\'s Fees';
        }
        dealRegId = ApexPages.currentPage().getParameters().get('DealId');
        opptyId = ApexPages.currentPage().getParameters().get('OpptyId');
        //recordTypeValue='PSC - '+recordTypeValue;
        //Fetching Custom setting data for the image details and Sub Type values for a particular Record Type
      /*  subTypeValue =  ApexPages.currentPage().getParameters().get('subType');
        subTypeDependents = Case__c.get('subTypeValue');
        if(subTypeDependents != null)||(subTypeDependents != '')
        {
            showPicklist = true;
            finalInquireyValues = new List<string>();
            finalInquireyValues = subTypeDependents.split(',');
            for(string st: finalInquireyValues)
            {
                inquiryValues.add(new SelectOption(st,st));
                
            }
            
        }*/
        
        Map<String,PSC_CaseType_Image__c> mapPscTypeImage = PSC_CaseType_Image__c.getAll();
        
        if(recordTypeValue !='' && recordTypeValue != null && mapPscTypeImage !=null && mapPscTypeImage.size()>0){
        
            //Fetching image details for the RT
            PSC_CaseType_Image__c subTypeImageObj = mapPscTypeImage.get(recordTypeValue);
             System.debug('recordTypeValue---->'+recordTypeValue);
             System.debug('depValues---->'+subTypeImageObj.Dependent_Values__c.split(','));
            //Fetching the list of Subtypes for a given case type
            List<String> depValues = subTypeImageObj.Dependent_Values__c.split(',');
            List<PSC_Subtype_Labels_wrapper> miniLst=new List<PSC_Subtype_Labels_wrapper>();
            dependentListwrapper=new List<PSC_Subtype_Labels_wrapper>();
            listdependentListwrapper=new List<List<PSC_Subtype_Labels_wrapper>>();
            imgLink = subTypeImageObj.ImgValue__c;
            
            for(String subVal:depValues){
                  String subVal1=subVal;
                  String labelforHeading=subVal1;
                  if(subVal.contains(' / ')||subVal.contains('/')||subVal.contains(' & ')||subVal.contains('&')||subVal.contains(' - ')||subVal.contains('-')||subVal.contains('\'')){
                                if(subVal=='Configuration Support(EMEA/APJ)'){
                                subVal='Configuration Support_EMEA_APJ';
                                }
                                if(subVal.contains('Finder')){
                                subVal='Finders Fees';
                                }
                                if(subVal.contains(' / ')){
                                subVal=subVal.replaceAll(' / ','_');    
                                }
                                if(subVal.contains('/')){
                                subVal=subVal.replaceAll('/','_');  
                                }
                                if(subVal.contains(' & ')){
                                subVal=subVal.replaceAll(' & ','_');    
                                }
                                if(subVal.contains('&')){
                                subVal=subVal.replaceAll('&','_');
                                }
                                if(subVal.contains(' - ')){
                                subVal=subVal.replaceAll(' - ','_');
                                }
                                if(subVal.contains('-')){
                                subVal=subVal.replaceAll('-','_');
                                }
                        
                            }
                        
                labName='PSC_'+subVal.replaceAll(' ','_');
                labelforHeading=labName+'_heading';
                dependentListwrapper.add(new PSC_Subtype_Labels_wrapper(subVal1,labelforHeading,labName));
                
            }
            Integer counter=0;
            for(PSC_Subtype_Labels_wrapper wrapLstRec:dependentListwrapper){
                miniLst.add(wrapLstRec);
                counter++;
                if(counter==3){
                    listdependentListwrapper.add(miniLst);
                    miniLst=new List<PSC_Subtype_Labels_wrapper>();
                    counter=0;
                }   
            }
            if(!miniLst.isEmpty()){
                    listdependentListwrapper.add(miniLst);  
            }
            System.debug('listdependentListwrapper--->'+listdependentListwrapper);
        }
        
        
        //Language Selection made by the user on the first page - Passed as a parameter in the URL
        //selectedLanguage= ApexPages.currentPage().getParameters().get('langForPage');
        if(user.ContactId!=null || user.ContactId!=''){
            Contact userCon=[SELECT id,name,Locale_Code__c from Contact where id=:user.ContactId];
            selectedLanguage=userCon.Locale_Code__c;
        
        }
        else{
        
        selectedLanguage='en_US';
        
        }
        system.debug('---selectedLanguage---'+selectedLanguage);
        //Initially defaulting the language to English
        //selectedLanguage = 'en_US';
        selectLanguage = new List<SelectOption>();
        Map<String,SC13_Language__c> mapLangauage = SC13_Language__c.getall();
        //Creating and adding value to List for sorted Language display
        List<String> lstLangSorted = new List<String>();
        lstLangSorted.addall(mapLangauage.keyset());
        lstLangSorted.sort();
        
        for(String langName : lstLangSorted){
            selectLanguage.add(new SelectOption(mapLangauage.get(langName).Language_Code__c , langName));
        }
    }
    
    //Fetching the Case record details from the  user input
    public Cases_PSC_Portal_Controller(ApexPages.StandardController controller){
        
        fieldMapConfig = PSC_FieldMapping__c.getValues('DealInquirySubTypeNonMandatory');
        isEduRec=false;
        isQuoteRec=false;
        isCredColRec=false;
        isGRORec=false;
        isDealRegMsg = false;
        isContractMgmtMsg = false;
        this.cs = (Case)controller.getRecord();
        caseObj =(Case)controller.getRecord();
        caseAttachment = new Attachment();
        caseAttachments = new List<Attachment>();
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        
        //Attachment Button

        addAtt = true;
        recordTypeValue = ApexPages.currentPage().getParameters().get('recordType');
        if(recordTypeValue==Label.Case_PSC_Invoicing_RT_Name_No_Translate){
        recordTypeValue='Finder\'s Fees';
        }
        if(recordTypeValue!=Label.PSC_Edu_Serv_Label_No_translate && recordTypeValue!=Label.PSC_Quote_No_Translate && recordTypeValue!=Label.PSC_CreditandColl_No_Translate && recordTypeValue!=Label.PSC_GRO_No_Translate){
        recordTypeValue='PSC - '+recordTypeValue;
        }
        if(recordTypeValue==Label.PSC_Edu_Serv_Label_No_translate){
        isEduRec=true;
        }
        if(recordTypeValue==Label.PSC_Quote_No_Translate){
        isQuoteRec=true;
        caseObj.Partner_Case__c=true;
        caseObj.Partner_Theater__c=theaterVal;
        caseObj.Partner_Country__c=countryVal;
        }
        if(recordTypeValue==Label.PSC_CreditandColl_No_Translate){
        recordTypeValue='Credit & Collections';
        caseObj.Credit_Collection_Sub_Type__c='Collection/Invoice Inquiry';
        isCredColRec=true;
        }
        if(recordTypeValue==Label.PSC_GRO_No_Translate){
        isGRORec=true;
        }
        fldString = new List<String>();
        pscSecLst=new List<String>();
        infoLst=new List<SC_RecordType_Mapping__c>();
        dispLst=new List<SC_RecordType_Mapping__c>();
        pscFldMappingMap = new Map<String,SC_RecordType_Mapping__c>();
        subTypeValue = ApexPages.currentPage().getParameters().get('subType');
        selectedLanguage =ApexPages.currentPage().getParameters().get('langForPage');
        dealRegId = ApexPages.currentPage().getParameters().get('DealId');
        opptyId = ApexPages.currentPage().getParameters().get('OpptyId');
        System.debug('----->recordTypeValue-->'+recordTypeValue);
        system.debug('subType --> '+subTypeValue );
        System.debug('----->dealRegId-->'+dealRegId);
        system.debug('opptyId --> '+opptyId );
        
         selectedInquiry = '';
         selectedReqType = '';
         selectedcredCollSubtypeValues='';
         inquiryTypeRequired = true;
         credColSubtypeReq = true;
         finalInquireyValues = new List<string>();
         finalReqTypeValues = new List<string>();
         finalcredCollSubtypeValues=new List<String>();
         inquiryValues = new List<selectOption>();
         inquiryValues.add(new SelectOption('None','--None--'));
         reqTypeValues = new List<selectOption>();
         reqTypeValues.add(new SelectOption('None','--None--'));
         credCollSubtypeValues = new List<selectOption>();
         credCollSubtypeValues.add(new SelectOption('None','--None--'));
        if(fieldMapConfig != null && fieldMapConfig.DependentValues__c.containsIgnoreCase(subTypeValue)){
            
            inquiryTypeRequired = false;
        }
        
         map<string,PSC_FieldMapping__c> customAll = PSC_FieldMapping__c.getAll();
         
        for(PSC_FieldMapping__c csConfig: customAll.values())
        {
            if(csConfig.UniqueControlling__c == subTypeValue && csConfig.FieldName__c == 'Inquiry Type')
            {
                showPicklist = true;
                finalInquireyValues = csConfig.DependentValues__c.split(',');
                for(string st: finalInquireyValues)
                {
                    inquiryValues.add(new SelectOption(st,st));
                    
                }
            }
            if(csConfig.UniqueControlling__c == subTypeValue && csConfig.FieldName__c == 'Request Type'){
                showReqType = true;
                finalReqTypeValues = csConfig.DependentValues__c.split(',');
                for(string st: finalReqTypeValues)
                {
                    reqTypeValues.add(new SelectOption(st,st));
                    
                }
            }
            if(csConfig.UniqueControlling__c == subTypeValue && csConfig.FieldName__c == 'Credit & Collections Department'){
                showCredColSubVal = true;
                finalcredCollSubtypeValues = csConfig.DependentValues__c.split(',');
                for(string st: finalcredCollSubtypeValues)
                {
                    credCollSubtypeValues.add(new SelectOption(st,st));
                    
                }
            }
        }
        
        
        if(recordTypeValue=='PSC - Channel System Support' && subTypeValue=='Partner Portal'){
        
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please direct your query to support@emc.com');
            ApexPages.addMessage(myMsg); 
        
        }
        
        if(recordTypeValue=='PSC - Order Management' && subTypeValue=='Renewal Status Request'){
        
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please direct your query to your Renewals Rep.');
            ApexPages.addMessage(myMsg); 
        
        }
        
        if(recordTypeValue=='PSC - Contract Management'){
            isContractMgmtMsg = true;
        }
        
        if(recordTypeValue=='PSC - Deal Registration and Lead Management'){
            isDealRegMsg = true;
        }
        
        if(recordTypeValue != ''){
            rtObj  = [Select id,Name from RecordType where sObjectType='Case' and name=:recordTypeValue];
            if(rtObj !=null){
                caseObj.recordtypeid = rtObj.id;
                caseObj.Record_Type_Hidden__c = recordTypeValue;
                caseObj.Type = subTypeValue;
                caseObj.Case_Sub_Type__c = subTypeValue;
                //caseObj.BU__c ='testBU';
                caseObj.Origin=System.Label.PSC_Partner_Central_Origin;
                System.debug('----->subType-->'+caseObj.Case_Sub_Type__c);
            }
        }
        
        recSubTypeMapping = '';
        fldString.clear();
         pscFldMappingMap.clear();
         recSubTypeMapping = recordTypeValue.trim()+'+'+subTypeValue;
        //recSubTypeMapping = recordTypeValue.trim();
        system.debug('---recSubTypeMapping---'+recSubTypeMapping);
        List<SC_RecordType_Mapping__c> lstRecord = [Select FieldName__c,Section_Name__c,Section_Label_Name__c,isMandatory__c,isRendered__c,SortOrder__c,RecordType_Subtype__c,On_webform__c from SC_RecordType_Mapping__c where RecordType_Subtype__c=:recSubTypeMapping and On_Portal__c=true order by SortOrder__c ];
        System.debug('----->lstRecord'+lstRecord);
        pscSecMappingMap = new Map<String,List<SC_RecordType_Mapping__c>>();
        
        for(SC_RecordType_Mapping__c pscRecObj: lstRecord){
            fldString.add(pscRecObj.FieldName__c);
            pscFldMappingMap.put(pscRecObj.FieldName__c,pscRecObj);
            
        if(pscSecMappingMap.containsKey(pscRecObj.Section_Name__c)){
                pscSecMappingMap.get(pscRecObj.Section_Name__c).add(pscRecObj);
                }
                else{
                pscSecMappingMap.put(pscRecObj.Section_Name__c,new List<SC_RecordType_Mapping__c>{pscRecObj});
                }
                
        }             
            if(pscSecMappingMap != null && pscSecMappingMap.size() > 0){
                
                pscSecLst.addAll(pscSecMappingMap.keySet());
                for(String str:pscSecLst){
                    countInf++;
                    countDisp++;
                    if(str=='Case Information'){
                        infoLst.addAll(pscSecMappingMap.get('Case Information'));
                        infoIndex=countInf-1;
                    }
                    if(str=='Description Information'){
                        dispLst.addAll(pscSecMappingMap.get('Description Information'));
                        dispIndex=countDisp-1;
                    }
                }
                if(infoIndex !=100){
                    pscSecLst.remove(infoIndex);
                }
                if(infoIndex !=100 && dispIndex !=100){
                    pscSecLst.remove(dispIndex-1);
                }
                if(dispIndex !=100 && infoIndex==100){
                    pscSecLst.remove(dispIndex);
                }
                System.debug('pscSecLst--->'+infoLst);
                System.debug('dispLst--->'+dispLst);
            }
    }
    
    //Getting Image links
    public string getImgLogo(){
    
        return imgLink;   
    }
    
    //Method to decide whether to display language options to user or not. It would be displayed on Web-form.
    public Boolean getisRenderLang() {  
        
        Boolean isRenderLang = true;    
        /*User partnerUser = [select Id,Name, ProfileID,ContactId from User WHERE id = :UserInfo.getUserId()];
        system.debug('---partnerUser---'+partnerUser);
        if(partnerUser.ProfileID != '00eS0000000MDjoIAG'){
            isRenderLang = false;
        }*/
        if(UserInfo.getUsertype()!='guest'){
         isRenderLang = false;
        }
        return isRenderLang;
    }
    
    //Wrapper class to combine the header and description text to be displayed on the VF page
    public class PSC_Subtype_Labels_wrapper {
    
        public String displayName {get;set;}
        public String headingLabel {get;set;}
        public String desclabelName {get;set;}
       
        public PSC_Subtype_Labels_wrapper(String subVal,String headLabName,String labName){
            displayName=subVal;
            headingLabel=headLabName;
            desclabelName=labName;   
           
        }
    }    
    
    //Method to save the Case record in the database
    public void SaveAction(){  
        System.debug('----->caseObj--->'+caseObj); 
        try{
            if(selectedInquiry != 'None'){
                caseObj.Inquiry_Type_1__c = selectedInquiry ;
         allowInsert = true;
            }
            
            else if(fieldMapConfig != null && !fieldMapConfig.DependentValues__c.containsIgnoreCase(subTypeValue)){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.PSC_Inquiry_Type_Mandatory_Error_Msg);
                ApexPages.addMessage(myMsg);
                caseSaved=false;
                allowInsert = false;
               
            }
            if(selectedReqType != 'None' && selectedReqType != ''){
                caseObj.Request_Type__c = selectedReqType ;
            caseSaved = true;
            }
             if(selectedcredCollSubtypeValues=='None'){
                       ApexPages.Message Error_Msg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.PSC_CreditCollection_Dept_Required);
                       ApexPages.addMessage(Error_Msg);
                       caseSaved=false;
                       allowInsert = false;      
                       
                }
            
            if(selectedcredCollSubtypeValues!='None'){
                caseObj.Credit_Collections_Department__c=selectedcredCollSubtypeValues;
                caseSaved = true;
            }
            
            if(dealRegId != null && dealRegId !=''){
                caseObj.Lead_Name__c = Id.valueOf(dealRegId);
                Lead dealLead = [Select id,DealReg_Deal_Registration_Number__c from Lead where Id =: dealRegId];
                if(dealLead != null){
                    caseObj.Deal_Registration_Number__c = dealLead.DealReg_Deal_Registration_Number__c;
                }
                
            }
            if(opptyId != null && opptyId != ''){
                caseObj.Opportunity_Name__c = Id.valueOf(opptyId);
            }
         if(caseObj.Legal_Company_Name__c!=null && caseObj.Legal_Company_Name__c!='' ){
            
            caseObj.Description =caseObj.Description+'    Legal Company Name : '+caseObj.Legal_Company_Name__c;
           }
            /*if(caseObj.Deal_Registration_Number__c!=null && caseObj.Deal_Registration_Number__c!=''){
                List<Lead> LeadList=[SELECT id,name FROM Lead WHERE Lead_Number__c = : caseObj.Deal_Registration_Number__c];
                
                if(LeadList.size()>0){
                    
                   caseObj.Deal_Registration_URL__c=URL.getSalesforceBaseUrl().toExternalForm() + '/' + LeadList[0].Id;
                    
                }
            }*/
        
         if(allowInsert != false){
            if(caseId==null)
                {   
           Insert cs;
                  caseSaved=true;
         }else{
                                cs.id=caseId;   
                            update(cs);
                            caseSaved=true;
                          }
            }
            
            else{
            
              caseSaved=false;
            }
         
     }catch(DMLException ex){
           ApexPages.addMessages(ex);
       caseSaved=false;
    }
   
    }
    
     //Method to cancel the transaction
    public Pagereference CancelAction(){
        Pagereference canPageRef=new Pagereference ('/apex/Cases_PSC_Portal_MainPage');
        return canPageRef;
    }
public PageReference saveAttachment()  {
         system.debug('----> cs.Id'+cs.Id);
         PageReference pageRef=null;
         try{
                List <Attachment> AttachmentsToInsert = new List <Attachment> (); 
                
                if (caseSaved==true) {
                    
                    //clear assets those are in
                    for( Attachment caseAttachment: caseAttachments){
                        if(caseAttachment.body!=null  ){
                             //caseAttachment.id=null;
                             System.debug('caseObj.id '+cs.id);
                             System.debug('caseAttachment.parentId '+caseAttachment.parentId);
                             System.debug('caseAttachment.name'+caseAttachment.name);
                            if(caseAttachment.parentId == null ){
                                caseAttachment.parentId = cs.id;
                                AttachmentsToInsert.add(caseAttachment);
                            }
                        }
                    }
                    
                    if(AttachmentsToInsert.size()>0){
                        system.debug('AttachmentsToInsert '+AttachmentsToInsert);
                        insert AttachmentsToInsert;
                        caseAttachment = null;
                        AttachmentsToInsert=null;
                        caseAttachments = new List<Attachment>();
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                    }
                    //Case newCaseRecord =[select id,CaseNumber from Case where id = :cs.id];
                    if((ApexPages.getMessages()).size()>0 ) {
                        try{
                            List<Attachment> Att_List =[SELECT Id FROM Attachment where ParentId=:cs.id];
                            if(Att_List != null && Att_List.size()>0  ){
                                delete Att_List;
                            }
                        }catch(exception e){
                            system.debug(e);
                        }
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info,Label.SC_FileAttachHelpText));
                        pageRef = null;
                        caseId=cs.id;
                        caseSaved=true;
                        cs.id=null;
                        
                        // delete newCaseRecord;
                    }else{
                        System.debug('----->cs'+cs);
                        if(cs.Id!=null){
                        Case newCaseRecord =[select id,CaseNumber from Case where id = :cs.id];
                        //pageRef=new PageReference('/apex/PSCsuccessPage?caseNum='+newCaseRecord.CaseNumber+'&langForPage='+selectedLanguage+'&recordType='+recordTypeValue);
                        pageRef=new PageReference('/' + newCaseRecord.Id);
                        pageRef.setRedirect(true);    
                        }
                    }
                           
                }
               
                
         }catch(Exception e){
             ApexPages.addMessages(e);
             pageRef=null;
             System.debug(e);
         }
         return pageRef;
    }
}