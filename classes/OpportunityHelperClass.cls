/*==============================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE       DEVELOPER     WR       DESCRIPTION                               
 |  ====       =========     ==       =========== 
 | 15-04-2014  Srikrishna    353275    Added logic to update Logged in user name, 
 |                                     Last modified date and last modifed information 
 | 17-09-2014  Bisna         CI 1086    removed query to userMap and code preceding as the variable is not used
 | 09-23-14    Vivek Barange   #0752      Tigger to make sales_force__c field to auto populated 
 |                                        based on the Account Owners BU Attribute. 
+=============================================================================*/
public class OpportunityHelperClass{
    public static boolean run = true;
    public static void meddicFieldUpdate(List<Opportunity> newOppList, List<Opportunity> oldOppList, Boolean beforeEvent, Boolean afterEvent, Boolean insertEvent, Boolean updateEvent){
        String UserId = UserInfo.getUserID();
        Set<Id> userIdSet = new Set<Id>();    
        for(Opportunity opp : newOppList){
            userIdSet.add(opp.ownerId);
            userIdSet.add(opp.Opportunity_Owner__c);            
        }
        
        if(beforeEvent){
            if(insertEvent){
                for(Opportunity opp : newOppList){
                    if(opp.Metrics__c != null || opp.Economic_Buyer__c != null || opp.Decision_Process__c != null || opp.Decision_Criteria__c != null || opp.Identify_Pain__c != null || opp.Champion__c != null){
                        opp.MEDDIC_Last_Updated_By__c = UserId;
                        opp.MEDDIC_Last_Updated__c = System.now();
                        opp.MEDDIC_Counter__c = 1;
                        
                    }        
                }     
            }
            if(TriggerContextUtility.runOnce()){
                if(updateEvent){
                    Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([select Id, Opportunity_Owner__c, Name, MEDDIC_Counter__c, Metrics__c, Economic_Buyer__c, Decision_Process__c, Decision_Criteria__c, Identify_Pain__c, Champion__c from Opportunity where Id in : oldOppList]);
                    for(Opportunity oldOpp : oldOppList){
                        for(Opportunity newOpp : newOppList){
                            if(oldOpp.Id == newOpp.Id){
                                if((oldOpp.Metrics__c != newOpp.Metrics__c) || (oldOpp.Economic_Buyer__c != newOpp.Economic_Buyer__c)
                                    || (oldOpp.Decision_Process__c != newOpp.Decision_Process__c) || (oldOpp.Decision_Criteria__c  != newOpp.Decision_Criteria__c )
                                    || (oldOpp.Identify_Pain__c != newOpp.Identify_Pain__c) || (oldOpp.Champion__c != newOpp.Champion__c)){
                                    newOpp.MEDDIC_Last_Updated_By__c = UserId;
                                    newOpp.MEDDIC_Last_Updated__c = System.now();
                                    if(oppMap.get(oldOpp.Id).MEDDIC_Counter__c == null){
                                        newOpp.MEDDIC_Counter__c = 1;
                                    }else{
                                        newOpp.MEDDIC_Counter__c = oppMap.get(oldOpp.Id).MEDDIC_Counter__c + 1;    
                                    }                            
                                }
                            }
                        }
                    }    
                }
            }
        }  
    }
    
    public static Map<Id, User> mapUsers;
    public static Map<Id, User> getMapUsers(Set<Id> ownerIds) {
        if(mapUsers==null || mapUsers.isEmpty()) {
            System.debug('ownerIds---->'+ownerIds);
            mapUsers = new Map<Id, User>([select Id, BU_Attribute__c from User where Id = :ownerIds]);
            return mapUsers;
        }
        return mapUsers;
    }
    
    public static Map<Id, User> leadOwnersMap;
    public static Map<Id, User> getLeadOwnersMap(Set<Id> ownerIds) {
        if(leadOwnersMap==null || leadOwnersMap.isEmpty()) {
            System.debug('ownerIds---->'+ownerIds);
            leadOwnersMap = new Map<Id, User>([select Id, BU_Attribute__c from User where Id = :ownerIds]);
            return leadOwnersMap;
        }
        return leadOwnersMap;
    }
    
    
    //#0752 - Created method to populate sales_force__c field
    public static void updateSalesForceField(List<Opportunity> lstOpty) {
        
        Set<Id> ownerIds = new Set<Id>();
        for(Opportunity opty : lstOpty) {
            if(opty.Opportunity_Owner__c != null) {
                ownerIds.add(opty.Opportunity_Owner__c);
            }
        }
        OpportunityHelperClass.getMapUsers(ownerIds);
        for(Opportunity opty : lstOpty) {
            System.debug('opty.Opportunity_Owner__c--->'+opty.Opportunity_Owner__c);
            System.debug('opty.ownerid--->'+opty.ownerid);
            System.debug('mapUsers--->'+mapUsers);

            if(mapUsers != null && !mapUsers.isEmpty() && mapUsers.containsKey(opty.Opportunity_Owner__c) && mapUsers.get(opty.Opportunity_Owner__c).BU_Attribute__c != null && mapUsers.get(opty.Opportunity_Owner__c).BU_Attribute__c.contains('CMA')) {
                opty.Sales_Force__c = 'ESG';
            } else {
                opty.Sales_Force__c = 'EMC';
            }
        }
    }
    
    //#0752 - Created method to populate sales_force__c field
    public static void updateSalesForceField(List<Lead> lstLead) {
        Set<Id> ownerIds = new Set<Id>();
        for(Lead lead : lstLead) {
            if(((String)lead.OwnerId).contains('005')) {
                ownerIds.add(lead.OwnerId);
            }
        }
        
        OpportunityHelperClass.getLeadOwnersMap(ownerIds);
        for(Lead lead : lstLead) {
            if(((String)lead.OwnerId).contains('005')) {
                if(leadOwnersMap != null && !leadOwnersMap.isEmpty() && leadOwnersMap.containsKey(lead.OwnerId) && leadOwnersMap.get(lead.OwnerId).BU_Attribute__c != null && leadOwnersMap.get(lead.OwnerId).BU_Attribute__c.contains('CMA')) {
                    lead.Sales_Force__c = 'ESG';
                } else {
                    lead.Sales_Force__c = 'EMC';
                }
            } else {
                lead.Sales_Force__c = 'EMC';
            }       
        }
    }
}