/*====================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR         DESCRIPTION                               

 |  ====          =========      ==         =========== 
 |  01/25/2013    Ketan Benegal  WR 196848  Remove DealReg_EMCTA_Certified_Email__c field.
 |  19/04/2011    Ashwini Gowda  Req# 2450  Test class for non-portal registration Form.
                                  2429 
 |  14/12/2011      Anil                    Removed Query for fetching Partner and used Custom setting Data Helper
 |  11-JAN-2012     Anil                    Removed role Id
    17-FEB-2015   Paridhi        PI         Modified to increase the coverage
 |=============================================================================================================== */
 
@isTest
Private Class PRM_DEALREG_NonPortalRegistration_TC 
{
    public static Contact cont1=new Contact();
    static list<Account> lstAccount = new list<Account>();
    static list<Account> lstPartnerAccount = new list<Account>();
    static list<Account> lstCombinedAccount = new list<Account>();
    /* @Method <This method is used test non-portal registration Form>.   
    @param - <void>  
    @return <void> - <Not Returning anything   
    @throws exception - <No Exception>
    */      
    Private static testMethod void startTest()
    {
        User partner;
        User PartnerUser = [Select Id,Name,ContactId from User where  IsActive = true and  UserType = 'PowerPartner' limit 1];
        User systemAdminUser = [Select Id,Name from User where Profile.Name ='System Administrator' and IsActive = true limit 1];
        system.debug('SysAdmin++++'+systemAdminUser);
        System.RunAs(systemAdminUser)
        {
            //PRM_VPP_JobDataHelper.createVPPCustomSettingData();
              CustomSettingDataHelper.dataValueMapCSData();
              CustomSettingDataHelper.eBizSFDCIntCSData();
              CustomSettingDataHelper.profilesCSData();
              CustomSettingDataHelper.dealRegistrationCSData();
              CustomSettingDataHelper.bypassLogicCSData();
              CustomSettingDataHelper.CountryMapping();
        }
        lstAccount = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        lstPartnerAccount = AccountAndProfileTestClassDataHelper.CreatePartnerAccount(); 
        lstCombinedAccount.addAll(lstAccount);
        lstCombinedAccount.addAll(lstPartnerAccount);
        insert lstCombinedAccount;
        for(Account account:lstPartnerAccount)
           {
            account.IsPartner = true;
            account.Lead_Oppty_Enabled__c = true;
            account.Type = 'Partner';
            account.PROFILED_ACCOUNT_FLAG__c = true;
            account.Status__c='A';
            }
        update lstPartnerAccount;
           
        list<Lead> leadlist = new list<Lead>();
        Lead newLead = new lead();
        newLead.lastname = 'Test Lead';
        newLead.company = 'EMC';
        newLead.DealReg_PSC_Owner__c = Userinfo.getUserId();
        newLead.DealReg_Deal_Registration_Status__c = 'New';
        newLead.DealReg_Of_Registration_Products__c = 3;
        newLead.DealReg_Deal_Description__c = 'test';
        newLead.DealReg_Deal_Registration_Justification__c = 'test';
        newLead.Partner__c = lstPartnerAccount [0].id;
        newLead.DealReg_Theater__c = 'EMEA';
        newLead.Channel__c = 'Indirect';
        newLead.City ='India';
        newLead.Street ='Bangalore';
        newLead.DealReg_Deal_Registration__c = true;
        newLead.DealReg_Pre_Sales_Engineer_Name__c = 'Test1';
        newLead.DealReg_Pre_Sales_Engineer_Phone__c = '75964';
        newLead.DealReg_Pre_Sales_Engineer_Email__c = 'test@t.com';
        //Ketan Benegal - Commented the following line for WR 196848. Dt.:01/25/2013
        //newLead.DealReg_EMCTA_Certified_Email__c = 'test@tt.com';
        leadlist.add(newLead);
        insert leadlist;
        
        Lead dealReg=leadlist[0];
    
        List<Product2>ProductList = new List<Product2>();
        Product2 ProductObj = new Product2();
        ProductObj.Name='Test Product';
        ProductObj.Business_Unit__c='STORAGE';
        ProductObj.Product_Type__c='CS HARDWARE';
        ProductList.add(ProductObj);
        insert ProductList;
        
        List<Partner_Product_Catalog__c> partnerProductList = new List<Partner_Product_Catalog__c>();
        Partner_Product_Catalog__c partnerProductObj = new Partner_Product_Catalog__c();
        partnerProductObj.Product__c= ProductList[0].id;
        partnerProductList.add(partnerProductObj);
        insert partnerProductList;
        
        List<Partner_Tier_Mapping__c> partnerTierList = new List<Partner_Tier_Mapping__c>();
        Partner_Tier_Mapping__c partnerTierObj = new Partner_Tier_Mapping__c();
        partnerTierObj.Partner_Product_Catalogue__c = partnerProductList[0].id;
        partnerTierObj.Theater__c='Americas';
        partnerTierObj.Tier__c='Signature';
        partnerTierList.add(partnerTierObj);
        insert partnerTierList;
        
        Registration_Product__c RegistratinObj = new Registration_Product__c ();
        RegistratinObj.Deal_Registration__c = leadlist[0].id;
        RegistratinObj.Partner_Product_Catalog__c = partnerProductList[0].id;
        insert RegistratinObj; 
        
        
         
        // Create PartnerUser            
        Contact cont = UserProfileTestClassDataHelper.createContact();
        cont.AccountId=lstAccount[1].Id;
        insert cont;       
       
        System.RunAs(systemAdminUser)
        {
         Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
         String distributorSuperUser = data.get('EMEA Distributor Super User').DataValue__c ;
         Profile amerUserProf = [select Id from Profile where Name=: distributorSuperUser];
         partner = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.id,null,cont.Id);
         insert partner;
         System.debug('partner121212 ---->'+partner.id);
        }  
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(dealReg); 
           
        PRM_DEALREG_NonPortalRegistrationCntrler dealRegController = new PRM_DEALREG_NonPortalRegistrationCntrler(stdController);
        dealRegController.doSearchWithoutFilters();
        //dealRegController.sortList(dealregcontroller.sortField, true) ;
        if(dealRegController.lstFullProductsDetails.size()>0 )
        {
           Integer selectedcount = 0 ;
           for(PRM_DEALREG_NonPortalRegistrationCntrler.ProductsDataContainer selectedRegProd: dealRegController.lstFullProductsDetails)
           {                 
                selectedRegProd.selected = true ;
                dealRegController.lstRegistrationProduct.add(selectedRegProd.selProduct);                 
                selectedcount ++ ;
               if(selectedcount > 3)
               break ;
            }
        }
        PageReference pageref=Page.PRM_DEALREG_NonPortalRegistrationForm;
        Test.setCurrentPage(pageref); 
        pageRef.getParameters().put('leadid',leadlist[0].id);   
        pageRef.getParameters().put('retURL',leadlist[0].id); 
        
        Test.startTest();
        dealRegController.selectedrecord = lstAccount[0].id ;
        dealRegController.populateValues();
        dealRegController.leadRecord.LastName ='TEST NAME';
        dealRegController.selectedProducts();
        //dealRegController.doSearchQuery();
        dealRegController.addProducts();
        dealRegController.changeLang();
        dealRegController.getItems();
        dealRegController.saveProducts();
        dealRegController.cancelDeal();
        dealRegController.redirectToPowerlink();
        dealRegController.getProductsDetails();
        dealRegController.doSearchSubCatalog();     
        dealRegController.addProductsOnLead();
        dealRegController.sortList(dealregcontroller.sortField, true) ;
        dealRegController.runSortAction() ;
      // dealRegController.deleteProducts();
        dealRegController.doSearchProduct();
       // CustomSearch
        dealRegController.clearresults();
        dealRegController.populateselectedrecord();
        dealRegController.sortListAccounts('name', TRUE);
        dealRegController.runSortAction1();
        dealRegController.search();
        dealRegController.populateAccountInfo();
        // setting address input fields.
        dealRegController.selectedAccountName = 'TestAcc';
        dealRegController.selectedCity = 'Testcity';
        dealRegController.selectedCountry = 'Test';
        dealRegController.selectedState='Test' ;
        dealRegController.selectedZipCode = 'Test' ;
        dealRegController.selectedStreet = 'Test';
        dealRegController.inputAccountName = 'TestAcc';
        dealRegController.inputCity = 'Testcity';
        dealRegController.inputCountry = 'Australia';
        dealRegController.inputState ='Test' ;
        dealRegController.inputZipCode = 'Test' ;
        dealRegController.inputStreet = 'Test';
        
        dealRegController.search() ;
        dealRegController.lang = 'en_CH' ;
        dealRegController.populateAccountInfo();
        dealRegController.getSearchCriteriaList() ;
        dealRegController.getAccountDetails();
        dealRegController.sortListAccounts('name', TRUE);
        dealRegController.sortField1 = 'name';
        dealRegController.sortAscending1 = true;
        dealRegController.runSortAction1(); 
        PRM_DEALREG_NonPortalRegistrationCntrler.ProductsDataContainer Productclass = new PRM_DEALREG_NonPortalRegistrationCntrler.ProductsDataContainer(partnerTierList[0]);
        Productclass.selected=true;
        dealRegController.lstRegistrationProduct = partnerTierList  ; 
        dealRegController.searchProduct = 'Test Product'  ;
        dealRegController.SearchProductCatalog = 'Test Product'  ;      
        dealRegController.sortList(dealregcontroller.sortField, true) ;
        dealRegController.updateProduct();
        dealRegController.saveLeadAndProducts();
        dealRegController.doSearchQuery();
        dealRegController.doSearchProduct();
        dealRegController.doSearchSubCatalog();    
        dealRegController.lstExistingRegistrationProduct = partnerTierList  ;
        dealRegController.deleteProducts(); 
        dealRegController.isPartnerPortalUser = true;
        dealRegController.doSearchExistingProduct();
        
         /* System.RunAs(PartnerUser)
         {  
            
             dealRegController.addProducts();    
         }*/
        Test.stopTest();       
    }
}