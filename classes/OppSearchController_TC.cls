/*=======================================================================================================+
    |  HISTORY  |                                                                           
    |  DATE          DEVELOPER        WR                    DESCRIPTION                               
    |  ====          =========        ==                    =========== 
    | 28-Feb-2014   Aarti Jindal                            Test Class for OppSearchController(in Advanced Oppty Search)
    | 14-Aug-2014   Vivek Barange    #1156                  Modifiying code to increase test coverage
    | 17-Sep-2014   Aagesh Jose      #1290                  Modifying code to increase test coverage
    | 11-Feb-2015   Vinod Jetti      #1723                  Modifying code to increase test coverage
    | 17-March-2015 Vinod Jetti      #1772                  Updated code to coverage of added componets in main class
    +=====================================================================================================*/
    @isTest
    Private class OppSearchController_TC{
        static testMethod void OppSearchControllerTC(){
        CustomSettingDataHelper.VisibilityProgramCSData();// Added by Naga
        CustomSettingDataHelper.dataValueMapCSData();
        CustomSettingDataHelper.eBizSFDCIntCSData();
        CustomSettingDataHelper.profilesCSData();
        CustomSettingDataHelper.dealRegistrationCSData();
        CustomSettingDataHelper.bypassLogicCSData();
        
        Test.setCurrentPageReference(new PageReference('Page.Visibility_OppSearch'));
        System.currentPageReference().getParameters().put('forecastgroup', 'user');
        
        OppSearchController obj=new OppSearchController();
        OppSearchController obj2=new OppSearchController();
        OppSearchController obj3=new OppSearchController();
        
         List<EMCChannel__c>  elist = new List<EMCChannel__c>();
         insert elist;
            
            String forecastAmt='4676';
            String accId='686769';
            String forecastAmount='11';
            String forecastStatus='Pipeline';
            String oppName='Test Oppty';
            String oppNumber='111';
            String accountName = 'Test acc';
            String quoteNumber='11';
            String globalDuns='aa';
            DateTime dateFrom=System.today();
            DateTime dateTo=System.today();
            String dist='ss';
            String countr='United States';
            String ara='dg';
            String stat='fddf';
            String prodFamily='gjkh';
            String prodName='kkj';
            String primeSE='jgjjk';
            String forecastGroupAssign='dd';
            String stageNam='jgjgjkh';
            String coreRep='fjg';
            String fMapping='hkhjklh';
            String salesplay='EHC: VBLOCK';
            String specReq = 'test';
        
        test.startTest();
        obj.lookupOppty=null;
        obj.flagDeals=true;
        obj.forecastGroupFlag=true;
        obj.countr=countr;
        obj.coreRep=coreRep;
        obj.forecastAmt=forecastAmt;
        obj.accId=accId;
        obj.oppName=oppName;
        obj.forecastAmount=forecastAmount;
        obj.forecastStatus=forecastStatus;
        obj.fMapping=fMapping;
        obj.oppNumber=oppNumber;
        obj.quoteNumber=quoteNumber;
        obj.globalDuns=globalDuns;
        obj.dateFrom=dateFrom;
        obj.dateTo=dateTo;
        obj.specReq=specReq;
        obj.accountName = accountName;
        //#1156 - below code is added to cover newly added lines
        obj.closeDateFrom = system.today();
        obj.closeDateTo = system.today();
        obj.dist=dist;
        obj.ara=ara;
        obj.stat=stat;
        obj.prodFamily=prodFamily;
        obj.prodName=prodName;
        obj.primeSE=primeSE;
        obj.forecastGroupAssign=forecastGroupAssign;
        obj.stageNam=stageNam;
        obj2.prodFamily=prodFamily;
        obj3.prodName=prodName;
        
        obj.ELA=true; 
        obj.salesplay=salesplay; 
        obj2.ELA=false;
        obj3.ELA=false;
       
        
        //#1723
        obj.EHC=true;
        obj.DataLake=true;
        obj2.EHC=false;
        obj2.DataLake=false;
        obj3.EHC=false;
        obj3.DataLake=false;
        //obj.searchOpportunityResults();
       // obj.runSortAction1();
        obj.assignClick();
        obj.processSelected();
        List<EMCChannel__c>  elist1 = new List<EMCChannel__c>();
        EMCChannel__c elist2 = new EMCChannel__c();
        elist2.Channel_Profile_Id__c = '3143241dfgfg';
        elist2.name = 'EMC Channel Profile Id';
        elist1.add(elist2);
        insert elist1;
        obj.restrictPermission();
        obj.optyOptyTeamMap();
        obj.processSelf();
        obj.backAction();
        obj.nextPageAction();
        obj.getOpptyRefinedlstWrap();
        obj2.searchOpportunityResults();
        obj3.searchOpportunityResults();
        
        test.stopTest();
       }
     
        static testMethod void OppSearchControllerTC1(){
        
          List<EMCChannel__c>  elist1 = new List<EMCChannel__c>();
          EMCChannel__c elist2 = new EMCChannel__c();
          elist2.Channel_Profile_Id__c = '3143241dfgfg';
          elist2.name = 'EMC Channel Profile Id';
          elist1.add(elist2);
          insert elist1;
         
          PageReference pageRef = Page.OpportunitySearchResult;
          Test.setCurrentPage(pageRef);
          pageref.getParameters().put('InsufficientAccess', 'True');
          
          ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
          ID sysid1 = [ Select id from Profile where name ='Standard User' limit 1].Id;
          
          User insertUser = new user(email='testPI-user@emailTest.com',profileId = sysid ,  UserName='testPI@emailforPI.com', alias='tuser1PI', CommunityNickName='tuser1PI', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US', FirstName = 'TestPI', LastName = 'UserPI' ,Forecast_Group__c = 'Maintenance Renewals',Role__c='System Admin'); 
            
          insert insertUser;
          
           User insertUserP1 = new user(email='testPI2-user@emailTest.com',profileId = sysid1 ,  UserName='testPI2@emailforPI.com', alias='tuserPI2', CommunityNickName='tuser12PI', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US', FirstName = 'TestPI', LastName = 'UserPI' ,Forecast_Group__c = 'Maintenance Renewals',Role__c='Standard User'); 
            
          insert insertUserP1;
          
              System.runAs(insertUser)
              {
               CustomSettingDataHelper.VisibilityProgramCSData();// Added by Naga
               CustomSettingDataHelper.dataValueMapCSData();
               CustomSettingDataHelper.eBizSFDCIntCSData();
               CustomSettingDataHelper.profilesCSData();
               CustomSettingDataHelper.dealRegistrationCSData();
               CustomSettingDataHelper.bypassLogicCSData();
        
          List<Account> acc = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();       
          insert acc;
        
          List<Opportunity> opptyList=new List<Opportunity>();
          List<AccountTeamMember> atmList=new List<AccountTeamMember>(); 
           Integer i;
           for(i=0;i<2;i++){
             Opportunity oppty=testclassUtils.getOppty();
             AccountTeamMember atm=new AccountTeamMember(AccountId=oppty.AccountId,UserId=insertUser.Id,TeamMemberRole='TeamMemberRole');
             opptyList.add(oppty);
             atmList.add(atm);
            }
        
          opptyList[0].Opportunity_Number__c = '111#3';
          opptyList[0].LeadSource = 'Deal Registration';
          opptyList[0].Opportunity_Type__c = 'Refresh';
          opptyList[0].Account = acc[0] ;
          opptyList[0].Expected_Close_Date__c = system.Today()+10;
          opptyList[1].Opportunity_Number__c = '111#2';
          opptyList[1].LeadSource = 'Deal Registration';
          opptyList[1].Opportunity_Type__c = 'Refresh';
          opptyList[1].Account = acc[1] ;
          opptyList[1].Expected_Close_Date__c = system.Today()+1;
          insert opptyList;
          insert atmList;
        
         List<OpportunityTeamMember> Lstotm=new List<OpportunityTeamMember>(); 
         OpportunityTeamMember otm=new OpportunityTeamMember(UserId= UserInfo.getUserId(),OpportunityId=opptyList[0].Id);
         Lstotm.add(otm);
        /*  List<OpportunityTeamMember> Lstotm=new List<OpportunityTeamMember>();
          for(i=0;i<2;i++){
          OpportunityTeamMember otm=new OpportunityTeamMember();
          otm.UserId=insertUser.Id;
          otm.OpportunityId=opptyList[0].Id;
          otm.Reason_for_Addition__c = insertUser.Role__c;
          Lstotm.add(otm);
          } */      
          insert Lstotm;   
          
          SalesTeamMember members;
          List<SalesTeamMember>  memberslist=new List<SalesTeamMember>();
           SalesTeamMember members2=new SalesTeamMember(True,Lstotm[0]);
           SalesTeamMember members1=new SalesTeamMember(True,Lstotm[0]);
           memberslist.add(members2);
           members = members2;
        
          List<OpportunityShare> optyShare=new List<OpportunityShare>();
          OpportunityShare optyShareinst=new OpportunityShare();
          optyShareinst.OpportunityId=opptyList[0].Id;
          optyShareinst.UserOrGroupId=insertUserP1.Id;
          optyShareinst.OpportunityAccessLevel='Edit';
          optyShare.add(optyShareinst);
          insert optyShare; 
          
          Product2 p2 = new Product2(Name='Test Product',isActive=true);
          insert p2;
          
          Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
          insert customPB; 
          
          Id pricebookId = Test.getStandardPricebookId();
          
       /*   PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = p2.Id,UnitPrice = 10000, IsActive = true);
          insert standardPrice;
          
         List<OpportunityLineItem>  ListOppline = new List<OpportunityLineItem>();
          OpportunityLineItem Oppline=new OpportunityLineItem();
          Oppline.OpportunityId=opptyList[0].Id;
          Oppline.PricebookEntryId=standardPrice.Id;
          ListOppline.add(Oppline);
          insert ListOppline;*/
        
          List<Search_Proxy_Container__c>  spclist1 = new List<Search_Proxy_Container__c>();
          Search_Proxy_Container__c spcobj=new Search_Proxy_Container__c();
          spcobj.Show_All_Opportunities__c=True;
          //Added for #1772 - Start
          spcobj.Show_opportunity_where_user_is_there__c= true;
          spcobj.Show_opportunity_where_user_is_not_there__c= true; 
          //#1772 - End
          spcobj.Sales_Play__c='VBLOCK';
          spcobj.User__c = UserInfo.getUserId();
          spcobj.Date__c = system.today()+1;
          spcobj.Close_Date__c = system.today()+30;
          spclist1.add(spcobj);
          insert spclist1; 
          
          List<Detail_Line__c> detailinelist=new List<Detail_Line__c>();
          Detail_Line__c detailineobj=new Detail_Line__c();
          detailineobj.Forecast_Group__c='Alliances Verticals';
          detailineobj.Opportunity__c=opptyList[0].Id;
          detailineobj.Opportunity_Product__c='Test Product';
          detailinelist.add(detailineobj);
          insert detailinelist; 
          
           List<Forecast_Mapping__c> forcastMappinglist= new List<Forecast_Mapping__c>();
           Forecast_Mapping__c forcastMapping=new Forecast_Mapping__c();
          forcastMapping.BU__c ='Test';
          forcastMapping.Forecast_Group__c='BRS';
          forcastMappinglist.add(forcastMapping);
          insert forcastMappinglist;
          
          List<Forecast_Group_Product__c> forcastProductslist= new List<Forecast_Group_Product__c>();
           Forecast_Group_Product__c forcastProducts=new Forecast_Group_Product__c();
          forcastProducts.Forecast_Mapping__c =forcastMappinglist[0].Id;
          forcastProductslist.add(forcastProducts);
          insert forcastProductslist;
          
           
          List<warapperUserAssignment> searchlist=new List<warapperUserAssignment>();
           warapperUserAssignment wrapObj0=new warapperUserAssignment(opptyList[0]);
           warapperUserAssignment wrapObj1=new warapperUserAssignment(opptyList[1]);
          wrapObj0.selected = true;
          searchlist.add(wrapObj0);
           wrapObj1.selected = true;
           searchlist.add(wrapObj1);
         // insert searchlist;
              
          OppSearchController obj1=new OppSearchController();
         test.startTest();
          ApexPages.StandardSetController oppSsc = new ApexPages.StandardSetController(opptyList);
          obj1.oppSsc = oppSsc;
          System.debug('#obj1.oppSsc--->'+obj1.oppSsc);
          //Added by Vinod to increse code coverage
          obj1.oppName = 'Test Oppty';
          obj1.forecastStatus='Pipeline';
          obj1.oppNumber= '111#';
          obj1.forecastGroupFlag=true;
          obj1.ELA=false; 
          obj1.EHC=false;
          obj1.DataLake=false;
          obj1.searchResultFlag = true;
          obj1.showAllOpptysSearchProxy.Show_All_Opportunities__c = false;
          //Added for #1772 - Start
          obj1.showAllOpptyswhereUserIsThereInSalesTeam.Show_opportunity_where_user_is_there__c= false;
          obj1.showAllOpptyswhereUserIsNotThereInSalesTeam.Show_opportunity_where_user_is_not_there__c= true;
          //#1772 - End
          obj1.searchOpportunityResults();
          obj1.getLstWrap();
          obj1.tempLstWrap=searchlist;
          //obj1.searchOpportunityResults(opptyList);
          //obj1.atmwrapper(); 
          //searchlist=obj1.getLstWrap();
          //searchlist[0].selected=true;
          //searchlist[1].selected=true;
          obj1.assignClick();
          obj1.runSortAction1();
          obj1.getOpptyRefinedlstWrap();
          if(obj1.lst_AccTeam_Refined != null && !obj1.lst_AccTeam_Refined.isEmpty()) {
            obj1.lst_AccTeam_Refined[0].selected=true;
        }
          obj1.addMembers(memberslist);
       // obj1.assignTeamAndCalculateSplit(insertUser.Id,opptyList[0].Id,atmList[0].TeamMemberRole) ;
          obj1.optyOptyTeamMap();
          obj1.processSelected();
          obj1.restrictPermission();
          obj1.optyOptyTeamMap();
          obj1.processSelf();
          obj1.backAction();
          obj1.nextPageAction();
          obj1.assignOthers(insertUser.Id);//added
          obj1.assignTeamAndCalculateSplit(insertUser.Id,opptyList[0].Id,insertUser.Role__c);
          obj1.processSelected();
          obj1.oppColumnsFlag = true;
          obj1.userColumnsFlag =true;
          searchlist=obj1.getLstWrap();
          obj1.first();
          obj1.last();
          obj1.previous();
          obj1.next();
          OppSearchController obj12=new OppSearchController();
           List<warapperUserAssignment> searchlist1=new List<warapperUserAssignment>();
           warapperUserAssignment wrapObj01=new warapperUserAssignment(opptyList[0]);
           wrapObj01.selected = true;
          searchlist1.add(wrapObj0);
           searchlist1=obj1.getLstWrap();
            
        test.stopTest();
        }
        }
    }