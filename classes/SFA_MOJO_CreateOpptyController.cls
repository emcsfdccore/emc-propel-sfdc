/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  07.02.2014      Sneha Jain     9641      Remove Validation on select of assets from different CPA.
+========================================================================================================================*/
public class SFA_MOJO_CreateOpptyController {
    
    public List<Asset__c> lstSelectedAssets = new List<Asset__c>();
    public List<Opportunity_Asset_Junction__c> lstJnObj = new List<Opportunity_Asset_Junction__c>();  
    public List<Lead_Asset_Junction__c> lstAssetJnObj = new List<Lead_Asset_Junction__c>();  
    public Set<Id> lstCreateOpty = new Set<Id>();    
    public Set<Id> setAssetIds = new Set<Id>();   
    public String accName,accId,accLkid,accdistrict, idListView,CustomUrl,Status ='Closed',ForecastCategoryName = 'Pipeline';    
    public datetime closeDate = datetime.now() + 120;      
    public Integer intActiveOpty {get;set;} {intActiveOpty = 0 ;} 
    public Integer intLinkToRenewalsLead {get;set;} {intLinkToRenewalsLead = 0 ;} 
    public Integer intLinkToCompetitiveLead {get;set;} {intLinkToCompetitiveLead = 0 ;} 
    public Integer intLinkToRefreshLead {get;set;} {intLinkToRefreshLead = 0 ;} 
    public Integer intCompetitiveAssetLinked {get;set;} {intCompetitiveAssetLinked = 0 ;}   
    public Integer intDiffAccount {get;set;} {intDiffAccount = 0;}
    public Integer intErrored {get;set;} {intErrored = 0;}
    public Integer intNoRec {get;set;} {intNoRec = 0;} 
    public Integer intCompetitiveAsset {get;set;} {intCompetitiveAsset = 0;}    
    public String idRecordType,strCloseDate,encodedCloseDate,encodedAccName,strAssetIds,encodedAssetIds,encodedaccLkid,dataMapRetUrl,dataValueMap,encodeddataValueMap; 
    public String AssetIds;
 
    public SFA_MOJO_CreateOpptyController(){}
    public SFA_MOJO_CreateOpptyController(ApexPages.StandardSetController controller) {
        Integer assetCount=0;
        //lstSelectedAssets = controller.getSelected();
        AssetIds = Apexpages.currentPage().getParameters().get('astId');
        system.debug('AssetIds------>'+AssetIds);
        for(String s:AssetIds.split(',')){
            setAssetIds.add(s.trim());
        }
        Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
        string OpptyClosedLostStatus = data.get('OpptyClosedStatus').datavalue__c;
        string R2R_Admin_Profiles = data.get('R2R_Admin_Profiles').datavalue__c;
        String R2R_UserIdsToByPass= data.get('R2R_UserIdsToByPass').datavalue__c;
      
        /*for (Asset__c selctdassets : lstSelectedAssets) 
        {
            setAssetIds.add(selctdassets.id);
        }*/
        system.debug('setAssetIds------>'+setAssetIds);
        lstSelectedAssets = [Select id,name,Product_Name_Vendor__c, Product_Family__c, Customer_Name__r.name,Customer_Name__r.Customer_Profiled_Account__c,Customer_Name__r.Account_District__c,
                             RecordType.DeveloperName,Customer_Name__r.Customer_Profiled_Account_Lookup__r.Name,Customer_Name__r.Customer_Profiled_Account_Lookup__c,
                             Customer_Name__c from Asset__c Where id in :setAssetIds limit 50000];
        lstAssetJnObj = [Select Id,Name,Related_Asset__c,Related_Asset__r.RecordType.DeveloperName,Related_Lead__r.Lead_Type_Based_on_Linking__c from Lead_Asset_Junction__c where Related_Asset__c in :setAssetIds
                         and Related_Lead__r.Status not in ('Closed','Converted To Opportunity')];                 
                       
                                
        user usrObj = [Select Id,Forecast_Group__c from user where id =:userinfo.getuserid()];
        
        if((lstSelectedAssets!=null)&&(lstSelectedAssets.size() > 0))
        {
            for(integer i=0; i<lstSelectedAssets.size(); i++)
            {
                accName = null;
                accId = null;
                accLkid = null;
                accdistrict = null;
                if(lstSelectedAssets[i].Customer_Name__c!=null)
                {
                    System.debug('usrObj.Forecast_Group__c--'+usrObj.Forecast_Group__c);
                  //if user belongs to maintenance renewals
                    if(usrObj.Forecast_Group__c =='Maintenance Renewals' && lstSelectedAssets[i].RecordType.DeveloperName=='Competitive_Install'){
                        intCompetitiveAsset = intCompetitiveAsset+1;
                    }
                    System.Debug('111-->' +lstSelectedAssets[i].RecordType.DeveloperName);
                    System.Debug('111-->' +intCompetitiveAsset);
                  //  if(lstSelectedAssets[i].Customer_Name__r.Customer_Profiled_Account_Lookup__c == null){
                    if(lstSelectedAssets[i].Customer_Name__r.Customer_Profiled_Account__c){
                    System.debug('True--------');
                    accdistrict= lstSelectedAssets[i].Customer_Name__r.Account_District__c;
                    accName = lstSelectedAssets[i].Customer_Name__c;    
                    accId = lstSelectedAssets[i].Customer_Name__r.Name;
                    accLkid = lstSelectedAssets[i].Customer_Name__r.Id;  
                    break;       
                    }
                   // else if(lstSelectedAssets[i].Customer_Name__r.Customer_Profiled_Account_Lookup__c != null){
                    else if(!lstSelectedAssets[i].Customer_Name__r.Customer_Profiled_Account__c ){
                    System.debug('false--------');
                  /*  accName = lstSelectedAssets[i].Customer_Name__r.Customer_Profiled_Account_Lookup__c ;    
                    accId = lstSelectedAssets[i].Customer_Name__r.Customer_Profiled_Account_Lookup__r.Name;
                    accLkid = lstSelectedAssets[i].Customer_Name__r.Customer_Profiled_Account_Lookup__c;         */
                    
                    accdistrict= lstSelectedAssets[i].Customer_Name__r.Account_District__c;
                    accName = lstSelectedAssets[i].Customer_Name__c;    
                    accId = lstSelectedAssets[i].Customer_Name__r.Name;
                    accLkid = lstSelectedAssets[i].Customer_Name__r.Id; 
                    if(lstSelectedAssets[i].Customer_Name__r.Customer_Profiled_Account_Lookup__c != null){
                          assetCount =assetCount+1;
                    }
                    
                    }
                     if(assetCount >= 2){
                        System.debug('false--------');
                        accdistrict= lstSelectedAssets[i].Customer_Name__r.Account_District__c;
                        accName = lstSelectedAssets[i].Customer_Name__r.Customer_Profiled_Account_Lookup__c ;    
                        accId = lstSelectedAssets[i].Customer_Name__r.Customer_Profiled_Account_Lookup__r.Name;
                        accLkid = lstSelectedAssets[i].Customer_Name__r.Customer_Profiled_Account_Lookup__c;       
                    
                    }
                 
                }
            }           
            for(integer j=0; j<lstSelectedAssets.size(); j++)
            {
                if(accdistrict!=lstSelectedAssets[j].Customer_Name__r.Account_District__c)
                {                   
                    intDiffAccount++;
                }
                else
                {
                    lstCreateOpty.add(lstSelectedAssets[j].id);                 
                }
            }
            
        }
        else
        {
            intNoRec++;         
        }           
        if((setAssetIds!=null)&&(setAssetIds.size() > 0))
        {
            lstJnObj = [select id,Asset_Record_Type_ID__c,Opportunity_Forecast_Status__c,Opportunity_Close_Date__c,Related_Asset__c,Related_Asset__r.id,Related_Opportunity__c,
                        Related_Opportunity__r.Opportunity_Type__c,Related_Opportunity__r.StageName,Record_Type_Name__c,Related_Asset__r.RecordType.DeveloperName from Opportunity_Asset_Junction__c where Related_Asset__r.id in:setAssetIds limit 50000]; 
                            
            
            if((lstJnObj!=null)&&(lstJnObj.size()>0))
            {               
                for(Opportunity_Asset_Junction__c j: lstJnObj)
                {   
                    System.Debug('OpptyClosedLostStatus--->' +OpptyClosedLostStatus);
                    System.Debug('junction--->' +j.Related_Opportunity__r.StageName);
                    
                    if(j.Opportunity_Forecast_Status__c.contains(Status))
                    {
                        lstCreateOpty.add(j.Related_Asset__r.id);                        
                    }                    
                    if((j.Related_Opportunity__r.Opportunity_Type__c =='Renewals' && usrObj.Forecast_Group__c =='Maintenance Renewals')
                             && j.Related_Asset__r.RecordType.DeveloperName=='EMC_Install'&& !OpptyClosedLostStatus.contains(j.Related_Opportunity__r.StageName) 
                             && (!R2R_Admin_Profiles.contains(userinfo.getProfileId()) || !R2R_UserIdsToByPass.contains(userinfo.getUserId())))  
                           
                    {                        
                        intActiveOpty++;
                    }
                    System.Debug('j.Related_Opportunity__r.Opportunity_Type__c--->' +j.Related_Opportunity__r.Opportunity_Type__c);
                    System.Debug('j.Related_Opportunity__r.StageName --->' +j.Related_Opportunity__r.StageName);
                    System.Debug('j.Record_Type_Name__c--->' +j.Record_Type_Name__c);
                    System.debug('!OpptyClosedLostStatus.contains(j.Related_Opportunity__r.StageName'+(!OpptyClosedLostStatus.contains(j.Related_Opportunity__r.StageName)));
                    if(!OpptyClosedLostStatus.contains(j.Related_Opportunity__r.StageName) && (!R2R_Admin_Profiles.contains(userinfo.getProfileId()) || !R2R_UserIdsToByPass.contains(userinfo.getUserId()))&&(((j.Related_Opportunity__r.Opportunity_Type__c =='Swap' ||
                    j.Related_Opportunity__r.Opportunity_Type__c=='Refresh/Swap')
                    && j.Related_Asset__r.RecordType.DeveloperName=='Competitive_Install')||
                    
                        (((j.Related_Opportunity__r.Opportunity_Type__c =='Refresh' && usrObj.Forecast_Group__c !='Maintenance Renewals') ||
                        (j.Related_Opportunity__r.Opportunity_Type__c=='Refresh/Swap') ) 
                        && j.Related_Asset__r.RecordType.DeveloperName=='EMC_Install'))
                         )           
                           
                    {    
                        System.Debug('OpptyClosedLostStatus111--->' +OpptyClosedLostStatus);
                        System.Debug('junction111--->' +j);
                        System.Debug('OpptyClosedLostStatus111--->' +OpptyClosedLostStatus);
                        System.Debug('junction111--->' +j.Related_Opportunity__r.StageName);
                        intCompetitiveAssetLinked++;
                        System.Debug('intCompetitiveAssetLinkedinside--->' +intCompetitiveAssetLinked);
                    }
                        System.Debug('intCompetitiveAssetLinkedoutside--->' +intCompetitiveAssetLinked);
                    
                }                                       
            } 
            if(lstAssetJnObj !=null && lstAssetJnObj.size()>0){
                for(Lead_Asset_Junction__c juncObj :lstAssetJnObj){
                    if(juncObj.Related_lead__r.Lead_Type_Based_on_Linking__c.contains('Renewals') && usrObj.Forecast_Group__c =='Maintenance Renewals'){
                        intLinkToRenewalsLead++;
                    }
                    if(juncObj.Related_lead__r.Lead_Type_Based_on_Linking__c.contains('Refresh') && usrObj.Forecast_Group__c !='Maintenance Renewals'){
                        intLinkToRefreshLead++;
                    }
                    if(juncObj.Related_Asset__r.RecordType.DeveloperName=='Competitive_Install'){
                        intLinkToCompetitiveLead++;
                    }
                }
            }           
            else
            {               
                for(Asset__c noreltdopty: lstSelectedAssets)
                {
                    lstCreateOpty.add(noreltdopty.id);
                }
            }
                    
        }
        System.debug('intActiveOpty@@@@@@@@@@'+intActiveOpty );
        System.debug('intDiffAccount @@@@@@@@@@@@@'+intActiveOpty );
        System.debug('intCompetitiveAsset @@@@@@@@@@@@@@@@@@'+intCompetitiveAsset);
        System.debug('intCompetitiveAssetLinked @@@@@@@@@@@@@@@'+intCompetitiveAssetLinked );
        System.debug('intLinkToRenewalsLead @@@@@@@@@@@@@@@@@@'+intLinkToRenewalsLead );
        System.debug('intLinkToRefreshLead @@@@@@@@@@@@@@@'+intLinkToRefreshLead);
        System.debug('intLinkToRefreshLead @@@@@@@@@@@'+intLinkToCompetitiveLead);
        
        intErrored = intActiveOpty + intDiffAccount +intCompetitiveAsset+intCompetitiveAssetLinked +intLinkToRenewalsLead +intLinkToRefreshLead+intLinkToCompetitiveLead;                        
        
        System.debug('intErrored @@@@@@@@@@'+intErrored );
        strAssetIds = '';                      
        for(Id assetid: lstCreateOpty)
        {           
            strAssetIds += assetid + ',' ;
        }
        // remove last additional comma from string
        
        if((strAssetIds!= null) &&(strAssetIds!= ''))
        {
            strAssetIds = strAssetIds.subString(0,strAssetIds.length()-1);          
        }
        
        
        dataValueMap  = data.get('Asset Ids field').DataValue__c ;
        dataMapRetUrl = data.get('Return URL in Asset').DataValue__c ;         

        
    }
    
     public void ValidateOpty(){
        System.Debug('22'+intCompetitiveAsset );
        System.Debug('22'+intCompetitiveAssetLinked );
        //Commenting the Validation Error message of Assets with nul District. Now the logic is calculated on Global Ultimate - 9641
        /*if(accdistrict == null || accdistrict == ''){
               ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Assets_Without_Account_District);
               ApexPages.addMessage(msg);
               intErrored++;
        }*/
        if(intCompetitiveAsset !=0)
        {
          ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.Competetive_Asset_can_Not_Be_Linked);
          ApexPages.addMessage(msg);
        }
        if(intNoRec!= 0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.No_Record_Selected_for_Create_Opportunity);
            ApexPages.addMessage(msg);
        }
        
        if(intActiveOpty!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Link_Already_Linked_1);
            ApexPages.addMessage(msg);
        }
        
        //Commenting the Validation Error message of Assets belonging to different CPA. Now the logic is calculated on Global Ultimate - 9641
        /*if(intDiffAccount!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Link_Not_Same_CPA);
            ApexPages.addMessage(msg);
        }*/
        if(intCompetitiveAssetLinked!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Link_Already_Linked_2);
            ApexPages.addMessage(msg);
        }
        if(intLinkToRenewalsLead!=0 || intLinkToRefreshLead!=0 || intLinkToCompetitiveLead!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Linked_To_Oppty);
            ApexPages.addMessage(msg);
        }
        
        
    }
    
   public PageReference CreateOpty(){
        
        ValidateOpty(); 
        if((intErrored<=0)&&(intNoRec<=0))
        {                              
            if(lstCreateOpty!=null)
            {               
                idListView = Apexpages.currentPage().getParameters().get('fcf');
                idRecordType = [select id from recordtype where Name = 'New Manually Created Record Type' and SobjectType = 'Opportunity' limit 1].Id;
                if((accId!=null)&&(accLkid!=null))
                {
                    encodedAccName = EncodingUtil.urlEncode(accId, 'UTF-8');
                    encodedaccLkid = EncodingUtil.urlEncode(accLkid, 'UTF-8');
                }
                if(strAssetIds!=null)
                {
                    encodedAssetIds =  EncodingUtil.urlEncode(strAssetIds, 'UTF-8');
                }
                if(dataValueMap!=null)
                {
                    encodeddataValueMap = EncodingUtil.urlEncode(dataValueMap, 'UTF-8');
                }
                strCloseDate = closeDate.format('MM/dd/yyyy') ;                         
                encodedCloseDate = EncodingUtil.urlEncode(strCloseDate, 'UTF-8');   
                 
                if((idListView!=null)&&(idRecordType!=null)&&(encodedAccName!=null)&&(encodedaccLkid!=null)&&(ForecastCategoryName!=null)&&(encodedCloseDate!=null)&&(encodedAssetIds!=null)&&(encodeddataValueMap!=null))
                {                   
                    CustomUrl= '/006/e?retURL='+dataMapRetUrl+idListView+'&RecordType='+idRecordType+'&ent=Opportunity&opp4='+encodedAccName+'&opp4_lkid='+encodedaccLkid+'&opp11='+ForecastCategoryName+'&opp9='+encodedCloseDate+'&'+encodeddataValueMap+'='+encodedAssetIds;               
                    System.debug('CustomUrl............'+CustomUrl);
                    return new PageReference(CustomUrl);
                }
                else if(idListView==null){
                    CustomUrl= '/006/e?retURL='+dataMapRetUrl+'&RecordType='+idRecordType+'&ent=Opportunity&opp4='+encodedAccName+'&opp4_lkid='+encodedaccLkid+'&opp11='+ForecastCategoryName+'&opp9='+encodedCloseDate+'&'+encodeddataValueMap+'='+encodedAssetIds;               
                    System.debug('CustomUrl............'+CustomUrl);
                    return new PageReference(CustomUrl);
                }
           
            }
        }
        //Remove Validation on select of assets from different CPA. If Assets are from different CPA, Account on New oppty should not be populated - 9641
        else if(intNoRec <=0 && intActiveOpty<=0 && intCompetitiveAsset <=0 && intCompetitiveAssetLinked <=0 && intLinkToRenewalsLead <=0 &&intLinkToRefreshLead <=0 && intLinkToCompetitiveLead <=0 && intDiffAccount!=0)
        {
            system.debug('----------Inside different CPA but no error code------');
            if(lstCreateOpty!=null)
            {               
                idListView = Apexpages.currentPage().getParameters().get('fcf');
                idRecordType = [select id from recordtype where Name = 'New Manually Created Record Type' and SobjectType = 'Opportunity' limit 1].Id;
                if(strAssetIds!=null)
                {
                    encodedAssetIds =  EncodingUtil.urlEncode(strAssetIds, 'UTF-8');
                }
                if(dataValueMap!=null)
                {
                    encodeddataValueMap = EncodingUtil.urlEncode(dataValueMap, 'UTF-8');
                }
                strCloseDate = closeDate.format('MM/dd/yyyy') ;                         
                encodedCloseDate = EncodingUtil.urlEncode(strCloseDate, 'UTF-8');   
                 
                if((idListView!=null)&&(idRecordType!=null)&&(ForecastCategoryName!=null)&&(encodedCloseDate!=null)&&(encodedAssetIds!=null)&&(encodeddataValueMap!=null))
                {                   
                    CustomUrl= '/006/e?retURL='+dataMapRetUrl+idListView+'&RecordType='+idRecordType+'&ent=Opportunity'+'&opp11='+ForecastCategoryName+'&opp9='+encodedCloseDate+'&'+encodeddataValueMap+'='+encodedAssetIds;               
                    System.debug('CustomUrl............'+CustomUrl);
                    return new PageReference(CustomUrl);
                }
                else if(idListView==null)
                {
                   return null;
                }
           
            }
        }
        return null;
    }   
}