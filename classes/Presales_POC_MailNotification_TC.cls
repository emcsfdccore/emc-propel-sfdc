/*========================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER                WR       DESCRIPTION                               
 |  ====          =========                ==       =========== 
 |  09/30/2013    Sayan Choudhury          279601   Created for Presales POC notification Class  
 |  20/01/2014    Bisna V P                CI:296   Updated the code to cover the to addresses for the email notification         
 +=======================================================================================================*/
 
@isTest(SeeAllData=true)
public class Presales_POC_MailNotification_TC{   

public static List<Case> Caselst = new List<Case>();
public void createCase(){
List<Contact> contactLSt = new List<Contact>();
RecordType recList = [Select Id From RecordType  Where SobjectType = 'Case' and DeveloperName = 'Presales_Proof_of_Concept'];

List<Account> lstaccount = new List<Account>();
        Account account1 = new Account();
        account1.name = 'Test';
        account1.Partner_Type__c='ISV';
        lstaccount.add(account1);
        insert lstaccount;
        Opportunity Oppty = new Opportunity();
        Oppty.AccountId = account1.Id;
        Date closeDate = date.today()+15;
        Date approvalDate = date.today();
        Oppty.amount= 10000;
        Date expirationDate = date.today()+30;
        Oppty.Name = 'Test Oppty';
        Oppty.Sales_Channel__c = 'Direct';
        Oppty.Sales_Force__c = 'EMC';
        Oppty.StageName = 'Pipeline';
        Oppty.Closed_Reason__c = 'Loss';
        Oppty.Close_Comments__c = 'Lost';
        Oppty.CloseDate = closeDate;
       Oppty.Sell_Relationship__c = 'Direct';
        Oppty.Quote_Version__c='v13';
        Oppty.Quote_Type__c='New';
        Oppty.Approval_Date__c=approvalDate ;
        Oppty.Expiration_Date__c=expirationDate ;
         insert Oppty;
         
         
         List<Presales_POC_SolutionCenter__c> custList = new List<Presales_POC_SolutionCenter__c>();
         List<Presales_POC_Solution_Mails__c> custListEmail = new List<Presales_POC_Solution_Mails__c>();
         
         for(Integer cust = 0 ; cust <=9 ; cust ++){
         
            Presales_POC_SolutionCenter__c custObj = new Presales_POC_SolutionCenter__c();
            Presales_POC_Solution_Mails__c custObjEmail = new Presales_POC_Solution_Mails__c();
            if(cust==0){
            custObj.name = 'Beijing Solutions Center';
            custObjEmail.name='Beijing Solutions Center';
            custObjEmail.Solution_Center_Emails__c='a.b1@c.com';
            }
            if(cust==1){
            custObj.name = 'Chengdu Solutions Center';
            custObjEmail.name='Chengdu Solutions Center';
            custObjEmail.Solution_Center_Emails__c='a.b2@c.com';
            }
            if(cust==2){
            custObj.name = 'Cork Solutions Center';
            custObjEmail.name='Cork Solutions Center';
            custObjEmail.Solution_Center_Emails__c='a.b3@c.com';
            }
            if(cust==3){
            custObj.name = 'Guangzhou Solutions Center';
            custObjEmail.name='Guangzhou Solutions Center';
            custObjEmail.Solution_Center_Emails__c='a.b4@c.com';
            }
            if(cust==4){
            custObj.name = 'Hong Kong Solutions Center';
            custObjEmail.name='Hong Kong Solutions Center';
            custObjEmail.Solution_Center_Emails__c='a.b5@c.com';
            }
            if(cust==5){
            custObj.name = 'Hopkinton Solutions Center';
            custObjEmail.name='Hopkinton Solutions Center';
            custObjEmail.Solution_Center_Emails__c='a.b6@c.com';
            }
            if(cust==6){
            custObj.name = 'POC ASD Group Global';
            custObjEmail.name='POC ASD Group Global';
            custObjEmail.Solution_Center_Emails__c='a.b7@c.com';
            }
            if(cust==7){
            custObj.name = 'Shanghai Solutions Center';
            custObjEmail.name='Shanghai Solutions Center';
            custObjEmail.Solution_Center_Emails__c='a.b8@c.com';
            }
            if(cust==8){
            custObj.name = 'Singapore Solutions Center';
            custObjEmail.name='Singapore Solutions Center';
            custObjEmail.Solution_Center_Emails__c='a.b9@c.com';
            }
            if(cust==9){
            custObj.name = 'Taiwan Solutions Center';
            custObjEmail.name='Taiwan Solutions Center';
            custObjEmail.Solution_Center_Emails__c='a.b10@c.com';
            }
            custList.add(custObj);
            custListEmail.add(custObjEmail);
         
         }
         
         Map<String , Presales_POC_SolutionCenter__c> mapSolution= Presales_POC_SolutionCenter__c.getAll();
         Map<String , Presales_POC_Solution_Mails__c> mapSolutionEmail= Presales_POC_Solution_Mails__c.getAll();
         
         if(mapSolution.isEmpty() && !custList.isEmpty()){
         
            insert custList;
         
         }
         if(mapSolutionEmail.isEmpty() && !custListEmail.isEmpty()){
         
            insert custListEmail;
         
         }
         
         List<Contact> poccontacts= new List<Contact>();
         
         if(!mapSolution.isEmpty()){
            contactLSt = [Select id,name,email from Contact where Presales_POC_Contact__c  = true and email!=null and name in :mapSolution.keySet() ];      

            }
         
         for(Integer k=0; k<=9;k++){
              Contact c= new Contact();
              c.Presales_POC_Contact__c= true;
         if(k==0){                     
             c.FirstName='Beijing';
             c.LastName = 'Solutions Center';
             c.Email='GSC.Beijing@emc.com';             
         }
         if(k==1){                     
             c.FirstName='Chengdu';
             c.LastName = 'Solutions Center';
             c.Email='GSC.Chengdu@emc.com';            
         }
         if(k==2){                     
             c.FirstName='Cork';
             c.LastName = 'Solutions Center';
             c.Email='GSC.Cork@emc.com';            
         }
         if(k==3){                     
             c.FirstName='Guangzhou';
             c.LastName = 'Solutions Center';
             c.Email='GSC.Guangzhou@emc.com';            
         }
         if(k==4){                     
             c.FirstName='Hong Kong';
             c.LastName = 'Solutions Center';
             c.Email='GSC.HongKong@emc.com';            
         }
         if(k==5){                     
             c.FirstName='Hopkinton';
             c.LastName = 'Solutions Center';
             c.Email='GSC.Hopkinton@emc.com';            
         }
         if(k==6){                     
             c.FirstName='Shanghai';
             c.LastName = 'Solutions Center';
             c.Email='GSC.Shanghai@emc.com';            
         }
         if(k==7){                     
             c.FirstName='Singapore';
             c.LastName = 'Solutions Center';
             c.Email='GSC.Singapore@emc.com';            
         }
         if(k==8){                     
             c.FirstName='Taiwan';
             c.LastName = 'Solutions Center';
             c.Email='GSC.Taiwan@emc.com';            
         }
         if(k==9){                     
             c.FirstName='POC ASD';
             c.LastName = 'Group Global';
             c.Email='ASD.SFDC.POC@emc.com';            
         }
         poccontacts.add(c);
        
         }
         
         if(poccontacts.size()>0 && contactLSt.size()<0){
         insert poccontacts;         
         }
        
for(Integer i=0 ; i<=9 ; i++){

    Case caseObj= new Case();
    
    caseObj.RecordTypeId=recList.id;
    caseObj.Opportunity_Name__c = Oppty.id;


    if(i==0){
    
    
   
   caseObj.Solutions_Center__c='Beijing Solutions Center';
   caseObj.Proof_Point__c='aksdkad';
   caseObj.Approving_DVP_Country_Manager__c='sajfgsluhg';
   caseObj.POC_Support_Type__c='Partner Enablement';
   
    }
    if(i==1){  
    caseObj.Solutions_Center__c='Chengdu Solutions Center';
    caseObj.Proof_Point__c='ksdhfksh';
    caseObj.Approving_DVP_Country_Manager__c='ksdfhhasdf';
    caseObj.POC_Support_Type__c='Partner- Sales Support';

    }
    if(i==2){  
    caseObj.Solutions_Center__c='Guangzhou Solutions Center';
    caseObj.Proof_Point__c='oeiurtou';
    caseObj.Approving_DVP_Country_Manager__c='oiryrtss';
    caseObj.POC_Support_Type__c='Partner- Sales Support';
    
    
  }
  if(i==3){  
    caseObj.Solutions_Center__c='Cork Solutions Center';
    caseObj.Proof_Point__c='ksdhf';
    caseObj.Approving_DVP_Country_Manager__c='kshhasdf';
    caseObj.POC_Support_Type__c='Partner Enablement';
    
    }
    if(i==4){  
    caseObj.Solutions_Center__c='Hong Kong Solutions Center';
    caseObj.Proof_Point__c='ksdhsfgf';
    caseObj.Approving_DVP_Country_Manager__c='kshhasdf';
    caseObj.POC_Support_Type__c='Partner Enablement';
    
    }
    if(i==5){  
    caseObj.Solutions_Center__c='Hopkinton Solutions Center';
    caseObj.Proof_Point__c='ksdhuyiof';
    caseObj.Approving_DVP_Country_Manager__c='kshhasfssdf';
    caseObj.POC_Support_Type__c='EMC- Direct Sales Support';
    
    }
    if(i==6){  
    caseObj.Solutions_Center__c='POC ASD Group Global';
    caseObj.Proof_Point__c='ritysdhf';
    caseObj.Approving_DVP_Country_Manager__c='fdjhdfdg';
    caseObj.POC_Support_Type__c='Partner Enablement';
    
    }
    if(i==7){  
    caseObj.Solutions_Center__c='Shanghai Solutions Center';
    caseObj.Proof_Point__c='portydhf';
    caseObj.Approving_DVP_Country_Manager__c='kshhrtyasdf';
    caseObj.POC_Support_Type__c='EMC- Direct Sales Support';
    
    }
    
    if(i==8){  
    caseObj.Solutions_Center__c='Singapore Solutions Center';
    caseObj.Proof_Point__c='portydyyweqhf';
    caseObj.Approving_DVP_Country_Manager__c='asdrtyasdf';
    caseObj.POC_Support_Type__c='Partner- Sales Support';
    
    }
    if(i==9){  
    caseObj.Solutions_Center__c='Taiwan Solutions Center';
    caseObj.Proof_Point__c='iurtydhf';
    caseObj.Approving_DVP_Country_Manager__c='nbvnbhrtyasdf';
    caseObj.POC_Support_Type__c='EMC- Direct Sales Support';
    }
    
    
   Caselst.add(caseObj);
} 
    
  insert Caselst;

}

 static testMethod void TestsendPOCMail(){ 
   Test.startTest();
  Presales_POC_MailNotification_TC objTC = new Presales_POC_MailNotification_TC();
  objTC.createCase();
  System.debug('case TC');
  Presales_POC_MailNotification classObj = new Presales_POC_MailNotification();
  classObj.sendPOCMail(Caselst);
 Test.stopTest();
 
 
 }
 }