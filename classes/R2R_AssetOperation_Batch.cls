/*=====================================================================================================+

|  HISTORY  |                                                                            

|  DATE          DEVELOPER                WR            DESCRIPTION                               

|  ====          =========                ==            =========== 
                                                       
| 30/07/2013    Anirudh Singh   Asset Management  This class is used to set the Sales Plan on Competitive Assets.                        
| 26/02/2014    Sneha Jain  Asset Management    Limit the batch job to run only for Competitive Assets FR 9706
| 23/04/2014    Sneha Jain  Asset Management    Include EMC Install records also for batch job                                                      
+=====================================================================================================*/

public class R2R_AssetOperation_Batch implements Database.Batchable<SObject> {
    
    public string batchName;    
    public string batchQuery;  
    
    String competitiveRT = 'Competitive Install';
    String emcInstallRT = 'EMC Install';
    
    public R2R_AssetOperation_Batch(){
        batchName = 'Asset Sales Strategy Batch: ' + Datetime.now();       
        Datetime dt = System.now();
        
        //Limit the batch job to run only for Competitive Assets FR 9706
        /*batchQuery = 'select id,Sales_Plan__c,RedZone_Competitive__c '+ 
                                'from Asset__c '+
                                'where (Target_Zone__c =\'Target\'  and Sales_Plan__c=\'\' and RecordTypeId != \'' + deInstallRecord + '\') or '+ 
                                '(Target_Zone__c !=\'Target\'  and Sales_Plan__c=\'Action Required\')';
        */     
        //Include EMC Install Assets with Contract Termination in Progress field unchecked 
        batchQuery = 'select id,Sales_Plan__c,RedZone_Competitive__c '+ 
                                'from Asset__c '+
                                'where ((((Target_Zone__c =\'Target\'  and Sales_Plan__c=\'\') or (Target_Zone__c !=\'Target\'  and Sales_Plan__c=\'Action Required\')) and RecordType.Name = \'' + competitiveRT + '\') OR (((Target_Zone__c =\'Target\'  and Sales_Plan__c=\'\' and Contract_Termination_in_Progress__c != true) or (Target_Zone__c !=\'Target\'  and Sales_Plan__c=\'Action Required\')) and RecordType.Name = \'' + emcInstallRT + '\'))';


        system.debug(batchQuery);      
    }
    
    /*@Method <This method gets executed acutomatically when the batch job is started.>
    @param <Database.BatchableContext BC - Batchable context>
    @return <void> - <Not returning anything>
    @throws exception - <No Exception>
    */
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(batchQuery);
    }
    
    /*@Method <This method gets executed acutomatically when the batch job is execution mode.>
    @param <Database.BatchableContext BC - Batchable context>
    @param <List<Sobject> scope - scope>
    @return <void> - <Not returning anything>
    @throws exception - <No Exception>
    */
    public void execute(Database.BatchableContext BC, list<SObject> scope){
        
        List<Asset__c> lstAssetToUpdate=(Asset__c[]) scope;
        if(lstAssetToUpdate.size()>0){
            for(Asset__c assetObj :lstAssetToUpdate){
                if(assetObj.Sales_Plan__c==null || assetObj.Sales_Plan__c==''){
                    assetObj.Sales_Plan__c ='Action Required';
                }
                else{
                    assetObj.Sales_Plan__c='';
                }
            }
        }
    
       update lstAssetToUpdate;
    }
    
  

    
    /*@Method <This method gets executed acutomatically when the batch job is finised. We are deleting the job at the end.>
@param <Database.BatchableContext BC - Batchable context>
@return <void> - <Not returning anything>
@throws exception - <No Exception>
*/
    public void finish(Database.BatchableContext BC){
        
       
       
       
    }   
}