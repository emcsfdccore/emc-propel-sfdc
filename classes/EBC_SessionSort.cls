/*=====================================================================================+
 | HISTORY                                                                             |
 |                                                                                     |
 | DATE        DEVELOPER   WR      DESCRIPTION                                         |
 | ==========  ==========  ======  ===========                                         |
 | 08/27/2013  Y. Salazar  286622  Global class for Comparable interface, adds sorting |
 |                                 support for Lists that contain non-primitive types, |
 |                                 that is, Lists of user-defined types. 
 | 06/12/2014  Abinay M S  CI:269  Modified constructor argument to capture the Briefing
                                   Center TimeZone.
 +====================================================================================*/

global class EBC_SessionSort implements Comparable{
  public Datetime timeAux{get;set;}
  public String startTime {get;set;}
  public String endTime {get;set;}
  public String topic {get;set;}
  public String notes {get;set;}
  public List<EBC_Session_Presenter__c> presenters {get;set;}
  public String sDate{get;set;}
   // CI:269 Modified constructor argument to capture the Briefing Center TimeZone.
  public EBC_SessionSort(EBC_Session__c s, List<EBC_Session_Presenter__c> p, string timeZone){
    timeAux = s.Session_Start_Time__c;
    startTime = (String.valueOf(s.Session_Start_Time__c.format('hh:mm a', timeZone)));
    endTime = (String.valueOf(s.Session_End_Time__c.format('hh:mm a', timeZone)));
    topic = s.Topic_Override__c;
    notes = s.Requester_Notes__c;
    sDate = (String.valueOf(s.Session_Start_Time__c.format('MM/dd/yyyy ')));
    
    presenters = new List<EBC_Session_Presenter__c>();
    for(EBC_Session_Presenter__c pres:p){
      if(pres.Session__c == s.Id){
        presenters.add(pres);
      }
    }
  }
  
  public Integer compareTo(Object compareTo){
    EBC_SessionSort compareToSess = (EBC_SessionSort)compareTo;
    if (timeAux== compareToSess.timeAux ) return 0;
    if (timeAux > compareToSess.timeAux ) return 1;
    return -1;      
  }
}