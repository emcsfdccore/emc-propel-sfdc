/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER          WR        DESCRIPTION                               

 |  ====          =========          ==        =========== 

 |  03/03/2014    Bhanu Prakash                This class is a util class used for triggers controller classes. 
 |  03/24/2014    Bhanu Prakash                Modified  as per Dennis Code review comments.
 |  04/03/2014    Shalabh Sharma               Added method sendEmail to send emails to partners
 |  06/02/2014    Shalabh Sharma               Updated code to flip Shared to Partner to Yes on partner approval
 |  06/03/2014    Shalabh Sharma               Updated code to add Additional Contact Email fields in cc for partner notifications
 |  23 July 2014    Avinash K                  Updated code to include different From addresses for Partner 
                                                acceptance emails based on Theater of Oppty Reg record
 |  11/Sep/2014    Aagesh Jose        1229      -Updated to add cc mails and to update email fields in opportunity     
 |  13-Jan-2015    Vinod Jetti       CI#1641    Update added fields in the opportunity
 |  07-April-2015  Vinod Jetti       CI#1825    Updated to add 'Is_this_an_EHC_deal__c' field
 |  08/11/2014    Bhanu Prakash     PROPEL     Added code to update "Opportunity_Registration_Nbr__c" field in Opportunity.               
 +==================================================================================================================**/
public without sharing class OPPTY_COLLABORATION_Util{
    public static Boolean isExecuted = false;
  
    /*
    | Method Name : shareOptty
    | @param      : List<Opportunity_Registration__c>
    | Description : This method used to share opportunity record with partners on Opportunity registration 
    |               partners will can see Opportunity record with visibility 'Read'
    | Exception : No Exception
    */
    public static void shareOptty(List<Opportunity_Registration__c> lstOpptys){
        List<OpportunityTeamMember> oppteams = new List<OpportunityTeamMember> ();
        OptyCollab_Custom_Setting__c opptyCustObj = OptyCollab_Custom_Setting__c.getValues('opptyTeamValues');
        OpportunityTeamMember opteam=null;
        system.debug('lstOpptys--->'+lstOpptys);
        for( Opportunity_Registration__c optyReg : lstOpptys){
            if(optyReg.Approving_Partner_User__c!=null && optyReg.Status__c.equals('Submitted to Partner')){
                opteam = new OpportunityTeamMember(
                OpportunityId = optyReg.Related_Opportunity__c,
                UserId = optyReg.Approving_Partner_User__c,
                Added_Through__c=opptyCustObj.addedThrough__c,
                Reason_for_Addition__c=opptyCustObj.reasonForAddition__c,
                TeamMemberRole='Approving Partner User'
                );
                oppteams.add(opteam); 
            }
            if(optyReg.Secondary_Partner_User_lookup__c!=null && optyReg.Status__c.equals('Submitted to Partner')){
                opteam = new OpportunityTeamMember(
                OpportunityId = optyReg.Related_Opportunity__c,
                UserId = optyReg.Secondary_Partner_User_lookup__c,
                Added_Through__c=opptyCustObj.addedThrough__c,
                Reason_for_Addition__c=opptyCustObj.reasonForAddition__c,
                TeamMemberRole='Secondary Partner User');
                oppteams.add(opteam);
            }
        }
        if (!oppteams.isEmpty()){
            Database.SaveResult[] res = database.insert(oppteams);
        }
    }
    
     /*
    | Method Name : cancelOpptySharing
    | @param      : List<Opportunity_Registration__c>
    | Description : This method removes partners from OpportunityShare object so related partners will loose the visibility with opportunity
    | Exception : No Exception  
    */
   public static void cancelOpptySharing(List<Opportunity_Registration__c> lstOptyRegs){
        List<OpportunityTeamMember> oppteams = new List<OpportunityTeamMember>();
        List<Id> accRemovUsers = new List<Id>();
        Set<Id> optyIds = new Set<Id>();
        for( Opportunity_Registration__c optyReg : lstOptyRegs){
                optyIds.add(optyReg.Related_Opportunity__c);
            
            if(optyReg.Approving_Partner_User__c!=null){
                accRemovUsers.add(optyReg.Approving_Partner_User__c);
            }
            if(optyReg.Secondary_Partner_User_lookup__c!=null){
                accRemovUsers.add(optyReg.Secondary_Partner_User_lookup__c);
            }  
        }
        if(!optyIds.isEmpty()){
            oppteams = [SELECT Id FROM OpportunityTeamMember WHERE OpportunityId in: optyIds and user.profile.UserType='PowerPartner' LIMIT 500];
           //oppteams = [SELECT Id FROM OpportunityTeamMember WHERE OpportunityId in: optyIds];
        }
        if (!oppteams.isEmpty()){
          Database.DeleteResult[] res = database.delete(oppteams);
        }
   }
  
    /*
    | Method Name : updateRegOnOppty
    | @param      : List<Opportunity_Registration__c>
    | Description : This method updates opportunity fields on the event of Opportunity registration object Creation/update.
    | Exception   : No Exception    
    */
    public void updateRegOnOppty(List<Opportunity_Registration__c> lstOpptyReg){
        List<Opportunity> lstOppty = new List<Opportunity>();
        Map<Id,Opportunity_Registration__c> mapOpptyReg = new Map<Id,Opportunity_Registration__c>();
        Map<Id,User> mapUser = new Map<Id,User>();
        Set<Id> setUser = new Set<Id>();
        //Create Map of opportunities from oppty reg. records
        for(Opportunity_Registration__c objReg : lstOpptyReg){
            mapOpptyReg.put(objReg.Related_Opportunity__c,objReg);
            setUser.add(objReg.PSC_Approving_User__c);
            setUser.add(objReg.Partner_Approving_User__c);
            setUser.add(objReg.Approving_Partner_User__c);
            setUser.add(objReg.Secondary_Partner_User_lookup__c);
            setUser.add(objReg.CreatedById);
        }
        // Fetch user details
        if(setUser.size()>0){
            mapUser = new Map<Id,User>([Select Id,Name, Email from User where Id in:setUser LIMIT 100]);
        }
        //Bhanu - added "Opportunity_Registration_Nbr__c" field in the query.
        //Get all Opptys related to oppty reg records
        Map<Id,Opportunity> mapOppty = new Map<Id,Opportunity>([Select Id,Registration_Type__c,Partner_Rejection_Reason__c,Opportunity_Registration_Creator__c,Partner_Approving_User_Email__c,Approving_Partner_User1__c,Shared_to_Partner__c,Cancellation_Comments__c,Cancellation_Reason__c,
                                        Opportunity_Registration_Status__c,Partner_Approval_Date_Time__c,Partner_Approval_Status__c,PSC_Approval_Date_Time__c,
                                        PSC_Approval_Status__c,Secondary_Partner_User1__c,Partner_Expected_Close_Date__c,Partner_Expected_Deal_Value__c,PSC_Approving_User__c,Partner_Approving_User__c,Opportunity_Registration_Nbr__c from Opportunity where Id IN: mapOpptyReg.keyset() LIMIT 5000]);
        //Update oppty fields based on relate oppty reg. record values 
        for(Opportunity oppty : mapOppty.values()){
            if( mapOpptyReg.get(oppty.Id).Approving_Partner_User__c != null && mapUser.containsKey(mapOpptyReg.get(oppty.Id).Approving_Partner_User__c)){
                oppty.Approving_Partner_User1__c = mapUser.get(mapOpptyReg.get(oppty.Id).Approving_Partner_User__c).Name;
            }
            else{
                oppty.Approving_Partner_User1__c = null;
            }
            oppty.Cancellation_Comments__c = mapOpptyReg.get(oppty.Id).Cancellation_Comments__c;
            oppty.Cancellation_Reason__c = mapOpptyReg.get(oppty.Id).Cancellation_Reason__c;
            oppty.Opportunity_Registration_Status__c = mapOpptyReg.get(oppty.Id).Status__c;
            oppty.Partner_Approval_Date_Time__c = mapOpptyReg.get(oppty.Id).Partner_Approval_Date_Time__c;
            oppty.Partner_Approval_Status__c = mapOpptyReg.get(oppty.Id).Partner_Approval_Status__c;
            oppty.PSC_Approval_Date_Time__c = mapOpptyReg.get(oppty.Id).PSC_Approval_Date_Time__c;
            oppty.Partner_Expected_Deal_Value__c = mapOpptyReg.get(oppty.Id).Expected_Deal_Value__c;
            oppty.Partner_Expected_Close_Date__c = mapOpptyReg.get(oppty.Id).Expected_Close_Date__c;
            oppty.PSC_Approval_Status__c = mapOpptyReg.get(oppty.Id).PSC_Approval_Status__c;
            oppty.Partner_Rejection_Reason__c = mapOpptyReg.get(oppty.Id).Partner_Rejection_Reason__c;
            if(mapUser.containsKey(mapOpptyReg.get(oppty.Id).CreatedById)){
                oppty.Opportunity_Registration_Creator__c =  mapUser.get(mapOpptyReg.get(oppty.Id).CreatedById).Name;
            }
            if( mapOpptyReg.get(oppty.Id).Secondary_Partner_User_lookup__c != null && mapUser.containsKey(mapOpptyReg.get(oppty.Id).Secondary_Partner_User_lookup__c)){
                oppty.Secondary_Partner_User1__c = mapUser.get(mapOpptyReg.get(oppty.Id).Secondary_Partner_User_lookup__c).Name;
            }
            else{
                oppty.Secondary_Partner_User1__c = null;
            }
            oppty.Registration_Type__c = mapOpptyReg.get(oppty.Id).Registration_Type__c;

            if( mapOpptyReg.get(oppty.Id).PSC_Approving_User__c != null && mapUser.containsKey(mapOpptyReg.get(oppty.Id).PSC_Approving_User__c)){
                oppty.PSC_Approving_User__c = mapUser.get(mapOpptyReg.get(oppty.Id).PSC_Approving_User__c).Name;
            }
            else{
                oppty.PSC_Approving_User__c = null;
            }
            if( mapOpptyReg.get(oppty.Id).Partner_Approving_User__c != null && mapUser.containsKey(mapOpptyReg.get(oppty.Id).Partner_Approving_User__c)){
                oppty.Partner_Approving_User__c = mapUser.get(mapOpptyReg.get(oppty.Id).Partner_Approving_User__c).Name;
                oppty.Partner_Approving_User_Email__c = mapUser.get(mapOpptyReg.get(oppty.Id).Partner_Approving_User__c).Email;
            }
            else{
                oppty.Partner_Approving_User__c = null;
            }
            //Bhanu - update "Opportunity_Registration_Nbr__c" field with oppty reg. no. if status is "Accepted/Submitted to Partner" - START
           if(mapOpptyReg.get(oppty.Id).Status__c.equals('Submitted to Partner') || mapOpptyReg.get(oppty.Id).Status__c.equals('Accepted by Partner') ){
                oppty.Opportunity_Registration_Nbr__c = mapOpptyReg.get(oppty.Id).Name;//Bhanu - Update Opportunity_Registration_Nbr__c field with Oppty reg no.
            }//Bhanu - update "Opportunity_Registration_Nbr__c" field with oppty reg. no. if status is "Accepted/Submitted to Partner" - END
            if(mapOpptyReg.get(oppty.Id).Status__c.equals('Accepted by Partner')){
                oppty.Shared_to_Partner__c = 'YES'; 
            }
            else if(mapOpptyReg.get(oppty.Id).Status__c.equals('Cancelled') || mapOpptyReg.get(oppty.Id).Status__c.equals('Rejected by Partner')){
                oppty.Shared_to_Partner__c = 'NO';
            }
            lstOppty.add(oppty);
        }
           Database.SaveResult[] res = database.update(lstOppty);
   }

    /*
    | Method Name : sendEmail
    | @param      : List<Opportunity_Registration__c>
    | Description : This method send email notifications on change of registration status.
    | Exception   : No Exception    
    */ 
    public void sendEmail(List<Opportunity_Registration__c> lstOpptyReg){
        String[] toAdd = new String[]{};
        String[] ccAdd = new String[]{};
        List<String> lstCCEmailIds = new List<String>();
        List<String> lstEmailIds = new List<String>();
        Id templateId;
        Opportunity_Registration__c oppReg = [Select Id,Status__c,Partner_Approval_Status__c,PDM__c,Related_Opportunity__c,
                                                Approving_Partner_User__c,Approving_Partner_User__r.Email,Approving_Partner_User__r.ContactId,
                                                Secondary_Partner_User_lookup__c,Secondary_Partner_User_lookup__r.Email,CreatedById,CreatedBy.Email,
                                                Additional_Contact_Email_1__c,Additional_Contact_Email_2__c,Additional_Email_Contact_1_PSC_Only__r.Email,Additional_Email_Contact_2_PSC_Only__r.Email, Theater1__c from Opportunity_Registration__c
                                                where id =: lstOpptyReg[0].Id LIMIT 1];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSaveAsActivity(false);
        if(oppReg.Status__c.equals('Rejected by Partner')){
            templateId = System.Label.OpptyCollab_PartnerRejTemplate;
            if(oppReg.PDM__c != null){
                lstCCEmailIds.add(oppReg.PDM__c);
            }
            ccAdd = lstCCEmailIds;
             mail.setTemplateId(templateId);    
             mail.setTargetObjectId(oppReg.CreatedById);
             mail.setWhatId(oppReg.Related_Opportunity__c);
             mail.setCcAddresses(ccAdd);
        }
        else if(oppReg.Status__c.equals('Cancelled') && oppReg.Partner_Approval_Status__c.equals('Accepted')){
            templateId = System.Label.OpptyCollab_PSCCancelTemplate;
            if(oppReg.PDM__c != null){
                lstCCEmailIds.add(oppReg.PDM__c);
            }
            if(oppReg.Additional_Contact_Email_1__c != null){
                lstCCEmailIds.add(oppReg.Additional_Contact_Email_1__c);
            }
            if(oppReg.Additional_Contact_Email_2__c != null){
                lstCCEmailIds.add(oppReg.Additional_Contact_Email_2__c);
            }
            //Changes made by Aagesh for CI Oct 2014 WR 1229 Starts
            if(oppReg.Additional_Email_Contact_1_PSC_Only__c != null){
                lstCCEmailIds.add(oppReg.Additional_Email_Contact_1_PSC_Only__r.Email);
            }
            if(oppReg.Additional_Email_Contact_2_PSC_Only__c != null){
                lstCCEmailIds.add(oppReg.Additional_Email_Contact_2_PSC_Only__r.Email);
            }
            //Changes made by Aagesh for CI Oct 2014 WR 1229 Ends
            lstCCEmailIds.add(oppReg.CreatedBy.Email);
            if(oppReg.Secondary_Partner_User_lookup__c != null){
                lstEmailIds.add(oppReg.Secondary_Partner_User_lookup__r.Email);
            }
            toAdd = lstEmailIds;
            ccAdd = lstCCEmailIds;
            mail.setToAddresses(toAdd);
            mail.setTemplateId(templateId);    
            mail.setTargetObjectId(oppReg.Approving_Partner_User__c);
            mail.setWhatId(oppReg.Id);
            mail.setCcAddresses(ccAdd);
        }
        else if(oppReg.Status__c.equals('Submitted to Partner')){
            templateId = System.Label.OpptyCollab_PartnerNotifyTemplate;
            if(oppReg.PDM__c != null){
                lstCCEmailIds.add(oppReg.PDM__c);
            }
            if(oppReg.Additional_Contact_Email_1__c != null){
                lstCCEmailIds.add(oppReg.Additional_Contact_Email_1__c);
            }
            if(oppReg.Additional_Contact_Email_2__c != null){
                lstCCEmailIds.add(oppReg.Additional_Contact_Email_2__c);
            }
            //Changes added for Req 1229
            if(oppReg.Additional_Email_Contact_1_PSC_Only__c != null)
                lstCCEmailIds.add(oppReg.Additional_Email_Contact_1_PSC_Only__r.Email);
            if(oppReg.Additional_Email_Contact_2_PSC_Only__c != null)
                lstCCEmailIds.add(oppReg.Additional_Email_Contact_2_PSC_Only__r.Email);
            //End of Changes for Req 1229
            lstCCEmailIds.add(oppReg.CreatedBy.Email);
            if(oppReg.Secondary_Partner_User_lookup__c != null){
                lstEmailIds.add(oppReg.Secondary_Partner_User_lookup__r.Email);
            }
            toAdd = lstEmailIds;
            ccAdd = lstCCEmailIds;
            mail.setToAddresses(toAdd);
            mail.setTemplateId(templateId);    
            mail.setTargetObjectId(oppReg.Approving_Partner_User__c);
            mail.setWhatId(oppReg.Id);
            mail.setCcAddresses(ccAdd);

            //Code for added by Avinash begins below..

            // map<ID,OrgWideEmailAddress> mapOrgAddress = new map<ID,OrgWideEmailAddress>([select Id, address from OrgWideEmailAddress where Address IN ('oppregistration@emc.com','pscemeadealregistration@emc.com','psc-apj@emc.com') limit 3]);
            map<ID,OrgWideEmailAddress> mapOrgAddress = new map<ID,OrgWideEmailAddress>([select Id, address, DisplayName from OrgWideEmailAddress where DisplayName IN ('Americas Approver','EMEA Approver','APJ Approver') limit 3]);

            if(mapOrgAddress != null && mapOrgAddress.size() > 0)
            {
                
                Id idAmericas, idAPJ, idEMEA;
                
                for (OrgWideEmailAddress mapValue : mapOrgAddress.values()) 
                {
                    if(mapValue.DisplayName == 'Americas Approver') {idAmericas = mapValue.id; continue;}
                    if(mapValue.DisplayName == 'EMEA Approver') {idEMEA = mapValue.id; continue;}
                    if(mapValue.DisplayName == 'APJ Approver') {idAPJ = mapValue.id; continue;}
                }

                if(oppReg.Theater1__c.equalsIgnoreCase('Americas'))
                    mail.setOrgWideEmailAddressId(idAmericas);

                else if (oppReg.Theater1__c.equalsIgnoreCase('APJ'))
                    mail.setOrgWideEmailAddressId(idAPJ);

                else if (oppReg.Theater1__c.equalsIgnoreCase('EMEA'))
                    mail.setOrgWideEmailAddressId(idEMEA);
            }

            //Code by Avinash ends above.

        }


         if(isExecuted == false){
            Messaging.SendEmailResult [] res = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail}); 
         }
         isExecuted = true;
    }
    
    
    //Changes made by Aagesh for CI Oct 2014 WR 1229 Starts
    
    public static void updateOpporunityemail(Map<Id, Opportunity_Registration__c> mapOptyReg)
    {
       List<Opportunity> lstUpdateOpty = new List<Opportunity>();
       List<Opportunity> lstQueryOpty = new List<Opportunity>();
       String dataErrs = '';
       Database.Saveresult[] results;
    
       if(!mapOptyReg.isEmpty())
         {
           //#1641 - added fields into query 'Is_this_a_Cross_Border_Deal__c,In_Which_Countries__c,Distributor_In_Origin_Country__c'     
           //#1825 - added field into the query 'EHC__c'
            lstQueryOpty = [Select id,Additional_Email_1_Sales_Only__c,Additional_Email_2_Sales_Only__c, Additional_Email_Contact_1_PSC_Only__c,Additional_Email_Contact_2_PSC_Only__c,
                                   Is_this_a_Cross_Border_Deal__c,In_Which_Countries__c,Distributor_In_Origin_Country__c,EHC__c from opportunity where id in :mapOptyReg.keySet()];   
                  
            for(Opportunity opty : lstQueryOpty)
            {
             Opportunity_Registration__c opprObj = mapOptyReg.get(opty.Id);
             System.debug('fields of oppty'+opprObj);
             opty.Additional_Email_1_Sales_Only__c = opprObj.Additional_Contact_Email_1__c;
             opty.Additional_Email_2_Sales_Only__c = opprObj.Additional_Contact_Email_2__c ;
             Id pscEmail1= opprObj.Additional_Email_Contact_1_PSC_Only__c;
             opty.Additional_Email_Contact_1_PSC_Only__c = pscEmail1;
             Id pscEmail2= opprObj.Additional_Email_Contact_2_PSC_Only__c;
             opty.Additional_Email_Contact_2_PSC_Only__c = pscEmail2;
            //added for CI#1641 - Start
             opty.Is_this_a_Cross_Border_Deal__c = opprObj.Is_this_a_Cross_Border_Deal__c;
             opty.In_Which_Countries__c = opprObj.In_Which_Countries__c;
             opty.Distributor_In_Origin_Country__c = opprObj.Distributor_In_Origin_Country__c;
            //End of CI#1641  
            //CI#1825 - Start
             opty.EHC__c = opprObj.Is_this_an_EHC_deal__c;
            //CI#1825 - End
             lstUpdateOpty.add(opty);
                }
                
            if((lstUpdateOpty!=null)&&(lstUpdateOpty.size() > 0))
            {
                results = Database.update(lstUpdateOpty, false);
            }
    
            if(results != null && results.size() >0)
            {
                for(Database.SaveResult sr : results)
                {
                    if(!sr.isSuccess())
                    {   
                        //Looking for errors if any.                
                        for (Database.Error err : sr.getErrors()) {
                            dataErrs += err.getMessage();
                        }
                        System.debug('An exception occurred while attempting an insert on ' + sr.getId());
                        System.debug('ERROR: ' + dataErrs);
                    }
                }
            }     
         }   
    }
    //Changes made by Aagesh for CI Oct 2014 WR 1229 Ends
    
}