/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req     DESCRIPTION                               
 |  ====            =========       ======     ===========  
 |  26/09/2014      Aarti Jindal                Test class for PRM_Portal_Scorecard_Homepage_Controller
 |                                            
+=============================================================================================================================*/

@isTest
private class PRM_Portal_Scorecard_Homepage_TC {

    static testMethod void homePageScorecardTest() {
        
       
        User insertUser1 = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
        User insertUser2= [Select id,UserType,u.ContactId,u.Contact.AccountId from User u where isActive=true and UserType='PowerPartner' limit 1];
        
       
       System.runAs(insertUser1){           
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();         
        }
       
        Account ProfiledAccount = PRM_PAN_VPP_TestClassHelper.createProfiledAccount();
        ProfiledAccount.Partner_Type__c ='Distributor'; 
        update ProfiledAccount;
        Test.StartTest();    
            
        ApexPages.StandardController controller01 = new ApexPages.StandardController(ProfiledAccount); 
        ApexPages.currentPage().getParameters().put('id',ProfiledAccount.Id);
        PRM_Portal_Scorecard_Homepage_Controller portalScoreCardObj1=new PRM_Portal_Scorecard_Homepage_Controller();
        portalScoreCardObj1.returnToPANORCountryPage();
        
        System.runAs(insertUser2){
        PRM_Portal_Scorecard_Homepage_Controller portalScoreCardObj2=new PRM_Portal_Scorecard_Homepage_Controller();
        }
        
        Test.StopTest();
    }
}