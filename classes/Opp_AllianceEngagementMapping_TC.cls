/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE       DEVELOPER       WR       DESCRIPTION                                
|
   ====       =========       ==       ===========  
|  03-Apr-2013  Prachi                 Created 
+===========================================================================*/ 
@isTest(SeeAllData = true) 
private class Opp_AllianceEngagementMapping_TC {
      
static testMethod void OppAllianceEngagementMapping(){
    
    Test.StartTest(); 
    Opportunity opt=new Opportunity();
    //opt=[Select id from Opportunity limit 1];
    opt=[Select id from Opportunity where Primary_Alliance_Partner__c!=null limit 1];
    ApexPages.StandardController sc = new ApexPages.StandardController(opt);
    Opp_AllianceEngagementMapping act = new Opp_AllianceEngagementMapping(sc);

    Test.StopTest();
}
}