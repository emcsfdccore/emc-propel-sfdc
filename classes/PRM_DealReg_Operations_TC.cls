/*============================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER           WR      DESCRIPTION                               

 |  ====          =========           ==      =========== 

 |  5/17/2011     Shalabh Sharma           This is the coverage class for DealReg_Operations   
    24/8/2011     Anirudh Singh            Commented populateDealRegFields method for SFA Release.   
    2 Dec 2011    Arif                     Updated createLead method to populate some mandatory fields for entering in approval process. 
    3 May 2012    Arif                     Cleaned up code for EMEA decommision  
    31jan 2013    vivek                    Improve code coverage 
    06 Jan 2013   Bhanu Prakash            Removed createOpportunity method added helper classes for test data creation
    03 sept  2014 Anvesh Kumar  PROPEL     commented call of methods 'createAccountAssociationBeforeDelete','CreateAccountAssociationAfterInsert','createAccountAssociationAfterUpdateDelete.    
 +============================================================================*/

 @isTest
private class PRM_DealReg_Operations_TC { 

   static List<Lead> emeaDR=new List<Lead>();
   public PRM_DealReg_Operations_TC()   //Creating test data for custom settings
   {
       CustomSettingDataHelper.dataValueMapCSData();
       CustomSettingDataHelper.eBizSFDCIntCSData();
       CustomSettingDataHelper.bypassLogicCSData();
       CustomSettingDataHelper.profilesCSData();
       CustomSettingDataHelper.dealRegistrationCSData();
       CustomSettingDataHelper.countryTheaterMapCSData();
        emeaDR=new List<Lead>();
        List<Account> account1 =  AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        List<Account> account2 =  AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        List<Account> account3 =  AccountAndProfileTestClassDataHelper.CreateT2PartnerAccount();
        emeaDR=LeadTestClassDataHelper.createDealReg(account1[0], null,account2[0], account3[0]);
        insert emeaDR;
    }

  static testmethod void TestAllMethods(){
        PRM_DealReg_Operations_TC prmdealregobj=new PRM_DealReg_Operations_TC();
        PRM_DealReg_Operations objoperatins = new PRM_DealReg_Operations(); 
      
        emeaDR=new List<Lead>();
        List<Account> account1 =  AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        List<Account> account2 =  AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        List<Account> account3 =  AccountAndProfileTestClassDataHelper.CreateT2PartnerAccount();
        emeaDR=LeadTestClassDataHelper.createDealReg(account1[0], null,account2[0], account3[0]);
        insert emeaDR;
        objoperatins.populateOpptyOwnerOnLead(emeaDR);
        Map<Id, Lead> oldMapLeads = new Map<Id, Lead>( [SELECT Id, name, RecordTypeId, Related_Opportunity__c,DealReg_Deal_Registration__c, DealReg_Deal_Registration_Status__c, DealReg_Handoff_to_TRACK__c FROM Lead where DealReg_Deal_Registration_Status__c = 'New' AND Related_Opportunity__c <> NULL LIMIT 5]);
        Map<Id, Lead> newLeadMaps = new Map<Id, Lead>();
        List<Lead> lisNewLeads = new List<Lead>();
        
        for(Lead leadRecord: oldMapLeads.values()){
            leadRecord.DealReg_Deal_Registration_Status__c = 'New';
            leadRecord.DealReg_Handoff_to_TRACK__c = false;
            leadRecord.DealReg_Theater__c = 'APJ';
            leadRecord.Country__c = 'India';
            lisNewLeads.add(leadRecord);
            newLeadMaps.put(leadRecord.Id,leadRecord);
        }
        objoperatins.linktoDealReg(newLeadMaps,oldMapLeads, false);//newMap,oldMap
        objoperatins.linktoDealReg(newLeadMaps,oldMapLeads, true);
        objoperatins.linktoDealReg(newLeadMaps,null, true);
        Map<Id,APPR_MTV__RecordAssociation__c> oldRecordAssociations=new Map<Id,APPR_MTV__RecordAssociation__c>([SELECT id,APPR_MTV__Account__c,Account_PartyId__c,APPR_MTV__Account_Role__c,APPR_MTV__Associated_Account__c,Associated_Account_PartyId__c,APPR_MTV__Primary__c FROM APPR_MTV__RecordAssociation__c where APPR_MTV__Account_Role__c='Distributor' LIMIT 5] );
        Map<Id,APPR_MTV__RecordAssociation__c> newRecordAssociations=new Map<Id,APPR_MTV__RecordAssociation__c>();
        List<APPR_MTV__RecordAssociation__c> listNewAPPRs = new List<APPR_MTV__RecordAssociation__c>();
        for(APPR_MTV__RecordAssociation__c APPRRecord: oldRecordAssociations.values()){
            APPRRecord.APPR_MTV__Account_Role__c= 'Distributor';
            listNewAPPRs.add(APPRRecord);
            newRecordAssociations.put(APPRRecord.Id,APPRRecord);
        }
        //objoperatins.createAccountAssociationAfterUpdateDelete(newRecordAssociations,oldRecordAssociations);
        //objoperatins.createAccountAssociationAfterInsert(newRecordAssociations,oldRecordAssociations);
        //objoperatins.createAccountAssociationBeforeDelete(newRecordAssociations,oldRecordAssociations);
        objoperatins.populatePartnerJudgement(newLeadMaps,false);//mapLeads
        objoperatins.populateDealSubmitterDetails(newLeadMaps,oldMapLeads,false);//newMapLeads,OldMapLeads
        objoperatins.dealRegMarkHandOfftoTrack(lisNewLeads,false);//newListLeads
        objoperatins.setRelatedAccount(oldMapLeads,newLeadMaps);
        objoperatins.updateRelatedAccountFieldsOnDR(emeaDR);
        objoperatins.populatedTheaterOnLead(lisNewLeads);
        objoperatins.populatePartnerOwnerEmailforDealReg(emeaDR);
        objoperatins.setRelatedAccountOnDealReg(account2);
        
        objoperatins.populateChannelManagerOnLead(newLeadMaps);
        objoperatins.ProcessRejectedLeads(lisNewLeads);
        objoperatins.populateRegProducts(lisNewLeads);
        objoperatins.takeAwayDelete(lisNewLeads);
        objoperatins.setNonDistributorDirectResllerFlag(account2);
        objoperatins.populateClusterOnAccount(account1);
        List<Extension_Request__c> extReq = new List<Extension_Request__c>();
        Extension_Request__c ext=new Extension_Request__c(Extension_Request_Status__c = 'Submitted',Submission_Date__c = null);
        Extension_Request__c ext1=new Extension_Request__c(Extension_Request_Status__c = 'Approved',Date_Approved_Rejected__c= null,Deal_Registration__c=emeaDR[0].id);
        extReq.add(ext);
        extReq.add(ext1);
        insert extReq;
        Map<Id, Extension_Request__c> oldMapExtReq = new Map<Id, Extension_Request__c>( [SELECT Id,Extension_Request_Status__c,Date_Approved_Rejected__c,Deal_Registration__c FROM Extension_Request__c where Extension_Request_Status__c='Approved' LIMIT 5]);
        Map<Id, Extension_Request__c> newMapExtReq = new Map<Id, Extension_Request__c>();
        List<Extension_Request__c> listExtReq = new List<Extension_Request__c>();
        
        for(Extension_Request__c ExtReqrecord: oldMapExtReq.values()){
            ExtReqrecord.Extension_Request_Status__c = 'Submitted';
            listExtReq.add(ExtReqrecord);
            newMapExtReq.put(ExtReqrecord.Id,ExtReqrecord);
        }
        objoperatins.setSubmissionORApprRejDate(extReq);
        objoperatins.populateFieldsOnERWhileCreation(extReq);
        objoperatins.updateDealRegExpirationFlag(newMapExtReq,oldMapExtReq);
        
    }
   
  
   
   static testmethod void testFieldRepApproval(){
        PRM_DealReg_Operations_TC prmdealregobj=new PRM_DealReg_Operations_TC();
        Test.startTest();
       
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(emeaDR[0].id);
        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()}); 
        Approval.ProcessResult result = Approval.process(req1);
        emeaDR[0].DealReg_Rejection_Reason__c = 'Criteria Not Met';
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        req2.setComments('Approve Request.');
        req2.setAction('Approve');
        req2.setWorkitemId(newWorkItemIds.get(0));
        emeaDR[0].DealReg_Rejection_Reason__c = 'Criteria Not Met';
        update emeaDR;  
        
        Test.stopTest();
       }
    
    static testmethod void testFieldRepRejection(){
        PRM_DealReg_Operations_TC prmdealregobj=new PRM_DealReg_Operations_TC();
        Test.startTest();
       
         Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
         req1.setComments('Submitting request for approval.');
         req1.setObjectId(emeaDR[0].id);
         req1.setNextApproverIds(new Id[] {UserInfo.getUserId()}); 
         Approval.ProcessResult result = Approval.process(req1);
         emeaDR[0].DealReg_Rejection_Reason__c = 'Criteria Not Met';
         List<Id> newWorkItemIds = result.getNewWorkitemIds();
         Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
         req2.setComments('Rejecting Request.');
         req2.setAction('Reject');
         req2.setWorkitemId(newWorkItemIds.get(0));
         emeaDR[0].DealReg_Rejection_Reason__c = 'Criteria Not Met';
         update emeaDR;  
        
         Test.stopTest();
    }
    
}