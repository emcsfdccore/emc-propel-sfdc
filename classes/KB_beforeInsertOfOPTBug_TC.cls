@isTest (SeeAllData=true)
private class KB_beforeInsertOfOPTBug_TC {

    static testMethod  void KB_beforeInsertOfOPTBug(){
        
        test.startTest();
        List<Id> idList = new List<Id>();
        
        Break_Fix__kav breakfix = new Break_Fix__kav();
        breakfix.Title = 'Test Class Break Fix';
        breakfix.UrlName = 'Test-Class-Breakfix'; 
        breakfix.Language = 'en_US';
        
        insert breakfix;
        idList.add(breakfix.id);
         
        List<KnowledgeArticleVersion> kav= [SELECT id,ArticleType, KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE Title = 'Test Class Break Fix' and id in :idList  AND Language = 'en_US'];
        KbManagement.PublishingService.publishArticle(kav[0].KnowledgeArticleId, true);
        
        System.debug('kav--->'+kav[0].ArticleNumber);   

        
        OPT_Bug__c optbug= new OPT_Bug__c();
        
        optbug.Article_Number__c=kav[0].ArticleNumber;
        optbug.Bug_Tracking_Number__c='1234554';
        
        insert optbug;
        test.stopTest();
        
        }
        }