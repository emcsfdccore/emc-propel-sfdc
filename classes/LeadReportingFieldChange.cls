/*==============================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER                WR       DESCRIPTION                               
 |  ====          =========                ==       =========== 
 | 3 May 2013   Prachi Bhasin           246408      Creation
 |10 Apr 2014   Sayan Choudhury         CI WR 389   Added Ebusinessreqparinsert method for CI April release wr 389
 +==============================================================================================================*/
public class LeadReportingFieldChange{
    public Static Boolean restrictDuplicate = false;
    public static boolean ischanged = false;
public void updateReportField( Map<Id,Lead> leadNewRecordsMap,  Map<Id,Lead> leadOldRecordsMap , List<Id> newOwnerId, List<Id> oldOwnerId){

        if(!restrictDuplicate){

        Set<String> restrictedProfileSet = new Set<String>();
        Set<String> allowedProfileSet = new Set<String>();

        Map<String,restrictedProfiles__c> mapRestrictedProfiles = restrictedProfiles__c.getall();

        for(restrictedProfiles__c restProf : mapRestrictedProfiles.values()){

            restrictedProfileSet.add(restProf.Profile_Name__c);
        }

        Map<String,allowedProfiles__c> mapAllowedProfiles = allowedProfiles__c.getall();

        for(allowedProfiles__c allowedProf : mapAllowedProfiles.values()){

            allowedProfileSet.add(allowedProf.Profile_Name__c);

         }

        Map<Id,User> newOwnersMap = new Map<Id,User>([select id,Profile.Name from User where Id IN :newOwnerId]);
        Map<Id,User> oldOwnersMap = new Map<Id,User>([select id,Profile.Name from User where Id IN :oldOwnerId]);
        List<User> runningUser = [Select id,Profile.Name,Theater__c from User where Id =: UserInfo.getUserid()];
        System.debug('newOwnersMap'+newOwnersMap);
        System.debug('oldOwnersMap'+oldOwnersMap);
        System.debug('restrictedProfileSet+++'+restrictedProfileSet);
        System.debug('allowedProfileSet+++'+allowedProfileSet);
        System.debug('mapRestrictedProfiles+++'+mapRestrictedProfiles);
        System.debug('mapAllowedProfiles+++'+mapAllowedProfiles);
        
                                                

        for(Lead leadObj : leadNewRecordsMap.values()){
            if(oldOwnersMap.containskey(leadOldRecordsMap.get(leadObj.id).ownerId) && newOwnersMap.containskey(leadObj.ownerid)){
         
           if(allowedProfileSet.contains(oldOwnersMap.get(leadOldRecordsMap.get(leadObj.id).ownerId).Profile.Name)){

                   if(!restrictedProfileSet.contains(newOwnersMap.get(leadObj.ownerid).Profile.Name)){


                          leadObj.Passed_from_Inside_Sales__c=true;
                          leadObj.Passed_from_Inside_Sales_Date_Time__c=System.now();
                          leadObj.Passed_from_Inside_Sales_Profile__c = oldOwnersMap.get(leadOldRecordsMap.get(leadObj.id).ownerid).Profile.Name;
                          leadObj.Passed_from_Inside_Sales_Name__c= UserInfo.getName();
                          leadObj.Passed_from_Inside_Sales_Theatre__c= runningUser[0].Theater__c;

                     }
            }
        }
        }

        restrictDuplicate = true;

    }
   }
   //Method Added for CI WR 389//
   /*This following method is used to set the ebusiness requested partner info with value of the field eBus_Requested_Partner__c if the field is populated
   else set the field with the value of eBus_Requested_Partner_Write_In__c*/
    public void Ebusinessreqparinsert(List<Lead> newleadLst ,Map<Id, Lead> oldleadMap , Boolean triggerCondition){  
    Set<Id> accIds = new Set<Id>();
    Map<Id,Account> mapAccs = new Map<Id,Account>();
    if(!ischanged){ 
    System.debug('triggerCondition---->'+triggerCondition);
    for(Lead leadobject :newleadLst){
        if(leadobject.eBus_Requested_Partner__c!=null && !triggerCondition){
                accIds.add(leadobject.eBus_Requested_Partner__c);
        }
        else if(leadobject.eBus_Requested_Partner__c!=null && triggerCondition){
                if(leadobject.eBus_Requested_Partner__c!=null && leadobject.eBus_Requested_Partner__c!=oldleadMap.get(leadobject.id).eBus_Requested_Partner__c)
                    accIds.add(leadobject.eBus_Requested_Partner__c);
        }

    }
        if(!accIds.isEmpty()){
             mapAccs = new Map<Id,Account>([SELECT ID, NAME, Address__c FROM Account WHERE id in:accIds]);
        }
         System.debug('***147, mapAccs= '+ mapAccs);
         if(!mapAccs.isEmpty()){
         for(Lead lead2 : newleadLst){
  
                if(lead2.eBus_Requested_Partner__c!=null){
                lead2.eBusiness_Request_Partner_Info__c=mapAccs.get(lead2.eBus_Requested_Partner__c).Name + mapAccs.get(lead2.eBus_Requested_Partner__c).Address__c.replaceAll('<br>', ' ') ;
                System.debug('***151,lead2.eBusiness_Request_Partner_Info__c= ' + lead2.eBusiness_Request_Partner_Info__c);
             }


             else if(lead2.eBus_Requested_Partner_Write_In__c!=null){
                lead2.eBusiness_Request_Partner_Info__c=lead2.eBus_Requested_Partner_Write_In__c;
             }  
        
          }
          }
            ischanged = true;        
        }
        
        }
    //End Of Code For CI WR 389 //

}