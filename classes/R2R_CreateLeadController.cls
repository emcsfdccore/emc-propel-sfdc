/*==================================================================================================================+

 |  HISTORY  |                                                                            

 |  DATE          DEVELOPER      WR        DESCRIPTION   strInputCode                            

 |  ====          =========      ==        =========== 

 |  24/02/2014    Srikrishna SM	FR 9639	  Commneted error conditions to avoid displaying error when asset is selected
 										  different Account distrinct and when Account District field is blank                    
 +==================================================================================================================**/
public class R2R_CreateLeadController {
    public List<Asset__c> lstSelectedAssets = new List<Asset__c>();
    public List<Lead_Asset_Junction__c> lstJnObj = new List<Lead_Asset_Junction__c>();  
    public List<Opportunity_Asset_Junction__c> lstOpptyJnObj = new List<Opportunity_Asset_Junction__c>();
    public Set<Id> setAssetIds = new Set<Id>();  
    public Set<Id> lstCreateLead = new Set<Id>();    
   // public Integer intActiveLead {get;set;} {intActiveLead = 0 ;} 
   // public Integer intCompetitiveAssetLinked {get;set;} {intCompetitiveAssetLinked = 0 ;}   
    public Integer intDiffAccount {get;set;} {intDiffAccount = 0;}
    public Integer intLinkToOpenLead {get;set;} {intLinkToOpenLead = 0;}
    public Integer intLinkToOpenOpportunity {get;set;} {intLinkToOpenOpportunity = 0;}
    public Integer intErrored {get;set;} {intErrored = 0;}
    public Integer intNoRec {get;set;} {intNoRec = 0;} 
    public Integer intNoAccDistrict {get;set;} {intNoAccDistrict = 0;}
    
   // public Integer intCompetitiveAsset {get;set;} {intCompetitiveAsset = 0;} 
    Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
    public String accdistrict,idListview,idRecordtype,dataValueMap,dataMapRetUrl,accName,accId,accLkid,strAssetIds,CustomUrl,encodeddataValueMap,encodedAccName,encodedAssetIds,encodedaccLkid ;
    
       
    
public R2R_CreateLeadController(ApexPages.StandardSetController controller) {
Integer assetCount=0;

string OpptyClosedLostStatus = data.get('OpptyClosedStatus').datavalue__c;
lstSelectedAssets = controller.getSelected();

 for (Asset__c selctdassets : lstSelectedAssets) 
        {
            setAssetIds.add(selctdassets.id);
        }
Map<Id,Asset__c> mapSelectedAssets = new map<Id,Asset__c>([Select id,name,Product_Name_Vendor__c, Product_Family__c, Customer_Name__r.name,Customer_Name__r.Customer_Profiled_Account__c,Customer_Name__r.Account_District__c,
                             RecordType.DeveloperName,Customer_Name__r.Customer_Profiled_Account_Lookup__r.Name,Customer_Name__r.Customer_Profiled_Account_Lookup__c,
                             Customer_Name__c from Asset__c Where id in :setAssetIds limit 50000]);
List<Lead_Asset_Junction__c> lstAssetJunction = [Select Id, Related_Lead__c from Lead_Asset_Junction__c where Related_Asset__c in :setAssetIds and Related_lead__r.Status not in ('Closed','Converted To Opportunity')];     
lstOpptyJnObj = [select id,Asset_Record_Type_ID__c,Opportunity_Forecast_Status__c,Opportunity_Close_Date__c,Related_Asset__c,Related_Asset__r.id,Related_Opportunity__c,
                        Related_Opportunity__r.Opportunity_Type__c,Related_Opportunity__r.StageName,Record_Type_Name__c from Opportunity_Asset_Junction__c where Related_Asset__c in:setAssetIds limit 50000];
if(lstAssetJunction !=null && !lstAssetJunction.isempty()){
    intLinkToOpenLead++;
}                
                          
    if((mapSelectedAssets!=null)&&(mapSelectedAssets.size() > 0)){
        for(asset__c assetObj :mapSelectedAssets.values()){
            accName = null;
            accId = null;
            accLkid = null;
            accdistrict=null;
            if(assetObj.Customer_Name__c!=null){
                if(assetObj.Customer_Name__r.Customer_Profiled_Account__c){
                    accdistrict= assetObj.Customer_Name__r.Account_District__c;
                    accName = assetObj.Customer_Name__c;    
                    accId = assetObj.Customer_Name__r.Name;
                    accLkid = assetObj.Customer_Name__r.Id;
                    break;
             
                }
                else if(!assetObj.Customer_Name__r.Customer_Profiled_Account__c ){
                        accId = assetObj.Customer_Name__r.Customer_Profiled_Account_Lookup__r.Name;
                        accLkid = assetObj.Customer_Name__r.Customer_Profiled_Account_Lookup__c;  
                        accdistrict= assetObj.Customer_Name__r.Account_District__c;
                        accName = assetObj.Customer_Name__c;    
                        accId = assetObj.Customer_Name__r.Name;
                        accLkid = assetObj.Customer_Name__r.Id; 
                if(assetObj.Customer_Name__r.Customer_Profiled_Account_Lookup__c != null){
                                  assetCount =assetCount+1;
                }
           
           }
                if(assetCount >= 2){
                accdistrict= assetObj.Customer_Name__r.Account_District__c;
                accName = assetObj.Customer_Name__r.Customer_Profiled_Account_Lookup__c ;    
                accId = assetObj.Customer_Name__r.Customer_Profiled_Account_Lookup__r.Name;
                accLkid = assetObj.Customer_Name__r.Customer_Profiled_Account_Lookup__c;  
                }
            }
        }
        
        if(accdistrict == null || accdistrict == ''){
           intNoAccDistrict++;
        }
        
        for(asset__c assetObj :mapSelectedAssets.values())
        {   
                if(accdistrict!=assetObj.Customer_Name__r.Account_District__c)
                {                   
                    intDiffAccount++;
                }
                else
                {
                    lstCreateLead.add(assetObj.id);                 
                }
        }       
    }

    else {
        intNoRec++;
    }
    for(Opportunity_Asset_Junction__c OpptyJunction :lstOpptyJnObj){
        if(!OpptyClosedLostStatus.contains(OpptyJunction.Related_Opportunity__r.StageName)
           && OpptyJunction.Related_Opportunity__r.Opportunity_Type__c.contains('Swap') && 
           mapSelectedAssets.get(OpptyJunction.Related_Asset__c).RecordType.DeveloperName=='Competitive_Install'){
           intLinkToOpenOpportunity++;
        }       
    }   
   
    
    intErrored = intLinkToOpenOpportunity+intNoRec+intDiffAccount+intLinkToOpenLead+intNoAccDistrict;
strAssetIds = '';                      
        for(Id assetid: lstCreateLead)
        {           
            strAssetIds += assetid + ',' ;
        }
        // remove last additional comma from string
        
        if((strAssetIds!= null) &&(strAssetIds!= ''))
        {
            strAssetIds = strAssetIds.subString(0,strAssetIds.length()-1);          
        }
        
        
        dataValueMap  = data.get('AssetIDFieldForLead').DataValue__c ;
        dataMapRetUrl = data.get('Return URL in Asset').DataValue__c ;
}
    public void validateLead(){
        if(intNoRec!= 0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.No_Record_Selected_for_Create_Opportunity);
            ApexPages.addMessage(msg);
        }
        if(intLinkToOpenLead !=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Link_To_Lead);
            ApexPages.addMessage(msg);
        }
        //FR-9639
        /*
        if(intDiffAccount!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Lead_Asset_not_in_same_CPA);
            ApexPages.addMessage(msg);
        }*/
        if(intLinkToOpenOpportunity!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Asset_Linked_To_Oppty);
            ApexPages.addMessage(msg);
        }
        //FR-9639
        /*
        if(intNoAccDistrict!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_Account_District_Cannot_Be_Blank);
            ApexPages.addMessage(msg);
        }
        */
    }
        

    public PageReference CreateLead(){
        validateLead();
        if(intErrored <=0){
            if(lstCreateLead!=null){
                idListView = Apexpages.currentPage().getParameters().get('fcf');
                idRecordtype= data.get('Tech_Refresh_Lead_Record_Type').datavalue__c;
                if((accId!=null)&&(accLkid!=null))
                {
                    encodedAccName = EncodingUtil.urlEncode(accId, 'UTF-8');
                    encodedaccLkid = EncodingUtil.urlEncode(accLkid, 'UTF-8');
                }
                if(strAssetIds!=null)
                {
                    encodedAssetIds =  EncodingUtil.urlEncode(strAssetIds, 'UTF-8');
                }
                 if(dataValueMap!=null)
                {
                    encodeddataValueMap = EncodingUtil.urlEncode(dataValueMap, 'UTF-8');
                }
                 if((idListView!=null)&&(idRecordType!=null)&&(encodedAccName!=null)&&(encodedaccLkid!=null)&&(encodedAssetIds!=null)&&(encodeddataValueMap!=null))
                {                   
                    CustomUrl= '/00Q/e?retURL='+dataMapRetUrl+idListView+'&RecordType='+idRecordType+'&ent=Lead&CF00N700000028Uj3='+encodedAccName+'&lea3='+encodedAccName+'&CF00N700000028Uj3_lkid='+encodedaccLkid+'&'+encodeddataValueMap+'='+encodedAssetIds;               
                    return new PageReference(CustomUrl);
                }
            }    
        }
        return null;
    }  
}