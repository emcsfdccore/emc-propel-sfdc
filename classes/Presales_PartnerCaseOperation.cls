/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 | 26/04/2012    Leonard Victor            This class is used for populating Partner Related Fields on Case
 | 30/05/2012    Leonard Victor            Updated class to include partner case creation through SFDC system
 | 15/09/2014    Vivek Barange   #1310     Updated trigger to restrict all profile users to update Case subtype field 
 |                                         except Business Admin or Systems Admin profiles
 | 15/09/2014    Akash Rastogi   #1335     Updated trigger to restrict all profile users to update Parent field on Child case
 |                                         except POC Team
 | 16/10/2014    Leonard Victor   1475     -Updated the caseDescriptionLock to ignore \r
+==================================================================================================================**/


public class Presales_PartnerCaseOperation {

    public Static Boolean partnerFlag = false;
    public Static Boolean instPartner = false;
    
    
    /* @Method <This method execute is used to populate Partner Related Fields on Case record on Insert or Update>
    @param <This method takes List<Case> as parameter>
    @return void - <Not returning anything>
    @throws exception - <No Exception>
    */ 
    
    public void insertPartner(List<Case> lstCase , Boolean updateGrp , List<Id> partnerCase){
    
    if(!partnerFlag){
        
        Map<Id,String> mapMailLst = new Map<Id,String>();
        Set<ID> caseSt = new Set<ID>();
        List<Id> cntList = new List<ID>();
        Set<ID> acctST = new Set<ID>();
        Map<Id,List<Id>> mapId = new Map<Id,List<Id>>();
        Map<Id,Account> mapAccount = new Map<Id,Account>();
        Map<Id,Account_Groupings__c> mapGrp = new Map<Id,Account_Groupings__c>();
        List<Account>  accList = new List<Account>();
        List<Account_Groupings__c>  grpLst = new List<Account_Groupings__c>();
        
        for(Case csObj : lstCase){

            //mapMailLst.put(csObj.id,csObj.Contact_Email1__c);
            cntList.add(csObj.contactid);
        }
        if (!updateGrp){
        
        Map<Id,Contact>  cntMap=new map<Id,Contact>([Select id,Partner_Contact2__c,Account.Grouping__c,Account.BillingCountry,Account.Theater1__c,accountid,email from contact where id in :cntList 
                                 and Partner_Contact2__c = true and Account.IsPartner=true]);
                                 
                
        if(cntMap.size()>0)
        {
    
            
            for(Case cse:lstCase){
                    if(cse.contactid!=null){
                    cse.Partner_Case__c = true;
                    cse.Partner_Grouping_Name__c = cntMap.get(cse.contactid).Account.Grouping__c; 
                    cse.Partner_Country__c = cntMap.get(cse.contactid).Account.BillingCountry;
                    cse.Partner_Theater__c = cntMap.get(cse.contactid).Account.Theater1__c;
                   }
           else
                   {
                     cse.Partner_Country__c = null;
                     cse.Partner_Theater__c = null;
                   }
                                    
        }

        
    }

    else{

        instPartner = true;
    }
    
      
    }
    
    if((updateGrp && partnerCase.size()>0) || (instPartner && partnerCase.size()>0)){
        
        Map<ID,Id> mapAcc = new Map<Id,Id>();
        
        
        grpLst = [SELECT Profiled_Account__c,id FROM Account_Groupings__c where id in: partnerCase];
        
        System.debug('updateGrp'+updateGrp);
        System.debug('updateGrp Section'+grpLst);
        
        for(Account_Groupings__c accGrp : grpLst){
            
            mapAcc.put(accGrp.Profiled_Account__c,accGrp.id);
        }
        
        if(mapAcc.keySet().size()>0){
            
            accList = [SELECT BillingCountry,Theater1__c,id FROM Account where id in: mapAcc.keyset()];
            
        }
        
        System.debug('updateGrp Section accList'+accList);
        
        if(accList.size()>0){
        
        for(Account acc:accList){
            
            if(mapAcc.containsKey(acc.id)){
            mapAccount.put(mapAcc.get(acc.id),acc);
            }
        }
        
        if(mapAccount.size()>0){
            
            for(Case cse:lstCase){
                
                    cse.Partner_Case__c = true;
                    cse.Partner_Country__c = mapAccount.get(cse.Partner_Grouping_Name__c).BillingCountry;
                    cse.Partner_Theater__c = mapAccount.get(cse.Partner_Grouping_Name__c).Theater1__c;
                    
                
        }
    }
        }
        
        else
        {
            for(Case cse:lstCase){
                if(cse.Partner_Grouping_Name__c!=null)
                cse.Partner_Grouping_Name__c.addError(System.Label.Presales_PartnerGrouping);
        else
                {
           cse.Partner_Country__c = null;
                   cse.Partner_Theater__c = null;
                }
                
        }
            
        }
        
    }
    
       partnerFlag = true;
       
       }
    
    }
    
    
    //#1310 - Updated below code to restrict all profile user to update Case subtype field except Business Admin or Systems Admin profiles
    public static void validateCaseSubType(List<Case> newList, Map<Id, Case> oldMap) {
        Case_Description_Lock__c descLockSetting = Case_Description_Lock__c.getInstance();
        Map<String, Profile_Mapping__c> mapProfiles = Profile_Mapping__c.getAll();
        for(Case cs : newList) {
            decimal now_n_Created = decimal.valueof((System.Now().getTime() - cs.createddate.getTime())/(1000));
            decimal created_n_Last = decimal.valueof((cs.LastmodifiedDate.getTime() - cs.createddate.getTime())/(1000));
            System.debug('now_n_Created : '+now_n_Created);
            System.debug('created_n_Last : '+created_n_Last);
            System.debug('descLockSetting.Now_N_Created__c : '+descLockSetting.Now_N_Created__c);
            System.debug('descLockSetting.Created_N_LastModified__c : '+descLockSetting.Created_N_LastModified__c);
            if(cs.RecordTypeId == System.Label.Renewals_Existing_Quote_Rework_Record_Type_Id 
                && cs.Type != oldMap.get(cs.Id).Type
                && !(mapProfiles.containsKey(UserInfo.getProfileId()) && mapProfiles.get(UserInfo.getProfileId()).Validation_1__c)) {
                if(cs.Parentid!=null){
                    if(now_n_Created > descLockSetting.Now_N_Created__c || created_n_Last > descLockSetting.Created_N_LastModified__c) {
                        cs.addError(System.Label.Case_Sub_Type_Not_Editable_For_Submitted_Renewals_Quote);
                    }
                } else {
                    cs.addError(System.Label.Case_Sub_Type_Not_Editable_For_Submitted_Renewals_Quote);
                }   
            }
            //#1335 - Added below code to validate parent field on child case
            if(cs.RecordTypeId == System.Label.Proof_of_Concept_Record_Type_Id 
                && !(mapProfiles.containsKey(UserInfo.getProfileId()) && mapProfiles.get(UserInfo.getProfileId()).Validation_2__c)
                && cs.Parentid != oldMap.get(cs.Id).Parentid) {
                if(cs.Parentid != null && oldMap.get(cs.Id).Parentid != null){
                    system.debug('Parentid : '+cs.Parentid);
                    if(now_n_Created > descLockSetting.Now_N_Created__c || created_n_Last > descLockSetting.Created_N_LastModified__c) {
                        cs.addError(System.Label.Case_Parent_Fields_Be_Edited_By_POC_Team);
                    }
                } else {
                    system.debug('Parentid : '+cs.Parentid);
                    cs.addError(System.Label.Case_Parent_Fields_Be_Edited_By_POC_Team);
                }    
            }
        }
    }
    
    public void caseDescriptionLock(List<Case> caselist, Map<Id, Case> oldMap)
    {
            for(Case caseLoop : caselist){
     //Added For Request 1236 Case Description Lock
         //Profiles__c profile = Profiles__c.getInstance();
         Case_Description_Lock__c descLockSetting = Case_Description_Lock__c.getInstance();
        sObject descObj = descLockSetting;
        Set<String> profileIdSet = new Set<String>();
        Schema.DescribeSObjectResult dsor = Case_Description_Lock__c.getSObjectType().getDescribe();
        Map<String, Schema.SObjectField> cusSettingFields = dsor.fields.getMap();
        for(String field : cusSettingFields.keyset()){
               Schema.DisplayType fieldType = cusSettingFields.get(field).getDescribe().getType();
                     if (Schema.DisplayType.STRING == fieldType && field.containsIgnorecase('__C')){
                              String myid = String.valueof(descObj.get(field));
                              system.debug('myid is------>'+myid);
                              profileIdSet.add(myid);
                         }

        }

        if((caseLoop.Record_Type_Developer_Name__c=='Presales_Accounts_Payable_T_E_Vendor_Master_Treasury'||caseLoop.Record_Type_Developer_Name__c=='Presales_Global_Revenue_Operations' ||caseLoop.Record_Type_Developer_Name__c=='Presales_Credit_Collections') && descLockSetting.Allow_Validation__c && !profileIdSet.contains(UserInfo.getProfileId())){
        //if((caseLoop.Record_Type_Developer_Name__c=='Presales_Accounts_Payable_T_E_Vendor_Master_Treasury'||caseLoop.Record_Type_Developer_Name__c=='Presales_Credit_Collections'||caseLoop.Record_Type_Developer_Name__c=='Presales_Global_Revenue_Operations') && !(UserInfo.getProfileId() == profile.System_Admin_API_Only__c)){
       // String tempDesc = caseLoop.Description;
        // Added as part of 1475: implemented as logic provided by SFDC team
       // if(!oldMap.get(caseLoop.id).Description.contains('\r')){
              //  tempDesc= tempDesc.replace('\r', '');
        //}
  
        if(!caseLoop.Description.equals(oldMap.get(caseLoop.id).Description)){
            
                if(caseLoop.Parentid!=null){
                    
                    decimal now_n_Created = decimal.valueof((System.Now().getTime() - caseloop.createddate.getTime())/(1000));
                    decimal created_n_Last = decimal.valueof((caseloop.LastmodifiedDate.getTime() - caseloop.createddate.getTime())/(1000));
                    if(now_n_Created >descLockSetting.Now_N_Created__c || created_n_Last >descLockSetting.Created_N_LastModified__c)
                        caseLoop.addError(System.Label.Description_field_is_not_editable);   
                }

                else{
                    
                    caseLoop.addError(System.Label.Description_field_is_not_editable);
                }
        }
        //End of Req 1236   
                
                    }
                }
            }
        }