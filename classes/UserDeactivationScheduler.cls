/*========================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE           DEVELOPER                WR                DESCRIPTION                               
 |  ====           =========                ==                =========== 
 |  13/08/2013      Sneha Jain          WR 261820       Scheduler class to deactivate the user records for the corresponding deactivated contacts
 |  18/12/2013      Srikrishna SM       Backword Arrow  Class is modified to check if there are 5 batch classes already scheduled for run
 +======================================================================================================================================*/

global  class UserDeactivationScheduler implements Schedulable
{
    global void execute(SchedulableContext SC)
    {
        try{
        	Integer runningBatchJobs = [select count() from AsyncApexJob where JobType = 'BatchApex' and status in ('Queued','Processing','Preparing')];
        	if(runningBatchJobs>4){
        		DateTime n = datetime.now().addMinutes(5);
                String cron = '';
                cron += n.second();
                cron += ' ' + n.minute();
                cron += ' ' + n.hour();
                cron += ' ' + n.day();
                cron += ' ' + n.month();
                cron += ' ' + '?';
                cron += ' ' + n.year();
                String jobName = 'Batch Job Deactivate Users - ' + n.format('MM-dd-yyyy-hh:mm:ss');
                UserDeactivationScheduler nextBatch = new UserDeactivationScheduler();
                Id scheduledJobID = System.schedule(jobName,cron,nextBatch);   
        		
        	}else{
        		CronTrigger ct = [SELECT id,CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :SC.getTriggerId()];
		        if(ct != null){
		            System.abortJob(ct.Id); 
		        }
		        
		        DeactivateUsers deactivateUsr = new DeactivateUsers();
		        database.executebatch(deactivateUsr,200);
        		
        	}
        }catch(Exception exp){
        	system.debug('Exception Occured : '+exp);
        }
        
    }
}