/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE       DEVELOPER       WR       DESCRIPTION                                
|
   ====       =========       ==       ===========  
|  02-Apr-2013  Prachi                 Created 
+===========================================================================*/ 
@isTest(SeeAllData=true)
private class AccountBeforeUpdateTrigger_TC {
  public static List<Account> oldAcc = New List<Account>();
  public static List<Account> newAcc = New List<Account>();
  public void createOldAcc(){              
      for(Integer k=0;k<2;k++){        
          Account accObj1 = new Account();        
          accObj1.name = 'TestAccount'+ k;        
          accObj1.BillingCountry ='United States';       
          accObj1.BillingCity = 'EDWARDS AFB';         
        
          accObj1.Customer_Profiled_Account__c=true;        
           
          accObj1.Partner_Type__c='Distributor';
          oldAcc.add(accObj1);
          }
  }

  public void createNewAcc(){              
      for(Integer k=0;k<2;k++){        
          Account accObj1 = new Account();        
          accObj1.name = 'TestAccount1'+ k;        
          accObj1.BillingCountry ='United States';       
          accObj1.BillingCity = 'EDWARDS AFB';         
         
          accObj1.Customer_Profiled_Account__c=true;        
          
          if(k==1){
          accObj1.Partner_Type__c='Distributor';
          }
          else {
          accObj1.Partner_Type__c='SaaS';
          }
          newAcc.add(accObj1);
          }
  }
    static testMethod void accountTest() {
 
Test.StartTest();
  AccountBeforeUpdateTrigger_TC objTC=new AccountBeforeUpdateTrigger_TC();
  AccountBeforeUpdateTrigger objAcc= new AccountBeforeUpdateTrigger(); 
  objTC.createOldAcc(); 
  objTC.createNewAcc();

  objAcc.beforeUpdate(oldAcc,newAcc);
Test.StopTest();
 
}
}