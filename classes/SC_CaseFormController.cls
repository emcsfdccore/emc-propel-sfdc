/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        ===========       
 
 | 15/06/2013     Leonard Victor             This Class is used for SC Webfor Case logic
 | 29/08/2013     Arvind Batra               September Release, Fix to remove the punctuation validation on additional email field
 | 27/09/2013     Jaspreet Singh             Method added to display description of the selected case sub type
   6 Dec 2013     Prasad K                   UAT defects for Viewstate exception    
   20 Jan 2014    Leonard Victor   CI:196    Added new method backtoCase 
 |  13-Oct-2014    Bisna V P    CI:1399    Remove code referencing unused fields
 | 2/18/2015       Bindu        CI:1211       Modified catch Block to handle the exception
 +==================================================================================================================**/

public class SC_CaseFormController{
    private final Case cs;
    public Boolean caseSaved {get; set;} {caseSaved=false;}
    public Boolean caseInserted{get; set;} {caseInserted=false;}
    public Attachment caseAttachment {get; set;}
    public list<Attachment> caseAttachments {get; set;}
     
    public Case caseObj {get; set;}
    public String recordTypeValue {get; set;}
    public String recordType {get; set;}

    //Language

    public String selectedLanguage{get; set;}
    public Map<String,String> mapLang = new Map<String,String>();



   //Attachment
    public static final integer attachmentsToAdd = 4;

    public List<Attachment> newAttachments{get;set;}
    
    //Recordtype Field Mapping
    public List<String> fldString{get; set;}
    public Map<String,SC_RecordType_Mapping__c> fldMappingMap{get; set;}
    public String recSubTypeMapping {get; set;}
    public RecordType rtObj = new RecordType();

    //Case ID
    public Id caseid;
    public PageReference caseUrl;

    //Rendering Fields
    public Boolean addAtt {get; set;}

    public Boolean isMCOTheater {get; set;}
    public Boolean isMCOPriority {get; set;}

    public Boolean stdType {get; set;}
    
    //Hidding Submit Button On Page Load
    
    public Boolean showSubmit {get; set;}
    
    public Boolean isCaseSubTypeDes {get;set;}
    public String caseSubTypeDes {get;set;}

    //Record Type Setting

    public String caseRecordType{get; set;}
 

    //Rerender Output Panel for Case Sub Type Description
     /* @Method <This method is used to rerender Output Panel for Case Sub Type Description>
    @param <This method takes no parameter>
    @return void - <Not returning anything>
    @throws exception - <No Exception>
    */
    public void displayDescription(){
        if(!stdType){
            caseObj.Type = caseObj.SC_Webform_Case_Sub_Type__c;
        }

        //String caseTypeFld = String.valueof(caseObj.Type);
            
        if (caseObj.Type =='--None--' || caseObj.Type =='' ){
           System.debug('caseObj.Type--->'+caseObj.Type);
           caseSubTypeDes = '';
           isCaseSubTypeDes = false;
        }else{
              isCaseSubTypeDes = true;
              Map<String, SC_IBG_CaseSubType_Desc__c> mapCaseDescCS = SC_IBG_CaseSubType_Desc__c.getAll();
              if(mapCaseDescCS.containsKey(caseObj.Type)){
                  SC_IBG_CaseSubType_Desc__c objCS = mapCaseDescCS.get(caseObj.Type);
                  //System.debug('objCS--'+objCS);
                  //String lab = objCS.Label_Name__c;
                  //System.debug('Label_Name__c--'+lab);
                  /*caseSubTypeDes=Label.lab;
                  System.debug('mapCaseDescCS-->'+caseSubTypeDes);*/
                  //caseSubTypeDes=caseObj.Type;
                  caseSubTypeDes=objCS.Label_Name__c;
              }
              //caseSubTypeDes=caseObj.Type;
        }
        System.debug('isCaseSubTypeDes'+isCaseSubTypeDes );
        System.debug('caseSubTypeDes'+caseSubTypeDes);
       
    }
     
   //Success Page

    public String caseNumber {get; set;}

      public SC_CaseFormController() {
          caseNumber = ApexPages.currentPage().getParameters().get('caseNmber');
          newAttachments=new List<Attachment>();  
          selectedLanguage = ApexPages.currentPage().getParameters().get('langForPage');
          //Added For Record Type Mapping from Sucess PAge
        recordTypeValue = ApexPages.currentPage().getParameters().get('recordType');
      }
 
/* @Method <This Constructor execute is used to Perfoem Initial level of logic on SC Case Page>
@param <This method takes StandardController as parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/

    public SC_CaseFormController(ApexPages.StandardController controller) {
        this.cs = (Case)controller.getRecord();
        caseAttachment = new Attachment();
        caseAttachments = new List<Attachment>();
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        caseAttachments.add(new Attachment());
        
        //Attachment Button

        addAtt = true;

        //Standard Type Logic

        stdType = true;
        
        //Hidding Submit Button On Intial Page Load
           
         showSubmit = false;
        
        //Language


        selectedLanguage = ApexPages.currentPage().getParameters().get('langForPage');
        recordTypeValue = ApexPages.currentPage().getParameters().get('recordType');
        if((selectedLanguage!='en_US') && (recordTypeValue=='Global Revenue Operations' || recordTypeValue=='Maintenance Contract Operations')){
            selectedLanguage='en_US';
        }
        System.debug('selectedLanguage'+selectedLanguage);

          Map<String,SC13_Language__c> mapLangauage = SC13_Language__c.getall();


            recSubTypeMapping = '';


          for(SC13_Language__c langSetting : mapLangauage.values()){


              mapLang.put(langSetting.Language_Code__c , langSetting.Language__c);

          }




     caseObj = (Case)controller.getRecord();  
        newAttachments=new List<Attachment>();  

     
       
        fldString = new List<String>();
            fldMappingMap = new Map<String,SC_RecordType_Mapping__c>();
        
            //recordTypeValue = ApexPages.currentPage().getParameters().get('recordType');

                                    System.debug('Thiss is recordTypeValue---->'+recordTypeValue);


            if(recordTypeValue!=null){
          
            

            //Getting Detault Type for Record Type



            Map<String,SC13_RecordType_Type__c> rtTypeMap = SC13_RecordType_Type__c.getall();

            if(rtTypeMap.containsKey(recordTypeValue.trim())){
            
                recordType = rtTypeMap.get(recordTypeValue.trim()).SC_Record_Type__c;

                 if(!rtTypeMap.get(recordTypeValue.trim()).isStandardType__c){
                        stdType = false;
                        System.debug('inside if looop------>'+stdType);
                    
                }
            }else{
                recordType = 'Presales Performance Analysis';
            }
            System.debug('Thiss is recordType---->'+recordType);
             rtObj  = [Select id,Name from RecordType where sObjectType='Case' and DeveloperName like 'Presales%' and name=:recordType.trim()];
            caseObj.recordtypeid = rtObj.id;

            if(rtObj.Name==Label.SC13_MaintenanceContractOperations_Donot_Translate){

                    isMCOTheater = true;
                    isMCOPriority = true;



            }

            
            caseObj.Type ='';


          //  recSubTypeMapping = recordType.trim()+'+'+(rtTypeMap.get(recordTypeValue).Type__c).trim();

            System.debug('Thiss is recSubTypeMapping---->'+recSubTypeMapping);

          
       }
            

    
   
      
   }

/* @Method <This is Action Method fot Add more Button>
@param <This method takes No parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/
    public void addMore(){
        
        for (Integer i=0; i<attachmentsToAdd; i++)
        {
            system.debug('first attmt--->'+newAttachments);
            newAttachments.add(new Attachment());
            system.debug('first attmt after adding--->'+newAttachments);
   
        }

        addAtt = false;
    } 
    
   /* @Method <This is Action Method for Case Save logic (Submit Button)>
@param <This method takes No parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/ 

public PageReference saveCaseOld()
{
    try
    {
    List<Attachment> attList =new List<Attachment>();
    ApexPages.Message myMsg;
    myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.SC_Error_Message);
    String email = caseObj.contact_Email1__c ; 
    Boolean createCase = true;
    boolean emcEmailValidation=true;
    
/*
//Section Commented as per below req. 
//Requirement 9018 ? Remove the Email Validation Rule on the Additional Notification Email fields on all Webforms
    
    /*if(recordTypeValue =='Install Base Group'){
        List<String> nameList1 = new List<String>();
        String email2 = caseObj.Additonal_Notification_Email_1__c + caseObj.Additonal_Notification_Email_2__c + caseObj.Additonal_Notification_Email_3__c;
        if(email2!='' && email2!=null)
            nameList1= email2.split('@emc.com');  
        
            for( String s : nameList1)
            {
                if(!s.contains('null') && !(s.contains('.')))
                {
                createCase = false;
                emcEmailValidation=false;
                }
            }
    }*/
    List<String> nameList= email.split('@');

    if (!(nameList[0].contains('.')) && email.contains('@emc.com'))
    {
    createCase = false;
    emcEmailValidation=false;
    }
//Added to show email validation once if any of the email field is not in proper format
    if(emcEmailValidation==false) {ApexPages.addMessage(myMsg);}

    if((ApexPages.getMessages()).size()>0)
    {
    createCase = false;
    }

    if(createCase)
    {
    //Inserting Case
        try
        {
        caseObj.Webform_Language__c = mapLang.get(selectedLanguage);
        insert caseObj;
        }
        catch(Exception e)
        {
        ApexPages.addMessages(e);
        }

        if(!newAttachments.isEmpty())
        {
            for (Attachment newAtt : newAttachments)
            {
            system.debug('attmt body--->'+newAtt.Body);
                if (newAtt.Body!=null)
                {
                newAtt.parentId=caseObj.id;
                attList.add(newAtt);
                }
            }
        }

        Case newCaseRecord =[select id,CaseNumber from Case where id = :caseObj.id];

        //Inserting Attachment
        if(!attList.isEmpty())
            try
            {
            insert attList;
            }
            catch(Exception e)
            {
            ApexPages.addMessages(e);
            }




        if(newCaseRecord != Null)
        {
            caseUrl = new PageReference('/apex/SC_Case_Success?caseNmber='+newCaseRecord.CaseNumber);
            caseUrl.setredirect(true);
        }

    }
    /*else
    {
    if(emcEmailValidation==false) {ApexPages.addMessage(myMsg);}
    }
    */
    return caseUrl;
}
catch(Exception e)
{
ApexPages.addMessages(e);
return caseUrl;
}
}
 


        public String getApplicationID(){

            return null;
        
    }

/* @Method <This  Action Method  is used for Select Button Used Next To Case Type Selection>
@param <This method takes No parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/

     public void renderFields(){


         fldString.clear();
         fldMappingMap.clear();
         recSubTypeMapping = '';
            
           if(!stdType){
            caseObj.Type = caseObj.SC_Webform_Case_Sub_Type__c;
           }


         String caseTypeFld = String.valueof(caseObj.Type);
            
            if (caseObj.Type =='--None--' || caseObj.Type =='' ){
            System.debug('caseObj.Type------>'+caseObj.Type);
                return;
            }
            recSubTypeMapping = recordType.trim()+'+'+caseTypeFld;

            System.debug('recSubTypeMapping------>'+recSubTypeMapping);
           
              List<SC_RecordType_Mapping__c> lstRecord = [Select FieldName__c,Section_Name__c,isMandatory__c,isRendered__c,SortOrder__c,RecordType_Subtype__c from SC_RecordType_Mapping__c where RecordType_Subtype__c=:recSubTypeMapping and isPSC_field__c = false  order by SortOrder__c ];

                for(SC_RecordType_Mapping__c scRecObj: lstRecord){
        
                    fldString.add(scRecObj.FieldName__c);
                    fldMappingMap.put(scRecObj.FieldName__c,scRecObj);
                }
                
                //Displaying Submit Button
                showSubmit = true;
        
       
    } 

/* @Method <This Action Method will be called on page load for intializing attachment>
@param <This method takes No parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/
        public void createInstance(){
        
    
        newAttachments=new List<Attachment>{new Attachment()};
        
        for (Integer i=0; i<attachmentsToAdd; i++)
        {
            system.debug('first attmt--->'+newAttachments);
            newAttachments.add(new Attachment());
            system.debug('first attmt after adding--->'+newAttachments);
   
        }
       
    }

     public PageReference saveAttachment()  {
         PageReference pageRef=null;
         try{
                List <Attachment> AttachmentsToInsert = new List <Attachment> (); 
                
                if (caseSaved==true) {
                    
                    //clear assets those are in
                    for( Attachment caseAttachment: caseAttachments){
                        if(caseAttachment.body!=null  ){
                             //caseAttachment.id=null;
                             System.debug('cs.id '+cs.id);
                             System.debug('caseAttachment.parentId '+caseAttachment.parentId);
                             System.debug('caseAttachment.name'+caseAttachment.name);
                            if(caseAttachment.parentId == null ){
                                caseAttachment.parentId = cs.id;
                                AttachmentsToInsert.add(caseAttachment);
                            }
                        }
                    }
                    
                    if(AttachmentsToInsert.size()>0){
                        system.debug('AttachmentsToInsert '+AttachmentsToInsert);
                        insert AttachmentsToInsert;
                        caseAttachment = null;
                        AttachmentsToInsert=null;
                        caseAttachments = new List<Attachment>();
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                        caseAttachments.add(new Attachment());
                    }
                    //Case newCaseRecord =[select id,CaseNumber from Case where id = :cs.id];
                    if((ApexPages.getMessages()).size()>0 ) {
                        try{
                            List<Attachment> Att_List =[SELECT Id FROM Attachment where ParentId=:cs.id];
                            if(Att_List != null && Att_List.size()>0  ){
                                delete Att_List;
                            }
                        }catch(exception e){
                            system.debug(e);
                        }
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info,Label.SC_FileAttachHelpText));
                        pageRef = null;
                        caseId=cs.id;
                        caseSaved=true;
                        cs.id=null;
                        
                        // delete newCaseRecord;
                    }else{
                        Case newCaseRecord =[select id,CaseNumber from Case where id = :cs.id];
                        pageRef = new PageReference('/apex/SC_Case_Success?caseNmber='+newCaseRecord.CaseNumber+'&langForPage='+selectedLanguage+'&recordType='+recordTypeValue );
                        pageRef.setRedirect(true);    
                    }
                           
                }
               
                
         }catch(Exception e){
             ApexPages.addMessages(e);
             pageRef=null;
             System.debug(e);
         }
         return pageRef;
    }
    public void saveCase() {
       try{ 
              // if(!caseInserted){ //Stop reinserting
       
                     validateEmail();
                    if(recordTypeValue.equalsIgnoreCase(Label.SC_Track_Resource_Hierarchy_Donot_Translate)){
                        trackFields();
                    }
                     if(caseSaved ){
                       
                         if(caseId==null){
                             cs.Webform_Language__c = mapLang.get(selectedLanguage); 
                             insert cs;    
                           }else{
                            cs.id=caseId;   
                            update(cs);
                          }
                        caseInserted=true;
                        caseSaved=true;
                        
                    }
                 //}
        }catch(exception e){
            //1211
           String errorMsg = e.getMessage();
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg.substring(errorMsg.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,') + 35)));
             caseSaved=false;
             //End of 1211
        }
    } 
    
     public void validateEmail(){
        System.debug('validate email....');      
        ApexPages.Message myMsg;
        myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter your EMC email address in the firstname.lastname@emc.com format');
        String email = cs.contact_Email1__c ; 
       
        List<String> nameList= email.split('@');
        System.debug('email  '+email );
        if ( email.contains('@emc.com')){
                if(nameList[0].contains('.')) {
                     caseSaved = true;  
                 }else{
                     caseSaved = false;  
                     ApexPages.addMessage(myMsg);
                 }
             
        }
         else{
             caseSaved = true;        
       }
 
        

 
    }

    public void trackFields(){
        System.debug('inside Track--------------------->');
      
        //commented for 1399
         /*String resourceMirror = String.valueof(cs.EN_Resource_to_Mirror__c); 
         String managerName = String.valueof(cs.PWID_Manager_Name__c);
         String resourceName = String.valueof(cs.PWID_Resource_Name__c);
         if(resourceMirror!=null){
            resourceMirror = resourceMirror.leftPad(6);
            resourceMirror = resourceMirror.Replace(' ','0');
            try{
            User resourceUser = [Select id from User where isactive = true and Employee_Number__c=:resourceMirror and Profile.UserType = 'Standard'  limit 1];
            System.debug('resourceUser-------->'+resourceUser);
            cs.Resource_to_Mirror__c = resourceUser.id;
            }
            catch(Exception ex){
                 caseSaved = false; 
                   ApexPages.Message myMsg;
                 myMsg = new ApexPages.Message(ApexPages.Severity.Warning,Label.SC_Error_Message2);
                 ApexPages.addMessage(myMsg);

            }
            


        }

        if(managerName!=null || resourceName!=null){
            //managerName = (managerName.leftPad(6)).Replace(' ','0');
            //resourceName = (resourceName.leftPad(6)).Replace(' ','0');
            System.debug('managerName----------->'+managerName+'resourceName-------------->'+resourceName);
            
            List<Contact> contLst = [SELECT Id,PowerlinkID_Badge__c FROM Contact  where Contact.recordtype.name = 'EMC Internal Contact'  and Active__c = true and PowerlinkID_Badge__c!=null and (PowerlinkID_Badge__c=:managerName or  PowerlinkID_Badge__c=:resourceName)];
            Map<String , Id> contactMap = new Map<String , Id>();

            for(Contact cnt : contLst){

                contactMap.put(cnt.PowerlinkID_Badge__c , cnt.id);


            }

            System.debug('contactMap----------->'+contactMap);

            if(contactMap.size()>0){

                if(managerName!=null){

                    System.debug('Inside IF1--->');

                        if(contactMap.containsKey(managerName)){
                            System.debug('Inside IF2--->');
                        cs.Manager_Name__c = contactMap.get(managerName);
                        }
                        else{
                            System.debug('Inside else1--->');
                                 caseSaved = false; 
                              ApexPages.Message myMsg;
                              myMsg = new ApexPages.Message(ApexPages.Severity.Warning,Label.SC_Error_Message3);
                             ApexPages.addMessage(myMsg);
    
                        }


                }
                if(resourceName!=null){
                    System.debug('Inside IF3--->');
                        if(contactMap.containsKey(resourceName)){
                            System.debug('Inside IF4--->');
                           cs.Resource_Name__c = contactMap.get(resourceName);

                        }
                        else{
                            System.debug('Inside else2--->');
                                 caseSaved = false; 
                              ApexPages.Message myMsg;
                              myMsg = new ApexPages.Message(ApexPages.Severity.Warning,Label.SC_Error_Message4);
                             ApexPages.addMessage(myMsg);
    
                        }


                }

                


            }

            else if(contactMap.size()<=0){
                         caseSaved = false; 
                      ApexPages.Message myMsg;
                      myMsg = new ApexPages.Message(ApexPages.Severity.Warning,Label.SC_Error_Message5);
                      ApexPages.addMessage(myMsg);

            }


        }*///comment ends for 1399
  
    }

//New Method to set Record type and Lang from Tank you page
         public PageReference backtoCase()  {

                    PageReference pageRef = null;
                    pageRef = new PageReference('/apex/SC_Case_Form?langForPage='+selectedLanguage+'&recordType='+recordTypeValue);
                    pageRef.setRedirect(true);  
                    return pageRef;


         }

}