/*=====================================================================================================+
|  HISTORY  |                                                                           
|  DATE          DEVELOPER        WR            DESCRIPTION                               
|  ====          =========        ==            =========== 
|  3-Mar-2011    Saravanan C    REQ# 1823       Test Method for PRM_PopPtnrBatch/Scheduler class
|  12-Jul-2011   Anirudh Singh                  Updated Lead Creation as per Lead Validation Rules.
|  Aug-2011      Saravanan C                    Test Class cleanup
|  11/1/2011     Prasad                         Taking more than 10 mins in test run..rduced the process time   
|  01/12/2011    Suman B                        Removed creation of Account and contacts.
|  14/12/2011       Anil                        Used Custom setting Data Halper
|  10 Oct 2013  Srikrishnadevaraya SM           This test class is modified to increase the processing time
+=====================================================================================================*/
@isTest  
private class PRM_PopulatePartneronLeadScheduler_TC {
    
    static testMethod void testPopulatePartnerOnBatch(){
        
        //setting the data for CustomSetting Bypass Logic custom settings
        /*
        CustomSettingBypassLogic__c byPassVar = new CustomSettingBypassLogic__c();
        byPassVar.By_Pass_Lead_Triggers__c=true;
        insert byPassVar;
        */
        //end of custom settings
        
        //setting the data for DataValueMap custom settings
        CustomSettingDataHelper.dataValueMapCSData();
        //setting the data for Profiles custom settings
        CustomSettingDataHelper.profilesCSData();
        CustomSettingDataHelper.eBizSFDCIntCSData();
        CustomSettingDataHelper.dealRegistrationCSData();
        //setting the data for bypass custom settings
        CustomSettingDataHelper.bypassLogicCSData();
        //end of custom settings
        
        //Create Leads
        List<Lead> leadList = new List<Lead>();
        for(integer i=0; i<5; i++){
            Lead newLead = new Lead(firstName = 'Test'+i, lastName = 'Sarv', email = 'noreply@yahoo.com', 
                                    Company = 'TestCompany',Status = 'Qualified',Sales_Force__c = 'EMC',Lead_Originator__c = 'Field',
                                    LeadSource = 'Manual',Partner__c =NULL , Tier_2_Partner__c =NULL , 
                                    Channel__c ='INDIRECT' , city='India', Street ='bangalore',Country__c='India');
            leadList.add(newLead);
        }
        
        //insert newly created lead list to database
        insert leadList;
        
        System.Test.startTest();
        //create query string - set limit to 1 form 20
        String Query ='SELECT id, ownerId, Reject_Lead__c, Accept_Lead__c,Tier_2_Partner__c, Partner__c, Previous_CAM_Email__c, CAM_Email__c, Channel__c,' 
                + ' Last_Inside_Sales_Owner__c, Last_EMC_Owner__c FROM Lead where (Partner__c = NULL OR Tier_2_Partner__c = NULL ) limit 1';
        
        //create a instance of batch class
        PRM_PopPtnrBatch pop=new PRM_PopPtnrBatch();
        Id batchProcessId = Database.executeBatch(new PRM_PopPtnrBatch(Query));
        String strScheduleTime ='0 0 0 3 9 ? ';
        strScheduleTime = strScheduleTime + Datetime.now().addYears(1).year();
        String jobId = System.schedule('test-PRM_PopulatePartneronLeadScheduler',strScheduleTime, new PRM_PopulatePartneronLeadScheduler());
        system.assertNotEquals(jobId , Null);
        Database.executeBatch(pop);
        System.Test.stopTest();        
    }
    
}