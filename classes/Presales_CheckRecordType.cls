/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        =========== 

 |  14/10/2011     Shalabh Sharma       This class is used to check whether case record is related to presales or not.    
 |  14March2013    Ganesh Soma         WR#247113    SOQL Optimization:Instead of quering on the RecordType object created a formula feild to get the developer name    
 |  24/12/2014     Bisna V P           CI WR 1427   created new method checkCaseEmailRecordTypeCI 
 |  13/01/2014     Vivek Barange       CI WR 1548   If the case is a child case (i.e. the parent filed is not empty) AND the "Also for Child Case" is FALSE then don't send email notficiation 
 |  10/03/2015     Vinod Jetti         CI WR 1675   Removed the condition 'caseRecord.Status=='Pending Customer' send notification to case owner regardless of the case status.
 |  15/04/2015     Bindu               CI 1727       Created a new method sendCaseEmailRecordTypeIBG
 +==================================================================================================================**/
 public class Presales_CheckRecordType{
    
    public string strCaseStatus = '';
/* @Method <This method execute is used to check if the case is related to Presales or not>
@param <This method takes List<Case> as parameter>
@returns List<Case>- <Returning list of case>
@throws exception - <No Exception>
*/   
     /* // ganesh commented on 12thmarch2013
     
     public List<Case> checkRecordType(List<Case> lstCase){
     System.debug('TEST**********'+lstCase);
     List<Case> lstPresalesCase = new List<Case>();
     Quering database for all case record types where developer name contains 'Presales'
     Map<Id,RecordType> mapRecordType = new Map<Id,RecordType>([select id,Name from RecordType where sObjectType='Case' and DeveloperName like 'Presales%']);
         for(Case caseRecord:lstCase){
            if(mapRecordType.containsKey(caseRecord.RecordTypeId)){
                lstPresalesCase.add(caseRecord);
            }
         }
         return lstPresalesCase;
     } */
     
     public List<Case> checkRecordType(List<Case> lstCase){
     System.debug('TEST**********'+lstCase);
     List<Case> lstPresalesCase = new List<Case>();     
         for(Case caseRecord:lstCase){
             if(caseRecord.Record_Type_Developer_Name__c != null && caseRecord.Record_Type_Developer_Name__c.contains('Presales')){             
                lstPresalesCase.add(caseRecord);
            }
         }
         return lstPresalesCase;
     }
/* @Method <This method execute is used to check if the task is related to Presales or not>
@param <This method takes List<Task> as parameter>
@returns List<Task>- <Returning list of Task>
@throws exception - <No Exception>
*/    
    public List<Task> checkTaskRecordType(List<Task> lstTask){
     
     List<Task> lstPresalesTask = new List<Task>();
     /*Quering database for all task record types where developer name contains 'Presales'*/
     Map<Id,RecordType> mapRecordType = new Map<Id,RecordType>([select id,Name from RecordType where sObjectType='Task' and DeveloperName like 'Presales%']);
         for(Task taskRecord:lstTask){
            if(mapRecordType.containsKey(taskRecord.RecordTypeId)){
                lstPresalesTask.add(taskRecord);
            }
         }
         return lstPresalesTask ;
     }
/* @Method <This method execute is used to check if the Case is related to Presales or not>
@param <This method takes List<EmailMessage> as parameter>
@returns Boolean- <Returning Boolean>
@throws exception - <No Exception>
*/

/* //Commented by Ganesh on 12march2013

    public Boolean checkCaseEmailRecordType(List<EmailMessage> caseEmail){
        Boolean isPresales=false;
        Case caseRecord = new Case();
        for(EmailMessage emailMessageLoop:caseEmail){
            caseRecord = [Select id,RecordTypeId from Case where Id =:emailMessageLoop.ParentId];
        }
        //Quering database for all case record types where developer name contains 'Presales'
        Map<Id,RecordType> mapRecordType = new Map<Id,RecordType>([select id,Name from RecordType where sObjectType='Case' and DeveloperName like 'Presales%']);
        if(mapRecordType.containsKey(caseRecord.RecordTypeId)){
            isPresales=true;    
        }
        return isPresales;
    } 
    
    */    
    
     public Boolean checkCaseEmailRecordType(List<EmailMessage> caseEmail){
        Boolean isPresales=false;
        strCaseStatus = '';
        Case caseRecord = new Case();
        for(EmailMessage emailMessageLoop:caseEmail){
            caseRecord = [Select id,Status,Record_Type_Developer_Name__c,RecordTypeId from Case where Id =:emailMessageLoop.ParentId];
        }      
        if(caseRecord!=null && (caseRecord.Record_Type_Developer_Name__c != null && caseRecord.Record_Type_Developer_Name__c.Contains('Presales')))
        {
             strCaseStatus = caseRecord.Status;
             isPresales=true;    
        }        
        return isPresales;
    } 
    //added for CI 1427
    public void sendCaseEmailRecordTypeCI(List<EmailMessage> caseEmail){
    
        List<Case> isPresalesCI=new List<Case>();
        String template = 'Pressales CI Case Additional Info';
        List<EmailTemplate> templateId= [select id from EmailTemplate where Name =: template limit 1];
        System.debug('templateId----'+templateId);
        
        Case caseRecord = new Case();
        for(EmailMessage emailMessageLoop:caseEmail){
            if(emailMessageLoop.Incoming){
            //1548 - Added ParentId into query
            caseRecord = [Select id, Status, ownerid, Record_Type_Developer_Name__c, RecordTypeId, ParentId from Case where Id =:emailMessageLoop.ParentId];
      //1548 - Added "caseRecord.ParentId == null" Check
      //1675 Removed the condition 'caseRecord.Status=='Pending Customer'
           // if(caseRecord!=null && caseRecord.ParentId == null && caseRecord.Record_Type_Developer_Name__c.Contains('Presales_Competitive_Analysis')){
               //Removed Parentid check
                if(caseRecord!=null && caseRecord.Record_Type_Developer_Name__c.Contains('Presales_Competitive_Analysis')){
            Messaging.SingleEmailMessage Message = new Messaging.SingleEmailMessage();
            Message.setWhatId(caseRecord.Id); 
            Message.setTemplateId(templateId[0].id);
            Message.saveAsActivity =false;
            String strOwnerId = (string)caseRecord.OwnerId;
            if(strOwnerId.startsWith('005')){
            Message.setTargetObjectId(caseRecord.ownerid); 
            Message.setOrgWideEmailAddressId(Id.valueOf(System.Label.CISFDCID));       
            Messaging.sendEmail(new Messaging.singleemailMessage[] {message});
            }
            }}
        }      
        
    } 
    //changes ends for CI 1427.
    
    //added for CI 1727
    public void sendCaseEmailRecordTypeIBG(List<EmailMessage> caseEmail){
    
        List<Case> isPresalesCI=new List<Case>();
        List<AutoNoti_PresalesIBG_Pending_status__c> IBGstatus = AutoNoti_PresalesIBG_Pending_status__c.getall().values();
        String template = 'IBG Case Additional Info';
        List<EmailTemplate> templateId= [select id from EmailTemplate where Name =: template limit 1];
        System.debug('templateId----'+templateId);
        
        Case caseRecord = new Case();
        for(EmailMessage emailMessageLoop:caseEmail){
            if(emailMessageLoop.Incoming){
            caseRecord = [Select id, Status, ownerid, Record_Type_Developer_Name__c, RecordTypeId, ParentId from Case where Id =:emailMessageLoop.ParentId];
             if(caseRecord!=null && caseRecord.Record_Type_Developer_Name__c.Contains('Presales_Install_Base_Group') ){
            if(caseRecord.Status.equalsIgnoreCase(IBGstatus[0].Case_Status__c) || caseRecord.Status.equalsIgnoreCase(IBGstatus[1].Case_Status__c))
            {          
            Messaging.SingleEmailMessage Message = new Messaging.SingleEmailMessage();
            Message.setWhatId(caseRecord.Id); 
            Message.setTemplateId(templateId[0].id);
            Message.saveAsActivity =false;
            String strOwnerId = (string)caseRecord.OwnerId;
            if(strOwnerId.startsWith('005')){
            Message.setTargetObjectId(caseRecord.ownerid); 
            Message.setOrgWideEmailAddressId(Id.valueOf(System.Label.EMCBS)); 
            Messaging.sendEmail(new Messaging.singleemailMessage[] {message});
            }
            }
            }
        }      
       } 
    } 
    //changes ends for CI 1727.
    
    
/* @Method <This method execute is used to check if case is created by Conversion Admin>
@param <This method takes no parameter>
@returns Boolean- <Returning Boolean>
@throws exception - <No Exception>
*/ 
    public Boolean checkIFConversionAdmin(){
        Boolean isAPI = false;
        Profile profileLoginUser = [Select Id,Name from Profile where Id=:UserInfo.getProfileId()];
        system.debug('profileLoginUser--->'+profileLoginUser);
        if(profileLoginUser.Name == 'System Administrator - API Only'){
            isAPI = true;   
        }   
        return isAPI;
    }     
 }