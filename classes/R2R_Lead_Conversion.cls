global class R2R_Lead_Conversion implements Database.Batchable<sObject>
{
   
  global void MergeAsset(List<String> leadid,List<String> opportunityId)
      {
          List<Asset__c> ls=new List<Asset__c>();
          List<Lead_Asset_Junction__c> lstLead=[Select Id,Related_Asset__c from Lead_Asset_Junction__c where Related_Lead__c in:leadid];
          List<String> lstopp= new List<string>();
          List<Opportunity_Asset_Junction__c> Oaj = new List<Opportunity_Asset_Junction__c>();
            for(String stopp:opportunityId)
                   {
              for(Lead_Asset_Junction__c laj : lstLead)
                      {
                Opportunity_Asset_Junction__c oppAsst = new Opportunity_Asset_Junction__c();
                   oppAsst.Related_Asset__c = laj.Related_Asset__c;
                      oppAsst.Related_Opportunity__c = stopp;
                         Oaj.add(oppAsst);
                      }

                    }
               insert Oaj; 
          
      }  
   global Database.QueryLocator start(Database.BatchableContext BC)
          {
       
           date d = system.today();
            return Database.getQueryLocator('Select Id,Related_Asset__c from Lead_Asset_Junction__c where createdDate =:d');
             
          }
   
     global void execute(Database.BatchableContext BC, List<Lead_Asset_Junction__c> leadAsst)
      { 
           List<String> lstopp= new List<string>();
           List<String> opts= new List<String>();

         List<Opportunity_Asset_Junction__c> Oaj = new List<Opportunity_Asset_Junction__c>();
            for(Lead_Asset_Junction__c laj : leadAsst)
                {
              opts.add(laj.Related_Lead__c); 
                }
              List<Opportunity> lstOpt=[Select Id from Opportunity where Related_Deal_Registration__c in:opts];

                   for(Opportunity ledOpp:lstOpt)
                     {
                     for(Lead_Asset_Junction__c laj : leadAsst)
                          {
                         Opportunity_Asset_Junction__c oppAsst = new Opportunity_Asset_Junction__c();
                            oppAsst.Related_Asset__c = laj.Related_Asset__c;
                              oppAsst.Related_Opportunity__c = ledOpp.id;
                           Oaj.add(oppAsst);
                          }
                      }
                  insert Oaj;
      }

    global void finish(Database.BatchableContext BC)
      {

      }
}