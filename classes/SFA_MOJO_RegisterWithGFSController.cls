/*========================================================================================================================+
|  HISTORY                                                                  
|                                                                           
|   DATE            DEVELOPER       WR          DESCRIPTION                               
|   ====            =========       ==          =========== 
|   12.10.2012      Smitha          MOJO        Initial Creation. Register with GFS button on Opty. Creates/update GFS record, Validates opty tied to asset or not.
|   19.10.2012      Avinash K       MOJO        Updated code to insert chatter feed on the Opportunity.
|   23 Nov 2012     Suresh D        216140      Modifed code to remove District Manager validation check.
|   10.01.2013      Medhavi D.      217884      Added sendmail method to send mails
|   08 Nov 2013     Sneha Jain      FR 9179     R2R GFS Modifications Req ID :9179
|   06 Feb 2014     Sneha Jain      FR 9495     Update Validation rule to allow Centera Products to be registered based on Number of Ports/Node or Capacity. 
|   26 Feb 2014     Sneha Jain      FR 9857     Adding Number of Ports/Nodes to query for displaying in Asset details
|   07 Mar 2014     Sneha Jain                  GFS Bug Fix Added Sales_Rep__c in the return field of SOQL query
|   16 June 2014     Vivek Barange   991        Modifed code to calculate total GFS Rep incentive           
|   08 Dec 2014     Bhanuprakash    PROPEL      E.002: added "GFS_Regional_Fin_Specialist" in OR condition        
|   16 Oct 2014     Partha Baruah   INC0716388  Fix added for "Register with GFS"Button Issue: unconditional creation of 3 GFS records in Opportunity 
|   11 Nov 2014     Bisna V P        CI:1484    GFS Registration - Validation on Required Fields
|   11 Nov 2014     Vivek Barange    CI:1483    Added new Column "DPAD/Isilon Rep Name" in Email Templete
|   2/17/2015       Bindu            CI:1673    Added Re-Registration Reason to email template
+========================================================================================================================*/


public class SFA_MOJO_RegisterWithGFSController {
    
    public Id Optyid;
    public List<Opportunity_Asset_Junction__c> lstRelAsset = new List<Opportunity_Asset_Junction__c>();
    public List<Trade_Ins_Competitive_Swap__c> lstGFSObj = new List<Trade_Ins_Competitive_Swap__c>();
    public Integer intNoAsset {get;set;} {intNoAsset = 0 ;} 
    public Integer intNotEligibleOpty {get;set;} {intNotEligibleOpty = 0 ;}
    public Integer intNotEligibleAsset {get;set;} {intNotEligibleAsset = 0 ;}
    public Set<Id> setAssetIds = new Set<Id>();
    public List<Asset__c> lstValidateAsset = new List<Asset__c>();
    public boolean isConnectrixProductAdded = false;
    public Id userAFM;
    public Opportunity opp;
    public Boolean isException{get;set;}
    public List<OpportunityLineItem> oppProduct = new List<OpportunityLineItem>();
    //Hold Custom setting for record type ids for Competitive Install/ EMC Install.
    Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
    String strAmericasTheater ='';
    String strEMEATheater ='';
    String strAPJTheater ='';
      List<String> ccAdd = new List<String>();
    
//Following List variables has the List of Chatter feeds to be inserted      -Avinash K
    public List<FeedItem> lstFeedItem = new List<FeedItem>();
    public static Boolean blnStaticVariableInsert = false;
    public static Boolean blnStaticVariable = false;
    public static Boolean blnstaticEmailAlert=false;
//Following variable added by Avinash K
    String strUserName = '';

    public SFA_MOJO_RegisterWithGFSController(ApexPages.StandardController controller) {
         
             
        Optyid = ApexPages.currentPage().getParameters().get('id');
      isException = false;
        if(Optyid==null)return;
        opp=[Select name,id,Product_Model__c,accountId,Account_TheaterText__c ,Partner__c,Country__c,Opportunity_Owner__c,District_Manager__c,District_Manager__r.Name,
        Quote_Cart_Number__c,Products__c,Opportunity_Number__c,ownerid,CloseDate,Account_Name1__c,StageName,Amount,RecordTypeId,Re_Registration_Reason__c from opportunity where id =:Optyid limit 1];                       
        
                                userAFM = [select id, email from user where id = :userInfo.getUserId() limit 1].id;
        //oppProduct=[select id,CurrencyIsoCode,PricebookEntryId,Product_Dependency__c,Product_Family__c, Product_Type__c,TotalPrice from OpportunityLineItem where OpportunityId =:Optyid];
        
        lstRelAsset = [select id,Related_Asset__c,Related_Asset__r.RecordType.DeveloperName,Related_Asset__r.Product_Name_Vendor__c,Related_Asset__r.Product_Family__c,
                       Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c, Related_Asset__r.GFS_Rep_Incentive_Num__c from Opportunity_Asset_Junction__c where Related_Opportunity__r.id = :Optyid];
        
        for(Opportunity_Asset_Junction__c assetId :lstRelAsset)
        {
            setAssetIds.add(assetId.Related_Asset__c);
        } 
        
        if((setAssetIds!=null)&&(setAssetIds.size() > 0))
        {
            lstValidateAsset = [select id,RecordType.DeveloperName,Model__c,Product_Name_Vendor__c,Serial_Number__c,Drive_Quantity__c,Drive_Size_GB__c,X5_Drive_Quantity__c,
                                Estimated_Raw_Capacity_TB__c,Total_Raw_Capacity_TB__c,Configuration_Details__c,Drive__c,X1_Drive__c,X2_Drive_Capacity_GB__c,
                                X2_Drive__c,X3_Drive_Capacity_GB__c,X3_Drive_Quantity__c,X4_Drive_Capacity_GB__c,X4_Drive_Quantity__c,X5_Drive_Capacity_GB__c,Number_of_Ports__c,Product_Family__c 
                                from Asset__c where id in:setAssetIds];                               
        }  
        if((lstValidateAsset!=null)&& (lstValidateAsset.size() > 0))
        {
            for(Asset__c configDetails :lstValidateAsset)
            {      
                if(configDetails.RecordType.DeveloperName =='EMC_Install' && configDetails.Product_Family__c=='CONNECTRIX'){
                   isConnectrixProductAdded = true;
                }
                //Update Validation rule to allow Centera Products to be registered based on Number of Ports/Node or Capacity. - FR 9495
                if(((configDetails.RecordType.DeveloperName =='EMC_Install' && (configDetails.Product_Family__c=='CONNECTRIX' ||configDetails.Product_Family__c=='CENTERA' ) && (configDetails.Number_of_Ports__c==null) && (configDetails.Estimated_Raw_Capacity_TB__c== null)) 
                    || (configDetails.RecordType.DeveloperName =='EMC_Install' && (configDetails.Product_Family__c!='CONNECTRIX' && configDetails.Product_Family__c !='CENTERA')&& configDetails.Estimated_Raw_Capacity_TB__c== null)) || 
                  (configDetails.RecordType.DeveloperName =='Competitive_Install' && ((configDetails.Product_Name_Vendor__c== null || configDetails.Model__c == null ) ||
                  (((configDetails.Drive__c==null || configDetails.X1_Drive__c == null) && (configDetails.X2_Drive_Capacity_GB__c==null || configDetails.X2_Drive__c== null) &&
                    (configDetails.X3_Drive_Capacity_GB__c==null || configDetails.X3_Drive_Quantity__c == null) && (configDetails.X4_Drive_Capacity_GB__c==null || configDetails.X4_Drive_Quantity__c == null) && 
                    (configDetails.X5_Drive_Capacity_GB__c==null || configDetails.X5_Drive_Quantity__c == null)
                  )
                  
                      && configDetails.Configuration_Details__c == null)&&(configDetails.Estimated_Raw_Capacity_TB__c== null)
                  )
                  
                  ))
                {
                    intNotEligibleAsset++;                    
                }
            }
        }   
        
        if((opp.accountid==null)||(opp.Country__c==null)||(opp.ownerid==null)||(opp.Opportunity_Number__c == null)||(opp.CloseDate == null) ||(opp.StageName==null)||(opp.Amount==null)||(opp.Products__c==null))
        {            
                intNotEligibleOpty++;  
        }
        if(lstRelAsset.size()<=0)
        {
            intNoAsset++;           
        }
        
        else
        {
            lstGFSObj = [select id,name,Registration_ID__c,RecordTypeid,Registration_Type__c,Related_Opportunity__c, Related_Opportunity__r.Account_Name1__c, Related_Opportunity__r.id,sales_rep__c,AFM__c from Trade_Ins_Competitive_Swap__c where Related_Opportunity__r.id = :Optyid ];          
        }
        strAmericasTheater= DataValueMap.get('America Theater Email Notification').DataValue__c;
        strEMEATheater = DataValueMap.get('EMEA Theater Email Notification').DataValue__c;
        strAPJTheater = DataValueMap.get('APJ Theater Email Notification').DataValue__c;
        
        
    }
    
    public void Validate()
    {   
        //Added for CI 1484
        try{update opp;}
        catch(System.DMLException e) {
        isException = true; 
        
        ApexPages.addMessages(e);
        
        }
        //CI 1484 changes ends here
        if(intNotEligibleAsset!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.Asset_missing_Configuration_Details_or_Total_Raw_Capacity);
            ApexPages.addMessage(msg);
        }
        if(intNoAsset!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.Cannot_register_an_opportunity_without_any_assets);
            ApexPages.addMessage(msg);
        }
        
        if(intNotEligibleOpty!=0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.Not_eligible_to_register_for_GFS);
            ApexPages.addMessage(msg);
        }
    }
    
    public PageReference RegisterGFS()
    {   isException = false;
        Validate();
     
        List<OAR_Member_Added__c> lstteam=new List<OAR_Member_Added__c>();
                
        if((intNoAsset<=0)&&(intNotEligibleOpty<=0)&&(intNotEligibleAsset<=0)&& (isException==false))
        {           
            
//Code by Avinash K begins...

            if (System.Userinfo.getFirstName() != null && System.Userinfo.getFirstName() != '') 
            {
                strUserName += System.Userinfo.getFirstName();
            }
            if (System.Userinfo.getLastName() != null && System.Userinfo.getLastName() != '') 
            {
                strUserName += ' ' + System.Userinfo.getLastName();
            }
            
//Code by Avinash k ends.

            //R2R GFS Modifications Start Req ID :9179 - Sneha Jain 08-Nov-2013 
            // Code added to include all Asset details on the GFS Registration email
            String assetDetails ='';
            List<Opportunity_Asset_Junction__c> OppAssetList = new List<Opportunity_Asset_Junction__c>();
            List<Asset__c> AssetList = new List<Asset__c>();
            Set<Id> AssetIdSet = new Set<Id>();
            //Query to fetch all Asset records associated the opportunity
            OppAssetList = [select Related_Asset__r.Id from Opportunity_Asset_Junction__c where Related_Opportunity__r.Id =: opp.Id];

            if(OppAssetList != null && OppAssetList.size()>0)
            {
                //Collection of all Asset record IDs
                for(Opportunity_Asset_Junction__c oppAssets : OppAssetList)
                {
                    AssetIdSet.add(oppAssets.Related_Asset__c);
                }
                if(AssetIdSet != null && AssetIdSet.size()>0)
                {              
                    //Query to fetch details of identified Assets
                    //Adding Number of Ports/Nodes to query for displaying in Asset details - FR 9857
                    AssetList = [select Serial_Number__c,Product_Name_Vendor__c,Product_Family__c,Model__c,Estimated_Raw_Capacity_TB__c,Number_of_Ports__c,DPAD_Isilon_Rep_Name__c from Asset__c where Id in : AssetIdSet];
                    if(AssetList != null && AssetList.size() >0)
                    {
                        //String formating to include the table of Asset records with corresponding field values
                        String heading = '<th>' + ' Serial Number ' + '</th> <th>' + ' Vendor ' + '</th> <th>' +  ' Product Family ' + '</th> <th>' + ' Model ' + '</th> <th> ' + ' Estimated Raw Capacity ' + '</th> <th>' + 'Number of Ports/Nodes' + '</th> <th>' + 'DPAD/Isilon Rep Name' + '</th>'; //#1483 - added new Column "DPAD/Isilon Rep Name"
                        for(Asset__c assets : AssetList)
                        {
                            assetDetails = assetDetails + '<tr><td>' + assets.Serial_Number__c + '</td><td>' + assets.Product_Name_Vendor__c + '</td><td>' + assets.Product_Family__c + '</td><td>' + assets.Model__c + '</td><td>' + assets.Estimated_Raw_Capacity_TB__c + '</td><td>' + assets.Number_of_Ports__c + '</td><td>' + assets.DPAD_Isilon_Rep_Name__c + '</td></tr>'; //#1483 - added new Column "DPAD/Isilon Rep Name"                                                                                           
                        }
                        assetDetails = '<table border="1" width="100%">' + heading + assetDetails + '</table>';
                    }
                }              
            }
            
            //R2R GFS Modifications End Req ID :9179 - Sneha Jain 08-Nov-2013   

            List<Trade_Ins_Competitive_Swap__c> lstTradeIns = new list<Trade_Ins_Competitive_Swap__c>();
            //Map<string,integer> maplinkedAssetType = new Map<string,integer>();
            //recordtype rectype = [Select Id,DeveloperName from recordType where developerName ='Trade_Ins'];
            Map<string, Map<String, integer>> maplinkedAssetType = getLinkedAssetTypeOnOpportunity(lstRelAsset);
            if(lstGFSObj.size()<=0)
            {
                
                
                // create record;
                if(!maplinkedAssetType.isempty()){
                    for(string regType :maplinkedAssetType.keyset()){
                        Trade_Ins_Competitive_Swap__c tics = new Trade_Ins_Competitive_Swap__c();
                        tics.Related_Opportunity__c = Optyid;   
                        tics.Sales_Rep__c = userAFM;
                        tics.Registration_Type__c=regType;
                        //tics.RecordTypeid = rectype.id;
                        if(regType=='Connectrix Trade In'){
                            tics.Uplift__c = maplinkedAssetType.get(regType).get('Swap_Value');
                        }
                        else{
                            tics.Swap_Value__c = maplinkedAssetType.get(regType).get('Swap_Value');
                        }
                        /* @WR-991 : Calulating total GFS Rep Incentive */
                        tics.Total_GFS_Rep_Incentive__c = maplinkedAssetType.get(regType).get('GFS_Rep_Inc');
                       lstTradeIns.add(tics);
                    }   
                }       
                try
                {
                    insert lstTradeIns;
                    //shipra
                    
                    String strBaseURL = URL.getSalesforceBaseUrl().getHost();
                    String strTradeInURL = 'https://' + strBaseURL + '/' +lstTradeIns[0].id;
                    String strOppURL = 'https://' + strBaseURL + '/' +opp.id;
                    
                    OAR_Member_Added__c oppMemAdd= new OAR_Member_Added__c();
                    oppMemAdd.Text_1__c=opp.Id;
                    oppMemAdd.Text_2__c=opp.Name;
                    oppMemAdd.Text_3__c=opp.Opportunity_Number__c;
                    oppMemAdd.Text_4__c=opp.Account_Name1__c;
                    oppMemAdd.Text_5__c=opp.StageName;
                    oppMemAdd.Text_6__c=opp.District_Manager__r.Name;
                    oppMemAdd.Text_7__c=opp.Country__c;
                    oppMemAdd.Text_8__c=opp.Quote_Cart_Number__c;
                    oppMemAdd.Text_9__c=System.label.SWAP_Registration_Request;
                    oppMemAdd.Text_10__c=lstTradeIns[0].ID;
                    oppMemAdd.Number_1__c=opp.Amount;
                    oppMemAdd.Text_11__c=opp.Products__c;
                    //1673
                    oppMemAdd.Text_12__c=opp.Re_Registration_Reason__c;
                    // End of 1673
                    oppMemAdd.Account_1__c=opp.Partner__c;
                    //oppMemAdd.Account_2__c=opp.Tier_2_Partner__c;
                    oppMemAdd.Condition__c='GFS Registration';
                    oppMemAdd.User_1__c=opp.Opportunity_Owner__c;
                    oppMemAdd.Team_Member_Added__c=lstTradeIns[0].Sales_Rep__c;
                    oppMemAdd.OpptyId_For_Team_Member__c=opp.Id;
                    oppMemAdd.Date_1__c=opp.CloseDate;
                    
                    //R2R GFS Modifications Req ID :9179 - Sneha Jain 08-Nov-2013
                    //Rich Text Field added to include all the Asset records on GFS Registration email
                    oppMemAdd.RichText_1__c = assetDetails;
                    
                    System.debug('opp.Account_TheaterText__c###'+opp.Account_TheaterText__c);
                    
                    if(opp.Account_TheaterText__c=='Americas')
                    {
                        if(strAmericasTheater!=null)
                        {
                            oppMemAdd.Email_notification_1__c=DataValueMap.get('America Theater Email Notification').DataValue__c;
                        }
                    }
                    else if(opp.Account_TheaterText__c=='EMEA')
                    {
                        if(strEMEATheater!=null)
                        {
                            oppMemAdd.Email_notification_1__c=DataValueMap.get('EMEA Theater Email Notification').DataValue__c;
                        }
                    }
                    
                    else if(opp.Account_TheaterText__c=='APJ')
                    {
                        if(strAPJTheater!=null)
                        {
                            oppMemAdd.Email_notification_1__c=DataValueMap.get('APJ Theater Email Notification').DataValue__c;
                        }
                    }
                    
                    blnstaticEmailAlert=true;
                    lstteam.add(oppMemAdd);

//Code by Avinash K begins...

                   // String strBaseURL = URL.getSalesforceBaseUrl().getHost();
                   // String strTradeInURL = 'https://' + strBaseURL + '/' +tics.id;
                   // String strOppURL = 'https://' + strBaseURL + '/' +opp.id;

                    
                    if(!blnStaticVariableInsert)
                    {
                        blnStaticVariableInsert = true;
                        FeedItem fi = new FeedItem();
                        //fi.Title = 'Return to the GFS Competitive Swap page';
                        //fi.LinkURL = strTradeInURL;
                        fi.ParentId = opp.id;
                        fi.Body = System.Label.MOJO_SWAP_Registration_Chatter_Post_Part_1 + opp.Account_Name1__c + ' : ' + strUserName + '\n \n';
                        fi.Body += System.Label.MOJO_SWAP_Registration_Chatter_Post_Part_2 + strOppURL; 


                        insert fi;
                    }

//Code by Avinash ends.


                    
                }
                Catch(DmlException e)
                {
                }
                    
            }           
            
            else
            {
                system.debug('else');                               
                
//Code by Avinash K begins...

                    String strBaseURL = URL.getSalesforceBaseUrl().getHost();

                    List<FeedItem> lstFeedItem = new List<FeedItem>();
                    
//Code by Avinash ends.

                for(Trade_Ins_Competitive_Swap__c t : lstGFSObj)
                {
                    if(!maplinkedAssetType.isempty()){
                        if(maplinkedAssetType.containskey(t.Registration_Type__c)){
                            t.Registration_ID__c = null;                    
                            t.Sales_Rep__c = userAFM;
                            if(maplinkedAssetType.get(t.Registration_Type__c) !=null && t.Registration_Type__c.contains('Connectrix')){
                                t.Uplift__c = maplinkedAssetType.get(t.Registration_Type__c).get('Swap_Value');
                                t.Swap_Value__c  = null;
                            }
                            if(maplinkedAssetType.get(t.Registration_Type__c) !=null && !t.Registration_Type__c.contains('Connectrix')){
                                t.Swap_Value__c= maplinkedAssetType.get(t.Registration_Type__c).get('Swap_Value');
                                t.Uplift__c=null;
                            }
                            /* @WR-991 : Calulating total GFS Rep Incentive*/
                            t.Total_GFS_Rep_Incentive__c = maplinkedAssetType.get(t.Registration_Type__c).get('GFS_Rep_Inc');
                            maplinkedAssetType.remove(t.Registration_Type__c);
                        }                        
                    }
                    else{
                         t.Swap_Value__c  = null;
                         t.Uplift__c=null;
                    } 

//Code by Avinash K begins...
                    
                    
                    if(!blnStaticVariable)
                    {
                        blnStaticVariable = true;
                        String strTradeInURL = 'https://' + strBaseURL + '/' +t.id;
                        String strOppURL = 'https://' + strBaseURL + '/' +t.Related_Opportunity__r.id;

                        FeedItem fi = new FeedItem();
                        //fi.Title = 'Return to the GFS Competitive Swap page';
                        //fi.LinkURL = strTradeInURL;
                        fi.ParentId = t.Related_Opportunity__r.id;
                        fi.Body = 'UPDATE ' + System.Label.MOJO_SWAP_Registration_Chatter_Post_Part_1 + t.Related_Opportunity__r.Account_Name1__c + ' : ' + strUserName + '\n \n';
                        fi.Body += System.Label.MOJO_SWAP_Registration_Chatter_Post_Part_2 + strOppURL; 

                        lstFeedItem.add(fi);
                    }
                    

//Code by Avinash ends.
                }
                if(!maplinkedAssetType.isempty()){
                    lstTradeIns.clear();
                   for(string regType :maplinkedAssetType.keyset()){
                       Trade_Ins_Competitive_Swap__c tics = new Trade_Ins_Competitive_Swap__c();
                       tics.Related_Opportunity__c = Optyid;   
                       tics.Sales_Rep__c = userAFM;
                       tics.Registration_Type__c=regType;
                       //tics.RecordTypeId = rectype.id;
                       if(regType=='Connectrix Trade In'){
                        tics.Uplift__c = maplinkedAssetType.get(regType).get('Swap_Value');
                       }
                       else{
                       tics.Swap_Value__c = maplinkedAssetType.get(regType).get('Swap_Value');
                       }
                       /* @WR-991 : Calulating total GFS Rep Incentive */
                       tics.Total_GFS_Rep_Incentive__c = maplinkedAssetType.get(regType).get('GFS_Rep_Inc');
                       lstTradeIns.add(tics);
                    }   
                }

                //update record
                try
                {
                    system.debug('lstGFSObj update'+lstGFSObj);
                    update lstGFSObj;
                    if(!blnstaticEmailAlert)
                    {
                        OAR_Member_Added__c oppMemAdd1= new OAR_Member_Added__c();
                        oppMemAdd1.Text_1__c=opp.Id;
                        oppMemAdd1.Text_2__c=opp.Name;
                        oppMemAdd1.Text_3__c=opp.Opportunity_Number__c;
                        oppMemAdd1.Text_4__c=opp.Account_Name1__c;
                        oppMemAdd1.Text_5__c=opp.StageName;
                        oppMemAdd1.Text_6__c=opp.District_Manager__r.Name;
                        oppMemAdd1.Text_7__c=opp.Country__c;
                        oppMemAdd1.Text_8__c=opp.Quote_Cart_Number__c;
                        oppMemAdd1.Text_9__c=System.label.Updated_SWAP_Registration_Request;
                        oppMemAdd1.Text_10__c=lstGFSObj[0].id;
                        oppMemAdd1.Number_1__c=opp.Amount;
                        oppMemAdd1.Text_11__c=opp.Products__c;
                        //1673
                        oppMemAdd1.Text_12__c=opp.Re_Registration_Reason__c;
                        // End of 1673
                        oppMemAdd1.Account_1__c=opp.Partner__c;
                        //oppMemAdd.Account_2__c=opp.Tier_2_Partner__c;
                        oppMemAdd1.Condition__c='GFS Registration';
                        oppMemAdd1.User_1__c=opp.Opportunity_Owner__c;
                        oppMemAdd1.Team_Member_Added__c=lstGFSObj[0].Sales_Rep__c;
                        oppMemAdd1.Date_1__c=opp.CloseDate;
                        oppMemAdd1.OpptyId_For_Team_Member__c=opp.Id;
                        
                        //R2R GFS Modifications Req ID :9179 - Sneha Jain 08-Nov-2013
                        //Rich Text Field added to include all the Asset records on GFS Registration email
                        oppMemAdd1.RichText_1__c = assetDetails;
                        
                        /*if(opp.Account_TheaterText__c=='America')
                        {
                            oppMemAdd1.Email_notification_1__c='sangeeta.hegde@emc.com';
                        }
                        else if(opp.Account_TheaterText__c=='EMEA')
                        {
                            oppMemAdd1.Email_notification_1__c='swarup.kemparaju@emc.com';
                        }
                        else if(opp.Account_TheaterText__c=='APJ')
                        {
                            oppMemAdd1.Email_notification_1__c='aditi.bharadwaj@emc.com';
                        }
                        */
                        if(opp.Account_TheaterText__c=='Americas')
                        {
                            if(strAmericasTheater!=null)
                            {
                                oppMemAdd1.Email_notification_1__c=DataValueMap.get('America Theater Email Notification').DataValue__c;
                            }
                        }
                        else if(opp.Account_TheaterText__c=='EMEA')
                        {
                            if(strEMEATheater!=null)
                            {
                                oppMemAdd1.Email_notification_1__c=DataValueMap.get('EMEA Theater Email Notification').DataValue__c;
                            }
                        }
                        else if(opp.Account_TheaterText__c=='APJ')
                        {
                            if(strAPJTheater!=null)
                            {
                                oppMemAdd1.Email_notification_1__c=DataValueMap.get('APJ Theater Email Notification').DataValue__c;
                            }
                        }
                        lstteam.add(oppMemAdd1);
                        }
                    
                    
//Code by Avinash K begins...
                    if (lstFeedItem != null && lstFeedItem.size() > 0) 
                    {
                        insert lstFeedItem;
                    }
//Code by Avinash ends.

                }
                Catch(DmlException e)
                {
                }
            }
                if(!lstTradeIns.isempty()){
                    try{
                        insert lstTradeIns;
                    }
                    catch(exception e){
                    
                    }
                }
               if(lstteam!=null && lstteam.size()>0)
                {
                    Enterprise_Under_Pen_Email_Notification.insertEmailMemberAlertRecords(lstteam);
                    sendEmail(lstteam);
                }
                PageReference opty= new PageReference ('/'+Optyid );                
                return opty;        
        }
        return null;
    }
    
    public PageReference Cancel()
    {
        PageReference optypg= new PageReference ('/'+Optyid );                              
        return optypg;  
    }
    
    public void sendEmail(List<OAR_Member_Added__c> oarLst){
    List<String> toAdd = new List<String>();
    List<String> ccAdd = new List<String>();
    List<Id> prId = new List<Id>();
    List<String> idList = new List<String>(); 
    String template;
    string connetrixEmail = datavaluemap.get('ConnectrixMail').datavalue__c;
    System.debug('oarLst--->'+oarLst[0].Id);
    
  List<AccountTeamMember> lstAccountMemebr = [Select a.UserId,a.TeamMemberRole  From AccountTeamMember a where a.AccountId=:opp.accountid];
      for(AccountTeamMember aObj :lstAccountMemebr){
        if(aObj.TeamMemberRole == System.Label.GFS_Area_Finance_Manager || 
            aObj.TeamMemberRole == System.Label.GFS_Channel_Finance_Manager || 
            aObj.TeamMemberRole == System.Label.GFS_Geo_Finance_Manager || 
            aObj.TeamMemberRole == System.Label.GFS_Regional_Finance_Manager || 
            aObj.TeamMemberRole == System.Label.GFS_Regional_Fin_Specialist || 
            aObj.TeamMemberRole == System.Label.GFS_Regional_Finance_Specialist ){//Bhanu: E.002: added "GFS_Regional_Fin_Specialist" in condition
        idList.add(aObj.UserId);}
      }
        //AFM user in email CC list
        List<Trade_Ins_Competitive_Swap__c> GFSList = new List<Trade_Ins_Competitive_Swap__c>();
        List<String> userList = new List<String>();
        if(opp.Id != null)
        {
            GFSList = [select g.Id,g.afm__c,g.afm__r.Id from Trade_Ins_Competitive_Swap__c g where g.Related_Opportunity__c =: opp.Id];
            if(GFSList.size() >0 && !GFSList.isempty())
            {
                for(Trade_Ins_Competitive_Swap__c gfsUser : GFSList)
                {
                    userList.add(gfsUser.afm__r.Id);
                }   
            }
        }
   
        if(opp.Account_TheaterText__c=='Americas')
          {
          //  toAdd.add('RFS_Americas1@emc.com');
            toAdd.add(System.Label.Transformation_RFS_Americas);
          }
         else if(opp.Account_TheaterText__c=='EMEA')
          {
          //  toAdd.add('RFSEMEA1@emc.com');
            toAdd.add(System.Label.Transformation_RFSEMEA);
          }
          else if(opp.Account_TheaterText__c=='APJ')
          {
          //  toAdd.add('RFSAPJ1@emc.com');
            toAdd.add(System.Label.Transformation_RFSAPJ);
          }
          //toAdd.add('colleen.m.davis@accenture.com');
          //toAdd.add('Deidra.Zarrella@emc.com');
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage(); 
        template = 'Transformation GFS Registration';
       //template ='Register With GFS';
        
        if(toAdd.size() > 0){
            Message.setToAddresses(toAdd);   
            system.debug('toAdd--->'+toAdd);
            ccAdd.addAll(getEmailAddresses(idList));
            //If AFM user present, include in Email CC List
            if(userList.size() >0 && !userList.isEmpty())
            {
                ccAdd.addAll(getEmailAddresses(userList));
            }
            if(isConnectrixProductAdded){
                ccAdd.add(connetrixEmail);
            }
            if(ccAdd.size() > 0){
              Message.setCcAddresses(ccAdd);
            }
            system.debug('ccAdd--->'+getEmailAddresses(idList));
            Message.setTargetObjectId(UserInfo.getUserId()); //The first Team Member ID found, this email address will not be in the emailList
    
            Message.setWhatId(oarLst[0].Id); 
            Message.setTemplateId([select id from EmailTemplate where Name =: template].id); 
            Message.SaveAsActivity = false;
            Messaging.sendEmail(new Messaging.singleemailMessage[] {message}); 
       } 
   }
   
   
    //To fetch the group member email id
   
   private List<String> getEmailAddresses(List<String> idList) {   
        List<String> mailToAddresses = new List<String>(); 
      if(idList.size() > 0){
          User[] usr = [SELECT email FROM user WHERE id IN :idList];    
          for(User u : usr) {          
          mailToAddresses.add(u.email);   
           }      
      }
        System.debug('mailToAddresses-->'+mailToAddresses); 
        return mailToAddresses;   
    }
    
    public static void updateReRegistrationDetails(List<Opportunity>Opportunities,String reason, boolean updateOpportunity){
        for(Opportunity opp:Opportunities){
        
            opp.Re_Registration_Reason__c=reason;
            opp.Re_Registration_Required_on__c= system.today();
        }
        if(updateOpportunity){
            update Opportunities;
        }
        
    }
    
    /* @WR-991 : Modified method to get total swap value and gfs rep incentive */
    /* @INC0716388 : Modified method to fix the uncondtional GFS record creation in the opportunity*/
    public Map<string, Map<String, integer>> getLinkedAssetTypeOnOpportunity(List<Opportunity_Asset_Junction__c> lstLinkedOpptyAsset){
        integer totalSwapValue = 0;
        integer totalGFSRepInc = 0;
        Map<String, Integer> mapValues = new Map<String, Integer>();
        mapValues.put('Swap_Value',totalSwapValue);
        mapValues.put('GFS_Rep_Inc',totalGFSRepInc);
        Map<string, Map<String, integer>> mapLinkingDetailsWithSwapValue = new Map<string, Map<String, integer>>();
        /*mapLinkingDetailsWithSwapValue.put('EMC Trade In', mapValues);
        mapLinkingDetailsWithSwapValue.put('Connectrix Trade In', mapValues);
        mapLinkingDetailsWithSwapValue.put('Competitive Swaps', mapValues);*/
        for(Opportunity_Asset_Junction__c junctionObj : lstLinkedOpptyAsset){
            if(junctionObj.Related_Asset__r.RecordType.DeveloperName=='EMC_Install' && junctionObj.Related_Asset__r.Product_Family__c !='CONNECTRIX'){
        //@INC0716388 : Moved Map "mapLinkingDetailsWithSwapValue" inside the IF statement .
        //mapLinkingDetailsWithSwapValue.put('EMC Trade In', mapValues);
        if(mapLinkingDetailsWithSwapValue !=null && mapLinkingDetailsWithSwapValue.size()>0 && mapLinkingDetailsWithSwapValue.containsKey('EMC Trade In')){
          totalSwapValue = mapLinkingDetailsWithSwapValue.get('EMC Trade In').get('Swap_Value');
          totalGFSRepInc = mapLinkingDetailsWithSwapValue.get('EMC Trade In').get('GFS_Rep_Inc');
          if(totalSwapValue==null){
            totalSwapValue = 0;
          }
          if(totalGFSRepInc == null) {
            totalGFSRepInc = 0;
          }
        }
        else{
          totalSwapValue = 0;
          totalGFSRepInc = 0;
        }
        if(junctionObj.Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c != null){
          totalSwapValue = totalSwapValue + integer.valueof(junctionObj.Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c);
        }  
        if(junctionObj.Related_Asset__r.GFS_Rep_Incentive_Num__c != null){
          totalGFSRepInc = totalGFSRepInc + integer.valueof(junctionObj.Related_Asset__r.GFS_Rep_Incentive_Num__c);
        }
        mapValues = new Map<String, Integer>();
        mapValues.put('Swap_Value',totalSwapValue);
        mapValues.put('GFS_Rep_Inc',totalGFSRepInc);
        mapLinkingDetailsWithSwapValue.put('EMC Trade In',mapValues);
            }
            if(junctionObj.Related_Asset__r.Product_Family__c =='CONNECTRIX'){
        //@INC0716388 : Moved Map "mapLinkingDetailsWithSwapValue" inside the IF statement .
        //mapLinkingDetailsWithSwapValue.put('Connectrix Trade In', mapValues);
        if(mapLinkingDetailsWithSwapValue !=null && mapLinkingDetailsWithSwapValue.size()>0 && mapLinkingDetailsWithSwapValue.containsKey('Connectrix Trade In')){
          totalSwapValue = mapLinkingDetailsWithSwapValue.get('Connectrix Trade In').get('Swap_Value');
          totalGFSRepInc = mapLinkingDetailsWithSwapValue.get('Connectrix Trade In').get('GFS_Rep_Inc');
          if(totalSwapValue==null){
            totalSwapValue = 0;
          }
          if(totalGFSRepInc == null) {
            totalGFSRepInc = 0;
          }
        }
        else{
          totalSwapValue = 0;
          totalGFSRepInc = 0;
        }  
        if(junctionObj.Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c != null){
          totalSwapValue = totalSwapValue + integer.valueof(junctionObj.Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c);
        }  
        if(junctionObj.Related_Asset__r.GFS_Rep_Incentive_Num__c != null){
          totalGFSRepInc = totalGFSRepInc + integer.valueof(junctionObj.Related_Asset__r.GFS_Rep_Incentive_Num__c);
        }
        mapValues = new Map<String, Integer>();
        mapValues.put('Swap_Value',totalSwapValue);
        mapValues.put('GFS_Rep_Inc',totalGFSRepInc);            
        mapLinkingDetailsWithSwapValue.put('Connectrix Trade In',mapValues);            
               
            }
            if(junctionObj.Related_Asset__r.RecordType.DeveloperName=='Competitive_Install' && junctionObj.Related_Asset__r.Product_Family__c !='CONNECTRIX'){
        //@INC0716388 : Moved Map "mapLinkingDetailsWithSwapValue" inside the IF statement .
        //mapLinkingDetailsWithSwapValue.put('Competitive Swaps', mapValues);
        if(mapLinkingDetailsWithSwapValue !=null && mapLinkingDetailsWithSwapValue.size()>0 && mapLinkingDetailsWithSwapValue.containsKey('Competitive Swaps')){
          totalSwapValue = mapLinkingDetailsWithSwapValue.get('Competitive Swaps').get('Swap_Value');
          totalGFSRepInc = mapLinkingDetailsWithSwapValue.get('Competitive Swaps').get('GFS_Rep_Inc');
          if(totalSwapValue==null){
            totalSwapValue = 0;
          }
          if(totalGFSRepInc == null) {
            totalGFSRepInc = 0;
          }
        }  
        else{
          totalSwapValue = 0;
          totalGFSRepInc = 0;
        }  
        if(junctionObj.Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c != null){
          totalSwapValue = totalSwapValue + integer.valueof(junctionObj.Related_Asset__r.Swap_Trade_In_Cost_Relief_Value__c);
        }  
        if(junctionObj.Related_Asset__r.GFS_Rep_Incentive_Num__c != null){
          totalGFSRepInc = totalGFSRepInc + integer.valueof(junctionObj.Related_Asset__r.GFS_Rep_Incentive_Num__c);
        }
        mapValues = new Map<String, Integer>();
        mapValues.put('Swap_Value',totalSwapValue);
        mapValues.put('GFS_Rep_Inc',totalGFSRepInc);
        mapLinkingDetailsWithSwapValue.put('Competitive Swaps',mapValues);                 
            }
        }
        system.debug('mapLinkingDetailsWithSwapValue : '+mapLinkingDetailsWithSwapValue);
        return mapLinkingDetailsWithSwapValue;

    }
}