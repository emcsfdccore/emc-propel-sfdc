/*=======================================================================================================+
|  HISTORY  |                                                                           
|  DATE          DEVELOPER        WR                    DESCRIPTION                               
|  ====          =========        ==                    =========== 
| 12-Jul-2011    Anirudh Singh    Helper Class          This test class is used to Create Account Data for Test Classes
| 27-Dec-2013    Srikrishna SM    Helper Class          Method is added to create single Acocunt
| 17-Feb-2015    Vinod Jetti      #1649                 Method called 'testclassUtils.getHubInfo()' 
| 15-Dec-2014    Bhanuprakash     PROPEL                Changed the 'Party_Number__c' value as it's changes as unique value
+=====================================================================================================*/

public class AccountAndProfileTestClassDataHelper {
    
    public static Account createSimpleAccount(){
        Account acc1 = new Account();
        acc1.Name = 'Test Account 1';
        acc1.BillingCountry = 'United States';
        acc1.BillingCity = 'Chicago';
        acc1.Core_Quota_House_Name__c='test';
        acc1.Party_Number__c = AccountAndProfileTestClassDataHelper.generateRandomString(15);
        return acc1;
    }

    public static list<Account> CreateCustomerAccount(){
        
        Map<String,Schema.RecordTypeInfo> recordTypes;
        Id accRecordTypeId;
        
        if(Account.sObjectType != null && Account.sObjectType.getDescribe() != null && 
            Account.sObjectType.getDescribe().getRecordTypeInfosByName() != null)
            recordTypes = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
        
        if(recordTypes!= null && recordTypes.get('Customer Account Record Type') != null &&
            recordTypes.get('Customer Account Record Type').getRecordTypeId() != null)
            accRecordTypeId = recordTypes.get('Customer Account Record Type').getRecordTypeId(); 
        
        List<Account> lstAccount = New List<Account>();
        
        //Creation of Customer Accounts
        Account objAccount = new Account();
        objAccount.name = 'UNITTESTAcc';
        objAccount.Party_Number__c = '1234';        
        objAccount.BillingCountry ='Colombia';
        objAccount.Synergy_Account_Number__c = '10';
        objAccount.Status__c='A';
        objAccount.State_Province_Local__c = 'testState1';
        objAccount.Street_Local__c='test1';
        objAccount.Zip_Postal_Code_Local__c='23454';
        //objAccount.DealReg_Address_Local__c='test2';
        objAccount.Party_Number__c = AccountAndProfileTestClassDataHelper.generateRandomString(15);

        lstAccount.add(objAccount);
        
        Account objAccount1 = new Account();
        objAccount1.name = 'UNITTESTAcc1';
        objAccount1.Party_Number__c = '12345';        
        objAccount1.BillingCountry ='Colombia';
        objAccount1.Synergy_Account_Number__c = '10';
        objAccount1.Status__c='A';
        objAccount1.State_Province_Local__c = 'testState2';
        objAccount1.Street_Local__c='test1';
        objAccount1.Zip_Postal_Code_Local__c='23454';
        //objAccount1.DealReg_Address_Local__c='test2';
        objAccount1.Party_Number__c = AccountAndProfileTestClassDataHelper.generateRandomString(15);

        lstAccount.add(objAccount1);
        
        Account objAccount2 = new Account();
        objAccount2.name = 'UNITTESTAcc1';
        objAccount2.Party_Number__c = '123456';        
        objAccount2.BillingCountry ='Colombia';
        objAccount2.Synergy_Account_Number__c = '10';
        objAccount2.Status__c='A';
        objAccount2.State_Province_Local__c = 'testState3';
        objAccount2.Street_Local__c='test1';
        objAccount2.Zip_Postal_Code_Local__c='23454';
        //objAccount2.DealReg_Address_Local__c='test2';
        objAccount2.Party_Number__c = AccountAndProfileTestClassDataHelper.generateRandomString(15);

        lstAccount.add(objAccount2);
        
        Account objAccount3 = new Account();
        objAccount3.name = 'UNITTESTAcc1';
        objAccount3.Party_Number__c = '123457';        
        objAccount3.BillingCountry ='Colombia';
        objAccount3.Synergy_Account_Number__c = '10';
        objAccount3.Status__c='A';
        objAccount3.State_Province_Local__c = 'testState3';
        objAccount3.Street_Local__c='test1';
        objAccount3.Zip_Postal_Code_Local__c='23454';
        //objAccount3.DealReg_Address_Local__c='test2';
        objAccount3.Party_Number__c = AccountAndProfileTestClassDataHelper.generateRandomString(15);

        lstAccount.add(objAccount3);
        
        return lstAccount;    
    }
     
    public static list<Account> CreatePartnerAccount(){
        Hub_Info__c objHubInfo = testclassUtils.getHubInfo();
        objHubInfo.Golden_Site_Identifier__c = Integer.ValueOf(AccountAndProfileTestClassDataHelper.generateRandomNumber());
        insert objHubInfo;
        List<Account> lstPartnerAccount = New List<Account>();
        
         //Creation of Partner Accounts
         Account objAccount = new Account();
         objAccount.name = 'TestPartnerAcc';
         objAccount.BillingCountry ='Colombia';
         objAccount.Synergy_Account_Number__c = '10';
         objAccount.Type = 'Partner';
         objAccount.Lead_Oppty_Enabled__c = true;
         objAccount.Partner_Type__c ='Distributor';
         objAccount.Hub_Info__c = objHubInfo.Id;
         objAccount.PROFILED_ACCOUNT_FLAG__c = true;
         objAccount.Party_Number__c = AccountAndProfileTestClassDataHelper.generateRandomString(15);

         lstPartnerAccount.add(objAccount);
         Hub_Info__c objHubInfo1 = testclassUtils.getHubInfo();
         objHubInfo1.Golden_Site_Identifier__c = AccountAndProfileTestClassDataHelper.generateRandomNumber();
         insert objHubInfo1;
         Account objAccount1 = new Account();
         objAccount1.name = 'TestPartnerAcc1';
         objAccount1.Party_Number__c = '12345';            
         objAccount1.BillingCountry ='Colombia';
         objAccount1.Synergy_Account_Number__c = '10';
         objAccount1.Type = 'Partner';
         objAccount1.Lead_Oppty_Enabled__c = true;
         objAccount1.Partner_Type__c ='Distributor';
         objAccount1.Hub_Info__c = objHubInfo1.Id;
         objAccount1.PROFILED_ACCOUNT_FLAG__c = true;
         objAccount1.Party_Number__c = AccountAndProfileTestClassDataHelper.generateRandomString(15);

         lstPartnerAccount.add(objAccount1);
         
         Hub_Info__c objHubInfo2 = testclassUtils.getHubInfo();
         objHubInfo2.Golden_Site_Identifier__c = AccountAndProfileTestClassDataHelper.generateRandomNumber();
         insert objHubInfo2;
         Account objAccount2 = new Account();
         objAccount2.name = 'TestPartnerAcc2';
         objAccount2.Party_Number__c = '12346';            
         objAccount2.BillingCountry ='Colombia';
         objAccount2.Synergy_Account_Number__c = '10';
         objAccount2.Type = 'Partner';
         objAccount2.Lead_Oppty_Enabled__c = true;
         objAccount2.Partner_Type__c ='Distribution VAR';
         objAccount2.Hub_Info__c = objHubInfo2.Id;
         objAccount2.PROFILED_ACCOUNT_FLAG__c = true;
         objAccount2.Party_Number__c = AccountAndProfileTestClassDataHelper.generateRandomString(15);

         lstPartnerAccount.add(objAccount2);
         
         Hub_Info__c objHubInfo3 = testclassUtils.getHubInfo();
         objHubInfo3.Golden_Site_Identifier__c = AccountAndProfileTestClassDataHelper.generateRandomNumber();
         insert objHubInfo3;
         Account objAccount3 = new Account();
         objAccount3.name = 'TestPartnerAcc3';
         objAccount3.Party_Number__c = '12347';            
         objAccount3.BillingCountry ='Colombia';
         objAccount3.Synergy_Account_Number__c = '10';
         objAccount3.Type = 'Partner';
         objAccount3.Lead_Oppty_Enabled__c = false;
         objAccount3.Partner_Type__c ='Distribution VAR';
         objAccount3.Hub_Info__c = objHubInfo3.Id;
         objAccount3.PROFILED_ACCOUNT_FLAG__c = true;
         objAccount3.Party_Number__c = AccountAndProfileTestClassDataHelper.generateRandomString(15);

         lstPartnerAccount.add(objAccount3);
         
         return lstPartnerAccount;
    }
    
     public static list<Account> CreateT2PartnerAccount(){
         List<Account> lstT2PartnerAccount= New List<Account>();
         
         Account objAccount2 = new Account();
         objAccount2.name = 'TestPartnerAcc2';
         objAccount2.Party_Number__c = '1234';            
         //objAccount2.BillingCountry ='Colombia';
         objAccount2.Synergy_Account_Number__c = '10';
         objAccount2.Type = 'Partner';
         objAccount2.Lead_Oppty_Enabled__c = true;
         objAccount2.Partner_Type__c ='Distribution VAR';
         objAccount2.PROFILED_ACCOUNT_FLAG__c = true;
         objAccount2.Party_Number__c = AccountAndProfileTestClassDataHelper.generateRandomString(15);

         lstT2PartnerAccount.add(objAccount2);
         
         Account objAccount3 = new Account();
         objAccount3.name = 'TestPartnerAcc3';
         objAccount3.Party_Number__c = '1234';            
         objAccount3.BillingCountry ='Colombia';
         objAccount3.Synergy_Account_Number__c = '10';
         //objAccount3.Type = 'Partner';
         objAccount3.Lead_Oppty_Enabled__c = false;
         objAccount3.Partner_Type__c ='Distribution VAR';
         objAccount3.PROFILED_ACCOUNT_FLAG__c = true;
         objAccount3.Party_Number__c = AccountAndProfileTestClassDataHelper.generateRandomString(15);

         lstT2PartnerAccount.add(objAccount3);
         
         return lstT2PartnerAccount;
     }
     
    public static Account_Groupings__c createAccountGrouping(Id accountid){
      Account_Groupings__c accGrp = new Account_Groupings__c();
            accGrp.Account__c = accountid ;
            accGrp.Name = 'Test-AccountGrp' ;
         return accGrp ;      
    }  

    //Added for Fixing party number issue

    public static String generateRandomString(Integer len) {
    final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    String randStr = '';
    while (randStr.length() < len) {
       Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
       randStr += chars.substring(idx, idx+1);
    }
    return randStr; 
}

// Added for fixing Golden Site Identifier issue
     public static Integer generateRandomNumber() {
         Integer ranNum = Crypto.getRandomInteger();
         return ranNum;
    }

}