/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE       DEVELOPER             WR       DESCRIPTION                                
|
   ====       =========             ==       ===========  
|  04-Apr-2013  Prachi                        Created 
|  26.12.2013   Bhanu Prakash     WR319143    Re-written test class to improve performance.
|  23.07.2014   Bisna V P         Updated to prevent 'Close Date must be in the future for all Open Opportunities' error
+===========================================================================*/ 

@isTest
private class BatchOpportunityFlipRecordType_TC {
//Edited by Bhanu - Start 
        BatchOpportunityFlipRecordType_TC(){
            List<Account> acctLst = new List<Account>();
            User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
    
            System.runAs(insertUser){
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.profilesCSData();
                CustomSettingDataHelper.dealRegistrationCSData();
                acctLst = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
                insert acctLst;
            }
            List<Opportunity> lstOpp = new List<Opportunity>();
            lstOpp = OpportunityTestClassDataHelper.createOpptys(acctLst[0],null);
            for(Integer i=0 ; i<lstOpp.size(); i++){
            lstOpp[i].CloseDate = system.today().addDays(+10);
            }
            System.runAs(insertUser){
                insert lstOpp;
            }
        }
//Edited by Bhanu - End
        static testMethod void BatchOpportunityFlipRecordType(){
            BatchOpportunityFlipRecordType_TC bOFT = new BatchOpportunityFlipRecordType_TC();
            Database.BatchableContext bc;
            Set <Id> setAcc=new Set <Id>();
            Map<String,CustomSettingDataValueMap__c> mapCustomSettingDataValueMap = CustomSettingDataValueMap__c.getall();
            CustomSettingDataValueMap__c OpptyRecordTypes = (mapCustomSettingDataValueMap.get('Booked_Part_Perf_Rating_Opp_RecType'));
            String query;
            String strRecType = OpptyRecordTypes.DataValue__c;
            String strAccountIds = '(';
            List <Account> lstAcc=new List <Account>();
            
            Test.StartTest(); 
            lstAcc=[Select id from Account limit 5];
            for(integer k=0;k<1;k++){
                setAcc.add(lstAcc[k].id);
                strAccountIds = strAccountIds + '\'' + lstAcc[k].id + '\',';
            }
            if(strAccountIds.endsWith(',')){
                strAccountIds = strAccountIds.substring(0,strAccountIds.length() -1);
                strAccountIds = strAccountIds + ')';
            }
            query='select id,name,stageName,partner__c,recordtypeid,bypass_validation__c,Tier_2_Partner__c from opportunity where stageName=' + '\'Booked\'' + ' and CloseDate>today and recordtypeid != \'' + strRecType + '\' and (Partner__r.Profiled_Account__c in ' + strAccountIds + ' OR Partner__c in ' + strAccountIds + ' OR Tier_2_Partner__r.Profiled_Account__c in ' + strAccountIds + ' OR Tier_2_Partner__c in ' + strAccountIds + ') order by LastModifiedDate desc LIMIT 5';
            
            BatchOpportunityFlipRecordType obj=new BatchOpportunityFlipRecordType (setAcc,false);
            BatchOpportunityFlipRecordType obj1=new BatchOpportunityFlipRecordType (setAcc,true);
            BatchOpportunityFlipRecordType obj2=new BatchOpportunityFlipRecordType (query,false);
            Id batchprocessId =  Database.executeBatch(obj2,10);
            BatchOpportunityFlipRecordType obj3=new BatchOpportunityFlipRecordType (query,true);
            
            List<Opportunity> oppty = [Select o.AccountId ,o.CloseDate, o.Id, o.Partner__c,o.Tier_2_Partner__c, o.Name, o.Opportunity_Owner__c ,o.OwnerId,o.IsClosed from Opportunity o where o.IsClosed = false and o.AccountId<>'0017000000YiRfS' and o.CloseDate>today limit 1];
            //List<Opportunity> oppty1 = [Select o.AccountId ,o.CloseDate, o.Id, o.Partner__c,o.Tier_2_Partner__c, o.Name, o.Opportunity_Owner__c ,o.OwnerId,o.IsClosed from Opportunity o where o.IsClosed = false and o.AccountId<>'0017000000YiRfS' and o.CloseDate<today limit 1];
            List<Opportunity> lstopp=new List<Opportunity>();
            lstopp.add(oppty[0]);
            //lstopp.add(oppty1[0]);
            
            List<SObject> st=new List<SObject>();
            st=(List<Sobject>)lstopp;
            
            //st=obj3.start(bc);
            obj3.execute(bc,st);
            obj3.finish(bc);
            Datetime dt = Datetime.now().addMinutes(1);
            String sch = '0 '+dt.minute()+' * '+dt.day()+' '+dt.month()+' '+' ? '+dt.year();
            System.Schedule('schedule',sch,obj3);
            
            Test.StopTest();
        }
}