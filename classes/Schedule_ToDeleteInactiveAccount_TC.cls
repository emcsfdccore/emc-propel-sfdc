/*=====================================================================================================+

|  HISTORY  |                                                                            

|  DATE          DEVELOPER                WR            DESCRIPTION                               

|  ====          =========                ==            =========== 
                                                       
|11 Sep 2012    Vivekananda             204934          SFA - Bug Fix for Batch Job that Removes Inactive Account Team Members.
                                                              Test Class for Schedule_ToDeleteInactiveAccountTeamMemb                                                
+=====================================================================================================*/
@isTest (seeAllData = false)
private class Schedule_ToDeleteInactiveAccount_TC 
{

    static testMethod void ScheduleToDelete() 
    {
            
            RunInactiveATbatch__c RIA = new RunInactiveATbatch__c(Name='FlagStatus',StatusFlag__c=true); 
            Database.insert(RIA,false);
            ATMLimit__c atlmt = new ATMLimit__c(LimitNumber__c ='100');
            Database.insert(atlmt,false);

            List<CustomSettingDataValueMap__c> lstData = new List<CustomSettingDataValueMap__c>();

            CustomSettingDataValueMap__c startday, endday;
            startday = new CustomSettingDataValueMap__c(Name = 'Start Date Number', Datavalue__c = '0');
            endday = new CustomSettingDataValueMap__c(Name = 'End Date Number', Datavalue__c = '0');

            lstData.add(startday);
            lstData.add(endday);

            insert lstData;
            
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dataValueMapCSData();
            
            List<Profile> profiles = [ Select id from Profile where name = 'System Administrator' limit 1];
            Account acc = UserProfileTestClassDataHelper.getAccount();
            User u = UserProfileTestClassDataHelper.createUser(profiles.get(0).id, null);
            
            insert acc;
            insert u;

            AccountTeamMember atm = new AccountTeamMember(UserId = u.id, AccountId = acc.id);
            insert atm;

            
            

            Schedule_ToDeleteInactiveAccountTeamMemb sh1 = new Schedule_ToDeleteInactiveAccountTeamMemb();        
            String sch = '0 0 23 * * ?';
            
            System.Test.startTest();
            system.schedule('Test Territory Check', sch, sh1);
            System.Test.stopTest();
            
            RIA.StatusFlag__c = true;
            Database.update(RIA,false);

            startday.Datavalue__c = '20';
            endday.Datavalue__c = '10';

            update lstData;

            // System.Test.startTest();
            // system.schedule('Test Territory Check', sch, sh1);
            // System.Test.stopTest();


    }
}