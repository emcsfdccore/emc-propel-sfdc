/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AP_for_Account_With_Profile_Warning_TC {

    private static testMethod void myTest()
    {   
        //Test data for the custom setting
        CustomSettingDataValueMap__c mapDataValueCS = new CustomSettingDataValueMap__c();
        mapDataValueCS.Name = 'House Account For AP Exclusion';
        mapDataValueCS.DataValue__c = 'LA House District,NA Service Provider House District,Territory Assignment House District,Team House District';
        insert mapDataValueCS;
        Test.startTest();
        
        Util.isTestCoverage = true;
        String jobId = System.schedule('firstTest', '0 0 0 15 3 ? 2022', new Schedule_AP_Account_Profile_Warning());
       
        Test.stopTest();
    }
    private static testMethod void testMethod2()
    {   
        //Test data for the custom setting
        CustomSettingDataValueMap__c mapDataValueCS = new CustomSettingDataValueMap__c();
        mapDataValueCS.Name = 'House Account For AP Exclusion';
        mapDataValueCS.DataValue__c = 'LA House District,NA Service Provider House District,Territory Assignment House District,Team House District';
        insert mapDataValueCS;
        Test.startTest();
        
        Util.isTestCoverage = false;
        String jobId = System.schedule('firstTest', '0 0 0 15 3 ? 2022', new Schedule_AP_Account_Profile_Warning());
       
        Test.stopTest();
    }
}