/*=========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER           WR/Req      DESCRIPTION                               
 |  ====            =========           ==          =========== 
 |  10.10.2010      Anand Sharma        #1185       Test class for the unit test of PRM_MassLeadOwnerChange class. 
 |  23.11.2010      Anand Sharma                    Update for better code coverage  
 |  05.07.2011      Anand Sharma                    Change account creation instaed of fetching account
 |   11.01.2012     Anil                           Removed role Id
 |  30.10.2013      Srikrishnadevaraya  #311032     This test class is modified to reduce the processing time
 |  27-Nov-2014     Vivek Barange        #1450     This test class is modified to fix test class failure
 +=========================================================================================================================*/
@isTest
private class PRM_MassLeadOwnerChange_TC {
    
    //use constructor to create the required data to be used across 3 test methods
    private PRM_MassLeadOwnerChange_TC(){
        
        CustomSettingDataHelper.dataValueMapCSData();
        CustomSettingDataHelper.profilesCSData();
        CustomSettingDataHelper.eBizSFDCIntCSData();
        CustomSettingDataHelper.dealRegistrationCSData();
        CustomSettingDataHelper.bypassLogicCSData();
        CustomSettingDataHelper.countryTheaterMapCSData();
        
    }

    static testMethod void startTest1() {
        Contact cont;
        Group g1;
        QueuesObject q1;
        User adminUser = [Select id from User where isActive = true and profile.Name='System Administrator' limit 1];
        system.runAs(adminUser){
            g1 = new Group(Name='group name1', type='Queue');
            insert g1;
            q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Lead');
            insert q1;
        }
        
        PRM_MassLeadOwnerChange_TC newInstance = new PRM_MassLeadOwnerChange_TC();
        List<User> sysAdminUser = [Select id,UserRole.Id,UserRole.Name from User where isActive=true and profile.Name='System Administrator' and UserRole.Name = 'Worldwide EMC Corporate' limit 1];
        ID sysid = [ Select id from Profile where name ='System Administrator - API Only' limit 1].id;
        
        User testUser = new User( email='test-user@fakeemail.com',profileid = sysid ,  UserName='test-userunique2@fakeemail.com', alias='tuser1', CommunityNickName='tuser1', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' );
        insert testUser;
        
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        
        Map<Id,User> lstPartner = new Map<Id,user>();
        list<User> lstuser=new List<User>();
        List<Account> acc = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        System.runAs(insertUser)
        {
        insert acc;
        cont = UserProfileTestClassDataHelper.createContact();
        //#1450 - Added below line to fixed test class failure
        cont.AccountID = acc[0].Id;
        insert cont;
        
        cont.AccountId=acc[0].id;
        cont.Email='test24@emc.com';
        
            update cont;
        }
        User partner1;
        System.runAs(insertUser)
        {
            Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
            String distributorSuperUser = data.get('EMEA Distributor Super User').DataValue__c ;
            Profile amerUserProf = [select Id from Profile where Name=: distributorSuperUser];
            //UserRole userRoles = [Select u.Id, u.Name from UserRole u where u.Name='AVNET TECHNOLOGY SOLUTIONS SANAYI VE TICARET A.S. Partner Executive'];
            partner1 = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.id,null,cont.Id);
        }
        lstuser.add(partner1);
        
        Contact cont1 = UserProfileTestClassDataHelper.createContact();
        System.runAs(insertUser)
        {
        cont1.AccountId=acc[1].id;
        insert cont1;
        cont1.Email='test23@emc.com';
        
            update cont1;
        }
        User partner2;
        System.runAs(insertUser)
        {
            Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
            String distributorSuperUser = data.get('EMEA Distributor Super User').DataValue__c ;
            Profile amerUserProf = [select Id from Profile where Name=: distributorSuperUser];
            //UserRole userRoles = [Select u.Id, u.Name from UserRole u where u.Name='AVNET TECHNOLOGY SOLUTIONS SANAYI VE TICARET A.S. Partner Executive'];
            partner2 = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.id,null,cont1.Id);
        }
        lstuser.add(partner2);
        System.runAs(insertUser)
        {
            if(lstuser.size()>0)
            {
            insert lstuser;
            }
        }
        lstPartner.put(partner2.Id,lstuser[0]);
        lstPartner.put(partner1.Id,lstuser[1]);         
        
        
                              
           
        Account objAccount = createAccount();
                                                                    
        List<Lead> lstLead = New List<Lead>();
                  
        for(Integer i=0; i<2; i++){
            Lead testLead2 = new Lead(LastName='testlm1_'+i, firstname='testfm2', company='test2',DealReg_Deal_Registration__c = true, 
                                  email='test2@test.com', Partner__c = objAccount.Id, Status='New', Sales_Force__c='EMC', city='India', Street ='bangalore',
                                  Lead_Originator__c='Field', LeadSource ='Manual', Originator_Details__c ='Core Referral', Channel__c='Direct', OwnerId=g1.Id);
            lstLead.add(testLead2); 
        }
        System.runAs(testUser){
            insert lstLead; 
        }
                     
        System.debug('lstLead ---->' + lstLead);
        
        PRM_MassLeadOwnerChange objLeadExtn;
        //System.runAs(lstPartner.values().get(0)){     
        System.runAs(sysAdminUser[0]){     
              
        ApexPages.StandardSetController stdCon = new ApexPages.StandardSetController(lstLead); 
        ApexPages.currentpage().getParameters().put('fcf','000000000000000');                 
        // test constructors
        objLeadExtn = new PRM_MassLeadOwnerChange(stdCon); 
        objLeadExtn.FetchLeadRecords();         
        
        stdCon.setSelected(lstLead);            
        objLeadExtn = new PRM_MassLeadOwnerChange(stdCon);
        objLeadExtn.FetchLeadRecords();
        objLeadExtn.objSearchUserQueueCon.selectedOwnerId = lstPartner.values().get(0).Id;
        System.debug('lstLead ---->' + lstLead); 
        objLeadExtn.ChangeOwner() ;
        objLeadExtn.ValidateLeadOwnerRole(lstPartner.values().get(1), lstPartner);
        objLeadExtn.Cancel();
        }     
                              
    } 

    private static testMethod void startTest2(){
        
        Group g2;
        QueuesObject q2;
        User adminUser = [Select id from User where isActive = true and profile.Name='System Administrator' limit 1];
        system.runAs(adminUser){
            g2 = new Group(Name='group name2', type='Queue');
            insert g2;
            q2 = new QueueSObject(QueueID = g2.id, SobjectType = 'Lead');
            insert q2;
        }
        
        PRM_MassLeadOwnerChange_TC newInstance = new PRM_MassLeadOwnerChange_TC();
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
       
        ID sysid = [ Select id from Profile where name ='System Administrator - API Only' limit 1].id;
        
        User testUser = new User( email='test-user@fakeemail.com',profileid = sysid ,  UserName='test-userunique3@fakeemail.com', alias='tuser1', CommunityNickName='tuser1', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' );
        insert testUser;
                    
        User nonpartner;
   
        Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
        String SysAdmin = data.get('System Administrator').Name ;
        Profile SysAdminProf = [select Id from Profile where Name=: SysAdmin];
        //UserRole userRoles = [Select u.Id, u.Name from UserRole u where u.Name='Worldwide EMC Corporate'];
        UserRole newRole = new UserRole();
        newRole.Name = 'testRole123';
        System.runAs(insertUser){
            insert newRole; 
        }
        
        nonpartner = UserProfileTestClassDataHelper.createPortalUser(SysAdminProf.id,newRole.Id,null);
        
        System.runAs(insertUser){
            insert nonpartner;  
        }
       
        List<Lead> lstLead = New List<Lead>();
        Account accountRecord = createAccount();
                     
        for(Integer i=0; i<202; i++){
            Lead testLead2 = new Lead(LastName='testlm1_'+i, firstname='testfm2', company='test2', DealReg_Deal_Registration__c = true, 
                                  email='test2@test.com', Status='New', Sales_Force__c='EMC', Partner__c = accountRecord.Id, city='India', Street ='bangalore',
                                  Lead_Originator__c='Field', LeadSource ='Manual', Originator_Details__c ='Core Referral', Channel__c ='Direct', OwnerId=g2.Id);
            lstLead.add(testLead2); 
        }
        System.runAs(testUser){
            insert lstLead; 
        }
            
        ApexPages.StandardSetController stdCon = new ApexPages.StandardSetController(lstLead); 
        ApexPages.currentpage().getParameters().put('fcf','000000000000000');                 
        // test constructors
        PRM_MassLeadOwnerChange objLeadExtn = new PRM_MassLeadOwnerChange(stdCon); 
        objLeadExtn.FetchLeadRecords();         
        objLeadExtn.objSearchUserQueueCon.selectedOwnerId = nonpartner.Id;
        stdCon.setSelected(lstLead);             
        objLeadExtn = new PRM_MassLeadOwnerChange(stdCon);
        objLeadExtn.FetchLeadRecords();
        objLeadExtn.ChangeOwner() ;                           
        objLeadExtn.Cancel();
    }
    
    private static testMethod void startTest3(){
        
        Group g2;
        QueuesObject q2;
        User adminUser = [Select id from User where isActive = true and profile.Name='System Administrator' limit 1];
        system.runAs(adminUser){
            g2 = new Group(Name='group name2', type='Queue');
            insert g2;
            q2 = new QueueSObject(QueueID = g2.id, SobjectType = 'Lead');
            insert q2;
        }
        List<User> sysAdminUser = [Select id,UserRole.Id,UserRole.Name from User where isActive=true and profile.Name='System Administrator' and UserRole.Name = 'Worldwide EMC Corporate' limit 1];
        PRM_MassLeadOwnerChange_TC newInstance = new PRM_MassLeadOwnerChange_TC();
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
       
        ID sysid = [ Select id from Profile where name ='System Administrator - API Only' limit 1].id;
                    
        User nonpartner;
   
        Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
        String SysAdmin = data.get('System Administrator').Name ;
        Profile SysAdminProf = [select Id from Profile where Name=: SysAdmin];
        //UserRole userRoles = [Select u.Id, u.Name from UserRole u where u.Name='Worldwide EMC Corporate'];
        UserRole newRole = new UserRole();
        newRole.Name = 'testRole123';
        System.runAs(insertUser){
            insert newRole; 
        }
        
        nonpartner = UserProfileTestClassDataHelper.createPortalUser(SysAdminProf.id,newRole.Id,null);
        
        System.runAs(insertUser){
            insert nonpartner;  
        }
       
        List<Lead> lstLead = New List<Lead>();
        Account accountRecord = createAccount();
                     
        for(Integer i=0; i<2; i++){
            Lead testLead2 = new Lead(LastName='testlm1_'+i, firstname='testfm2', company='test2', DealReg_Deal_Registration__c = true, 
                                  email='test2@test.com', Status='New', Sales_Force__c='EMC', Partner__c = accountRecord.Id, city='India', Street ='bangalore',
                                  Lead_Originator__c='Field', LeadSource ='Manual', Originator_Details__c ='Core Referral', Channel__c ='Direct', OwnerId=g2.Id);
            lstLead.add(testLead2); 
        }
        Lead testLead = new Lead(LastName='testlm1_'+3, firstname='testfm2', company='test2', DealReg_Deal_Registration__c = true, 
                                  email='test2@test.com', Status='New', Sales_Force__c='EMC', Partner__c = accountRecord.Id, city='India', Street ='bangalore',
                                  Lead_Originator__c='Field', LeadSource ='Manual', Originator_Details__c ='Core Referral', Channel__c ='Direct', OwnerId=sysAdminUser[0].Id);
        lstLead.add(testLead);
        
        System.runAs(sysAdminUser[0]){
            insert lstLead; 
        }
        System.runAs(sysAdminUser[0]){     
            ApexPages.StandardSetController stdCon = new ApexPages.StandardSetController(lstLead);  
            ApexPages.currentpage().getParameters().put('fcf','000000000000000');                
            // test constructors
            PRM_MassLeadOwnerChange objLeadExtn = new PRM_MassLeadOwnerChange(stdCon); 
            objLeadExtn.FetchLeadRecords();         
            objLeadExtn.objSearchUserQueueCon.selectedOwnerId = nonpartner.Id;
            stdCon.setSelected(lstLead);             
            objLeadExtn = new PRM_MassLeadOwnerChange(stdCon);
            objLeadExtn.FetchLeadRecords();
            objLeadExtn.ChangeOwner() ;                           
            objLeadExtn.Cancel();
        }
    }

    private static testMethod void startTest4(){
        
        Group g2;
        QueuesObject q2;
        User adminUser = [Select id from User where isActive = true and profile.Name='System Administrator' limit 1];
        User adminUser1 = [Select id,UserRole.Id,UserRole.Name from User where isActive=true and UserRole.Name = 'XYZ TEST ACCOUNT Partner Executive' limit 1];
        //A2F INFORMATICA LTDA Partner Ececutive
        system.runAs(adminUser){
            g2 = new Group(Name='group name2', type='Queue');
            insert g2;
            q2 = new QueueSObject(QueueID = g2.id, SobjectType = 'Lead');
            insert q2;
        }
        List<User> sysAdminUser = [Select id,UserRole.Id,UserRole.Name from User where isActive=true and profile.Name='System Administrator' and UserRole.Name = 'Worldwide EMC Corporate' limit 1];
        PRM_MassLeadOwnerChange_TC newInstance = new PRM_MassLeadOwnerChange_TC();
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
       
        ID sysid = [ Select id from Profile where name ='System Administrator - API Only' limit 1].id;
                    
        User nonpartner;
   
        Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
        String SysAdmin = data.get('System Administrator').Name ;
        Profile SysAdminProf = [select Id from Profile where Name=: SysAdmin];
        //UserRole userRoles = [Select u.Id, u.Name from UserRole u where u.Name='Worldwide EMC Corporate'];
        UserRole newRole = new UserRole();
        newRole.Name = 'testRole123';
        System.runAs(insertUser){
            insert newRole; 
        }
        
        nonpartner = UserProfileTestClassDataHelper.createPortalUser(SysAdminProf.id,newRole.Id,null);
        
        System.runAs(insertUser){
            insert nonpartner;  
        }
       
        List<Lead> lstLead = New List<Lead>();
        Account accountRecord = createAccount();
                     
        for(Integer i=0; i<2; i++){
            Lead testLead2 = new Lead(LastName='testlm1_'+i, firstname='testfm2', company='test2', DealReg_Deal_Registration__c = true, 
                                  email='test2@test.com', Status='New', Sales_Force__c='EMC', Partner__c = accountRecord.Id, city='India', Street ='bangalore',
                                  Lead_Originator__c='Field', LeadSource ='Manual', Originator_Details__c ='Core Referral', Channel__c ='Direct', OwnerId=g2.Id);
            lstLead.add(testLead2); 
        }
        Lead testLead = new Lead(LastName='testlm1_'+3, firstname='testfm2', company='test2', DealReg_Deal_Registration__c = true, 
                                  email='test2@test.com', Status='New', Sales_Force__c='EMC', Partner__c = accountRecord.Id, city='India', Street ='bangalore',
                                  Lead_Originator__c='Field', LeadSource ='Manual', Originator_Details__c ='Core Referral', Channel__c ='Direct', OwnerId=sysAdminUser[0].Id);
        lstLead.add(testLead);
        
        System.runAs(sysAdminUser[0]){
            insert lstLead; 
        }
        System.runAs(adminUser1){     
            ApexPages.StandardSetController stdCon = new ApexPages.StandardSetController(lstLead);  
            ApexPages.currentpage().getParameters().put('fcf','000000000000000');                
            // test constructors
            PRM_MassLeadOwnerChange objLeadExtn = new PRM_MassLeadOwnerChange(stdCon); 
            objLeadExtn.FetchLeadRecords();         
            objLeadExtn.objSearchUserQueueCon.selectedOwnerId = nonpartner.Id;
            stdCon.setSelected(lstLead);             
            objLeadExtn = new PRM_MassLeadOwnerChange(stdCon);
            objLeadExtn.FetchLeadRecords();
            objLeadExtn.ChangeOwner() ;                           
            objLeadExtn.Cancel();
        }
    }
    
    private static Account createAccount(){
        //Creating the dummy account for test class.
        Map<String,Schema.RecordTypeInfo> recordTypes = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id accRecordTypeId = recordTypes.get('T2_Partner Profile Record Type').getRecordTypeId();
    
        Account objAccount = new Account(
            name = 'UNITTESTAcc',
            Party_Number__c = '1234',
            BillingCountry ='Colombia',
            Synergy_Account_Number__c = '10',
            Lead_Oppty_Enabled__c = true, 
            Partner_Type__c ='Distributor',
            Type = 'Partner' ,
            PROFILED_ACCOUNT_FLAG__c= true,
            recordtypeid= accRecordTypeId           
        );
        insert objAccount;
        objAccount.Lead_Oppty_Enabled__c = true;
        objAccount.Type = 'Partner';
        objAccount.Status__c='A';
        update objAccount;
        System.Debug('objAccount ----> ' + objAccount) ; 
        return objAccount;
    }
}