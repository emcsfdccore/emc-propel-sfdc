/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        ===========       
 
 | 26/07/2013     Leonard Victor    This Class is used for Internal Contact Logic, where accounts wil be taken from 
                    custom setting and populated in acctouns section on case.
   18/06/2014     Sayan Choudhury    Added the checkingChildcaserecordtype method to incorporate CI WR 150 as part of July'14 release            
 +==================================================================================================================**/

public class Presales_Account_Split {

/* @Method <This Method is Used for Performing the main Split Logic>
@param <This method takes List<Case> and List<Id>(ContactID) as parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/
    

    public void split(List<Case> lstCase , List<Id> lstcntID){
        Map<Id,Contact> mapInternalContact = new Map<Id,Contact>([Select id from Contact where id in :lstcntID and recordtype.name = 'EMC Internal Contact']);

        Map<String,AccountSplit__c> accSplitMap = AccountSplit__c.getall();
        if(accSplitMap != null && accSplitMap.size() >0){
            Integer accSize = accSplitMap.size();
            Integer selectAcct = Math.Mod((Math.floor(Math.random()*14).intValue()),accSize);

            if (selectAcct>accSize){
                selectAcct=0;
            }
            //AccountSplit__c accResult = AccountSplit__c.getValues(string.valueOf(x));

            Id internalAcc = accSplitMap.get(String.valueof(selectAcct)).account__c;

            for(Case caseRecord:lstCase){
                if(mapInternalContact.containsKey(caseRecord.contactid)){
                    caseRecord.accountID = internalAcc;
                }
            }
        }
    }
//@this method is used throw validation error if any child case of POC type is having Parent record type Other than POC(CI July'14 WR 150)//
public void checkingChildcaserecordtype(List<Case> cs){
        List<Id> childCasetobentered = new List<Id>();
       for(case chilldCs : cs){
                if(chilldCs.ParentId!= null && chilldCs.Record_Type_Developer_Name__c=='Presales_Proof_of_Concept'){
                    childCasetobentered.add(chilldCs.ParentId);
                    }
                }

                if(!childCasetobentered.isEmpty()){
                Map<Id,Case> parentCase = new Map<Id,Case>([Select id,Record_Type_Developer_Name__c from Case where id in:childCasetobentered and Record_Type_Developer_Name__c = 'Presales_Proof_of_Concept' ]);

                      for(case cstobeEntered :cs){
                      
                    if(!parentCase.containsKey(cstobeEntered.ParentId) && cstobeEntered.ParentId!=null){
                        cstobeEntered.addError('POC Child cases can only be linked to a parent case where the case type = Proof of Concept');
                    }
                }
                }
    }
}