/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req                  DESCRIPTION                               
 |  ====            =========       ======                  =========== 
 |  11-02-2013      Anand Sharma    System Administration   This class is used as a Test class Utility class for System Admin App                     
 | 
 +=========================================================================================================================*/
@isTest
private class SystemAdministrationUtility_TC {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        SystemAdministration_Utility objSysAminUtil = new SystemAdministration_Utility();
        Set<Id> userIds = new Set<Id>();
        userIds.add(UserInfo.getUserId());
        SystemAdministration_Utility.sendEmailOnNewModifiedDeleteUser(userIds);
        SystemAdministration_Utility.getEnviromentAsProduction();
        objSysAminUtil.updateCustomSetting(); 
        objSysAminUtil.inputstr = '5';
        objSysAminUtil.updateCustomSetting();
        objSysAminUtil.runBatchJob();
    }
    
    static testMethod void SystemAdminAccessTest() {
        List<User> lstUser = [Select Id, UserType, UserName from User where Profile.Name='System Admin Access Profile' and isActive=true ];
        if(lstUser != null && lstUser.size() >0 ){
            System.runAs(lstUser.get(0)){
                SystemAdministration_Utility objSysAminUtil = new SystemAdministration_Utility();
                try{
                    SystemAdministration_Utility.grantAccessToAllEligibleUsers();
                }catch (exception ex){
                    
                }
            }
        }
    }
    
    static testMethod void mySystemAdminAccessControllerTest() {
        // TO DO: implement unit test
        List<User> lstUser = [Select Id, UserType from User where UserType='Guest' and isActive=true];
        List<User> lstUserAdmin = [Select Id, UserType, UserName from User where Profile.Name='System Administrator' and isActive=true limit 2];
                
        if(lstUser != null && lstUser.size() >1 ){
            System.runAs(lstUser.get(1)){
                SystemAdminAccessController objSysAminController = new SystemAdminAccessController();               
                objSysAminController.Cancel();
                
                objSysAminController.provideSysAdminAccessAsGuestUser();
                if(lstUserAdmin != null && lstUserAdmin.size() >0){
                    List<System_Admin_Users__c> lstDeleteSysAdmUser = [select Id from System_Admin_Users__c where id =:lstUserAdmin.get(0).Id];
                    if(lstDeleteSysAdmUser != null && lstDeleteSysAdmUser.size() >0){
                        delete lstDeleteSysAdmUser;
                    }
                    List<System_Admin_Users__c> lstInsertSysAdmUser = new List<System_Admin_Users__c>();    
                    System_Admin_Users__c objSysAdmUser = new System_Admin_Users__c();
                    objSysAdmUser.Active__c = true;
                    objSysAdmUser.User_Name__c = lstUserAdmin.get(0).Id;
                    objSysAdmUser.Reason__c ='Test Reason';
                    objSysAdmUser.ManagerName__c =lstUserAdmin.get(0).Id;
                    lstInsertSysAdmUser.add(objSysAdmUser);
                    Database.insert( lstInsertSysAdmUser, false);
                    
                    objSysAminController.strEmailId =lstUserAdmin.get(0).UserName;
                    objSysAminController.provideSysAdminAccessAsGuestUser();
                    objSysAminController.strReason = 'Test';
                    objSysAminController.provideSysAdminAccessAsGuestUser();
                    
                    objSysAminController.strEmailId = '';
                    objSysAminController.provideSysAdminAccessAsGuestUser();
                    
                    objSysAminController.isGuest = false;
                    objSysAminController.provideSysAdminAccessAsGuestUser();
                    
                    lstInsertSysAdmUser.clear();
                    objSysAminController.strEmailId =lstUserAdmin.get(0).UserName;
                    objSysAdmUser.End_Date_Time__c = System.Now().addDays(10);
                    objSysAdmUser.Is_a_Temp_System_Admin__c = false;
                    lstInsertSysAdmUser.add(objSysAdmUser);
                    Database.update( lstInsertSysAdmUser, false);
                    objSysAminController.isGuest = true;
                    objSysAminController.provideSysAdminAccessAsGuestUser();
                    
                    lstInsertSysAdmUser.clear();
                    objSysAdmUser.User_Name__c = null;
                    objSysAdmUser.End_Date_Time__c = System.Now().addDays(-1);
                    objSysAdmUser.Is_a_Temp_System_Admin__c = false;
                    lstInsertSysAdmUser.add(objSysAdmUser);
                    Database.update( lstInsertSysAdmUser, false);
                    objSysAminController.isGuest = false;
                    objSysAminController.provideSysAdminAccessAsGuestUser();
                }
                
                objSysAminController.strReturnUrl =''; 
                objSysAminController.Cancel();
            }
        }
    }
    
    static testMethod void SystemAdminAccessJobBatch() {
        // TO DO: implement unit test
        
        SystemAdminAccessJob objSysAminAccessJob = new SystemAdminAccessJob();
        objSysAminAccessJob = new SystemAdminAccessJob('select id from user where isActive=true limit 1');
        objSysAminAccessJob = new SystemAdminAccessJob(false);
        try{
            Test.StartTest();           
                // run batch           
                Id batchProcessId = Database.executeBatch(new SystemAdminAccessJob());  
            Test.StopTest();
        }catch (Exception ex){
            
        }
    }
    
    static testMethod void SystemAdminAccessJobSchedule() {
                  
        // run batch 
        Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
        CustomSettingDataValueMap__c jobObjectDetails = DataValueMap.get('SystemAdminProdEnviroment');
        if(jobObjectDetails != null){
            jobObjectDetails.DataValue__c = UserInfo.getOrganizationId();
            update jobObjectDetails;
        }
        
        
        Test.StartTest(); 
        String strScheduleTime ='0 0 0 3 9 ? ';
        strScheduleTime = strScheduleTime + Datetime.now().addYears(1).year();
        Datetime dt = System.now(); 
        dt = dt.addMinutes(1);
        String jobName = 'System Admin Users- ' + dt.format('MM-dd-yyyy-hh:');
        //  String educationQuery = 'Select Id, Name from Education__c order by createddate DESC limit 10 ' ;
        Id jobAdminUtilityBatchId = System.schedule(jobName,strScheduleTime,new SystemAdminAccessJob());      
                    
        Test.StopTest();
        
    }
    
    static testMethod void SystemAdminAccessJobScheduleNonProduction() {
        
        Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
        CustomSettingDataValueMap__c jobObjectDetails = DataValueMap.get('SystemAdminProdEnviroment');
        if(jobObjectDetails != null){
            jobObjectDetails.DataValue__c ='';
            update jobObjectDetails;
        }
        
        Test.StartTest();    
        String strScheduleTime ='0 0 0 3 9 ? ';
        strScheduleTime = strScheduleTime + Datetime.now().addYears(1).year();
        Datetime dt = System.now(); 
        dt = dt.addMinutes(10);
        String jobName = 'System Admin Users- ' + dt.format('MM-dd-yyyy-hh:');

        //  String educationQuery = 'Select Id, Name from Education__c order by createddate DESC limit 10 ' ;
        Id jobAdminUtilityBatchId = System.schedule(jobName,strScheduleTime,new SystemAdminAccessJob(true));
            
        Test.StopTest();
        
    }
    
    static testmethod void TestSystemAdminRequestLog(){
        user userObj = [Select Id,Name,email from User where Profile.Name !='System Administrator' and isportalEnabled=false and IsActive =true limit 1];
        System_Admin_Request_Logs__c logRequest = new System_Admin_Request_Logs__c();
        logRequest.User_Name__c= userObj.Id;
        logRequest.Email__c=userObj.email;
        logRequest.Start_Date__c= System.Now().Adddays(1);
        logRequest.End_Date__c= System.Now().Adddays(10);
        logRequest.Reason__c= 'Test';
        test.startTest();
        insert logRequest;
        test.stopTest();
    }
}