/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
    05-Jun-2014      Abinaya M S                 Assigned 'Internal meeting' value to Briefing_Type__c field to satisfy validation rule.
    12-March-2015    Vinod Jetti       CI:1731   Updated to coverage of Added method in main class.
 */
@isTest
private class EBC_Impact_TC {

    static testMethod void EBC_Impact() {
        EBC_Briefing_Event__c bEvent=new EBC_Briefing_Event__c(Name='test',Start_Date__c=date.today(),Briefing_Type__c = 'Internal meeting',End_Date__c=date.today(),Briefing_Status__c='Pending');    
        insert bEvent;
        Account acc = new Account(Name = 'Test', Status__c = 'A');
        insert acc;
        Opportunity opp = new Opportunity(Name = 'test', StageName = 'Pipeline', Sales_Channel__c = 'Direct', AccountID = acc.id,CurrencyIsoCode = 'USD', CloseDate = system.Today()+1);
        insert opp;
        EBC_Related_Opportunity__c ebcRelOppty = new EBC_Related_Opportunity__c(Briefing_Event__c = bEvent.Id,Opportunity_Name__c = opp.Id);
        insert ebcRelOppty;
        
    ApexPages.currentPage().getParameters().put('Id',bEvent.Id);
    ApexPages.StandardController ctController=new ApexPages.StandardController(bEvent);
    EBC_Impact impact = new EBC_Impact(ctController);
    impact.backToBriefingEvent();
    impact.objBEvent=bEvent;
    impact.saveRecord();
    
    ApexPages.StandardController ctControllers=new ApexPages.StandardController(bEvent);
    EBC_Impact impacts = new EBC_Impact(ctControllers);
    impacts.NotFromSite=false;
    impacts.saveRecord();
    impacts.EBCRelatedOppty();
    }
}