/* ===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE       DEVELOPER       WR       DESCRIPTION                               
 |  ====       =========       ==       =========== 
 |  08-JUN-11  M Mutebi                 Created
 |  28-Jan-15  Karan Shekhar            Changed with best practices, Project: Platform innovation
 +===========================================================================*/
@isTest
private class MapHubInfoPopulation_TC {
  
  
    static set<string> testPartyIds = new set<string>{'177777778'};
    static set<double> testGrids = new set <double> {5555559};  // 528909
    static double gridSet = 4144149; 
    static Set<double> guGridSet = new Set<double>{4144167}; 
    static Set<double> buGridSet = new Set<double>{4144176}; 
    static Set<double> duGridSet = new Set<double>{4144194};
    static Set<boolean> atpGridSet  = new Set<boolean> {false}; 
    static List<Account> accList= new List<Account>();
    // added method to create test data, Karan Shekhar 28,Jan,2015 :Platform innovation
    private static void createTestData() {
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1]; 
        System.runAs(insertUser){
    
            CustomSettingDataHelper.hubToSFDCMapData();  
            CustomSettingDataHelper.dataValueMapCSData();       
        }  
        Hub_Outbound_Staging__c hub = new Hub_Outbound_Staging__c(Party_Id__c='177777778', Status__c='E',Retry_Count__c=1,Golden_Site_Name__c ='TestInfoRecord2', Global_Ultimate_Golden_Site_ID__c = 9800578,
                                                                     Business_Unit_Ultimate_Golden_Site_ID__c = 230067, Golden_Site_Identifier__c=0000000, ATP_Facilitated_Account__c=false);
        insert hub;
        Hub_Outbound_Staging_GTM__c gtm1 = new Hub_Outbound_Staging_GTM__c(Golden_Site_Identifier__c = 5555559, GTM_Property_Type_Code__c ='Test code one',
                                                                             GTM_Property_Value_Code__c = 'Yes', Status__c = '');
        insert gtm1;
        Hub_Info__c hubInfoObj= new Hub_Info__c(Name='testHub',Golden_Site_Identifier__c=9800578);
        insert hubInfoObj;
        accList=AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        for(Integer i =0;i<accList.size();i++){
            if(i==0){
                accList[i].Party_Id__c='177777778';
            }
        }
        if(!accList.isEmpty()) {
            insert accList;
        }
    }   
    static testMethod void testBatch(){
        MapHubInfoPopulation_TC.createTestData();
        //Starting the Test
        Test.StartTest();
        MapHubInfoPopulation testPopulation = new MapHubInfoPopulation(); 
        Id batchprocessId =  Database.executeBatch(testPopulation,10);   
        MapHubInfoPopulation testPopulation1 = new MapHubInfoPopulation();  
        testPopulation1.isTestRun= true;
        Id batchprocessId1 =  Database.executeBatch(testPopulation1,10);
        //Prachi: code ends  
        Test.StopTest();
     
        //assert that the records inserted in the staging object have also been created/updated in Hub_Info__c
    
        List<Hub_Outbound_Staging_GTM__c> gtmRecs = [Select Golden_Site_Identifier__c,
                                                               GTM_Key__c,
                                                               GTM_Property_Type_Code__c,
                                                               GTM_Property_Value_Code__c,
                                                               Status__c
                                                        From   Hub_Outbound_Staging_GTM__c
                                                        Where  Golden_Site_Identifier__c in :testGrids];
        //confirm the insertion of the two Hub OutBound GTM records                                                    
        // System.assert(gtmRecs.size() == 1);
        
        //check if the Hub Outbound Records also exist in Hub Info with all the respective attributes
        //check is done by retrieving one specif record                                                     
        List<Hub_Info__c> hubInfoTest = [SELECT id, Golden_Site_Identifier__c, Golden_Site_Name__c,Global_Ultimate_Golden_Site_Identifier__c,
                                    Business_Unit_Ultimate_Golden_Site_ID__c
                                    FROM Hub_Info__c
                                    WHERE Golden_Site_Name__c = 'TestInfoRecord2'];
         //system.assert(hubInfoTest.size() == 1);
        
        //Negative Tests on the Hub_Info Object
        for (Hub_Info__c hubInfoTestRecord : hubInfoTest){
                
            system.debug('These assertions shouldnt fail because these values are not null');
            system.assertNotEquals(hubInfoTestRecord.Golden_Site_Identifier__c,null);
            system.assertNotEquals(hubInfoTestRecord.Global_Ultimate_Golden_Site_Identifier__c,null);
            system.assertNotEquals(hubInfoTestRecord.Business_Unit_Ultimate_Golden_Site_ID__c,null);
            
            //write the actual Record values to the Debug Log 
            
            system.debug('hubInfoTestRecord.Golden_Site_Identifier__c=' + hubInfoTestRecord.Golden_Site_Identifier__c);
            system.debug('hubInfoTestRecord.Global_Ultimate_Golden_Site_Identifer__c =' + hubInfoTestRecord.Global_Ultimate_Golden_Site_Identifier__c);
            system.debug('hubInfoTestRecord.Business_Unit_Ultimate_Golden_Site_ID__c=' + hubInfoTestRecord.Business_Unit_Ultimate_Golden_Site_ID__c);
        
        }
        
        //verify one specific account record, if it's been populated with the right golden_site_Name
       
        List<Account> acctRecord = [SELECT id, Golden_Site_Identifier__c, Golden_Site_Name__c, Party_ID__c, Hub_Info__c 
                                     FROM Account
                                     WHERE Party_ID__c = '177777778'];
                                   
        //confirm if the list contains one record                      
        system.assert(acctRecord.size() == 1);
            
        // check the  record values   
        for (Account TestRecord : acctRecord){
        
            //system.assertNotEquals(TestRecord.Golden_Site_Identifier__c,null);
            system.assertEquals(TestRecord.Party_ID__c, '177777778');
            
            //write the TestRecord values to the Debug 
            
            system.debug('TestRecord.Golden_Site_Identifier__c' + TestRecord.Golden_Site_Identifier__c);
            system.debug('TestRecord.Golden_Site_Name__c' + TestRecord.Golden_Site_Name__c);
            system.debug('TestRecord.Hub_Info__c' + TestRecord.Hub_Info__c);
            
        }
        
    }
}