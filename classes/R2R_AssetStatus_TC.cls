/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  06 Dec 2013      Sneha Jain                 Commenting the test class as the base class 'R2R_AssetStatus' is covered by another test class 'R2R_AssetStatus__TC_New'. Hence this is a redundant class.
 +=========================================================================================================================*/
@isTest
private Class R2R_AssetStatus_TC{

    static testMethod void setSalesPlanTest()
    {
        /*
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser)
        {
          PRM_VPP_JobDataHelper.createVPPCustomSettingData();   
        } 
        List<Opportunity> lstOpportunityToInsert = new List<Opportunity>();
        List<Asset__c> lstAssetToInsert = new List<Asset__c>();
        List<Opportunity_Asset_Junction__c> lstAssetJunctionToInsert = new List<Opportunity_Asset_Junction__c>();
        String linkingStatus='';
        User u1=[Select id,Name from User where Forecast_Group__c='Maintenance Renewals' and isActive=true limit 1];
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where RecordType.DeveloperName like 'EMC_Install' or Name Like 'Competitive_Install'];
        //Running as user with Forecast_Group__c='Maintenance Renewals'
        System.runAs(u1){

            Opportunity opp=testclassUtils.getOppty ();
            insert opp;
    
            Account accuser=testclassUtils.getAccount () ;
            insert accuser;
    
            Asset__c assetuser= new Asset__c();
            assetuser.Name='Testasset1';
            assetuser.Customer_Name__c=accuser.id;
            assetuser.Contract_Number__c='abcd111qqq';        
            insert assetuser;
    
            assetuser.Red_Zone_Priority__c='RedZone';
            Date ced=  System.Today().adddays(40);
            assetuser.Contract_End_Date__c=ced-1;
            assetuser.Field_To_update__c='deffered over on '+System.today();
            
        
            update assetuser;
        
    
        
            try{
                Opportunity_Asset_Junction__c oajuser=new Opportunity_Asset_Junction__c();
                oajuser.Related_Asset__c=assetuser.id;
                oajuser.Related_Account__c=accuser.id;
                oajuser.Related_Opportunity__c=opp.id;
                insert oajuser;
            }
            Catch(Exception exp){
             System.Debug(+exp);
            }
            opp.StageName='Upside';
            update opp;
            
        }
        //user runing ends here
        // creating and inserting opportunities
        Opportunity opp1=testclassUtils.getOppty ();
        lstOpportunityToInsert.add(opp1);   
        
        Opportunity opp2=testclassUtils.getOppty ();
        lstOpportunityToInsert.add(opp2);
        insert lstOpportunityToInsert;
        
        lstOpportunityToInsert[0].StageName='Upside';
        update lstOpportunityToInsert[0];
        
        
        
        //creating and inserting account
        Account acc=testclassUtils.getAccount () ;
        insert acc;
       
        //creating and inserting assets
        Asset__c asset1= new Asset__c();
        asset1.Name='Testasset1';
        asset1.Customer_Name__c=acc.id;
        asset1.RecordTypeId=recordType[0].id;
        asset1.Red_Zone_Priority__c='RedZone';
        asset1.Deffered_to_Renewals__c= false;
        lstAssetToInsert.add(asset1);
        
        
        Asset__c asset2= new Asset__c();
        asset2.Name='Testasset2';
        asset2.Customer_Name__c=acc.id;
        asset2.RecordTypeId=recordType[1].id;
        lstAssetToInsert.add(asset2);
        insert lstAssetToInsert;
        lstAssetToInsert[0].Red_Zone_Priority__c ='Target';
        update lstAssetToInsert;
        //creating and inserting Opportunity_Asset_Junction
        try{   
            Opportunity_Asset_Junction__c oaj1=new Opportunity_Asset_Junction__c();
            oaj1.Related_Asset__c=lstAssetToInsert[0].id;
            oaj1.Related_Account__c=acc.id;
            oaj1.Related_Opportunity__c=lstOpportunityToInsert[0].id;
            lstAssetJunctionToInsert.add(oaj1);
            

            Opportunity_Asset_Junction__c oaj2=new Opportunity_Asset_Junction__c();
            oaj2.Related_Asset__c=lstAssetToInsert[1].id;
            oaj2.Related_Account__c=acc.id;
            oaj2.Related_Opportunity__c=lstOpportunityToInsert[1].id;
            lstAssetJunctionToInsert.add(oaj2);
            insert lstAssetJunctionToInsert;
        }
        Catch(Exception e){
        System.debug(+e);
        }    
        lstOpportunityToInsert[0].StageName='Strong Upside';    
        lstOpportunityToInsert[1].StageName='Strong Upside';
        update lstOpportunityToInsert;          
        lstAssetToInsert[0].Red_Zone_Priority__c='Target';
        update lstAssetToInsert;
        linkingStatus='Link to Refresh Lead';    
       Map<id, Opportunity> Map_Opportunities = new Map<id, Opportunity>();
       Map_Opportunities.put(opp1.id,opp1);
       Map_Opportunities.put(opp2.id,opp2);
       Map<Id,Asset__c> Map_Assets = new Map<Id,Asset__c> (); 
       Map_Assets.put(asset1.id,asset1);
        Map_Assets.put(asset2.id,asset2);
        R2R_AssetStatus obj= new R2R_AssetStatus();
        obj.setSalesPlan(lstAssetJunctionToInsert,Map_Assets,Map_Opportunities,true);
        obj.setOpportunityStatus(lstAssetJunctionToInsert,Map_Opportunities);
        */
    }
    
    static testMethod void setSalesPlanLeadTest()
    {
        /*
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser)
        {
          PRM_VPP_JobDataHelper.createVPPCustomSettingData();   
        }
        List<Lead> listoflead= new List<Lead>();
        Map<Id,Lead> mapoflead= new Map<Id,Lead>();
        List<Asset__c> listofasset= new List<Asset__c>();
        List<Lead_Asset_Junction__c> listofleadasstjunction= new List<Lead_Asset_Junction__c>();
         List<RecordType> recordType1= [select id,DeveloperName,Name from RecordType where RecordType.DeveloperName like 'EMC_Install' or Name Like 'Competitive_Install'];
        Account acc1= new Account();
        acc1.Name='DumyAccount';
        insert acc1;
        /*
        Opportunity oppty= new Opportunity();
        oppty.Name='Testoppty';
        oppty.AccountId = acc1.Id;
        Date closeDate =  System.today()+5;
        oppty.CloseDate=closeDate;
        oppty.Sales_Channel__c='Direct';
        oppty.Sales_Force__c='EMC';
        oppty.StageName='Pipeline';
        insert oppty;
        */
        /*
            Asset__c ast1= new Asset__c();
            ast1.Name='Testasset1';
            ast1.Customer_Name__c=acc1.id;
            ast1.RecordTypeId=recordType1[0].id;
            ast1.Red_Zone_Priority__c=null;
            ast1.Deffered_to_Renewals__c= false;
            listofasset.add(ast1);
            
            Asset__c ast2= new Asset__c();
            ast2.Name='Testasset1';
            ast2.Customer_Name__c=acc1.id;
            ast2.RecordTypeId=recordType1[1].id;
            ast2.Red_Zone_Priority__c=null;
            ast2.Deffered_to_Renewals__c= false;
            listofasset.add(ast2);

        insert listofasset;

        Lead lead= new Lead();
        lead.Company='TestLead';
        lead.LastName='Test123';
        lead.Status='New';
        lead.Sales_Force__c='EMC';
        lead.Lead_Originator__c='Customer Intelligence';
        listoflead.add(lead);

        Lead lead1= new Lead();
        lead1.Company='TestLeadabc';
        lead1.LastName='Test123';
        lead1.Status='New';
        lead1.Sales_Force__c='EMC';
        lead1.Lead_Originator__c='Customer Intelligence';
        listoflead.add(lead1);


        try{
        Lead_Asset_Junction__c laj1= new Lead_Asset_Junction__c();
        laj1.Related_Asset__c=listofasset[0].id;
        laj1.Related_Lead__c=listoflead[0].id;
        listofleadasstjunction.add(laj1);

        Lead_Asset_Junction__c laj2= new Lead_Asset_Junction__c();
        laj2.Related_Asset__c=listofasset[1].id;
        laj2.Related_Lead__c=listoflead[1].id;
        listofleadasstjunction.add(laj2);
        insert listofleadasstjunction;
        }
        Catch(Exception exp){
        System.Debug(+exp);
        }
        mapoflead.put(lead.id,lead);
        mapoflead.put(lead1.id,lead1);

        Test.startTest();
        R2R_AssetStatus objassetstatus= new R2R_AssetStatus();
        objassetstatus.setSalesPlan(mapoflead);

        Asset__c asttt= new Asset__c();
        asttt.Name='Testasset1';
        asttt.Customer_Name__c=acc1.id;
        asttt.RecordTypeId=recordType1[0].id;
        asttt.Red_Zone_Priority__c='Target';
        asttt.Sales_Plan__c='Related Lead';
        insert asttt;

        Lead lead2= new Lead();
        lead2.Company='TestLeadabc';
        lead2.LastName='Test123';
        lead2.Status='New';
        lead2.Sales_Force__c='EMC';
        lead2.Lead_Originator__c='Customer Intelligence';
        lead2.Status='New';
        insert lead2;

        Lead_Asset_Junction__c laj3= new Lead_Asset_Junction__c();
        laj3.Related_Asset__c=asttt.id;
        laj3.Related_Lead__c=lead2.id;
        try{
            insert laj3;
            }
        catch (exception e){
        }  
         if(laj3.id !=null){
             delete laj3;
         }

        Map<Id,Lead> mapoflead1= new Map<Id,Lead>();
        mapoflead1.put(lead2.id,lead2);


        objassetstatus.setSalesPlan(mapoflead1);
        Test.stopTest();
        */
    }
}