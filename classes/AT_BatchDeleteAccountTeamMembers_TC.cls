@isTest
/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE       DEVELOPER       WR       DESCRIPTION                               
 |  ====       =========       ==       =========== 
 |  17-JUN-09  S Douglas                Created
 |  17-Feb-2015  Vinod Jetti   #1649    Created Hub Account Data oject data for testing
 |  18-DEC-14  Bhanuprakash    PROPEL   replaced ‘Sales-Sales Rep’ with ‘Enterprise Sales Rep’ for AccountTeamMember field
 +===========================================================================*/
 
private class AT_BatchDeleteAccountTeamMembers_TC {
 static testMethod void testBatch()
  {
    //setup data for test -- we can test a maximum of 200 records for Batch Apex
    //Note that we are subject to the 500 DML governor limit and the 500 query row governor limit 
    //We will create 3 accounts and select 100 users
    //We will add the all 100 users to the account team for each of the accounts created
    //We will add the following interface table rows (to be deleted):
    //  first 75 users for account 1
    //  last 75 users for account 2
    //  no rows for account 3
    
    //Accounts
    List <Account> accounts = new List <Account>();
    //#1649 - Start    
        List<Hub_Info__c> lstHubInfo = new List<Hub_Info__c>();
        Hub_Info__c objHubInfo =new Hub_Info__c();
        objHubInfo.Site_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo.Parent_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo.Global_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo.Golden_Site_Identifier__c = 685240;
        lstHubInfo.add(objHubInfo);
        
        Hub_Info__c objHubInfo1 =new Hub_Info__c();
        objHubInfo1.Site_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo1.Parent_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo1.Global_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo1.Golden_Site_Identifier__c = 302052;
        lstHubInfo.add(objHubInfo1);
        
        Hub_Info__c objHubInfo2 =new Hub_Info__c();
        objHubInfo2.Site_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo2.Parent_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo2.Global_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo2.Golden_Site_Identifier__c = 302012;
        lstHubInfo.add(objHubInfo2);
        insert lstHubInfo;
    //#1649 - End   
    accounts.add (new Account(name = 'BatchDeleteAccount1', Hub_Info__c = lstHubInfo[0].Id
                                     //Site_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8)),
                                     //Parent_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8)),
                                     //Global_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8))
                                     ));
    accounts.add (new Account(name = 'BatchDeleteAccount2', Hub_Info__c = lstHubInfo[1].Id
                                     //Site_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8)),
                                     //Parent_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8)),
                                     //Global_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8))
                                     ));
    accounts.add (new Account(name = 'BatchDeleteAccount3', Hub_Info__c = lstHubInfo[2].Id
                                     //Site_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8)),
                                     //Parent_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8)),
                                     //Global_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8))
                                     ));
    insert accounts;

    //Just query to get some users--we don't seem to be able to add them programmatically
    //List<User> users = [SELECT ID FROM user where isActive = true LIMIT 100];

    //Create Account Team Records
    List <AccountTeamMember> members = new List<AccountTeamMember>();
    for (Integer x=0; x<3;x++) {
        for (Integer y=0; y<100; y++) {
            members.add (new AccountTeamMember(AccountId=accounts[x].Id,
                                                userId=(new User(Id = Userinfo.getUserId())).Id,
                                                teammemberrole='Enterprise Sales Rep'));
        }
    }
    insert members; 
    
    
    //Create Interface Table records
    List <AccountTeamDelete__c> oitRecs = new List <AccountTeamDelete__c>();

    //Account 1
    for (Integer y=0; y<75; y++) {
        oitRecs.add (new AccountTeamDelete__c(Account__c=accounts[0].Id,
                                                userId__c=(new User(Id = Userinfo.getUserId())).Id));
    }
    //Account 2
    for (Integer y=25; y<100; y++) {
        oitRecs.add (new AccountTeamDelete__c(Account__c=accounts[1].Id,
                                                userId__c=(new User(Id = Userinfo.getUserId())).Id));
    }

    insert oitRecs;

    List<String> testAccts = new List<String>();
    testAccts.add(accounts[0].id);
    testAccts.add(accounts[1].id);
    String acct3id = accounts[2].id;

    //Note:  we can't check the record counts of the interface table here or we will exceed the 500 query row governor limit
    //       the 100 users we queried for counts against the limit
    //System.AssertEquals(150, Database.countQuery('SELECT count() FROM AccountTeamDelete__c WHERE AccountId__c IN :testAccts'), 'Rows Inserted into Interface Table is incorrect for accounts 1 and 2');
    //System.AssertEquals(0, Database.countQuery('SELECT count() FROM AccountTeamDelete__c WHERE AccountId__c = :acct3id'), 'Rows Inserted into Interface Table is incorrect for account 3');
    //System.AssertEquals(200, Database.countQuery('SELECT count() FROM AccountTeamMember WHERE AccountId IN :testAccts'), 'Rows Inserted into AccountTeamMembers table is incorrect for accounts 1 and 2');
    //System.AssertEquals(100, Database.countQuery('SELECT count() FROM AccountTeamMember WHERE AccountId = :acct3id'), 'Rows Inserted into AccountTeamMembers table is incorrect for account 3');

    AT_BatchDeleteAccountTeamMembers deleteAcctBatch =  new AT_BatchDeleteAccountTeamMembers();
    deleteAcctBatch.unitTestAccounts = testAccts;
    deleteAcctBatch.query = 'SELECT Id, AccountId__c, UserId__c, LastModifiedDate, Account__c FROM AccountTeamDelete__c WHERE ProcessedFlag__c = false AND Account__c IN :unitTestAccounts LIMIT 200';
    Test.startTest();
    ID batchprocessid = Database.executeBatch(deleteAcctBatch);
    System.Debug('Batch Process Id = ' + batchprocessid);
    Test.stopTest(); 
    //System.AssertEquals(50, Database.countQuery('SELECT count() FROM AccountTeamMember WHERE AccountId IN :testAccts'),'AccountTeam Member record count incorrect after Batch Apex program for accounts 1 and 2');
    //System.AssertEquals(100, Database.countQuery('SELECT count() FROM AccountTeamMember WHERE AccountId = :acct3id'),'AccountTeam Member record count incorrect after Batch Apex program for account 3');
  }
}