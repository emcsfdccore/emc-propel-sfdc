/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  20 Aug 2014     Partha Baruah   WR-273      Modified AssetUnlinking method to introduce the code logic for 'Unlink & Defer' Button in Lead detail page
 |  19 Sep 2014     Partha Baruah   WR-294      Created a new method "updateLeadAssetCount" for counting the number of assets associated with a lead
 |  24 Sep 2014     Sneha Jain      WR 291(c)   R2R Nov'14 Rel : Reps should be allowed to link/unlink assets from BOOKED,EVAL and SUBMITTED Non-Renewals opportunity
 |  15 Oct 2014     Sneha Jain      INC0729833  R2R Nov'14 Rel : Related Leads/Opps should appear in global search of Asset Serial Number
 |  06/04/2015      Vivek           CI#1872     Update the Lead related info to asset object
 +=========================================================================================================================*/

global Class R2R_Lead_Management{
  String errormessage='';  
  //Added a static flag to defer assets for 'Unlink & Defer' Button 
  static boolean isDefer = false;  
  
  

  
  /* @Method <This method is used to Asset Unlinking>
    @param <It is taking OpportunityId as Parameter>
    @returntype - String SuccessMessage >
    @throws exception - <No Exception>
    */  
  webservice static string AssetUnlinking(Id LeadId, List<Id> linkIds){
      //Added for restricting access to 'EMC Contract Renewals User' Profile from linking an Asset to Opportunities
        String returnMessage = 'cantUnLink';
        User u = [select Id,Name, ProfileID from User WHERE id = :UserInfo.getUserId()];
        Profile pr = [SELECT Id, Name FROM Profile WHERE id = :u.ProfileID];
        if(pr.Name == System.Label.R2R_Restricted_Profile_Name){
      return returnMessage; //System.Label.R2R_Insufficient_Privilege_Error      
        }
        //Added for restricting access to 'EMC Contract Renewals User' Profile from linking an Asset to Opportunities - Ends here
        String successmessage = 'failure';
        map<string,customsettingdatavaluemap__c> datavaluemap = customsettingdatavaluemap__c.getall();
        string R2R_Admin_Profiles = datavaluemap.get('R2R_Admin_Profiles').datavalue__c;
        String R2R_UserIdsToByPass = datavaluemap.get('R2R_UserIdsToByPass').datavalue__c;
        Lead leadrecord = null;
        try {
            
             System.Debug('-->'+userinfo.getProfileId());                               
            if (R2R_Admin_Profiles.contains(userinfo.getProfileId()) || R2R_UserIdsToByPass.contains(userinfo.getUserId())){
    
                
                List<Lead_Asset_Junction__c> lstlinks = [select Id, Related_Asset__c from Lead_Asset_Junction__c where id in :linkIds];
                // WR- 273 Calls the deferAsset() method if the 'Unlink & Defer' button is clicked and not the 'Unlink' button
                if(isDefer) {
                    deferAsset (lstlinks)  ;
                }
                delete lstlinks;
                 successmessage = 'success';
                   }
            if(successmessage !='success'){         
                leadrecord = [Select Id,name from Lead  where id =:LeadId
                                            and Status NOT IN ('Closed','Converted To Opportunity')];
            }                                
            if (leadrecord !=null){
                 List<Lead_Asset_Junction__c> lstlinks=[select id,Related_Asset__c from Lead_Asset_Junction__c where id in :linkIds];
                // WR- 273 Calls the deferAsset() method if the 'Unlink & Defer' button is clicked and not the 'Unlink' button                    
                if(isDefer){
                    deferAsset (lstlinks ) ;
                }
                 delete lstlinks;
                 successmessage = 'success';
                   }                                
                                                   
        }catch(exception e){
            successmessage = 'failure'+e;
            
        }
        return  successmessage;
   }
  
    // This webservice will add the logic to the custom button unlink & Defer(in object Lead_Asset_Junction__c) 
    webservice static string assetDeferUnliking(Id LeadId, List<Id> linkIds){ 
        String returnMessage='';
        isDefer = true;
        returnMessage = AssetUnlinking(LeadId, linkIds);
        return returnMessage;
    }
    
    //WR- 273  Checks the "Defer Asset" checkbox in one or more assets which were associated with the Opportunity on 'Unlink & Defer' button click  
    public static void deferAsset(List<Lead_Asset_Junction__c> lstLeadAsset ){
        Set<Id> assetIds = new Set<Id>();
        for(Lead_Asset_Junction__c assetJunc : lstLeadAsset) {
            assetIds.add(assetJunc.Related_Asset__c);
        }
        if(assetIds !=null && assetIds.size()>0){
            List<Asset__c> lstAsset = [select Deffered_to_Renewals__c from Asset__c where id in :assetIds];
            if(lstAsset !=null && lstAsset.size()>0){
                for(Asset__c asset : lstAsset) {
                    asset.Deffered_to_Renewals__c = true;
                }
                try{
                    update lstAsset;
                }catch(exception e){}
            }
        }   
    }
  
/* @Method <This method is used Asset can only be attached to Lead when the Lead ,Opportunity and the Asset are within the same Customer Profiled Account family>
    @param <It is taking List<Lead_Asset_Junction__c> as Parameter>
    @returntype - <String> >
    @throws exception - <No Exception>
    */  
    public String  ProfileValidate(List<Lead_Asset_Junction__c> lstjnt){
        set<id> setLeadIds = new set<id>();
        set<id> setAssetIds = new set<id>();
          for(Lead_Asset_Junction__c leadAssetJunctionObj : lstjnt){
              setLeadIds.add(leadAssetJunctionObj.Related_Lead__c);
              setAssetIds.add(leadAssetJunctionObj.Related_Asset__c);
            }
        Map<Id,lead> mapLead = new map<id,lead>([Select id,Related_Account__r.District_Lookup__r.Name from lead where id in :setLeadIds]);
        Map<Id,Asset__c> mapAsset = new map<id,Asset__c>([Select id,District_Lookup_from_Account__c from Asset__c where id in :setAssetIds]);   
         for(Lead_Asset_Junction__c las : lstjnt){
           if(mapAsset.get(las.Related_Asset__c).District_Lookup_from_Account__c != mapLead.get(las.Related_lead__c).Related_Account__r.District_Lookup__r.Name){
             errormessage='Asset can only be attached to Lead when the Lead ,Opportunity and the Asset are within the same Customer Profiled Account family';
               }
             }
              return errormessage;
    }
    
/* @Method <This method is used to Lead and Opportunity should have same Type>
    @param <It is taking Map<Lead,List<Opportunity>> as Parameter>
    @returntype - <String> >
    @throws exception - <No Exception>
    */ 
  public String validateleadOpprtSameType(Map<Lead,List<Opportunity>> leadwithOppt){
        for(Lead lbj:leadwithOppt.keyset()){ 
            List<Opportunity> opty = new List<Opportunity>();
                opty=leadwithOppt.get(lbj);
            for(Opportunity optys:opty){
                if(lbj.Lead_Type_Based_on_Linking__c!=optys.Opportunity_Type__c)
                    errormessage='Lead and Opportunity should have same type';
                        }
                    }
        return errormessage;
        } 
    
/* @Method <This method is used asset is attached to a lead with a status of New, Working, Nurture, Appointment or Qualified ? it cannot be linked to a Refresh, Refresh/Swap or Swap Opportunity>
    @param <It is taking Map<Lead,List<Asset__c>> as Parameter>
    @returntype - <String> >
    @throws exception - < Exception>
    */  
   public String Assetvalidatelinked(Id LeadId,List<Id> opty)
   {
           Opportunity oppty;
          try{
           List<Lead> IsLead =[Select Id,Related_Opportunity__c from Lead  where id =:LeadId 
                                  and Status IN ('New','Working','Nurture','Appointment','Qualified')];
                   if(oppty!=null){
                     List<Opportunity> IsOpty=[Select Id,Opportunity_Type__c from Opportunity where id=:opty and 
                         Opportunity_Type__c NOT in('Refresh','Refresh/Swap')];
                            errormessage = 'cannot be linked to a Refresh, Refresh/Swap or Swap Opportunity';
                                 }
            }catch(exception e)
                 { errormessage='errormessage'+e;}
                       return errormessage;
  }   
/* @Method <This method is used to If an asset is attached to a lead with a status of ?Passed to Renewals? it cannot be linked to a Renewals Opportunity >
    @param <It is taking Map<Id,List<Id>> as Parameter>
    @returntype - <String> >
    @throws exception - <No Exception>
    */       
   public String AssetvalidatelinkedLead(Map<Id,List<Id>> mapleadIdwithasset){
      set<Id> setAssetId = new set<Id>(); 
      for(Id leadId :mapleadIdwithasset.keyset()){
            for(id assetId :mapleadIdwithasset.get(leadId)){
                setAssetId.add(assetId);
            }   
                
        }
        Map<Id,Lead_Asset_Junction__c> mapLeadAssetJunction = new map<id,Lead_Asset_Junction__c>([Select Id,Related_Asset__c,Related_Lead__c from Lead_Asset_Junction__c
                                                                                                  where Related_Asset__c in :setAssetId and Related_Lead__c in :mapleadIdwithasset.keyset()]);
        Map<Id,Lead> mapLead = new map<Id,Lead>([Select Id,Status from Lead where id in :mapleadIdwithasset.keyset() and Status ='Pass to Renewals']);
        Map<Id,Asset__c> mapAssets = new map<Id,Asset__c>([Select Id,RecordType.DeveloperName from Asset__c where id in:setAssetId and RecordType.DeveloperName='Competitive Install']);
        for(Id leadId :mapleadIdwithasset.keyset()){
            for(id assetId : mapleadIdwithasset.get(leadId)){
            if(mapLead .containskey(leadId) && mapAssets.containskey(assetId)){
                    errormessage = 'A Competitive asset cannot be linked to an Renewal opportunity';
                            }
                       }
                     }
                  
                        return errormessage;
           }
     
/* @Method <This method is used to An asset (EMC or Competitive) can only be linked to one open lead at a time>
    @param <It is taking List<Asset__c> as Parameter>
    @returntype - <String> >
    @throws exception - <No Exception>
    */  
    public String Assetlinkedtopenlead(List<Asset__c> leadwithasset){
          
          for(Asset__c ast:[Select Id from Asset__c where Id not in:leadwithasset])
                    {
                        Asset__c IsAsset=new Asset__c();
                        IsAsset.Id=ast.Id;
                        
                        errormessage = 'An asset (EMC or Competitive) can only be linked to one open lead at a time';
                        
                      }
            
               return errormessage;
                 }
 
  public static set<string> insertAssetLeadLinks(id LeadId, List<id> lstselectedAssetId){
        set<string> seterrormessage = new set<string>();
        string errormessage='';
        List<Lead_Asset_Junction__c> lstAssetJunction = new list<Lead_Asset_Junction__c>();
        for(id assetid :lstselectedAssetId){
            Lead_Asset_Junction__c leadAssetJunctionObj = new Lead_Asset_Junction__c();
            leadAssetJunctionObj.Related_Asset__c = assetid;
            leadAssetJunctionObj.Related_Lead__c= LeadId;
            lstAssetJunction.add(leadAssetJunctionObj);
        }
        if(!lstAssetJunction.isempty()){            
            try{
                insert lstAssetJunction;
            }
            catch(exception e){
               errormessage=e.getdmlmessage(0);
               seterrormessage.add(errormessage);
               system.debug(errormessage);
            }
        }
         return seterrormessage;
           }
   public static void mapAssetsToOppty(map<id,lead> convertedToOpptyLeads){        
        List<Lead_Asset_Junction__c> lstAssetJunction = new List<Lead_Asset_Junction__c>();
        map<Id,List<Id>> mapOpptyWithAsset = new map<id,list<id>>();
        map<Lead,map<Id,List<Id>>> mapLeadwithOpptyAssets = new map<Lead,map<Id,List<Id>>>();
        try{
            lstAssetJunction = [Select Id, Related_Lead__c,Related_Asset__c from Lead_Asset_Junction__c where 
                            Related_Lead__c in :convertedToOpptyLeads.keyset()];
        }        
        catch (exception e){
                
        }      
        if(!lstAssetJunction.isempty()){
           for(Lead_Asset_Junction__c junctionObj :lstAssetJunction){
               
                List<Id> lstAssetId = mapOpptyWithAsset.get(convertedToOpptyLeads.get(junctionObj.Related_Lead__c).Related_Opportunity__c);
                if(lstAssetId==null){
                    lstAssetId = new list<Id>();
                }
                lstAssetId.add(junctionObj.Related_Asset__c);
                mapOpptyWithAsset.put(convertedToOpptyLeads.get(junctionObj.Related_Lead__c).Related_Opportunity__c,lstAssetId);
                mapLeadwithOpptyAssets.put(convertedToOpptyLeads.get(junctionObj.Related_Lead__c),mapOpptyWithAsset);
           }     
        }
        if(!mapLeadwithOpptyAssets.isempty()){
           for(Lead leadObj:mapLeadwithOpptyAssets.keyset()){
                try{
                insertOpportunityAssetJunction(mapLeadwithOpptyAssets.get(leadObj));
                }
                catch(exception e){
                    if(e.getTypeName()=='System.DmlException'){
                        leadObj.adderror(e.getDMLmessage(0));
                    }
                }
            }   
        }
   }        
   public static void insertOpportunityAssetJunction(map<id,List<Id>> mapOpptyWithAsset){
        List<Opportunity_Asset_Junction__c> lstAssetJunctionToInsert = new list<Opportunity_Asset_Junction__c>();
        for(id opptyId :mapOpptyWithAsset.keyset()){
            for(id assetId :mapOpptyWithAsset.get(opptyId) ){
                Opportunity_Asset_Junction__c assetJunctionObj = new Opportunity_Asset_Junction__c();
                assetJunctionObj.Related_Asset__c = assetId;
                assetJunctionObj.Related_Opportunity__c = opptyId;
                lstAssetJunctionToInsert.add(assetJunctionObj);
            }
        
        }
        if(lstAssetJunctionToInsert.size()>0){
           try {
               insert lstAssetJunctionToInsert;
           }
           catch(exception e){
                 throw e;          
                 
           }
        }
    }  
     public static void insertLeadAssetJunction(map<Lead,List<Id>> mapleadWithAssetId){
        List<lead_Asset_Junction__c> lstAssetJunctionToInsert = new list<lead_Asset_Junction__c>();
        for(lead leadobj:mapleadWithAssetId.keyset()){
            for(id assetId :mapleadWithAssetId.get(leadobj) ){
                Lead_Asset_Junction__c assetJunctionObj = new lead_Asset_Junction__c();
                assetJunctionObj.Related_Asset__c = assetId;
                assetJunctionObj.Related_lead__c = leadobj.Id;
                lstAssetJunctionToInsert.add(assetJunctionObj);
            }
            if(lstAssetJunctionToInsert.size()>0){
                try{
                    insert lstAssetJunctionToInsert;
                }
                catch(exception e){
                      string errormessage=e.getdmlmessage(0);
                     leadobj.adderror(errormessage);
                } 
           }
        
        }
        
    }
    
     /* 
     @Method <This method is used to calculate the number of assets associated with a lead>   
    */  
    //WR-294  R2R Nov'14 Release 
    public void updateLeadAssetCount(List<Lead_Asset_Junction__c> listofleadassetjunc){
        
        //Collect all the associated leads
        Set<Id> setofLeadids= new Set<Id>(); 
        for(Lead_Asset_Junction__c laj:listofleadassetjunc){
            setofLeadids.add(laj.Related_Lead__c);      
        }
    
        if(setofLeadids !=null && setofLeadids.size()>0){ 
            // This query here returns the 'related leads' and count of 'related assets' in the Lead_Asset_Junction__c object 
            Map<Id,Lead> mapofLeadToUpdate= new  Map<Id,Lead>([Select id,Asset_Count__c from Lead where id in:setofLeadids]);        
            if(mapofLeadToUpdate !=null && mapofLeadToUpdate.size()>0){
                for(Lead leadRec: mapofLeadToUpdate.values()){
                    leadRec.Asset_Count__c=0;
                }
            }
            
            List<aggregateResult> results = [select Related_Lead__c, COUNT_DISTINCT(Related_Asset__c) AssetCount from Lead_Asset_Junction__c where Related_Lead__c in:setofLeadids group by Related_Lead__c ];   
            if(results.size()>0&& results!= null ){
                for(AggregateResult ar : results) {
                    Lead lead = mapofLeadToUpdate.get((Id)ar.get('Related_Lead__c'));
                    lead.Asset_Count__c = Integer.valueOf(ar.get('AssetCount')); 
                }  
            }
            //Update the count for each lead
            if (mapofLeadToUpdate.size()>0 && !mapofLeadToUpdate.isEmpty()){
                update mapofLeadToUpdate.values();    
            }
        }   
    }
    
 /* @Method <This method is used to set the lead Type(Lead_Type_Based_on_Linking__c) based on the lead status and type of asset linked to it>
   
    */    
    public void setLeadType(List<Lead_Asset_Junction__c> listofleadassetjunc){
            Map<String,CustomSettingDataValueMap__c> datavalue= CustomSettingDataValueMap__c.getall();
            string leadstatus= datavalue.get('R2R_lead_Status').datavalue__c;
            System.Debug(leadstatus);
            String leadpasstorenewals= datavalue.get('R2R_Lead_passtorenewals').datavalue__c;
            set<Id> setofLeadids= new set<Id>(); 
            set<Id> setofAssetids= new set<Id>(); 
            List<Lead_Asset_Junction__c> leadassjun= new  List<Lead_Asset_Junction__c>();     
            Map<Lead,List<Lead_Asset_Junction__c>> mapofleadwithEMCAssets = new map<Lead,List<Lead_Asset_Junction__c>>();  
            Map<Lead,List<Lead_Asset_Junction__c>> mapofleadwithCompAssets = new Map<Lead,List<Lead_Asset_Junction__c>>(); 
          
            //INC0729833 - R2R Nov'14 Rel : Related Leads/Opps should appear in global search of Asset Serial Number
            Map<Id,String> leadIdAssetSerialMap = new Map<Id,String>();
            for(Lead_Asset_Junction__c laj:listofleadassetjunc){
                setofLeadids.add(laj.Related_Lead__c);
                setofAssetids.add(laj.Related_Asset__c);  
            }
            Map<Id,Lead> mapofLeadToUpdate= new  Map<Id,Lead>([Select id,Status,Lead_Type_Based_on_Linking__c from Lead where id in:setofLeadids]); 
            Map<Id,Lead_Asset_Junction__c> mapAsset = new map<Id,Lead_Asset_Junction__c>([Select Id,Related_Lead__c,Related_Asset__c,Related_Asset__r.RecordType.DeveloperName from Lead_Asset_Junction__c where Related_Lead__c in:setofLeadids]);
            
            //INC0729833 - R2R Nov'14 Rel : Related Leads/Opps should appear in global search of Asset Serial Number
            if(mapAsset != null && mapAsset.size()>0){
                for(Lead_Asset_Junction__c laj: mapAsset.values()){
                    setofAssetids.add(laj.Related_Asset__c);
                }   
            }
            Map<Id,Asset__c> assetIdMap = new Map<Id,Asset__c>([Select Id,Serial_Number__c from Asset__c where Id IN: setofAssetids]);
            for(Lead_Asset_Junction__c assetObj: mapAsset.values()){
            
                //INC0729833 - R2R Nov'14 Rel : Related Leads/Opps should appear in global search of Asset Serial Number
                if(leadIdAssetSerialMap !=null && leadIdAssetSerialMap.containsKey(assetObj.Related_Lead__c)){
                    String tempStr = leadIdAssetSerialMap.get(assetObj.Related_Lead__c);
                    if(assetIdMap !=null && assetIdMap.containsKey(assetObj.Related_Asset__c)){
                        tempStr = tempStr + ';' + assetIdMap.get(assetObj.Related_Asset__c).Serial_Number__c;
                    }
                    leadIdAssetSerialMap.put(assetObj.Related_Lead__c,tempStr);
                }
                else{
                    if(assetIdMap !=null && assetIdMap.containsKey(assetObj.Related_Asset__c)){
                        String tempStr = assetIdMap.get(assetObj.Related_Asset__c).Serial_Number__c;
                        leadIdAssetSerialMap.put(assetObj.Related_Lead__c,tempStr);
                    }
                }
                lead leadObj = mapofLeadToUpdate.get(assetObj.Related_Lead__c);               
                if(assetObj.Related_Asset__r.RecordType.DeveloperName=='EMC_Install'){
                    list<Lead_Asset_Junction__c> listofEmcAssets = mapofleadwithEMCAssets.get(leadObj);
                    if(listofEmcAssets==null){
                        listofEmcAssets = new list<Lead_Asset_Junction__c>();
                    }
                    listofEmcAssets.add(assetObj);
                    mapofleadwithEMCAssets.put(leadObj,listofEmcAssets);
                }
                else if(assetObj.Related_Asset__r.RecordType.DeveloperName=='Competitive_Install'){
                    list<Lead_Asset_Junction__c> listofCompAssets = mapofleadwithCompAssets.get(leadObj);
                    if(listofCompAssets==null){
                        listofCompAssets = new list<Lead_Asset_Junction__c>();
                    }
                    listofCompAssets.add(assetObj);
                    mapofleadwithCompAssets.put(leadObj,listofCompAssets);
                }
                
            }
            for(lead leadObj :mapofLeadToUpdate.values()){
                if(leadstatus.contains(leadObj.status) && mapofleadwithEMCAssets.get(leadObj)!=null && mapofleadwithCompAssets.get(leadObj)==null){
                    leadObj.Lead_Type_Based_on_Linking__c ='Refresh Lead';
                }
                else if(leadstatus.contains(leadObj.status) && mapofleadwithEMCAssets.get(leadObj)==null && mapofleadwithCompAssets.get(leadObj)!=null){
                    leadObj.Lead_Type_Based_on_Linking__c ='Swap Lead';
                }
                else if(leadstatus.contains(leadObj.status) && mapofleadwithEMCAssets.get(leadObj)!=null && mapofleadwithCompAssets.get(leadObj)!=null){
                    leadObj.Lead_Type_Based_on_Linking__c ='Refresh/Swap Lead';
                }
                else if(leadObj.status == 'Pass To Renewals'&& mapofleadwithEMCAssets.get(leadObj)!=null && mapofleadwithCompAssets.get(leadObj)==null){
                    leadObj.Lead_Type_Based_on_Linking__c ='Renewals Lead';
                }   
                else if(mapofleadwithEMCAssets.get(leadObj)==null && mapofleadwithCompAssets.get(leadObj)==null){
                    leadObj.Lead_Type_Based_on_Linking__c ='';
                }

                //INC0729833 - R2R Nov'14 Rel : Related Leads/Opps should appear in global search of Asset Serial Number
                leadObj.Assets_Serial_Number__c = leadIdAssetSerialMap.get(leadObj.Id);
            }
            update mapofLeadToUpdate.values();
        }
    public static map<lead,string> validatePassTorenewalsLead(map<id,Lead> mapfromTrigger){
        map<lead,string> mapLeadWithError = new map<lead,string>();
        List<Lead_Asset_Junction__c> lstAssetJunction = [Select Id,related_lead__c,Related_Asset__r.RecordType.DeveloperName from Lead_Asset_Junction__c where related_lead__c in :mapfromTrigger.keyset()];
        for(Lead_Asset_Junction__c junctionObj :lstAssetJunction){
            if(junctionObj.Related_Asset__r.RecordType.DeveloperName =='Competitive_Install'){
               mapLeadWithError.put(mapfromTrigger.get(junctionObj.related_lead__c),'The status of a lead cannot be changes to Pass to Renewals if a Competitive Asset is attached to the lead.');
            }
        }
        return mapLeadWithError;
    } 
    public static void validateOpportunityConversion(map<id,Lead> mapfromTrigger){
        map<string,CustomSettingDatavalueMap__c> datavaluemap = CustomSettingDatavalueMap__c.getall();
        string ClosedOpptyStatus = datavaluemap.get('OpptyClosedStatus').datavalue__c;
        string deletedStatus = datavaluemap.get('R2R_Install_Base_Status').datavalue__c;
        string R2RAdminProfiles= datavaluemap.get('R2R_Admin_Profiles').datavalue__c;
        map<lead,string> mapLeadWithError = new map<lead,string>();
        map<id,List<Lead_Asset_Junction__c>> mapLeadwithAssetJunction = new map<id,List<Lead_Asset_Junction__c>>();
        List<Lead_Asset_Junction__c> lstAssetJunction = [Select Id,related_lead__c,Related_Asset__r.Install_Base_Status__c,Related_Asset__r.RecordType.DeveloperName from Lead_Asset_Junction__c where related_lead__c in :mapfromTrigger.keyset()];
        set<id> setOpptyId = new set<id>();
        for(lead leadObj :mapfromTrigger.values()){            
               setOpptyId.add(leadObj.Related_Opportunity__c);            
        }
         map<Id,Opportunity> mapRenewalOppty = new map<Id,Opportunity>([Select Id,Opportunity_Type__c,StageName from Opportunity where id in :setOpptyId]);
        if(!lstAssetJunction.isempty()){
            for(Lead_Asset_Junction__c assetJunction :lstAssetJunction ){
                List<Lead_Asset_Junction__c> assetJunctionlist = mapLeadwithAssetJunction.get(assetJunction.Related_Lead__c);
                if(assetJunctionlist ==null){
                   assetJunctionlist = new List<Lead_Asset_Junction__c>();
                }
                assetJunctionlist.add(assetJunction);
                mapLeadwithAssetJunction.put(assetJunction.Related_Lead__c,assetJunctionlist);
            }
        }                                                
        System.debug(mapLeadwithAssetJunction);                                                 
        for(id leadId:mapfromTrigger.keyset()){
            if(mapLeadwithAssetJunction !=null){
                if(mapLeadwithAssetJunction.get(leadId) !=null){
                   for(Lead_Asset_Junction__c junctionObj:mapLeadwithAssetJunction.get(leadId)){
                       if(deletedStatus.contains(junctionObj.Related_Asset__r.Install_Base_Status__c)){
                           mapfromTrigger.get(leadId).adderror(System.Label.R2R_Asset_InstallBaseStatusError);
                          
                       } 
                    }
                }
                if(!mapRenewalOppty.isempty()){
                       if(mapLeadwithAssetJunction !=null){
                           if(mapLeadwithAssetJunction.get(leadId) !=null){
                                for(Lead_Asset_Junction__c junctionObj:mapLeadwithAssetJunction.get(leadId)){
                                    if(junctionObj.Related_Asset__r.RecordType.DeveloperName=='Competitive_Install' && mapRenewalOppty.get(mapfromTrigger.get(leadId).Related_Opportunity__c).Opportunity_Type__c=='Renewals'){
                                        mapfromTrigger.get(leadId).adderror(System.Label.R2R_Asset_Link_Competitive);
                                    }
                                    //WR 291(c) - R2R Nov'14 Rel - Reps should be allowed to link/unlink assets from BOOKED,EVAL and SUBMITTED Non-Renewals opportunity
                                    //else if(!R2RAdminProfiles.contains(userInfo.getProfileId()) && ClosedOpptyStatus.contains(mapRenewalOppty.get(mapfromTrigger.get(leadId).Related_Opportunity__c).StageName)){
                                    //else if(!R2RAdminProfiles.contains(userInfo.getProfileId()) && ClosedOpptyStatus.contains(mapRenewalOppty.get(mapfromTrigger.get(leadId).Related_Opportunity__c).StageName) && mapRenewalOppty.get(mapfromTrigger.get(leadId).Related_Opportunity__c).StageName != 'Booked'){
                                    //if(!R2RAdminProfiles.contains(userinfo.getProfileId()) && ((ClosedOpptyStatus.contains(mapRenewalOppty.get(mapfromTrigger.get(leadId).Related_Opportunity__c).stagename) && mapRenewalOppty.get(mapfromTrigger.get(leadId).Related_Opportunity__c).stagename != 'Booked') || (ClosedOpptyStatus.contains(mapRenewalOppty.get(mapfromTrigger.get(leadId).Related_Opportunity__c).stagename) && mapRenewalOppty.get(mapfromTrigger.get(leadId).Related_Opportunity__c).Opportunity_Type__c == 'Renewals' ))){
                                    if(!R2RAdminProfiles.contains(userinfo.getProfileId()) && ((mapRenewalOppty.get(mapfromTrigger.get(leadId).Related_Opportunity__c).stagename == 'Closed') || (ClosedOpptyStatus.contains(mapRenewalOppty.get(mapfromTrigger.get(leadId).Related_Opportunity__c).stagename) && mapRenewalOppty.get(mapfromTrigger.get(leadId).Related_Opportunity__c).Opportunity_Type__c == 'Renewals' ))){
                                        mapfromTrigger.get(leadId).adderror(System.Label.R2R_Asset_Link_closed_Opty);
                                    }
                                }   
                            }    
                        }
                }
            }    
        }
        //return mapLeadWithError;
    }

    /* @WR - CI:1872
    @Method <This method is used to update assets based on lead info>
    @param <It is taking List<Lead_Asset_Junction__c> as Parameter>
    @returntype - void
    @throws exception - <No Exception>
    */  
    public static void updateAssetFromLead(List<Lead_Asset_Junction__c> lstTriggeredData) {
        Set<Id> assetIds = new Set<Id>();
        for(Lead_Asset_Junction__c obj : lstTriggeredData) {
            assetIds.add(obj.Related_Asset__c);
        }
            
        Map<Id, Lead_Asset_Junction__c> assetLeadMap = new Map<Id, Lead_Asset_Junction__c>();
        List<Lead_Asset_Junction__c> lstLeadAssetJunction = [Select Id, Related_Asset__c, Related_Lead__c, Related_Lead__r.Name, Related_Lead__r.Lead_Number__c, Related_Lead__r.Status from Lead_Asset_Junction__c where Related_Asset__c = :assetIds and Related_Lead__r.Status != 'Closed' order by CreatedDate ASC];
        
        for(Lead_Asset_Junction__c obj : lstLeadAssetJunction) {
            if(!assetLeadMap.containsKey(obj.Related_Asset__c)) {
                assetLeadMap.put(obj.Related_Asset__c, obj);
            }
        }

        List<Asset__c> lstAsset = new List<Asset__c>();
        for(Id id : assetLeadMap.keySet()) {
            Lead_Asset_Junction__c obj = assetLeadMap.get(id);
            Asset__c asset = new Asset__c();
            asset.Id = obj.Related_Asset__c;
            asset.Lead_Name__c = obj.Related_Lead__c;
            asset.Lead_Number__c = obj.Related_Lead__r.Lead_Number__c;
            asset.Lead_status__c = obj.Related_Lead__r.Status;
            lstAsset.add(asset);
        }
        for(Lead_Asset_Junction__c laj : lstTriggeredData) {
            if(!assetLeadMap.containsKey(laj.Related_Asset__c)) {
                Asset__c asset1 = new Asset__c();
                asset1.Id = laj.Related_Asset__c;
                asset1.Lead_Name__c = null;
                asset1.Lead_Number__c = '';
                asset1.Lead_status__c = '';
                lstAsset.add(asset1);
            }
        }
        update lstAsset;    
    }
 }