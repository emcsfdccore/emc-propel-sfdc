/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       		WR          DESCRIPTION                               
 |  ====            =========       		==          =========== 
 |  01.06.2013       EAG App Admins     		        Initial Creation.
 |  16.11.2013      Bhanuprakash         WR319143       Added Test specific queries in 3 places(Removee later)
 |	30.07.2014		Srikrishna			 WR358243		Filtering Education Service Artilce Types being added to PayLoad Object														
 +===========================================================================*/
global class KB_BatchOutboundMessages implements Database.Batchable<sObject> { 

    public String ArticleQuery = '';    
    public DateTime BatchExecution; 
    public List<Knowledge_Integration_Trigger__c> lstKnowIntTgrToInsert = new List<Knowledge_Integration_Trigger__c>();
    public List<KB_Outbound_Payload__c> lstKB = new List<KB_Outbound_Payload__c>();
    //public String dt = '2013-03-01T11:29:26.000Z';
    //Parameterised constructor
    global KB_BatchOutboundMessages (){ 
        //List<KnowledgeArticleVersion> lstKnowPub = [Select Id, PublishStatus from KnowledgeArticleVersion where PublishStatus='Online' AND Language='en_US' limit 1000];
        ArticleQuery = 'Select Id, Article_Type__c, Knowledge_Article_Version_Id__c from KB_Outbound_Payload__c WHERE Is_Processed__c = False';
		//BatchExecution = System.Now();
    }
    // Method : To Start the batch job  
    global Database.QueryLocator start(Database.BatchableContext BC){
        Map<String,KB_BatchStartTime__c> mapCS = KB_BatchStartTime__c.getAll();
        for(KB_BatchStartTime__c objKB : mapCS.values())
            BatchExecution = objKB.Start_Time__c;           
        createBatchToProcess();
        for(KB_BatchStartTime__c objKB : mapCS.values()){
            objKB.Start_Time__c = System.NOW();
            update objKB;
        }
        //BatchExecution = System.Now().AddMinutes(-15);
        Database.QueryLocator db = Database.getQueryLocator(ArticleQuery);
        return db;
   }

   private void createBatchToProcess(){
        String strQuery = '';
        strQuery += 'SELECT Id, KnowledgeArticleId, ArticleNumber, LastModifiedDate, ArticleType FROM KnowledgeArticleVersion ';
        strQuery += ' WHERE PublishStatus = \'Online\' AND Language = \'en_US\'';
        strQuery += ' AND LastModifiedDate>:BatchExecution' ;
        List<KnowledgeArticleVersion> lstKnowArtPubVersion = Database.query(strQuery);

        String strQuery1 = '';
        strQuery1 += 'SELECT Id, KnowledgeArticleId, ArticleNumber, LastModifiedDate, ArticleType FROM KnowledgeArticleVersion ';
        strQuery1 += ' WHERE PublishStatus = \'Archived\' AND Language = \'en_US\'';
        strQuery1 += ' AND LastModifiedDate >:BatchExecution';
        List<KnowledgeArticleVersion> lstKnowArtArchVersion = Database.query(strQuery1);
        
        List<KB_Outbound_Payload__c> lstKB = new List<KB_Outbound_Payload__c>();
        for(KnowledgeArticleVersion objKAV : lstKnowArtPubVersion){
            lstKnowArtArchVersion.add(objKAV);
        }
        //Custom Setting code added for WR358243
        Map<String,KB_ExcludedArticleTypes__c> mapKB = KB_ExcludedArticleTypes__c.getAll();
        String articleTypeName = mapKB.get('EducationService').datavalue__c;
        
        for(KnowledgeArticleVersion objKAV : lstKnowArtArchVersion){
            //If condition added for WR358243
            if(!articleTypeName.contains(objKAV.ArticleType)){
	            KB_Outbound_Payload__c obj = new KB_Outbound_Payload__c();
	            obj.Article_Type__c = objKAV.ArticleType;
	            obj.Knowledge_Article_Version_Id__c = objKAV.Id;
	            obj.Article_Number__c = objKAV.ArticleNumber;
	            lstKB.add(obj);
        	}
        }
        try {
            Database.insert(lstKB);
        } catch (Exception ex){
            String errMsg = ex.getMessage();
            system.Debug(errMsg);
        } 
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<KB_Outbound_Payload__c> lstArticleToProcess = (KB_Outbound_Payload__c[])scope;
        for(KB_Outbound_Payload__c onjKnowArt : lstArticleToProcess){
        		Knowledge_Integration_Trigger__c knowIntTgr=  new Knowledge_Integration_Trigger__c();
                knowIntTgr.Object_ID__c = onjKnowArt.Knowledge_Article_Version_Id__c;
                knowIntTgr.Object_Type__c = onjKnowArt.Article_Type__c;
                lstKnowIntTgrToInsert.add(knowIntTgr);	
        }
        try {
            Database.insert(lstKnowIntTgrToInsert);
        } catch (Exception ex){
            String errMsg = ex.getMessage();
            system.Debug(errMsg);
        }
    }

    global void finish(Database.BatchableContext BC){
        List<KB_Outbound_Payload__c> lstProcessedPayLoad = new List<KB_Outbound_Payload__c>();
        List<KB_Outbound_Payload__c> lstProcessedPayLoadRecords = [Select Id,Is_Processed__c from KB_Outbound_Payload__c where Is_Processed__c=False];
        for(KB_Outbound_Payload__c objOB:lstProcessedPayLoadRecords){
            objOB.Is_Processed__c = True;
            lstProcessedPayLoad.add(objOB);
        }
        try {
            Database.update(lstProcessedPayLoad);
        } catch (Exception ex){
            String errMsg = ex.getMessage();
            system.Debug(errMsg);
        }
        List<CronTrigger> listCronTrigger = [select Id from CronTrigger where State = 'DELETED' and nextfiretime = null];

        if (listCronTrigger.size() > 0){
           for (Integer i = 0; i < listCronTrigger.size(); i++) 
               System.abortJob(listCronTrigger[i].Id); 
        }
        DateTime n = datetime.now().addMinutes(15);
          String cron = '';
          cron += n.second();
          cron += ' ' + n.minute();
          cron += ' ' + n.hour();
          cron += ' ' + n.day();
          cron += ' ' + n.month();
          cron += ' ' + '?';
          cron += ' ' + n.year();
          
        String jobName = 'Batch Job To Send OBMs - ' + n.format('MM-dd-yyyy-hh:mm:ss');
        
        KB_BatchOutboundMessagesScheduler objScheduler = new KB_BatchOutboundMessagesScheduler();
        Id scheduledJobID = System.schedule(jobName,cron,objScheduler);
   }
}