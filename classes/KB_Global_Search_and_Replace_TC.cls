/*==================================================================================================================+                                                                          
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        ===========  
 |  Dec/17/2014   Bisna V P    CI 1127     Updated to solve -Find and Replace tool discrepancy 
 ====================================================================================================================*/
@isTest (SeeAllData=true)
private class KB_Global_Search_and_Replace_TC {

    static testMethod void KBGlobalSearchReplace(){  
          
        test.startTest();  
              
        Break_Fix__kav breakfix = new Break_Fix__kav();
        breakfix.Title = 'TTTest Class Break Fix';         
        breakfix.Cause__C='Bisna vadakkina'; 
        breakfix.ValidationStatus = 'Work In Progress';
        breakfix.UrlName = 'TTTest-Class-Break-fix'; 
        breakfix.Language = 'en_US';      
        insert breakfix; 
        
        List<Id> idList = new List<Id>();
        idList.add(breakfix.id);
        
        KnowledgeArticleVersion kav= [SELECT id,ArticleType, KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE  id in :idList];                
        KbManagement.PublishingService.publishArticle(kav.KnowledgeArticleId, true); 
        
        
                               
        List<String> liststrFields= new List<String>();
        liststrFields.add('Title');
        liststrFields.add('Cause__c');
        
        KB_Global_Search_and_Replace searchreplace= new KB_Global_Search_and_Replace();
        //1127 changes
        searchreplace.strArticleType='Break_Fix__kav';
        searchreplace.getArticleTypes();
        searchreplace.getPublishStatus();
        searchreplace.getArticleFields1();
        searchreplace.isPublish();                      
        searchreplace.strPublishStatus ='Draft';
        searchreplace.strSearchString= 'Bisna';       
        searchreplace.strReplacementString ='SuccessTest';
        searchreplace.strFields =liststrFields;
        searchreplace.bSearchOnly=false;
        searchreplace.bDisableRegex=false;
        searchreplace.publish=false;
        searchreplace.bCaseSensitive = false;        
        searchreplace.bProcessing = false;      
        searchreplace.performSearch();
        searchreplace.refresh();
        //1127 changes ends here
    
            
        test.stopTest();
        
        
   }          
}