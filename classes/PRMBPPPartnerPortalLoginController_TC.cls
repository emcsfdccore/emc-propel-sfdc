/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
|  17/Sep/2014  Jaypal Nimesh       BPP Project      Test Class for PRMBPPPartnerPortalLoginController
|
+=====================================================================================================*/

@isTest
private class PRMBPPPartnerPortalLoginController_TC{

    static testmethod void portalLoginCtrlMethod(){
    
        //Calling CustomSettingDataHelper class method to get BPP data
        CustomSettingDataHelper.BusinessPartnerProgramData();
        PRMBPPPartnerPortalLoginController portalloginctrl = new PRMBPPPartnerPortalLoginController();
        PageReference page = portalloginctrl.login();
    }
}