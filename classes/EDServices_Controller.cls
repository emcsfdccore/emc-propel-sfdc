/*==================================================================================================================+

|  HISTORY  |                                                                           

|  DATE          DEVELOPER      WR        DESCRIPTION                               

|  ====          =========      ==        =========== 

|  12/13/2012     Shalabh Sharma       This controller is associated with EDServices_Case_Creation and EDServices_Success.Used to save record in
Case and Education Services Contact Object

|  04/03/2014     Abinaya        WR:567 Modified the code to ensure the attachment filename and size should not exceeds the maximum limit.      
|  09/15/2014     Bindu          1294    Changes made to work like SC case form
|  10/14/2014     Bindu          1358    Added Request Type
|  11/07/2014     Vinod          1488    Added logic if the case sub type is 'EMC Proven Professional Support', Update record type to 'Education Services Proven Record Type'.
|  1/14/2015      Bindu          1690    Added logic if the case sub type is 'Certificate Support' or ' Course completion support'
|  2/11/2015      Bindu          1697    Added logic if the case sub type is  'Director Development Support' or 'IC/Manager Development Support'
|  3/17/2015      Bisna          1880    Added logic for new EMCU webform
+==================================================================================================================**/
public class EDServices_Controller {
    
    public Case caseRecord{get;set;}
    public Education_Services_Contact__c eDServicesContact{get;set;}
    public Boolean caseSaved {get; set;} {caseSaved=false;}
    public ID caseRecordId;
    public List<Attachment> attachments;
    public List<Attachment> newAttachments{get;set;}
    public List<Education_Services_Contact__c> lstEdServiceContact = new List<Education_Services_Contact__c>();
    public static final integer attachmentsToAdd = 5;
    public Case cs = new Case();
    PageReference caseUrl;
    //Added for 1294
    String rtName = System.label.EDServices_RecordType;
   // RecordType rt;
    List<RecordType> rtList = new List<RecordType>();
    public Map<String, RecordType> rtMap = new Map<String, RecordType>();
    public String recordTypeValue {get; set;}
    public Boolean showSubmit {get; set;}
    public List<String> fldString{get; set;}
    public Map<String,SC_RecordType_Mapping__c> fldMappingMap{get; set;}
    public String recSubTypeMapping {get; set;}
    public String recordType {get; set;}
    //1880 change
    public String type {get; set;}

    public EDServices_Controller(){
        showSubmit = false;
        fldString = new List<String>();
        fldMappingMap = new Map<String,SC_RecordType_Mapping__c>();
        eDServicesContact = new Education_Services_Contact__c(); 
        caseRecord = new Case();
        //1880 change
        type= ApexPages.CurrentPage().getparameters().get('type');
        System.debug('type--->'+type);
        //1488 Changes 
        //1690 Added Global_Education_Support
        rtMap = new Map<String, RecordType>();
        rtList = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType = 'case' and developername  in ('Education_Services_Proven','Education_Services_Record_Type','Global_Education_Support','EMCU_Record_Type')];
        //rt = [Select Id, Name from RecordType where DeveloperName=:rtName];
       
        for(RecordType rt: rtList){
            rtMap.put(rt.DeveloperName,rt);
        }
        //1880 change
        if(type=='EMCU'){
            recordType = rtMap.get('EMCU_Record_Type').Name; 
            caseRecord.recordtypeid = rtMap.get('EMCU_Record_Type').id;
        }else{
            recordType = rtMap.get('Education_Services_Record_Type').Name; 
            caseRecord.recordtypeid = rtMap.get('Education_Services_Record_Type').id;
        }
        System.debug('RecordType----->'+recordType);
    }
       //End of 1488
    public PageReference saveAttachment()  {
        PageReference pageRef=null;
        try{
            List <Attachment> AttachmentsToInsert = new List <Attachment> (); 
            System.debug('newAttachments--->'+newAttachments);
            if (caseSaved==true) {
                for( Attachment caseAttachment: newAttachments){
                    if(caseAttachment.body!=null  ){
                        //caseAttachment.id=null;
                        System.debug('cs.id '+cs.id);
                        System.debug('caseAttachment.parentId '+caseAttachment.parentId);
                        System.debug('caseAttachment.name'+caseAttachment.name);
                        if(caseAttachment.parentId == null ){
                            caseAttachment.parentId = cs.id;
                            AttachmentsToInsert.add(caseAttachment);
                        }
                    }
                }                
                if(AttachmentsToInsert.size()>0){
                    system.debug('AttachmentsToInsert '+AttachmentsToInsert);
                    insert AttachmentsToInsert;
                    newAttachments = null;
                    AttachmentsToInsert=null;
                    newAttachments= new List<Attachment>();
                    newAttachments.add(new Attachment());
                    newAttachments.add(new Attachment());
                    newAttachments.add(new Attachment());
                    newAttachments.add(new Attachment());
                    newAttachments.add(new Attachment());
                }
                // WR:567-Added below code to ensure the attachment filename and size should not exceeds the maximum limit.
                Case newCaseRecord =[select id,CaseNumber from Case where id = :cs.id];
                if((ApexPages.getMessages()).size()>0 ) {
                    try{
                        List<Attachment> Att_List =[SELECT Id FROM Attachment where ParentId=:cs.id];
                        if(Att_List != null && Att_List.size()>0  ){
                            delete Att_List;
                        }
                    }catch(exception e){
                        system.debug(e);
                    }
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info,Label.SC_FileAttachHelpText));
                    pageRef = null;
                    caseRecordId=cs.id;
                    caseSaved=true;
                    cs.id=null;                    
                    // delete newCaseRecord;
                }else{
                    //pageRef = new PageReference('/apex/SC_Case_Success?caseNmber='+newCaseRecord.CaseNumber+'&langForPage='+selectedLanguage+'&recordType='+recordTypeValue );
                    pageRef = new PageReference('/apex/EDServices_Success' );
                    //pageRef.setRedirect(true);    
                }
                //pageRef = new PageReference('/apex/EDServices_Success?caseNmber='+newCaseRecord.CaseNumber );
                //pageRef = new PageReference('/apex/EDServices_Success' );
                //pageRef.setRedirect(true);    
            }  
        }catch(Exception e){
            ApexPages.addMessages(e);
            pageRef=null;
            System.debug(e);
        }  
        return pageRef;
    }
    
    public void save(){
       
        Education_Services_Contact__c edSer = new Education_Services_Contact__c();
        lstEdServiceContact = [Select Id,Name from Education_Services_Contact__c where Name=:eDServicesContact.Name]; 
        system.debug('edSer--->'+edSer);   
        system.debug('caseRecordId**->'+caseRecordId);     
  
        if(lstEdServiceContact.size()>0){ 
            //1358 adding Request_Type__c           
            cs.Subject = caseRecord.Request_Type__c;
                   
            
            //1488 
            
            if(type=='EMCU'){
            cs.Description = caseRecord.Description+'\n'+'\n'+
                'Request Type: '+caseRecord.Request_Type__c+'\n'+
                'First Name: '+eDServicesContact.First_Name__c+'\n'+
                'Last Name: '+eDServicesContact.Last_Name__c+'\n'+
                'EMC Badge Number: '+caseRecord.Badge__c+'\n'+
                'Contact Phone: '+eDServicesContact.Contact_Phone__c+'\n'+
                'Company Address: '+eDServicesContact.Company_Address__c+'\n'+
                'City: '+eDServicesContact.City__c+'\n'+
                'Country: '+eDServicesContact.Country__c+'\n'+
                'Postal Zip Code: '+eDServicesContact.Post_Zip_Code__c+'\n'+
                //'Audience Type: '+eDServicesContact.Audience_Type__c+'\n'+
                
                'Preferred Language: '+eDServicesContact.Preferred_Language__c+'\n'+
                'Email Address: '+eDServicesContact.Name+'\n'
                ;
            
            
                cs.Task_Type__c ='EMCU'; 
                cs.RecordTypeId = rtMap.get('EMCU_Record_Type').id;                     
            }
            else{
                cs.Description = caseRecord.Description+'\n'+'\n'+
                'Request Type: '+caseRecord.Request_Type__c+'\n'+
                'First Name: '+eDServicesContact.First_Name__c+'\n'+
                'Last Name: '+eDServicesContact.Last_Name__c+'\n'+
                'Company Name: '+eDServicesContact.Company_Name__c+'\n'+
                'Contact Phone: '+eDServicesContact.Contact_Phone__c+'\n'+
                'Company Address: '+eDServicesContact.Company_Address__c+'\n'+
                'City: '+eDServicesContact.City__c+'\n'+
                'Country: '+eDServicesContact.Country__c+'\n'+
                'Postal Zip Code: '+eDServicesContact.Post_Zip_Code__c+'\n'+
                //'Audience Type: '+eDServicesContact.Audience_Type__c+'\n'+
                
                'Preferred Language: '+eDServicesContact.Preferred_Language__c+'\n'
                ;  
                if(caserecord.type == 'EMC Proven Professional Support'){
                    cs.RecordTypeId = rtMap.get('Education_Services_Proven').id;
                }
                
               //End of 1488
                 
               //1690
               //1697 
               if(caserecord.type == 'Certificate Support' || caserecord.type == 'Course Completion Support' || caserecord.type =='Director Development Support' || caserecord.type =='IC/Manager Development Support' ){
                    cs.RecordTypeId = rtMap.get('Global_Education_Support').id;
                }
                if(caserecord.type == 'Director Development Support' || caserecord.type =='IC/Manager Development Support' ){
                    cs.Task_Type__c ='EMCU';
                    }               
            }
            //End of 1690
            //End of 1697
            cs.EDS_Contact_Email__c = lstEdServiceContact[0].Id;
            //cs.Inquiry_Type__c = caseRecord.Inquiry_Type__c;
            cs.Type=caseRecord.Type;
            //1358 adding Request_Type__c
            cs.Request_Type__c= caseRecord.Request_Type__c;
            //Added as part of 1697
            cs.badge__c = caseRecord.badge__c;
            //End of 1697
            
            
            
            if(caseRecordId==null){
                //cs.Webform_Language__c = mapLang.get(selectedLanguage); 
                insert cs;    
            }else{
                cs.id=caseRecordId;   
                update cs;
            }
            caseSaved=true;
        }
        
        else {
            Education_Services_Contact__c eDServCon = new Education_Services_Contact__c();
            eDServCon = eDServicesContact.clone();
            insert eDServCon;
            
            //Case cs = new Case();
            //1358 adding Request_Type__c
            cs.Subject = caseRecord.Request_Type__c;
            
           if(type=='EMCU'){
           
                cs.Description = caseRecord.Description+'\n'+'\n'+
                'Request Type: '+caseRecord.Request_Type__c+'\n'+
                'First Name: '+eDServicesContact.First_Name__c+'\n'+
                'Last Name: '+eDServicesContact.Last_Name__c+'\n'+
                'EMC Badge Number: '+caseRecord.Badge__c+'\n'+
                'Contact Phone: '+eDServicesContact.Contact_Phone__c+'\n'+
                'Company Address: '+eDServicesContact.Company_Address__c+'\n'+
                'City: '+eDServicesContact.City__c+'\n'+
                'Country: '+eDServicesContact.Country__c+'\n'+
                'Postal Zip Code: '+eDServicesContact.Post_Zip_Code__c+'\n'+
                //'Audience Type: '+eDServicesContact.Audience_Type__c+'\n'+
                
                'Preferred Language: '+eDServicesContact.Preferred_Language__c+'\n'+
                'Email Address: '+eDServicesContact.Name+'\n'
                ;
                cs.Task_Type__c ='EMCU';   
                cs.RecordTypeId = rtMap.get('EMCU_Record_Type').id;             
            }
            else{
                cs.Description = caseRecord.Description+'\n'+'\n'+
                'Request Type: '+caseRecord.Request_Type__c+'\n'+
                'First Name: '+eDServicesContact.First_Name__c+'\n'+
                'Last Name: '+eDServicesContact.Last_Name__c+'\n'+
                'Company Name: '+eDServicesContact.Company_Name__c+'\n'+
                'Contact Phone: '+eDServicesContact.Contact_Phone__c+'\n'+
                'Company Address: '+eDServicesContact.Company_Address__c+'\n'+
                'City: '+eDServicesContact.City__c+'\n'+
                'Country: '+eDServicesContact.Country__c+'\n'+
                'Postal Zip Code: '+eDServicesContact.Post_Zip_Code__c+'\n'+
               // 'Audience Type: '+eDServicesContact.Audience_Type__c+'\n'+
                'Preferred Language: '+eDServicesContact.Preferred_Language__c+'\n'
                ;
            //1488
             if(caserecord.type == 'EMC Proven Professional Support'){
              cs.RecordTypeId = rtMap.get('Education_Services_Proven').id;
             }
               
           //End of 1488
            
           //1690
           //1697
           if(caserecord.type == 'Certificate Support' || caserecord.type == 'Course Completion Support'|| caserecord.type =='Director Development Support' || caserecord.type =='IC/Manager Development Support' ){
                cs.RecordTypeId = rtMap.get('Global_Education_Support').id;
            }
            if(caserecord.type == 'Director Development Support' || caserecord.type =='IC/Manager Development Support' ){
                cs.Task_Type__c ='EMCU';
                }
                
            }
            //End of 1690
            //End of 1697
            cs.EDS_Contact_Email__c = eDServCon.Id;
           // cs.Inquiry_Type__c = caseRecord.Inquiry_Type__c;
            cs.Type=caseRecord.Type;
            //1358 adding Request_Type__c
            cs.Request_Type__c= caseRecord.Request_Type__c;
            //Added as part of 1697
            cs.badge__c = caseRecord.badge__c;
            //End of 1697
            if(caseRecordId==null){
                //cs.Webform_Language__c = mapLang.get(selectedLanguage); 
                insert cs;    
            }else{
                cs.id=caseRecordId;   
                update cs;
            }
            caseSaved=true;
        }
        system.debug('------cs---'+cs);
       
    }
    /*Method to return case number on success page*/    
    public String getApplicationID(){
        
        Case newCaseRecord =[select id,CaseNumber from Case where id = :cs.Id];
        return newCaseRecord.CaseNumber;
    }
    
    public void createInstance(){        
        system.debug('first attmt on load--->'+newAttachments);
        newAttachments=new List<Attachment>{new Attachment()};
            newAttachments.add(new Attachment());
        newAttachments.add(new Attachment());
        newAttachments.add(new Attachment());
        newAttachments.add(new Attachment());
    }

    //Added as part of 1294
     public void renderFields(){
         fldString.clear();
         fldMappingMap.clear();
         recSubTypeMapping = '';
         String recordTyp = '';
            
         
         String caseTypeFld = String.valueof(caseRecord.Type);
            
           if (caseRecord.Type =='--None--' || caseRecord.Type =='' ){
             System.debug('caseObj.Type------>'+caseRecord.Type);
                return;
            }
             // recordTypeValue = ApexPages.currentPage().getParameters().get('recordType');
            recSubTypeMapping = recordType.trim()+'+'+caseTypeFld;
            recordTyp = recordType.trim();

            System.debug('recSubTypeMapping------>'+recSubTypeMapping);
           
              List<SC_RecordType_Mapping__c> lstRecord = [Select FieldName__c,Section_Name__c,isMandatory__c,isRendered__c,SortOrder__c,RecordType_Subtype__c from SC_RecordType_Mapping__c where ((RecordType_Subtype__c=:recSubTypeMapping or RecordType_Subtype__c=:recordTyp) and isPSC_field__c = false) order by SortOrder__c ];

               for(SC_RecordType_Mapping__c scRecObj: lstRecord){
        
                    fldString.add(scRecObj.FieldName__c);
                    fldMappingMap.put(scRecObj.FieldName__c,scRecObj);
               }
                
                //Displaying Submit Button
                showSubmit = true;
        
       
    } 
}