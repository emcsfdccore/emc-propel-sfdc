/*===========================================================================+
|  Created History                                                                  

|  DATE       DEVELOPER  WORK REQUEST    DESCRIPTION                               
|  ====       =========  ============    =========== 
|  5/09/2012  Leonard V                  Test class for Presales Partner Case
|  
|  08/10/2012  Leonard V                 Updated TC to incorporate GBS reelase changes
| 
|  10/13/2012  John T                    Added TC for Detailed Product Trigger
|  18April2013 Ganesh Soma WR#252927     Increased Code Coverage
|  06/24/2013  Vivek Barange WR#150      To increase Test Coverage
|  13-Oct-2014    Bisna V P    CI:1399    Remove code referencing unused fields
|  16/10/2014     Bindu        1475       Created checkingUpdateTrigger Method
|  06-Nov-2014    Vinod       CI:1387     Created checkInsertTrigger Method
+===========================================================================*/

@isTest (SeeAllData=true)

private class PreSales_TC1 {
    public Static List<Case> caseLst = new List<Case>();
    public Static List<Contact> contLst = new List<Contact>();
    public Static List<Account> acctLst = new List<Account>();
    public Static List<Opportunity> oppLst = [Select id, Opportunity_Number__c from Opportunity limit 2];
    public Static List<User> lstUser = [Select Id,email,Country__C,UserRoleId  from user where Profile_Name__c like 'Presales%' and UserRoleId!=null and isActive = true Limit 2];
    public Static List<Presales_Account__c> preAcctLst = new List<Presales_Account__c>(); 
    
    //Method for Creation Of Account    
    public void createAccount(){
        
        System.debug('Inside Account Creation');    
        for (Integer a=0;a<=1;a++ )
        {
            Account acctObj = new Account();
            acctObj.Name = 'PreSalese Partner Testing';
            acctObj.Synergy_Account_Number__c = '123456';            
            acctObj.BillingCountry = 'India';            
            acctLst.add(acctObj);
        }
        
        Database.insert(acctLst);
        System.debug('Account Created');         
        
        acctLst = [Select ID,Synergy_Account_Number__c,IsPartner,Theater1__c,BillingCountry  from Account where Name =:acctLst[0].Name];
        system.debug('acctlst------->'+acctLst);        
    }
    
    // Method For Contact Creation
    
    public void createContact(){        
        List<RecordType> recList = [ Select Id, Name from RecordType where sObjectType ='Contact' AND Name ='Partner Contact'];      
         for(Integer c=0;c<4;c++){
            Contact contObj = new Contact();
            contObj.LastName = 'Tester';
            contObj.Phone = '999999999';
            contObj.Email = 'test'+c+'@acc.com';
            contObj.RecordTypeId = recList[0].id;
            contObj.AccountId = acctLst[0].id;
            contLst.add(contObj);
        }        
        Database.insert(contLst);
        System.debug('Contact Created');         
        contLst = [Select ID,Email,Phone from Contact where id in:contLst];
        System.debug('caseLst----> ' + contLst);        
    }
    
    //Method for Creation Of Case
    
    public void createCase(){
        List<Case> lstCase = new List<Case>(); 
        Map<String , Id> recid = new Map<String , Id>();        
        List<RecordType> recList = [Select id,name from recordtype where name in ('Professional Services Inquiry' , 'vLab Demo') ];
        for(Integer i=1 ; i<=6; i++){
            Case caseObj_Insert = new Case();                  
            if(i<=5){
                caseObj_Insert.Description = 'TEST1';
                caseObj_Insert.Subject = 'TEST1';
                caseObj_Insert.Origin = 'Email';                
                caseObj_Insert.Type = 'Application RFP Assistance';
                caseObj_Insert.Record_Type_Hidden__c = 'Application RFP Assistance';
                caseObj_Insert.RecordTypeId = recList[0].id; 
                caseObj_Insert.ContactId = contLst[0].id;
                caseObj_Insert.contact_Email1__c   = contLst[0].email;
            }
            else if(i==6){
                caseObj_Insert.Description = 'VLAB Test';
                caseObj_Insert.Subject = 'VLAB Test';
                //1399 commenting this assignment
                //caseObj_Insert.vLab_Contact_Email__c = contLst[0].email;
                caseObj_Insert.RecordTypeId = recList[1].id;
            }
          lstCase.add(caseObj_Insert);
        }
        //Case Insertion
        Database.insert(lstCase);
        caseLst =[select ID,LastModifiedDate,recordtypeid,status,CreatedDate,Priority,Theatre__C,Country__c,Case_Resolution_Time__c,Partner_Case__c,Partner_Grouping_Name__c,Partner_Country__c,Partner_Theater__c,contact_Email1__c    from Case where id in :lstCase];
        System.debug('caseLst----> ' + caseLst);
    }
    
    
    static testMethod  void partnerCase(){
        PreSales_TC1 testObj = new PreSales_TC1();        
        List<Id> partnerCase = new List<Id>();
        Presales_PartnerCaseOperation partCasOp = new Presales_PartnerCaseOperation();
        List<Account_Groupings__c> grpLst = new List<Account_Groupings__c>();
        
        testObj.createAccount();        
        for(Integer acc=0 ; acc<acctLst.size() ; acc++){            
            acctLst[acc].IsPartner = true;            
        }
        update acctLst;
        
        for (Integer grp = 0; grp <=1 ; grp ++ )
        {            
            Account_Groupings__c grpObj = new Account_Groupings__c();
            if(grp==0)
                grpObj.Profiled_Account__c = acctLst[0].id;
            else
                grpObj.Profiled_Account__c = acctLst[1].id;
            grpLst.add(grpObj);
        }
        
        insert grpLst;        
        acctLst[0].Grouping__c =grpLst[0].id ;
        update acctLst[0];        
        system.debug('acctLst-****-->'+acctLst);
        testObj.createContact();        
        test.startTest();
        system.debug('grpLst****>' +grpLst);             
        testObj.createCase();        
        caseLst[0].Partner_Grouping_Name__c = grpLst[1].id;
        caseLst[0].contact_Email1__c = contLst[1].email;        
        Presales_PartnerCaseOperation.partnerFlag = false;         
        update caseLst[0];
        test.stopTest();
        
        
    }
    static testMethod  void quoteChange(){        
        PreSales_TC1 testObj = new PreSales_TC1();
        testObj.createAccount();
        testObj.createContact();        
        test.StartTest();
        testObj.createCase();   
        Quote_Custom__c quoteObj = new Quote_Custom__c();
        quoteObj.Number_of_Configurations__c = 12;
        quoteObj.Name = '1234567890';
        quoteObj.Quote_Type__c = 'New';
        quoteObj.Case_Name__c = caseLst[0].id;
        insert quoteObj;
        test.StopTest();
        
    }
    
    static testMethod  void detailedProductTrigger(){
        
        PreSales_TC1 testObj = new PreSales_TC1();
        testObj.createAccount();
        testObj.createContact();        
        test.StartTest();
        testObj.createCase();
        caseLst =[select ID,LastModifiedDate,status,Record_Type_Hidden__c,contact_Email1__c  from Case where id in :caseLst];
        Detailed_Product_Presales__c DP_Insert = new Detailed_Product_Presales__c();
        DP_Insert.Product_Type__c = 'VNX';
        DP_Insert.Product_Bucket__c = 'UNIFIED';        
        DP_Insert.CurrencyIsoCode = 'JPY';
        DP_Insert.Case__c = caseLst[0].Id;
        insert(DP_Insert);
        delete(DP_Insert);        
        test.StopTest();      
    }
    
    
    static testMethod void caseSLA(){        
        try{             
            //Invoking contact creation and Case creation Methods for creating test Data
            PreSales_TC1 testObj = new PreSales_TC1();
            testObj.createAccount();
            testObj.createContact();     
            lstUser[0].Exception__c = true;
            lstUser[0].email = contLst[0].Email;
            lstUser[0].Country__c = 'United States';
            lstUser[0].Theater__c = 'Global';
            Database.update(lstUser);
            
            //Commented by Ganesh on 23jan2013
            testObj.createCase();                
            
            //Creating SLA object and insering a record in to it           
            
            Test.starttest();                
            Presales_SLA__c slaObj = new Presales_SLA__c();
            List<Presales_SLA__c> lstSLA = new List<Presales_SLA__c>();
            slaObj.Case_Record_Type__c = 'Application RFP Assistance';
            slaObj.Theater__c = 'Global';
            slaObj.Priority__c = 'Normal';
            slaObj.Time_to_resolution__c =20;
            slaObj.Country__c = 'United States';
            lstSLA.add(slaObj);
            Database.insert(lstSLA);
            //To Check SLA Before Delete 
            Presales_SLA_Class sla_Obj = new Presales_SLA_Class();
            sla_Obj.chkSLABeforeDelete(lstSLA);
            sla_Obj.populateChildData(caseLst,caseLst);    //sla_Obj.populateChildData(lst_case,lst_case);  //Commneted by Ganesh on 23jan2013
            lstSLA[0].Priority__c = 'Medium';
            lstSLA[0].Theater__c = 'APJ';
            update lstSLA;
            sla_Obj.presalesStdCaseTime(caseLst, 'Insert'); //sla_Obj.presalesStdCaseTime(lst_case, 'Insert'); //Commneted by Ganesh on 23jan2013
            sla_Obj.chkUniqueSLAValidation(lstSLA , 'Insert');                    
            delete lstSLA;
            
            Test.stoptest();
            
        } catch (System.DmlException e){
            System.debug('DML exception: ' + e.getDmlMessage(0));   
        }
    }
    
    static testMethod void RelatedOpportunityTriggerCoverage(){
        
        try{ 
            
            //Invoking contact creation and Case creation Methods for creating test Data
            PreSales_TC1 testObj = new PreSales_TC1();
            testObj.createAccount();
            testObj.createContact();     
            testObj.createCase();                
            Test.starttest();                
            Related_Opportunity__c obj = new Related_Opportunity__c();
            obj.Case__c =  caseLst[0].Id;
            insert obj;
            Test.stoptest();
            
        } catch (System.DmlException e){
            System.debug('DML exception: ' + e.getDmlMessage(0));   
        }
        
    }
    
    static testMethod  void insertPresalesAccountTest(){
        PreSales_TC1 testObj = new PreSales_TC1();
        test.startTest();
        testObj.createAccount();
        Presales_Account_Operation presalesAccountOperation = new Presales_Account_Operation();
        presalesAccountOperation.insertPresalesAccount(acctLst);
        test.stopTest();
    }
    static testMethod  void updatePresalesAccountTest(){
        PreSales_TC1 testObj = new PreSales_TC1();
        test.startTest();
        testObj.createAccount();
        Presales_Account_Operation presalesAccountOperation = new Presales_Account_Operation();
        presalesAccountOperation.updatePresalesAccount(acctLst);
        test.stopTest();
    }
    
    //Method for Creation Of Case
    public Case createCase(Id recId){
        
        Case caseObj = new Case();
        caseObj.Description = 'TEST1';
        caseObj.Subject = 'TEST1';
        caseObj.Origin = 'Email';
        caseObj.Type = 'Proof of Concept';
        caseObj.RecordTypeId = recId;
        caseObj.Description = 'VLAB Test';
        caseObj.Subject = 'VLAB Test';
        return caseObj;
    }
    
    /* @150 - To cover checkingChildcaserecordtype() of Presales_Account_Split class */ 
    static testMethod void checkingChildcaserecordtypeTest() {
        
        test.startTest();
        
        PreSales_TC1 testObj = new PreSales_TC1();
        
        Account acc1 = testclassUtils.getAccount();
        acc1.status__c = 'A';
        insert acc1;
        
        Account acc2 = testclassUtils.getAccount();
        insert acc2;
        
        //get standard pricebook
        Pricebook2  standardPb = testclassUtils.getPricebook2();
        
        Product2 prd1 = testclassUtils.getProduct2('test','USD', 'test');
        insert prd1;
        
        PricebookEntry pbe1 = testclassUtils.getPricebookEntry(prd1.id ,standardPb.id, 12, true);
        insert pbe1;
        
        Opportunity opp1 = testclassUtils.getOpportunity('Opp1','Upside',Date.today(), standardPb.id, acc1.id);
        opp1.Opportunity_Number__c = '11223344';
        insert opp1;
        
        OpportunityLineItem lineItem1 = testclassUtils.getOpportunityLineItem(opp1.id, pbe1.id, 4, 200);    
        insert lineItem1;
        
        List<RecordType> recList = [Select id, name, DeveloperName from recordtype where name = 'Proof of Concept'];
        List<Case> cases = new List<Case>();
        
        
        Case pCase = [Select id from Case where RecordTypeId = :recList[0].id limit 1];
        
        Case cCase = testObj.createCase(recList[0].id);
        cCase.Parent = pCase;
        cCase.ParentId = pCase.Id;
        
        cCase.Opportunity_Name__c = Opp1.Id;
        cases.add(cCase);
        
        insert cases;
        
       /* Presales_Account_Split pas = new Presales_Account_Split();
        pas.checkingChildcaserecordtype(Cases);*/
        test.stopTest();
    }
    
    // Created for 1387
    Static testmethod void checkInsertTrigger(){
    List<Case> lstCase = new List<Case>(); 
        Map<String , Id> recid = new Map<String , Id>();        
        List<RecordType> recList = [Select id,name from recordtype where name in ('Proof of Concept Registration') ];
        Case caseObj_Insert = new Case();  
        for(Integer i=1 ; i<=2; i++){
                caseObj_Insert.Description = 'POC Registration Test';
                caseObj_Insert.Subject = 'POC Registration Test';
                caseObj_Insert.Status = 'open';
                caseObj_Insert.Origin = 'system';
                caseObj_Insert.Proof_Point__c = 'POC Test';
                caseObj_Insert.Approving_DVP_Country_Manager__c = 'POC Test';
                caseObj_Insert.RecordTypeId = recList[0].id;
               }
           lstCase.add(caseObj_Insert);
        //Case Insertion
        Database.insert(lstCase);
    } 
    
    // Created as part of 1475 to check trigger update
    static testMethod void checkingUpdateTrigger() 
    {
    List<Case> lstCase = new List<Case>(); 
        Map<String , Id> recid = new Map<String , Id>();        
        List<RecordType> recList = [Select id,name from recordtype where name in ('Global Revenue Operations') ];
        Case caseObj_Insert = new Case();  
        for(Integer i=1 ; i<=2; i++){
                            
  
                caseObj_Insert.Description = 'TEST1';
                caseObj_Insert.Subject = 'TEST1';
                caseObj_Insert.Origin = 'Email';                
                caseObj_Insert.Type = 'Partner Order Mgmt Order Submission';
                caseObj_Insert.Record_Type_Hidden__c = 'Partner Order Mgmt Order Submission';
                caseObj_Insert.RecordTypeId = recList[0].id; 
               // caseObj_Insert.ContactId = contLst[0].id;
              //  caseObj_Insert.contact_Email1__c   = contLst[0].email;
                caseObj_Insert.Ship_to_Country__c ='Albania';
            }
           lstCase.add(caseObj_Insert);
        
        //Case Insertion
        Database.insert(lstCase);
        caseLst =[select ID,LastModifiedDate,recordtypeid,status,CreatedDate,Priority,Theatre__C,Country__c,Case_Resolution_Time__c,Partner_Case__c,Partner_Grouping_Name__c,Partner_Country__c,Partner_Theater__c,contact_Email1__c    from Case where id in :lstCase];
        System.debug('caseLst----> ' + caseLst);
        
        for(Case cs: lstCase)
            {
        cs.Origin = 'Phone';  
        cs.Description ='My test';
            }
             test.startTest();
             Try{
        update lstCase;
        }
        catch( Exception E)
        {
        System.debug(E);
        }
         test.stopTest();
        }
    
    }