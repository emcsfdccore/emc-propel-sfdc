/*

Modified By     :   Avinash Kaltari
Modified Date   :   30 May 2012
Purpose         :   To increase the coverage for the 'AfterInsertUpdateTA_AssignmentRule' trigger.
Backward Arrorw :   Optimized class and improved code coverage. Jaypal Nimesh - Dated: 24/12/2013

Modified By     :   Naga
Modified Date   :   18 Feb 2014
Purpose         :   Visibility Program Country and State classification.
Visibility      :   Removed EMC Classification filed as part of Country and State Classification.
*/

@isTest
Private class TO_AccountTARuleAssignment_TC {

    Private static testMethod void myUnitTest() { 
    
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
        
        //Inserting custom setting data in different context
        System.runAs(insertUser)
        {           
            CustomSettingDataHelper.adminConversionCSData();
            CustomSettingDataHelper.houseAccountCSData();
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
        }
        
        //Creating group
        Group testGroup;
           
        System.runAs(insertUser)
        {           
           testGroup = new Group(Name = 'TestGrp');
           insert testGroup;
        }
        
        //Creating TA Assignment Rule records
        List<TA_Assignment_Rule__c> taRuleList = new List<TA_Assignment_Rule__c>();
        
        TA_Assignment_Rule__c tr = new TA_Assignment_Rule__c(name='rule', Opportunity_Private_Access_Group__c = testGroup.Id, Country__c='United States',State_Or_Province__c='California',Group_Id__c=testGroup.Id);
        taRuleList.add(tr);
        TA_Assignment_Rule__c tr2 = new TA_Assignment_Rule__c(name='rule1', Opportunity_Private_Access_Group__c = testGroup.Id, Country__c='United States',State_Or_Province__c='California',Group_Id__c=testGroup.Id);
        taRuleList.add(tr2);
        TA_Assignment_Rule__c tr3 = new TA_Assignment_Rule__c(name='rule', Opportunity_Private_Access_Group__c = testGroup.Id, Country__c='USA',State_Or_Province__c='MA',Group_Id__c=testGroup.Id);
        taRuleList.add(tr3);
        TA_Assignment_Rule__c tr4 = new TA_Assignment_Rule__c(name='rule1', Opportunity_Private_Access_Group__c = testGroup.Id, Country__c='India',State_Or_Province__c='',Group_Id__c=testGroup.Id);
        taRuleList.add(tr4); 
       
        insert taRuleList;  
        
        //Creating accounts with specific data
        List<Account> accList = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        
        for(Integer i = 0 ; i < accList.size() ; i++){
            if(i == 0){
                accList[i].TA_Assignment_Type__c = 'Create';
                accList[i].BillingCountry = 'United States';
                accList[i].BillingState = 'California';
            }
            else if(i == 1){
                accList[i].TA_Assignment_Type__c = 'Create';
                accList[i].BillingCountry = 'Canada';
                accList[i].BillingState = null;
            }
            else{
                accList[i].TA_Assignment_Type__c = 'Update';
                accList[i].BillingCountry = 'India';
                accList[i].BillingState = null;
            }
        }
        
        insert accList;
        
        //This List is used here to insert Account Share records.
        List<AccountShare> accShareList=new List<AccountShare>();
        
        for(Integer i=0; i<accList.size();i++)
        {
            accShareList.add(new AccountShare(UserOrGroupId=testGroup.Id,accountId=accList[i].Id,AccountAccessLevel='Read', OpportunityAccessLevel='Read',CaseAccessLevel='Read'));
        }
        
        insert accShareList;
        
        Test.StartTest();
        
        //Creating instance of class and executing batch
        TO_AccountTARuleAssignment reassign = new TO_AccountTARuleAssignment();
        
        reassign.query='Select id,BillingCountry,TA_Assignment_Rule__c, TA_Assignment_Type__c, BillingState,(Select id,UserOrGroupId from Shares where RowCause=\'Manual\')  from Account order by CreatedDate DESC LIMIT 5';
        //Added LIMIT by Jaypal Nimesh for Backward Arrow Project - 17 Dec 2013
        
        Database.executeBatch(reassign);
        
        reassign.COLLECTION_LIMIT = 3;
        
        for(Integer i = 0 ; i < accList.size() ; i++){
            accList[i].TA_Assignment_Type__c='Update';
            accList[i].TA_Assignment_Rule__c= taRuleList[i].Id;
        }
        update accList;
        
        //Batch initiated
        ID batchprocessId = Database.executeBatch(reassign);
        
        Test.stopTest(); 
    }
    
    Private static testMethod void myUnitTestSR(){
        
        Test.startTest();
        
        //Creating and Deleting accounts to cover SaveResult code
        Account acc = new Account(name = 'TestClassCoverage');
        Database.SaveResult[] lsr = Database.insert(new Account[]{acc, new Account(Name = 'Acme')},false);
        
        TO_AccountTARuleAssignment taReassign = new TO_AccountTARuleAssignment();
        taReassign.createorUpdateRecords(lsr);
        
        Account[] accToDel = [SELECT Id, Name FROM Account WHERE Name = 'TestClassCoverage']; 
        Database.DeleteResult[] deleteResult= Database.delete(accToDel, false);
        taReassign.delShares(deleteResult);
        
        Test.stopTest();
    }
}