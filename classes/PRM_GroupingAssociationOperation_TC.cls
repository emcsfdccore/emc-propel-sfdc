/*=====================================================================================================+

|  HISTORY  |                                                                           

|  DATE          DEVELOPER                WR            DESCRIPTION                               

|  ====          =========                ==            =========== 
| 05/07/2010     Suman B                                Updated for Test Clsss for 
                                                        PRM_GroupingAssociationScheduler.
                                                          
| 01-12-2011     Anil                                   Fixed MIXED_DML Exception.
| 11-JAN-2012    Anil                                   Removed role Id
| 11-11-2013    Jaypal Nimesh       Backward Arrow      Changes made to optimize the test class
| 18-Feb-2015   Vinod Jetti             #1649           method called 'testclassutils.getHubInfo()'
+=====================================================================================================*/

@isTest
private class PRM_GroupingAssociationOperation_TC{

    static testMethod void myUnitTest(){
    
    System.runAs(new User(Id = UserInfo.getUserId())){
        
        CustomSettingDataHelper.dataValueMapCSData();
        CustomSettingDataHelper.eBizSFDCIntCSData();
        CustomSettingDataHelper.dealRegistrationCSData();
        CustomSettingDataHelper.bypassLogicCSData();
        CustomSettingDataHelper.countryTheaterMapCSData();
   }
    Profile p = [select id from profile where name='System Administrator' limit 1];
    
    User sysAdminUser = new User(alias = 'utest', email='testSample1@test.com',
                      emailencodingkey='UTF-8', lastname='Unit Test', 
                      languagelocalekey='en_US',
                      localesidkey='en_GB', profileid = p.Id,
                      timezonesidkey='Europe/London', 
                      username='sysAdminTest@emc.com');
                      
    insert sysAdminUser;
    
    User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
    
    /*System.runAs(insertUser)
    {
        PRM_VPP_JobDataHelper.createVPPCustomSettingData();     
    }*/
    
    Contact cont = UserProfileTestClassDataHelper.createContact();
    List<Account> accList = new List<Account>();
    
    System.runAs(sysAdminUser){
        //#1649 - Start
        Hub_Info__c objHubInfo1 = testclassutils.getHubInfo();
        objHubInfo1.Golden_Site_Identifier__c = 674418;
        insert objHubInfo1;
        //#1649 - End
        List<Account> acc = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        insert acc;
        
         Account objAccount = new Account();
         objAccount.name = 'TestPartnerAcc';
         objAccount.Party_Number__c = '1234';            
         objAccount.BillingCountry ='Colombia';
         objAccount.Synergy_Account_Number__c = '3139';
         objAccount.Type = 'Partner';
         objAccount.Lead_Oppty_Enabled__c = true;
         objAccount.Partner_Type__c ='Distributor';
         objAccount.Hub_Info__c = objHubInfo1.Id;
         //objAccount.Site_DUNS_Entity__c='001017771';
         objAccount.PROFILED_ACCOUNT_FLAG__c = true;
         objAccount.PROFILED_ACCOUNT_FLAG__c =true;
     objAccount.Cluster__c='LA2';
     //objAccount.Site_DUNS_Entity__c = '234561234';
     objAccount.Grouping_Batch_Operation__c =  'Association&' + acc[0].Id;
     insert objAccount;
     
     objAccount.IsPartner=true;
     update objAccount;
     
     accList.add(objAccount);
            
        cont.Email='test23@emc.com';       
        insert cont;       
        cont.AccountId=accList[0].id;
               
        update cont; 
    }
        
        User partner;
        System.runAs(insertUser)
        {       
        Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();       
        String distributorSuperUser = data.get('EMEA Distributor Partner User').DataValue__c ;
        Profile amerUserProf = [select Id from Profile where Name=: distributorSuperUser];       
        partner = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.id,null,cont.Id);
        //insert partner;
        }
       
        /*List<Account> acclist = [Select id,name,Child_Partner_Users_Count__c,Site_DUNS_Entity__c
                                    FROM Account  where Site_DUNS_Entity__c != Null 
                                    and Child_Partner_Users_Count__c >0 and id=:partner.contact.account.id limit 1] ;*/
       
        
        if(accList!= Null && accList.size()>1){
         
        Map<String,Account> mapAccount = new Map<String,Account>();
        mapAccount.put(accList[0].Site_DUNS_Entity_Hub__c,accList[0]);
       System.runAs(sysAdminUser){ 
        APPR_MTV__RecordAssociation__c association = new APPR_MTV__RecordAssociation__c();
        association.APPR_MTV__Associated_Account__c = accList[0].Id;
        association.APPR_MTV__Account__c = accList[0].Id;
        insert association;
        Test.startTest();
        PRM_GroupingAssociationOperation obj1 =  new PRM_GroupingAssociationOperation();
        obj1.fetchingOfBulkAccounts();
        obj1.creatingAssociationOnBulk(mapAccount);
        Test.stopTest();
      } 
    }
  }
}