/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  
 | 3 March 2010     Prasad Kothawade 125264     Updated test method to change synergy lead id.
 | 27.01.2011       Shipra Misra    151892      Updated test class as two new required fields which have been introduced to the system. "Primary ISV & Infrastructure" & "Primary Outsourcer & Integrator".
   25-May-2011      Srinivas Nallapati          Chnaged for June11 release
   23-Jul-2011      Anand Sharma                Updated email field value of user record creation
   03/30/2014       validation Errorfix for March'14 release 
 | 17-Feb-2015       Vinod Jetti   #1649   Created Hub Account Data object data for testing  
 +===========================================================================*/


@isTest

Private class DeployTrigger{
    
   public DeployTrigger(){
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' AND Name Like 'EAG%' limit 1];
        System.runAs(insertUser)
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();   
        }
   }
   
   public static testmethod void deployLead_CalculateQAge(){
        DeployTrigger dt = new DeployTrigger();
        String owner;
        Lead mylead= new Lead();
        QueueSobject myGrpMem = [Select q.SobjectType, q.QueueId From QueueSobject q where SobjectType = 'Lead' limit 1];
        mylead.Synergy_Lead_ID__c='123456789011111';
        mylead.Integration_Error_Message__c='';
        mylead.Status = 'New' ;
        mylead.Company = 'DeployTest' ;
        mylead.Channel__c = 'INDIRECT' ;
        mylead.City = 'DIRECT' ;
        mylead.Street = 'DIRECT' ;
        mylead.Country = 'DIRECT' ;
        mylead.Sales_Force__c = 'EMC' ;
        mylead.LastName = 'EMC' ;
        mylead.ownerId=myGrpMem.QueueId;
        insert mylead;
        mylead.Q_Ownership_End__c=System.today().addDays(-1);
        mylead.Q_Ownership_Start__c=System.today().addDays(1);
        update mylead;
        mylead.Running_Q_Ownership_Duration__c=null;
        update mylead;
    }
    
    private static UserRole createRole(String roleName){
        UserRole NewusrRole1=new UserRole(Name=roleName+Math.Random());
        insert NewusrRole1; 
        return NewusrRole1;
    } 
 
  
    private static testmethod void deploySetActivityOwner(){
        DeployTrigger dt = new DeployTrigger();
        Lead mylead= new Lead();
        User adUser = [Select id from User where isActive=true and profile.Name='System Administrator' AND Name Like 'EAG%' limit 1];
        User myUserFrom;
        User myUserTo;
        system.runAs(adUser){
        UserRole usrRole=createRole('test111111');
        Profile amerUserProf = [select Id from Profile where Name=:'AMER Commercial User'];
        myUserTo= UserProfileTestClassDataHelper.createUser(amerUserProf.id,usrRole.id);
        insert myUserTo;
        UserRole usrRole2=createRole('test44444');
        Profile amerUserProf1 = [select Id from Profile where Name=:'AMER Inside Sales/SMB User'];
        myUserFrom= UserProfileTestClassDataHelper.createUser(amerUserProf1.id,usrRole2.id);
        insert myUserFrom;  
        }
        
        mylead.Synergy_Lead_ID__c='12345678905555';
        mylead.Integration_Error_Message__c='';
        mylead.Status = 'New' ;
        mylead.Company = 'DeployTest' ;
        mylead.Channel__c = 'DIRECT' ;
        mylead.City = 'DIRECT' ;
        mylead.Street = 'DIRECT' ;
        mylead.Country = 'DIRECT';
        mylead.Sales_Force__c = 'EMC';
        mylead.LastName = 'EMC';
        mylead.ownerId=myUserFrom.Id;
        mylead.Accept_Lead__c=false;
        mylead.Related_Account__c=(new Account(name='test')).id; 
        insert mylead;
        
        Event myEvent= new Event();
        myEvent.whoid=mylead.Id;
        myEvent.EndDateTime=System.now().addDays(2);
        myEvent.StartDateTime=System.now().addHours(2);
        myEvent.Type='Appointment Set (SA)';
        myEvent.Subject='Call';
        insert myEvent;
       
        mylead.ownerId=myUserTo.Id;
        mylead.Accept_Lead__c=true;
        update mylead;
       
  }  
 
  public static testmethod void deployUpdateLeadPorfiles(){
        DeployTrigger dt = new DeployTrigger();
        Lead mylead= new Lead();
        User admUser = [Select id from User where isActive=true and profile.Name='System Administrator' AND Name Like 'EAG%' limit 1];
        UserRole usrRole;
        Profile emeaUserProf = [select Id from Profile where Name=:'EMEA Commercial User'];
        User emeauser;
        system.runAs(admUser){
            usrRole=createRole('test22222');
            emeauser= UserProfileTestClassDataHelper.createUser(emeaUserProf.id,usrRole.id);
            insert emeauser;    
        }
        
        mylead.Synergy_Lead_ID__c='1234567893333';
        mylead.Integration_Error_Message__c='';
        mylead.Status = 'New' ;
        mylead.Company = 'DeployTest' ;
        mylead.Channel__c = 'DIRECT' ;
        mylead.City = 'DIRECT' ;
        mylead.Street = 'DIRECT' ;
        mylead.Country = 'DIRECT';
        mylead.Sales_Force__c = 'EMC';
        mylead.LastName = 'EMC';
        mylead.ownerId=emeauser.Id;
        mylead.Accept_Lead__c=false;
        insert mylead;
        Account acc = new Account(Name = 'TestAcc', Synergy_Account_Number__c = '123456', Party_ID__c='6525556');       
        insert acc;        
        acc =[select id,Party_ID__c  from account where Party_ID__c!=''  limit 1];
        mylead.related_account__c= acc.id;        
        mylead.Party_ID__c = acc.Party_ID__c; 
        update mylead;
        
}

    public static testmethod void deployCheckUserRecordBeforeDelete(){
        User adminUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
        UserRole newusrrole;
        system.runAs(adminUser){
            newusrrole=createRole('test99999'); 
        }
        User_Attribute_Mapping__c new_attribute=new User_Attribute_Mapping__c(
                                                           Name=newusrrole.Name,
                                                           BU__c='Captiva',
                                                           Area__c='Adriatics',
                                                           Country__c='India',
                                                           Division__c='ANZ',
                                                           Super_Division__c='Mid Market',
                                                           Region__c='AMER',
                                                           Sales_Role__c='Admin',
                                                           Super_Area__c='Australia',
                                                           District__c='Accenture',
                                                           Theater__c='APJ');
        
            insert new_attribute;   

                User_Attribute_Mapping__c newAttr=[Select Name,BU__c,
                                                        Area__c,
                                                        Country__c,
                                                        Division__c,
                                                        Region__c,
                                                        Sales_Role__c,
                                                        Super_Area__c,
                                                        District__c,
                                                        Theater__c from User_Attribute_Mapping__c where Name=:new_attribute.Name];
            Delete newAttr; 
        
        }
public static testmethod void deployCreateIndividualUserGroup(){
        User aUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
        UserRole usrRole;
        Profile emeaUserProf = [select Id from Profile where Name=:'EMEA Commercial User'];
        User emeauser;
        system.runAs(aUser){
            usrRole=createRole('test33333');
            emeauser= UserProfileTestClassDataHelper.createUser(emeaUserProf.id,usrRole.id);
            insert emeauser;    
        }
        }     
        
public static testmethod void deployOpportunityLineItemUpdateLog(){
        
        DeployTrigger dt = new DeployTrigger();
        List<Account> lstaccount = new List<Account>();
        
        //#1649 - Start    
        List<Hub_Info__c> lstHubInfo = new List<Hub_Info__c>();
        Hub_Info__c objHubInfo =new Hub_Info__c();
        objHubInfo.Site_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo.Parent_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo.Global_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo.Golden_Site_Identifier__c = 30226;
        lstHubInfo.add(objHubInfo);
        
        Hub_Info__c objHubInfo1 =new Hub_Info__c();
        objHubInfo1.Site_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo1.Parent_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo1.Global_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        objHubInfo1.Golden_Site_Identifier__c = 30426;
        lstHubInfo.add(objHubInfo1);
        insert lstHubInfo;
        //#1649 - End
        Account account1 = new Account();
        account1.name = 'Test';
        account1.Hub_Info__c = lstHubInfo[0].Id;
        //account1.Site_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        //account1.Parent_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        //account1.Global_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        account1.Partner_Type__c='ISV';
        lstaccount.add(account1);
        
        Account account2 = new Account();
        account2.name = 'Test2';
        account2.Hub_Info__c = lstHubInfo[1].Id; 
        //account2.Site_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        //account2.Parent_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        //account2.Global_DUNS_Entity__c = String.valueOf(EMC_UTILITY.generateRandomInt(8));
        account2.Partner_Type__c='Outsourcer';
        lstaccount.add(account2);
        
        insert lstaccount;
        Opportunity Oppty = new Opportunity();
        //Date closeDate = date.newinstance(2012, 11, 17);      
       // Date approvalDate = date.newinstance(2014, 11, 1);
        //Date expirationDate = date.newinstance(2015, 11, 18);
        Date closeDate = System.today()+100;
        Date approvalDate = System.today()+50;
        Date expirationDate = System.today()+300;
        Oppty.Name = 'Test Oppty';
        Oppty.Sales_Channel__c = 'Direct';
        Oppty.Sales_Force__c = 'EMC';
        Oppty.StageName = 'Pipeline';
        Oppty.Closed_Reason__c = 'Loss';
        Oppty.Close_Comments__c = 'Lost';
        Oppty.CloseDate = closeDate;
        Oppty.Sell_Relationship__c = 'Direct';
        Oppty.Quote_Version__c='v13';
        Oppty.Quote_Type__c='New';
        Oppty.Approval_Date__c=approvalDate ;
        Oppty.Expiration_Date__c=expirationDate;
        Oppty.AccountId=account1.Id;
        Oppty.Amount=75900;
        Oppty.bypass_validation__c=true;
        // 24/5/2011 Changed for VF rules - Srinivas Nallapati
        //Oppty.Primary_Outsourcer_System_Integrator__c=account2.id;
        //Oppty.Primary_ISV_Infrastructure__c=account1.id;
        //Oppty.Outsourcer_System_Integrator_involved__c = false;
        //Oppty.ISV_Infrastructure_involved__c = false; 
        /**/
        
        insert Oppty;
        CustomSettingDataHelper.adminConversionCSData();           
        CustomSettingDataHelper.houseAccountCSData();
        Product2 prod = new Product2(Name = 'Test Product',CurrencyIsoCode = 'USD',productCode = 'ABC', IsActive = true);      
        insert prod;         
        String standardPriceBookId = '01s70000000EkOZ';            
        PricebookEntry pricebook = new PricebookEntry(Pricebook2Id = standardPriceBookId, Product2Id = prod.Id, UnitPrice = 10000,IsActive = true);            
        insert pricebook; 
        //pricebook=[Select Id from PricebookEntry where IsActive=true and CurrencyIsoCode='USD'limit 1];
        OpportunityLineItem opptyLineItem= new OpportunityLineItem();
        opptyLineItem.OpportunityId=Oppty.Id;
        opptyLineItem.Quantity=5;
        opptyLineItem.Quote_Amount__c=5000;
        opptyLineItem.PricebookEntryId=pricebook.Id;
        insert opptyLineItem;
        //OpportunityLineItem opplnitem=[Select UnitPrice,OpportunityId from OpportunityLineItem where OpportunityId=:Oppty.Id];
        //update opplnitem;

}
}