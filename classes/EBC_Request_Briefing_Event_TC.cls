/*
*  Created By:              Emmanuel Cruz
*  Created Date :         18-JUNY-13
*  Last Modified By :    Emmanuel Cruz
*  Description :             Test Class for EBC_Request_Briefing_Event controller.

   Modified Date :       05-Jun-2014
*  Last Modified By :    Abinaya M S
*  Description :         call EBC_Limited_Profile__c custom settings.

*/

@isTest(SeeAllData = true)
private class EBC_Request_Briefing_Event_TC{

  static EBC_Briefing_Event__c newBE;
  static PageReference pageBE;
  
  private static void init(){
    newBE = new EBC_Briefing_Event__c(Name='test');
    pageBE = Page.EBC_Request_Master_Briefing_E_Layout;
    Test.setCurrentPage(pageBE);
  }

  static testMethod void validate_method_saveAndNew() {
    init();
    ApexPages.StandardController sc = new ApexPages.standardController(newBE);
    EBC_Request_Briefing_Event ctrl = new EBC_Request_Briefing_Event(sc);
    Map<String,EBC_Limited_Profile__c> ebcProfiles = EBC_Limited_Profile__c.getAll();
    System.debug('ebcProfiles --->'+ebcProfiles);
    ctrl.saveAndNew();
    ctrl.save();
  }

  static testMethod void validate_method_save() {
    init();
    ApexPages.StandardController sc = new ApexPages.standardController(newBE);
    EBC_Request_Briefing_Event ctrl = new EBC_Request_Briefing_Event(sc);
    ctrl.save();
    ctrl.saveAndNew();
  }

  static testMethod void validate_method_cancel() {
    init();
    ApexPages.StandardController sc = new ApexPages.standardController(newBE);
    EBC_Request_Briefing_Event ctrl = new EBC_Request_Briefing_Event(sc);
    ctrl.cancel();
  }

}