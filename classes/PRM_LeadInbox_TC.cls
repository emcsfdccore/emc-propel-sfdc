/*=========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  23.09.2010      Anand Sharma    #779        Test class for the unit test of PRM_LeadInbox class.             
 
 |  06.10.2010      Anand Sharma                Better code coverage.
 
 |  08/10/2010      Karthik Shivprakash         Added startTest and stopTest 
                                                method to avoid too many DML
 |                                              Exception error. 
 |  13.10.2010      Ashwini Gowda    #1143      Blank out the EMC Lead Rejected checkbox 
 |                                              after a lead has been accepted by a different user.
 |  14/12/2011      Anil                        Removed Query for fetching Partner and used Custom setting Data Helper
 |  11-JAN-2012     Anil                        Removed role Id
 |  22/Nov/2013     Jaypal Nimesh   Backward    Changes made in test class to optimize processing time
                                    Arrow       
 | 19/Nov/2014     Akash Rastogi   WR#939       Updated to match active account filter criteria                             
 +=========================================================================================================================*/

@isTest
private class PRM_LeadInbox_TC {

    static testMethod void leadInboxUnitTest() {
    
        System.runAs(new user(Id = UserInfo.getUserId())){
        
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
        }  
            
           List<Account> accList = AccountAndProfileTestClassDataHelper.CreatePartnerAccount(); 
           //updated for WR#939
           accList[0].Status__c = 'A';
           insert accList;
           
           List<Account> lstDistributorAccount = new List<Account>();
           
           for(account distiacc: accList){
                if(distiacc.Partner_Type__c=='Distributor'&& distiacc.Lead_Oppty_Enabled__c==true){
                   distiacc.Ispartner=true;
                }
                lstDistributorAccount.add(distiacc);
            }
            Update lstDistributorAccount;
           
           Contact cont = UserProfileTestClassDataHelper.createContact();
           cont.AccountId = accList[0].Id;
           insert cont;
           
           Group testGroup;
           
           System.runAs(new user(Id = UserInfo.getUserId()))
           {           
               testGroup = new Group(Name = 'TestGrp', Type = 'Queue');
               insert testGroup;
               
               QueueSobject testQueue = new QueueSObject(QueueId = testGroup.Id, SobjectType = 'Lead');        
               insert testQueue;
           }
        
        List<Lead> leadlist = LeadTestClassDataHelper.createDealReg(accList[0], null, lstDistributorAccount[0], accList[2]);
        
        insert leadlist;
        
        for(Lead lead : leadlist){
            
            lead.OwnerId = testGroup.Id;
            lead.DealReg_Deal_Registration__c = false;
            lead.Originator_Details__c = 'Banners';
        }
        
        update leadlist;
        
        System.currentPageReference().getParameters().put('selectedRow',leadlist[0].Id);
        
        Test.startTest();
        
        /* Create a new instance of the object */
        PRM_LeadInbox objLeadInbox = new PRM_LeadInbox();
        
        /* call accept method */
        objLeadInbox.accept();
        
        /* call getLeadInbox property */ 
        objLeadInbox.getLeadInbox();
        
        /*get Full Lead List */
        objLeadInbox.FullLead();
        
        /* get only limited number of record */
        objLeadInbox.limitLeadInbox();
        
        objLeadInbox.transferToLead();
        
         //check when the lead is accepted is EMC_Lead_Rejected__c false
        System.assertEquals(false, leadlist[0].EMC_Lead_Rejected__c); 
        
        System.assertEquals(false, leadlist[1].DealReg_Deal_Registration__c); 
        System.assertEquals(testGroup.Id, leadlist[1].OwnerId);
        
        Test.stopTest();
    }
}