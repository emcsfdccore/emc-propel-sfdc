/*======================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER                WR        DESCRIPTION                               
 |  ====          =========                ==        =========== 
 | 27/03/2012    Bhanuprakash              CI       This class used to show all Competitors and product details 
 +=====================================================================================================*/
public class Opp_CompetitorAndPrsingDetails {
    public string dbg {get;set;}
    public List<OpportunityCompetitorProduct__c> objCompProd {set;get;}
    public List<OppCompProdWrap> oCPWrapList {get;set;} 
    public List<OppCompProdWrap> oldOCPWrapList {get;set;} 
    public boolean isItForSave {get;set;}
    public boolean isEditable {get;set;}
    public boolean isEditing {get;set;}
    public ID oppId {set;get;}
    public boolean showAdd {set;get;}
    public boolean isFirstSave {set;get;}
    public boolean isFirstEdit {set;get;}
    public boolean isFirstDelete {set;get;}
    public boolean isOptyClosePast {set;get;}
    public Opportunity currentOppty {set;get;}
    public boolean isAddSession {set;get;}
    public boolean isDisplayNormalMsg {set;get;}
    
    public Map<ID,Competitor__c> existingComps {set;get;}
    public Map<ID,Competitor_Product__c> existingProds {set;get;}
    public List<OpportunityCompetitorProduct__c> editableJunction = new List<OpportunityCompetitorProduct__c> ();
    public List<OpportunityCompetitorProduct__c> deletableJunction = new List<OpportunityCompetitorProduct__c> ();
    //public Id editId;
    
    public Opp_CompetitorAndPrsingDetails(ApexPages.StandardController std) {
        Schema.DescribeFieldResult dfr = OpportunityCompetitorProduct__c.Competitor__c.getDescribe();
        currentOppty = (Opportunity) std.getRecord();
        isEditable = true;//dfr.isUpdateable();
        oppId = std.getID();
        isItForSave = false;
        showAdd = true;
        isEditing = false;
        isFirstSave = false;
        isFirstEdit = false;
        isAddSession = false;
        isFirstDelete = false;
        oldOCPWrapList = oCPWrapList;
        System.debug('***** oldOCPWrapList size = '+ oldOCPWrapList);
        existingComps = new Map<ID,Competitor__c>([Select Id, Name from Competitor__c]);
        existingProds = new Map<ID,Competitor_Product__c>([Select Id, Name from Competitor_Product__c]);
        System.debug('*** existingComps = ' + existingComps);
        objCompProd = new List<OpportunityCompetitorProduct__c>();
        System.debug('**** isEditable = ' + isEditable + ' : ');
        getOppty();
        loadWrapperData();
        dbg = 'First time load';
    }
    public void getOppty(){
        if(currentOppty != null){
        System.debug('*** currentOppty not null');
            currentOppty = [select id,stageName,AccountId,Account_Name1__c,Closed_Reason__c,Competitor__c,Close_Comments__c,Duplicate_Opportunity__c,Competitor_Lost_To__c,Competitor_Product__c, Product_Model__c,
Competitor_Product_ID__c,Sales_Channel__c,Renewals_Close_Details__c,Closed_Reason_Action__c,Opportunity_type__c, CloseDate from Opportunity where id =: currentOppty.Id];
        }
    }
    public void loadWrapperData(){
        oCPWrapList = new List<OppCompProdWrap>();        
        //objCompProd = [SELECT Id, Name, Auto_Create_Competitor_Asset__c, Competitor_Not_Listed__c, Competitor__c,Competitor_Product__c,Competitor__r.Name, Competitor_Product__r.Name,  Competitor_Product__r.CI_Wiki_Link__c, Competitor__r.Pricing_Calculator_Link__c, Competitor_Product__r.Competitive_One_Pager_Link__c,  Competitor_Lost_To__c, Opportunity__r.AccountId, CI_Wiki__c, Competitive_One_Pager__c, Pricing_Calculator__c FROM OpportunityCompetitorProduct__c WHERE Opportunity__c=: oppId and Competitor__r.Active__c=true ORDER BY CreatedDate  ASC ];
        objCompProd = [SELECT Id, Name, Auto_Create_Competitor_Asset__c, Competitor_Not_Listed__c, Competitor__c,Competitor_Product__c,Competitor__r.Name, Competitor_Product__r.Name,  Competitor_Product__r.CI_Wiki_Link__c, Competitor__r.Pricing_Calculator_Link__c, Competitor_Product__r.Competitive_One_Pager_Link__c,  Competitor_Lost_To__c, Opportunity__r.AccountId, CI_Wiki__c, Competitive_One_Pager__c, Pricing_Calculator__c FROM OpportunityCompetitorProduct__c WHERE Opportunity__c=: oppId and Competitor__c != null ORDER BY CreatedDate  ASC ];
        for(OpportunityCompetitorProduct__c o : objCompProd){
                    System.debug('**** o.Competitor__r.Name = ' + o.Competitor__r.Name);
            oCPWrapList.add(new OppCompProdWrap(o,true));
        }        
    }
    public void addOppCompProd(){
        isAddSession = true;
        OpportunityCompetitorProduct__c o = new OpportunityCompetitorProduct__c(Opportunity__c = oppId);
        oCPWrapList.add(new OppCompProdWrap(o,false));
        isItForSave = true;
        System.debug('**** oCPWrapList size = ' + oCPWrapList.size());
    }
    public void deleteOppCompProd(){
        Id delId = ApexPages.currentPage().getParameters().get('delId');
        List<ID> toBeDeleteIDs = new List<ID>();
        List<OppCompProdWrap> oCPWrapList_Temp = new List<OppCompProdWrap>();
        Boolean isFirstRecord = true;
        for(OppCompProdWrap ow : oCPWrapList){
        System.debug('*** Search for delets');
            //isFirstRecord = true;
            if(ow.ocpId == delId && isFirstRecord ){
                System.debug('**** morking deletable ');
                isFirstDelete = true;
                break;
            }else{
                isFirstRecord = false;
                break;
            } 
        }
        
        for(OppCompProdWrap ow : oCPWrapList){
            if(ow.ocpId != delId){
                oCPWrapList_Temp.add(ow);
            }else{
                toBeDeleteIDs.add(delId);
            }
        }
        try{
        if(toBeDeleteIDs.size() > 0){
            Database.DeleteResult[] dr = system.database.delete(toBeDeleteIDs);
            //system.database.DeleteResult[] dr =  system.database.delete(toBeDeleteIDs);
            system.debug('The record has been deleted & the result is::'+dr);
            loadWrapperData();
            //Update by Mung Fan        
            if(isFirstDelete){
                System.debug('**** in isFirstDelete section oCPWrapList size = ' + oCPWrapList.size());
                if(oCPWrapList.size() == 0){
                    System.debug('*** isFirstDelete, if size = 0');
                    updateOppty(null,null,null);     
                }else {
                    System.debug('*** isFirstDelete, if size > 0');
                    updateOppty(oCPWrapList[0].ocp.Competitor__r.Name,oCPWrapList[0].ocp.Competitor_Product__r.Name,oCPWrapList[0].ocp.Competitor_Product__c);   
                }
                isFirstDelete = false;
        	} 
            // end of update by Mung Fan
            
            //database.delete(toBeDeleteIDs);
        }//Update by Mung Fan 
        } catch(Exception ex) {
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
             ApexPages.addMessage(myMsg);
            isDisplayNormalMsg = true;
        }// end of update by Mung Fan
        //oCPWrapList = oCPWrapList_Temp;
        //loadWrapperData();    
          
    }
    public void editOppCompProd(){
        isItForSave = true;
        isEditing = true;
        Boolean isFirstRecord = true;
        Id editId = ApexPages.currentPage().getParameters().get('editId');
        //UPDATE OPPTY IF THE FIRST RECORD IS EDITED
         for(OppCompProdWrap ow : oCPWrapList){
        System.debug('*** Search for edits');
            isFirstRecord = true;
            if(ow.ocpId == editId && isFirstRecord){
            isFirstEdit = true;
            System.debug('*** marking editable');
               editableJunction.add(ow.ocp);
            }
            isFirstRecord = false;
        }
        
        //List<OpportunityCompetitorProduct__c> oCPList_Temp = new List<OpportunityCompetitorProduct__c>(); 
        for(OppCompProdWrap ow : oCPWrapList){
            if(ow.ocpId == editId){
               //oCPList_Temp.add(ow.ocp);
               ow.flag = false;
            }
        }
        //update oCPList_Temp;
        showAdd = false;        
    }
    public void saveOppCompProd(){
        dbg = dbg + 'executing saveOppCompProd method';
        showAdd = true;
        isEditing = false;
        Boolean isError = false;
        isAddSession = false;
        List<OppCompProdWrap> oCPWrapList_Temp = new List<OppCompProdWrap>(); 
        List<OpportunityCompetitorProduct__c> optyCompTemp = new List<OpportunityCompetitorProduct__c>();
        List<Asset__c> optyCompAsset = new List<Asset__c>();
        Asset__c ast;
        String firstCName = null;
        String firstCPName = null;
        String compName;
        Competitor__c tempComp;
        Competitor_Product__c tempProd;
        
        System.debug(':::The proceced records from UI before refine, cPWrapList:::::::::::'+oCPWrapList);
                
        for(OppCompProdWrap ow : oCPWrapList){
            if(!ow.flag){
                tempComp = existingComps.get(ow.ocp.Competitor__c);
                tempProd = existingProds.get(ow.ocp.Competitor_Product__c);
                
                Boolean isComNListed = false;
                if(ow.ocp.Competitor_Not_Listed__c != null && ow.ocp.Competitor_Not_Listed__c.length() > 0)
                    isComNListed = true;
                
                System.debug('*** isComNListed = ' + isComNListed);
                
                if(tempComp == null && isComNListed ){//if Comp. Null and NListed field not empty
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Competitor_Not_Listed_Donot_See_Competitor_List_Error_Msg);
                    ApexPages.addMessage(myMsg);
                    isError = true;
                    isAddSession =true;
                } else if((tempComp != null && tempComp.name == 'Not Listed') && !isComNListed && ow.ocp.Competitor_Not_Listed__c == null){
                    //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter the Competitor that is not listed in the "Competitor Not Listed" field.  If there is no competition, please select "No Competition" in the "Competitor" field.');
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Competitor_Not_Listed_Error_Msg);
                    ApexPages.addMessage(myMsg);
                    isError = true;
                    isAddSession =true;
                } else if((tempComp != null && tempComp.name != 'Not Listed') && isComNListed){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Competitor_Not_Listed_Field_Populate_Error_Msg);
                    //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'If you do not see a Competitor in the list, please also select "Not Listed" in the Competitor field.  If there is no competition, please select "No Competition"');
                    ApexPages.addMessage(myMsg);
                    isError = true;
                    isAddSession =true;

        //Changes made by Renu***Starting

                } /*else if((tempComp != null && tempComp.name != 'Not Listed') && !isComNListed && tempProd == null){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a Competitor Product for the selected Competitor');
                    ApexPages.addMessage(myMsg);
                    isError = true;
                    isAddSession =true;

        //Changes made by Renu***End

                } */else {
                    if(tempComp != null && firstCName == null){
                            if(tempComp.Name != null && tempComp.Name != 'Not Listed')
                            System.debug('*** setting first comp. name ');
                                firstCName = tempComp.Name;
                                if(tempProd != null)
                                    firstCPName = tempProd.Name;
                                else
                                    firstCPName = null;
                    }
                    
                        //Create Asset object
                    if(ow.ocp.Auto_Create_Competitor_Asset__c){
                    System.debug('**** adding new Asset**** ow.ocp.Competitor__r.Name = ' + tempComp.name);
                       if(ow.ocp != null && ow.ocp.Competitor_Product__c != null)
                        optyCompAsset.add(createAsset(tempComp, ow.ocp));
                    }
                    optyCompTemp.add(ow.ocp);
                    oCPWrapList_Temp.add(ow);
                }                       
            }
            
        }
        
        if(!isError){
            try{
                // Update by Mung Fan. Move the code to the end of the if block
                /*
                System.debug('*** No errors : Upserting competitors BEFORE');
                    system.Database.upsert(optyCompTemp);
                System.debug('*** No errors : Upserting competitors AFTER');
                */
                // end of Update by Mung Fan
                
                if(firstCName != null){
                    isFirstSave = true;
                }
                //Update Oppty if isFirstSave = true
                /* //comment out by Mung Fan
                if(isFirstSave && (currentOppty.Competitor__c == null  && currentOppty.Product_Model__c == null )){
                        updateOppty(firstCName, firstCPName);
                        System.debug('**** updating Oppty**** currentOppty = ' + currentOppty); 
                }*/
                
                    for(OppCompProdWrap ow : oCPWrapList_Temp){
                        ow.flag = true;
                        ow.ocpId = ow.ocp.id;
                    }
                
                Savepoint sp = Database.setSavepoint();
                
                // Update by Mung Fan. 
                System.debug('*** No errors : Upserting competitors BEFORE');
                   system.Database.upsert(optyCompTemp);
                System.debug('*** No errors : Upserting competitors AFTER');  
                
                loadWrapperData();
                //   if(isFirstEdit){
                        System.debug('**** !editableJunction.isEmpty()');
                        boolean isErrorOnUpdate = updateOppty(oCPWrapList[0].ocp.Competitor__r.Name,oCPWrapList[0].ocp.Competitor_Product__r.Name,oCPWrapList[0].ocp.Competitor_Product__c);   
                if(isErrorOnUpdate){
                    Database.rollback( sp );
                    loadWrapperData();
                }
                else{
                    isFirstEdit = false;
                
                   // }                
                isItForSave = false;       
                isEditing = false;
                isAddSession = false;
                showAdd = true;
                }
                // End of update by Mung Fan. 
            }catch(Exception e){
                if(e.getMessage().contains('Value does not exist or does not match filter criteria')){
                 //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select valied Competitor for Product'));
                //return null;
                } else {                    
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                	ApexPages.addMessage(myMsg);
                }
                isAddSession = true;
                isItForSave = true; 
                isEditing = true;
                showAdd = false;
            }
            
        }//if !isError
        dbg = dbg + 'End of save:showAdd:'+showAdd+'isItForSave::'+isItForSave;
    }
   public void oppCompCancel(){
        isItForSave = false;
        showAdd = true;
        isEditing = false;
        isAddSession = false;
        List<OppCompProdWrap> oppTempWrapList = new List<OppCompProdWrap>();
         for(OppCompProdWrap ow : oCPWrapList){
         if(ow.ocp != null && ow.ocpId != null)
           {
              ow.flag = true;
              oppTempWrapList.add(ow);
            }
         }
      oCPWrapList = new List<OppCompProdWrap>();
             oCPWrapList = oppTempWrapList;
             
      system.debug('********* oCPWrapList:::'+oCPWrapList);
        }
    public class OppCompProdWrap{
        public ID ocpId {get;set;}
        public OpportunityCompetitorProduct__c ocp {get;set;}
        public boolean flag {get;set;}
        OppCompProdWrap(OpportunityCompetitorProduct__c o, boolean isDisplay){
            ocp = o;
            ocpId = o.id;
            flag = isDisplay;
        }
    }
    /*
    * Create new Asset object
    */
    public Asset__c createAsset(Competitor__c tempComp, OpportunityCompetitorProduct__c ocp){
    System.debug('*** ocp.Opportunity__r.AccountId = ' + ocp.Opportunity__r.AccountId);
    System.debug('*** ocp.Competitor__r.Name = ' + ocp.Competitor__r.name);
        Asset__c compAsset = new Asset__c();
            compAsset.Name = tempComp.name;
            compAsset.Customer_Name__c = currentOppty.AccountId;
            compAsset.Install_Base_Status__c = 'Install';
            if(tempComp.Name == null ){
                compAsset.Product_Name_Vendor__c = 'Not Listed';
            }else {
                compAsset.Product_Name_Vendor__c = tempComp.name;
            }
        Competitor_Product__c cptemp = [select id,name from Competitor_Product__c where id =: ocp.Competitor_Product__c ];
            compAsset.Product_Family__c = cptemp.name;
            compAsset.Model__c = cptemp.name;
            //compAsset.Opportunity_Name__c = closeOppObj.Id;
            //insert compAsset;
            return compAsset;
    }
    public boolean updateOppty(String compName, String compProdName, ID competitorProductId){
        Savepoint sp = Database.setSavepoint();
        boolean isErrorOnThis = false;
       
        currentOppty.Competitor__c = compName;
        currentOppty.Product_Model__c = compProdName;
        // update by Mung Fan for ID-String conversion
        if (competitorProductId != null){
            currentOppty.Competitor_Product_ID__c = '' + competitorProductId;
        } else {
            currentOppty.Competitor_Product_ID__c = null; 
        }
        // end of update
        
        database.SaveResult sr;
        try{
            sr = database.update(currentOppty);
            if(! sr.isSuccess() ){
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, err.getMessage() );
                    ApexPages.addMessage(myMsg);
                    Database.rollback( sp );
                    isErrorOnThis = true;
                }

            }
            
        }catch(exception exOpty){
            string errMsg = exOpty.getMessage();
            ApexPages.Message myMsg;
            if(errMsg.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION,')){
                errMsg = errMsg.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION,', '[]');
                myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, errMsg);
            }else{
                myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, exOpty.getMessage());
            }
            
            ApexPages.addMessage(myMsg);
            Database.rollback( sp );
            isErrorOnThis = true;
        }

        System.debug('**** in updateOppty method currentOppty = ' + currentOppty);
        return isErrorOnThis;
        
    }
    public string CompetitorLostTo {get;set;}  {CompetitorLostTo  = '';}
    public pagereference updateCompetitorLost(){ 
        Id editId = ApexPages.currentPage().getParameters().get('editId');
        system.debug('********editId::'+ editId);
        CompetitorLostTo = Apexpages.currentPage().getParameters().get('CompetitorLostTo'); 
        system.debug('********oCPWrapList::'+ oCPWrapList);
        system.debug('****parameter from UI OutSide Forloop::'+CompetitorLostTo);
        for(OppCompProdWrap ow : oCPWrapList){ 
            system.debug('****parameter from UI::'+CompetitorLostTo);
            system.debug('****current iteration ID is::'+ow.ocpID);
                if(ow.ocpid == CompetitorLostTo){ 
                    ow.ocp.Competitor_Lost_To__c =  ow.ocp.Auto_Create_Competitor_Asset__c; 
                } 
        }
        return null; 
    }
    public void initialize(){
        isDisplayNormalMsg = false;
        Long closeTime = Datetime.newInstance(currentOppty.CloseDate.year(), currentOppty.CloseDate.month(),currentOppty.CloseDate.day()).getTime() ;
        Long nowTime = System.now().getTime();
        System.debug('*** oppty close date : ' + currentOppty.CloseDate + ' : ' + 'System.now() = ' + System.now());
        System.debug('*** closeTime = ' + closeTime + ' : ' + 'nowTime' + nowTime);
        //if(currentOppty.CloseDate < System.now()){
            isOptyClosePast = (closeTime < nowTime) ? true : false;
        //}
        if(isOptyClosePast){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,System.Label.CompProduct_CloseDateNeedsFuture));
            isDisplayNormalMsg = true;
        }
        
        System.debug('***** isOptyClosePast = ' + isOptyClosePast);
    }
}