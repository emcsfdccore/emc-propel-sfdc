/*================================================================================================================================================+
 |  HISTORY  |
 |  DATE          DEVELOPER               WR                        DESCRIPTION 
 |  ====          =========               ==                        =========== 
 | 24 Jan 2013    Anirudh Singh          219168                     This Test Class is used to unit Test EMC_Consulting Classes          
 | 17 June 2014   Akash Rastogi           989                       Modified class to cover validateCompetency EMC_Consulting_Operations class
 | 14-April-2015  Vinod Jetti             #1849                     Modified class to cover populatePortfolio method in EMC_Consulting_Operations class
 +===========================================================================================================================================================*/
 
@isTest(SeeAllData=true)
public class EMC_Consulting_TC {
/*@Method <This method will be used to unite Test EMC consulting functionality>
@param <This method is not taking any input parameters>
@return <void> - <Not Returning anything>
@throws exception - <No Exception>
*/
    Private static testMethod void EMC_Consulting_Operations(){
        User EMCConsultingUser = [Select id from User where Profile.Name = 'EMC Consulting User' and IsActive=True limit 1];
        
        Opportunity Opp= new Opportunity();
        opp = testclassUtils.getOppty();
        insert opp;     
        
        Detailed_Product__c detailProdObj = new Detailed_Product__c();
        decimal affinityPartner = 50;
        detailProdObj.Percent_Delivered_Affinity__c = 50;
        detailProdObj.Affinity_Partners__c = 'None';
        detailProdObj.Total_Competency__c = null; 
        detailProdObj.Sub_Practice_1__c = 'AADD-Appl Devel & Integration';
        detailProdObj.Sub_Practice_1_Dollar__c = 100.00;
        detailProdObj.Sub_Practice_2__c = 'AADD-Appl Devel & Integration';
        detailProdObj.Sub_Practice_2_Dollar__c = 100.00;
        detailProdObj.Sub_Practice_3__c = 'AADD-Appl Devel & Integration';
        detailProdObj.Sub_Practice_3_Dollar__c = 100.00;
        detailProdObj.Sub_Practice_4__c = 'AADD-Appl Devel & Integration';
        detailProdObj.Sub_Practice_4_Dollar__c = 100.00;
        detailProdObj.Sub_Practice_5__c = 'AADD-Appl Devel & Integration';
        detailProdObj.Sub_Practice_5_Dollar__c = 100.00;
        detailProdObj.Identified_Oppty__c ='EMC';
        detailProdObj.Opportunity__c = opp.Id;
        detailProdObj.Start_Date__c = System.Now().Date();
        
        Insert detailProdObj;
        
        // WR :989 - one more instance of detailed product created to increase the coverage
        
        Detailed_Product__c detailProdObj2 = new Detailed_Product__c();
        decimal affinityPartner1 = 50;
        detailProdObj2.Percent_Delivered_Affinity__c = 50;
        detailProdObj2.Affinity_Partners__c = 'None';
       
        detailProdObj2.Sub_Practice_1_Dollar__c = null;
        
        detailProdObj2.Sub_Practice_2_Dollar__c = null;
        
        detailProdObj2.Sub_Practice_3_Dollar__c = null;
        
        detailProdObj2.Sub_Practice_4_Dollar__c = null;
        detailProdObj2.Sub_Practice_5__c = 'AADD-Appl Devel & Integration';
        detailProdObj2.Sub_Practice_5_Dollar__c = 100.00;
        detailProdObj2.Identified_Oppty__c ='EMC';
        detailProdObj2.Opportunity__c = opp.Id;
        detailProdObj2.Start_Date__c = System.Now().Date();
        
        Insert detailProdObj2;
        
               
        PageReference pageRef = Page.EMCC_Total_Competency_Alert;
        pageRef.getParameters().put('Id', detailProdObj.id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdCon = new ApexPages.StandardController(detailProdObj);
        
         EMCC_Total_Competency_Alert_Controller emcConsultingController = new EMCC_Total_Competency_Alert_Controller(stdCon);
        
        detailProdObj.Sub_Practice_1_Dollar__c =200;
        Update detailProdObj;
        ApexPages.StandardController stdCon1 = new ApexPages.StandardController(detailProdObj);
        
        EMCC_Total_Competency_Alert_Controller emcConsultingController1 = new EMCC_Total_Competency_Alert_Controller(stdCon1);
 
        EMC_Consulting_Operations.isAlreadyExecuted = false;
        
        /* @WR - 989 : Below code is used to cover validateCompetency 
        *              EMC_Consulting_Operations class 
        *  Line : 86 - 112
        */
        Account acc = testclassUtils.getAccount();
        insert acc;
        
        
        //get standard pricebook
        Pricebook2  standardPb = testclassUtils.getPricebook2();
        
        Product2 prd1 = testclassUtils.getProduct2('PS Consulting','USD', 'PSC');
        insert prd1;
        
        PricebookEntry pbe1 = testclassUtils.getPricebookEntry(prd1.id ,standardPb.id, 0, true);
        insert pbe1;
        
        Opportunity opp1 = testclassUtils.getOpportunity('Opp1','Upside',Date.today(), pbe1.Pricebook2Id, acc.id);
        insert opp1;
        
        OpportunityLineItem lineItem1 = testclassUtils.getOpportunityLineItem(opp1.id, pbe1.id, 4, 200);
        insert lineItem1;
        
        List<Detailed_Product__c> lstDetailProd = new List<Detailed_Product__c>();
        for(Integer i=0;i<=2;i++){
        Detailed_Product__c detailProdObj1 = new Detailed_Product__c();
        detailProdObj1.Percent_Delivered_Affinity__c = 50;
        detailProdObj1.Affinity_Partners__c = 'None';
        detailProdObj1.Opportunity__c = opp1.Id;
        detailProdObj1.GS_Play__c = 'Big Data: Federation Data Lake';
        detailProdObj1.Offer_aligned_to_play__c = 'Federation Big Data Vision Workshop';
        detailProdObj1.CurrencyIsoCode = 'USD';
        detailProdObj1.Start_Date__c = System.Today();
        lstDetailProd.add(detailProdObj1);
        }
        insert lstDetailProd;
      //#1849 changes - Start
       EMC_Consulting_Operations.populatePortfolio(lstDetailProd); 
      //#1849 changes - End 
   }
}