/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE       DEVELOPER        WR       DESCRIPTION                               
 |  ====       =========        ==       =========== 
 |  16-Dec-2014 Bhanuprakash  PROPEL     Created new controller for new hybris Create Quote action  
 |  24-Apr-2015 Bhanuprakash  PROPEL     Fixed defect # 20082 by handling exception on update oppty          
  +===========================================================================*/

public with sharing class Oppty_CreateQuoteController{
 //Declaring class variables.
   public Boolean showBackLink{set;get;}
   public String createQuoteEncryptKey;
   public static PROPEL_General_Settings__c gCS;
   private Blob key;
   private String iv;
   private Blob ivBinary;
   private String Ivb64;
   private Blob dataValue;
   private Blob encrypted;
   private String encryptedb64;
   private String queryString = '?';
   
   public Opportunity  quriedOppty {get;set;}
   public Opportunity currentOppty{get;set;}
 
   public Oppty_CreateQuoteController(ApexPages.StandardController controller){
        this.currentOppty=(Opportunity)controller.getRecord();
        System.debug('**** in Controller constructer');
        //Get encrypt base64 format
        gCS = PROPEL_General_Settings__c.getInstance();
        createQuoteEncryptKey = gCS.Create_Quote_Encrypt_Key__c;
        System.debug('*** formate64 = ' + createQuoteEncryptKey);
        iv = generateRandomString(16);
        System.debug('*** iv = ' + iv);
        ivBinary = Blob.valueOf(iv); 
        ivb64 = EncodingUtil.base64encode(ivBinary); // this is the iv value that will be sent to Hybris
        System.debug('**** ivb64 = ' + ivb64); 
   }//Opp_CreateQuote()
   
   // Perform Validation
   /*   Opens a VF page that will validate the following 
        Update and save the opportunity to verify if there are any errors
        Validate that Sales Channel and Sales Force are populated
        Validate that Quote # is not already present
        Check that the user can edit the record (by querying UserRecordAccess for edit rights)
        If there are errors, display the error in a page message and give them a button to navigate back to the opportunity
    */
   public PageReference validate() {
        try{//Update and save the opportunity to verify if there are any errors
            update currentOppty;
            //throw new CreateQuoteException(System.Label.PROPEL_User_need_Edit_access);
        }catch(Exception e){
            ApexPages.addmessages(e);
            System.debug('*** Error : ' + e.getMessage());
             showBackLink=true; return null;
        }//end catch
        try       {
           if(currentOppty.Id!=null) {
              String opportId=currentOppty.id+''; 
              if(opportId.subString(0,3)=='006') {
                   quriedOppty = [select o.Id, o.Name,o.Edit_Access__c ,o.Quote_Operating_Unit__c,o.Sell_Relationship__c,o.Update_Forecast_Amount_from_Quote__c,o.Opportunity_Number__c,
                                        o.Quote_Cart_Number__c,o.Integration_Error_Message__c,o.Quote_Order_Type__c, o.Sales_Force__c, o.Sales_Channel__c,Partner__r.Partner_Quoting_Account_SAP_UCID__c,o.Tier_2_Partner__r.Partner_Quoting_Account_SAP_UCID__c, o.primary_alliance_partner__r.UCID__c, o.secondary_alliance_partner__r.UCID__c, o.oem_partner__r.UCID__c, o.Service_Provider__r.UCID__c, o.Account.UCID__c from Opportunity o where Id =: currentOppty.Id];
              }
           } 
       } catch(QueryException e) { 
        //ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage() ));
        System.debug('the exception is' +e); 
       }
       
        //Check User access on Opportunity
        if(!checkOpportunityAccess()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.PROPEL_User_need_Edit_access));
            showBackLink = true; return null;
            System.debug('**** User not having access on this Oppty');
        }
            System.debug('**** User having access on this Oppty');
        //Validate that Sales Channel and Sales Force are populated
        if(quriedOppty.Sales_Channel__c == null || quriedOppty.Sales_Channel__c == '' ){
            //showSalesChannelError = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.PROPEL_SalesChannel_Required));
            showBackLink = true; return null;
        }

         if(quriedOppty.Sales_Force__c == null || quriedOppty.Sales_Force__c == '' ){
            //showSalesForceError = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.PROPEL_SalesForce_Required));
            showBackLink = true; return null;
        }
        //Show Exception if Quote Number is already populated
        if(quriedOppty.Quote_Cart_Number__c != null ){
            //showQuoteNumberError = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.PROPEL_Duplicate_Quote));
            showBackLink = true;  return null;
        }
        //Prepare Hybris URL
        //PROPEL_General_Settings__c gS = PROPEL_General_Settings__c.getInstance();
        String hybrisURL = gCS.Hybris_URL__c;
        System.debug('**** hybrisURL = ' + hybrisURL);
        
        //Prepare Query String
        //Prepare encrypted 'distributionChannel' parameter to send as query string.
        if(quriedOppty.Sales_Channel__c != null && quriedOppty.Sales_Channel__c != ''){
            if(quriedOppty.Sales_Channel__c == gCS.Direct_Distribution_Channels__c){
                System.debug('**** Direct = ' + gCS.Direct_Distribution_Channels__c);
                addQueryString('distributionChannel',quriedOppty.Sales_Channel__c);
                //hybrisURL+='&' + 'distributionChannel' + '=' + encriptValue(quriedOppty.Sales_Channel__c);
            }
        }
        
        //Prepare encrypted 'quoteName' parameter to send as query string.
        if(quriedOppty.Name != null && quriedOppty.Name != ''){
            addQueryString('quoteName',quriedOppty.Name);
            //hybrisURL+='&' + 'quoteName' + '=' + encriptValue(quriedOppty.Name);
        }
        
         //Prepare encrypted 'account' parameter to send as query string.
        if(quriedOppty.Account.UCID__c != null && quriedOppty.Account.UCID__c != ''){
            addQueryString('account',quriedOppty.Account.UCID__c);
            //hybrisURL+='&' + 'account' + '=' + encriptValue(quriedOppty.Account.UCID__c);
        }
        
          //Prepare encrypted 'tier1' parameter to send as query string.
        if(quriedOppty.Partner__r.Partner_Quoting_Account_SAP_UCID__c != null ){
            addQueryString('tier1',quriedOppty.Partner__r.Partner_Quoting_Account_SAP_UCID__c);
            //hybrisURL+='&' + 'tier1' + '=' + encriptValue(quriedOppty.Partner__r.Partner_Quoting_Account_SAP_UCID__c);
        }
        
          //Prepare encrypted 'tier2' parameter to send as query string.
        if(quriedOppty.Tier_2_Partner__r.Partner_Quoting_Account_SAP_UCID__c != null){
            addQueryString('tier2',quriedOppty.Tier_2_Partner__r.Partner_Quoting_Account_SAP_UCID__c);
            //hybrisURL+='&' + 'tier2' + '=' + encriptValue(quriedOppty.Tier_2_Partner__r.Partner_Quoting_Account_SAP_UCID__c);
        }
        
        //Prepare encrypted 'opportunityNumber' parameter to send as query string.
        if(quriedOppty.Opportunity_Number__c != null && quriedOppty.Opportunity_Number__c != ''){
            addQueryString('opportunityNumber',quriedOppty.Opportunity_Number__c);
            //hybrisURL+='&' + 'opportunityNumber' + '=' + encriptValue(quriedOppty.Opportunity_Number__c);
        }
   
          //Prepare encrypted 'primaryAlliancePartner' parameter to send as query string.
        if(quriedOppty.primary_alliance_partner__r.UCID__c != null && quriedOppty.primary_alliance_partner__r.UCID__c != ''){// TO-DO : Create Account
            addQueryString('primaryAlliancePartner',quriedOppty.primary_alliance_partner__r.UCID__c);
            //hybrisURL+='&' + 'primaryAlliancePartner' + '=' + encriptValue(quriedOppty.primary_alliance_partner__r.UCID__c);
        }
        
          //Prepare encrypted 'secondaryAlliancePartner' parameter to send as query string.
        if(quriedOppty.secondary_alliance_partner__r.UCID__c != null && quriedOppty.secondary_alliance_partner__r.UCID__c != ''){
            addQueryString('secondaryAlliancePartner',quriedOppty.secondary_alliance_partner__r.UCID__c);
            //hybrisURL+='&' + 'secondaryAlliancePartner' + '=' + encriptValue(quriedOppty.secondary_alliance_partner__r.UCID__c);
        }
        
          //Prepare encrypted 'oemPartner' parameter to send as query string.
        if(quriedOppty.oem_partner__r.UCID__c != null && quriedOppty.oem_partner__r.UCID__c != ''){
            addQueryString('oemPartner',quriedOppty.oem_partner__r.UCID__c);
            //hybrisURL+='&' + 'oemPartner' + '=' + encriptValue(quriedOppty.oem_partner__r.UCID__c);
        }
        
          //Prepare encrypted 'servicePartner' parameter to send as query string.
        if(quriedOppty.Service_Provider__r.UCID__c != null && quriedOppty.Service_Provider__r.UCID__c != ''){// TO-DO : Create Account
            addQueryString('servicePartner',quriedOppty.Service_Provider__r.UCID__c);
            //hybrisURL+='&' + 'servicePartner' + '=' + encriptValue(quriedOppty.Service_Provider__r.UCID__c);
        }
        
          //Prepare encrypted 'quoteSource' parameter to send as query string.(If 'Sales_Force__c' value is 'ESG' then send 'IIG'
        if(quriedOppty.Sales_Force__c  != null && quriedOppty.Sales_Force__c != ''){
                if(quriedOppty.Sales_Force__c == 'ESG')
                    //hybrisURL+='&' + 'quoteSource' + '=' + encriptValue('IIG');
                    addQueryString('quoteSource','IIG');
                else
                    addQueryString('quoteSource',quriedOppty.Sales_Force__c);
        }
        
        //Prepare encrypted 'iv' parameter to send as query string.
        /*if((iv != null && iv != '') && ivb64!= null){
            //addQueryString('iv',ivb64);
            System.debug('**** iv if ');
            queryString+='&' + 'iv' + '=' + ivb64;
        }*/
        
        queryString+='&' + 'iv' + '=' + ivb64;
        queryString+= '&site=propel'; 
        System.debug('****** url = ' + hybrisURL + queryString);
        PageReference redirectPage = new PageReference(hybrisURL + queryString);
        System.debug('***** page URL = ' + redirectPage.getUrl());
        return redirectPage;
    }
    public PageReference redirectToOppty(){
        return new PageReference('/'+quriedOppty.Id);
    }
    /*
    *  Check current user has access with the Opportunity
    */
    private  Boolean checkOpportunityAccess(){
    System.debug('**** in checkOpportunityAccess method');
        Id cUserId = UserInfo.getUserId();
        List<UserRecordAccess> lstURA = [SELECT RecordId FROM UserRecordAccess WHERE HasEditAccess = true AND UserId =:cUserId AND RecordId=:quriedOppty.Id];
        if(lstURA != null && !lstURA.isEmpty()){
        System.debug('*** User hase Edit access on this Oppty');
            return true;
        }
    System.debug('**** in checkOpportunityAccess method returning false');
        return false;
    }
    /*
    *   Encrypts value passed
    */
    private String encriptValue(String value){
    System.debug('**** in encriptValue()');
        // key should be in a custom setting
        if(createQuoteEncryptKey != null && createQuoteEncryptKey != ''){
            key = EncodingUtil.base64Decode(createQuoteEncryptKey);
            System.debug('**** key = ' + key);
            
            dataValue = Blob.valueOf(value);
            encrypted = Crypto.encrypt('AES128', key, ivBinary, dataValue);
            encryptedb64 = EncodingUtil.base64Encode(encrypted); // this is the parameter value that is sent to Hybris
            System.debug('*** final value = ' + encryptedb64);
        }
    return encryptedb64;
    }
    /*
    * Generates 16 digit random string
    */
    public String generateRandomString(Integer len){
    System.debug('**** in generateRandomString()');
        final string chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while(randStr.length()<len){
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
            randStr+= chars.substring(idx, idx+1);
        }
        return randStr;
    }
    /*
    * Checks and adds new query string
    */
    public void addQueryString(String name, String value){
        System.debug('**** in addQueryString()');
        if(queryString.endsWith('?'))
                queryString+= name + '=' + EncodingUtil.urlEncode(encriptValue(value), 'UTF-8');
            else
                queryString+='&' + name + '=' + EncodingUtil.urlEncode(encriptValue(value), 'UTF-8');
        
    }
    /*
    * Custom exception class
    */
    private class CreateQuoteException extends Exception{}
   }