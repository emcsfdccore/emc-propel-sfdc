/*================================================================================================================================================+
 |  HISTORY  |
 |  DATE          DEVELOPER               WR                        DESCRIPTION 
 |  ====          =========               ==                        =========== 
 |  21 Nov 2014   Vivek Barange         875                         Fetching batch size from dataValueMap Custom setting
 +===========================================================================================================================================================*/
global class Schedule_AP_Audit_Customer_Profiling implements schedulable
{
    global void execute(SchedulableContext sc)
    {
       Map<String,CustomSettingDataValueMap__c> dataValueMap = CustomSettingDataValueMap__c.getall();
       integer batchSize = (integer)dataValueMap.get('APAuditCustomerProfilingBatchLimit').DateValue__c;
       AP_Customer_Profile_Auditing clsBatch = new AP_Customer_Profile_Auditing();
       ID idBatch = Database.executeBatch(clsBatch, batchSize);
    }
}