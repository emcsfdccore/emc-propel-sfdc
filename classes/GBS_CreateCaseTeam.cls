/*======================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
| 31/Oct/2014  Jaypal Nimesh   GBS Comm Project     Created class to insert Case Team.
|
+=======================================================================================*/
public class GBS_CreateCaseTeam {
    
    public void createCaseTeamMember(List<Case> newCaseList){
        
        List<CaseTeamRole> caseTeamRoleList = [Select Id, Name from CaseTeamRole where Name = 'Observer'];
        
        List<UserRole> userRoleList = [Select Id, Name, DeveloperName from UserRole where DeveloperName = 'Basic_Employee'];
        
        List<CaseTeamMember> CaseTeamMemberList = new List<CaseTeamMember>();
        
        if(caseTeamRoleList != null && caseTeamRoleList.size() > 0 && userRoleList != null && userRoleList.size() > 0){
            
            for(Case cs : newCaseList){
            
                if(Userinfo.getUserType().equalsIgnoreCase('Standard') && Userinfo.getUserRoleId() != null && Userinfo.getUserRoleId().equals(userRoleList[0].Id)){
                    
                    CaseTeamMember ctm = new CaseTeamMember(MemberId = Userinfo.getUserId(), ParentId = cs.Id, TeamRoleId = caseTeamRoleList[0].Id);
                    CaseTeamMemberList.add(ctm);
                }
            }
            
            if(CaseTeamMemberList != null && CaseTeamMemberList.size() > 0){
                
                Database.SaveResult[] srList = Database.insert(CaseTeamMemberList, false);
            }   
        }
    }
}