/*========================================================================================================================+
|  HISTORY                                                                  
|                                                                           
|   DATE            DEVELOPER       WR          DESCRIPTION                               
|   ====            =========       ==          =========== 
|   10/17/2014      Sneha Jain      291     R2R Nov'14 Rel - Initial Creation - Opportunity need to be CLOSED from Custom CLOSE button on detail page
!   12/17/2014      Bindu Sri       CI 1559  Replaced the closed reason values
+========================================================================================================================*/
public class Opportunity_Close_Controller{

    public String reasons{get;set;}
    public String operations{get;set;}
    public Boolean alreadyClosed{get;set;}
    
    //Get the record getting CLOSED
    public Opportunity closeOppObj{get;set;}{closeOppObj= new Opportunity();}
    public String opptyId ='';
    List<Opportunity_Asset_Junction__c> oajList = new List<Opportunity_Asset_Junction__c>();
    public Boolean blnAutoCreateAsset {get;set;} {blnAutoCreateAsset = false;}
    public Boolean blnDiasbleAssetCheckbox {get;set;} {blnDiasbleAssetCheckbox = true;}

    public String strCheckboxLabel {get;set;}
    public String strCheckboxHelpText {get;set;}

    public PageReference ClosedReasonChange()
    {
        if(closeOppObj.Closed_Reason__c == 'Lost to Competition')
        {
            blnDiasbleAssetCheckbox = false;
            blnAutoCreateAsset = true;
        }
        else
        {
            blnDiasbleAssetCheckbox = true;
            blnAutoCreateAsset = false;
        }
        return null;
    }


    public Opportunity_Close_Controller(ApexPages.StandardController controller){
        closeOppObj = (Opportunity)controller.getRecord();
        alreadyClosed = false;
        String validationStages = System.Label.Opportunity_Close_Validation;
        if(closeOppObj != null){
            closeOppObj = [select id,stageName,AccountId,Account_Name1__c,Closed_Reason__c,Competitor__c,Close_Comments__c,Duplicate_Opportunity__c,Competitor_Lost_To__c,Competitor_Product__c,Sales_Channel__c,Renewals_Close_Details__c,Closed_Reason_Action__c,Opportunity_type__c from Opportunity where id =: closeOppObj.Id];
            //User cannot close an already CLOSED opportunity
            //if(closeOppObj.stageName == 'Closed'){
            if(closeOppObj.stageName != '' && validationStages != '' && validationStages.contains(closeOppObj.stageName)){
                alreadyClosed = true;
                ApexPages.Message myMsg;
                myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.R2R_Opp_already_closed);
                ApexPages.addMessage(myMsg);
            }
            closeOppObj.stageName='Closed';
        }

        Map<String,CustomSettingDataValueMap__c> mapDataValueMap = CustomSettingDataValueMap__c.getAll();

        if(mapDataValueMap!= null)
        {
            if(mapDataValueMap.get('R2RCloseCheckboxLabel') != null &&
                mapDataValueMap.get('R2RCloseCheckboxLabel').DataValue__c != null)
            {
                strCheckboxLabel = mapDataValueMap.get('R2RCloseCheckboxLabel').DataValue__c;
            }

            if(mapDataValueMap.get('R2RCheckboxHelpText') != null &&
                mapDataValueMap.get('R2RCheckboxHelpText').DataValue__c != null)
            {
                strCheckboxHelpText = mapDataValueMap.get('R2RCheckboxHelpText').DataValue__c;
            }

        }

    }
    
    //Closed Reason for the opportunity.
    public List<SelectOption> getclosedReasons() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('--None--','--None--'));
        //options.add(new SelectOption('Chose Incumbent','Chose Incumbent'));
        options.add(new SelectOption('Lost to Competition','Lost to Competition'));
        options.add(new SelectOption('Competition - Create Competitive Asset','Competition - Create Competitive Asset'));
        //options.add(new SelectOption('Deal Reg Expired','Deal Reg Expired'));
        options.add(new SelectOption('Duplicate','Duplicate'));
        //options.add(new SelectOption('Lost Funding','Lost Funding'));
        //Added as part of CI 1559
        options.add(new SelectOption('Project Cancelled','Project Cancelled'));
        
        //options.add(new SelectOption('Outpriced','Outpriced'));
        //options.add(new SelectOption('Outsourcer','Outsourcer'));
        //options.add(new SelectOption('Partner','Partner'));
        //options.add(new SelectOption('Qualified Out','Qualified Out'));
        options.add(new SelectOption('EMC withdrew Bid','EMC withdrew Bid'));
        // End of CI 1559
        options.add(new SelectOption('TCE Issue/Product Quality','TCE Issue/Product Quality'));
        //options.add(new SelectOption('Terminated','Terminated'));
        //options.add(new SelectOption('Unable to Assign Resources','Unable to Assign Resources'));
        return options;
    }
    
    //User clicks on SAVE button after providing the CLOSE information. The details get saved on the Opportunity record
    public pageReference saveOpp(){
        
        if(closeOppObj !=null){
            closeOppObj.stageName = 'Closed';
            if(closeOppObj.Closed_Reason__c == '--None--'){
                closeOppObj.Closed_Reason__c = '';
            }
            else{
                closeOppObj.Closed_Reason__c = closeOppObj.Closed_Reason__c;//reasons;
            }   
            system.debug('----reasons---'+reasons);
            system.debug('----closeOppObj.Closed_Reason__c---:'+closeOppObj.Closed_Reason__c);
            //If Closed Reason = Duplicate, auto select Transfer Assets for next page (if there are assets linked to the opportunity.
            if(closeOppObj.Closed_Reason__c == 'Duplicate'){
                operations = 'Transfer Assets';
            }
            try{
                system.debug('----closeOppObj.Close_Comments__c---'+closeOppObj.Close_Comments__c);
                //Update the opportunity record with the details entered by the user.
                update closeOppObj;
                if(closeOppObj.Closed_Reason__c == 'Lost to Competition' && blnAutoCreateAsset){

                    //system.debug('#### Inside Inertion Statement');

                    //Create new Competitive Record when Closed Reason selected = 'Competition - Create Competitive Asset'
                    Asset__c compAsset = new Asset__c();
                    compAsset.Name = closeOppObj.Competitor_Lost_To__c + ' - ' + closeOppObj.Account_Name1__c;
                    compAsset.Customer_Name__c = closeOppObj.AccountId;
                    compAsset.Install_Base_Status__c = 'Install';
                    compAsset.Product_Name_Vendor__c = closeOppObj.Competitor_Lost_To__c;
                    compAsset.Product_Family__c = closeOppObj.Competitor_Product__c;
                    compAsset.Model__c = closeOppObj.Competitor_Product__c;
                    compAsset.Opportunity_Name__c = closeOppObj.Id;
                    insert compAsset;
                    //system.debug('#### After Insertion Statement; Asset Id: '+compAsset.id);
                }
            }
            catch(Exception e){
                system.debug('----Catch exception----'+e.getMessage());
                String fullError = e.getMessage();
                Integer startIndex = 0;
                if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') || e.getMessage().contains('CIRCULAR_DEPENDENCY')){
                    if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                        startIndex = fullError.lastindexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,') + 35;  
                    }
                    else if(e.getMessage().contains('CIRCULAR_DEPENDENCY')){
                        startIndex = fullError.lastindexOf('CIRCULAR_DEPENDENCY,') + 21;    
                    }
                    String displayError = fullError.substring(startIndex,fullError.IndexOf(':',startIndex));
                    ApexPages.Message myMsg;
                    myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, displayError);
                    ApexPages.addMessage(myMsg);
                }
                return null;                
            }
        }
        //Check if any assets are linked to the opportunity only if Opportunity type != Renewals
        if(closeOppObj.Opportunity_type__c != 'Renewals'){
            oajList = [select id,Related_Asset__c,Related_Opportunity__c from Opportunity_Asset_Junction__c where Related_Opportunity__c =: closeOppObj.Id];
            //If assets are linked to the Opportunity, redirect the user to Asset action page
            if(oajList !=null && oajList.size()>0){
                return new pageReference('/apex/R2Rchoose_action_to_assets?OppId='+closeOppObj.Id);
            }
        }   
        //Redirect user to Opportunity detail page
        return new pageReference('/'+closeOppObj.Id);
    }
    
    //User clicks on SAVE button after providing the details of Asset action.
    public PageReference saveAssetOp(){
        
        system.debug('----operations---'+operations);
        system.debug('----closeOppObj.Closed_Reason__c---'+closeOppObj.Closed_Reason__c);
        
        //No action selected, user will get an error message
        if(operations == null){
            ApexPages.Message myMsg;
            myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select an option.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        
        //If user selects 'Transfer Assets' then all the associated assets should also get linked to the opportunity provided
        if(operations == 'Transfer Assets'){
            system.debug('----closeOppObj.Duplicate_Opportunity__c---'+closeOppObj.Duplicate_Opportunity__c);
            if(closeOppObj.Duplicate_Opportunity__c !=null){
                Set<Opportunity> oppSet = new Set<Opportunity>();
                oppSet.add(closeOppObj);
                Map<Id,Id> oppDupIdMap = new Map<Id,Id>();
                oppDupIdMap.put(closeOppObj.Id,closeOppObj.Duplicate_Opportunity__c);
                R2R_Opportunity_Management oppAsset = new R2R_Opportunity_Management();
                oppAsset.transferAssetToDuplicateOpp(oppSet,oppDupIdMap);
                return new pageReference('/'+closeOppObj.Id);
            }
            //Opportunity is mandatory to transfer the assets
            else{
                ApexPages.Message myMsg;
                myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.R2R_Transfer_to_Opp_Required);
                ApexPages.addMessage(myMsg);
            }
            return null;
        }
        //If user selects 'Defer Assets' then all the associated assets should get Deferred (Defer Asset = true)
        if(operations == 'Defer Assets'){
            system.debug('---oajList---'+oajList);
            if(oajList !=null && oajList.size()>0){
                List<Id> assetIdList = new List<Id>();
                for(Opportunity_Asset_Junction__c oaj: oajList){
                    assetIdList.add(oaj.Related_Asset__c);
                }
                if(assetIdList !=null && assetIdList.size()>0){
                    //R2R_AssetStatus deferAssetsObj = new R2R_AssetStatus();
                    //deferAssetsObj.deferAsset(assetIdList);
                    R2R_AssetStatus.deferAsset(assetIdList);
                }
            }
            return new pageReference('/'+closeOppObj.Id);
        }
        //If user selects 'No Action' then no separate action should take place on the Assets
        if(operations == 'No Action'){
            return new pageReference('/'+closeOppObj.Id);
        }
        system.debug('---operations---'+operations);
        return new pageReference('/'+closeOppObj.Id);
    }
}