/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER             WR        DESCRIPTION                               

 |  ====          =========             ==        =========== 

 |  28/10/2013    Srikrishnadevaraya    SM             This Test class is used to test the functionality of KB_Controller.
 |  06/26/2014    Vivek Barange         1055           To Increase Test Coverage.   
 |  Dec/11/2014   Bisna V P             CI 1555        updated the Clone Option for Security KB article type.
                      
 +==================================================================================================================**/
@isTest 
private class KB_Controller_TC {
    static testMethod  void kbController(){
        
        //Insert Custom Setting Data
        CustomSettingDataHelper.eBizSFDCIntCSData();
        CustomSettingDataHelper.dataValueMapCSData();
        //Insert Custom Setting Data
        List<User> userList = new List<User>();
        test.startTest();
        Account A1 = new Account(Name = 'Test Account');
        insert A1;

        List<Contact> lstCont = new List<Contact>();

        for(Integer i=0;i<2;i++){
            Contact contObj = new Contact();
            contObj.AccountID = A1.id;
            contObj.FirstName = 'TestFirst'+i;
            contObj.LastName = 'TestLast'+i; 
            contObj.email = 'sep.contact'+i+'@fakeemail.com';
            lstCont.add(contObj);
            }
        
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser)
        {
            insert lstCont;
        }
        
        List<Profile> profiles = [ Select id from Profile where name in ('Knowledge EMC Internal Portal User','Knowledge Customer Portal User')];
        User internalPortal_user = new User( email='test-user@fakeemail.com', contactid = lstCont[0].id, profileid = profiles[0].id, UserName='test-user@fakeemail.com', alias='tuser1', CommunityNickName='tuser1', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User', FederationIdentifier = '123' );
        userList.add(internalPortal_user);             

        User customerPortal_user = new User( email='customer.contact@fakeemail.com', contactid = lstCont[1].id, profileid = profiles[1].id, UserName='customer.contact@fakeemail.com', alias='custuser', CommunityNickName='custuser', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Customer', LastName = 'User' );
        userList.add(customerPortal_user);
        
        if(!userList.isEmpty()){
            insert userList;    
        }

        Map<String,Id> idMap = new Map<String,Id>();
        Map<id,Id> idMap1 = new Map<id,Id>();
        List<Id> idList = new List<Id>();
        List<Linked_SR__C> listlinkedSR= new List<Linked_SR__C>();
        List<T3_Categories__c> lstT3 = new List<T3_Categories__c>();
        List<T3_Categories__c> lstT3New = new List<T3_Categories__c>();
        
        ETA__kav eta = new ETA__kav();        
        eta.Title = 'Test Class ETA';
        eta.UrlName = 'Test-Class-eta'; 
        eta.Language = 'en_US';
        eta.ValidationStatus = 'Work In Progress'; 
        eta.Audience__c =' Level 30 = Customers';
        eta.Related_Articles__c='[ArticleLink Number=123456hhjhhhggkjghhjgghjg UrlName=abcdehjg]fghijjhfdzghzkdfgjhjhjhfjgklmno';       
        eta.Notes__c='][ArticleLink6hhjhhhggkjghhjgghjg UrlName=abcdehjgfghijjhfdzghzkdfgjhjhjhfjgklmno';          
        insert eta;
        idMap.put('ETA',eta.id);
        idList.add(eta.id);
        
        //1555 changes
         Security_KB__kav secrurityArticle = new Security_KB__kav();        
        secrurityArticle.Title = 'Test Class secrurityArticle';
        secrurityArticle.UrlName = 'Test-Class-secrurityArticle'; 
        secrurityArticle.Language = 'en_US';
        secrurityArticle.ValidationStatus = 'Work In Progress'; 
        secrurityArticle.Audience__c =' Level 30 = Customers';
        secrurityArticle.Related_Articles__c='[ArticleLink Number=123456hhjhhhggkjghhjgghjg UrlName=abcdehjg]fghijjhfdzghzkdfgjhjhjhfjgklmno';       
        secrurityArticle.Notes__c='][ArticleLink6hhjhhhggkjghhjgghjg UrlName=abcdehjgfghijjhfdzghzkdfgjhjhjhfjgklmno';             
        secrurityArticle.CVE_ID__c ='sghfgjsdjhsjdhsf';
        secrurityArticle.Affected_Product_and_Versions__c = 'sdfgsjjshkjh';
        secrurityArticle.Summary__c ='sfdshdjfhksj';
        secrurityArticle.Link_to_advisories__c = 'dfjjhsjfheh';
        insert secrurityArticle;
        
        //idMap.put('secrurityArticle',secrurityArticle.id);
        idList.add(secrurityArticle.id); 
        
        //1555 changes 
     
        T3_Categories__c t3Parent = new T3_Categories__c();
        t3Parent.T3_ID__c='11112';
        t3Parent.Term_Type__c = 'Product';
        t3Parent.Parent_T3_ID__c = '1';  
              
        insert t3Parent;

        for(Integer i=1 ; i<=2 ; i++){
            T3_Categories__c t3version = new T3_Categories__c();
            t3version.T3_ID__c = '111112'+i;
            //String.valueof(i);
            t3version.Term_Type__c = 'Version';
            t3version.Name = 'Testing Version'+i;  
            t3version.Parent_T3_ID__c=  t3Parent.T3_ID__c;          
            lstT3.add(t3version);    
        }
        insert lstT3;
            
        for(Integer i=1 ; i<=2 ; i++){
            T3_Categories__c t3versionNew = new T3_Categories__c();
            t3versionNew.T3_ID__c = '11112'+i;
            t3versionNew.Term_Type__c = 'Version';
            t3versionNew.Name = 'Testing Version'+i;  
            t3versionNew.Parent_T3_ID__c= lstT3[0].T3_ID__C;        
            lstT3New.add(t3versionNew);    
        }

        insert lstT3New;
               
        eta.T3_Categories__c=lstT3[0].T3_ID__c+','+lstT3[0].T3_ID__c;
        eta.T3_Product__c=lstT3[0].Name+','+lstT3[0].Name;
        eta.T3_Version__c=t3Parent.Name+lstT3[0].Name+','+t3Parent.Name+lstT3[0].Name;                           
        update eta;                                         
           
        List<KnowledgeArticleVersion> kav= [SELECT id,ArticleType, KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE  id in :idList];
        KbManagement.PublishingService.publishArticle(kav[0].KnowledgeArticleId, false);    
        KbManagement.PublishingService.publishArticle(kav[1].KnowledgeArticleId, false);        
                    
        Article_Summary__c articlesummary= new Article_Summary__c();
        articlesummary.Article_Number__c=kav[0].ArticleNumber;
        articlesummary.Name= '22222';
        //kav[0].ArticleNumber; 
                  
        insert articlesummary;               
                       
        Linked_SR__c linkedSR1= new Linked_SR__c();
        //@1055 - Added Article Number 
        linkedSR1.Article_Number__c=kav[0].ArticleNumber;
        linkedSR1.Article_Solved_My_Problem__c=true;        
        linkedSR1.SR_Number__c=1234567;
        linkedSR1.Article_Summary__c=articlesummary.id;
        linkedSR1.Linking_User__c=UserInfo.getUserId();
        listlinkedSR.add(linkedSR1);        
        insert linkedSR1;     
                                
        ApexPages.StandardController controllerstandard= new ApexPages.StandardController(eta);
        KB_Controller kbcontrollerstandard= new KB_Controller(controllerstandard);        
        ApexPages.currentpage().getParameters().put('Id',kav[0].KnowledgeArticleId);      
        ApexPages.KnowledgeArticleVersionStandardController controller = new ApexPages.KnowledgeArticleVersionStandardController(eta);  
        KB_Controller hwToObj1= new KB_Controller(controller); 
        
        Article_Rating__c rateArttemp=new Article_Rating__c();
        rateArttemp.Raters_Email_Address__c='abcd@abcd.abcd';
        rateArttemp.Comments__c=  'ashdhsiahefhk';
        rateArttemp.Rating__c=15;
        rateArttemp.Solved_My_Problem__c=true;        
        hwToObj1.rateArt=rateArttemp;                                  
        hwToObj1.rateArticle();
        
        //@1055 - Added Article Number
        hwToObj1.articleNo =kav[0].ArticleNumber;

        hwToObj1.linkingObj.Article_Number__c = kav[0].ArticleNumber;
        hwToObj1.reassingArticle();
        
        hwToObj1.editSR();
        hwToObj1.articleNo =kav[0].ArticleNumber;
        hwToObj1.linkingObj.SR_Number__c=1234567;
        hwToObj1.linkingObj.Article_Solved_My_Problem__c=true;                
        hwToObj1.saveSR();
        
        hwToObj1.linkingObj.SR_Number__c=null;
        hwToObj1.saveSR();
                       
        KB_Controller.insertArticle('- Testing Clone_eta',eta.id); 
        KB_Controller.insertArticle('- Testing Clone_security',secrurityArticle.id); 
         
        System.runas(internalPortal_user){      
        System.debug('========we are here======');
        ApexPages.StandardController controllerstandard1= new ApexPages.StandardController(eta);
        KB_Controller kbcontrollerstandard1= new KB_Controller(controllerstandard1);        
        ApexPages.currentpage().getParameters().put('Id',kav[0].KnowledgeArticleId);      
        ApexPages.KnowledgeArticleVersionStandardController controller1 = new ApexPages.KnowledgeArticleVersionStandardController(eta);  
        
        }             
                             
        User usr = [Select Id,FederationIdentifier  from user where FederationIdentifier !=null limit 1];
        KB_CheckUserType.chkUserType(usr.FederationIdentifier);
        KB_CheckUserType.chkUserType('asada');
        KB_CheckUserType.chkUserType(internalPortal_user.FederationIdentifier);
        test.stopTest();
         
     }  
        
       
}