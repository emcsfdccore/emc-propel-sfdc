/**
 * @author: Srikrishna SM
 * @Date: 11/03/2014
 * @Description: Test class to cover OnDeleteUpdateAssets trigger
 */
@isTest
private class R2R_UpdateAssetsOnDelete_TC {

    static testMethod void onDeleteUpdateAssetsTest() {
        CustomSettingDataHelper.eBizSFDCIntCSData();
        CustomSettingDataHelper.dataValueMapCSData();
        String campaignName = '';
        //Insert Asset Based Campaign
        Asset_Based_Campaigns__c assetBasedCamp = new Asset_Based_Campaigns__c();
        assetBasedCamp.Name = 'Year 2014';
        assetBasedCamp.Campaign_Eligibility__c = 'Eligible';
        insert assetBasedCamp;
        
        //Insert Account
        Account account = AccountAndProfileTestClassDataHelper.createSimpleAccount();
        insert account;
        
        //Insert Asset
        Asset__c testAsset = new Asset__c();
        testAsset.Name = 'Test1';
        testAsset.Customer_Name__c = account.Id;
        insert testAsset;
        
        //Insert Junction Objects
        List<Asset_Campaign_Junction__c> acJunctionList = new List<Asset_Campaign_Junction__c>();
        
        Asset_Campaign_Junction__c acjunction1 = new Asset_Campaign_Junction__c();
        acjunction1.Related_Asset__c = testAsset.Id;
        acjunction1.Related_Asset_Campaign__c = assetBasedCamp.Id;
        acJunctionList.add(acjunction1);
        
        Asset_Campaign_Junction__c acjunction2 = new Asset_Campaign_Junction__c();
        acjunction2.Related_Asset__c = testAsset.Id;
        acjunction2.Related_Asset_Campaign__c = assetBasedCamp.Id;
        acJunctionList.add(acjunction2);
        
        insert acJunctionList;
        
        //Check the Asset Based Campaigns field in Asset Object
        
        List<Asset__c> assetList1 = [Select Id, Name, Asset_Based_Campaigns__c from Asset__c where id =:testAsset.Id];
        if(!assetList1.isEmpty()){
            campaignName = assetList1[0].Asset_Based_Campaigns__c;
        }
        
        System.assertEquals('Year 2014,Year 2014', campaignName);
        
        //Delete Asset Based Campaigns
        Test.startTest();        
        delete assetBasedCamp;
        Test.stopTest();
    }
}