public class PRM_BPP_Disti_VAR_Compliance{
    
    public Transient List<List<Velocity_Rule_Results__c>> varResultsListParent;
    public Transient List<Velocity_Rule_Results__c> varResultsList;
    public Transient List<Account> accountRecord;
    User userRecord;
     
    public PRM_BPP_Disti_VAR_Compliance(){
        
    }
    
    public PRM_BPP_Disti_VAR_Compliance(ApexPages.StandardController controller){
        
    }
    
    public List<List<Velocity_Rule_Results__c>> getVarAccounts() {
        userRecord = [Select u.Id,u.ContactId,u.Contact.AccountId, u.Profile.Name from User u where u.Id=:UserInfo.getUserId()]; 
        accountRecord = [Select a.Id,a.Name From Account a where a.Preferred_Distributor__c=:userRecord.Contact.AccountId and Partner_Type__c excludes ('Distributor','Direct Reseller') Limit 5000];
        varResultsListParent = new List<List<Velocity_Rule_Results__c>>();
        varResultsList = new List<Velocity_Rule_Results__c>();
        if(userRecord.Profile.Name.containsIgnoreCase('AMER Distributor')){ 
            for (Velocity_Rule_Results__c velRuleResult : [SELECT AccountID__r.Name,AccountID__r.Theater1__c,Speciality_RuleID__r.Tier__c, VS_Rule_Result_Display_Name__c, RequiredCount__c, Current__c, Status__c FROM Velocity_Rule_Results__c WHERE Speciality_RuleID__r.Tier__c in ('Silver','Gold','Platinum') and AccountID__c IN :accountRecord limit 45000]){
                varResultsList.add(velRuleResult);
                if(varResultsList.size() == 5000) {
                    varResultsListParent.add(varResultsList);
                    varResultsList = new List<Velocity_Rule_Results__c>();
                }
            }
            varResultsListParent.add(varResultsList);
        }
        else{
            for (Velocity_Rule_Results__c velRuleResult : [SELECT AccountID__r.Name,AccountID__r.Theater1__c,Speciality_RuleID__r.Tier__c, VS_Rule_Result_Display_Name__c, RequiredCount__c, Current__c, Status__c FROM Velocity_Rule_Results__c WHERE Speciality_RuleID__r.Tier__c in ('Silver','Gold','Platinum') and required_value_type__c not in ('USD', 'YEN') and AccountID__c IN :accountRecord limit 45000]){
                varResultsList.add(velRuleResult);
                if(varResultsList.size() == 5000) {
                    varResultsListParent.add(varResultsList);
                    varResultsList = new List<Velocity_Rule_Results__c>();
                }
            }
        }
        return varResultsListParent;
    }
    
    public pagereference ExportVarCompliance(){
        return new PageReference('/'+'apex/PRM_BPP_Disti_Var_Compliance_Export');
    }

}