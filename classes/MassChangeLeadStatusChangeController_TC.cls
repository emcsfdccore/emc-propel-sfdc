/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE       DEVELOPER       WR       DESCRIPTION                                
|
   ====       =========       ==       ===========  
|  03-Apr-2013  Prachi                 Created 
+===========================================================================*/ 
@isTest(SeeAllData = true) 
private class MassChangeLeadStatusChangeController_TC {
         public static List <Lead> leadLst= new List <Lead> ();
         public void createLead(){
     for(Integer k=0;k<3;k++){ 
        Lead leadObj = new Lead();
        leadObj.Company = 'Company'+k;
        leadObj.Lead_Originator__c = 'Field';
            //leadObj.CurrencyIsoCode='EUR - EURO';
            leadObj.LastNameLocal='last name'+k;
            leadObj.LastName='last'+k;
            leadObj.LeadSource='Manual';
            leadObj.Status='New';
            leadObj.Sales_Force__c='EMC';
        leadLst.add(leadObj);
     }
        Database.insert(leadLst);
        System.debug('lead Created'); 
        
     }
static testMethod void MassChangeLeadStatusChangeController(){
    
    Test.StartTest(); 
    //leadObj=[Select id from Lead limit 10];
    MassChangeLeadStatusChangeController_TC objTC=new MassChangeLeadStatusChangeController_TC();
    objTC.createLead();
    ApexPages.StandardSetController sc = new ApexPages.StandardSetController(leadLst);
    sc.setSelected(leadLst);
    MassChangeLeadStatusChangeController objC = new MassChangeLeadStatusChangeController(sc);
    objC.newStatus ='Closed';
    objC.checkSelectedStatus();
    objC.customSave();
    ApexPages.StandardSetController sc1 = new ApexPages.StandardSetController(leadLst);
    MassChangeLeadStatusChangeController objC1 = new MassChangeLeadStatusChangeController(sc1);
    objC1.checkSelectedStatus();
    Test.StopTest();
}
}