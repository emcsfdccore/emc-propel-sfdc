/*==================================================================================================================+                                                                          
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        ===========  
 |  Dec/17/2014   Bisna V P    CI 1127     Updated to solve -Find and Replace tool discrepancy 
 |  Jan/15/2015   Bisna V P    CI 1631     Updated Find and Replace tool UI and Email Notifications enhancements
 ====================================================================================================================*/
global class BatchKnowledgeGlobalReplace implements Database.Batchable<sObject>, Database.Stateful {

    Private Integer nBatchSize = 100;
    Private String strApexBatchJobId = '';    
    Private String strSearchJobId = '';
    Private String strArticleType = '';
    Private String strPublishStatus = '';
    Private Boolean bIsMasterLanguage = true;   
    Private String strSearchString = '';
    Private String strReplacementString = '';
    Private String strFieldNames = '';
    Private integer nMaxSizePlainTextBody = 16384; // Arbitrary - Have no idea how large the plain text body can be    
    public Integer UpdateCount = 0;
    public Integer TotalCount = 0;
    public String  strReplacementLog = '';       
    public Boolean publish;
    
    global BatchKnowledgeGlobalReplace(String strSearchJobId, Boolean publish) { 
        this.strSearchJobId = strSearchJobId;               
        this.strReplacementLog = '';
        this.publish = publish;     
        System.debug('strSearchJobId='+strSearchJobId);    
    } 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        this.strApexBatchJobId = BC.getJobId();
        String strQuery = 'SELECT Id from KB_Global_Search_And_Replace__c';
        strQuery += ' WHERE Apex_BatchId__c = \''+this.strSearchJobId+'\' ';
        strQuery += 'AND Replacement_Requested__c= true';
        //1631 change - added condition in query: 'AND Replacement_Requested__c= true'
        return database.getquerylocator(strQuery);   
           
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        // Record Ids are concatenated to allow all replacement tasks
        // for this batch scope to be returned in a single SOQL query       
        String strScopeIds = '';
        for (sObject scopeObject : scope) {
            if (strScopeIds.length()>0) strScopeIds += ',';
            strScopeIds += '\''+(String)scopeObject.get('Id')+'\'';
        }        
        
        // Base SOQL construct
        String strQuery = '';
        strQuery += 'SELECT Id, ArticleId__c, KnowledgeArticleId__c, Article_Type__c, ';         
        strQuery += ' PublishStatus__c,  ';
        strQuery += ' Search_String__c, Replacement_String__c, Field_Names__c '; 
        strQuery += ' FROM KB_Global_Search_And_Replace__c';          
        strQuery += ' WHERE Id IN ('+strScopeIds+')';

        List <sObject> tasks = new List<sObject>();
        tasks = Database.query(strQuery);
        
        try {
            globalReplace(tasks);
        } catch (Exception ex){
            String errMsg = ex.getMessage();
            system.Debug(errMsg);
        } 
    }
    
    public void globalReplace(List<sObject> searchAndReplaceTasks){
        for(sObject task: searchAndReplaceTasks) {
            this.TotalCount++;
            
            String strTaskId = task.Id;
            String strArticleId = (String)task.get('ArticleId__c');
            String strKnowledgeArticleId = (String)task.get('KnowledgeArticleId__c');
            this.strArticleType = (String)task.get('Article_Type__c');       
            this.strFieldNames = (String)task.get('Field_Names__c');
            this.strPublishStatus = (String)task.get('PublishStatus__c');                    
            List <String> listFieldNames = this.strFieldNames.split(',');
            
            // Create new draft of article to be updated (if required)
            String strDraftId = '';
            String strQuery = '';
            String strArticleNumber = '';
            String strArticleTitle = '';
            
            if (this.strPublishStatus.equals('Online')) {
                // Determine if draft of published article already exists
                strQuery = 'SELECT Id, ArticleNumber, Title';
                strQuery += ' FROM '+this.strArticleType;
                strQuery += ' WHERE KnowledgeArticleId = \''+strKnowledgeArticleId +'\' ';
                strQuery += ' AND PublishStatus = \'Draft\' ';

                List <sObject> listExistingDrafts = Database.query(strQuery); 
                
                if (listExistingDrafts != null && listExistingDrafts.size() >= 1) {
                    strDraftId = listExistingDrafts[0].Id;
                    strArticleNumber = (String)listExistingDrafts[0].get('ArticleNumber');
                    strArticleTitle = (String)listExistingDrafts[0].get('Title');
                    
                    this.strPublishStatus = 'Draft';
                    strReplacementLog += 'Draft exists for article '+strArticleNumber+' Title='+strArticleTitle+' (will be kept as Draft)\n';  
                } else {
                    
                        strDraftId = KbManagement.PublishingService.editOnlineArticle(strKnowledgeArticleId, false);
                     
                }
            } else {
                strDraftId = strArticleId;
            }

            // Get new (or existing) draft of article
            strQuery = 'SELECT ArticleNumber, ';
            if (this.strFieldNames.toLowerCase().indexOf('title')<0) strQuery += 'Title, ';
            strQuery += this.strFieldNames;
            strQuery += ' FROM '+this.strArticleType;
            strQuery += ' WHERE Id = \''+strDraftId+'\'';
            
            List <sObject> listDrafts;
            try {
                listDrafts = Database.query(strQuery);
            } catch (Exception ex){
                String errMsg = ex.getMessage();
                system.Debug(errMsg);
            }               
            
            // Iterate through fields and perform string replacement(s)
            if (listDrafts != null && listDrafts.size() == 1) {
                sObject draft = listDrafts[0];
                
                strArticleNumber = (String)draft.get('ArticleNumber');
                strArticleTitle = (String)draft.get('Title');
                
                this.strSearchString = (String)task.get('Search_String__c');
                this.strReplacementString = (String)task.get('Replacement_String__c');
                
                System.debug('*** Batch Knowledge Global Replace *** Search String='+this.strSearchString);
                System.debug('*** Batch Knowledge Global Replace *** Replacement String='+this.strReplacementString);
                
                String strRegEx = this.strSearchString;
                Pattern strSearchStringPattern = Pattern.compile(strRegEx);
                              
                for (String strFieldName: listFieldNames) {
                    String strFieldValue = (String)draft.get(strFieldName);
                    
                    System.debug('*** Batch Knowledge Global Replace *** Field Name='+strFieldName);
                    System.debug('*** Batch Knowledge Global Replace *** Field Value='+strFieldValue);
                    
                    Matcher matcher = strSearchStringPattern.matcher(strFieldValue);

                    draft.put(strFieldName, matcher.replaceAll(this.strReplacementString));
                    
                    System.debug('*** Batch Knowledge Global Replace *** Field='+strFieldName+' New Value='+draft.get(strFieldName));
                }
                
                Database.SaveResult results;
                try {
                    this.UpdateCount++;
                    results = Database.update(draft);
                } catch (Exception ex){
                    String errMsg = ex.getMessage();
                    system.Debug(errMsg);
                }                   
                        
                // Republish updated Draft article (if article was originally published)
                if (results.isSuccess()) {
                    if (strPublishStatus.equals('Online') || publish==true) {
                        if ( !bIsMasterLanguage) {
                            // Mark translation as complete - this actually publishes the translation
                            KbManagement.PublishingService.completeTranslation(strDraftId);  
                        } else {                        
                            KbManagement.PublishingService.publishArticle(strKnowledgeArticleId, true);
                        } 
                        
                        strReplacementLog += 'Modified and republished article '+strArticleNumber+' Title='+strArticleTitle+'\n';                       
                    } else {
                        strReplacementLog += 'Modified and retained draft article '+strArticleNumber+' Title='+strArticleTitle+'\n';
                    }
                } else {
                    strReplacementLog += 'Unable to update draft article '+strArticleNumber+' Title='+strArticleTitle+'\n';
                }
            } 
        }
    }
    
    
    global void finish(Database.BatchableContext BC){
        // Clean up - Delete all replacement tasks records from work queue
        /* 1631 changes, commenting this part
        List <KB_Global_Search_And_Replace__c> listGSR = [select Id from KB_Global_Search_And_Replace__c where Apex_BatchId__c = :this.strSearchJobId];
        try {
            //Database.delete(listGSR);  
        } catch (Exception ex){
            String errMsg = ex.getMessage();
            system.Debug(errMsg);
        } */
        
        String strQuery = '';
        List<KB_Global_Search_History__c> listSearchStatus = new List<KB_Global_Search_History__c>();
        
        strQuery += 'SELECT Id, Articles_Searched__c, Articles_Updated__c';
        strQuery += ' FROM KB_Global_Search_History__c';
        strQuery += ' WHERE Apex_Replacement_BatchId__c LIKE \'%'+this.strApexBatchJobId.substring(0,15)+'%\' ';
        
        try {
            listSearchStatus = Database.query(strQuery);     
        } catch (Exception ex){
            String errMsg = ex.getMessage();
            system.Debug(errMsg);
        }                   

        if (listSearchStatus != null && listSearchStatus.size() > 0) {
            KB_Global_Search_History__c searchStatus = listSearchStatus[0];
            
            searchStatus.Articles_Updated__c = this.UpdateCount;
            this.TotalCount = Integer.valueOf(searchStatus.Articles_Searched__c);
            
            try {
                Database.update(searchStatus);
            } catch (Exception ex){
                String errMsg = ex.getMessage();
                system.Debug(errMsg);
            }               
        }
                                
        // Query the AsyncApexJob object to retrieve the current job's metadata.
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
           TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :this.strApexBatchJobId];             
           
        // Email the Batch Job's submitter that the Job is finished.
        if (strReplacementLog.length()>nMaxSizePlainTextBody) {
            strReplacementLog = strReplacementLog.substring(0,nMaxSizePlainTextBody) + '...truncated';
        }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Salesforce Knowledge Global Search and Replace-Replacement Complete');
        mail.setHTMLBody(
        
         '<p>Article Type='+this.strArticleType+'<br></br>'+
         'Publish Status='+this.strPublishStatus+'<br></br>'+
         'Search Fields='+this.strFieldNames+'<br></br>'+
         'Search String='+this.strSearchString+'<br></br>'+
         'Replacement String='+this.strReplacementString+'<br></br><br></br></p>'+
         '<p>Total number of articles searched='+this.TotalCount+'<br></br>'+
         'Total number of articles updated='+this.UpdateCount+'<br></br><br></br></p>'+
         '<p>Salesforce.com Job Statistics:<br></br>'+
         'Number of Errors='+a.NumberOfErrors+'<br></br>'+
         'Status='+a.Status+'<br></br><br></br></p>'+
         'Replacement Log:'+'<br></br>'+
         'Please '+
          '<a href="'+system.label.KBViewStatusSearchDetail+this.strSearchJobId+'">Click Here</a>' +
           ' To View Articles Identified for Replacement:<br></br>'
           //1631 changes, commented strReplacementLog & added link to the search status page.
         //strReplacementLog         
         );
          
          
        if (!Test.isRunningTest()) {  
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}