/*==========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER         WR          DESCRIPTION                               
 |  ====            =========         ==          =========== 
 |  14.01.2013      Anirudh Singh     219167       This class will be used to prcess the detailed product depending upon the
 |                                                 trigger invokation.
 |  11 Jun 2014     Vivek Barange     989         One of the competency must be mandatory On opportunity- Detailed Product.
 |  14-April-2015   Vinod Jetti       1849          Created populatePortfolio method             
 +==============================================================================================================================*/
 
 
public class EMC_Consulting_Operations {
   public static boolean isAlreadyExecuted = false;
   public static Set<String> OPPTY_STAGES = new Set<String>{'Upside', 'Strong Upside', 'Commit', 'Booked'};
   public static String PS_CONSULTING_PRODUCT = 'PS CONSULTING';
  
  /*@Method <This method will be used to calculate the Total Competency($) value>
  @param <This method takes list<Detailed_Product__c> from BeforeUpdateInsertDetailedProduct Trigger>
  @return <void> - <Not Returning anything>
  @throws exception - <No Exception>
  */
    public void calculateTotalCompetency(List<Detailed_Product__c> lstDetailedProduct)
    {
        for (Detailed_Product__c detailProdObj :lstDetailedProduct){                          
             if(detailProdObj.Sub_Practice_1_Dollar__c !=null){
                detailProdObj.Total_Competency__c = detailProdObj.Sub_Practice_1_Dollar__c;
                detailProdObj.Is_Total_Competency_Updated__c = true;
             }
             if(detailProdObj.Sub_Practice_1_Dollar__c ==null ){
                detailProdObj.Total_Competency__c = null;
                detailProdObj.Is_Total_Competency_Updated__c = false;
             }
             if(detailProdObj.Sub_Practice_2_Dollar__c !=null){
                if(detailProdObj.Total_Competency__c == null  ){
                   detailProdObj.Total_Competency__c =  detailProdObj.Sub_Practice_2_Dollar__c; 
                } 
                else if(detailProdObj.Total_Competency__c !=null){
                    detailProdObj.Total_Competency__c +=  detailProdObj.Sub_Practice_2_Dollar__c;
                }                  
                detailProdObj.Is_Total_Competency_Updated__c = true;
             }
             if(detailProdObj.Sub_Practice_2_Dollar__c ==null ){
                detailProdObj.Total_Competency__c = detailProdObj.Total_Competency__c;                
             }
             if(detailProdObj.Sub_Practice_3_Dollar__c !=null){
                 if(detailProdObj.Total_Competency__c == null  ){
                   detailProdObj.Total_Competency__c =  detailProdObj.Sub_Practice_3_Dollar__c; 
                }
                else if(detailProdObj.Total_Competency__c !=null){
                    detailProdObj.Total_Competency__c +=  detailProdObj.Sub_Practice_3_Dollar__c;
                }                 
                detailProdObj.Is_Total_Competency_Updated__c = true;
             }
             if(detailProdObj.Sub_Practice_3_Dollar__c ==null ){
                detailProdObj.Total_Competency__c = detailProdObj.Total_Competency__c;                
             }
             if(detailProdObj.Sub_Practice_4_Dollar__c !=null){
                if(detailProdObj.Total_Competency__c == null  ){
                   detailProdObj.Total_Competency__c =  detailProdObj.Sub_Practice_4_Dollar__c; 
                }
               else if(detailProdObj.Total_Competency__c !=null){
                    detailProdObj.Total_Competency__c +=  detailProdObj.Sub_Practice_4_Dollar__c;
                }                 
                detailProdObj.Is_Total_Competency_Updated__c = true;
             }
             if(detailProdObj.Sub_Practice_4_Dollar__c ==null ){
                detailProdObj.Total_Competency__c = detailProdObj.Total_Competency__c;                
             }
             if(detailProdObj.Sub_Practice_5_Dollar__c !=null){
                if(detailProdObj.Total_Competency__c == null  ){
                   detailProdObj.Total_Competency__c =  detailProdObj.Sub_Practice_5_Dollar__c; 
                }
               else if(detailProdObj.Total_Competency__c !=null){
                    detailProdObj.Total_Competency__c +=  detailProdObj.Sub_Practice_5_Dollar__c;
                }                 
                detailProdObj.Is_Total_Competency_Updated__c = true;
             }
             if(detailProdObj.Sub_Practice_5_Dollar__c ==null ){
                detailProdObj.Total_Competency__c = detailProdObj.Total_Competency__c;                
             }
        }
    }
    
    /* 
    *  WR-989 : One of the competency must be mandatory On opportunity- Detailed Product
    *  Modified Date : 11 Jun 2014
    *  Description: Method is used to validate competency where atleast one competency should be mandatory if following 
    *               conditions are satisfied:
    *               - Opportunity created by CSD/MCSD (Profile = EMC Consulting User) AND
    *               - PS Consulting selected as a product on Opportunity AND
    *               - Opportunity status is "Upside" or higher (i.e. Upside, Strong Upside, Commit, Booked) AND
    *               - Competency 1 is blank
    
    */
    public static void validateCompetency(List<Detailed_Product__c> lstDetailedProduct, Set<Id> optyIds) {
        
        if(isAlreadyExecuted) return;
        isAlreadyExecuted = true;
        
        Profiles__c profile = Profiles__c.getInstance();
                
        Map<Id, Opportunity> mapOpty = new Map<Id, Opportunity>([Select StageName, Products__c, Createdby.Profile.Name,Owner.Profile.Name From 
                                                             Opportunity where Id IN :optyIds]);

        List<OpportunityLineItem> lstOptyLineItems = [Select PricebookEntry.Product2.Name, OpportunityId From OpportunityLineItem
                                                       where OpportunityId IN :optyIds and PricebookEntry.Product2.Name = :PS_CONSULTING_PRODUCT];
        Map<Id, Set<String>> mapOptyProduct = new Map<Id, Set<String>>();                                              
        
        for(OpportunityLineItem optyLineItem : lstOptyLineItems) {
            if(mapOptyProduct.containsKey(optyLineItem.OpportunityId)) {
                Set<String> products = mapOptyProduct.get(optyLineItem.OpportunityId);
                products.add(optyLineItem.PricebookEntry.Product2.Name);
                mapOptyProduct.put(optyLineItem.OpportunityId, products);
            } else {
                mapOptyProduct.put(optyLineItem.OpportunityId, new Set<String>{optyLineItem.PricebookEntry.Product2.Name});
            }
        }
        for(Detailed_Product__c detailedProduct : lstDetailedProduct) {
            if(detailedProduct.Opportunity__c == null) {continue;}
            Opportunity opty = mapOpty.get(detailedProduct.Opportunity__c);
            Set<String> selectedProducts = mapOptyProduct.get(detailedProduct.Opportunity__c);
            if(OPPTY_STAGES.contains(opty.StageName) && ((opty.Createdby != null 
      && opty.Createdby.Profile.Id == profile.EMC_Consulting_User__c) || 
        UserInfo.getProfileId().equalsIgnoreCase(profile.EMC_Consulting_User__c))
                && (detailedProduct.Sub_Practice_1__c == null || detailedProduct.Sub_Practice_1__c == '') 
                && selectedProducts != null && !selectedProducts.IsEmpty() &&
                selectedProducts.contains(PS_CONSULTING_PRODUCT)) {
                
                detailedProduct.addError(System.Label.SC_Competency_Required);
                
            }        
        }

    } 
    //Start of 1849
    /*@Method <This method will be used to populate the portfolio value>
  @param <This method takes list<Detailed_Product__c> from BeforeUpdateInsertDetailedProduct Trigger>
  @return <void> - <Not Returning anything>
  @throws exception - <No Exception>
  */
  
    public static void populatePortfolio(List<Detailed_Product__c> lstDetailedProducts)
    {
        Map<String, String> offerPortfolioMap  = new Map<String, String>();
        List<Offer_Portfolio_Mapping__c> offerPortfolioList = Offer_Portfolio_Mapping__c.getall().Values();
         for(Offer_Portfolio_Mapping__c opm : offerPortfolioList ) {
            offerPortfolioMap.put(opm.Offer__c, opm.Portfolio__c);  
        }
        String port = '';
        for(Detailed_Product__c dp : lstDetailedProducts) {
            Set<String> portfolioSet = new Set<String>();
            if(dp.Offer_aligned_to_play__c != null) {
            for(String off : dp.Offer_aligned_to_play__c.split(';')) {
                    String mappedPort = offerPortfolioMap.get(off);
                    if(!portfolioSet.contains(mappedPort)) {
                        if(offerPortfolioMap.get(off) != null) {
                            port += offerPortfolioMap.get(off)+',';
                            portfolioSet.add(mappedPort);
                            system.debug('portfolioSet---->'+portfolioSet);
                        }
                    }
                }   
                if(port.length()>0){
                port = port.substring(0, port.length()-1);
                }
            }
            dp.Portfolio__c = port;
        }
    }
    //End of 1849
}