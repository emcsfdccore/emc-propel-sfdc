/*==================================================================================================================+                                                                          
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        ===========  
 | 12/12/2014       Aarti      395546     Controller for the Tech Connect Submission Page                                                 
+==================================================================================================================**/

public class techConnSubmissionController{
	private Tech_Connect_Partner__c techCon;
	public Boolean flag;
	//Default constructor
	public techConnSubmissionController(ApexPages.StandardController controller) {
		this.techCon = (Tech_Connect_Partner__c)controller.getRecord();
	}
	//Method invoked on click of save button on Tech Connect Onboarding form
	public Pagereference SubmissionAction(){
		try{
           insert techCon;
           flag=true;
		}catch(DMLException ex){
           ApexPages.addMessages(ex);
           flag=false;
		}
		if(flag)
		{
		    PageReference pageRef = new  PageReference('/apex/TechConnectSubmissionPage');
			return pageRef ;
		}
		else
           return null;
	}
}