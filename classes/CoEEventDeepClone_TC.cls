@isTest
private class CoEEventDeepClone_TC{
    static testmethod void testCloneAll()
    {
        CoE_Event__c coeEventDummy=new CoE_Event__c();
        insert coeEventDummy;
        CoE_Customer__c coeCustomerDummy=new CoE_Customer__c();
         
        coeCustomerDummy.CoE_Event__c=coeEventDummy.Id;
        insert coeCustomerDummy;
        Id i=CoEEventDeepClone.cloneAll(coeEventDummy.Id);
        System.debug('Id returned='+i);
    }
}