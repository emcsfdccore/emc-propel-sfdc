/*=====================================================================================================+
|  HISTORY  | 
|  DATE          DEVELOPER               WR                    DESCRIPTION 
|  ====          =========               ==                    =========== 
|  12 Dec- 2013   Bisna VP              CI 164         This class is used to validate OEM partner lookup fields in opportunity object 
|  10 Feb 2014    Jaypal Nimesh         Backward Arrow  Updated salesChannel method signature & added condition to Bypass Validation WR# 340066
|  28-Mar-2014    Leonard Victor        CI April release  Updated the class in order to fix oppty mass assignment functionality
|  16-Jun-2014    Abinaya M S           CI July release  WR:1013- Bypassing the DDO validation when LeadSource is Contract Renewal and Opportunity Owner BU Attribute starting with 'CMA'.
|  19-Feb-2015    Vinod Jetti           #1649             Replaced field 'Emc_classification__c' to 'Emc_classification_Hub__c'
|  12-Feb-2015    Vivek Barange         #1693             When partner is added, make DDO check box unchecked and sales channel to  indirect
+=====================================================================================================+*/

public class OppyOEMPartnerAccounts{               
    
    public boolean verifiedOEM = false;
    //Added to fix Oppty Mass reassignment
    public Static Boolean isExecuted = false;
    public void OEMPartnerAccounts(List<Opportunity> oppor){                          
        if(!verifiedOEM){
            Set<Id> setAcc=new Set<Id>();
            Map<id,Account> mapAccount= new Map<id,Account>();
            for(Opportunity oppty: oppor)
            {
                if(oppty.OEM_Partner__c!= null)
                    setAcc.add(oppty.OEM_Partner__c);       
            }
            if(setAcc != null && setAcc.size()>0){
                mapAccount= new Map<id,Account>([select id, Type, IsPartner,Partner_Type__c from Account where id in:(setAcc)]);        
            }
            for(Opportunity opp : oppor){                 
                if(opp.OEM_Partner__c!=null){
                    if(mapAccount != null && mapAccount.size()>0){                                        
                        if(mapAccount.get(opp.OEM_Partner__c) != null){
                            Account a= mapAccount.get(opp.OEM_Partner__c);
                            if(a.IsPartner == false){
                                opp.OEM_Partner__c.addError(System.Label.SelctAcctOfTypeOEM);
                            }                    
                            else{
                                if( !a.Partner_Type__c.contains('OEM')){
                                    opp.OEM_Partner__c.addError(System.Label.SelctAcctOfTypeOEM);
                                }
                            }                                          
                        }
                    }                           
                    opp.OEM_Partner_Tracking__c = system.now(); 
                }  
            }
            verifiedOEM = true; 
        }
    }   
    public void OEMPartnerAccountsUpdate(List<Opportunity> newOpps,Map <id,Opportunity> Map_OldOpp){                          
        if(!verifiedOEM){
            Set<Id> setAcc=new Set<Id>();
            Map<id,Account> mapAccount= new Map<id,Account>();
            for(Opportunity oppty: newOpps)
            {
                if(oppty.OEM_Partner__c!= null)
                    setAcc.add(oppty.OEM_Partner__c);       
            }
            if(setAcc != null && setAcc.size()>0){
                mapAccount= new Map<id,Account>([select id, Type, IsPartner,Partner_Type__c from Account where id in:(setAcc)]);        
            }
            for(Opportunity opp : newOpps){                                 
                if(opp.OEM_Partner__c != null && Map_OldOpp.get(opp.id).OEM_Partner__c != opp.OEM_Partner__c){
                    if(mapAccount != null && mapAccount.size()>0){
                        if(mapAccount.get(opp.OEM_Partner__c) != null){
                            Account a= mapAccount.get(opp.OEM_Partner__c);
                            if(a.IsPartner == false){
                                opp.OEM_Partner__c.addError(System.Label.SelctAcctOfTypeOEM);
                            }
                            else{
                                if( !a.Partner_Type__c.contains('OEM')){
                                    opp.OEM_Partner__c.addError(System.Label.SelctAcctOfTypeOEM);
                                }
                            }                                          
                        }
                    }
                    //opp.OEM_Partner_Tracking__c = system.now();     
                }
                else if(opp.OEM_Partner__c == null  ){
                    opp.OEM_Partner_Tracking__c = null; 
                } 
                if(Map_OldOpp.get(opp.id).OEM_Partner__c != opp.OEM_Partner__c && Map_OldOpp.get(opp.id).OEM_Partner__c== null)
                    opp.OEM_Partner_Tracking__c = system.now();   
            }
            verifiedOEM = true; 
        }
    }
    //Signature updated for WR# 340066. Jaypal Nimesh - Dated: 10 Feb 2014
    public void salesChannel(List<Opportunity> opptyList, Map<Id, Opportunity> opptyOldMap){ 
        if (!Test.isRunningTest() && !isExecuted){
            Map<String,CustomSettingDataValueMap__c> DataValueMap= CustomSettingDataValueMap__c.getall();
            String emcClassifiaction = DataValueMap.get('EMC Classification').DataValue__c;
            String apiProfile = DataValueMap.get('System Admin API').DataValue__c;
            if(Userinfo.getProfileid()!=apiProfile){
                List<String> brokenValues = emcClassifiaction.split(',');
                //Map<Id,Id> mapOpptyAcc = new Map<Id,Id>();
                Set<Id> setAccount = new Set<Id>();
                System.debug('opptyList -------->'+opptyList);
                for(Opportunity oppObj : opptyList ){
                    system.debug('****'+oppObj.Accountid);
                    //mapOpptyAcc.put(oppObj.id,oppObj.Accountid);
                    setAccount.add(oppObj.Accountid);
                }
                Map<Id,Account> mapAccount= new Map<Id,Account>([Select id,EMC_Classification_Hub__c from Account where id in : setAccount]);
                //system.debug('****'+mapAccount+'**'+mapOpptyAcc);
                for(Opportunity oppSalesObj : opptyList){
                    System.debug('emcClassifiaction-------->'+emcClassifiaction);
                    System.debug('mapAccount-------->'+mapAccount);
                    System.debug('mapAccount-------->'+oppSalesObj.Accountid);
                    System.debug('(mapAccount.get(oppSalesObj.Accountid))-------->'+(mapAccount.get(oppSalesObj.Accountid)));
                    
                    if((mapAccount.get(oppSalesObj.Accountid)).EMC_Classification_Hub__c!=null){
                        System.debug('mapAccount-------->'+oppSalesObj.Accountid);
                        for(String str:brokenValues){
                            if(brokenValues!=null){
                                if(mapAccount.get(oppSalesObj.Accountid).EMC_Classification_Hub__c.containsIgnoreCase(str.trim())){
                                    //Start - #1693
                                    if(oppSalesObj.Partner__c != null || oppSalesObj.Tier_2_Partner__c  != null || oppSalesObj.Primary_Alliance_Partner__c != null || oppSalesObj.Service_Provider__c != null || oppSalesObj.OEM_Partner__c != null) {
                                        oppSalesObj.Direct_Deal_Override__c = false;
                                        oppSalesObj.Sales_Channel__c = 'Indirect';
                                    }
                                    //End - #1693
                                    if(oppSalesObj.Sales_Channel__c == 'Direct' && !oppSalesObj.Direct_Deal_Override__c && oppSalesObj.Partner__c!=null ){
                                        oppSalesObj.Sales_Channel__c = 'Indirect'; 
                                        system.debug('opptyOldMap1--->');
                                    }
                                    else if(oppSalesObj.Sales_Channel__c == 'Direct' && oppSalesObj.Direct_Deal_Override__c && oppSalesObj.Partner__c!=null){
                                        oppSalesObj.Sales_Channel__c = 'Direct'; 
                                        oppSalesObj.Partner__c = null; 
                                        system.debug('opptyOldMap2--->');
                                    }
                                    else if(oppSalesObj.Sales_Channel__c == 'Direct' && !oppSalesObj.Direct_Deal_Override__c)
                                    {   
                                        system.debug('opptyOldMap--->'+opptyOldMap);
                                        
                                        //WR:1013- Bypassing the DDO validation when LeadSource is Contract Renewal and Opportunity Owner BU Attribute starting with 'CMA'.
                                        /*string OppOwnerBUAtt = '';
                                        if(oppSalesObj.Account_Owner_BU_Attribute__c!=null){
                                            OppOwnerBUAtt = oppSalesObj.Account_Owner_BU_Attribute__c.SubString(0,3);
                                        }*/
                                        //Update scenario - Added bypass_validation__c check for Existing Oppty Update. Jaypal Nimesh  WR# 340066
                                        if(opptyOldMap != null && opptyOldMap.size() > 0 && !(oppSalesObj.bypass_validation__c || opptyOldMap.get(oppSalesObj.Id).bypass_validation__c) && !(oppSalesObj.LeadSource=='Contract Renewal' || oppSalesObj.Sales_Force__c == 'ESG' || oppSalesObj.Secondary_Alliance_Partner__c!=null || oppSalesObj.Primary_Alliance_Partner__c!=null || oppSalesObj.Service_Provider__c!=null || oppSalesObj.OEM_Partner__c!=null)){                        
                                            oppSalesObj.Sales_Channel__c = 'Indirect';                                
                                            oppSalesObj.Direct_Deal_Override__c.addError(System.Label.OppyOEMParErrorMsg);
                                        }
                                        //Insert Scenario - Added bypass_validation__c check for New Oppty Insert. Jaypal Nimesh  WR# 340066
                                        else if(opptyOldMap == null && !oppSalesObj.bypass_validation__c && !(oppSalesObj.LeadSource=='Contract Renewal' || oppSalesObj.Sales_Force__c == 'ESG' || oppSalesObj.Secondary_Alliance_Partner__c!=null || oppSalesObj.Primary_Alliance_Partner__c!=null || oppSalesObj.Service_Provider__c!=null || oppSalesObj.OEM_Partner__c!=null)){                        
                                            oppSalesObj.Sales_Channel__c = 'Indirect';                                
                                            oppSalesObj.Direct_Deal_Override__c.addError(System.Label.OppyOEMParErrorMsg);
                                        }
                                    }
                                    if(oppSalesObj.Direct_Deal_Override__c){
                                        oppSalesObj.Sales_Channel__c = 'Direct';
                                    }
                                }
                            }
                            System.debug('Sales_Channel__c-------->'+oppSalesObj.Sales_Channel__c);
                        }
                    }                   
                }
            }         
            isExecuted = true;                         
        }
    }
    
    //Created future method to reset bypass_validation__c field as part of Backward Arrow eRFC
    @future
    public static void resetOpptyBypassValidation(Set<Id> opptyIdSet){
        List<Opportunity> opptyListToUpdate = [Select Id, bypass_validation__c from Opportunity where Id IN: opptyIdSet];
        for(Opportunity opp: opptyListToUpdate){
            opp.bypass_validation__c = false;
        }       
        update opptyListToUpdate; 
    }
}