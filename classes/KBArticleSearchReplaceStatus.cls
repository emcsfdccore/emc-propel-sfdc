/*==================================================================================================================+                                                                          
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        ===========  
 |  Jan/13/2015   Bisna V P    CI 1631     Created this controller for Find and Replace tool UI and Email Notifications enhancements
 ====================================================================================================================*/

public with sharing class KBArticleSearchReplaceStatus {

public Transient List<KB_Global_Search_History__c> searchHistoryList = new List<KB_Global_Search_History__c>();
public Transient List<KB_Global_Search_And_Replace__c> searchArticleList = new List<KB_Global_Search_And_Replace__c>();
public KB_Global_Search_History__c searchHistoryListDetail{get;set;}
public Id apexBatchId {get;set;}
public integer limits = 20;

// paging
public PageManager objPageManager {get;set;}
Map<String,CustomSettingDataValueMap__c> dataValueMap = CustomSettingDataValueMap__c.getAll();
public Boolean status=true;
List<KB_Global_Search_And_Replace__c> listSearchDetails = new List<KB_Global_Search_And_Replace__c>();
List<KB_Global_Search_History__c> listHistDetails = new List<KB_Global_Search_History__c>();
 Date days90Before;




public KBArticleSearchReplaceStatus (){

    CustomSettingDataValueMap__c pageDetails = DataValueMap.get('View_All_Education');
        Integer pageSize = 200;        
        try
        {            
            pageSize  = Integer.valueOf(pageDetails.DataValue__c);
        }
        catch(Exception e)
        {
          pageSize = 200;
        } 
        objPageManager = new PageManager(pageSize);



   
    days90Before = Date.today()-90;
    System.debug('days90Before----->'+days90Before);
    searchHistoryList = new List<KB_Global_Search_History__c>();
    searchHistoryListDetail = new KB_Global_Search_History__c();
    apexBatchId= ApexPages.currentPage().getParameters().get('id');
    
    if(!Test.isRunningTest())
    searchHistoryListDetail = [select Replacement_Requested__c,Apex_BatchId__c,Apex_BatchId_List__c,Apex_Replacement_BatchId__c,CreatedDate,Articles_Searched__c,Articles_Selected__c,Articles_Updated__c,Article_Type__c,Field_Names__c,PublishStatus__c,Replacement_String__c,Search_String__c,CreatedBy.Name,Name from KB_Global_Search_History__c where Apex_BatchId__c=:apexBatchId limit 1];
    status=true;
     getsearchArticleList();
     getsearchHistoryList();
    
}


  public List<KB_Global_Search_And_Replace__c> getsearchArticleList(){

          searchArticleList=[select Replacement_Requested__c,Apex_BatchId__c, ArticleId__c,ArticleNumber__c,Title__c from KB_Global_Search_And_Replace__c where Apex_BatchId__c=:apexBatchId limit 15000];

           status=true;       
    
           return searchArticleList;
    }

      public List<KB_Global_Search_History__c> getsearchHistoryList(){

        searchHistoryList = [select Apex_BatchId__c,Replacement_Requested__c,Apex_BatchId_List__c,Apex_Replacement_BatchId__c,CreatedDate,Articles_Searched__c,Articles_Selected__c,Articles_Updated__c,Article_Type__c,Field_Names__c,PublishStatus__c,Replacement_String__c,Search_String__c,CreatedBy.Name,Name from KB_Global_Search_History__c where CreatedDate >= :days90Before   ORDER BY CreatedDate Desc limit 15000];//Group BY Apex_BatchId__c

           status=true;       
    
           return searchHistoryList;
    }




    public List<KB_Global_Search_And_Replace__c> getsearchDispArtList(){
  
        if(searchArticleList==null)
        getsearchArticleList();
        
        listSearchDetails.clear();     
        if(searchArticleList != null){        
           objPageManager.numberOfRows = searchArticleList.size();             
          for(integer i=objPageManager.startIndex;i<objPageManager.endIndex&&i<searchArticleList.size();i++){
                  listSearchDetails.add(searchArticleList.get(i));
          }
        }       
         return listSearchDetails ;   
    }

     public List<KB_Global_Search_History__c> getsearchDispHistoryList(){
  
        if(searchHistoryList==null)
        getsearchHistoryList();
        
        listHistDetails.clear();     
        if(searchHistoryList != null){        
           objPageManager.numberOfRows = searchHistoryList.size();             
          for(integer i=objPageManager.startIndex;i<objPageManager.endIndex&&i<searchHistoryList.size();i++){
                  listHistDetails.add(searchHistoryList.get(i));
          }
        }       
         return listHistDetails ;   
    }


    
    
    public Boolean getstatus(){
        System.Debug('Status----------->' +status);
        return status;
    }
}