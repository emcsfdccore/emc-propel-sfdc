/*==================================================================================================================+

 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER      WR                DESCRIPTION                               
 |  ====          =========      ==                =========== 
 |  14/11/2014   Srikrishna SM   BPP Jan Release   This Class will be used as controller for GAF BPP Visualforce Page Component.
 +==================================================================================================================*/

  public class PRM_GAF_BPP_Controller{
      //Setters and Getters
      public List<GAF__c> gaf {get; set;}
      public String  GAFDirectResellerVar{get;set;}
      public String  GAFDirectResellerVarApproved{get;set;}
      public String  GAFDistributor2015{get;set;}
      public String  GAFDistributor2015Approved{get;set;}
      public String  PANGaf2015{get;set;}
      public String  PANGaf2015Approved{get;set;}
      public String  GAFrecordtype{get;set;}
      public boolean isDirectType {get;set;}
      public boolean isDistiType {get;set;}
      public boolean isPanType {get;set;}
      public String theater{get;set;}
      public Boolean emcLoyal{get;set;}
      private GAF__c gafObj;
      //Constructor
      public PRM_GAF_BPP_Controller(ApexPages.StandardController controller){
          this.gafObj = (GAF__c)controller.getRecord();
          Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
gaf = [Select g.Business_Partner_Program_Track_Tier__c ,g.Business_Partner_Program_Track__c,g.Business_Partner_Program_Tier__c,g.Id, g.Services_Goal_Total__c, g.Services_Goal_Q4__c, g.Services_Goal_Q3__c, g.Services_Goal_Q2__c, g.Services_Goal_Q1__c, 
                   g.All_In_Goal_Total__c, g.All_In_Goal_Q4__c, g.All_In_Goal_Q3__c, g.All_In_Goal_Q2__c, g.All_In_Goal_Q1__c,
                   g.All_In_Pct_Goal_Q1__c,g.All_In_Pct_Goal_Q2__c,g.All_In_Pct_Goal_Q3__c,g.All_In_Pct_Goal_Q4__c,g.All_In_Pct_Goal_Total__c,
                   g.Product_Rebate_Rate_Q1__c,g.Product_Rebate_Rate_Q2__c,g.Product_Rebate_Rate_Q3__c,g.Product_Rebate_Rate_Q4__c,
                   g.All_In_Actual_Q1__c,g.All_In_Actual_Q2__c,g.All_In_Actual_Q3__c,g.All_In_Actual_Q4__c,g.All_In_Actual_Total__c,
                   g.Product_Actual_Q1__c,g.Product_Actual_Q2__c,g.Product_Actual_Q3__c,g.Product_Actual_Q4__c,g.Product_Actual_Total__c,
                   g.Product_Rebate_Q1__c,g.Product_Rebate_Q2__c,g.Product_Rebate_Q3__c,g.Product_Rebate_Q4__c,g.Product_Rebate_Total__c,                   
                   g.Services_Actual_Q1__c,g.Services_Actual_Q2__c,g.Services_Actual_Q3__c,g.Services_Actual_Q4__c,g.Services_Actual_Total__c,
                   g.Services_Pct_Goal_Q1__c,g.Services_Pct_Goal_Q2__c,g.Services_Pct_Goal_Q3__c,g.Services_Pct_Goal_Q4__c,g.Services_Pct_Goal_Total__c,
                   g.Services_Rebate_Rate_Q1__c,g.Services_Rebate_Rate_Q2__c,g.Services_Rebate_Rate_Q3__c,g.Services_Rebate_Rate_Q4__c,
                   g.Services_Rebate_Q1__c,g.Services_Rebate_Q2__c,g.Services_Rebate_Q3__c,g.Services_Rebate_Q4__c,g.Services_Rebate_Total__c,
                   g.Performance_Rebate_Q1__c,g.Performance_Rebate_Q2__c,g.Performance_Rebate_Q3__c,g.Performance_Rebate_Q4__c,g.Performance_Rebate_Total__c,
                   g.Other_Rebate_Q1__c,g.Other_Rebate_Q2__c,g.Other_Rebate_Q3__c,g.Other_Rebate_Q4__c,g.Other_Rebates_Total__c,
                   g.All_Rebate_Q1__c,g.All_Rebate_Q2__c,g.All_Rebate_Q3__c,g.All_Rebate_Q4__c,g.All_Rebate_Total__c,
                   g.KPO_1_Q1__c,g.KPO_1_Q2__c,g.KPO_1_Q3__c,g.KPO_1_Q4__c,g.KPO_2_Q1__c,g.KPO_2_Q2__c,g.KPO_2_Q3__c,g.KPO_2_Q4__c,g.KPO_3_Q1__c,g.KPO_3_Q2__c,g.KPO_3_Q3__c,g.KPO_3_Q4__c,
                   g.All_In_Rebate_Rate_Q1__c, g.All_In_Rebate_Rate_Q2__c,g.All_In_Rebate_Rate_Q3__c,g.All_In_Rebate_Rate_Q4__c,
                   g.Pan_All_In_Goal__c, g.Pan_Services_Goal__c, g.Pan_Product_Total__c, g.Pan_Product_Rebate_Total__c,g.Pan_All_In_Pct_Goal_Total__c, g.Pan_All_In_Rebate_Rate__c,
                   g.Pan_Services_Pct_Goal_Total__c, g.Pan_Services_Rebate_Rate__c, g.Pan_Services_Rebate__c,
                   g.Annual_Performance_Rebate__c, g.Other_Annual_Rebate_Total__c, g.Total_Annual_Rebate__c, g.Disti_All_In_Actual_Q1__c,g.Disti_All_In_Actual_Q2__c,
                   g.Disti_All_In_Actual_Q3__c,g.Disti_All_In_Actual_Q4__c,g.Disti_Services_Actual_Q1__c,g.Disti_Services_Actual_Q2__c,g.Disti_Services_Actual_Q3__c,
                   g.Disti_Services_Actual_Q4__c,g.Disti_Product_Actual_Q1__c,g.Disti_Product_Actual_Q2__c,g.Disti_Product_Actual_Q3__c,g.Disti_Product_Actual_Q4__c,
                   g.Disti_Services_Actual_Total__c,g.Dist_Product_Actual_Total__c,g.Disti_All_In_Actual_Total__c,g.Disti_All_In_Pct_Goal_Q1__c,g.Disti_All_In_Pct_Goal_Q2__c,
                   g.Disti_All_In_Pct_Goal_Q3__c,g.Disti_All_In_Pct_Goal_Q4__c,g.Disti_Services_Pct_Goal_Q1__c,g.Disti_Services_Pct_Goal_Q2__c,g.Disti_Services_Pct_Goal_Q3__c,
                   g.Disti_Services_Pct_Goal_Q4__c,g.Disti_Services_Pct_Goal_Total__c,g.Disti_All_In_Pct_Goal_Total__c,g.Partner_Name__r.Name, g.GAF_Start_Date__c,
                   g.Partner_Type__c, g.GAF_End_Date__c, g.Velocty_Srvce_Providr_Tier__c, g.GAF_Submission_Date__c, g.Partner_GAF_Approver__c, g.GAF_Approval_Date__c,
                   g.GAF_Goal_Level__c, g.Year__c, g.primary_currency__c, g.GAF_Status__c, g.GAF_Revenue_Rebate_Record__c, g.GAF_Revenue_Rebate_Target__c, g.Revenue_Refresh_Date__c,
                   g.Partner_GAF_Approver__r.Name, g.GAF_Disclaimer_Details__c, g.GAF_Disclaimer__c, g.RecordTypeId, g.Partner_Name__r.Theater1__c,
                   g.EMC_True_Goal__c, g.EMC_True_Rebate__c, g.EMC_True_Rebate_Rate__c, g.Partner_Name__r.Additional_Incentive_Programs__c  
                   From GAF__c g where Id =: gafObj.Id]; 
                   
                if(gaf[0].RecordTypeId!=null){
                    GAFrecordtype=gaf[0].RecordTypeId;
                    GAFDirectResellerVar=DataValueMap.get('GAFDirectResellerVar').DataValue__c;
                    GAFDirectResellerVarApproved=DataValueMap.get('GAFDirectResellerVarApproved').DataValue__c;
                    GAFDistributor2015=DataValueMap.get('GAFDistributor2015').DataValue__c;
                    GAFDistributor2015Approved=DataValueMap.get('GAFDistributor2015Approved').DataValue__c;
                    PANGaf2015=DataValueMap.get('PANGaf2015').DataValue__c;
                    PANGaf2015Approved=DataValueMap.get('PANGaf2015Approved').DataValue__c;
                }
                
                /*     
                if(GAFrecordtype == GAFDirectResellerVar || GAFrecordtype == GAFDirectResellerVarApproved){                           
                    isDirectType = true;
                }
                
                if(GAFrecordtype == GAFDistributor2015 || GAFrecordtype == GAFDistributor2015Approved){                           
                    isDistiType = true;
                }
                
                if(GAFrecordtype == PANGaf2015 || GAFrecordtype == PANGaf2015Approved){                           
                    isPanType = true;
                }
                */
                 
                theater= gaf[0].Partner_Name__r.Theater1__c;
                if(gaf[0].Partner_Name__r.Additional_Incentive_Programs__c != null){
                	emcLoyal = true;
                }else{
                	emcLoyal = false;
                }
                
      }  
      
  }