/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE       DEVELOPER       WR       DESCRIPTION                                
|
   ====       =========       ==       ===========  
|  04-Apr-2013  Prachi                 Created 
+===========================================================================*/
@isTest(SeeAllData = true) 
private class Opp_SendEmailToDistributionList_TC { 
        // public static String DEFAULT_QUERY=''; 
static testMethod void Opp_SendEmailToDistributionList(){
    
    Test.StartTest(); 
    Opp_SendEmailToDistributionList obj=new Opp_SendEmailToDistributionList();
    //Opp_SendEmailToDistributionList.DEFAULT_QUERY='Select id,Status__c from Opportunity_Integration_Log__c where Status__c = \'Open\' Limit 1';
    Database.BatchableContext bc;
    List<SObject> st=new List<SObject>();
    Database.QueryLocator q;
    q=obj.start(bc);
    obj.execute(bc,st);
    obj.finish(bc);     
    Test.StopTest();
}
}