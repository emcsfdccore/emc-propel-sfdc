/*==================================================================================================================+

 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        =========== 
 |  16/07/2014    Srikrishna SM       This Test class is used Test Functionality of Article MetaData Handler Controller
 +==================================================================================================================**/
@isTest
private class ESKB_ArticleMetadataHandler_TC{
    static testMethod void artilceMetadataTest1() {
        ApexPages.currentPage().getParameters().put('articleNumber', '123');
        ApexPages.currentPage().getParameters().put('Title', 'Test 123');
        
        List<Article_Metadata__c> metadataList = new List<Article_Metadata__c>();
        Article_Metadata__c metadata1 = new Article_Metadata__c();
        metadata1.Archive_Candidate__c = 'Yes';
        metadata1.Archive_Comments__c = 'Archive This Article';
        metadata1.Article_Number__c = '123';
        metadata1.Article_Title__c = 'Test 123';
        metadataList.add(metadata1);
               
        insert metadataList;
        
        ESKB_ArticleMetadataHandler mHandler = new ESKB_ArticleMetadataHandler();
                
        mHandler.quickSave();
        mHandler.cancel();
        
        List<Article_Metadata__c> mList = [Select Id, Archive_Candidate__c, Archive_Comments__c, Article_Number__c, Article_Title__c from Article_Metadata__c LIMIT 1];
        if(!mList.isEmpty()){
            for(Article_Metadata__c amData : mList){
                System.assertEquals(amData.Archive_Comments__c, 'Archive This Article');
                System.assertEquals('123', amData.Article_Number__c);
                System.assertEquals('Test 123', amData.Article_Title__c);
            }
        }
    }
    static testMethod void artilceMetadataTest2() {
        ApexPages.currentPage().getParameters().put('articleNumber', '123');
        ApexPages.currentPage().getParameters().put('Title', 'Test 123');
        ESKB_ArticleMetadataHandler mHandler = new ESKB_ArticleMetadataHandler();
                
        mHandler.quickSave();
        
        List<Article_Metadata__c> mList = [Select Id, Archive_Candidate__c, Archive_Comments__c, Article_Number__c, Article_Title__c from Article_Metadata__c LIMIT 1];
        if(!mList.isEmpty()){
            for(Article_Metadata__c amData : mList){
                System.assertEquals('123', amData.Article_Number__c);
                System.assertEquals('Test 123', amData.Article_Title__c);
            }
        }
    }
}