/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER               WR          DESCRIPTION                               
 |  ====            =========               ==          =========== 
 |  27 Dec 2013      Bhanu Prakash        WR319143      Initial Creation.
 | 06 Mar 15        Jaypal Nimesh   PI March    Commented class as part of PI code clean up.
 +===========================================================================*/

@isTest
private class AccountPartnerSEFieldsUpdateBatch_TC {

     static testMethod void AccountPartnerSEFieldsUpdateBatch(){
     /*
     
         System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        }   
        
        
        Account acc1 = new Account();
        acc1.name = 'TestAccName';
        acc1.PROFILED_ACCOUNT_FLAG__c = true;
        acc1.rating_eligible__c = true;
        insert acc1;
        
        Contact con1 = new Contact();
        con1.LastName = 'TestContact';
        con1.Account_Name_Local__c = acc1.Id;
        con1.partner_se__c = true;
        insert con1;
        
         String queryAccount ='select id,grouping__c,PROFILED_ACCOUNT_FLAG__c,Partner_Leverage_Factor_Average__c,'+
                              'Partner_Leverage_Factor_Total__c,name,theater1__c,velocity_Solution_Provider_Tier__c,'+
                              '(select contact.id,contact.Partner_Leverage_Factor__c from account.contacts where contact.partner_se__c=true)'+
                              ' from account where PROFILED_ACCOUNT_FLAG__c=true and rating_eligible__c=true limit 1'; 
          Test.startTest();
           Database.executebatch(new AccountPartnerSEFieldsUpdateBatch(queryAccount),200);
          Test.stopTest(); 
          */
     }
     
   /*  static testMethod void test() {
         Account acc1 = new Account();
        acc1.name = 'TestAccName';
        acc1.PROFILED_ACCOUNT_FLAG__c = true;
        acc1.rating_eligible__c = true;
        insert acc1;
        
        Contact con1 = new Contact();
        con1.LastName = 'TestContact';
        con1.Account_Name_Local__c = acc1.Id;
        con1.partner_se__c = true;
        insert con1;
        String queryAccount ='select id,grouping__c,PROFILED_ACCOUNT_FLAG__c,Partner_Leverage_Factor_Average__c,Partner_Leverage_Factor_Total__c,name,theater1__c,(select contact.id,contact.Partner_Leverage_Factor__c from account.contacts where contact.partner_se__c=true) from account where PROFILED_ACCOUNT_FLAG__c=true and rating_eligible__c=true and id=\'' + acc1.id + '\' order by theater1__c LIMIT 10' ;  
        AccountPartnerSEFieldsUpdateBatch accountBatch= new AccountPartnerSEFieldsUpdateBatch(queryAccount);
        database.executebatch(accountBatch);
        
       // testObj.deletePartnerPerformanceRecords(lstPartnerPerfRecordsInserted);       
        Test.stopTest();
    }*/
}