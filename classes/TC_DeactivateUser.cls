/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER     WR        DESCRIPTION                               
 |  ====            =========     ==        =========== 
 |  13 Dec 2011     Shipra      177094      Test Class.SFA - Remove inactive users from Account Team.
 |  03 Nov 2012     Vivekananda 204934      Test Clsss.SFA - Test Class for To_DeleteInactiveAccountTeamMemb.
 |  10 Oct 2013     Srikrishna              This test class is modified to improve the performance
 |  04 Dec 2014     Bhanuprakash    PROPEL 	Part of E.002, Replaced 'Sales-Sales Rep’ with ‘Enterprise Sales Rep’   
  +===========================================================================*/

@isTest
private class TC_DeactivateUser 
{
    static testMethod void testDeleteInactiveAccountTeamMembers(){
        
        //Create Custom Setting records for the test class
        CustomSettingDataHelper.dataValueMapCSData();
        CustomSettingDataHelper.eBizSFDCIntCSData();
        //end of custom settings
        
        RunInactiveATbatch__c RIA = new RunInactiveATbatch__c();
        RIA.Name ='FlagStatus';            
        RIA.StatusFlag__c = true;
        Database.insert(RIA,false); 
        
        ATMLimit__c atlmt = new ATMLimit__c(LimitNumber__c ='100');
        Database.insert(atlmt,false);
        
        User testUser;
        
        //fetch System Admin User
        User sysAdminUser = [SELECT  Id, Name FROM User WHERE profile.name='System Administrator' and isActive=true limit 1];
        
        system.runAs(sysAdminUser){
            ID profileId = [ Select id from Profile where name ='Business Admin' limit 1].id;
            UserRole NewusrRole1 = new UserRole(Name='TestRole756');
            insert NewusrRole1;
            testUser = UserProfileTestClassDataHelper.createUser(profileId, NewusrRole1.Id);
            insert testUser;    
        }
        
        Id uId = testUser.Id;
        
        Account accObj = new Account();
        accObj.Name ='TestAccount';        
        insert accObj;        
        
        //Insert AccountTeamMember records
        AccountTeamMember objAccTeam = new AccountTeamMember();
        objAccTeam.AccountId = accObj.Id;
        objAccTeam.TeamMemberRole ='Enterprise Sales Rep';
        objAccTeam.UserId=testUser.Id;
        
        insert objAccTeam;
        
        System.runAs(sysAdminUser)
        {
            User updateUsr = [Select id, IsActive  from User where Id = :testUser.Id];
            updateUsr.IsActive = false; 
            update updateUsr;   
        }
        
        To_DeleteInactiveAccountTeamMemb objTest = new To_DeleteInactiveAccountTeamMemb();
        objTest.query ='Select a.AccountAccessLevel, a.AccountId, a.Id, a.LastModifiedDate, a.TeamMemberRole, a.UserId, a.User.IsActive, a.User.LastModifiedDate, a.User.Name from AccountTeamMember a where a.User.IsActive = false LIMIT 1';       
        
        System.Test.startTest(); 
        id  batchid = Database.executeBatch(objTest);        
        System.Test.stopTest();
        
        RIA.StatusFlag__c = false;
        Database.update(RIA,false);
    } 
}