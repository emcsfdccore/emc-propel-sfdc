@istest

public class PSC_Request_Escalation_Ctrl_TC
{
    public static testmethod void PSCReqEscTest()
    {
        System.runAs(new user(Id = UserInfo.getUserId()))
            {    
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
                CustomSettingDataHelper.dealRegistrationCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.partnerBoardingCSData();
                CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
                CustomSettingDataHelper.VCEStaticCSData();
                CustomSettingDataHelper.PSCCaseTypeImageData();
                CustomSettingDataHelper.PSCCaseTypeImageSiteData();
                CustomSettingDataHelper.PSCFieldMappingData();
                CustomSettingDataHelper.PSCCasesLanguageData();
           }
           
           List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where RecordType.Name = 'PSC - Business Partner Program' LIMIT 1];

        list<case> listcase = new list<case>();
        if(recordType !=null && recordType.size()>0){
            listcase.add(new case( Origin ='Community', STATUS = 'New', parentid = null, SUBJECT='testcase0', DESCRIPTION = 'Testing description',  RecordTypeId = recordType[0].id));
            listcase.add(new case( Origin ='Community', STATUS = 'New', parentid = null, SUBJECT='testcase1', DESCRIPTION = 'Testing description',RecordTypeId = recordType[0].id));
        }
        if(listcase !=null && listcase.size()>0){
            insert listcase;        
            Listcase[0].Escalation_Request__c = true;
            listcase[0].Escalation_Justification__c = 'Test Escalation';
            ListCase[1].Escalation_Justification__c = 'Test Escalation 1';
            update listcase;
            }
        ApexPages.StandardController obj1 = new ApexPages.StandardController(listCase[0]);
      PSC_Request_Escalation_Ctrl ReqEsc = new PSC_Request_Escalation_Ctrl(obj1);
    ReqEsc.SaveAction();
                ApexPages.StandardController obj2 = new ApexPages.StandardController(listCase[1]);
      PSC_Request_Escalation_Ctrl ReqEsc1 = new PSC_Request_Escalation_Ctrl(obj2);
    ReqEsc1.SaveAction();
    }
}