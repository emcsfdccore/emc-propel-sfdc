@isTest

/*===========================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER                WR        DESCRIPTION                               

 |  ====          =========                ==        =========== 

 | 02/02/2012      Anil Sure                         Fixed Test class for Feb'12 Release and Increased Coverage
   6 April 2012    Arif                              Commented 'removeGroupingFromAccount' method call.
 | 18/02/2015      Vinod Jetti            #1649      created 'Hub_Info__c' data instead of Account field 'Site_Duns_Entity__c'.
================================================================================*/
private class PRM_GroupingOfAssignment_TC{
    private static testMethod void GroupingOfAssignment(){
        PRM_GroupingOfAssignment groupingObj = new PRM_GroupingOfAssignment();
        
        Map<Id,Account_Groupings__c> triggerNewMap = new Map<Id,Account_Groupings__c>();
        Map<Id,Account_Groupings__c> triggerOldMap = new Map<Id,Account_Groupings__c>();
        
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser)
        {
        PRM_VPP_JobDataHelper.createVPPCustomSettingData();   
        }
        //List<Account> accout = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();   
        
        //Amit: Creation of Partner Accounts
       
      /*   Hub_Info__c objHubInfo = new Hub_Info__c();
         objHubInfo.Site_DUNS_Entity__c = '001017771';
         objHubInfo.Global_DUNS_Entity__c = '001017771';
         objHubInfo.Golden_Site_Identifier__c = 30104256;
         insert objHubInfo;
         */
         Hub_Info__c objHubInfo = [select Id,Site_DUNS_Entity__c,Golden_Site_Identifier__c from Hub_info__c Where Site_DUNS_Entity__c != null Limit 1];
                       
        Account_Groupings__c grouping1 = new Account_Groupings__c();
        grouping1.Name = 'TestGroup123';
        grouping1.Active__c = true;
        //grouping1.Inactivation_Reason__c = 'No longer a partner';
        insert grouping1;
         
         List<Account> accout = new List<Account>();
        
         Account objAccount = new Account();
         objAccount.name = 'TestPartnerAcc';
         objAccount.Party_Number__c = '232323';            
         objAccount.BillingCountry ='Colombia';
         objAccount.Synergy_Account_Number__c = '10';
         objAccount.Type = 'Partner';
         objAccount.Lead_Oppty_Enabled__c = true;
         objAccount.Partner_Type__c ='Distributor';
         objAccount.Hub_Info__c = objHubInfo.Id;
         //objAccount.Site_DUNS_Entity__c='001017771';
         objAccount.PROFILED_ACCOUNT_FLAG__c = true;
         accout.add(objAccount);
  
         /*
         Account objAccount1 = new Account();
         objAccount1.name = 'TestPartnerAcc1';
         objAccount1.Party_Number__c = '4321';            
         objAccount1.BillingCountry ='Colombia';
         objAccount1.Synergy_Account_Number__c = '10';
         objAccount1.Type = 'Partner';
         objAccount1.Lead_Oppty_Enabled__c = true;
         objAccount1.Partner_Type__c ='Distributor';
         objAccount1.Site_DUNS_Entity__c='001017771';
         objAccount1.PROFILED_ACCOUNT_FLAG__c = true;
         accout.add(objAccount1);                
         
         Account objAccount2 = new Account();
         objAccount2.name = 'TestPartnerAcc2';
         objAccount2.Party_Number__c = '5678';            
         objAccount2.BillingCountry ='Colombia';
         objAccount2.Synergy_Account_Number__c = '10';
         objAccount2.Type = 'Partner';
         objAccount2.Lead_Oppty_Enabled__c = true;
         objAccount2.Partner_Type__c ='Distribution VAR';
         objAccount2.Site_DUNS_Entity__c='002017771';
         objAccount2.PROFILED_ACCOUNT_FLAG__c = true;
         accout.add(objAccount2);
         
         Account objAccount3 = new Account();
         objAccount3.name = 'TestPartnerAcc3';
         objAccount3.Party_Number__c = '8765';            
         objAccount3.BillingCountry ='Colombia';
         objAccount3.Synergy_Account_Number__c = '10';
         objAccount3.Type = 'Partner';
         objAccount3.Lead_Oppty_Enabled__c = false;
         objAccount3.Partner_Type__c ='Distribution VAR';
         objAccount3.Site_DUNS_Entity__c='002017771';
         objAccount3.PROFILED_ACCOUNT_FLAG__c = true;
         accout.add(objAccount3);
         */
         system.debug('accout is:'+ accout);
         
        insert accout;
        List<Account> lstacc = [Select Id,Name, Site_Duns_Entity_Hub__c from Account Where Id =:accout[0].id];
        system.debug('#inserted account is'+lstacc);
        //Creation of Partner Accounts End
        
        Account_Groupings__c grouping = new Account_Groupings__c();
        grouping = new Account_Groupings__c();
        grouping.Name = 'TestClass323';
        grouping.Active__c = true;
        grouping.Profiled_Account__c = accout[0].id;
        //grouping.No_Master_Required__c = false;
        grouping.Master_Grouping__c = grouping1.Id;
               
        insert grouping;
        grouping.Active__c = false;
 
        List<Account_groupings__c> groupingList= [select id,id__c,profiled_account__c,Profiled_Account_Site_Duns__c,master_grouping__c,active__c from account_groupings__c where id=:grouping.id];
        system.debug('#groupinglist---->'+groupingList);
      
        for(Account_groupings__c grp :groupingList){
            triggerOldMap.put(grp.id,grp);
            grp.master_grouping__c=grp.id;
            triggerNewMap.put(grp.Id,grp);
        }
        
        //List<Account> accounts = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        //insert accounts;        
        
        List<Account> accountList = new List<Account>();
        Set<String> accountSiteDuns = new Set<String>();
        Set<Id> accountIds = new Set<Id>();
        for(Account acc: accout){
            acc.Selected__c=true;
            accountSiteDuns.add(acc.Site_DUNS_Entity_Hub__c);
            accountList.add(acc);
            accountIds.add(acc.id);
        }
        update accountList;
        
        system.debug('#accountList---->'+accountList);
        
        Map<String,String> AccountsGroupMap =  new Map<String,String>();
        String groupingValues = groupingList[0].id+'='+groupingList[0].master_grouping__c+'@'+groupingList[0].Profiled_Account__c;        
        String groupingId = accout[0].Site_DUNS_Entity_Hub__c;
        
        AccountsGroupMap.put(groupingId,groupingValues);
        Test.StartTest();
        groupingObj.createGrouping(groupingList);
        groupingObj.beforeInsertOnGrouping(groupingList);
               
        Map<Id,String> accountIdWithPartnerType = new Map<Id,String>();
        accountIdWithPartnerType.put(groupingList[0].profiled_account__c,groupingValues);
          
        Map<Id,Account> accountIdWithPartnerType1 = new Map<Id,Account>();
        accountIdWithPartnerType1.put(groupingList[0].profiled_account__c,accout[0]);
     
        //groupingObj.removeGroupingFromAccount(accountSiteDuns,accountIds);
        groupingObj.updateAccounts(AccountsGroupMap,accout);
        groupingObj.updateRelatedGrouping(AccountsGroupMap);
        groupingObj.updateRelatedPartnerType(accountIdWithPartnerType1);
        groupingObj.isUnderLimit(accountIdWithPartnerType1);
        
        groupingObj.removeGrouingFromAccount(accountIdWithPartnerType,AccountsGroupMap);
        groupingObj.fetchRelatedAccounts(accountIdWithPartnerType);
        
        groupingObj.removeAllRelatedGrouping(accountIdWithPartnerType);
        
        System.debug('groupingList[0].id__c '+ groupingList[0].id__c);
        
        groupingObj.masterGroupingUpdate(triggerNewMap, triggerOldMap);
        groupingObj.createGrouping(groupingList);
        groupingObj.updateOfGrouping(groupingList, triggerNewMap, triggerOldMap);
        groupingObj.beforeInsertOnGrouping(groupingList);
        
        Test.StopTest();
    }
}