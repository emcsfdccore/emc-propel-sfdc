/*==================================================================================================================+

 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        =========== 
 |  19/03/2014    Srikrishna SM         This Test class is used for Pricing Requests Sharing(Re-written this test class to 
                                        increase the code coverage)
 +==================================================================================================================**/
@isTest
private Class R2R_Asset_Management_TC{
    
    static testMethod void setSalesPlanTest(){
        //Create required custom setting data for the test class
        System.runAs(new user(Id = UserInfo.getUserId())){
        
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
             CustomSettingDataHelper.GBSConfigData();
        }
        
        //Create Account
        List<Account> lstAccount = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();       
        insert lstAccount;
        
        //Create Contact
        Contact testContact = new Contact(Lastname = 'Test Contact', AccountId = lstAccount[0].Id);
        insert testContact;
        
        //Create Opportunities
        List<Opportunity> opportunityList = new List<Opportunity>();
        Opportunity validOpty = new Opportunity (Name = 'Test Opportunity ', AccountId = lstAccount[0].id, Sales_Force__c = 'EMC',CurrencyIsoCode = 'USD', StageName = 'Pipeline',District_Manager__c = testContact.id,Products__c = 'test',CloseDate = System.today(), VCE_Related__c='VMWare Other',Amount = 500, Opportunity_Number__c = '9999999 test');
        opportunityList.add(validOpty);
        insert opportunityList;
        
        //Get Record Type Id for EMC Install Type
        CustomSettingDataValueMap__c emcInstallRecordTypeId = CustomSettingDataValueMap__c.getValues('EMC Install Record Type');
        CustomSettingDataValueMap__c emcDeInstallRecordTypeId = CustomSettingDataValueMap__c.getValues('EMC Install - NonActionable');
        
        //Create Assets to test setRecordTypeOnMethod
        
        List<Asset__c> listForSetRecordTypeOnAsset = new List<Asset__c>();
        Asset__c testAsset0 = new Asset__c(name = 'Test Asset 00', Customer_Name__c = lstAccount[0].id, Install_Base_Status__c = 'Awaiting Customer Install');
        Asset__c testAsset1 = new Asset__c(name = 'Test Asset 01', Customer_Name__c = lstAccount[0].id, Install_Base_Status__c = 'Test Else Block');
        listForSetRecordTypeOnAsset.add(testAsset0);
        listForSetRecordTypeOnAsset.add(testAsset1);
        
        insert listForSetRecordTypeOnAsset;
        
        //Create Assets to test postChatterFeed
        List<Asset__c> listForpostChatterFeed = new List<Asset__c>();
        Asset__c testAsset2 = new Asset__c(name = 'Test Asset 02', Customer_Name__c = lstAccount[0].id, RecordTypeId = emcInstallRecordTypeId.DataValue__c, Deffered_to_Renewals__c = true, Chatter_Notification_Indicator__c='Does not exist');
        Asset__c testAsset3 = new Asset__c(name = 'Test Asset 03', Customer_Name__c = lstAccount[0].id, RecordTypeId = emcInstallRecordTypeId.DataValue__c, Deffered_to_Renewals__c = true, Chatter_Notification_Indicator__c='Opportunity');
        Asset__c testAsset4 = new Asset__c(name = 'Test Asset 04', Customer_Name__c = lstAccount[0].id, RecordTypeId = emcInstallRecordTypeId.DataValue__c, Deffered_to_Renewals__c = true, Chatter_Notification_Indicator__c='Opportunity 150 90');
        listForpostChatterFeed.add(testAsset2);
        listForpostChatterFeed.add(testAsset3);
        listForpostChatterFeed.add(testAsset4);
        
        insert listForpostChatterFeed;
        
        //Create Assets to test updateGDWField
        List<Asset__c> listForUpdateGDWField = new List<Asset__c>();
        Asset__c testAsset5 = new Asset__c(name = 'Test Asset 05', Customer_Name__c = lstAccount[0].id, Comments__c='60');
        Asset__c testAsset6 = new Asset__c(name = 'Test Asset 06', Customer_Name__c = lstAccount[0].id, Serial_Number__c='6011');
        Asset__c testAsset7 = new Asset__c(name = 'Test Asset 07', Customer_Name__c = lstAccount[0].id, Asset_Depreciation_Term__c='100');
        Asset__c testAsset8 = new Asset__c(name = 'Test Asset 08', Customer_Name__c = lstAccount[0].id);
        Asset__c testAsset9 = new Asset__c(name = 'Test Asset 09', Customer_Name__c = lstAccount[0].id, Deffered_to_Renewals__c= true);
        Asset__c testAsset10 = new Asset__c(name = 'Test Asset 10', Customer_Name__c = lstAccount[0].id, Product_Name_Vendor__c='Intel');
        Asset__c testAsset11 = new Asset__c(name = 'Test Asset 11', Customer_Name__c = lstAccount[0].id, Party_Number__c='123');
        Asset__c testAsset12 = new Asset__c(name = 'Test Asset 12', Customer_Name__c = lstAccount[0].id, IB_Instance_Number__c='1234');
        Asset__c testAsset13 = new Asset__c(name = 'Test Asset 13', Customer_Name__c = lstAccount[0].id, Opportunity_Number_Renew__c='NoTest');
        Asset__c testAsset14 = new Asset__c(name = 'Test Asset 14', Customer_Name__c = lstAccount[0].id, Hardware_Post_Warranty_Estimate_Annual_N__c=20);
        Asset__c testAsset15 = new Asset__c(name = 'Test Asset 15', Customer_Name__c = lstAccount[0].id, Software_Post_Warranty_Estimate_Annual__c=20);
        Asset__c testAsset16 = new Asset__c(name = 'Test Asset 16', Customer_Name__c = lstAccount[0].id, Application__c='SFDC');
        Asset__c testAsset17 = new Asset__c(name = 'Test Asset 17', Customer_Name__c = lstAccount[0].id, Swap_Trade_In_Cost_Relief_Value__c=100.00);
        Asset__c testAsset18 = new Asset__c(name = 'Test Asset 18', Customer_Name__c = lstAccount[0].id, RecordTypeId = emcInstallRecordTypeId.DataValue__c);
        Asset__c testAsset19 = new Asset__c(name = 'Test Asset 19', Customer_Name__c = lstAccount[0].id, Original_Purchase_Price_Num__c=100);
        Asset__c testAsset20 = new Asset__c(name = 'Test Asset 20', Customer_Name__c = lstAccount[0].id, Contract_End_Date__c = system.today()+100, Deffered_to_Renewals__c =true);
        listForUpdateGDWField.add(testAsset5);
        listForUpdateGDWField.add(testAsset6);
        listForUpdateGDWField.add(testAsset7);
        listForUpdateGDWField.add(testAsset8);
        listForUpdateGDWField.add(testAsset9);
        listForUpdateGDWField.add(testAsset10);
        listForUpdateGDWField.add(testAsset11);
        listForUpdateGDWField.add(testAsset12);
        listForUpdateGDWField.add(testAsset13);
        listForUpdateGDWField.add(testAsset14);
        listForUpdateGDWField.add(testAsset15);
        listForUpdateGDWField.add(testAsset16);
        listForUpdateGDWField.add(testAsset17);
        listForUpdateGDWField.add(testAsset18);
        listForUpdateGDWField.add(testAsset19);
        listForUpdateGDWField.add(testAsset20);
        insert listForUpdateGDWField;
        
        //Create Opportunity Asset Junctions
        List<Opportunity_Asset_Junction__c> oppAssetJunctionList = new List<Opportunity_Asset_Junction__c>();
        Opportunity_Asset_Junction__c oaj1 = new Opportunity_Asset_Junction__c (Related_Asset__c = testAsset2.id, Related_Opportunity__c = validOpty.id);
        Opportunity_Asset_Junction__c oaj2 = new Opportunity_Asset_Junction__c (Related_Asset__c = testAsset3.id, Related_Opportunity__c = validOpty.id);
        Opportunity_Asset_Junction__c oaj3 = new Opportunity_Asset_Junction__c (Related_Asset__c = testAsset4.id, Related_Opportunity__c = validOpty.id);
        oppAssetJunctionList.add(oaj1);   
        oppAssetJunctionList.add(oaj2);   
        oppAssetJunctionList.add(oaj3);   
        insert oppAssetJunctionList;
        
        //Start Test Context
        test.startTest();
        R2R_Asset_Management R2RAssetMgmtInstance = new R2R_Asset_Management();  
        R2RAssetMgmtInstance.setRecordTypeOnAsset(listForSetRecordTypeOnAsset);
        R2RAssetMgmtInstance.flipAssetRT(listForSetRecordTypeOnAsset, emcInstallRecordTypeId.DataValue__c);
        R2RAssetMgmtInstance.postChatterFeed(listForpostChatterFeed);
        
        listForUpdateGDWField[0].Comments__c='80';
        update listForUpdateGDWField;
        listForUpdateGDWField[1].Serial_Number__c='8011';
        update listForUpdateGDWField;
        listForUpdateGDWField[2].Asset_Depreciation_Term__c='999';
        update listForUpdateGDWField;
        listForUpdateGDWField[3].Name='Test Asset 100';
        update listForUpdateGDWField;
        listForUpdateGDWField[4].Deffered_to_Renewals__c=false;
        update listForUpdateGDWField;
        listForUpdateGDWField[5].Product_Name_Vendor__c='Dell';
        update listForUpdateGDWField;
        listForUpdateGDWField[6].Party_Number__c='333';
        update listForUpdateGDWField;
        listForUpdateGDWField[7].IB_Instance_Number__c='333';
        update listForUpdateGDWField;
        listForUpdateGDWField[8].Opportunity_Number_Renew__c='Test';
        update listForUpdateGDWField;
        listForUpdateGDWField[9].Hardware_Post_Warranty_Estimate_Annual_N__c=55;
        update listForUpdateGDWField;
        listForUpdateGDWField[10].Software_Post_Warranty_Estimate_Annual__c=55;
        update listForUpdateGDWField;
        listForUpdateGDWField[11].Application__c='Application';
        update listForUpdateGDWField;
        listForUpdateGDWField[12].Swap_Trade_In_Cost_Relief_Value__c=123.12;
        update listForUpdateGDWField;
        listForUpdateGDWField[13].RecordTypeId=emcDeInstallRecordTypeId.DataValue__c;
        update listForUpdateGDWField;
        listForUpdateGDWField[14].Original_Purchase_Price_Num__c=200;
        update listForUpdateGDWField;
        listForUpdateGDWField[15].Contract_End_Date__c = system.today(); 
        update listForUpdateGDWField;
        
        test.stopTest();
        //End Test Context
    }
}