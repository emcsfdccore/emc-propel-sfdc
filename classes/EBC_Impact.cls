/*
 *  Created By       :- Sunil Arora
 *  Created Date     :- 13/04/2010
 *  Last Modified By :- Sunil Arora
 *  Description      :- This class will be called from EC_Impact_Page and used to show/save Impact record.

Modified by :   Avinash Kaltari
Modified on :   12-Jun-2012
Reason      :   To eliminate hardcoding of IDs for System Administrator and EBC Power User profiles

Modified by :   Vivek Barange
Modified on :   10-Feb-2015
Reason      :   #1684 - To add EBC Related Opportunity List into EBC Impact Page

Modified By :   Akash Rastogi
Modified On :   12-Feb-2015
Reason      :   #1702 - To replace "How_did_the_briefing_impact_the_sale_acc__c" with "What_objectives_did_this_briefing_meet__c" from query

Modified By :   Vinod Jetti
Modified On :   12-March-2015
Reason      :   #1731 - Added "New EBC Related Opportunity" Button into EBC Impact Page

 */
public class EBC_Impact
{
    public Id briefingEventId; // to hold the id of the briefing event
    public EBC_Briefing_Event__c objBEvent {get;set;} // to hold the briefing event object record
    public Boolean NotFromSite {get;set;}
    public Boolean ImpactSubmitted {get;set;}
    public Integer noOfRelatedOpptys {get;set;}
    public String  statusOfOppty {get;set;}
    //Start - #1684
    public List<EBC_Related_Opportunity__c> lstEBCRelatedOppty {get;set;}
    //End - #1684
    public EBC_Impact(ApexPages.StandardController stdController)
    {        
        try
        {
            briefingEventId= System.currentPageReference().getParameters().get('id'); // id of the parent Briefing Event Object
            objBEvent = [Select id, Name, Start_Date__c, End_Date__c from EBC_Briefing_Event__c where id =:briefingEventId];
            //Start - #1684
            lstEBCRelatedOppty = [Select Name, Opportunity_Name__r.Name, Opportunity_Number__c, Forecast_Status__c, Forecasted_Amount__c, Close_Date__c, Account_Name__c, Party_ID__c from EBC_Related_Opportunity__c where Briefing_Event__c = :briefingEventId];
            //End - #1684
        }
        catch(StringException strEx)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , system.label.Valid_BE_id ));
            return;
        }

        if(briefingEventId != null) 
        {
            // retrieve the Feedback fields from the Briefing Event and store it in objBEvent
            //WR#1702 Changes
            //#1731 - Added Inner query to check the size of the 'EBC_Related_Opportunity__c' to display error message
            objBEvent=[Select  Id,Name,Start_Date__c,End_Date__c,Did_the_opportunity_Close__c,Was_EMC_Jet_Used_to_Transport_Customer__c,Actual__c,
                       Comments_status_of_opportunity_if_no__c,Comments_briefing_impact_the_sale_acco__c,Status_of_Opportunity__c,
                       What_objectives_did_this_briefing_meet__c,Impact_Status_Picklist__c, (Select Id, Name from EBC_Related_Opportunity__r where Briefing_Event__c =: briefingEventId)     
                       From EBC_Briefing_Event__c where Id=:briefingEventId];
             
            ImpactSubmitted = false;
            NotFromSite = true;
            Profile p = [Select Id, Name from Profile where Id =: UserInfo.getProfileId()];

            if(p.Name == 'EBC Application Site Profile')
            {
                NotFromSite=false;  
            }
            //EBC Power User Profile Id=00e70000001FbAv and System Administrator Profile Id=00e70000000wBza

            //Avinash's code begins...

            Id SystemAdministratorId, EBCPowerUserId ;
            List<Profile> lstid = [SELECT Name, Id FROM Profile where name in (:System.Label.System_Administrator_Profile_Name ,:System.Label.EBC_Power_User_Profile_Name)  limit 2];
            for(Profile prof : lstid)
            {
                if(prof.name == 'System Administrator')
                    SystemAdministratorId = prof.id;
                if(prof.name == 'EBC Power User')
                    EBCPowerUserId = prof.id;
            }

            //Avinash's code ends.


            if(objBEvent.Impact_Status_Picklist__c == 'Submitted' && !(p.Id == EBCPowerUserId || p.Id == SystemAdministratorId))
            {
                ImpactSubmitted = true;   
            }

        }
    }

    // this method updates the briefing event record, sessions related to the briefing event and the session presenters related to the briefing event  
    public PageReference saveRecord()
    {   
       //#1731 - Start
         if(objBEvent.EBC_Related_Opportunity__r.Size() == 0 &&  objBEvent.Status_of_Opportunity__c == null){      
            return null;
         } 
         //#1731 - End
       try
        {  
            objBEvent.Impact_Status_Picklist__c = 'Submitted';
            update objBEvent;
            //update sessionPresentersList; // updates the session presenters related to the briefing event with the Rating and Notes fields
            if(NotFromSite == true)
            {
                return backToBriefingEvent();
            }
            else
            {
                ImpactSubmitted = true;  
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO , 'Impact submitted successfully'));
                return null;     
            }
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , system.label.EBC_Impact_Exception+' Error: '+e.getMessage()));
            return null;
        }           
    } 
   // Added for 1731 by Vinod
    public PageReference EBCRelatedOppty(){
        briefingEventId= System.currentPageReference().getParameters().get('id');
        objBEvent=[Select Id,Name From EBC_Briefing_Event__c where Id=:briefingEventId];
        PageReference pageref = new PageReference('/a4u/e?CF00N70000003a66Q='+objBEvent.name+'&CF00N70000003a66Q_lkid='+briefingEventId+'&saveURL=/apex/EBC_Impact_Form?id='+briefingEventId+'&cancelURL=/apex/EBC_Impact_Form?id='+briefingEventId);
        pageRef.setRedirect(true);
        return pageref;
    }
   // End of 1731 changes
    // method to go back to the briefing event detail page
    public PageReference backToBriefingEvent()
    { 
        PageReference pageRef = new PageReference('/' + briefingEventId);
        pageRef.setRedirect(true);
        return pageRef;
    }  
}