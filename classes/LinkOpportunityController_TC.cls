/*==================================================================================================================+
 |  HISTORY  |                                                                            
 |  DATE          DEVELOPER        WR        DESCRIPTION                               
 |  ====          =========        ==        =========== 
 |  18/12/2014   Vivek Barange    1557       Created class to test LinkOpportunityController funtionlities
==========================================================================================*/
@isTest
public class LinkOpportunityController_TC {
    public static testMethod void linkOppty() {
        
        CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
        custset.By_Pass_Account_Triggers__c = true;
        custset.By_Pass_Account_Validation_Rules__c = true;
        custset.By_Pass_Opportunity_Triggers__c = true;
        custset.By_Pass_Opportunity_Validation_Rules__c = true;
        insert custset;
        
        List<User> lstUser = [Select id from user where IsActive = true and Theater__c in ('APJ') limit 1];
        
        Account acctObj = new Account();
        acctObj.Name = 'Pricing Testing';
        acctObj.Synergy_Account_Number__c = '123456';
        Database.insert(acctObj);
        
        Opportunity opp_Obj = new Opportunity();
        opp_Obj.AccountId =acctObj.id;
        opp_Obj.Opportunity_Owner__c = lstUser[0].id;
        opp_Obj.Name = 'TEST 1';
        opp_Obj.Opportunity_Number__c='2';
        opp_Obj.StageName = 'TEST 1';
        opp_Obj.CloseDate = Date.today()+10;
        opp_Obj.Sales_Force__c = 'EMC';
        opp_Obj.Sales_Channel__c = 'Direct';
        opp_Obj.VMWare_Related__c = 'VMWare Backup';
        opp_Obj.Amount = 222.00;
        opp_Obj.Competitor__c='CSC';
        opp_Obj.Product_Model__c='6140';
        Database.insert(opp_Obj);
        
        Opportunity opp_Obj1 = new Opportunity();
        opp_Obj1.AccountId =acctObj.id;
        opp_Obj1.Opportunity_Owner__c = lstUser[0].id;
        opp_Obj1.Name = 'TEST 2';
        opp_Obj1.Opportunity_Number__c='3';
        opp_Obj1.StageName = 'TEST 1';
        opp_Obj1.CloseDate = Date.today()+10;
        opp_Obj1.Sales_Force__c = 'EMC';
        opp_Obj1.Sales_Channel__c = 'Direct';
        opp_Obj1.VMWare_Related__c = 'VMWare Backup';
        opp_Obj1.Amount = 222.00;
        opp_Obj1.Competitor__c='CSC';
        opp_Obj1.Product_Model__c='6140';
        Database.insert(opp_Obj1);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(opp_Obj);
    LinkOpportunityController lpc = new LinkOpportunityController(sc);
        
        lpc.linkOppty();
        lpc.proxy = new Search_Proxy_Container__c();
        lpc.proxy.Select_Child_Opportunity__c = opp_Obj1.Id;
        
        lpc.linkOppty();
        lpc.linkOppty();
    }
    
    public static testMethod void linkOppty1() {
        
        CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
        custset.By_Pass_Account_Triggers__c = true;
        custset.By_Pass_Account_Validation_Rules__c = true;
        custset.By_Pass_Opportunity_Triggers__c = true;
        custset.By_Pass_Opportunity_Validation_Rules__c = true;
        insert custset;
        
        List<User> lstUser = [Select id from user where IsActive = true and Theater__c in ('APJ') limit 1];
        
        Account acctObj = new Account();
        acctObj.Name = 'Pricing Testing';
        acctObj.Synergy_Account_Number__c = '123456';
        Database.insert(acctObj);
        
        Opportunity opp_Obj = new Opportunity();
        opp_Obj.AccountId =acctObj.id;
        opp_Obj.Opportunity_Owner__c = lstUser[0].id;
        opp_Obj.Name = 'TEST 1';
        opp_Obj.Opportunity_Number__c='2';
        opp_Obj.StageName = 'Closed';
        opp_Obj.CloseDate = Date.today()-100;
        opp_Obj.Sales_Force__c = 'EMC';
        opp_Obj.Sales_Channel__c = 'Direct';
        opp_Obj.VMWare_Related__c = 'VMWare Backup';
        opp_Obj.Amount = 222.00;
        opp_Obj.Competitor__c='CSC';
        opp_Obj.Product_Model__c='6140';
        Database.insert(opp_Obj);
        
        Opportunity opp_Obj1 = new Opportunity();
        opp_Obj1.AccountId =acctObj.id;
        opp_Obj1.Opportunity_Owner__c = lstUser[0].id;
        opp_Obj1.Name = 'TEST 2';
        opp_Obj1.Opportunity_Number__c='3';
        opp_Obj1.StageName = 'Closed';
        opp_Obj1.CloseDate = Date.today()-100;
        opp_Obj1.Sales_Force__c = 'EMC';
        opp_Obj1.Sales_Channel__c = 'Direct';
        opp_Obj1.VMWare_Related__c = 'VMWare Backup';
        opp_Obj1.Amount = 222.00;
        opp_Obj1.Competitor__c='CSC';
        opp_Obj1.Product_Model__c='6140';
        opp_Obj1.Related_Opportunity__c = opp_Obj.Id;
        Database.insert(opp_Obj1);
        
        opp_Obj1.Opportunity_Number__c = '2';

        ApexPages.StandardController sc = new ApexPages.StandardController(opp_Obj);
    LinkOpportunityController lpc = new LinkOpportunityController(sc);
        
        lpc.proxy = new Search_Proxy_Container__c();
        lpc.proxy.Select_Child_Opportunity__c = opp_Obj1.Id;
        lpc.linkOppty(); 
        lpc.cancel();
    }
}