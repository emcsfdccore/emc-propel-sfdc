/*=============================================================================
    HISTORY                                                                  
                                                               
    DATE            DEVELOPER                WR             DESCRIPTION                               
    ====            =========                ==             =========== 
 |	10/04/2013	 	 Shipra Misra			248315		SFA - Concurrent Batch Job Fix
                                                             
==============================================================================*/
global with sharing class Scheduler_Batch_Delete implements Schedulable 
{
    global void execute(SchedulableContext sc )
    {
    	map<string,string> mapBtDel = new map<string,string>();
		BatchDeleteData DeleteBatch = new BatchDeleteData(mapBtDel);
    	Database.executebatch(DeleteBatch);
    }
}