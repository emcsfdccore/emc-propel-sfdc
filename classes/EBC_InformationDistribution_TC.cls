/*
*  Created By:       Yamilett Salazar
*  Created Date :    21 Aug, 2013
*  Last Modified By: Yamilett Salazar
*  Description :     Test Class for EBC_InformationDistribution controller
*  WR:900    05/15/2014    Abinaya    Assigning test value to newly created field.
*  WR:269    06/12/2014    Abinaya    Created Briefing Center record and used in Briefing Event to calculate Time Zone for session.
*/

@isTest
private class EBC_InformationDistribution_TC{
  
  
  static testMethod void testEBC_InformationDistributionI(){
    Id emcAccount = '0017000000OmdVa';
    Id emcInternalRT = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND Name = 'EMC Internal Contact' LIMIT 1].Id;
    Contact internal = new Contact(
      RecordTypeId = emcInternalRT,
      AccountId = emcAccount,
      Salutation = 'Mr.',
      FirstName = 'Test',
      LastName = 'Contact',
      Title = 'Engineer',
      PowerlinkID_Badge__c = '001234',
      Cost_Center__c = 'Mx1',
      EBC_Title__c = 'Developer',
      Organization__c = 'Global Solutions - Cloud Infrastructure',
      Active_Presenter__c = true,
      Language_Preference__c = 'English',
      Start_Date__c = date.newInstance(2010, 1, 1),
      Phone = '12345678',
      MobilePhone = '01234569',
      Email = 'contact@sfdc01.com',
      EBC_Name__c = 'Test Contact',
      Discussion_Leader_Location__c = 'Mx',
      Discussion_Leader_Area_of_Expertise__c = 'Dev',
      Active__c = true
    );
    insert internal;
    EBC_Briefing_Center__c bc = new EBC_Briefing_Center__c(
    Name = 'Test location',
    Briefing_Center_Time_Zone__c = 'IST'
    );
    insert bc;
    
    EBC_Briefing_Event__c  be = new EBC_Briefing_Event__c(
      Name='test',
      Start_Date__c=date.today().addDays(-2),
      End_Date__c=date.today().addDays(2),
      Briefing_Center__c = bc.id,
      Briefing_Status__c='Open',
      Objective_of_the_Visit_picklist__c = 'Other',//WR:900 Assigning test value
      Objective_of_the_Visit__c = 'TestClass',//WR:900 Assigning test value
      Requestor_Name__c = internal.Id
    );
    insert be;
    
    EBC_Invitees__c EBCin = new EBC_Invitees__c(
        Briefing_Event__c = be.id,
        Attending__c = true
    );
    insert EBCin;
    
    List<EBC_Session__c> sess = new List<EBC_Session__c>();
    EBC_Session__c sess1 = new EBC_Session__c(
      Name = 'Session1',
      Briefing_Event__c = be.Id,
      Session_Start_Time__c = datetime.now(),
      Session_End_Time__c = datetime.now().addHours(2),
      Briefing_Advisor_Notes__c = 'No advisor notes.',
      Requester_Notes__c = 'No requester notes.',
      Topic_Override__c = 'Test1'
    );
    sess.add(sess1);
    EBC_Session__c sess2 = new EBC_Session__c(
      Name = 'Session2',
      Briefing_Event__c = be.Id,
      Session_Start_Time__c = datetime.now(),
      Session_End_Time__c = datetime.now().addHours(3),
      Briefing_Advisor_Notes__c = 'No advisor notes.',
      Requester_Notes__c = 'No requester notes.',
      Topic_Override__c = 'Test2'
    );
    sess.add(sess2);
    EBC_Session__c sess3 = new EBC_Session__c(
      Name = 'Session3',
      Briefing_Event__c = be.Id,
      Session_Start_Time__c = datetime.now().addHours(-4),
      Session_End_Time__c = datetime.now().addHours(3),
      Briefing_Advisor_Notes__c = 'No advisor notes.',
      Requester_Notes__c = 'No requester notes.',
      Topic_Override__c = 'Test3'
    );
    sess.add(sess3);
    EBC_Session__c sess4 = new EBC_Session__c(
      Name = 'Session4',
      Briefing_Event__c = be.Id,
      Session_Start_Time__c = datetime.now().addHours(2),
      Session_End_Time__c = datetime.now().addHours(3),
      Briefing_Advisor_Notes__c = 'No advisor notes.',
      Requester_Notes__c = 'No requester notes.',
      Topic_Override__c = 'Test4'
    );
    sess.add(sess4);
    insert sess;
    EBC_Session__c sess5 = new EBC_Session__c(
      Name = 'Session5',
      Briefing_Event__c = be.Id,
      Session_Start_Time__c = datetime.now().addHours(-4),
      Session_End_Time__c = datetime.now().addHours(3),
      Briefing_Advisor_Notes__c = 'No advisor notes.',
      Requester_Notes__c = 'No requester notes.',
      Topic_Override__c = 'Test5'
    );
    insert sess5;
    List<EBC_Session_Presenter__c> pres = new List<EBC_Session_Presenter__c>();
    pres.add(new EBC_Session_Presenter__c(Name = sess1.Name + 'Pres1a', Session__c = sess1.Id));
    pres.add(new EBC_Session_Presenter__c(Name = sess1.Name + 'Pres1b', Session__c = sess1.Id));
    pres.add(new EBC_Session_Presenter__c(Name = sess2.Name + 'Pres2a', Session__c = sess2.Id));
    pres.add(new EBC_Session_Presenter__c(Name = sess2.Name + 'Pres2b', Session__c = sess2.Id));
    insert pres;
    
    PageReference fbp = Page.EBC_InformationDistribution;
    fbp.getParameters().put('id', be.Id);
    fbp.getParameters().put('qet', 'true');
    Test.setCurrentPage(fbp);
    ApexPages.StandardController sc = new ApexPages.standardController(be);
    EBC_InformationDistribution ctrl = new EBC_InformationDistribution(sc);
    
    PageReference fbp1 = Page.EBC_InformationDistribution;
    fbp1.getParameters().put('id', be.Id);
    fbp1.getParameters().put('qet', 'false');
    Test.setCurrentPage(fbp1);
    ApexPages.StandardController sc1 = new ApexPages.standardController(be);
    EBC_InformationDistribution ctrl1 = new EBC_InformationDistribution(sc1);
    
    
  }
}