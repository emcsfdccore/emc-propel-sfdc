/**
 * 
Modified by :   Avinash Kaltari
Modified on :   17-Feb-2014
Reason      :   Created the class to include the testmethod from "SiteLoginController" class
 */

@isTest
public class SiteLoginController_TC
{
    static testMethod void testSiteLoginController () 
    {
        // Instantiate a new controller with all parameters in the page
        SiteLoginController controller = new SiteLoginController ();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 

        System.assertEquals(controller.login(),null);                           
    }
}