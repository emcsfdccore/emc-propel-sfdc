/*==================================================================================================================+

 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        =========== 
 |  17/01/2013    Medhavi D.     217883     This Test class is used for Pricing Requests Sharing
 |  25/11/2013   Naga          #311032      This test class is modified to reduce the processing time 
 |  27/03/2014   Srikrishna SM              This test class is modified to increase the code coverage 
 |  10 July 2013  Vivek Barange  #1006      Modified test class to increase test coverage
 |  12/17/2014    Bindu Sri      CI 1559    Replaced closed reason values 
 |  19/02/2015    Vinod Jetti    CI 1649    created Hub Account Data object record instad of Account 'Global_DUNS_Entity__c' field
 +==================================================================================================================**/

 @isTest 
private class Transformation_AssetController_TC {
  
  //Create Custom setting data required for the test class
  Transformation_AssetController_TC(){
    CustomSettingDataHelper.dataValueMapCSData();
    CustomSettingDataHelper.eBizSFDCIntCSData();
    CustomSettingDataHelper.bypassLogicCSData();
    CustomSettingDataHelper.profilesCSData();
    CustomSettingDataHelper.dealRegistrationCSData();
  }
  
  static testMethod void linkAssetsControllerTest(){
    String rtName = System.Label.R2R_Restricted_Profile_Name;
    Id profileId = [select id from profile where name =: rtName].Id;
    User emcContractUser = UserProfileTestClassDataHelper.createUser(profileId, null);
    insert emcContractUser;

    Transformation_AssetController_TC testObj = new Transformation_AssetController_TC();
    Id recTypeId = [Select r.Id, r.name from RecordType r where r.name =: 'EMC Install' limit 1].Id;
    Id recTypeCIId = [Select r.Id, r.name from RecordType r where r.name =: 'Competitive Install' limit 1].Id;
    
    //Create Account
    List<Account> acc = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();       
      insert acc;
      
    //Create Opportunity
    List<Opportunity> oppList = new List<Opportunity>();
    oppList.add(new Opportunity(Name = 'TestOpp0',AccountId = acc[0].Id, CloseDate = system.today() + 5, StageName = 'Submitted',Sales_Channel__c='Direct',Sales_Force__c='EMC'));
    oppList.add(new Opportunity(Name = 'TestOpp1',AccountId = acc[0].Id, CloseDate = system.today() + 7, StageName = 'Booked'));
    oppList.add(new Opportunity(Name = 'TestOpp2',AccountId = acc[0].Id, CloseDate = system.today() + 7, StageName = 'Pipeline'));
    oppList.add(new Opportunity(Name = 'TestOpp3',AccountId = acc[0].Id, CloseDate = system.today() + 5, StageName = 'Pipeline',Opportunity_type__c = 'Refresh'));
    oppList.add(new Opportunity(Name = 'TestOpp4',AccountId = acc[0].Id, CloseDate = system.today() + 7, StageName = 'Eval',Opportunity_type__c = 'Refresh'));
    Database.insert(oppList);
    
            oppList[3].Opportunity_type__c = 'Refresh';
            oppList[4].Opportunity_type__c = 'Refresh';
            //oppList[7].Opportunity_type__c = 'Refresh';
            update oppList;
    
    //Create Asset
    List<Asset__c> assetList = new List<Asset__c>();
    assetList.add(new asset__c(Customer_Name__c = acc[0].id, Install_Base_Status__c='Install', RecordTypeId = recTypeId));
    assetList.add(new asset__c(Customer_Name__c = acc[0].id, Install_Base_Status__c='Install', RecordTypeId = recTypeCIId));
    Database.insert(assetList);
    
    List<Asset__c> assetList1 = new List<Asset__c>();
    assetList1.add(new asset__c(Customer_Name__c = acc[0].id, Install_Base_Status__c='Install', RecordTypeId = recTypeId));
    assetList1.add(new asset__c(Customer_Name__c = acc[0].id, Install_Base_Status__c='Install', RecordTypeId = recTypeCIId));
    Database.insert(assetList1);
    
    //Create Opportunity Asset Junction
    List<Opportunity_Asset_Junction__c> oaJunctionList = new List<Opportunity_Asset_Junction__c>();
    oaJunctionList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList[0].Id, Related_Opportunity__c=oppList[0].Id) );
    oaJunctionList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList[1].Id, Related_Opportunity__c=oppList[1].Id) );
    oaJunctionList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList[1].Id, Related_Opportunity__c=oppList[2].Id) );
    oaJunctionList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList1[0].Id, Related_Opportunity__c=oppList[3].Id) );
    oaJunctionList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList1[1].Id, Related_Opportunity__c=oppList[4].Id) );
    insert oaJunctionList;
    //listOprAsset.add(opptyAssetObj);
    
    User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
    //Instanciate StandardSetController 
    Test.startTest();
    for(Integer k=0; K<3; k++){
      if(k==0){
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(assetList);
        PageReference acctPage = new PageReference('/apex/Transformation_LinkAssets?retURL=?opportunityId='+oppList[0].id+'&selAcId='+oppList[0].AccountId+'&assetid='+assetList[0].Id);
        Test.setCurrentPage(acctPage);
            
        Transformation_LinkAssetsController controlLinkAsset= new Transformation_LinkAssetsController(setCon);
        controlLinkAsset.linkAssets();
        controlLinkAsset.clearData();
        controlLinkAsset.cancelLink();
        controlLinkAsset.createOppty();
        controlLinkAsset.getListOfOpps();
        controlLinkAsset.setOpptyName();
        controlLinkAsset.linkAssetsFromReasonCode();
        controlLinkAsset.cancelLinkFromReasonCode();
      }
      if(k==1){
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(assetList1);
        PageReference acctPage = new PageReference('/apex/Transformation_LinkAssets?retURL=?opportunityId='+oppList[3].id+'&selAcId='+oppList[4].AccountId+'&assetid='+assetList1[1].Id);
        Test.setCurrentPage(acctPage);
        Transformation_LinkAssetsController controlLinkAsset= new Transformation_LinkAssetsController(setCon);
      }
      if(k==2){
        System.runAs(insertUser){
          ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(assetList);
          PageReference acctPage = new PageReference('/apex/Transformation_LinkAssets?retURL=?opportunityId='+oppList[2].id+'&selAcId='+oppList[0].AccountId+'&assetid='+assetList[1].Id);
          Test.setCurrentPage(acctPage);
          Transformation_LinkAssetsController controlLinkAsset= new Transformation_LinkAssetsController(setCon);
        }
      }
    }
    Test.stopTest();
 }
  
  
  static testMethod void linkControllerTest() {
    Id recTypeId = [Select r.Id, r.name from RecordType r where r.name =: 'EMC Install' limit 1].Id;
    Id recTypeId2 = [Select r.Id, r.name, r.DeveloperName from RecordType r where r.DeveloperName =: 'Competitive_Install' limit 1].Id;
    
    Transformation_AssetController_TC testObj = new Transformation_AssetController_TC();
    //Craete Accounts    
    List<Account> acctLst = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();       
      insert acctLst;
    
    acctLst[1].Customer_Profiled_Account_Lookup__c = acctLst[2].Id;
    acctLst[1].Hub_Info__c = acctLst[0].Hub_Info__c;
    //acctLst[1].Global_DUNS_Entity__c = '1234';
    update acctLst[1];
    
    //Creating Lead records
    List<Lead> leadList = new List<Lead>();
    leadList.add(new Lead(Company = 'TestComp0',LastName='TestLast0',Status='New',Sales_Force__c='EMC',Lead_Originator__c='Customer Intelligence'));
    insert leadList;        
    
    //Insert Opportunity
       List<Opportunity> oppList = new List<Opportunity>();
        // inset Renewal oppty
        oppList.add(new Opportunity(Name = 'TestOpp0',AccountId = acctLst[0].Id, CloseDate = system.today() + 5, StageName = 'Submitted',Sales_Channel__c='Direct',Sales_Force__c='EMC'));
        oppList.add(new Opportunity(Name = 'TestOpp1',AccountId = acctLst[1].Id, CloseDate = system.today() + 7, StageName = 'Booked'));
        oppList.add(new Opportunity(Name = 'TestOpp2',AccountId = acctLst[1].Id, CloseDate = system.today() + 7, StageName = 'Pipeline'));
        //CI 1559
       oppList.add(new Opportunity(Name = 'TestOpp3',AccountId = acctLst[1].Id, CloseDate = system.today(), Closed_Reason__c = 'Project Cancelled', StageName = 'Closed',Sales_Channel__c='Direct', close_Comments__c = 'TestClosed', Sales_Force__c='EMC'));
         //End of CI 1559
        oppList.add(new Opportunity(Name = 'TestOpp5',AccountId = acctLst[1].Id, CloseDate = system.today() + 5, StageName = 'Pipeline',Opportunity_type__c = 'Refresh'));
        oppList.add(new Opportunity(Name = 'TestOpp6',AccountId = acctLst[1].Id, CloseDate = system.today() + 7, StageName = 'Eval',Opportunity_type__c = 'Refresh'));
        oppList.add(new Opportunity(Name = 'TestOpp7',AccountId = acctLst[1].Id, CloseDate = system.today() + 7, StageName = 'Booked',Opportunity_type__c = 'Refresh'));
         //CI 1559
        oppList.add(new Opportunity(Name = 'TestOpp8',AccountId = acctLst[1].Id, CloseDate = system.today() +6, close_Comments__c = 'TestClosed',Closed_Reason__c = 'Project Cancelled', StageName = 'Closed',Sales_Channel__c='Direct', Sales_Force__c='EMC',Opportunity_type__c = 'Refresh'));
        //End of CI 1559
    insert oppList;
    
            oppList[5].Opportunity_type__c = 'Refresh';
            oppList[6].Opportunity_type__c = 'Refresh';
            oppList[7].Opportunity_type__c = 'Refresh';
            update oppList;
  //Create Assets
    List<Asset__c> assetList = new List<Asset__c>();
    Asset__c asset1 = new Asset__c();
    asset1.customer_Name__c = acctLst[0].id;
    asset1.Disposition_Status__c ='Installed';
    asset1.Install_Base_Status__c='Install';
    asset1.RecordTypeId = recTypeId;
    assetList.add(asset1);
    
    Asset__c asset2 = new Asset__c();
    asset2.customer_Name__c = acctLst[0].id;
    asset2.Disposition_Status__c ='Installed';
    asset2.Install_Base_Status__c='Install';
    asset2.RecordTypeId = recTypeId2;
    assetList.add(asset2);
    insert assetList; 
    
    //Create Junction Objects
    List<Opportunity_Asset_Junction__c> lstJnObj = new List<Opportunity_Asset_Junction__c>();
    lstJnObj.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList[0].Id, Related_Opportunity__c=oppList[0].Id));
    insert lstJnObj;
    
    List<Lead_Asset_Junction__c> lajList = new List<Lead_Asset_Junction__c>();
    Lead_Asset_Junction__c leadAssetObj1 = new Lead_Asset_Junction__c();
    leadAssetObj1.Related_Asset__c =assetList[0].Id;
    leadAssetObj1.Related_Lead__c = leadList[0].Id;
    lajList.add(leadAssetObj1);
    insert lajList;
    
    /*String rtName = System.Label.R2R_Restricted_Profile_Name;
        Id profileId = [select id from profile where name =: rtName].Id;
        User emcContractUser = UserProfileTestClassDataHelper.createUser(profileId, null);
        insert emcContractUser;
        
        System.runAs(emcContractUser){
            List<Account> acctLst1 = AccountAndProfileTestClassDataHelper.createSimpleAccount();
            acctLst1[0].OwnerId = emcContractUser.Id;        
            insert acctLst1;
            ApexPages.currentpage().getParameters().put('accId',null); 
            ApexPages.StandardController setCon = new ApexPages.StandardController(acctLst1[0]);
            Transformation_AssetMassLinkController controlLinkAsset= new Transformation_AssetMassLinkController(setCon);
        }*/
        
    Test.startTest();
    
        Transformation_AssetMassLinkController controlObj= new Transformation_AssetMassLinkController();
        Transformation_AssetMassLinkController controlOb = new Transformation_AssetMassLinkController(new ApexPages.StandardController(acctLst[0]));
        
        //controlOb.getAssetWrapperlist();
        controlOb.assetsToAddList.add(asset1); 
        controlOb.assetsToAddList.add(asset2);
        
        controlOb.linkAssest();
        
        List<Transformation_AssetMassLinkController.assWrapper> wrapObj = controlOb.getAssetsDetails();
        wrapObj[0].Selected = true;
        controlOb.getAssetsDetails();
        controlOb.getselectedAssets();
        controlOb.processSelected();
        controlOb.SelectedRecord();
        
        controlOb.setCon.setPageSize(25);
        Integer reminder = math.mod(controlOb.setCon.getResultSize(),controlOb.setCon.getPageSize());
        Integer listToCreateSize = (reminder == 0?(controlOb.setCon.getResultSize()/controlOb.setCon.getPageSize()):(controlOb.setCon.getResultSize()/controlOb.setCon.getPageSize())+1);
        for(Integer i=0; i < listToCreateSize ; i++){
            controlOb.listOfAssetWrapperlist.add(new List<Transformation_AssetMassLinkController.assWrapper>());
        }
        
        controlOb.linkAssest();
        
        List<MiTrendConfig__c>  miTrendConfigList =  new List<MiTrendConfig__c>();
        miTrendConfigList.add(new MiTrendConfig__c(Name = 'Test1', ParameterApiName__c = String.valueOf(acctLst[0].Id) ));
        insert miTrendConfigList;
            
        controlOb.getstrMiTrendURL();
    
        ApexPages.currentpage().getParameters().put('accId',acctLst[0].Id);          
        ApexPages.currentpage().getParameters().put('opportunityId',oppList[0].Id);         
        ApexPages.StandardController setCon0 = new ApexPages.StandardController(acctLst[0]);                                 
        Transformation_AssetMassLinkController obj0 = new Transformation_AssetMassLinkController(setCon0);

        ApexPages.currentpage().getParameters().put('accId',acctLst[0].Id);          
        ApexPages.currentpage().getParameters().put('opportunityId',oppList[0].Id);         
        ApexPages.StandardController setCon1 = new ApexPages.StandardController(acctLst[0]);                                 
        Transformation_AssetMassLinkController obj1 = new Transformation_AssetMassLinkController(setCon1);
        
        ApexPages.currentpage().getParameters().put('accId',acctLst[1].Id);          
        ApexPages.currentpage().getParameters().put('opportunityId',oppList[1].Id);         
        ApexPages.StandardController setCon2 = new ApexPages.StandardController(acctLst[1]);                                 
        Transformation_AssetMassLinkController obj2 = new Transformation_AssetMassLinkController(setCon2);

        PageReference acctPage = new PageReference('/apex/Transformation_AssertMassLink?opportunityId='+oppList[0].id+'&accId='+assetList[0].Customer_Name__c);
                 
        Test.setCurrentPage(acctPage);
        
        Transformation_AssetMassLinkController controlOb1 = new Transformation_AssetMassLinkController(new ApexPages.StandardController(acctLst[0]));
        
        controlOb1.getoppsAssetsDetails();
        /* #1006 - Calling searchCustomerAsset method */
        controlOb1.searchCustomerAsset();
        controlOb1.customerAssetName = acctLst[0].id;
        controlOb1.searchCustomerAsset();
        for(Integer i=0; i < (math.mod(controlOb1.setCon.getResultSize(),controlOb1.setCon.getPageSize()) == 0?(controlOb1.setCon.getResultSize()/controlOb1.setCon.getPageSize()):(controlOb1.setCon.getResultSize()/controlOb1.setCon.getPageSize())+1) ; i++){
            List<Transformation_AssetMassLinkController.assWrapper> lstAssWrapper = new List<Transformation_AssetMassLinkController.assWrapper>();
            Transformation_AssetMassLinkController.assWrapper assWrapper = new Transformation_AssetMassLinkController.assWrapper(assetList[0]);
            assWrapper.selected = true;
            lstAssWrapper.add(assWrapper);
            controlOb1.listOfAssetWrapperlist.add(lstAssWrapper);
        }
        
        controlOb1.linkAssest();

        boolean result = controlOb1.hasResults;
        
        ApexPages.currentPage().getParameters().put('ObjectiveEditId',assetList[0].Id);
        controlObj.EditRecord();
        
        controlObj.cancelLink();
        ApexPages.currentPage().getParameters().put('ObjectiveDeleteId',assetList[0].Id);
        controlObj.DeleteRecord();
        
        controlOb1.getoppsAssetsDetails();
    Test.stopTest();
  }
}//end of classt