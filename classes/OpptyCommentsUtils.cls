/*==================================================================================================================+                                                                          
 |  DATE          DEVELOPER         WR        DESCRIPTION                               
 |  ====          =========         ==        ===========  
 | Jan/28/2015   Jaideep Mahato    398782    To Populate 5 most recent comments on Opportunity    
 ====================================================================================================================*/

public class OpptyCommentsUtils{
public Static Boolean isExecuted = false;

 // Included this code for Opportunity Page Re-Design
       public static void commentsHistory(List<Opportunity> NewopptyLst,Map<Id,Opportunity> OldopptyMap,Boolean isInsert){
        if(!isExecuted){
        List<Opportunity_Comments_History__c> commentsLst = new List<Opportunity_Comments_History__c>();
        Id rectypIdNextStep = Schema.SObjectType.Opportunity_Comments_History__c.RecordTypeInfosByName.get('Sales – Next Steps').RecordTypeId;
        Id rectypIdSolution = Schema.SObjectType.Opportunity_Comments_History__c.RecordTypeInfosByName.get('Presales – Solution Win').RecordTypeId;        
        Map<Id,List<Opportunity_Comments_History__c>> mapOppandCommentNotes = new Map<Id,List<Opportunity_Comments_History__c>>();
        Map<Id,List<Opportunity_Comments_History__c>> mapOppandCommentSolution = new Map<Id,List<Opportunity_Comments_History__c>>();
        List<Opportunity> opptyLst = new List<Opportunity>();
        
            
            if(isInsert){
                    opptyLst = [Select id,Next_Steps_new__c,Most_Recent_5_Solutions_Win_Comments__c,Solutions_Win_Comments__c,Next_Steps__c from Opportunity where id in :NewopptyLst];
            }
            else{
                    opptyLst.addAll(NewopptyLst);
            }
          
        
           for(Opportunity oppObj :opptyLst ){
                if((isInsert || (oppObj.Next_Steps_new__c!=OldopptyMap.get(oppObj.id).Next_Steps_new__c))&&oppObj.Next_Steps_new__c!=null){ 
                Opportunity_Comments_History__c commentsObj = new Opportunity_Comments_History__c();
                commentsObj.recordtypeid = rectypIdNextStep;
                commentsObj.Comments__c = oppObj.Next_Steps_new__c;
                commentsObj.OpportunityId__c = oppObj.id;
                commentsLst.add(commentsObj);
               }
               if((isInsert || (oppObj.Most_Recent_5_Solutions_Win_Comments__c!=OldopptyMap.get(oppObj.id).Most_Recent_5_Solutions_Win_Comments__c))&&oppObj.Most_Recent_5_Solutions_Win_Comments__c!=null){
                Opportunity_Comments_History__c commentsObj = new Opportunity_Comments_History__c();
                commentsObj.recordtypeid = rectypIdSolution;
                commentsObj.Comments__c = oppObj.Most_Recent_5_Solutions_Win_Comments__c;
                commentsObj.OpportunityId__c = oppObj.id;
                commentsLst.add(commentsObj);
                System.debug('commentsObj***** --->'+commentsObj);
               }
                  
           }
         
           
           if(!commentsLst.isEmpty())
              insert commentsLst;

            List<Opportunity_Comments_History__c> lstCommentsHistory = new List<Opportunity_Comments_History__c>([Select Creator_Name__c,RecordTypeId,CreatedDate,CreatedById,Comments__c,OpportunityId__c from Opportunity_Comments_History__c where OpportunityId__c in :NewopptyLst  order by CreatedDate desc ]);

            for(Opportunity_Comments_History__c commObj : lstCommentsHistory){
                if(commObj.Recordtypeid ==rectypIdNextStep){
                if(mapOppandCommentNotes.containsKey(commObj.OpportunityId__c)){
                    if(mapOppandCommentNotes.get(commObj.OpportunityId__c).size()<=4 )
                        mapOppandCommentNotes.get(commObj.OpportunityId__c).add(commObj);
                }
                else{
                    List<Opportunity_Comments_History__c> tempLst = new List<Opportunity_Comments_History__c>();
                    tempLst.add(commObj);
                    mapOppandCommentNotes.put(commObj.OpportunityId__c ,tempLst );
                }
                }
                else if(commObj.Recordtypeid ==rectypIdSolution){
                if(mapOppandCommentSolution.containsKey(commObj.OpportunityId__c)){
                    if(mapOppandCommentSolution.get(commObj.OpportunityId__c).size()<=4 )
                        mapOppandCommentSolution.get(commObj.OpportunityId__c).add(commObj);
                }
                else{
                    List<Opportunity_Comments_History__c> tempLst = new List<Opportunity_Comments_History__c>();
                    tempLst.add(commObj);
                    mapOppandCommentSolution.put(commObj.OpportunityId__c ,tempLst );
                }
                }
            }

            String commentString;
            
             for(Opportunity oppObj :opptyLst ){
                if((isInsert || (oppObj.Next_Steps_new__c != OldopptyMap.get(oppObj.id).Next_Steps_new__c))&&oppObj.Next_Steps_new__c!=null){ 
                commentString = '';
                for(Opportunity_Comments_History__c comObj : mapOppandCommentNotes.get(oppObj.id)){
                    commentString = commentString+comObj.Creator_Name__c+' '+comObj.Comments__c+'\n';
                    System.debug('commentString-------->'+commentString);
                    oppObj.Next_Steps__c = commentString;
                }
                }
                if((isInsert || (oppObj.Most_Recent_5_Solutions_Win_Comments__c!=OldopptyMap.get(oppObj.id).Most_Recent_5_Solutions_Win_Comments__c))&&oppObj.Most_Recent_5_Solutions_Win_Comments__c!=null){ 
                commentString = '';
                for(Opportunity_Comments_History__c comObj : mapOppandCommentSolution.get(oppObj.id)){
                    commentString = commentString+comObj.Creator_Name__c+' '+comObj.Comments__c+'\n';
                    System.debug('commentStringssss-------->'+commentString);
                    oppObj.Solutions_Win_Comments__c = commentString;
                }
                }
                
           }
           
          /* if(isInsert){
                update opptyLst;
           }*/
            isExecuted = true;

        }
         

       }
  
}