global class CoEEventDeepClone
{
    webservice static Id cloneAll(Id i)
    {
        System.debug('Inside CoE Event Clone Function'+i);
        CoE_Event__c coeEvent=[select Id,Name,Theater__c,Country__c,Date_of_engagement__c,Location__c,COE_offering__c,CoE_Sub_Offering__c,Current_tech_requirements__c,Current_Tech_Requirements_BU__c,Channel__c,Month__c,Qx_YY__c from CoE_Event__c where Id=:i limit 1];
        System.debug(coeEvent);
        //list of customers for the old CoE Event
        List<CoE_Customer__c> customersList=new List<CoE_Customer__c>();
        //List of customers for the cloned CoE event
        List<CoE_Customer__c> newCustomersList= new List<CoE_Customer__c>();
        
        CoE_Event__c newCoeEvent=coeEvent.clone(false,true);//deep clone
        System.debug('New CoE Event'+newCoeEvent);
        //newCoeEvent.Theater__c=coeEvent.Theater__c;
        newCoeEvent.Name=newCoeEvent.Name;
        insert newCoeEvent;
        
        customersList=[Select Id,Name,Account__c,Action_Items_from_the_meeting__c,Aligned_to_Country_Priority__c,APJ_Key_Priorities__c,Channel__c,CoE_Contact__c,CoE_Event__c,COE_Offering__c,COEs_Effort_Description__c,CoE_Sub_Offering__c,Country__c,Current_tech_requirements__c,Current_Tech_Requirements_BU__c,Date_of_engagement__c,Global_Alliances__c,Month__c,New_tech_requirement_expanding_deal__c,New_Tech_Requirements_BU__c,Partner_Customer__c,Priority__c,Qx_YY__c,RGMB_Bets__c,Sales_Contact__c,Sales_Stage__c,Strategic_Bets__c,Theater__c,Vertical__c from CoE_Customer__c where CoE_Event__c=:coeEvent.Id];
        
        if(!customersList.isEmpty())
        {
            for(CoE_Customer__c coeCustomer:customersList)
            {
                CoE_Customer__c newCoeCustomer=coeCustomer.clone(false,true);
                newCoeCustomer.CoE_Event__c=newCoeEvent.Id;
                //newCoeCustomer.Name=newcoeCustomer.Name+System.now();
                System.debug('Cloned new Customer'+newCoeCustomer);
                newCustomersList.add(newCoeCustomer);
                
            }
            insert newCustomersList;
        }
        System.debug(newCoeEvent.Id);
        return newCoeEvent.Id;                
    }
}