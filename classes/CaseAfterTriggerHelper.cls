public class CaseAfterTriggerHelper {
    
    public static boolean isInitialCompetitorUpdate = true;
    public static boolean isCaseAfterTriggerRunning = false;
    
    public static void CaseInitialCompetitorProductCreator(List<case> lstNewCase,Map<ID,case> nCaseMap){
        List<CaseCompetitorProduct__c> caseCompetitors = new List<CaseCompetitorProduct__c> ();
        Set<ID> mapCaseID = (new Map<ID,Case>(lstNewCase)).keySet();
        //On Competitor/Product update in case object, prevent inserting same junction object START
        List<CaseCompetitorProduct__c> objCompProd ;
        objCompProd = [select ID, case__c,case__r.subject,Competitor__c, Competitor_Product__c, Competitor_Product__r.name,  Competitor__r.Name, Competitor_Not_Listed__c, Competitor__r.CI_Wiki_Link__c, Competitor__r.Pricing_Calculator_Link__c,Competitor_Product__r.Competitive_One_Pager_Link__c,CI_Wiki__c, Competitive_One_Pager__c, Pricing_Calculator__c,createddate from CaseCompetitorProduct__c where case__c = :mapCaseID ORDER BY CreatedDate ASC];
        
        Map<String,CaseCompetitorProduct__c> mapOppCompProd = new Map<String,CaseCompetitorProduct__c>();
        
        for(CaseCompetitorProduct__c ocp : objCompProd){
            //mapOppCompProd.put(ocp.Competitor__r.Name+'+'+ocp.Competitor_Product__r.name, ocp);
            mapOppCompProd.put(ocp.Competitor__c+'+'+ocp.Competitor_Product__c, ocp);
        }
        //System.debug('**************************The mapOppCompProd::'+mapOppCompProd);
        //On Competitor/Product update in case object, prevent inserting same junction object START
        for(case c :lstNewCase){
            if( (mapOppCompProd.keyset().isEmpty()) || ( ( !mapOppCompProd.keySet().isEmpty()) && mapOppCompProd.get(c.Competitor_New__c+'+'+c.Competitor_Product_New__c) == null) ){//Prevent for not having same combination of Competitor in VF list
                System.debug('************************** New combination');
                CaseCompetitorProduct__c o = new CaseCompetitorProduct__c(case__c = c.id);
                o.competitor__c = c.Competitor_New__c;
                o.Competitor_Product__c = c.Competitor_Product_New__c;
                o.Is_Created_from_Case__c = true;
                caseCompetitors.add(o);
            }
            
        }
        
        if(caseCompetitors.size() > 0){
            CaseAfterTriggerHelper.isCaseAfterTriggerRunning = true;
            database.insert(caseCompetitors);
            List<CaseCompetitorProduct__c> caseCompsToUpdate = [select ID, case__c,Is_Created_from_Case__c from CaseCompetitorProduct__c where case__c IN :lstNewCase and Is_Created_from_Case__c = true and ID NOT IN :caseCompetitors];
            if(caseCompsToUpdate.size() > 0){
                for(CaseCompetitorProduct__c cx : caseCompsToUpdate){
                    cx.Is_Created_from_Case__c = false;
                }
                database.update(caseCompsToUpdate);
            }
            
        }
    }
    
    public static void CaseCompetitorProductCreator(List<case> lstNewCase){
        
        Map<ID,Case> mapOppIdCase = new Map<ID,Case>();
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosById();
        Map<String, CaseCompetitorProduct__c> caseCompetitors = new Map<String,CaseCompetitorProduct__c> ();
        List<ID> optyIDs = new List<ID>();
        Map<string,string> mapOptyNotListString = new Map<string,string>();
        for(case c :lstNewCase){
            String rType = rtMap.get(c.RecordTypeId).getName();
            if(rType == System.Label.CompetitiveIntelligence_RecordLabel){
                optyIDs.add(c.Opportunity_Name__c);
            }
        }
        if(optyIDs.size() > 0 ){
            List<OpportunityCompetitorProduct__c> optycomList = [select ID, Opportunity__c,Competitor_Not_Listed__c,Competitor_Product__c,Competitor__c from OpportunityCompetitorProduct__c where Opportunity__c = :OptyIds];
            for(OpportunityCompetitorProduct__c ocx : optyComList){
                mapOptyNotListString.put(''+ocx.Opportunity__c+ocx.Competitor__c+ocx.Competitor_Product__c, ocx.Competitor_Not_Listed__c );
            }
        }
        system.debug('-------------The mapOptyNotListString::'+mapOptyNotListString);
        
        for(Case c : lstNewCase){
            String rType = rtMap.get(c.RecordTypeId).getName();
            if(rType == System.Label.CompetitiveIntelligence_RecordLabel){
                System.debug('*** This Case is CI RecordType c.Opportunity_Name__r.Id = ' + c.Opportunity_Name__c);
                if(c.Opportunity_Name__c !=null){
                    mapOppIdCase.put(c.Opportunity_Name__c,c);
                }
                CaseCompetitorProduct__c o = new CaseCompetitorProduct__c(case__c = c.id);
                o.competitor__c = c.Competitor_New__c;
                o.Competitor_Product__c = c.Competitor_Product_New__c;
                system.debug('The mapping info::'+''+c.Opportunity_Name__c+o.competitor__c+o.Competitor_Product__c+'::');
                o.Competitor_Not_Listed__c = mapOptyNotListString.get(''+c.Opportunity_Name__c+o.competitor__c+o.Competitor_Product__c);
                caseCompetitors.put(c.id+''+o.competitor__c+''+o.Competitor_Product__c,o);
                System.debug('*** This Case is Competitor_New__c.Id = ' + c.Competitor_New__c + ' c.Competitor_Product_New__c ' +  c.Competitor_Product_New__c);
            }
        }
        List<CaseCompetitorProduct__c> caseCompsTobeInsert = caseCompetitors.values();
        database.insert(caseCompsTobeInsert);
        
        System.debug('*** mapOppIdCase keyset = ' + mapOppIdCase.keySet());
        
        if(!mapOppIdCase.isEmpty()){
            try{
                
                List<CaseCompetitorProduct__c> objCaseCompProd = new List<CaseCompetitorProduct__c>();
                List<OpportunityCompetitorProduct__c> objCompProd = [SELECT Id, Name,Opportunity__c, Competitor_Not_Listed__c, Competitor__c,Competitor_Product__c,Competitor__r.Name, Competitor_Product__r.Name,Competitor__r.CI_Wiki_Link__c, Competitor__r.Competitive_One_Pager_Link__c, Competitor__r.Pricing_Calculator_Link__c FROM OpportunityCompetitorProduct__c WHERE Opportunity__c=: mapOppIdCase.keySet() and Competitor__r.Active__c=true];
                Map<ID,List<OpportunityCompetitorProduct__c>> mapOppCompProd = new Map<ID,List<OpportunityCompetitorProduct__c>>();
                
                for(OpportunityCompetitorProduct__c ocp : objCompProd){
                    System.debug('*** in oppCopProd loop ocp.Opportunity__c = ' + ocp.Opportunity__c);
                    if(mapOppCompProd.get(ocp.Opportunity__c) != null){
                        System.debug('*** opp not in map');
                        mapOppCompProd.get(ocp.Opportunity__c).add(ocp);
                    }else {
                        System.debug('*** opp is in map');
                        List<OpportunityCompetitorProduct__c> ocs = new List<OpportunityCompetitorProduct__c>();
                        ocs.add(ocp);
                        mapOppCompProd.put(ocp.Opportunity__c, ocs);
                    }
                }
                System.debug('*** mapOppCompProd size = ' + mapOppCompProd.keySet().size());
                CaseCompetitorProduct__c cp;
                for(Case c : mapOppIdCase.values()){
                    System.debug('*** in Case loop');
                    if(mapOppCompProd.get(c.Opportunity_Name__c) != null){
                        System.debug('*** mapOppCompProd.get(c.Opportunity_Name__c) size = ' + mapOppCompProd.get(c.Opportunity_Name__c).size());
                        for(OpportunityCompetitorProduct__c ocp : mapOppCompProd.get(c.Opportunity_Name__c)){
                            System.debug('*** Creating caseCompProd');
                            cp = new CaseCompetitorProduct__c();
                            cp.Case__c = c.Id;
                            if(ocp.Competitor__c != null)
                                cp.Competitor__c = ocp.Competitor__c;
                            if(ocp.Competitor_Product__c != null)
                                cp.Competitor_Product__c = ocp.Competitor_Product__c;
                            system.debug('The mapping info::'+''+c.Opportunity_Name__c+cp.competitor__c+cp.Competitor_Product__c+'::');
                            cp.Competitor_Not_Listed__c = mapOptyNotListString.get(''+c.Opportunity_Name__c+cp.competitor__c+cp.Competitor_Product__c);
                            
                            if(!(caseCompetitors.containsKey(cp.Case__c+''+ocp.Competitor__c+''+cp.Competitor_Product__c))){    
                                objCaseCompProd.add(cp);
                            }
                        }
                    }
                }
                System.debug('*** objCaseCompProd size = ' + objCaseCompProd.size());
                CaseAfterTriggerHelper.isCaseAfterTriggerRunning = true;
                insert objCaseCompProd;                
                
            }catch(Exception ex){
                System.debug('*** Exception = ' +ex.getMessage());   
            }
        }
    }// End of CaseCompetitorProductCreator(List<case> lstNewCase)
    
} // End of CaseAfterTriggerHelper