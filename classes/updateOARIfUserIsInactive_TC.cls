/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 */
@isTest(SeeAllData = true)
public class updateOARIfUserIsInactive_TC 
{

    static Opportunity_Assignment_Rule__c testOAR = new Opportunity_Assignment_Rule__c();
    static Profile objProfile = [Select id,Name from Profile where name=:'System Administrator'];
    static List<User> testUserList = new List<User>();
    static Opportunity_Assignment_Rule__c oppAssigRuleRecord = new Opportunity_Assignment_Rule__c();
            
    public static testMethod void updateOARIfUserIsInactiveTestMethod() 
    { 
        //User record
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
        System.runAs(insertUser)
        {      
           testUserList.add(new User(
             Username='test1234'+Math.random()+'@emc.com.test',
             TimeZoneSidKey='America/New_York',
             LocaleSidKey='en_US',
             FirstName='Direct',
             LastName='Rep',
             ProfileId = objProfile.Id,
             email='john123Test@emc.com',
             Alias='test',
             EmailEncodingKey='ISO-8859-1',
             LanguageLocaleKey='en_US',
             Forecast_Group__c='Direct',
             BU__c='NA',
             Employee_Number__c='9323782818',
             IsActive=true) );
             
             insert testUserList;
        }
        
        //Insert OAR record
        oppAssigRuleRecord.Resource_Name__c = testUserList[0].Id;
        oppAssigRuleRecord.Resource_Sales_Force__c='EMC';
        oppAssigRuleRecord.Active__c=true;
        oppAssigRuleRecord.Partner_Type__c='All Partners';
        oppAssigRuleRecord.Coverage_Model__c='A-Hunting';
        oppAssigRuleRecord.Customer_Segment__c='Enterprise Tier 1';
        oppAssigRuleRecord.Resource_Opportunity_Sales_Team_Role__c='Channel Ã¢â‚¬â€œ Execution Sales Rep';        
        insert oppAssigRuleRecord;
        
        
        test.startTest();
            
            System.runAs(insertUser)
            {
                testUserList[0].isActive = false;
                update testUserList[0];
            }   
            updateOARIfUserIsInactive batchCall = new updateOARIfUserIsInactive();
            Database.executebatch(batchCall,200);
        test.stopTest();
        
    }
 }