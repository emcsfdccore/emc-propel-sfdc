/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |                                              Initial Creation.
 |  16.12.2013      Aagesh         WR319143      Re-written test class to improve performance.
 |  24.12.2013      Bhanuparakash  WR319143      Did Code clean-up for code-review.
 +===========================================================================*/
@isTest
public Class R2R_Lead_Conversion_TC
{
    public static testmethod void testsR2R_Lead_Conversion()
    {
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        }   
        //Creating Account
        Account acc= R2R_datahelper.createAccount();
        insert acc;

       List<Asset__c> listAssets=new List<Asset__c>();
        List<Asset__c> assetList = new List<Asset__c>();
        assetList = AssetTestClassDataHelper.createAsset(acc);//Bhanu- Created new method in Helper class 26-Dec-2013
        insert assetList; 
        
        //Creating Lead
         List<Lead> leadList = new List<Lead>();
         leadList = LeadTestClassDataHelper.createLead();        
         insert leadList;
      
        List<Opportunity> opptyList = new List<Opportunity>();
       // opptyList = OpportunityTestClassDataHelper.createOpptys(acc,leadList[0]);
        
        Opportunity Oppty1 = new Opportunity();
        Oppty1.Name = 'testoppty1';
        if (acc != null)
        Oppty1.AccountId = acc.id;
        Oppty1.Sales_Channel__c = 'Direct';
        Oppty1.Sales_Force__c = 'EMC';
        Oppty1.StageName='Pipeline';
        Oppty1.CloseDate=system.today();
        if (leadList[0]!= null){
        Oppty1.Related_Deal_Registration__c= leadList[0].id;
        opptyList.add(Oppty1);
        }      
        
        insert opptyList;
        
        List<Lead_Asset_Junction__c> listlaj= new List<Lead_Asset_Junction__c>();
        Lead_Asset_Junction__c laj1= new Lead_Asset_Junction__c();
        laj1.Related_Asset__c=assetList[1].id;
        laj1.Related_Lead__c=leadList[0].id;
        listlaj.add(laj1);
        /*
        Lead_Asset_Junction__c laj2= new Lead_Asset_Junction__c();
        laj2.Related_Asset__c=assetList[1].id;
        laj2.Related_Lead__c=leadList[1].id;  
        listlaj.add(laj2);
        */
        insert listlaj;
         
        List<String> listLeadIds= new List<String>();
            //for ( Lead l: leadList){
            
                listLeadIds.add(leadList[1].id);
           // }
            
            List<String> lstOpptyIds = new List<String>();
            for (Opportunity o: opptyList){
                lstOpptyIds.add(o.Id);
            }
            
        Database.BatchableContext bc;
        Database.QueryLocator dql;
        Test.startTest();
            
            system.debug('lstOpptyIds---->'+lstOpptyIds);
            R2R_Lead_Conversion LCobj=new R2R_Lead_Conversion(); 
            LCobj.MergeAsset(listLeadIds,lstOpptyIds);
            ID batchprocessid = Database.executeBatch(LCobj);
            dql=LCobj.start(bc);
            LCobj.execute(bc,listlaj);
            LCobj.finish(bc);
            Test.stopTest();
    }
}