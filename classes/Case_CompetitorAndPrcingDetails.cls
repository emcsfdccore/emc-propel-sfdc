public class Case_CompetitorAndPrcingDetails {
    
    public List<CaseCompetitorProduct__c> objCompProd {set;get;}
    public List<compProdWrap> cPWrapList {get;set;} 
    public boolean isItForSave {get;set;}
    public boolean isEditable {get;set;}
    public boolean isEditing {get;set;}
    public boolean isFirstSave {set;get;}
    public boolean isFirstEdit {set;get;}
    public boolean isFirstDelete {set;get;}
    public boolean isFirstRecord {set;get;}
    //public Opportunity currentOppty {set;get;}
    public Case currentCase {set;get;}
    public Case curentCase {set;get;}
    public ID caseId {set;get;}
    public boolean showAdd {set;get;}
    public boolean isAddSession {set;get;}
    public Map<ID,Competitor__c> existingComps {set;get;}
    public Map<ID,Competitor_Product__c> existingProds {set;get;}
    public List<CaseCompetitorProduct__c> editableJunction = new List<CaseCompetitorProduct__c> ();
    public List<CaseCompetitorProduct__c> notFirstEditableJunction = new List<CaseCompetitorProduct__c> ();
    public List<CaseCompetitorProduct__c> deletableJunction = new List<CaseCompetitorProduct__c> ();
    public Map<String,CaseCompetitorProduct__c> mapCompNProd = new Map<String,CaseCompetitorProduct__c>();
    public boolean displayErrorMsgTop { get;set;}
    public boolean displayErrorMsgBelow { get;set;}
    public Case_CompetitorAndPrcingDetails(ApexPages.StandardController std) {
    System.debug('**** std.getRecord() = ' + std.getRecord());
        currentCase = (Case) std.getRecord();
        Schema.DescribeFieldResult dfr = CaseCompetitorProduct__c.Case__c.getDescribe();      
        isEditable = true;//dfr.isUpdateable();
        caseId = std.getID();
        isItForSave = false;
        showAdd = true;
        isEditing = false;
        isFirstSave = false;
        isFirstEdit = false;
        isAddSession = false;
        isFirstDelete = false;
        objCompProd = new List<CaseCompetitorProduct__c>();
        existingComps = new Map<ID,Competitor__c>([Select Id, Name from Competitor__c]);
        existingProds = new Map<ID,Competitor_Product__c>([Select Id, Name from Competitor_Product__c]);
        System.debug('*** existingComps = ' + existingComps);
        getOppty();
        loadWrapperData();
        displayErrorMsgTop = true;
        displayErrorMsgBelow = false;
    }
    public void getOppty(){
        curentCase = [select Id, Competitor_New__c, Competitor_Product_New__c, Opportunity_Name__c from Case where Id =: currentCase.Id];
        System.debug('**** retrived Case = ' + curentCase);
        /*if(curentCase.Opportunity_Name__c != null){
        System.debug('*** currentOppty not null');
            currentOppty = [select id,stageName,AccountId,Account_Name1__c,Closed_Reason__c,Competitor__c,Close_Comments__c,Duplicate_Opportunity__c,Competitor_Lost_To__c,Competitor_Product__c, Product_Model__c,
Sales_Channel__c,Renewals_Close_Details__c,Closed_Reason_Action__c,Opportunity_type__c from Opportunity where id =: curentCase.Opportunity_Name__c];
        }*/
    }
    public void loadWrapperData(){
        cPWrapList = new List<compProdWrap>();        
        //objCompProd = [select ID, case__c,case__r.subject,Competitor__c, Competitor_Product__c, Competitor_Product__r.name,  Competitor__r.Name, Competitor_Not_Listed__c, Competitor__r.CI_Wiki_Link__c, Competitor__r.Pricing_Calculator_Link__c,Competitor_Product__r.Competitive_One_Pager_Link__c,CI_Wiki__c, Competitive_One_Pager__c, Pricing_Calculator__c,createddate from CaseCompetitorProduct__c where case__c = :caseID and Competitor__r.Active__c=true and Competitor_Product__r.Active__c=true ORDER BY CreatedDate ASC];
        objCompProd = [select ID,Auto_Create_Competitor_Asset__c, case__c,case__r.subject,Competitor__c, Competitor_Product__c, Competitor_Product__r.name,  Competitor__r.Name, Competitor_Not_Listed__c, Competitor__r.CI_Wiki_Link__c, Competitor__r.Pricing_Calculator_Link__c,Competitor_Product__r.Competitive_One_Pager_Link__c,CI_Wiki__c, Competitive_One_Pager__c, Pricing_Calculator__c,createddate,Is_Created_from_Case__c from CaseCompetitorProduct__c where case__c = :caseID ORDER BY CreatedDate ASC];
        //List<CaseCompetitorProduct__c> objCompProd1 = new List<CaseCompetitorProduct__c>();
        //List<CaseCompetitorProduct__c> objCompProd2 = new List<CaseCompetitorProduct__c>();
        //objCompProd1 = [select ID,Auto_Create_Competitor_Asset__c, case__c,case__r.subject,Competitor__c, Competitor_Product__c, Competitor_Product__r.name,  Competitor__r.Name, Competitor_Not_Listed__c, Competitor__r.CI_Wiki_Link__c, Competitor__r.Pricing_Calculator_Link__c,Competitor_Product__r.Competitive_One_Pager_Link__c,CI_Wiki__c, Competitive_One_Pager__c, Pricing_Calculator__c,createddate,Is_Created_from_Case__c from CaseCompetitorProduct__c where case__c = :caseID and Is_Created_from_Case__c = true ORDER BY CreatedDate desc ];
        //objCompProd2 = [select ID,Auto_Create_Competitor_Asset__c, case__c,case__r.subject,Competitor__c, Competitor_Product__c, Competitor_Product__r.name,  Competitor__r.Name, Competitor_Not_Listed__c, Competitor__r.CI_Wiki_Link__c, Competitor__r.Pricing_Calculator_Link__c,Competitor_Product__r.Competitive_One_Pager_Link__c,CI_Wiki__c, Competitive_One_Pager__c, Pricing_Calculator__c,createddate,Is_Created_from_Case__c from CaseCompetitorProduct__c where case__c = :caseID and Is_Created_from_Case__c != true ORDER BY CreatedDate ASC ];

        //objCompProd = new List<CaseCompetitorProduct__c>();
        //objCompProd.addAll(objCompProd1);
        //objCompProd.addAll(objCompProd2);
        for(CaseCompetitorProduct__c o : objCompProd){
            cPWrapList.add(new compProdWrap(o,true));
            if(o.Competitor_Product__c != null){
                mapCompNProd.put(o.Competitor__r.Name +'+' + o.Competitor_Product__r.name,o);
            } else {
                mapCompNProd.put(o.Competitor__r.Name +'+' + null,o);
            }
        }
    }
    public void addCaseCompProd(){
        isAddSession = true;
        CaseCompetitorProduct__c o = new CaseCompetitorProduct__c(case__c = caseID);
        cPWrapList.add(new compProdWrap(o,false));
        isItForSave = true;
    }
    public void deleteCaseCompProd(){
        Id delId = ApexPages.currentPage().getParameters().get('delId');
        List<ID> toBeDeleteIDs = new List<ID>();
        Boolean isFirstRecord = false;
        for(compProdWrap ow : cPWrapList){
        System.debug('*** Search for delets delId = '+ delId);
            isFirstRecord = true;
            if(ow.cpId == delId && isFirstRecord ){
                System.debug('**** morking deletable ');
                isFirstDelete = true;
            } 
            isFirstRecord = false;
        }
        
        boolean keepCurrentCaseCompProd = true;
        String currentCaseCompProdId = curentCase.Competitor_New__c+'_'+curentCase.Competitor_Product_New__c;      
        List<compProdWrap> cPWrapList_Temp = new List<compProdWrap>(); 
        for(compProdWrap ow : cPWrapList){
            if(ow.cpId != delId){
                cPWrapList_Temp.add(ow);
            }else{                
                toBeDeleteIDs.add(delId);
                String concatCompProdId = ''+ ow.cp.Competitor__r.Id +'_'+ ow.cp.Competitor_Product__r.Id;
                if (currentCaseCompProdId.equals( concatCompProdId )){
                    keepCurrentCaseCompProd = false;
                }
            }
        }
        if(toBeDeleteIDs.size() > 0){
            database.delete(toBeDeleteIDs);
        }
        cPWrapList = cPWrapList_Temp;
        
        if(isFirstDelete){
            System.debug('**** in isFirstDelete section oCPWrapList size = ' + cPWrapList.size());
            if(cPWrapList.size() == 0){
                System.debug('*** isFirstDelete, if size = 0');
                CaseAfterTriggerHelper.isInitialCompetitorUpdate = false;
                updateOppty(null,null);     
            }else {
                System.debug('*** isFirstDelete, if size > 0');
                System.debug('The total list of cPWrapList----->'+cPWrapList);
                CaseAfterTriggerHelper.isInitialCompetitorUpdate = false;
                CaseCompetitorProduct__c cpParam = new CaseCompetitorProduct__c();
                if (!keepCurrentCaseCompProd ) {
                cpParam = cPWrapList[0].cp;
                for(compProdWrap ow : cPWrapList){
                    if(cpParam.createdDate > ow.cp.createddate){
                        cpParam = ow.cp;
                    }
                }
                updateOppty(cPWrapList[0].cp.Competitor__r.Id, cPWrapList[0].cp.Competitor_Product__r.Id);  
                //updateOppty(cpParam.Competitor__r.Id, cpParam.Competitor_Product__r.Id);  
                }
            }
            isFirstDelete = false;
        }
        
    }
    public void editCaseCompProd(){
        isItForSave = true;
        showAdd = false;
        isEditing = true;
        isAddSession = false;
        editableJunction = new List<CaseCompetitorProduct__c> ();
         Boolean isThisFirstRecord = true;
        Id editId = ApexPages.currentPage().getParameters().get('editId');
        try{
        for(compProdWrap ow : cPWrapList){
        System.debug('*** Search for edits');
            if(isThisFirstRecord){
        System.debug('***isThisFirstRecord condition ');
                if(ow.cpId == editId && isThisFirstRecord){
                    isFirstEdit = true;
                }
                isThisFirstRecord = false;
            }else if(ow.cpId == editId) {//Editing other than first record
                notFirstEditableJunction.add(ow.cp);
            }
        }
        }catch(Exception e){
            System.debug('**** Exception : ' + e.getMessage());
        }
        System.debug('**** edit(), notFirstEditableJunction = ' + notFirstEditableJunction);
        List<compProdWrap> cPWrapList_Temp = new List<compProdWrap>(); 
        for(compProdWrap ow : cPWrapList){
            if(ow.cpId == editId){
                ow.flag = false;
            }
        }        
    }
    public void saveCaseCompProd(){
        showAdd = true;
        isEditing = false;
        Boolean isError = false;
        String compName;
        Competitor__c tempComp;
        Competitor_Product__c tempProd;
        displayErrorMsgBelow = false;
        displayErrorMsgTop = true;
        String firstCName = null;
        String firstCPName = null;
        isAddSession = false;
        List<Asset__c> optyCompAsset = new List<Asset__c>();
        
        List<compProdWrap> cPWrapList_Temp = new List<compProdWrap>(); 
        List<CaseCompetitorProduct__c> caListTemp = new List<CaseCompetitorProduct__c>();
        String compProdName;
        System.debug(':::The proceced records from UI before refine, cPWrapList:::::::::::'+cPWrapList);
        for(compProdWrap ow : cPWrapList){
            if(ow.cp.Competitor__c != null || ow.cp.Competitor_Product__c != null || ow.cp.Competitor_Not_Listed__c!=null ){ //Changes made by Renu
                cPWrapList_Temp.add(ow);
            }
        }
        cPWrapList = cPWrapList_Temp;
        cPWrapList_Temp = new List<compProdWrap>(); 
        //System.debug(':::The proceced records from UI after refine, cPWrapList:::::::::::'+cPWrapList);
        //ApexPages.Message myX = new ApexPages.Message(ApexPages.Severity.ERROR,'accccc.');
                    //ApexPages.addMessage(myX);
        for(compProdWrap ow : cPWrapList){
       // compName = existingComps.get(ow.cp.Competitor__c).Name;//ow.cp.Competitor_r.Name;
        //compProdName =existingProds.get(ow.cp.Competitor_Product__c).Name;// ow.cp.Competitor_Product_r.Name;
        
      System.debug('*** ow.cp.Competitor_Not_Listed__c= ' + ow.cp.Competitor_Not_Listed__c);

        
            if(ow.flag == false && mapCompNProd.get(compName + '+' + compProdName) == null){
                            tempComp = existingComps.get(ow.cp.Competitor__c);
                tempProd = existingProds.get(ow.cp.Competitor_Product__c);
                           
                // initialize the isComNListed inside the for-loop
                Boolean isComNListed = false;           
                if(ow.cp.Competitor_Not_Listed__c != null && ow.cp.Competitor_Not_Listed__c.length() > 0)
                    isComNListed = true;
                
                if(tempComp == null && isComNListed ){//if Comp. Null and NListed field not empty
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Competitor_Not_Listed_Donot_See_Competitor_List_Error_Msg);
                    ApexPages.addMessage(myMsg);
                    isError = true;
                    isAddSession = true;
                    displayErrorMsgBelow = true;
                    displayErrorMsgTop = false;                        
                } else if((tempComp != null && tempComp.name == 'Not Listed') && !isComNListed){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Competitor_Not_Listed_Error_Msg);
                    ApexPages.addMessage(myMsg);
                    isError = true;
                    isAddSession =true;
                    displayErrorMsgBelow = true;
                    displayErrorMsgTop = false;                     
                } else if((tempComp != null && tempComp.name != 'Not Listed') && isComNListed){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.Competitor_Not_Listed_Field_Populate_Error_Msg);
                    ApexPages.addMessage(myMsg);
                    isError = true;
                    isAddSession =true;
                    displayErrorMsgBelow = true;
                    displayErrorMsgTop = false;                     
                
          /*//Changes made by Renu***Starting
          else if((tempComp != null && tempComp.name != 'Not Listed') && !isComNListed && tempProd == null){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter the Competitor Product associated with the selected Competitor');
                    ApexPages.addMessage(myMsg);
                    isError = true;
                    isAddSession =true;
        
          //Changes made by Renu***End  */       

                } else {
                        if(tempComp != null && firstCName == null){
                            System.debug('*** setting first comp. name ');
                                firstCName = tempComp.Id;
                        if(firstCPName == null && tempProd != null){  //Updated by Srinivas
                            System.debug('*** setting first comp. Prod name ');
                                firstCPName = tempProd.Id;
                        }
                    }
                    
                    //Create Asset object
                    if(ow.cp.Auto_Create_Competitor_Asset__c){
                        System.debug('**** adding new Asset**** ow.ocp.Competitor__r.Name = ' + tempComp.name);
                        //optyCompAsset.add(createAsset(tempComp, ow.cp));
                    }
                    caListTemp.add(ow.cp);
                    cPWrapList_Temp.add(ow);  
                }
            }
        }
        if(firstCName != null){
                isFirstSave = true;
        }
        //Update Oppty if isFirstSave = true
        if(isFirstSave){// && (curentCase.Competitor_New__c == null  && curentCase.Competitor_Product_New__c == null )){
            System.debug('**** updating Case**** curentCase = ' + curentCase);  
            CaseAfterTriggerHelper.isInitialCompetitorUpdate = false;
                CaseCompetitorProduct__c cpParam = new CaseCompetitorProduct__c();
                cpParam = cPWrapList[0].cp;
                
            for(compProdWrap ow : cPWrapList){
                    if(ow.cp.Is_Created_from_Case__c == true || cpParam.Is_Created_from_Case__c == true){
                        cpParam = ow.cp;
                        break;
                    }
                    else if(cpParam.createdDate > ow.cp.createddate){
                        cpParam = ow.cp;
                    }
                }
		
            system.debug('********* cPWrapList case compitotior product:::'+cpWrapList);
            system.debug('********* cpParam case compitotior product:::'+cpParam);
            //updateOppty(cPWrapList[0].cp.Competitor__r.Id, cPWrapList[0].cp.Competitor_Product__r.Id);  
            updateOppty(cpParam.Competitor__c, cpParam.Competitor_Product__c); 
            //updateOppty(cpParam.Competitor__r.Id, cpParam.Competitor_Product__r.Id); 
            //updateOppty(firstCName, firstCPName);
        }
        if(!isError){
            system.debug('The upserting records of CaseCompetitorProduct__c::::::'+caListTemp);
            system.debug('*** editableJunction = ' + editableJunction.size());
            system.debug('*** notFirstEditableJunction = ' + notFirstEditableJunction.size());
            CaseAfterTriggerHelper.isInitialCompetitorUpdate = false;
            try{
            System.debug('*** No errors : Upserting competitors BEFORE');
                system.Database.upsert(caListTemp); 
                System.debug('*** No errors : Upserting competitors AFTER');
            }catch(Exception e){
                //if(e.getMessage().contains('Value does not exist or does not match filter criteria')){
                System.debug('*** in upsert exception');
                 //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select valied Competitor for Product'));
                //return null;
                //}
            }           
            for(compProdWrap ow : cPWrapList_Temp){
                ow.flag = true;
                ow.cpId = ow.cp.id;
            }
            isItForSave = false;
            loadWrapperData(); 
            
            if(isFirstEdit){
                CaseCompetitorProduct__c cpParam = new CaseCompetitorProduct__c();
                cpParam = cPWrapList[0].cp;
                for(compProdWrap ow : cPWrapList){
                    if(ow.cp.Is_Created_from_Case__c == true || cpParam.Is_Created_from_Case__c == true){
                        cpParam = ow.cp;
                        break;
                    }
                    if(cpParam.createdDate > ow.cp.createddate){
                        cpParam = ow.cp;
                    }
                }
                system.debug('********* cPWrapList case compitotior product:::'+cpWrapList);
                system.debug('********* cpParam case compitotior product:::'+cpParam);
                //updateOppty(cPWrapList[0].cp.Competitor__r.Id, cPWrapList[0].cp.Competitor_Product__r.Id);  
                updateOppty(cpParam.Competitor__r.Id, cpParam.Competitor_Product__r.Id);  
                System.debug('**** !editableJunction.isEmpty()');
                if(cPWrapList[0].cp.Competitor_Product__c != null){
                    //updateOppty(cPWrapList[0].cp.Competitor__r.Id,cPWrapList[0].cp.Competitor_Product__r.Id);
                } else {
                    //updateOppty(cPWrapList[0].cp.Competitor__r.Id,null);    
                }
                isFirstEdit = false;                
            }
        }//if !isError  
    }
     public void caseCancel(){
        isItForSave = false;
        showAdd = true;
        isEditing = false;
        isAddSession = false;
       List<compProdWrap> tempWrapList = new List<compProdWrap>();
        for(compProdWrap ow : cPWrapList){
            if(ow.cp != null && ow.cpid != null)
           {
              ow.flag = true;
              tempWrapList.add(ow);
            }
         }
       cPWrapList = new List<compProdWrap>();
             cPWrapList = tempWrapList;
             
      system.debug('********* cPWrapList:::'+cpWrapList);
}

    public class compProdWrap{
        public ID cpId {get;set;}
        public CaseCompetitorProduct__c cp {get;set;}
        public boolean flag {get;set;}
        compProdWrap(CaseCompetitorProduct__c o, boolean isDisplay){
            cp = o;
            cpId = o.id;
            flag = isDisplay;
        }
    }
    public void updateOppty(ID compID, Id compProdID){
        try{
           System.debug('**** Competitor ID = ' + compID); 
           System.debug('**** in updateOppty method curentCase = ' + compProdID);
            
                curentCase.Competitor_New__c = compID;
                curentCase.Competitor_Product_New__c = compProdID;
                update curentCase;
                System.debug('**** in updateOppty method curentCase = ' + curentCase);
        }catch(Exception e){
            System.debug('****Exception updateing case message = ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Exception : ' + e.getMessage());
                ApexPages.addMessage(myMsg);
        }
    }
    
    
    
}