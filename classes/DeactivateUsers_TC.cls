/*============================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE       DEVELOPER     WR       		DESCRIPTION                               
 |  ====       =========     ==       		=========== 
 |  18/07/2013  Sneha Jain  WR 261820 		Test Class for DeactivateUsers
 |  19/12/2013  Srikrishna  Backward Arrow 	Test Class modified for optimization
 +============================================================================*/
  
@isTest
private class DeactivateUsers_TC 
{
    static TestMethod void deactivateUserFromContactTestMethod(){
	    //Query profile of System Admin
	    Profile pId = [select id from profile where name='System Administrator' limit 1];
	    User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
    
	    //Create Custom Setting Data required for the test class
	    System.runAs(insertUser){
		    CustomSettingDataHelper.eBizSFDCIntCSData();
		    CustomSettingDataHelper.dataValueMapCSData();
		    CustomSettingDataHelper.userDeactivationNotidication();
	    }
      
    //Create user record
    	System.runAs(insertUser){   
        //List<User> testUserList = new List<User>();
	        List<User> testUserList = userHelper(pId.Id);
	        testUserList[0].email='testSample1@test.com';
	        testUserList[1].email='testSample1@test.com';
	        testUserList[2].email='testSample2@test.com';
	        testUserList[3].email='testSample2@test.com';
	        testUserList[4].email='testSample3@test.com';
	        
	        if(!testUserList.isEmpty())          
	        insert testUserList;
    	}
        //Create Account record
        Account testAccount = new Account(Name = 'EMC (Internal) Account');
        insert testAccount;

        List<RecordType> contactRT = [Select Id From RecordType where Name='EMC Internal Contact'];
        //Create Contact record
        
        //List<Contact> testContactList = new List<Contact>();
        List<Contact> testContactList = contactHelper(testAccount.Id, contactRT[0].Id);
        insert testContactList; 

        Test.startTest();
		for(Integer i=0; i<3; i++){
			testContactList[i].Active__c = false;
        	testContactList[i].Inactive_Reason__c = 'Left Company';
		}
        if(!testContactList.isEmpty())
        update testContactList;
        
        
        //Changes made to cover the Scheduler class and Recall Batch class
        String CRON_EXP = '0 0 * */1 * ?';
          
        List<CronTrigger> cronTriggerToAbort = [Select c.State, c.NextFireTime, c.Id, c.CronExpression From CronTrigger c where CronExpression =: CRON_EXP];
          
        //Iterating over cronTriggerToAbort list
        for(Integer i = 0 ; i < cronTriggerToAbort.size() ; i++){
            
            System.abortJob(cronTriggerToAbort[i].Id);
        }
        
        UserDeactivationScheduler sch = new UserDeactivationScheduler();
        system.schedule('Test User Deactivation',CRON_EXP,sch);
        
        Test.stopTest();
    }
    
    private static List<Contact> contactHelper(Id accId, Id recTypeId){
    	List<Contact> conList = new List<Contact>();
    	for(Integer i=0; i<3; i++){
    		Contact con = new Contact(FirstName='TestFirstName'+i+1, LastName='TestLastName'+i+1,AccountId = accId, Email = 'testSample'+i+1+'@test.com',Active__c = true,RecordTypeID = recTypeId);
    		conList.add(con);
    	}
    	return conList;
    }
    
    private static List<User> userHelper(Id pId){
    	List<User> uList = new List<User>();
    	for(Integer i=0; i<5; i++){
    		User newUser = new User(alias = 'utest'+i+1,
			                  emailencodingkey='UTF-8', lastname='Unit Test'+i+1, 
			                  languagelocalekey='en_US',
			                  localesidkey='en_GB', profileid = pId,
			                  timezonesidkey='Europe/London', 
			                  username='test'+i+1+'.test@emc.com');
            uList.add(newUser);      
    	}
    	return uList;
    }
}