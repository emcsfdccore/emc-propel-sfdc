/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
|  01/Sep/2014  Jaypal Nimesh       BPP Project     Created class for Funded Headcount logic.
|
+=====================================================================================================*/

public class PRM_BPP_Funded_Headcount{
    
    //Method to update the Total count of funded heads field on account
    public static void updateFundedHeadCount(Set<Id> AccountIdSet){
        
        //Initializing Map & list
        Map<Id, List<Contact>> accIdConListMap = new Map<Id, List<Contact>>();
        List<Account> accList = new List<Account>();
        
        //Querying the contacts with funded heads
        List<Contact> fundedContactList = [Select Id, AccountId from Contact where Funded_Head__c = true AND AccountId IN: AccountIdSet];
        
        if(fundedContactList != null && fundedContactList.size() > 0){
            
            //Copy the original to new set to remove the Account Id which still has at least 1 funded head 
            Set<Id> zeroFundedHeadSet = new Set<Id>();
            zeroFundedHeadSet.addAll(AccountIdSet);
            
            //Iterating and filling the map to bulkify the code
            for(Contact con : fundedContactList){
                
                //This set will be left with Account ids which has zero funded heads now
                zeroFundedHeadSet.remove(con.AccountId);
                
                if(accIdConListMap.containsKey(con.AccountId)){                
                    accIdConListMap.get(con.AccountId).add(con);
                }
                else{
                    accIdConListMap.put(con.AccountId, new List<Contact>{con});
                }
            }           
            //Checks if map has items to process
            if(accIdConListMap != null && accIdConListMap.size() > 0){
                    
                //Iterating over map keyset and creting instance of account for DML
                for(Id accId : accIdConListMap.keySet()){
                    
                    Account acc = new Account(Id = accId, Total_Count_of_Funded_Heads__c = accIdConListMap.get(accId).size());
                    accList.add(acc);
                }
                // Updating account list
                Database.SaveResult[] srList = Database.update(accList, false);
            }
            //Calling method to update field to zero
            if(zeroFundedHeadSet != null && zeroFundedHeadSet.size() > 0){              
                updateFundedHeadCountToZero(zeroFundedHeadSet);
            }
        }
        else{
            //Calling method to update field to zero        
            updateFundedHeadCountToZero(AccountIdSet);
        }
    }
    
    //Method to update the Total count of funded head to zero
    public static void updateFundedHeadCountToZero(Set<Id> AccIdSet){
        
        List<Account> accListToUpdate = new List<Account>();
        
        for(Id accId : AccIdSet){
            
            //Setting the field value to null in place of zero
            Account acc = new Account(Id = accId, Total_Count_of_Funded_Heads__c = null);
            accListToUpdate.add(acc);
        }       
        // Updating account list
        Database.SaveResult[] srList = Database.update(accListToUpdate, false);
    }
}