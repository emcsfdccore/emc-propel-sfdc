/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  29.10.2012      Smitha Thomas       MOJO    Initial Creation.Creating this test class to unit test 
                                                SFA_MOJO_RegisterWithGFSController class.
 |  30.10.2012      Avinash Kaltari     MOJO    Updated test class and added test methods to increase coverage
 |  21.11.2013      Sneha Jain          R2R     Optimized test class to create test data instead of querying
 |  27.03.2014      Sneha Jain          R2R     Worked on test class to increase the code coverage
 |  23.06.2014      Vivek Barange       991     Modified Test class to cover WR 991 changes 
 |  19.11.2014      Vivek Barange       1483     Modified Test class to cover WR 1483 changes
+========================================================================================================================*/ 

@isTest
private class SFA_MOJO_RegisterWithGFSController_TC 
{
    static testMethod void registerWithGFSTest()
    {    
        //Custom Setting data creation
        System.runAs(new User(id= UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
        }
        
        //Insert user record with System Admin profile
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='testR2Ruser3@emailR2R3Test.com', alias='tuser1', CommunityNickName='tuser1', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User'); 
        insert insertUser;
        
        String emcInstallRecordType = '';
        String competitiveRecordType = '';
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where RecordType.DeveloperName like 'EMC_Install' or Name Like 'Competitive_Install'];
        if(recordType[0].DeveloperName == 'EMC_Install')
        {
            emcInstallRecordType = recordType[0].id;
            competitiveRecordType = recordType[1].id;
        }
        if(recordType[0].DeveloperName == 'Competitive_Install')
        {
            competitiveRecordType = recordType[0].id;
            emcInstallRecordType = recordType[1].id;
        }
        
        System.runAs(insertUser)
        {
        
            //Creating and inserting account
            //1483 changes adding Status__c = 'A'
            List<Account> accList = new List<Account>();
            accList.add(new Account(Name = 'TestAcount0', Status__c = 'A' ));
            accList.add(new Account(Name = 'TestAcount1',Customer_Profiled_Account__c = true,BillingCountry = 'India',Account_Flag__c = 'Primary', Status__c = 'A' ));
            accList.add(new Account(Name = 'TestAcount2',Customer_Profiled_Account__c = true,BillingCountry = 'United Kingdom',Account_Flag__c = 'Primary', Status__c = 'A' ));
            accList.add(new Account(Name = 'TestAcount3',Customer_Profiled_Account__c = true,BillingCountry = 'United States',Account_Flag__c = 'Primary', Status__c = 'A' ));
            insert accList;

            //Contact record creation
            Contact con = new Contact(Lastname = 'Test Contact', AccountId = accList[0].Id);

            //Opportunity record creation
            List<Opportunity> oppList = new List<Opportunity>();
            oppList.add(new Opportunity(Name = 'TestOpp0',AccountId=accList[0].Id,Sales_Force__c = 'EMC',CloseDate = System.today(),StageName = 'Pipeline'));
            oppList.add(new Opportunity(Name = 'TestOpp1',AccountId=accList[1].Id,Sales_Force__c = 'EMC',CloseDate = System.today()+1,StageName = 'Pipeline',Opportunity_Number__c='12',Amount=1000,Products__c='testProduct1'));
            oppList.add(new Opportunity(Name = 'TestOpp2',AccountId=accList[2].Id,Sales_Force__c = 'EMC',CloseDate = System.today()+2,StageName = 'Pipeline'));
            oppList.add(new Opportunity(Name = 'TestOpp3',AccountId=accList[3].Id,Sales_Force__c = 'EMC',CloseDate = System.today()+3,StageName = 'Pipeline'));
            oppList.add(new Opportunity(Name = 'TestOpp4',AccountId=accList[3].Id,Sales_Force__c = 'EMC',CloseDate = System.today()+4,StageName = 'Pipeline'));
            oppList.add(new Opportunity(Name = 'TestOpp5',AccountId=accList[3].Id,Sales_Force__c = 'EMC',CloseDate = System.today()+5,StageName = 'Pipeline',Opportunity_Number__c='123',Amount=500,Products__c='testProduct1'));
            oppList.add(new Opportunity(Name = 'TestOpp6',AccountId=accList[1].Id,Sales_Force__c = 'EMC',CloseDate = System.today()+6,StageName = 'Pipeline',Opportunity_Number__c='1234',Amount=2000,Products__c='testProduct2'));
            oppList.add(new Opportunity(Name = 'TestOpp7',AccountId=accList[1].Id,Sales_Force__c = 'EMC',CloseDate = System.today()+7,StageName = 'Pipeline',Opportunity_Number__c='12345',Amount=3000,Products__c='testProduct3'));
            /* @991 - add one more instance for Opportunity object */
            oppList.add(new Opportunity(Name = 'TestOpp8',AccountId=accList[1].Id,Sales_Force__c = 'EMC',CloseDate = System.today()+7,StageName = 'Pipeline',Opportunity_Number__c='123456',Amount=3000,Products__c='testProduct3'));
            insert oppList;       
                    
            //Asset record creation
            List<Asset__c> assetList = new List<Asset__c>();
            assetList.add(new Asset__c(Name ='TestAsset0',Customer_Name__c=accList[0].Id,Configuration_Details__c='test0', Total_Raw_Capacity_GB__c = 'test0',Estimated_Raw_Capacity_TB__c = 10,RecordTypeId = emcInstallRecordType,Product_Family__c ='CONNECTRIX',Swap_Trade_In_Cost_Relief_Value__c=1));
            assetList.add(new Asset__c(Name ='TestAsset1',Customer_Name__c=accList[1].Id,Configuration_Details__c='test1', Total_Raw_Capacity_GB__c = 'test1'));
            assetList.add(new Asset__c(Name ='TestAsset2',Customer_Name__c=accList[2].Id,Configuration_Details__c='test2', Total_Raw_Capacity_GB__c = 'test2'));
            assetList.add(new Asset__c(Name ='TestAsset3',Customer_Name__c=accList[3].Id,Configuration_Details__c='test3', Total_Raw_Capacity_GB__c = 'test3'));
            assetList.add(new Asset__c(Name ='TestAsset4',Customer_Name__c=accList[3].Id,Configuration_Details__c='test4', Total_Raw_Capacity_GB__c = 'test3',Product_Family__c ='test',Estimated_Raw_Capacity_TB__c = 10,RecordTypeId = emcInstallRecordType));
            assetList.add(new Asset__c(Name ='TestAsset5',Customer_Name__c=accList[3].Id,Configuration_Details__c='test5', Total_Raw_Capacity_GB__c = 'test3',Product_Family__c ='CONNECTRIX',Estimated_Raw_Capacity_TB__c = 20));
            assetList.add(new Asset__c(Name ='TestAsset6',Customer_Name__c=accList[0].Id,Configuration_Details__c='test6', Total_Raw_Capacity_GB__c = 'test6',Estimated_Raw_Capacity_TB__c = 30,RecordTypeId = emcInstallRecordType,Product_Family__c ='CONNECTRIX',Swap_Trade_In_Cost_Relief_Value__c=1));
            assetList.add(new Asset__c(Name ='TestAsset7',Customer_Name__c=accList[0].Id,Configuration_Details__c='test7', Total_Raw_Capacity_GB__c = 'test7',Estimated_Raw_Capacity_TB__c = 40,RecordTypeId = competitiveRecordType,Product_Family__c ='test',Swap_Trade_In_Cost_Relief_Value__c=2,Drive__c=2,X1_Drive__c=2,X2_Drive_Capacity_GB__c=2,X2_Drive__c=2,X3_Drive_Capacity_GB__c=2,X3_Drive_Quantity__c=2,X4_Drive_Capacity_GB__c=2,X4_Drive_Quantity__c=2,X5_Drive_Capacity_GB__c=2,X5_Drive_Quantity__c=2,Product_Name_Vendor__c='Dell',Model__c='2145-8A4'));
            /* @991 - add one more instance for Asset object */
            
            assetList.add(new Asset__c(Name ='TestAsset8',Customer_Name__c=accList[0].Id,Configuration_Details__c='test6', Total_Raw_Capacity_GB__c = 'test6',Estimated_Raw_Capacity_TB__c = 30,RecordTypeId = emcInstallRecordType,Product_Family__c ='CONNECTRIX',GFS_Rep_Incentive_Num__c=10));
            insert assetList;

            //Opportunity Asset Junction record creation
            List<Opportunity_Asset_Junction__c> oajList= new List<Opportunity_Asset_Junction__c>();
            oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList[0].Id, Related_Opportunity__c = oppList[1].Id));
            oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList[1].Id, Related_Opportunity__c = oppList[2].Id));
            oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList[2].Id, Related_Opportunity__c = oppList[3].Id));
            oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList[3].Id, Related_Opportunity__c = oppList[4].Id));
            oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList[4].Id, Related_Opportunity__c = oppList[5].Id));
            oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList[6].Id, Related_Opportunity__c = oppList[6].Id));
            oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList[7].Id, Related_Opportunity__c = oppList[7].Id));
            /* @991 - add one more instance for Opportunity_Asset_Junction__c object */
            oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = assetList[8].Id, Related_Opportunity__c = oppList[8].Id));
            insert oajList;
            
            
            //OAR record creation
            List<OAR_Member_Added__c> OARlist = new List<OAR_Member_Added__c>();
            OARlist.add(new OAR_Member_Added__c(Name = 'TestOAR1'));
            OARlist.add(new OAR_Member_Added__c(Name = 'TestOAR2'));
            insert OARlist;
     
            //Trade Ins Competitive Swap record creation
            List<Trade_Ins_Competitive_Swap__c> lstTradeIns = new List<Trade_Ins_Competitive_Swap__c>();
            List<Trade_Ins_Competitive_Swap__c> tradeInList = new List<Trade_Ins_Competitive_Swap__c>();
            tradeInList.add(new Trade_Ins_Competitive_Swap__c(Related_Opportunity__c = oppList[0].Id,Registration_ID__c = 'test0', Swap_Value__c = 1));
            tradeInList.add(new Trade_Ins_Competitive_Swap__c(Related_Opportunity__c = oppList[0].Id,Registration_ID__c = 'test1', Swap_Value__c = 2));
            tradeInList.add(new Trade_Ins_Competitive_Swap__c(Related_Opportunity__c = oppList[1].Id,Registration_ID__c = 'test2', Swap_Value__c = 3,Registration_Type__c='Connectrix Trade In'));
            tradeInList.add(new Trade_Ins_Competitive_Swap__c(Related_Opportunity__c = oppList[7].Id,Registration_ID__c = 'test3', Swap_Value__c = 4,Registration_Type__c='Connectrix Trade In'));
            insert tradeInList;
            
            Test.startTest();
                
                ApexPages.currentpage().getParameters().put('id',oppList[0].Id);
                ApexPages.StandardController oppCont0 = new ApexPages.StandardController(oppList[0]);
                SFA_MOJO_RegisterWithGFSController cont0 = new SFA_MOJO_RegisterWithGFSController(oppCont0);
                cont0.RegisterGFS();
                
                ApexPages.currentpage().getParameters().put('id',oppList[1].Id);
                ApexPages.StandardController oppCont1 = new ApexPages.StandardController(oppList[1]);
                SFA_MOJO_RegisterWithGFSController cont1 = new SFA_MOJO_RegisterWithGFSController(oppCont1);
                cont1.RegisterGFS();
                
                ApexPages.currentpage().getParameters().put('id',oppList[2].Id);
                ApexPages.StandardController oppCont2 = new ApexPages.StandardController(oppList[2]);
                SFA_MOJO_RegisterWithGFSController cont2 = new SFA_MOJO_RegisterWithGFSController(oppCont2);
                cont2.RegisterGFS();
                
                ApexPages.currentpage().getParameters().put('id',oppList[3].Id);
                ApexPages.StandardController oppCont3 = new ApexPages.StandardController(oppList[3]);
                SFA_MOJO_RegisterWithGFSController cont3 = new SFA_MOJO_RegisterWithGFSController(oppCont3);
                cont3.RegisterGFS();
                cont3.sendEmail(OARlist);
                cont3.cancel();
                SFA_MOJO_RegisterWithGFSController.updateReRegistrationDetails(oppList,'testReason',true);
                cont3.getLinkedAssetTypeOnOpportunity(oajList);
                
                ApexPages.currentpage().getParameters().put('id',oppList[4].Id);
                ApexPages.StandardController oppCont4 = new ApexPages.StandardController(oppList[4]);
                SFA_MOJO_RegisterWithGFSController cont4 = new SFA_MOJO_RegisterWithGFSController(oppCont4);
                cont4.RegisterGFS();
                
                ApexPages.currentpage().getParameters().put('id',oppList[5].Id);
                ApexPages.StandardController oppCont5 = new ApexPages.StandardController(oppList[5]);
                SFA_MOJO_RegisterWithGFSController cont5 = new SFA_MOJO_RegisterWithGFSController(oppCont5);
                cont5.RegisterGFS();

                ApexPages.currentpage().getParameters().put('id',oppList[6].Id);
                ApexPages.StandardController oppCont6 = new ApexPages.StandardController(oppList[6]);
                SFA_MOJO_RegisterWithGFSController cont6 = new SFA_MOJO_RegisterWithGFSController(oppCont6);
                cont6.RegisterGFS();
                
                ApexPages.currentpage().getParameters().put('id',oppList[7].Id);
                ApexPages.StandardController oppCont7 = new ApexPages.StandardController(oppList[7]);
                SFA_MOJO_RegisterWithGFSController cont7 = new SFA_MOJO_RegisterWithGFSController(oppCont7);
                cont7.RegisterGFS();
                
            Test.stopTest();
        }   
    }
}