/**

Created By      :   Avinash Kaltari
Created Date    :   24 May 2012
Purpose         :   To increase the coverage of TestClassUtils class

Modified By     :   Vivek Barange
Modified Date   :   23 june 2014
WR              :   989
Purpose         :   To increase the coverage of TestClassUtils class

Modified By     :   Vinod Jetti
Modified Date   :   17 Feb 2015
WR              :   1649
Purpose         :   To increase the coverage of TestClassUtils class                                              
*/   
@IsTest 
private class TestClassUtils_TC 
{
    
    static testMethod void TestClassUtilsTest() 
    {
        CustomSettingDataValueMap__c cs = testclassUtils.getCustomSetRec('Name', 'Value');
        
        
        testclassUtils.InsertCustomSetRec('Name', 'Value');
        
        //testclassUtils.getCustomSettingrec_Profiles('Name Prasad111');
        
        DealRegistration_CustomSetting__c drcs = testclassUtils.getCustomSettingrec_dealreg('name', 'pscQueueId');
        
        OpportunityIntegration__c opics = testclassUtils.getCustomSettingrec_opptyIntg('name','name');
        
        OpportunityIntegration__c opics1 = testclassUtils.getCustomSettingrec_opptyIntg_IntAdmin('name1','name1');
        
        Lead l = testclassUtils.getLeadrec();
        
        testclassUtils.getRecord(l);
        
        Opportunity opp = testclassUtils.getOppty();
        
        Account acc = testclassUtils.getAccount();
        
        Contact con = testclassUtils.getContact();
        //#1649
        Hub_Info__c hbinfo = testclassUtils.getHubInfo();
        
        Id i = testclassUtils.getRecordTypeId('Contact', 'EMC Internal Contact');
        
        String strProfile = [select name from Profile limit 1].name;
        String strRole = [select name from UserRole limit 1].name;
        User u;
        
        try
        {
            u = testclassUtils.queryUser(strProfile, strRole);
        }
        catch(Exception e)
        {
            
        }
        
        u = testclassUtils.getUser(strProfile, strRole);
    }
    
    /* @WR - 989 : To increase "testClassUtils" coverage */
    static testMethod void TestClassUtilsTest1() {
        Account acc = testclassUtils.getAccount();
        Pricebook2  standardPb = testclassUtils.getPricebook2();
        Product2 prd1 = testclassUtils.getProduct2('PS Consulting','USD', 'PSC');
        PricebookEntry pbe1 = testclassUtils.getPricebookEntry(prd1.id ,standardPb.id, 0, true);
        Opportunity opp1 = testclassUtils.getOpportunity('Opp1','Upside',Date.today(), pbe1.Pricebook2Id, acc.id);
        OpportunityLineItem lineItem1 = testclassUtils.getOpportunityLineItem(opp1.id, pbe1.id, 4, 200);
        
    }
}