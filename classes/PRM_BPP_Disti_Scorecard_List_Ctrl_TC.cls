@isTest
private class PRM_BPP_Disti_Scorecard_List_Ctrl_TC {

    static testMethod void myUnitTest() {
        
        List<Account> objAccount = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        List<Account> associatedAccount = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        List<APPR_MTV__RecordAssociation__c> testAccAssociationList = new List<APPR_MTV__RecordAssociation__c>();
        String strAccountRoleDistributor = System.Label.Account_Role_Distributor;
        String strAccountRoleT2VAR = System.Label.Account_Role_T2_VAR;
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser)
        {
        //PRM_VPP_JobDataHelper.createVPPCustomSettingData(); 
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();  
            CustomSettingDataHelper.federationTierData();  
        }
        
        //logic added to change the Party Id because it is unique.
        for(Integer i=0;i<objAccount.size();i++) {
        
            objAccount[i].Party_Number__c = '123'+i;
            objAccount[i].APPR_MTV__Association_Account__c= true;
            objAccount[i].Lead_Oppty_Enabled__c=true;   
        }
        if(!objAccount.isEmpty()) {
            insert objAccount;
        }
        
        //logic added to change the Party Id because it is unique for associated Accounts.
        for(Integer i=0;i<associatedAccount.size();i++) {
        
            associatedAccount[i].Party_Number__c = '1234'+i;
            associatedAccount[i].APPR_MTV__Association_Account__c= true;
            associatedAccount[i].Lead_Oppty_Enabled__c=true;            
        }
        if(!associatedAccount.isEmpty()) {
            insert associatedAccount ;
        }
         
        //create account association records
        for(Integer i =0;i<objAccount.size();i++) {
            
            APPR_MTV__RecordAssociation__c accAssobj1 = new APPR_MTV__RecordAssociation__c(APPR_MTV__Account__c=objAccount[i].Id,APPR_MTV__Associated_Account__c=associatedAccount[i].Id,APPR_MTV__Account_Role__c = strAccountRoleDistributor);
            testAccAssociationList.add(accAssobj1);
            APPR_MTV__RecordAssociation__c accAssobj2 = new APPR_MTV__RecordAssociation__c(APPR_MTV__Account__c=objAccount[i].Id,APPR_MTV__Associated_Account__c=associatedAccount[i].Id,APPR_MTV__Account_Role__c = strAccountRoleT2VAR);
            testAccAssociationList.add(accAssobj2);
        }
        if(!testAccAssociationList.isempty()) {
            insert testAccAssociationList;
        }
        ApexPages.StandardController controller01 = new ApexPages.StandardController(objAccount[0]); 
        ApexPages.currentPage().getParameters().put('id',objAccount[0].Id);
        PRM_BPP_Disti_Scorecard_List_Controller myVarList = new PRM_BPP_Disti_Scorecard_List_Controller() ;           
        myVarList.getVarAccounts();
        myVarList.solProviderScore();

    }
}