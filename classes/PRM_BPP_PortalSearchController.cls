/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
|  11/Sep/2014  Jaypal Nimesh       BPP Project     Created controller class for Portal Search.
|
+=====================================================================================================*/

public class PRM_BPP_PortalSearchController {
    
    //Initializing variables
    public String objectName{get;set;}    
    public String searchStr{get;set;}    
    List<String> searchObjList = new List<String>();
    
    //Adding Lead and Opportunity as select options
    public List<SelectOption> getSearchObjList()    
    {   
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Lead','Leads/Deal Registrations'));
        options.add(new SelectOption('Opportunity','Opportunities'));
        options.add(new SelectOption('Case','Cases'));   
  
        return options;
    }
    
    //Method called on Search Action
    public pageReference searchRecords(){
        
        PageReference pageRefer = null;
        
        if(objectName == 'Lead'){
            pageRefer = new PageReference('/search/SearchResults?searchType=1&sen=00Q&setLast=1&sbstr='+searchStr);
        }
        if(objectName == 'Opportunity'){
            pageRefer = new PageReference('/search/SearchResults?searchType=1&sen=006&setLast=1&sbstr='+searchStr);
        }
        if(objectName == 'Case'){
            pageRefer = new PageReference('/search/SearchResults?searchType=1&sen=500&setLast=1&sbstr='+searchStr);
        }
        
        pageRefer.setRedirect(true);
        return pageRefer;
    }        
}