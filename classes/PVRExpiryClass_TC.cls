/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req     DESCRIPTION                               
 |  ====            =========       ======     =========== 
 |  23.03.2015      Paridhi Aggarwal    DM PVR        Test class for the unit test of PVRExpiryClass
+=========================================================================================================================*/
@isTest
private class PVRExpiryClass_TC 
{
 static list<Case> lstCase = new list<Case>();
 static testMethod void TC_PVRExpiryClass1()
 {
     
    System.runAs(new user(Id = UserInfo.getUserId()))
            {
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
                CustomSettingDataHelper.dealRegistrationCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.partnerBoardingCSData();
                CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
                CustomSettingDataHelper.VCEStaticCSData();
                CustomSettingDataHelper.PSCCaseTypeImageData();
                CustomSettingDataHelper.PSCFieldMappingData();
                CustomSettingDataHelper.SC13LanguageData();
            }
    // create account record.
    List<Account> acc = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
    acc[0].Core_Quota_Rep__c = UserInfo.getUserId();
    insert acc; 
    
    Contact contact = UserProfileTestClassDataHelper.createContact();
    insert contact;
    
    // Create Opportunity Record.
    list<Opportunity>  listOpt = new list<Opportunity>();
    listOpt.add(new Opportunity(Name = 'TestOpp0',AccountId = acc[0].Id, CloseDate = system.today() + 5, StageName = 'Pipeline',Sales_Channel__c='Direct',Sales_Force__c='EMC'));
    insert listOpt;
    
    Test.Starttest();        
    
        
    Case objCase = new Case(); 
     objCase.RecordTypeId=[select Id from RecordType where Name='Renewals Policy Variation Request (PVR)'].id;
     
    objCase.Type = 'Data Migration';
    objCase.Concession_End_Date__c = System.today();
    //objCase.Opportunity_Name__c=listOpt[0].id;
    objCase.Field_Rep__c=contact.id;
    objCase.Customer_Account_Name__c = acc[0].id;
    objCase.Global_Account__c=True;
    objCase.Global_Ultimate_Identifier__c ='Text';
    objcase.Description = '8907';
    objCase.Theatre__c = 'EMEA';
    lstCase.add(objCase);
    insert lstCase;
    
    
    
    
    
    
    objCase.Concession_End_Date__c = System.today()+5;
    update lstCase;
    
     
    
    
     test.stoptest();

  }
}