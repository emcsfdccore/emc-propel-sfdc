/*==================================================================================================================+

 |  HISTORY  |                                                                            

 |  DATE          DEVELOPER      WR        DESCRIPTION                              

 |  ====          =========      ==        =========== 
 |  07/25/2014    Sneha Jain    PROPEL  FD-I.006 SAP CRM OB Core SFDC Opportunity - Initial Creation
 |  09/18/2014    Bhanuprakash  PROPEL  Added OrderDetailsCompleteABAFlag field to CaseData and added new renewal oppty fields.
 |  09/23/2014    Bhanuprakash  PROPEL  Added createRenewalOpportunity method.
 |  10/09/2014    Bhanuprakash  PROPEL  Added soldToParty filed in CaseData and changed closeDate data type to DateTime.
 +==================================================================================================================*/

global class OpportunityQuoteMain
{
    public static String exceptionDescription = null;
    // Opportunity Update data from SAP
    global class OpportunityUpdateData{
    
        webservice String opportunityNumber;
        webservice String accountPartyNumber;
        webservice String tier2PartnerPartyNum;
        webservice String quoteType;
        webservice String quoteCartNumber;
        webservice String salesOrg;
        webservice String quoteVersion;
        webservice String quoteStatus;
        webservice String soNumber;
        webservice DateTime  closeDate;
        webservice String orderCurrency;
        webservice String priceFloor;
        webservice Double HWRevenue;
        webservice Double SWRevenue;
        webservice Double servicesRevenue;
        webservice Double prepaidHWMARevenue;
        webservice Double prepaidSWMARevenue;
        webservice Double HWWarrantyUpgradeRevenue;
        webservice Double partsRetentionRevenue;
        webservice Double warrantyRevenue;
        webservice Double HWDiscount;
        webservice Double SWDiscount;
        webservice Double servicesDiscount;
        webservice Double prepaidHWMADiscount;
        webservice Double prepaidSWMADiscount;
        webservice Double HWWarrantyUpgradeDiscount;
        webservice Double partsRetentionDiscount;
        webservice Double warrantyDiscount;
        webservice String distributionChannel;
        webservice Double redirectValue;
        //OLI details
        webservice List<LineItemData> lineItemData;
        //Case details
        webservice List<CaseData> caseData;
        //Bhanu - adding fields to support Create renewal opportunity
        
    }
    
    // OpportunityLineItems data from SAP
    global class LineItemData{
    
        webservice String productName;
        webservice Double quoteAmount;
    }
    //OpportunityLineItems for Renewal Opportunity
    global class RenewalOpportunityLineItemData{
        webservice String productName;
        webservice Double forecastAmt;
    }
    //Renewal Opportunity data class
    global class RenewalOpportunityData  {
        //Mandatory fields
        webservice String opportunityName;
        webservice String accOwnerId;
        //webservice String salesChannel;
        webservice String currencyCode;
        webservice String leadSource;
        webservice String endCustomer;
        webservice String forecastStatus;
        webservice DateTime closeDate;
        //Optional fields
        //webservice String salesForce;
        webservice String districtReseller;
        webservice String distributionVAR;
        webservice String contractNum;
        //webservice String productName;
        //webservice Double forecastAmt;
        webservice String distributionChannel;
        webservice List<RenewalOpportunityLineItemData> lstRenewalLineItems;
    }
    
    // Case details from SAP
    global class CaseData{
        webservice String distiDirectResellerPartyNumber;
        webservice String distributionVARPartyNumber;
        webservice String billToParty;  
        webservice String shipToParty;  
        webservice String installAtParty;
        webservice String orderType;
        webservice String orderCurrency;
        webservice Double orderValueWoutFreight;
        webservice String purchaseOrderNumbers;
        webservice Datetime quoteSubmissionDateTime;
        webservice String contactEmail;
        webservice String oracleEmailAlias;
        webservice String quoteDistrict; 
        webservice Boolean OrderDetailsCompleteABAFlag=false;
        webservice String soldToParty;
    }
    
    
    //Status Response class
    global class StatusResponse{
        webservice Boolean isSuccess;
        webservice String errorMessage;
        webservice List<String> SFDCOpportunityIdList;
        webservice String SFDCQuoteId;
    }
    
    //Main webservice method
    webservice static StatusResponse updateOpportunityData(OpportunityUpdateData requestOppData){
            OpportunityQuoteHelperClass oppHelp = new OpportunityQuoteHelperClass();
        return oppHelp.wsUpdate(requestOppData);
    }
    
    webservice static StatusResponse createRenewalOpportunity(RenewalOpportunityData renewalOppData){
         OpportunityQuoteHelperClass oppHelp = new OpportunityQuoteHelperClass();
         StatusResponse response = new StatusResponse();
         try{
            response = oppHelp.createRenewalOpportunity(renewalOppData);
         }catch(Exception e){
         System.debug('**** Exception in Main class');
            if(exceptionDescription!=null){
                response.errorMessage = exceptionDescription;
                System.debug('*** known exception in Main class reason = ' + exceptionDescription);
            }else {
                response.errorMessage = 'Exception : ' + e.getMessage();
                System.debug('***Unknown exception ' + e.getMessage());
            }
            response.isSuccess = false;
        }
        return response;
    }
}