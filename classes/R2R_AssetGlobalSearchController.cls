/*==========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  23/09/2013      Anirudh Singh  Global       This controller will be used for R2R_AssetGlobalSearchController page.
 |                                 Search
+========================================================================================================================*/

public class R2R_AssetGlobalSearchController
{
 public string assName {get; set;} 
 public List<Asset__c> assResult {get; set; } 
 public R2R_AssetGlobalSearchController(ApexPages.StandardController controller) { 
    assName = ''; 
    assResult = new List<Asset__c>();
  }
   public pagereference SearchAssets(){
        //string s = '%'+assName+'%';
        if(assName != null && assName !=''){
            assResult =[Select Name,Customer_Name__c,Serial_Number__c,Install_City__c,Model__c, Install_Country__c,Install_Date__c,Core_Quota_Rep_from_Account__c,
                    Party_Number__c,Product_Name_Vendor__c,Product_Family__c,Install_Base_Status__c,RecordType.DeveloperName from Asset__c where Serial_Number__c =: assName LIMIT 50];
        }
        if(assName == null || assName ==''){
           ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,System.Label.R2R_SearchCriteria_Cant_Be_Blank);
           ApexPages.addMessage(msg); 
        }
        else if(assResult.isempty()){
           ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info,System.Label.R2R_No_Matching_Asset);
           ApexPages.addMessage(msg);
        }
    return null;
  }
  
}