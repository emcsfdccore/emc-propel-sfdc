@isTest (SeeAllData=true)
private class KB_CalculateCountOfSR_TC {


    static testMethod  void KBCalculateCountOfSR(){
        
        test.startTest();
        List<Id> idList= new List<Id>();
        List<Linked_SR__C> listlinkedSR= new List<Linked_SR__C>();
        List<Linked_SR__C> listlinkedSR1= new List<Linked_SR__C>();
        Set<Id> optbugids= new Set<Id>();
        Set<Id> linkids= new Set<Id>();
        //creating articles 
        
        Break_Fix__kav breakfix = new Break_Fix__kav();
        breakfix.Title = 'Test Class ';
        breakfix.UrlName = 'Test-Class';  
        breakfix.ValidationStatus = 'Work In Progress';
        breakfix.Language = 'en_US';
        insert breakfix;
        idList.add(breakfix.id);
        
        Break_Fix__kav breakfix1 = new Break_Fix__kav();
        breakfix1.Title = 'Test Class1 ';
        breakfix1.UrlName = 'Test-Class1';  
        breakfix1.ValidationStatus = 'Work In Progress';
        breakfix1.Language = 'en_US';
        insert breakfix1;
        idList.add(breakfix1.id);
        
        //publishing articles
        
        KnowledgeArticleVersion kav= [SELECT id,Title,ArticleType,ValidationStatus,Language,KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE Title = 'Test Class' and id in :idList ];       
        KbManagement.PublishingService.publishArticle(kav.KnowledgeArticleId, true);
        KnowledgeArticleVersion kav1= [SELECT id,Title,ArticleType,ValidationStatus,Language,KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE Title = 'Test Class1' and id in :idList ];       
        KbManagement.PublishingService.publishArticle(kav1.KnowledgeArticleId, true);                       
        //creating linked SRs
                     
        Linked_SR__c linkedSR= new Linked_SR__c();
        linkedSR.Article_Number__c=kav.ArticleNumber;
        linkedSR.Article_ID__c=kav.KnowledgeArticleId;
        linkedSR.Article_Version_ID__c=kav.id;
        linkedSR.SR_Number__c=12345;                  
        
        insert linkedSR;
        listlinkedSR.add(linkedSR);
        linkids.add(linkedSR.id);
        
        
        KB_CalculateCountOfSR cal=new KB_CalculateCountOfSR();
        cal.calculateCountofSROnInsert(listlinkedSR,false); 
        
        //Creating article summary
        
        Article_Summary__c articlesummary= new Article_Summary__c();
        articlesummary.Name= kav1.ArticleNumber;      
        insert articlesummary;               
        
        Linked_SR__c linkedSR1= new Linked_SR__c();
        linkedSR1.Article_Number__c=kav1.ArticleNumber;
        linkedSR1.Article_ID__c=kav1.KnowledgeArticleId;
        linkedSR1.Article_Version_ID__c=kav1.id;
        linkedSR1.SR_Number__c=123456;
        linkedSR1.Article_Summary__c=articlesummary.id;
        
        insert linkedSR1;
        listlinkedSR1.add(linkedSR1);
        linkids.add(linkedSR1.id);
        
        Linked_SR__c linkedSR2= new Linked_SR__c();
        linkedSR2.Article_Number__c=kav1.ArticleNumber;
        linkedSR2.Article_ID__c=kav1.KnowledgeArticleId;       
        linkedSR2.SR_Number__c=1234567;
        linkedSR2.Article_Summary__c=articlesummary.id;
        
        insert linkedSR2;
        listlinkedSR1.add(linkedSR2); 
        linkids.add(linkedSR2.id);
                               
        KB_CalculateCountOfSR cal1=new KB_CalculateCountOfSR();
        cal1.calculateCountofSROnInsert(listlinkedSR1,false);
        
        OPT_Bug__c opt1= new OPT_Bug__c();
        opt1.Article_Number__c=kav1.ArticleNumber;
        opt1.Bug_Tracking_Number__c='111000';
        insert opt1;
        
        optbugids.add(opt1.id);
        
        KB_CalculateCountOfSR.getKnowledgeId(linkids,true);
        KB_CalculateCountOfSR.getKnowledgeId(optbugids,false);
                
        test.stopTest();   
    }
}