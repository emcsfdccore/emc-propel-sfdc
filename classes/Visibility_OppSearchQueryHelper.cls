/*=======================================================================================================+
|  HISTORY  |                                                                           
|  DATE          DEVELOPER        WR                    DESCRIPTION                               
|  ====          =========        ==                    =========== 
|  28-Feb-2014   Jaspreet Singh                         This class acts as the Helper Class for OppSearchController
                                                        This class generates dynamic SOQL, queries for Opportunity
                                                        and returns list of Opportunity
|  11-Aug-2014   Vivek Barage     #1156                 Modified the code to add 2 new search criteria 
                            "close date from" and "close date to" 
|  16-sep-2014   Aagesh Jose      #1290                 Modified code to add new search criteria ELA and salesplay fields                           
|  19-Jan-2015   Vinod Jetti      #1595                 Updated code to replace 'LoggedInUser' to 'UserInfo.getUserId()'
|  11-Feb-2015   Vinod Jetti      #1723                 Modified code to add new search criteria EHC and DataLake fields
+=====================================================================================================*/
public with sharing class Visibility_OppSearchQueryHelper{
    
    //Variables to refer dynamic query
    private string searchQuery;
    private List<String> tempQueryPart = new List<String>();
    private Set<Id> productOppIds = new Set<Id>();
    private Set<Id> fMappingProdId = new Set<Id>();
    
    //Variables Used in creating query
    private Decimal amountFrom;
    private Decimal amountTo;
    private DateTime dateFrom;
    private DateTime dateTo;
    
    private Date closeDateFrom;
    private Date closeDateTo;
    
    private Id objAccountId;
    private string stageNameValue;
    private ID oppId;
    private String oppNumberValue;
    private String quoteNumberValue;
    private String globalDunsValue;
    private String districtValue;
    private String countryValue;
    private String areaValue;
    private String stateValue;
    private Id objRepId;
    private Id seID;
    
    //Variables for stageName, Originator based on which we filter Opportunity
    private String stageNameValues;
    private String originatorValues;
    private String leadSourceValues;
    private String opportunityTypeValues;
    
    //Added for #1290
    private boolean ELA;
    private String salesplayValue;
    
    //Added for CI #1723
    private boolean EHC;
    private boolean DataLake;
    
    public Visibility_OppSearchQueryHelper(){
        searchQuery = '';
    }
    
    /* @ Method           - <This method is called From OppSearchController class>
                            <When Opportunity Search is performed From Advanced Search Tab>
       @ Params           - <Instance of OppSearchController>
       @ Returns          - <List of Opportunities>
       @ throwsException  - <No Exception>
    */
    public void OpportunityQueryHelper(OppSearchController objOsc){
    
        //Clear the set & list
        fMappingProdId.clear();
        productOppIds.clear();
        tempQueryPart.clear();
        
        
        if(CustomSettingDataValueMap__c.getValues('Visibility_OppSearchQueryPartI').DataValue__c != null){
            searchQuery = CustomSettingDataValueMap__c.getValues('Visibility_OppSearchQueryPartI').DataValue__c;
        }
        
        if(CustomSettingDataValueMap__c.getValues('Visibility_OppSearchQueryPartII').DataValue__c != null ){
            searchQuery += CustomSettingDataValueMap__c.getValues('Visibility_OppSearchQueryPartII').DataValue__c ;
        }
        
        // Query clauses for Opportunity Filters Section amountFromValue - objOsc.forecastAmt!= null && objOsc.forecastAmt.length()>0
        if(objOsc.amountFromValue!= null && Visibility_OppSearchCriteria__c.getValues('Forecast Amount From').Opportunity_Search_Criteria__c != null){
            amountFrom = objOsc.amountFromValue;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Forecast Amount From').Opportunity_Search_Criteria__c  + ' amountFrom');
        }
        
        // objOsc.forecastAmount!= null && objOsc.forecastAmount.length() > 0 
        if(objOsc.amountToValue!= null && Visibility_OppSearchCriteria__c.getValues('Forecast Amount To').Opportunity_Search_Criteria__c != null){
            amountTo = objOsc.amountToValue;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Forecast Amount To').Opportunity_Search_Criteria__c  + ' amountTo');
        }
        
        if(objOsc.accId != null && objOsc.accId.length() > 0 && Visibility_OppSearchCriteria__c.getValues('Account Name').Opportunity_Search_Criteria__c != null){
            objAccountId = objOsc.accId;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Account Name').Opportunity_Search_Criteria__c  + ' objAccountId');
        }
        
        if(objOsc.forecastStatus != null && ( objOsc.forecastStatus.length() > 0 && objOsc.forecastStatus != '--None--' ) && Visibility_OppSearchCriteria__c.getValues('Forecast Status').Opportunity_Search_Criteria__c  + ' stageNameValue'!= null){
            stageNameValue = objOsc.forecastStatus;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Forecast Status').Opportunity_Search_Criteria__c  + ' stageNameValue');
        }
                
        if(objOsc.oppName != null && objOsc.oppName != '' && Visibility_OppSearchCriteria__c.getValues('Opportunity Name').Opportunity_Search_Criteria__c != null){
            oppId =  objOsc.oppName ;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Opportunity Name').Opportunity_Search_Criteria__c  + ' oppId');
        }
        
        /*Fetch the Product Ids related to the Forecast Group
          Add them into another set and use in the Query for Product
          Which will give Opportunity ID, which is used in the Main query for Opp*/
        System.debug('##objOsc.fMapping-->'+objOsc.fMapping);
        System.debug('##objOsc.flagDeals-->'+objOsc.flagDeals);
        if(objOsc.fMapping != null && (objOsc.fMapping.length() > 0 && objOsc.fMapping != '--None--')  && objOsc.flagDeals==true) {
            
            System.debug('##objOsc.fMapping-->'+objOsc.fMapping);
            //Query for related Forecast Group Products
            List<Forecast_Group_Product__c> obj;
            try{
                obj = [SELECT Product__c FROM Forecast_Group_Product__c where Forecast_Mapping__r.Forecast_Group__c = : objOsc.fMapping];
            }catch(Exception e){
                System.debug('##ForecastQueryException-->'+e.getMessage());
            }
            
            if(obj != null && obj.size() != 0){
                for(Forecast_Group_Product__c objFgp : obj){
                    fMappingProdId.add(objFgp.Product__c);                  
                }   
            }
            System.debug('##fMappingProdId-->'+fMappingProdId);
        }
        
        if(objOsc.oppNumber!= null && objOsc.oppNumber.length() > 0 && Visibility_OppSearchCriteria__c.getValues('Opportunity Number').Opportunity_Search_Criteria__c != null){
            oppNumberValue = objOsc.oppNumber + '%'; 
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Opportunity Number').Opportunity_Search_Criteria__c  + ' oppNumberValue');   
        }
        
        if(objOsc.quoteNumber!= null && objOsc.quoteNumber.length() > 0  && Visibility_OppSearchCriteria__c.getValues('Quote Number').Opportunity_Search_Criteria__c != null){
            quoteNumberValue = objOsc.quoteNumber + '%';
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Quote Number').Opportunity_Search_Criteria__c  + ' quoteNumberValue');   
        }
    
        if(objOsc.globalDuns!= null && objOsc.globalDuns.length() > 0 && Visibility_OppSearchCriteria__c.getValues('Global DUNS').Opportunity_Search_Criteria__c != null){
            globalDunsValue = objOsc.globalDuns + '%';
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Global DUNS').Opportunity_Search_Criteria__c  + ' globalDunsValue');   
        }
        
        //#1156 - Updated below code to use renamed custom setting fields ("Created Date From" and "Created Date To")
        if(objOsc.dateFrom != null && Visibility_OppSearchCriteria__c.getValues('Created date From').Opportunity_Search_Criteria__c != null){
            dateFrom = objOsc.dateFrom;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Created date From').Opportunity_Search_Criteria__c  + ' dateFrom');
        }
        
        if(objOsc.dateTo != null && Visibility_OppSearchCriteria__c.getValues('Created date To').Opportunity_Search_Criteria__c  != null){ 
            //dateTo = objOsc.dateTo;
            System.debug('#dateTo  before - GMT value:'+String.valueofGmt(objOsc.dateTo) );
            //Create a GMT todate with adding 23 Hr, 59mm 59 ss to return result for a single day search.           
            dateTo=DateTime.newInstanceGmt(Integer.valueof(String.valueofGmt(objOsc.dateTo).substring(0,4)),Integer.valueof(String.valueofGmt(objOsc.dateTo).substring(5,7)),Integer.valueof(String.valueofGmt(objOsc.dateTo).substring(8,10)),23,59,59);
            System.debug('#dateTo  Final value:'+dateTo);
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Created date To').Opportunity_Search_Criteria__c  + ' dateTo');
        }
        
        //#1156 - Updated below code to add new filter criteria in query where clause "Close Date From" and "Close Date To"
        if(objOsc.closeDateFrom != null && Visibility_OppSearchCriteria__c.getValues('Close date From').Opportunity_Search_Criteria__c != null){
            closeDateFrom = objOsc.closeDateFrom;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Close date From').Opportunity_Search_Criteria__c  + ' closeDateFrom');
        }
        
        if(objOsc.closeDateTo != null && Visibility_OppSearchCriteria__c.getValues('Close date To').Opportunity_Search_Criteria__c  != null){ 
            //System.debug('#closeDateTo  before - GMT value:'+String.valueofGmt(objOsc.closeDateTo) );
            //closeDateTo=DateTime.newInstanceGmt(Integer.valueof(String.valueofGmt(objOsc.closeDateTo).substring(0,4)),Integer.valueof(String.valueofGmt(objOsc.closeDateTo).substring(5,7)),Integer.valueof(String.valueofGmt(objOsc.closeDateTo).substring(8,10)),23,59,59);
            //System.debug('#closeDateTo  Final value:'+closeDateTo);
            closeDateTo = objOsc.closeDateTo;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Close date To').Opportunity_Search_Criteria__c  + ' closeDateTo');
        }
        
        // Query clauses for Geography Section
        if(objOsc.dist!= null && objOsc.dist.length() > 0 && Visibility_OppSearchCriteria__c.getValues('District').Opportunity_Search_Criteria__c  != null){
            districtValue = objOsc.dist + '%';
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('District').Opportunity_Search_Criteria__c  + ' districtValue');
        }
        
        if(objOsc.countr!= null && objOsc.countr.length() > 0 && Visibility_OppSearchCriteria__c.getValues('Country').Opportunity_Search_Criteria__c != null){
            countryValue = objOsc.countr + '%';
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Country').Opportunity_Search_Criteria__c  + ' countryValue');
        }
        
        if(objOsc.ara!= null && objOsc.ara.length() > 0 && Visibility_OppSearchCriteria__c.getValues('Area').Opportunity_Search_Criteria__c != null){
            areaValue = objOsc.ara + '%';
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Area').Opportunity_Search_Criteria__c  + ' areaValue');   
        }
        
        if(objOsc.stat!= null && objOsc.stat.length() > 0 && Visibility_OppSearchCriteria__c.getValues('State').Opportunity_Search_Criteria__c != null){
            stateValue = objOsc.stat + '%';
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('State').Opportunity_Search_Criteria__c  + ' stateValue');
        }
        
        //Query Clauses for Resources Involved Section
        if(objOsc.coreRep != null && objOsc.coreRep.length() > 0 && Visibility_OppSearchCriteria__c.getValues('Core Quota Rep').Opportunity_Search_Criteria__c != null){
            objRepId = objOsc.coreRep;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Core Quota Rep').Opportunity_Search_Criteria__c  + ' objRepId');
        }
        
        If(objOsc.primeSE != null && objOsc.primeSE.length() > 0 && Visibility_OppSearchCriteria__c.getValues('Primary SE').Opportunity_Search_Criteria__c != null){
            seID = objOsc.primeSE;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Primary SE').Opportunity_Search_Criteria__c  + ' seID');
        }
        
        
        /*Query Clauses Product Section / Opportunity Line Items 
          Fetch the Opportunity Ids related to Product and Product Family
          Add the OpportunityIds to set, which will be used in the main Search Query */
        String prodOppQuery =  ' ' ;
        if((objOsc.prodFamily != null && objOsc.prodFamily.length() > 0 ) || (objOsc.prodName != null && objOsc.prodName.length() > 0)){
            string productNameValue =  '\'%' + objOsc.prodName +  '%\'';
            string productFamilyValue = '\'%' + objOsc.prodFamily + '%\'';
            prodOppQuery = 'SELECT  OpportunityId FROM OpportunityLineItem WHERE ';
            if((objOsc.prodFamily != null && objOsc.prodFamily.length() > 0 ) && (objOsc.prodName != null && objOsc.prodName.length() > 0)) {
                prodOppQuery += ' PricebookEntry.Product2.Name Like '+ productNameValue ;
                prodOppQuery += ' AND Product_Family__c LIKE '+ productFamilyValue ;
            }else{
                if(objOsc.prodFamily != null && objOsc.prodFamily.length() > 0){
                    prodOppQuery += ' Product_Family__c LIKE '+ productFamilyValue;
                }else{
                    prodOppQuery += ' PricebookEntry.Product2.Name Like '+ productNameValue;
                }
            }
        }
                
        //Adding Product Ids fetched from the Forecast Group Mapping
        if(fMappingProdId.size() != 0){
            if(prodOppQuery.contains('WHERE')){
                prodOppQuery += ' AND PricebookEntry.Product2.Id IN : fMappingProdId';
            }else{
                prodOppQuery = 'SELECT  OpportunityId FROM OpportunityLineItem WHERE ';
                prodOppQuery += ' PricebookEntry.Product2.ID IN : fMappingProdId';
            }
        } else if(objOsc.flagDeals==true) //if checkbox is checked and no forecast group mapping
            { if(prodOppQuery.contains('WHERE')){
                    prodOppQuery += ' AND id=null';
                }else{
                    prodOppQuery = 'SELECT  OpportunityId FROM OpportunityLineItem WHERE id=null';                    
                }            
            }
        //Commented as we are passing the product query to main query
        /*if(prodOppQuery.contains('WHERE')){
            prodOppQuery += ' LIMIT  8000';
            System.debug('##prodOppQuery-->'+prodOppQuery);
             List<OpportunityLineItem> lstOli = Database.Query(prodOppQuery);
             if(lstOli.size() > 0){
                for(OpportunityLineItem objOli : lstOli){
                    productOppIds.add(objOli.OpportunityId);
                }
            }
        } */    

   //Added for #1290 - Deal specific section
        if(objOsc.ELA){
           ELA = objOsc.ELA;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('ELA').Opportunity_Search_Criteria__c  + ' ELA');
        
        }
        
         if(objOsc.salesplay != null && objOsc.salesplay.length() > 0){
            salesplayValue = '(';
            for(String s : objOsc.salesplay.split(';')) {
                salesplayValue += '\''+s+'\',';
            }
            salesplayValue = salesplayValue.subString(0,salesplayValue.length()-1);
            salesplayValue+=')';
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('Sales Play').Opportunity_Search_Criteria__c +salesplayValue);
            
        }  
    //Added for #1290 - Deal specific section  
    
     //Added for CI #1723 -Start
        if(objOsc.EHC){
           EHC = objOsc.EHC;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('EHC').Opportunity_Search_Criteria__c  + ' EHC');
        
        }
        if(objOsc.DataLake){
           DataLake = objOsc.DataLake;
            tempQueryPart.add(' ' + Visibility_OppSearchCriteria__c.getValues('DataLake').Opportunity_Search_Criteria__c  + ' DataLake');
        
        }
        //CI #1723 -End     
        
        if(tempQueryPart.size() > 0){
            searchQuery +=  ' WHERE ' +tempQueryPart[0];
            for(Integer i =1;  i<tempQueryPart.size(); i++ ){
                System.debug('##tempQueryPart[i]-->'+tempQueryPart[i]);
                searchQuery += ' AND '+ tempQueryPart[i];
            }
        }
        
        // Add Opp Ids fetched from Products 
        System.debug('##productOppIds.size-->'+productOppIds.size());
        //if(productOppIds.size() > 0){ 
        if (prodOppQuery.trim().length()>0) {
            System.debug('searchQuery.contains'+searchQuery.contains('WHERE'));
            if(searchQuery.contains('WHERE')){
                //searchQuery += ' AND Id IN : productOppIds';
                searchQuery += ' AND Id IN ( '+ prodOppQuery +')';
                System.debug('##searchQuery-->'+searchQuery);
            }else{
                //searchQuery += ' WHERE Id IN : productOppIds';
                searchQuery += ' WHERE Id IN ('+ prodOppQuery+')';
            }
        } 
        
        
        
        //Closed Date Filter Criteria
        if(searchQuery.contains('WHERE')){
            searchQuery += ' AND CloseDate > YESTERDAY ';
            System.debug('##searchQuery-->'+searchQuery);
        }else{
            searchQuery += ' WHERE CloseDate > YESTERDAY ';
        }
        
        //Filtering Opportunities for Closed, Booked and Won
        if(CustomSettingDataValueMap__c.getValues('Visibility_OppQueryStageNameValues').DataValue__c != null){
            stageNameValues = CustomSettingDataValueMap__c.getValues('Visibility_OppQueryStageNameValues').DataValue__c;
            if(stageNameValues != null && stageNameValues.length() > 0 && Visibility_OppSearchCriteria__c.getValues('StageNameFilter').Opportunity_Search_Criteria__c != null){
                List<String> stageValues = stageNameValues.split(',');
                for(String tempString : stageValues){
                    searchQuery += ' '+Visibility_OppSearchCriteria__c.getValues('StageNameFilter').Opportunity_Search_Criteria__c+' '+'\''+tempString+'\'';
                }
            }
        }
        
        //Filtering Opportunities Where Originator is Renewals
        /*Removing Filter As per Prabhat's Mail
        originatorValues = CustomSettingDataValueMap__c.getValues('Visibility_OppQueryOriginatorValues').DataValue__c;
        if(originatorValues != null && originatorValues.length() > 0 ){
            searchQuery += ' '+Visibility_OppSearchCriteria__c.getValues('OriginatorFilter').Opportunity_Search_Criteria__c+' '+'\''+originatorValues+'\'';
        }*/
        
        //Filtering Opportunities where Lead Source is Contract Renewal
        if(CustomSettingDataValueMap__c.getValues('Visibility_OppQueryLeadSourceValues').DataValue__c != null){
            leadSourceValues = CustomSettingDataValueMap__c.getValues('Visibility_OppQueryLeadSourceValues').DataValue__c;
            if(leadSourceValues != null && leadSourceValues.length() > 0 && Visibility_OppSearchCriteria__c.getValues('LeadSourceFilter').Opportunity_Search_Criteria__c != null){
                searchQuery += ' '+Visibility_OppSearchCriteria__c.getValues('LeadSourceFilter').Opportunity_Search_Criteria__c+' '+'\''+leadSourceValues+'\'';
            }
        }
        
        //Filtering Opportunities where Type is Renewals
        if(CustomSettingDataValueMap__c.getValues('Visibility_OppQueryOpportunityTypeVal').DataValue__c != null){
            opportunityTypeValues = CustomSettingDataValueMap__c.getValues('Visibility_OppQueryOpportunityTypeVal').DataValue__c;
            if(opportunityTypeValues != null && opportunityTypeValues.length() > 0 && Visibility_OppSearchCriteria__c.getValues('OpportunityTypeFilter').Opportunity_Search_Criteria__c != null){
                searchQuery += ' '+Visibility_OppSearchCriteria__c.getValues('OpportunityTypeFilter').Opportunity_Search_Criteria__c+' '+'\''+opportunityTypeValues+'\'';
            }
        }
        
    }
    
    /* @ Method           - <This method is called From OppSearchController class for sorting results>
       @ Params           - <Field Name based on which sorting is done>
                            <Field Order ASC Or DESC>
       @ Returns          - <List of Opportunities in Sorted Order>
       @ throwsException  - <No Exception>
    */
    public List<Opportunity> sortedResults(String fieldValue, Boolean fieldOrder){
        if(searchQuery.contains('ORDER')){
            Integer indexOrder = searchQuery.indexOf('ORDER');
            searchQuery = searchQuery.subString(0, indexOrder);
        }
        searchQuery += ' ORDER BY ' + fieldValue;
        
        if(fieldOrder == true){
            searchQuery += ' ASC ';
        }else{
            searchQuery += ' DESC ';
        }
        
        if(CustomSettingDataValueMap__c.getValues('Visibility_OppQueryLimit').DataValue__c != null){
            searchQuery += CustomSettingDataValueMap__c.getValues('Visibility_OppQueryLimit').DataValue__c;
        }
        //#1595
        searchQuery = searchQuery.replace('LoggedInUser',UserInfo.getUserId());
        system.debug('searchQuery**************************************************'+searchQuery);
        return Database.Query(searchQuery);
    }
    
    /* @ Method           - <This method is called From OppSearchController class>
                            <When Opportunity Search is performed From Assign Button>
       @ Params           - <Instance of OppSearchController>
       @ Returns          - <List of User Assignment>
       @ throwsException  - <No Exception>
    */
     //public List<User_Assignment__c> UserAssignmentQueryHelper(OppSearchController objOsc){
    
        /*System.debug('###objOsc--->'+objOsc);
        
        //Clear the set & list
        fMappingProdId.clear();
        productOppIds.clear();
        tempQueryPart.clear();
        searchQuery = '';
                    
        searchQuery +=  ' SELECT Opportunity__c, Opportunity__r.Amount, Opportunity__r.Dollar_Amount__c, Account__r.Name, Opportunity_Name__c ';  
        searchQuery +=  ' ,Opportunity__r.Quote_Cart__c, Opportunity__r.Quote_Cart_Number__c, Opportunity__r.StageName, Account__r.Primary_SE__r.Name ';
        searchQuery +=  ' ,Account__r.Primary_SE__c, Account__r.BillingCity, Account__r.BillingCountry, Account__r.Account_Area__c '; 
        searchQuery +=  ' ,Account__r.Core_Quota_Rep__r.Name ,Account__r.Core_Quota_Rep__c, Account__r.Global_DUNS_Entity__c ';
        searchQuery +=  ' ,Opportunity_Number__c, Opportunity__r.CloseDate, Opportunity__r.CreatedDate, Account__c FROM User_Assignment__c';
        
         // Query clauses for Opportunity Filters Section
        if(objOsc.forecastAmt.length()>0){
            Decimal amountFrom = double.valueOf(objOsc.forecastAmt);
            tempQueryPart.add(' Opportunity__r.Dollar_Amount__c >= :amountFrom');
        }
        
        if(objOsc.forecastAmount.length() > 0){
            Decimal amountTo = double.valueOf(objOsc.forecastAmount);
            tempQueryPart.add(' Opportunity__r.Dollar_Amount__c <= :amountTo ');
        }
        
        System.debug('objOsc.opp.Accountid'+objOsc.accId);
        if(objOsc.accId != null && objOsc.accId.length() > 0){
            Id objAccountId = objOsc.accId;
            tempQueryPart.add(' Account__c = :objAccountId ');
        }
        
        
        if(objOsc.forecastStatus != null && ( objOsc.forecastStatus.length() > 0 && objOsc.forecastStatus != '--None--' )){
            string stageNameValue = objOsc.forecastStatus;
            tempQueryPart.add(' Opportunity__r.StageName = : stageNameValue '); 
        }
        
        
        if(objOsc.oppName != null && objOsc.oppName != ''){
            ID oppId =  objOsc.oppName ;
            tempQueryPart.add(' Opportunity__c = : oppId');
            //tempQueryPart.add(' Name Like : opportunityName');
        }
        
        /*if(objOsc.oppName != null && objOsc.oppName != ''){
            string opportunityName = '%' + objOsc.oppName + '%';
            tempQueryPart.add(' Opportunity_Name__c Like : opportunityName');
        }*/
        
        //Fetch the Product Ids related to the Forecast Group
        //Add them into another set and use in the Query for Product
        //Which will give Opportunity ID, which is used in the Main query for Opp
        /*System.debug('##objOsc.fMapping-->'+objOsc.fMapping);
        System.debug('##objOsc.flagDeals-->'+objOsc.flagDeals);
        if(objOsc.fMapping != null && (objOsc.fMapping.length() > 0 && objOsc.fMapping != '--None--')  && objOsc.flagDeals==true) {
            
            System.debug('##objOsc.fMapping-->'+objOsc.fMapping);
            //Query for related Forecast Group Products
            List<Forecast_Group_Product__c> obj;
            try{
                obj = [SELECT Product__c FROM Forecast_Group_Product__c where Forecast_Mapping__r.Forecast_Group__c = : objOsc.fMapping];
            }catch(Exception e){
                System.debug('##ForecastQueryException-->'+e.getMessage());
            }
            
            if(obj.size() != 0){
                for(Forecast_Group_Product__c objFgp : obj){
                    fMappingProdId.add(objFgp.Product__c);                  
                }   
            }
            System.debug('##fMappingProdId-->'+fMappingProdId);
        }
        
        if(objOsc.oppNumber.length() > 0){
            String oppNumberValue = objOsc.oppNumber + '%'; 
            tempQueryPart.add(' Opportunity_Number__c LIKE : oppNumberValue' );    
        }
        
        if(objOsc.quoteNumber.length() > 0 ){
            String quoteNumberValue = objOsc.quoteNumber + '%';
            tempQueryPart.add(' Opportunity__r.Quote_Cart_Number__c = : quoteNumberValue');
        }
    
        if(objOsc.globalDuns.length() > 0){
            String globalDunsValue = objOsc.globalDuns + '%';
            tempQueryPart.add(' Account__r.Global_DUNS_Entity__c = : globalDunsValue');
        }
        
        System.debug('dateFrom'+objOsc.dateFrom);
        System.debug('dateTo'+objOsc.dateTo);
        if(objOsc.dateFrom != null){
            DateTime d = objOsc.dateFrom;
            tempQueryPart.add(' Opportunity__r.CreatedDate >= :d');
        }
        
        if(objOsc.dateTo != null){ 
            DateTime d1 = objOsc.dateTo;
            tempQueryPart.add(' Opportunity__r.CreatedDate <= :d1');
        }
        
            
        // Query clauses for Geography Section
        if(objOsc.dist.length() > 0){
            String districtValue = objOsc.dist + '%';
            tempQueryPart.add(' Account__r.BillingCity = : districtValue');
        }
        
        if(objOsc.countr.length() > 0){
            String countryValue = objOsc.countr + '%';
            tempQueryPart.add(' Account__r.BillingCountry = : countryValue ' );
        }
        
        if(objOsc.ara.length() > 0){
            String areaValue = objOsc.ara + '%';
            tempQueryPart.add(' Account__r.Account_Area__c = : areaValue ');
        }
        
        if(objOsc.stat.length() > 0){
            String stateValue = objOsc.stat + '%';
            tempQueryPart.add(' Account__r.BillingState = : stateValue');
        }
        
        //Query Clauses for Resources Involved Section
        System.debug('objOsc.coreRep'+objOsc.coreRep);
        if(objOsc.coreRep != null && objOsc.coreRep.length() > 0){
            Id objRepId = objOsc.coreRep;
            tempQueryPart.add(' Account__r.Core_Quota_rep__r.Id = :objAccountId ');
        }
        
        System.debug('objOsc.primeSE '+objOsc.primeSE );
        If(objOsc.primeSE != null && objOsc.primeSE.length() > 0){
            Id seID = objOsc.primeSE;
            tempQueryPart.add(' Account__r.Primary_SE__r.Id = :seID ');
        }
        
         // Query Clauses Product Section / Opportunity Line Items 
        //Fetch the Opportunity Ids related to Product and Product Family
        //Add the OpportunityIds to set, which will be used in the main Search Query */
        /*String prodOppQuery =  ' ' ;
        if(objOsc.prodFamily.length() > 0 || objOsc.prodName.length() > 0){
            string productNameValue =  '%' + objOsc.prodName +  '%';
            string productFamilyValue = '%' + objOsc.prodFamily + '%';
            prodOppQuery = 'SELECT Id, OpportunityId FROM OpportunityLineItem WHERE ';
            if(objOsc.prodFamily.length() > 0 && objOsc.prodName.length() > 0) {
                prodOppQuery += ' PricebookEntry.Product2.Name Like : productNameValue';
                prodOppQuery += ' AND Product_Family__c LIKE : productFamilyValue' ;
            }else{
                if(objOsc.prodFamily.length() > 0){
                    prodOppQuery += ' AND Product_Family__c LIKE : productFamilyValue';
                }else{
                    prodOppQuery += ' PricebookEntry.Product2.Name Like : productNameValue';
                }
            }
        }
        
        //Adding Product Ids fetched from the Forecast Group Mapping
        if(fMappingProdId.size() != 0){
            if(prodOppQuery.contains('WHERE')){
                prodOppQuery += ' AND PricebookEntry.Product2.Id IN : fMappingProdId';
            }else{
                prodOppQuery = 'SELECT Id, OpportunityId FROM OpportunityLineItem WHERE ';
                prodOppQuery += ' PricebookEntry.Product2.ID IN : fMappingProdId';
            }
        }
        
        if(prodOppQuery.contains('WHERE')){
            prodOppQuery += ' LIMIT  8000';
            System.debug('##prodOppQuery-->'+prodOppQuery);
             List<OpportunityLineItem> lstOli = Database.Query(prodOppQuery);
             if(lstOli.size() > 0){
                for(OpportunityLineItem objOli : lstOli){
                    productOppIds.add(objOli.OpportunityId);
                }
            }
            System.debug('##productOppIds-->'+productOppIds);
        }                     
        
        System.debug('##tempQueryPart-->'+tempQueryPart);
        
        if(tempQueryPart.size() > 0){
            searchQuery +=  ' WHERE ' + tempQueryPart[0];
            
            for(Integer i = 1;  i<tempQueryPart.size(); i++ ){
                System.debug('##tempQueryPart[i]-->'+tempQueryPart[i]);
                searchQuery += ' AND '+ tempQueryPart[i];
            }
            System.debug('##searchQuery-->'+searchQuery);
        }
        
        // Add Opp Ids fetched from Products 
        System.debug('##productOppIds.size-->'+productOppIds.size());
        if(productOppIds.size() > 0){
            System.debug('searchQuery.contains'+searchQuery.contains('WHERE'));
            if(searchQuery.contains('WHERE')){
                searchQuery += ' AND Opportunity__r IN : productOppIds';
                System.debug('##searchQuery-->'+searchQuery);
            }else{
                searchQuery += ' WHERE Id IN : productOppIds';
            }
            
        }
        
        //Closed Data Filter Criteria
        if(searchQuery.contains('WHERE')){
            searchQuery += ' AND Opportunity__r.CloseDate > YESTERDAY ';
            System.debug('##searchQuery-->'+searchQuery);
        }else{
            searchQuery += ' WHERE Opportunity__r.CloseDate > YESTERDAY ';
        }
        
        //Filter Opportunities with Status set to Closed, Booked, Omitted
        searchQuery += ' AND Opportunity__r.StageName != '+'\''+'Closed'+'\'';
        searchQuery += ' AND Opportunity__r.StageName != '+'\''+'Booked'+'\'';
        searchQuery += ' AND Opportunity__r.Originator__c != '+'\''+'Renewals'+'\'';
               
        searchQuery += ' LIMIT 1001';
        
        System.debug('##searchQuery-->'+searchQuery); */
        //return Database.Query(searchQuery);   
       // return null;   
    //}
    
}