/*===========================================================================+
|  HISTORY                                                                  
|                                                                           
|  DATE       DEVELOPER       WR       DESCRIPTION                               
|  ====       =========       ==       =========== 
|  12 Feb 2014   Aarti Jindal         Initial Creation-Helper Class to check if any other job 
                                      is running on 'Account' object.
+===========================================================================*/




public class BatchJobAndSchedularHelper{

/* @Method <This method 'IsAnyApexJobExecuting' is used to extract jobs related to Account 
             and to check if any of those is in process while the delete job is running>
   @param <obj is used to get the object name whose related jobs we need from custom setting >
   @return <boolean> - <Returning true if any other job is in process otherwise false>
*/
    
    public static boolean isAnyApexJobExecuting(String obj){
       
                List<sObject> ob=new List<sObject>();
                List<String> strList =new List<string>();
                BatchJobAndSchedularManager__c jobs = BatchJobAndSchedularManager__c.getValues(obj);
                if(jobs!=null) 
                {
                String strJobs= jobs.JobNames__c;
                System.debug('-------->string'+strJobs);
                if(strJobs!=null && strJobs!='')
                {
                    String[] strJobsList= strJobs.split(',',-2);
                    System.debug('-------->list'+strJobsList);
                    for(String str:strJobsList)
                    {
                        str='\''+str+'\'';
                        strList.add(str);
                    }
                    
                    String q ='SELECT id FROM AsyncApexJob WHERE (jobType=\'BatchApex\' OR jobType=\'BatchApexWorker\') AND (Status != \'Completed\') AND ApexClassId IN' +strList ; 
                    System.debug('--------->query'+q);
                    ob=Database.query(q);
                    System.debug('--------->ob.size()'+ob.size());
                 }  
               }
                
                if(ob!=null && ob.size()>0  )
                {
                     return true;
                }
                else
                {
                    return false;
                }
             
             
        } 
        
 /* @Method <This method 'IsRescheduleJob' is used to decide whether the delete job needs to be rescheduled>
    @param <'obj' is the name of the object whose jobs we need to extract from custom setting,
            'className' is the name of the batch class name that is currently running>
    @return <boolean> - <Returning true if any other job is in process and 
                         reschedule custom setting value is true otherwise returning false>
*/
       
     
     public static boolean isRescheduleJob(String obj, String className){
         
         String resch;
         CustomSettingDataValueMap__c reschDelJob=CustomSettingDataValueMap__c.getvalues('Reschd'+className);
         if(reschDelJob!=null)
            {
                 resch=reschDelJob.DataValue__c;
            }
             
        if(IsAnyApexJobExecuting(obj) && resch.equalsIgnoreCase('TRUE'))
        {
            return true;
        }
        else
        {
        
            return false;
        }
     
     }
 }