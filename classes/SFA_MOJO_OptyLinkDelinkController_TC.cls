/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  28.10.2012      Shipra          MOJO        Initial Creation.Creating this test class to unit test 
                                                SFA_MOJO_OptyLinkDelinkController class.
    31.10.2012      Avinash K       MOJO        Updated the linkUnlinkUnitTest test method to increase coverage for
                                                SFA_MOJO_OptyLinkDelinkController
    19.03.2014      Sneha Jain      R2R         Modified the class to make it according to coding best practice standards. Removed SeeAllData = true
    12/18/2014      Bindu Sri       CI 1559     Replaced closed reason values
 |  19/02/2015      Vinod Jetti     #1649       created Hub Account Data object record instad of Account 'Site_DUNS_Entity__c,Global_DUNS_Entity__c' fields
+========================================================================================================================*/

@isTest
private class SFA_MOJO_OptyLinkDelinkController_TC 
{

    static testMethod void linkUnlinkUnitTest() 
    {
        try
        {
            //Creating Custom Setting data
            System.runAs(new user(Id = UserInfo.getUserId()))
            {
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.bypassLogicCSData();
            } 
            
            //Insert user record where Profile is Sys Admin and Forecast Group is Maintenance Renewals
            ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
            User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='test-user@emailTest.com', alias='tuser1', CommunityNickName='tuser1', 
            TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
            LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' ,Forecast_Group__c = 'Maintenance Renewals'); 
        
            insert insertUser;
            System.runAs(insertUser)
            {    //#1649 Insert Hub Account Data
                 Hub_Info__c objHubInfo = new Hub_Info__c();
                 objHubInfo.Site_DUNS_Entity__c = '123456';
                 objHubInfo.Global_DUNS_Entity__c = '663399';
                 objHubInfo.Golden_Site_Identifier__c = 3050645;
                 insert objHubInfo;
                //#1649 - End
                //Insert Account
                List<Account> accList = new List<Account>();
                accList.add(new Account(Name='TestAccount0',CurrencyIsoCode='USD',Account_Flag__c='Primary', Hub_Info__c = objHubInfo.Id /*Site_DUNS_Entity__c = '123456',Global_DUNS_Entity__c = '663399' */ ));  
                insert accList;
                
                //Creating Opportunity
                List<Opportunity> oppList = new List<Opportunity>();
                oppList.add(new Opportunity(Name = 'TestOpp0',AccountId=accList[0].id,Sales_Force__c='EMC',CurrencyIsoCode='USD',StageName ='Pipeline',CloseDate = System.today()+5,VCE_Related__c='VMWare Other',Amount = 500));
                // CI 1559 Replaced competition to Lost to competition
                oppList.add(new Opportunity(Name = 'TestOpp1',AccountId=accList[0].id,Sales_Force__c='EMC',CurrencyIsoCode='USD',StageName ='Closed',Closed_Reason__c = 'Lost to Competition',Close_Comments__c = 'Test', CloseDate = System.today(),VCE_Related__c='VMWare Other',Amount = 500));
                insert oppList;

                Set<Id> setOppId = new Set<Id>();
                setOppId.add(oppList[0].id);
                
                //Creating Assets
                List<Asset__c> lstAsset = new List<Asset__c>();
                
                Asset__c oppAssetRecord1 = new Asset__c();
                oppAssetRecord1.Name = 'Asset Test 1';
                oppAssetRecord1.Configuration_Details__c='EMC';
                oppAssetRecord1.Swap_Trade_In_Cost_Relief_Value__c=789.00;
                oppAssetRecord1.Product_Family__c='Services Only';
                oppAssetRecord1.Total_Raw_Capacity_GB__c = '11';
                oppAssetRecord1.Customer_Name__c=accList[0].id;        
                lstAsset.add(oppAssetRecord1);
                
                Asset__c oppAssetRecord2 = new Asset__c();
                oppAssetRecord2.Name = 'Asset Test 2';
                oppAssetRecord2.Configuration_Details__c='EMC';
                oppAssetRecord2.Swap_Trade_In_Cost_Relief_Value__c=789.00;
                oppAssetRecord2.Product_Family__c='Services Only';
                oppAssetRecord2.Total_Raw_Capacity_GB__c = '22.0';
                oppAssetRecord2.Customer_Name__c=accList[0].id;        
                lstAsset.add(oppAssetRecord2);
                
                Asset__c oppAssetRecord3 = new Asset__c();
                oppAssetRecord3.Name = 'Asset Test 3';
                oppAssetRecord3.Configuration_Details__c='EMC';
                oppAssetRecord3.Swap_Trade_In_Cost_Relief_Value__c=789.00;
                oppAssetRecord3.Product_Family__c='Services Only';
                oppAssetRecord3.Total_Raw_Capacity_GB__c = '33.0';
                oppAssetRecord3.Customer_Name__c=accList[0].id;        
                lstAsset.add(oppAssetRecord3);
                
                Asset__c oppAssetRecord4 = new Asset__c();
                oppAssetRecord4.Name = 'Asset Test 4';
                oppAssetRecord4.Configuration_Details__c='EMC';
                oppAssetRecord4.Swap_Trade_In_Cost_Relief_Value__c=789.00;
                oppAssetRecord4.Product_Family__c='Services Only';
                oppAssetRecord4.Total_Raw_Capacity_GB__c = '44.0';
                oppAssetRecord4.Customer_Name__c=accList[0].id;        
                lstAsset.add(oppAssetRecord4);
                
                Asset__c oppAssetRecord5 = new Asset__c();
                oppAssetRecord5.Name = 'Asset Test 5';
                oppAssetRecord5.Configuration_Details__c='EMC';
                oppAssetRecord5.Swap_Trade_In_Cost_Relief_Value__c=789.00;
                oppAssetRecord5.Product_Family__c='Services Only';
                oppAssetRecord5.Total_Raw_Capacity_GB__c = '55.0';
                oppAssetRecord5.Customer_Name__c=accList[0].id;        
                lstAsset.add(oppAssetRecord5);
                
                Asset__c oppAssetRecord6 = new Asset__c();
                oppAssetRecord6.Name = 'Asset Test 6';
                oppAssetRecord6.Configuration_Details__c='EMC';
                oppAssetRecord6.Swap_Trade_In_Cost_Relief_Value__c=789.00;
                oppAssetRecord6.Product_Family__c='Services Only';
                oppAssetRecord6.Total_Raw_Capacity_GB__c = '66.0';
                oppAssetRecord6.Customer_Name__c=accList[0].id;        
                lstAsset.add(oppAssetRecord6);
                
                insert lstAsset;
                
                //Inserting Opportunity Asset Junction records
                List<Opportunity_Asset_Junction__c> lstOAJ = new List<Opportunity_Asset_Junction__c>();
                Opportunity_Asset_Junction__c oaj1 = new Opportunity_Asset_Junction__c(Related_Asset__c = lstAsset[0].id, Related_Opportunity__c = oppList[0].id);

                Opportunity_Asset_Junction__c oaj2 = new Opportunity_Asset_Junction__c(Related_Asset__c = lstAsset[1].id, Related_Opportunity__c = oppList[0].id, Related_Account__c = accList[0].id);

                Opportunity_Asset_Junction__c oaj3 = new Opportunity_Asset_Junction__c(Related_Asset__c = lstAsset[2].id, Related_Opportunity__c = oppList[0].id, Related_Account__c = accList[0].id);

                Opportunity_Asset_Junction__c oaj4 = new Opportunity_Asset_Junction__c(Related_Asset__c = lstAsset[3].id, Related_Opportunity__c = oppList[0].id, Related_Account__c = accList[0].id);

                Opportunity_Asset_Junction__c oaj5 = new Opportunity_Asset_Junction__c(Related_Asset__c = lstAsset[4].id, Related_Opportunity__c = oppList[0].id, Related_Account__c = accList[0].id);

                Opportunity_Asset_Junction__c oaj6 = new Opportunity_Asset_Junction__c(Related_Asset__c = lstAsset[5].id, Related_Opportunity__c = oppList[0].id, Related_Account__c = accList[0].id);

                Opportunity_Asset_Junction__c oaj7 = new Opportunity_Asset_Junction__c(Related_Asset__c = lstAsset[5].id, Related_Opportunity__c = oppList[1].id, Related_Account__c = accList[0].id);

                lstOAJ.add(oaj1);
                lstOAJ.add(oaj2);
                lstOAJ.add(oaj3);
                lstOAJ.add(oaj4);
                lstOAJ.add(oaj5);
                lstOAJ.add(oaj6);
                lstOAJ.add(oaj7);
                
                insert lstOAJ;
                Test.startTest();
                //Inserting feedItem
                FeedItem fItem = new FeedItem();
                fItem.Type = 'LinkPost';
                fItem.ParentId =oppList[0].id;
                fItem.Title='Sales Promotions'; 
                fItem.LinkUrl='http://one.emc.com/clearspace/community/active/operation_mojo?view=overview';
                fItem.Body = 'There are competitive assets linked to this opportunity - Check out the Sales Promotions that you may qualify for!';
                insert fItem;

                //Inserting GFS records
                Trade_Ins_Competitive_Swap__c trade1 = new Trade_Ins_Competitive_Swap__c(Related_Opportunity__c = oppList[0].id, Registration_ID__c = 'test', Swap_Value__c = 1);
                Database.SaveResult result = Database.insert(trade1);
                trade1.Comments__c ='TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestestTestTestTestTestTestTestTestTestTest';
                Database.SaveResult result2 = Database.update(trade1);

                List<Database.SaveResult> results = new List<Database.SaveResult>();
                results.add(result);
                results.add(result2);
                
                ApexPages.currentpage().getParameters().put('Id',oppList[0].Id);
                ApexPages.StandardController oppController=new ApexPages.StandardController(oppList[0]);
                
                SFA_MOJO_OptyLinkDelinkController sfaMojoLinkUnlink= new SFA_MOJO_OptyLinkDelinkController(oppController);            
                SFA_MOJO_OptyLinkDelinkController.assetwrapper astwrp= new SFA_MOJO_OptyLinkDelinkController.assetwrapper(lstAsset[4],'xxx');   
                astwrp.selected=true;
                

                sfaMojoLinkUnlink.updateTradeInsAndSwaps(setOppId);

                List<SFA_MOJO_OptyLinkDelinkController.assetwrapper> lst = sfaMojoLinkUnlink.getAssetstoBeLinkedDetails();
                lst.clear();
                lst.add(astwrp);
                sfaMojoLinkUnlink.delinkAsset();
                sfaMojoLinkUnlink.doSave();

                sfaMojoLinkUnlink.docancel();
                sfaMojoLinkUnlink.linkToOpportunity();

                sfaMojoLinkUnlink.updateTradeInsAndSwaps(setOppId);
                Boolean bln1 = sfaMojoLinkUnlink.getOpportunityClosed();
                Boolean bln2 = sfaMojoLinkUnlink.getCancel();
                lst = sfaMojoLinkUnlink.getAssetsDetails();
                lst = sfaMojoLinkUnlink.getAssetstoBeLinkedDetails();

                sfaMojoLinkUnlink.getData(results);
                
                //Calling default constructor
                SFA_MOJO_OptyLinkDelinkController obj1 = new SFA_MOJO_OptyLinkDelinkController();
                
                //Calling parametrized constructor
                ApexPages.currentpage().getParameters().put('Id',oppList[1].Id);
                ApexPages.StandardController oppController2 = new ApexPages.StandardController(oppList[1]);
                SFA_MOJO_OptyLinkDelinkController obj2 = new SFA_MOJO_OptyLinkDelinkController(oppController2);
                
                //SFA_MOJO_OptyLinkDelinkController.Feedpost(oppList[0].Id);
                Test.stopTest();
            }
        }
        catch(Exception e)
        {
            
        }
    }
}