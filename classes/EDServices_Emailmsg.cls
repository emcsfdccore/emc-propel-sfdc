/*==================================================================================================================+

|  HISTORY  |                                                                           

|  DATE          DEVELOPER               WR        DESCRIPTION                               


|  ====          =========               ==        =========== 


|  14March2013    Ganesh Soma         WR#247113    SOQL Optimization:Instead of quering on the RecordType object created a formula feild to get the developer name 
|  02July2013     Ganesh Soma         WR#274122    Commented the code which is populating 'IsMailAfterClose__c' field value when the Case status is Closed. 
|                                                  Which is causing the Email Alert 'Education Services - Send Do Not Reply Email' to fire two times.                      
|  15Oct2013      Jaspreet Singh                   Added Method FieldUpdatesForWorkFlows replaces Presales Field update workflows.
|  11Nov2013      Leonard Victor                   Added logic for disabling Case Auto closure after 96hrs
|  22May2014      Abinaya                          Added New Education Proven RecordType. (WR:922)
|  14Nov2014     Bindu               WR#1487      Routing to EMCU mailbox
|  17Mar2015     Bisna               CI WR 1880   New EMCU Webform changes
+=================================================================================================================================================================**/

public class EDServices_Emailmsg {
    Id caseId;
    String fromAddress;
    //Boolean isIncoming;
    String toAddress;
    //String bccAddress;
    String subject;
    String body;
    Case cs;
    //EmailMessage emailMessageLoop;
    Education_Services_Contact__c edserv = new Education_Services_Contact__c();
    public List<Education_Services_Contact__c> lstEdServiceContact = new List<Education_Services_Contact__c>();
    
    /*
    public void EdServ_emsg(List<EmailMessage> emsg){
    
    for(EmailMessage emailMessageLoop:emsg){
    caseId = emailMessageLoop.ParentId;
    fromAddress = emailMessageLoop.FromAddress;
    //isIncoming = emailMessageLoop.Incoming;
    //toAddress = emailMessageLoop.ToAddress;
    subject = emailMessageLoop.Subject;
    body = emailMessageLoop.TextBody;
    cs=[SELECT RecordtypeId,EDS_Contact_Email__c,Status,IsMailAfterClose__c FROM Case WHERE Id =: emailMessageLoop.ParentId]; 
    RecordType id = [select Id,Name,DeveloperName from RecordType where Id =:cs.RecordTypeId and SobjectType='Case'];
    //Reuben L - added fromAddress != null check to if and else if statements to prevent null email address errors with mailer daemon servers
    if( id.DeveloperName== (System.label.EDServices_RecordType) && emailMessageLoop.Incoming == true && fromAddress != null)
    {          
    lstEdServiceContact=[Select Name,id from Education_Services_Contact__c where name=:fromAddress];
    if(lstEdServiceContact.size()>0 && cs.EDS_Contact_Email__c == null)
    cs.EDS_Contact_Email__c = lstEdServiceContact[0].Id;
    else if(cs.EDS_Contact_Email__c == null && fromAddress != null)
    {
    edserv.Name=fromAddress;
    insert edserv;
    cs.EDS_Contact_Email__c=edserv.Id;     
    } 
    if(cs.Status=='Closed'){
    cs.IsMailAfterClose__c = true;
    }
    
    update cs;       
    }        
    }   
    }*/    
    public void EdServ_emsg(List<EmailMessage> emsg){
        
        for(EmailMessage emailMessageLoop:emsg){
            caseId = emailMessageLoop.ParentId;
            fromAddress = emailMessageLoop.FromAddress;
            //isIncoming = emailMessageLoop.Incoming;
            //toAddress = emailMessageLoop.ToAddress;
            subject = emailMessageLoop.Subject;
            body = emailMessageLoop.TextBody;
            cs=[SELECT RecordtypeId,Record_Type_Developer_Name__c,EDS_Contact_Email__c,Status,Origin,Sub_task_Type__c,Type,IsMailAfterClose__c FROM Case WHERE Id =: emailMessageLoop.ParentId]; 
            // Ganesh commneted below to get the Developer name from the formula field instead of quering on the Recordtype object 
            // RecordType id = [select Id,Name,DeveloperName from RecordType where Id =:cs.RecordTypeId and SobjectType='Case'];
            //Reuben L - added fromAddress != null check to if and else if statements to prevent null email address errors with mailer daemon servers
            //WR-922:Included Education Services Proven RecordType in the below condition to update Contact Email in case.
            //1880 change
            if((cs.Record_Type_Developer_Name__c!=null && (cs.Record_Type_Developer_Name__c == (System.label.EDServices_RecordType) || cs.Record_Type_Developer_Name__c == (System.label.EDServices_Proven_RecordType) || cs.Record_Type_Developer_Name__c == (System.label.Global_Education_Service)|| cs.Record_Type_Developer_Name__c == (System.label.EMCU_Record_Type))) && emailMessageLoop.Incoming == true && fromAddress != null)
            {          
                lstEdServiceContact=[Select Name,id from Education_Services_Contact__c where name=:fromAddress];
                if(lstEdServiceContact.size()>0 && cs.EDS_Contact_Email__c == null)
                    cs.EDS_Contact_Email__c = lstEdServiceContact[0].Id;
                else if(cs.EDS_Contact_Email__c == null && fromAddress != null)
                {
                    edserv.Name=fromAddress;
                    insert edserv;
                    cs.EDS_Contact_Email__c=edserv.Id;     
                } 
               /* //WR#1487 Routing to EMCU mailbox
                 if((cs.Record_Type_Developer_Name__c == (System.label.Global_Education_Service) || cs.Record_Type_Developer_Name__c == (System.label.EMCU_Record_Type) )&& cs.Origin=='Email')
                 {
                 System.debug('Mailbox address'+System.label.EMCU_Mailbox_address);
                 emailMessageLoop.ToAddress= System.label.EMCU_Mailbox_address;
                  System.debug('To Address'+emailMessageLoop.ToAddress);
                 cs.Task_Type__c='EMCU';
                 System.debug('sub_task_type'+cs.Task_Type__c);
                 }
                 //End of #1487
                 */
                //WR#274122-Commented by Ganesh
                /*
                if(cs.Status=='Closed'){
                cs.IsMailAfterClose__c = true;
                }
                */
                update cs;       
            }        
        }   
    }
    /* @Method <This method execute is used to auto assign a case to a queue according to the assignment rule.>
    @param <This method takes List<Case> as parameter>
    @return void - <Not returning anything>
    @throws exception - <No Exception>
    */
    public void autoAssignmentToQueue(List<Case> lstCase){
        List<Case> lstToUpdate = [select Id,OwnerId from Case where Id in:lstCase]; 
        List<CaseShare> cs = new List<CaseShare>();
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule= true;
        if(lstToUpdate.size()>0){
            for(Case caseRecord:lstToUpdate){
                caseRecord.setOptions(dmo);                
            }            
        }        
        update lstToUpdate;   
    }
    /* @Method <This method is replacing the existing workflows for case object field updates>
    @param <Pass Flag as flagOnlyInsert == true while before inserting and add for conditions only where we are doing for Insert>
    @returns void*/
    public void FieldUpdatesForWorkFlows(List<case> lstCase, Boolean flagOnlyInsert){
        for(Case objCase : lstCase){
            string ownerId = string.valueOf(objCase.OwnerId);
            system.debug('###ownerId--->'+ownerId);
            System.debug('Method start ###objCase.IsEscalated--->'+objCase.IsEscalated);
            System.debug('Method start ###objCase.wasEscalated__c--->'+objCase.wasEscalated__c);
            system.debug('flagOnlyInsert--->'+flagOnlyInsert);
            //WR-922:Included Education Services Proven RecordType in the below condition to update Status in case.
            if(ownerId.left(3) == '00G' && (objCase.Record_Type_Name__c == 'Education Services Record Type' || objCase.Record_Type_Developer_Name__c == System.label.EDServices_Proven_RecordType || 
                                            objCase.Record_Type_Developer_Name__c == 'Global_Education_Support' || objCase.Record_Type_Developer_Name__c == 'EMCU_Record_Type') && objCase.status == 'Assigned'){
                                                //EdServices - Assigned to New  //Global Education Support - Assigned to New
                                                //Evaluate the rule when a record is created, and any time it's edited to subsequently meet criteria
                                                system.debug('###ownerId--->'+ownerId);
                                                objCase.Status = 'Submitted';
                                            }
            //WR-922:Included Education Services Proven RecordType in the below condition to update Status in case.
            if(ownerId.left(3) != '00G' && (objCase.Record_Type_Name__c == 'Education Services Record Type' || objCase.Record_Type_Developer_Name__c == System.label.EDServices_Proven_RecordType || 
                                            objCase.Record_Type_Developer_Name__c == 'Global_Education_Support' ||objCase.Record_Type_Developer_Name__c == 'EMCU_Record_Type') && objCase.status == 'Submitted' && !(flagOnlyInsert)){
                                                //EdServices - New to Assigned //Global Education Support - New to Assigned
                                                //Evaluate the rule when a record is created, and any time it's edited to subsequently meet criteria
                                                objCase.Status = 'Assigned';
                                            }
            /*  if(objCase.IsMailAfterClose__c == true && objCase.status == 'Closed' && objCase.Record_Type_Name__c == 'Education Services Record Type'){
            //Education Services - New email when Closed
            //Evaluate the rule when a record is created, and any time it's edited to subsequently meet criteria    
            objCase.IsMailAfterClose__c = false;
            }*/
                        /*if( (objCase.Record_Type_Name__c == 'Education Services Record Type' || objCase.Record_Type_Developer_Name__c == 'Global_Education_Support')
            && (objCase.Status == 'In Progress' || objCase.Status == 'Customer Response Required')){
            //Education Services - Updatse New Email Checkbox by Status //Global Education Support - Update New Email Checkbox by Status
            //Evaluate the rule when a record is created, and any time it's edited to subsequently meet criteria
            objCase.New_Email__c = false;
            }*/
                        /* if( ( objCase.Record_Type_Developer_Name__c == 'Global_Education_Support' || objCase.Record_Type_Name__c == 'Education Services Record Type')
            && objCase.EDS_Contact_Email__r.Name != objCase.EDS_Email__c){
            //Global Education Support - Email Field Update
            //Evaluate the rule when a record is created, and every time it's edited
            objCase.EDS_Email__c = objCase.EDS_Contact_Email__r.Name;
            objCase.EdServices_IsEmailToCase__c = true;
            }*/
                        //System.debug('objCase#$#$#$#$##$--->'+objCase);            
        }
    }    
    /* @Method <This method is Used to Blank out SLA_Completed__c  so that Auto Closure Dosent happen>
    @param <Listt of email message>
    @returns void*/
    
    public void disableAutoClosure(List<EmailMessage> lstemailMsg){        
        List<Id> caseIdLst = new List<Id>();        
        for(EmailMessage emailMessageLoop : lstemailMsg){
            if(emailMessageLoop.Incoming)            
                caseIdLst.add(emailMessageLoop.ParentId);
        }        
        System.debug('caseIdLst-------->'+caseIdLst);        
        if(caseIdLst.size()>0){            
            List<Case> lstCase= [Select id,SLA_Completed__c,Status from case where id in :caseIdLst];
            System.debug('caseIdLst-------->'+lstCase);            
            for(Case caseObj : lstCase){                
                if(caseObj.Status == 'Resolved' && caseObj.SLA_Completed__c!=null )
                    caseObj.SLA_Completed__c = null;
            }            
            System.debug('caseIdLst111111-------->'+lstCase);  
            update lstCase;
        }
    }
    /* @Method <This method is Used to Blank out SLA_Completed__c  so that Auto Closure Dosent happen>
    @param <Map of Feed Item>
    @returns void*/    
    public void disableAutoClosureFeedItem(Map<Id,FeedItem> mapFeed){        
        List<Id> caseLst = new List<Id>();
        Boolean isUpdated = false;        
        for(FeedItem feed:mapFeed.values()){
            caseLst.add(feed.ParentId);  
        }        
        if(caseLst.size()>0){            
            List<Case> caseObLst = [Select id,SLA_Completed__c,Status,Record_Type_Developer_Name__c from Case where id in :caseLst];
            for(Case caseObj : caseObLst){
                if(caseObj.Status=='Resolved' && caseObj.SLA_Completed__c!=null &&  ((caseObj.Record_Type_Developer_Name__c!=null && caseObj.Record_Type_Developer_Name__c.Contains('Education_Services')) || (caseObj.Record_Type_Developer_Name__c!=null && (caseObj.Record_Type_Developer_Name__c.Contains('Global_Education')||caseObj.Record_Type_Developer_Name__c.Contains('EMCU'))))){
                    caseObj.SLA_Completed__c = null;
                    isUpdated = true;
                }
            }
            if(isUpdated){
                update caseObLst;
            }
        }
    }
}