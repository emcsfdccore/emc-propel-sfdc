public with sharing class Case_PSC_Requestor_Case_Escalation_Ctrl {

    public String selectedLanguage{get; set;}
    Public  String CaseNum{get; set;}
    Public  String ConName{get; set;}
    Public  String ConMail{get; set;}
    Public  String ConPhone{get; set;}
   ///Public  String Contact{get; set;}
    Public  String comment{get;set;}
   //Public  String Email{get; set;}
    Public  String OpptyNum{get; set;}
    Public  Case InputCaseData{get; set;}
    List<Opportunity> OpptyList;
    List<Lead> LeadList;
    List<Opportunity_Registration__c> OpptyRegList;
    //Public String RT{get; set;}
    Private Boolean Flag = False;
    
    /*public Case_PSC_Requestor_Case_Escalation_Ctrl(ApexPages.StandardController controller) {
        OpptyNum = '';
        //LeadNum = '';
        this.InputCaseData = (Case)controller.getRecord();

    }*/
    public Case_PSC_Requestor_Case_Escalation_Ctrl(){
     selectedLanguage= ApexPages.currentPage().getParameters().get('langForPage');
     if(selectedLanguage==null || selectedLanguage==''){
     
     selectedLanguage= 'en_US';
     
     }
    }
     //Method to cancel the transaction
    public Pagereference CancelAction(){
        Pagereference canPageRef=new Pagereference ('/apex/Cases_PSC_Site_MainPage');
        return canPageRef;
    }
    public static boolean isUserEmailValid(String userEmail) {
        Boolean flag = false;
        if(userEmail != null) {
            String  emailRegex      = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
            Pattern emailPattern    = Pattern.compile(emailRegex);
            Matcher emailMatcher    = emailPattern.matcher(userEmail.trim());
            flag                    =  emailMatcher.matches();
        }
        system.debug('FlagEmail*******'+flag);
        return flag;
    }
    
    Public Pagereference CheckCase()
    {
        if(CaseNum == null || CaseNum == ''){
            
            ApexPages.Message Error_Msg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.PSC_Esc_CaseNum_Req);
              ApexPages.addMessage(Error_Msg);      
              return null; 
        }
        if(comment == null || comment == ''){
            
            ApexPages.Message Error_Msg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.PSC_Esc_Justify_Req);
              ApexPages.addMessage(Error_Msg);      
              return null; 
        }
        
        List<Case> CaseList = [SELECT id,record_type_name__c,Escalation_Request__c,Escalation_Justification__c,CaseNumber,description FROM Case WHERE CaseNumber = :CaseNum ];
        if(CaseList.size()>0){
            String Cust_Label = System.label.Case_PSC_RT;
            List<String> RTList = Cust_Label.split(';');
            For(String RT : RTList)
            {
                
                If(string.valueof(CaseList[0].Record_Type_Name__c) == RT)
                    {
                        System.debug('inside if '+RT);
                        Flag = True;
                        break;
                    }  
            }
            if(Flag){
                if(ConName!=null){
                CaseList[0].description=CaseList[0].description+' Contact Name :'+ConName;
                }
                if(ConMail!=null && ConMail!=''){
                                      Boolean emailCheck=isUserEmailValid(ConMail);
                                      if(!emailCheck){
                                      ApexPages.Message Error_Msg = new ApexPages.Message(ApexPages.Severity.ERROR,system.label.Case_PSC_Email_Check);
                                      ApexPages.addMessage(Error_Msg);      
                                      Return null;              
                                                                                }
                CaseList[0].description=CaseList[0].description+'\n'+' Contact Mail :'+ConMail;
                }
                if(ConPhone!=null){
                CaseList[0].description=CaseList[0].description+'\n'+' Contact Phone :'+ConPhone;
                }
                if(comment!=null){
                CaseList[0].Escalation_Justification__c=comment;
                }
                CaseList[0].Escalation_Request__c=true;
                
            }
            update CaseList;
                Pagereference Refer_To_Case = new pagereference('/apex/PSCEscalteSuccessPage');
                return Refer_To_Case;
                
        }
        
        else If(CaseList.size() == 0) 
        {              
            ApexPages.Message Error_Msg = new ApexPages.Message(ApexPages.Severity.ERROR,system.label.PSC_Invalid_Case_Num);
            ApexPages.addMessage(Error_Msg);      
            Return Null;
        }
        else
        {
            
            ApexPages.Message Error_Msg = new ApexPages.Message(ApexPages.Severity.ERROR,system.label.PSC_Esc_Non_Authentic);
            ApexPages.addMessage(Error_Msg);      
            Return Null;
            
            Return Null;
        }  
    }  
}