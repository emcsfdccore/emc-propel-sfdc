/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER               WR          DESCRIPTION                               
 |  ====            =========               ==          =========== 
 |  08.06.2013      EAG App Admins                      Initial Creation.
 |  16.11.2013      Bhanu Prakash         WR319143      uncommented line no. 52 to execute KB_BatchOutboundMessages batch.
 |  17.Dec.2014     Bisna V P             CI WR 1127    Updated to solve -Find and Replace tool discrepancy 
 +===========================================================================*/
@isTest
private class BatchKnowledgeGlobalSearch_TC {
    static testMethod void batchKnowledgeGlobalSearch(){        
        Test.startTest(); 

        ESA__kav breakfix = new ESA__kav();
        breakfix.Title = 'Testing article'; 
        breakfix.Cause__C='Bisna vadakkina'; 
        breakfix.ValidationStatus = 'Work In Progress';
        breakfix.UrlName = 'TTTest-Class-Break-fix'; 
        breakfix.Link_To_Remedies__c= 'Bisna vadakkina'; 
        breakfix.Language = 'en_US'; 
        breakfix.Cause__c= ' TTTest...';    
        insert breakfix;        

        List<id> idList = new List<id>();
        idList.add(breakfix.id);
        String strArticleType='ESA__kav';
        String strPublishStatus ='Draft';
        String strSearchString= 'Bisna';       
        String strReplacementString ='SuccessTest';                      
        String strSearchFields ='Link_To_Remedies__c'; 
                                                                                         
        BatchKnowledgeGlobalSearch batch =
                new BatchKnowledgeGlobalSearch(false,strArticleType, strPublishStatus, 
                strSearchFields, strSearchString, strReplacementString,false);
                                 
        KB_Global_Search_Scope__c gss= new KB_Global_Search_Scope__c();
        gss.ArticleId__c = breakfix.id;        
        insert gss;
        
        List<Id> idListgss= new List<Id>();
        idListgss.add(gss.id);
       // batch.strQuery = 'SELECT ArticleId__c from KB_Global_Search_Scope__c where id in:idListgss LIMIT 5';//where id=gss.id//Bhanu-redused Limit form 100 to 5      
        //batch.nBatchSize = 5;
        Database.executeBatch(batch);
//Added by Bhanu- Start
        // BatchKnowledgeGlobalSearch batch2 = new BatchKnowledgeGlobalSearch(true, strArticleType, strPublishStatus,strSearchFields, strSearchString, strReplacementString,true);
        //1127 changes
        BatchKnowledgeGlobalSearch batch3 = new BatchKnowledgeGlobalSearch( false, strArticleType, strPublishStatus, strSearchFields,strSearchString, strReplacementString,false);
       // batch3.strQuery = 'SELECT ArticleId__c from KB_Global_Search_Scope__c where id in:idListgss LIMIT 5';//where id =gss.id//Bhanu-redused Limit form 100 to 5       
        //batch3.nBatchSize = 5;
        Database.executeBatch(batch3);
        //1127 changes ends here
//Added by Bhanu - End
        Test.stopTest();                
    } 
}