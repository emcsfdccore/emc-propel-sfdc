/*
| History
| -------------
| 22-April-2015   Vinod Jetti   CI#1876   Updated test for coverage of added componenets in main class
*/
@isTest
private class R2R_Utility_TC
{   
  static testMethod void setTestAssetOnOpportunity()
    {             
        System.runAs(new user(Id = UserInfo.getUserId()))
            {
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
                CustomSettingDataHelper.countryTheaterMapCSData();
                CustomSettingDataHelper.dealRegistrationCSData();
                CustomSettingDataHelper.VCEStaticCSData();
            } 
        
        Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
        String recordtypeEMC = data.get('Asset EMC Install Record Type Id').DataValue__c;
        String recordtypeComp = data.get('Asset Competitive Record Type Id').DataValue__c;
        Boolean flag = true;    
           
        List<Account> acc = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();  
        insert acc;  
        //Creating list type ID
        List<Id> listId=new List<Id>();
        listId.add(acc[0].id);
        
        //Creating Lead
        list<Lead> listLead = new list<Lead>();
        listLead.add(new lead (Company='TestLead' ,LastName='Test0', Status='Working',Sales_Force__c='EMC',Lead_Originator__c='Customer Intelligence'));
        listLead.add(new lead (Company='TestLead' ,LastName='Test1', Status='Pass to Renewals',Sales_Force__c='EMC',Lead_Originator__c='Customer Intelligence'));
        insert listLead;
        
        // create asset
        List<Asset__c> listSelectedAssets=new List<Asset__c>();
        listSelectedAssets.add( new Asset__c(Name='TestassetComp',Customer_Name__c=acc[0].id, RecordTypeId = recordtypeComp,Product_Name_Vendor__c='Dell', Swap_Trade_In_Cost_Relief_Value__c = 123));
        listSelectedAssets.add( new Asset__c(Name='TestassetEMC',Customer_Name__c=acc[0].id, RecordTypeId = recordtypeEMC,Product_Name_Vendor__c='EMC', Product_Family__c ='CONNECTRIX' , Swap_Trade_In_Cost_Relief_Value__c = 123 ));
        listSelectedAssets.add( new Asset__c(Name='TestassetEMC',Customer_Name__c=acc[0].id, RecordTypeId = recordtypeEMC,Product_Name_Vendor__c='EMC',  Swap_Trade_In_Cost_Relief_Value__c = 123 ));
        insert listSelectedAssets ;
               
        //Creating Oppty
        List<Opportunity> listOpp=new List<Opportunity>();
        listOpp.add( new Opportunity(Name='Testoppty0', AccountId = acc[0].id, Opportunity_Type__c='Refresh',closeDate =  System.today()+5, Sales_Channel__c='Direct', Sales_Force__c='EMC',StageName='Pipeline' ));
        listOpp.add( new Opportunity(Name='Testoppty1', AccountId = acc[0].id, Opportunity_Type__c='Renewals',closeDate =  System.today()+5, Sales_Channel__c='Direct', Sales_Force__c='EMC',StageName='Pipeline' ));
        insert listOpp;
        
        //Creating Opportunity  asset junction
        List<Opportunity_Asset_Junction__c> listOaj= new List<Opportunity_Asset_Junction__c>();
        listOaj.add( new Opportunity_Asset_Junction__c(Related_Asset__c=listSelectedAssets[0].id, Related_Opportunity__c=listOpp[0].id));
        listOaj.add( new Opportunity_Asset_Junction__c(Related_Asset__c=listSelectedAssets[1].id, Related_Opportunity__c=listOpp[1].id));
        listOaj.add( new Opportunity_Asset_Junction__c(Related_Asset__c=listSelectedAssets[2].id, Related_Opportunity__c=listOpp[1].id));
        insert listOaj; 
           
        //Creating Lead asset junction with lead status =Swap and EMC asset attached
        List<Lead_Asset_Junction__c> listlaj= new List<Lead_Asset_Junction__c>();
        listlaj.add(new Lead_Asset_Junction__c ( Related_Asset__c = listSelectedAssets[0].id, Related_Lead__c=listLead[0].id ));
        listlaj.add(new Lead_Asset_Junction__c ( Related_Asset__c = listSelectedAssets[1].id, Related_Lead__c=listLead[1].id ));
        insert(listlaj);
         
         list<Trade_Ins_Competitive_Swap__c> Traderecord = new list<Trade_Ins_Competitive_Swap__c>();
         Traderecord.add(new Trade_Ins_Competitive_Swap__c(related_opportunity__c = listOpp[0].id, Registration_Type__c = 'Connectrix Trade In' ));
         Traderecord.add(new Trade_Ins_Competitive_Swap__c(related_opportunity__c = listOpp[0].id, Registration_Type__c = 'EMC Trade In' ));
         insert Traderecord;

        //Creating list type ID
        set<Id> setAssetId=new set<Id>();
        setAssetId.add(listSelectedAssets[0].id);
        setAssetId.add(listSelectedAssets[1].id); 

        //Creating list type ID
        set<ID> setOppId = new set<ID>();
        setOppId.add(listOpp[0].id);
        setOppId.add(listOpp[1].id);
           
        Test.startTest();
            R2R_Utility.getAsset(listId);
            R2R_Utility.getLeads('test');
            R2R_Utility.getAssets('test');
            R2R_Utility.getLeads('test','TestLead');
            R2R_Utility.getAssetOnOpportunity(listOpp[0].id);
            R2R_Utility.validateLeadLinking(listlaj);
            R2R_Utility.getAssetsOnLead(listLead[0].id);
            R2R_Utility.getAssetOnProfile(acc[0].id);
            R2R_Utility R2RUtility = new R2R_Utility();
            //#1876 passing falge parameter in this method
            R2RUtility.getOpportunityReregistration(listSelectedAssets,flag);
            R2RUtility.getOpptyRelatedToAsset(setAssetId);
            R2RUtility.setTotalSwapValueOnTradeIn(setOppId);
        Test.stopTest();
    }

}