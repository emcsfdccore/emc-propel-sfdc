/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
|  17/Sep/2014  Aarti Jindal       BPP Project      Test Class for PRM_BPP_MultiplePSMsToPartnerAccount
|
+=====================================================================================================*/

@isTest
private class PRM_BPP_MultiplePSMsToPartnerAccount_TC{
    
    private static testMethod void onInsertPSMLogic(){
    
      //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        } 
        
      
        List<User> userList=new List<User>();
        userList = [Select Id,Profile.name from User where Profile.name = 'System Administrator' AND isActive = true limit 3];
        
        List<Account> accList=new List<Account>();
        
        //Inserting Account records
        for(Integer k=0;k<3;k++){        
          Account accObj1 = new Account();        
          accObj1.name = 'TestAccount'+ k;        
          accObj1.BillingCountry ='United States';       
          accObj1.BillingCity = 'EDWARDS AFB';         
          accObj1.Partner_Type__c='Distribution VAR';
          accList.add(accObj1);
        }
        insert accList;
        
        Test.StartTest();
        // List of PSMs which are not alredy there in account team of respective accounts
        List<Additional_Partner_Sales_Managers__c> psmList=new List<Additional_Partner_Sales_Managers__c>();
        
        //List of PSMs which are already there in their respective account teams
        List<Additional_Partner_Sales_Managers__c> adpsmList=new List<Additional_Partner_Sales_Managers__c>();
        
        for(Integer j=0;j<2;j++){
           psmList.add(new Additional_Partner_Sales_Managers__c(TeamMember__c=userList[j].Id,Team_Role__c='PSM',Account__c=accList[j].Id));
        }
        
        //Testing after insert trigger
        insert psmList; 
        //Testing after update trigger
        psmList[0].Team_Role__c='NAM';
        update psmList;
        
        //Testing after delete trigger
        delete psmList[1];
        
        AccountTeamMember atm = new AccountTeamMember(AccountId=accList[2].Id,UserId=userList[2].Id,TeamMemberRole='PSM');
        insert atm;
        
        adpsmList.add(new Additional_Partner_Sales_Managers__c(TeamMember__c=userList[2].Id,Team_Role__c='PSM',Account__c=accList[2].Id));
        insert adpsmList;
        
        adpsmList[0].Team_Role__c='NAM';
        update adpsmList;
        delete adpsmList;
        
        Test.StopTest();    
    }
}