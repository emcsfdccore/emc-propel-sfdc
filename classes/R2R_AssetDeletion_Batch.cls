/*=====================================================================================================+
|  HISTORY  |                                                                            
|  DATE          DEVELOPER                WR            DESCRIPTION                               
|  ====          =========                ==            ===========                                                     
| 10/09/2014    Partha Baruah       WR 302/380      R2R Nov'14 Rel - Delete assets with "Marked_for_Deletion__c" field value as Yes and not associated to any Lead or Opp
| 10/12/2014    Sneha Jain          WR 302/380      R2R Nov'14 Rel - Implemented Schedulable interface for scheduling the batch class
+=====================================================================================================*/

public class R2R_AssetDeletion_Batch implements Database.Batchable<SObject>, Schedulable {
       
    public string batchQuery = '';  
    String emcInstallRT = 'EMC Install';
    String emcDeinstallRT= 'EMC De-Install';
    
    //Added to schedule the class from UI
    public void execute(SchedulableContext sc) {      
      
        Id batchProcessId = Database.executeBatch(new R2R_AssetDeletion_Batch(), 200);
    }
    
    public R2R_AssetDeletion_Batch (){

        //This query returns the assets (Install or De-Install) with "Marked_for_Deletion__c" field value set as Yes
        batchQuery = 'select id,Marked_for_Deletion__c, Name, IB_Instance_Number__c, Serial_Number__c' + ' from Asset__c where (RecordType.Name = \'' + emcInstallRT + '\' or RecordType.Name =  \'' + emcDeinstallRT + '\' )and Marked_for_Deletion__c = \'Yes\'';
    }
    
    /*@Method <This method gets executed acutomatically when the batch job is started.>
    @param <Database.BatchableContext BC - Batchable context>
    @return <void> - <Not returning anything>
    @throws exception - <No Exception>
    */
    public Database.QueryLocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(batchQuery);
    }
    
    /*@Method <This method gets executed automatically when the batch job is execution mode.>
    @param <Database.BatchableContext BC - Batchable context>
    @param <List<Sobject> scope - scope>
    @return <void> - <Not returning anything>
    @throws exception - <No Exception>
    */
    public void execute(Database.BatchableContext BC, list<SObject> scope){
        Set<Id> assetIdCombined = new Set<Id>();
        List<Asset__c> assetsToDelList = new List <Asset__c>();
        List<Asset__c> assetMarkedTrueList = (Asset__c[]) scope;
        
        if(assetMarkedTrueList.size()>0){
            
            //Query all the Lead Asset Junction records associated with the Assets marked for deletion
            List<Lead_Asset_Junction__c> lstLeadAsset = [select Related_Asset__c from Lead_Asset_Junction__c where  Related_Asset__c in: assetMarkedTrueList ];
            for(Lead_Asset_Junction__c leadAssetObj : lstLeadAsset){
                assetIdCombined.add(leadAssetObj.Related_Asset__c);          
            }

            //Query all the Opportunity Asset Junction records associated with the Assets marked for deletion
            List<Opportunity_Asset_Junction__c> lstOppAsset = [select Related_Asset__c from Opportunity_Asset_Junction__c where Related_Asset__c in: assetMarkedTrueList];
            for(Opportunity_Asset_Junction__c opptyAssetObj : lstOppAsset){
                assetIdCombined.add(opptyAssetObj.Related_Asset__c);          
            }
            //Filter only those asset records which do not have any Lead/Oppty Asset junction record attached
            
            system.debug('#### AssetID combined :: '+assetIdCombined);
            system.debug('#### Asset Marked true list :: '+assetMarkedTrueList);

            for(Asset__c assetObj : assetMarkedTrueList){
                if(assetIdCombined ==null || assetIdCombined.size() ==  0 || 
                    (assetIdCombined.size()>0 && !assetIdCombined.contains(assetObj.Id)))
                {
                    assetsToDelList.add(assetObj);
                }   
            }   
            
            system.debug('#### Assets to dlete list :: '+assetsToDelList);

            //Delete the assets which are marked for deletion and are not associated to any lead or opportunity
            if(assetsToDelList!=null && assetsToDelList.size()>0)
            {
                //delete assetsToDelList;

                //Added code below for R2R Feb 2015 Release WR 399190

                System.debug('#### Inside Deletion block');
                System.debug('#### Assets for deletion: '+assetsToDelList);

                List<Deleted_Assets__c> lstDeletedAssets = new List<Deleted_Assets__c>();

                for (Asset__c objAsset: assetsToDelList) 
                {
                    lstDeletedAssets.add(new Deleted_Assets__c(Asset_Id__c = objAsset.Id, Asset_Name__c = objAsset.Name, 
                        Deletion_Date__c = System.today(), IB_Instance_Number__c = objAsset.IB_Instance_Number__c, Serial_Number__c = objAsset.Serial_Number__c));
                }
                

                delete assetsToDelList;

                system.debug('#### Deleetd assets to be inserted (after delete) :: '+lstDeletedAssets);
                
                insert lstDeletedAssets;

                //Code added for R2R Feb 2015 Release WR 399190 ends above
            }
        }
    }
    
    /*@Method <This method gets executed automatically when the batch job is finised. We are deleting the job at the end.>
    @param <Database.BatchableContext BC - Batchable context>
    @return <void> - <Not returning anything>
    @throws exception - <No Exception>
    */
    public void finish(Database.BatchableContext BC){      
   
    }   
}