/*=============================================================================
    HISTORY                                                                  
                                                               
    DATE            DEVELOPER                WR             DESCRIPTION                               
    ====            =========                ==             =========== 
    19-03-2014     Aarti Jindal         Visibility         Test Class for Visibility_ChatterPost
==============================================================================*/
@isTest(seeAllData=true)
private class TC_Visibility_ChatterPost {

    //Added for Fixing party number issue

    public static String generateRandomString(Integer len) {
    final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    String randStr = '';
    while (randStr.length() < len) {
       Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
       randStr += chars.substring(idx, idx+1);
    }
    return randStr; 
}
 
    
        
		private static testMethod void testVisibility_ChatterPost(){
               
        List<Account> lstAccount = new List<Account>();
        List<Opportunity> opptyList=new List<Opportunity>();
        
        //Fetch Users
         User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1]; 
        
        //Create Accounts and assign Core Quota Rep   
        
        Account tempAccObj= new Account(name='UNITTESTAcc 0',Party_Number__c = TC_Visibility_ChatterPost.generateRandomString(10), BillingCountry ='Colombia',Synergy_Account_Number__c = '10', Status__c='A',State_Province_Local__c = 'testState3',Street_Local__c='test1',Zip_Postal_Code_Local__c='23454',Core_Quota_Rep__c = insertUser.Id);
        lstAccount.add(tempAccObj);
        Account tempAccObj1= new Account(name='UNITTESTAcc 1',Party_Number__c = TC_Visibility_ChatterPost.generateRandomString(12), BillingCountry ='Colombia',Synergy_Account_Number__c = '10', Status__c='A',State_Province_Local__c = 'testState3',Street_Local__c='test1',Zip_Postal_Code_Local__c='23454',Core_Quota_Rep__c = insertUser.Id);
        lstAccount.add(tempAccObj1);
        Account tempAccObj2= new Account(name='UNITTESTAcc 2',Party_Number__c = TC_Visibility_ChatterPost.generateRandomString(15), BillingCountry ='Colombia',Synergy_Account_Number__c = '10', Status__c='A',State_Province_Local__c = 'testState3',Street_Local__c='test1',Zip_Postal_Code_Local__c='23454',Core_Quota_Rep__c = insertUser.Id);
        lstAccount.add(tempAccObj2);
        
        insert lstAccount;
        
        //Create Opportunities 
        for(Account objAcc : lstAccount){
            Opportunity tempOppty = testclassUtils.getOppty();
            tempOppty.AccountId =  objAcc.id;
            opptyList.add(tempOppty);
        }
        Test.startTest();
        insert opptyList;
        Test.stopTest();
    } 
}