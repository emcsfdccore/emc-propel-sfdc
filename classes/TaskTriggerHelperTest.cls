// Test class for UpdateTypePicklist Trigger  -WR # 230704
// Modify the test class for UpdateTypePiclist Trigger -WR # 259803
//6-June-2014 CI 676, updated task recordtype queries as 'Renewals Record Type' is deactivated

@isTest(SeeAllData=true)

private class TaskTriggerHelperTest {

//Test Method for Task Trigger Helper class    
static testmethod void MyUnitTest(){
Boolean hasfired = TaskTriggerHelper.hasAlreadyfired();
TaskTriggerHelper.setAlreadyfired();
}
 //Task Insertion into custom Object          
 static testmethod void createNewTask() {
       //Updated CI 676, changed DeveloperName like 'Renewal%' to  'Master%'
       RecordType taskRecordType = [select id from RecordType where sObjectType='Task' and DeveloperName like 'Master%' LIMIT 1];
          
         Account acc = testclassUtils.getAccount() ;
            
             insert acc ;
   // Opportunity Opp= testclassUtils.getOppty ();
       //  insert Opp;
      Id accid = acc.Id;
      Lead led=new Lead();
      led.company='test';
      led.lastname='test name';
      
      insert led;
        Task T = new Task();
        T.Type__c = 'PVR';
        T.Recordtypeid = taskRecordType.id;
        T.Type_Detail__c = 'Redirect';
        T.Subject = 'Renewals Type';
        T.Task_Comments__c = 'Test1';
        T.Description = 'Test';
        T.Rework_Types__c = 'Correction';
        
        T.Status = 'Completed';
        T.Status_Update_Date_Time__c = DateTime.now();
        T.VCE_Assigned_To__c = 'Testing';
        T.VCE_Last_Modifying_Partner__c = 'EMC';
        T.WhatId = accid;
        T.Type='Email';
        // T.whoid=led.id;
        
        insert T;
        
               
     
OAR_Member_Added__c obj = new OAR_Member_Added__c();
obj.Text_1__c = T.Type__c;
obj.Text_2__c = T.Type_Detail__c;
obj.Text_3__c = T.Subject;
obj.Text_4__c = T.Description;
obj.Text_7__c = T.Status;

  obj.user_1__c = t.ownerid;

  obj.User2__c = t.CreatedById;

String urlForObj= URL.getSalesforceBaseUrl().toExternalForm() + '/'+T.WhatId;
obj.Task_Related_To_URL__c = urlForObj;
String RelUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/'+T.Id;
obj.Task_URL__c = RelUrl;

    //  String Oppnum = opp.Opportunity_Number__c;
    //  obj.Text_5__c = Oppnum;
      String accname = T.Whatid;
      obj.Text_6__c =accname ;
   
insert obj;
update T;        
        }
 // Task Field Udpate Test       
Static TestMethod Void TriggerTest(){

            String Curr = Userinfo.getDefaultCurrency();
            String UName = Userinfo.getName();
            DateTime ModDate = DateTime.now();
            
          Opportunity Opp= testclassUtils.getOppty ();
          insert Opp;
          Id oppid = opp.Id;
Lead led=new Lead();
led.company='test';
led.lastname='test name';

Insert led;

 Task T1 = new Task();
 //Updated CI 676, changed DeveloperName like 'Renewal%' to  'Master%'
 RecordType taskRecordType = [select id from RecordType where sObjectType='Task' and DeveloperName like 'Master%' LIMIT 1];
        T1.Type__c = 'PVR';
        T1.Type_Detail__c = 'Redirect';
        T1.Subject = 'Renewals Type';
        T1.Recordtypeid = taskRecordType.id;
        T1.Rework_Types__c = 'Correction';
        T1.Task_Comments__c = 'Test1';
        T1.Description  += '\n'+ ' ' +Uname +':'+' '+ ModDate +':'+' ';
        T1.Status = 'Completed';
        T1.Status_Update_Date_Time__c = DateTime.now();
        T1.VCE_Assigned_To__c = 'Testing';
        T1.VCE_Last_Modifying_Partner__c = 'EMC';
        T1.Whatid = oppid;
        T1.Type='Email';
       // T1.whoid = led.id;
        insert T1;
        
        OAR_Member_Added__c obj1 = new OAR_Member_Added__c();
       // String Oppname = T1.WhatId;
        // obj1.Text_5__c = Oppname;
         insert obj1;
      
    
if(T1.Type__c != NULL)
 {                  
   T1.Type = T1.Type__c;
 }

T1.AddError('Please select a value for the field Type');

}

}