/**===
   25-May-2011      Srinivas Nallapati          Changed for June11 release
   15-Apr-2013      Prachi Bhasin               To improve coverage
   18-Jul-2013      Victor Galvan               To improve coverage of two Triggers (EBC_deleteBriefingEvent
                                                and EBC_addBriefingEvent) and a class (EBC_addCustomInvitees)
   23-Aug-2013      Victor Galvan               To improve coverage for trigger EBC_addInvitees                                             
   05-Jun-2014      Abinaya M S                 Assign 'Internal meeting' value to Briefing_Type__c field to satisfy validation rule.
 ===*/
@isTest(seeAllData=true)
private class EBC_addCustomInvitees_TC {

    static testMethod void EBC_addCustomInvitees()
    {
        //donotchange
            //This code is used here to get record type id
    Schema.DescribeSObjectResult myObjectSchema = Schema.SObjectType.Event;
    Map<String,Schema.RecordTypeInfo> rtMapByName = myObjectSchema.getRecordTypeInfosByName();
    Schema.RecordTypeInfo record_Type_name_RT = rtMapByName.get('Briefing Event');
    Id rTypeId= record_Type_name_RT.getRecordTypeId();
        
        
        //Prachi
         //List<User> lstUser = [Select id,Email from user where IsActive = true and Theater__c in ('APJ')limit 4];
         List<User> lstUser = [Select id,Email from User where Profile.Name='System Administrator' AND IsActive=true limit 1];
         Set<String> userEmailSet = new Set<string>();
         userEmailSet.add(lstUser[0].Email);
        EBC_Briefing_Event__c objBri = new EBC_Briefing_Event__c();
        objBri.Briefing_Name__c='briefing event';
        objBri.Briefing_Type__c = 'Internal meeting';
        insert objBri;
        EBC_Invitees__c objInv= new EBC_Invitees__c();
        objInv.Briefing_Team_Role__c='EMC Employee';
        objInv.Briefing_Event__c =objBri.id;
        insert objInv;
        EBC_addCustomInvitees objAdd = new EBC_addCustomInvitees();
        objAdd.createSchedulerInvitee(objBri.id,lstUser[0].Id);
        
  
        Contact con = new Contact();
        con.Email=(lstUser[0].Email)+'x';
        con.Active__c=true;
        con.LastName='abc';
        insert con;
        Set<Id> contactIdSet = new Set<Id>();
        contactIdSet.add(con.id);
                
        EBC_addCustomInvitees.createInvitees(objBri.id);
        objAdd.createSchedulerInvitee(objBri.id,lstUser[0].Id);
        
        Event eventToDelete = new Event();
        eventToDelete.DurationInMinutes = 30;
        eventToDelete.ActivityDateTime = system.now();
        eventToDelete.RecordTypeId = rTypeId;
        eventToDelete.OwnerId = '02370000000nVuJ'; //Hopkinton Calendar
        eventToDelete.WhatId = objBri.Id;
        eventToDelete.Briefing_Event__c = true;
        insert eventToDelete;
        eventToDelete.DurationInMinutes = 35;
        
        update eventToDelete;
        delete eventToDelete;
        
        //////////////
        
        EBC_Briefing_Event__c objBri2 = new EBC_Briefing_Event__c();
        objBri2.Briefing_Name__c='briefing event test';
        objBri2.Briefing_Type__c = 'Internal meeting';
        insert objBri2;
        
        
        Event e = new Event();
        e.DurationInMinutes = 30;
        e.ActivityDateTime = system.now();
        e.RecordTypeId = rTypeId;
        e.OwnerId = '02370000000nVuJ'; //Hopkinton Calendar
        e.WhatId = objBri2.Id;
        e.Briefing_Event__c = true;
        insert e;
        
        User invitee = [SELECT id, Name FROM User Where Profile.Name='EBC Power User' AND isActive=true limit 1];
        EventRelation er = new EventRelation();
        er.EventId = e.Id;
        er.RelationId = invitee.Id;
        insert er;
        
        EBC_addCustomInvitees.createInvitees(objBri2.Id);
        
        
        //
        
        
        //The next code is to increase the coverage % of the EBC_addBriefingTrigger because there is no a
        //special Test Class for this. 
        
        //We create a Briefing Event to fecth with the Event
        EBC_Briefing_Event__c briefingE = new EBC_Briefing_Event__c();
        briefingE.Name = 'Briefing Event for Testing addBriefingEvent trigger';
        briefingE.Briefing_Status__c = 'Cancel';
        briefingE.Briefing_Type__c = 'Internal meeting';
        briefingE.Objective_of_the_Visit_picklist__c = 'Test';
        briefingE.Objective_of_the_Visit__c = 'test';
        insert briefingE;
        
        //We create the Event and associate it with the past Briefing Event (briefingE)
        Event e2 = new Event();
        e2.DurationInMinutes = 10;
        e2.ActivityDateTime = system.now();
        e2.RecordTypeId = rTypeId;
        e2.OwnerId = '02370000000nVuJ'; //Hopkinton Calendar
        e2.WhatId = briefingE.Id;
        e2.Briefing_Event__c = true;
        insert e2;
        
        ////
        
        //We create anonther Briefing Event to cover the code of the Trigger (EBC_addBriefingTrigger)
        //where a Event does not have a Briefing Event related.
        EBC_Briefing_Event__c briefingE2 = new EBC_Briefing_Event__c();
        briefingE2.Name = 'Briefing Event for Testing addBriefingEvent trigger';
        briefingE2.Briefing_Status__c = 'Cancel';
        briefingE2.Briefing_Type__c = 'Internal meeting';
        briefingE2.Objective_of_the_Visit_picklist__c = 'Test';
        briefingE2.Objective_of_the_Visit__c = 'test';
        insert briefingE2;
        
        //Create an event but without a Briefing Event assigned or fetched.
        Event e3 = new Event();
        e3.DurationInMinutes = 10;
        e3.ActivityDateTime = system.now();
        e3.RecordTypeId = rTypeId;
        e3.OwnerId = '02370000000nVuJ'; //Hopkinton Calendar
        e3.WhatId = briefingE2.Id;
        e3.Briefing_Event__c = true;
        insert e3;
        
        
        //The next code breaks the governor limits. Thats why we use the command Test.startTest();
        Test.startTest();
                
        //We create a Briefing Event to cover the code of the Trigger (EBC_addBriefingTrigger)
        //where It is not necessary to create the Scheduler
        EBC_Briefing_Event__c briefingE3 = new EBC_Briefing_Event__c();
        briefingE3.Name = 'Briefing Event for Testing addBriefingEvent trigger';
        briefingE3.Briefing_Status__c = 'Cancel';
        briefingE3.Briefing_Type__c = 'Internal meeting';
        insert briefingE3;
        
        //Create the invitee that is going to be the Scheduler and assign it to the past
        //Briefing Event (briefingE3)
        EBC_Invitees__c inviteeForTest= new EBC_Invitees__c();
        inviteeForTest.Briefing_Event__c = briefingE3.Id;
        inviteeForTest.Briefing_Team_Role__c = 'Scheduler';
        insert inviteeForTest;
        
        //Then, we create the event and fetch it with the past Briefing Event (biefingE3)
        Event e4 = new Event();
        e4.DurationInMinutes = 10;
        e4.ActivityDateTime = system.now();
        e4.RecordTypeId = rTypeId;
        e4.OwnerId = '02370000000nVuJ'; //Hopkinton Calendar
        e4.WhatId = briefingE3.Id;
        e4.Briefing_Event__c = true;
        insert e4;
        
        EBC_addCustomInvitees.createInvitees = true;
        update e4;
        
        //Start WR#289707
        //We create a new Briefing Event to improve the coverage of the trigger EBC_addInvitees and cover the scenario
        //where the contact has a user but this does not have full license
        EBC_Briefing_Event__c briefingE4 = new EBC_Briefing_Event__c();
        briefingE4.Name = 'Briefing Event To Test EBC_addInvitees trigger';
        briefingE4.Briefing_Status__c = 'Cancel';
        briefingE4.Briefing_Type__c = 'Internal meeting';
        //Then, create a list of 10 contacts
        List<Contact> contactsRequestorLst = [SELECT Id, Name, Email FROM Contact WHERE Account.Name='EMC (Internal) Account' LIMIT 10];
        insert briefingE4;
        
        //Then, we search a contact that does not have a user to cover the scenario where the Requestor is a contact
        //that does not have a User.
        for(Contact c : contactsRequestorLst){
            List<User> userFoundLst =[Select Id,Profile.UserLicenseId from User where Email =:c.Email and IsActive=true];
            if(userFoundLst==null || userFoundLst.size()==0){
                //If contact does not have user, the Requestor will be the EBC Default User but we assign
                //the contact, the trigger EBC_addInvitee will assign the EBC Default User as Requestor
                    briefingE4.Requestor_Name__c = c.Id;
                    break;
            }
        }
        //Then update the Briefing Event
        update briefingE4;
        
        //Then, we searches a contact that has a User with Salesforce or Salesforce Platform to cover the scenario 
        //where the Requestor is a contact that has a user
        for(Contact c : contactsRequestorLst){
            List<User> userFoundLst =[Select Id,Profile.UserLicenseId,Email from User where Email =:c.Email and IsActive=true];
            if(userFoundLst.size()>0){
                //Get the license of the user found
                UserLicense userLic= [Select u.Name, u.Id From UserLicense u WHERE u.Id=:userFoundLst[0].Profile.UserLicenseId];
                    if(userLic.Name=='Salesforce' || userLic.Name=='Salesforce Platform'){
                        //If User has Salesforce or Salesforce Platform license, we proceed to assign it as Requestor
                        //and cover the code where the contact has a user and this has whether Salesforce license
                        //or Salesforce Platform.
                        //Validate that the user and contact have the same Email because the trigger EBC_addInvitees
                        //searches the user by the contact's email.
                        if(userFoundLst[0].Email==c.Email){
                            briefingE4.Requestor_Name__c = c.Id;
                            break;
                        }
                    }
            }
        }
        update briefingE4;
        //End WR#289707
        
        
        Event e5 = new Event();
        e5.DurationInMinutes = 10;
        e5.ActivityDateTime = system.now();
        e5.RecordTypeId = rTypeId;
        e5.OwnerId = '02370000000nVuJ'; //Hopkinton Calendar
        e5.WhatId = briefingE4.Id;
        e5.Briefing_Event__c = true;
        insert e5;
        
        User inviteeUser = [SELECT id, Name FROM User Where Profile.Name='EBC Power User' AND isActive=true limit 1];
        EventRelation er2 = new EventRelation();
        er2.EventId = e5.Id;
        er2.RelationId = inviteeUser.Id;
        insert er2;
        
        EBC_addCustomInvitees.createInvitees = true;
        update e5;
        
        //Update Briefing Event to improve coverage
        update briefingE4;
        
        Test.stopTest();
        
        
        

        
    }
}