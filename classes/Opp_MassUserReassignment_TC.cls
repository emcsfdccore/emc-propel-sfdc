/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE         DEVELOPER        WR             DESCRIPTION                               
 |  ====         =========        ==             =========== 
 |  06-Nov-2014  Vivek Barange   WR#1537         Added code to cover newly added code as a part of 1537
+===========================================================================*/
@isTest
Private class Opp_MassUserReassignment_TC{
    private static Account acct;
    private static list<id> oppid = new list<id>();
    private static  Map<Id,decimal>addUsrSpltPrct = new Map<Id,decimal>();
    private static List<Opportunity> list_updateOppty = new List<Opportunity>();
    private static Map<Id,decimal> map_replaceUserSpltPerct = new map<id,decimal>();
    private static  list<User> usrList = new list<User>();
    private static list<Opportunity> opptys = new list<Opportunity>();
    private static Set<Id> SetOwnerOppIds =new Set<Id>(); 


    Private static testMethod void reassignmentOperation(){   

        Util.isTestCoverage=true;

        Detail_Line__c detailLine = [select Forecast_Group__c,Owner__r.Id,Owner__r.Name,Opportunity__c,Opportunity__r.Id from Detail_Line__c where Forecast_Group__c = 'Channel' limit 1 ];
        system.debug('===>detailLine '+detailLine );
        oppid.add(detailLine.Opportunity__r.Id);
        SetOwnerOppIds.add(detailLine.Opportunity__r.Id);
        system.debug('===>oppid'+oppid);
        Opp_MassUserReassignment mR = new Opp_MassUserReassignment();
        mR.remove(oppid,detailLine.Owner__r.Id);
        usrList =[Select Id,IsActive,Name,Forecast_Group__c,Role__c from User where Forecast_Group__c='Direct' and IsActive=true limit 5];
        map_replaceUserSpltPerct.put(detailLine.Opportunity__r.Id,50);
        list_updateOppty = (mR.getOpportunties(oppid));
        // mR.addSalesTeamMember(usrList[0],list_updateOppty,'Edit',SetOwnerOppIds, map_replaceUserSpltPerct );
        Test.startTest();
        //Code Updated Lines: 30-36 as test class was failing during Sep Release Migration.  
        mR.replace(oppid,usrList[0].Id,usrList[0].Id);
        system.debug('===>usrList[0].Id'+usrList[0].Id+' '+usrList[1].Id);
        mR.replace(oppid,usrList[1].Id,usrList[1].Id);
        Test.stopTest();
        //mR.replace(oppid,usrList[1].Id,usrList[0].Id);
        // mR.addSalesTeamMember(usrList[2],list_updateOppty,'Edit',false, map_replaceUserSpltPerct );
        //mR.replace(oppid,usrList[1].Id,usrList[2].Id);





    }

    Private static testMethod void removeOperation(){   
        Util.isTestCoverage=true;
        Detail_Line__c detailLine = [select Forecast_Group__c,Owner__r.Id,Owner__r.Name,Opportunity__c,Opportunity__r.Id from Detail_Line__c where Forecast_Group__c = 'Direct' limit 1 ];
        system.debug('===>detailLine '+detailLine );
        oppid.add(detailLine.Opportunity__r.Id);
        system.debug('===>oppid'+oppid);
        Opp_MassUserReassignment mR = new Opp_MassUserReassignment();
        mR.remove(oppid,detailLine.Owner__r.Id);
        usrList =[Select Id,IsActive,Name,Forecast_Group__c,Role__c from User where Forecast_Group__c='Direct' and IsActive=true limit 5];
        map_replaceUserSpltPerct.put(detailLine.Opportunity__r.Id,50);
        list_updateOppty = (mR.getOpportunties(oppid));
        mR.addSalesTeamMember(usrList[0],list_updateOppty,'Edit',SetOwnerOppIds, map_replaceUserSpltPerct );
        Test.startTest();

        mR.remove(oppid,usrList[0].Id);
        mR.remove(oppid,usrList[1].Id);
        usrList =[Select Id,IsActive,Name,Forecast_Group__c,Role__c from User where Forecast_Group__c='Channel' and IsActive=true limit 5];


        // mR.addSalesTeamMember(usrList[0],list_updateOppty,'Edit',SetOwnerOppIds, map_replaceUserSpltPerct );
        // mR.remove(oppid,usrList[0].Id);
        
        //#1537 - Add below code to cover newly added code as a part of 1537
        //has Profile as System Administrator.
        Profile objProfile = [Select id,Name from Profile where name=:'System Administrator'];
        
        
        //Query Standard user which is active and has System Administrator Profile.     
        User objUser = new User(
             Username='test1234'+Math.random()+'@emc.com.test',
             TimeZoneSidKey='America/New_York',
             LocaleSidKey='en_US',
             FirstName='Direct',
             LastName='Rep',
             ProfileId = objProfile.id,
             email='john@emc.com',
             Alias='test',
             EmailEncodingKey='ISO-8859-1',
             LanguageLocaleKey='en_US',
             Forecast_Group__c='Direct',
             BU__c='NA',
             Employee_Number__c='9323782818',
             IsActive=true,
             Country__c='United States',
             CurrencyIsoCode='USD'
        );
        insert objUser;
        
        Account accServiceProvider = new Account(Name = 'Test Account Alliance'
                                ,RecordTypeId='012700000005IlnAAE'
                                ,Type='Partner'
                                ,CurrencyIsoCode='USD'
                                ,Status__c = 'A'
                                ,OwnerId = objUser.Id
                                ,BillingPostalCode='1000'
                                ,BillingCountry='United States'
                                ,Non_Distributor_Direct_Reseller_Partner__c=true
                                ,Partner_Type__c='Service Provider,Direct Reseller,Distribution VAR'
                                ,Reporting_Segmentation_Group__c='Global Alliances'
                                ,PROFILED_ACCOUNT_FLAG__c=true
                                ,Coverage_Model__c='A-Hunting'
                                ,Customer_Segment__c='Enterprise Tier 1'
                                ,Grouping__c='a0F70000004CnKu'
                                ,Master_Grouping__c='a0F70000004CoyI'
                                ,Lead_Oppty_Enabled__c = true,Child_Partner_Users_Count__c=2);
        insert accServiceProvider;
        
        List<Opportunity> lstOppty=new List<Opportunity>();  
        Opportunity Oppty = new Opportunity(Name = 'Test Opportunity '
                                ,AccountId=accServiceProvider.Id
                                ,Sales_Force__c='EMC'
                                ,CurrencyIsoCode='USD'
                                //,Partner__c = accServiceProvider.Id
                                //,Tier_2_Partner__c = acctToBeAdded.get(i).Id
                                //,Primary_Alliance_Partner__c=acctToBeAddedAliance.get(i).Id
                                //,Secondary_Alliance_Partner__c=acctToBeAddedAliance.get(i).Id
                                ,Service_Provider__c=accServiceProvider.Id
                                ,Opportunity_Owner__c=objUser.id
                                ,StageName ='Pipeline',CloseDate = System.today(),VCE_Related__c='VMWare Other',Amount = 500);
        lstOppty.add(Oppty);
        insert lstOppty;
        accServiceProvider.Partner_Type__c = 'Direct Reseller';
        update accServiceProvider;                  
        mR.byPassValdn(lstOppty, false);

    }
}