/*=================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE           DEVELOPER        WR           DESCRIPTION                               
 |  ====           =========        ==           =========== 
 |  11-JUNY-13      E. Cruz       268708         Initial Creation.
 |  04-June-14      Abinaya.MS    698            set Visiblity on section based on profile.
 |   
 +=================================================================================*/
 
 public with sharing class EBC_Request_Briefing_Event{
    //variable declaration
    private ApexPages.StandardController controller;
    
    public EBC_Briefing_Event__c NewBEvent {get;set;}
    public boolean showBriefingDiscussionSection{get;set;}
    //The constructor
    public EBC_Request_Briefing_Event(ApexPages.StandardController controller){
    //WR: 689 set Visiblity on section based on profile through customsetttings.
      String userProfileID = UserInfo.getProfileId();
      
      Map<String,EBC_Limited_Profile__c> ebcProfileID= EBC_Limited_Profile__c.getAll();
       if(ebcProfileID!=null){
            for(EBC_Limited_Profile__c ebcProfiles : ebcProfileID.values()){
                if(ebcProfiles.Profile_Id__c == userProfileID ){
                    showBriefingDiscussionSection = true;
                    break;
                }else{
                    showBriefingDiscussionSection = false;
                }
            }
        }
      this.controller = controller;
      this.NewBEvent = (EBC_Briefing_Event__c)controller.getRecord();
      this.NewBEvent.Name = ApexPages.currentPage().getParameters().get('acc');
      this.NewBEvent.Customer_Name__c = ApexPages.currentPage().getParameters().get('aid');
    }
    
    public PageReference save(){
      try{ 
        insert NewBEvent;
      }
      catch(System.DMLException e){ 
        ApexPages.addMessages(e);
        return NULL;
      }
      PageReference page = new PageReference('/' + NewBEvent.Id);
      page.setRedirect(true);
      return page;
    }
    
    public PageReference saveAndNew(){
      try{
        insert NewBEvent;
      }
      catch(System.DMLException e){
        ApexPages.addMessages(e);
        return NULL;
      }
      PageReference page = new PageReference(ApexPages.currentPage().getUrl());
      page.setRedirect(true);
      return page;
    }

    public PageReference cancel(){
      PageReference page = new PageReference('/' + ApexPages.currentPage().getParameters().get('aid'));
      page.setRedirect(true);
      return page;
    }
}