@isTest(SeeAllData = true)
private Class R2R_Integration_Management_TC{

    //Added for Fixing party number issue

    public static String generateRandomString(Integer len) {
    final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    String randStr = '';
    while (randStr.length() < len) {
       Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
       randStr += chars.substring(idx, idx+1);
    }
    return randStr; 
}
    


    static testMethod void integrationManagement()
    {
    Test.startTest();
    User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser)
        {
          PRM_VPP_JobDataHelper.createVPPCustomSettingData();   
        }
    
    map<string,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getall();
    List<Id> idlist= new List<Id>();
    Id uid=DataValueMap.get('IntegrationUser').DataValue__c;
    idList.add(uid);
    User u1=[Select id,Name,IsActive from User where IsActive=true and id in :idList limit 1]; 
     
    System.runAs(u1){
    
    Opportunity opp=testclassUtils.getOppty ();
    insert opp; 
                         
    Account acc=testclassUtils.getAccount () ;
    acc.Party_Number__c= R2R_Integration_Management_TC.generateRandomString(15);
    insert acc;

    Asset__c asset= new Asset__c();
    asset.Name='Testasset1';
    asset.Customer_Name__c=acc.id;
    asset.Red_Zone_Priority__c='null';    
    asset.Deffered_to_Renewals__c= false;
    asset.Party_Number__c= '9876';
    
    insert asset; 
    
    //Account acc2=testclassUtils.getAccount () ;
   // acc2.Party_Number__c= '5678';
    //insert acc2;
    
    
    Asset__c asset2= new Asset__c();
    asset2.Name='Testasset2';
    asset2.Customer_Name__c=acc.id;
    asset2.Red_Zone_Priority__c='null';    
    asset2.Deffered_to_Renewals__c= false;
    asset2.Party_Number__c= '987654';
    insert asset2; 
    
    Asset__c asset3= new Asset__c();
    asset3.Name='Testasset3';
    asset3.Customer_Name__c=acc.id;
    asset3.Red_Zone_Priority__c='null';    
    asset3.Deffered_to_Renewals__c= false;
    asset3.Party_Number__c= '98765';   
    
    insert asset3;
    }
         
   
    
    Test.stopTest();
    }
}