//***********************************************************************************
// Name             : Trasnformation_RecallBatch.cls
// Description      : Class is to Recall Pricing requetes from the approval process
// Created By       : 
// Created Date     : 
// ************************Version Updates*******************************************
// version 1.1      : Changes made to refactor class into another scheduler class 
//                  : in order to run the batch every hour
//                  : By - Jaypal Nimesh (EMC) Dated - 07/05/2013
//
// **********************************************************************************

global class Trasnformation_RecallBatch implements Database.Batchable<sObject> { 
  
    global String query;
    global List<Pricing_Requests__c> lstPAR = new List<Pricing_Requests__c>();
    global List<String> lstComments = new List<String>();

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query); 
    }
        
    global void execute(Database.BatchableContext BC, SObject[] scope){ 
        recall(scope); 
    }

    global void recall(sObject[] pWRId){ 
        
        for(SObject sobj : pWRId){
            
            Pricing_Requests__c cWObj = (Pricing_Requests__c)sobj;
            lstPAR.add(cWObj);
            lstComments.add(cWObj.Recall_Comments__c);
        }
        
        List<ProcessInstanceWorkitem> workitem = new List<ProcessInstanceWorkitem>([select Id, ProcessInstance.TargetObjectId from ProcessInstanceWorkitem where ProcessInstance.TargetObjectId IN: lstPAR AND ProcessInstance.status= 'Pending']);
        List<Approval.ProcessWorkItemRequest> lstPwr = new List<Approval.ProcessWorkItemRequest>();

        if(workitem.size() > 0){
            
            for(Integer i=0; i < workitem.size() ; i++){
                
                Approval.ProcessWorkItemRequest pwr = new Approval.ProcessWorkItemRequest();
                pwr.setWorkitemId(workitem[i].id);
                pwr.setComments(lstComments[i]);
                pwr.setAction('Removed'); 
                lstPwr.add(pwr);                 
           }
           
           List<Approval.ProcessResult> pr = Approval.process(lstPwr);  
        }    
    }
    
    global void finish(Database.BatchableContext BC){}             
}