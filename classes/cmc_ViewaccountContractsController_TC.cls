/* 
Author - Vinod
CreatedDate - 11-Aug-2014
Description - Test class to test the "cmc_ViewaccountContractsController" class 
*/

@isTest
private class cmc_ViewaccountContractsController_TC{
     
    public static testmethod void webServiceCallout1Test(){
    //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        } 
       Account acc= new Account(Name='Testaccount',Party_Number__c='123456');
        insert acc;
       test.startTest();
        Test.setMock(WebServiceMock.class, new cmc_WebServiceMockImpl());
        ApexPages.StandardController stdController= new ApexPages.StandardController(acc);
        cmc_ViewaccountContractsController accController= new cmc_ViewaccountContractsController(stdController);    
        accController.PdfView();
        
        accController.firs();
        accController.nex();
        accController.prev();
        accController.las();
     
        accController.contractID = '3245178';
        accController.DetailedView();
        test.stopTest();       
       }
}