/* ctrlCommunitiesCasesListViewclass 
* Created by:  Matthew Roark
* Created Date: 09/15/2014
* Last Updated by:  Haytham Lotfy
* Last Update Date:  10/20/2014
*
* Description:  This class acts as the controller for the 'CommunitiesCasesListView' Visualforce page
*  
* Change log:
*
* 09/15/2014 - MR - Class created.
* 09/17/2014 - MR - Updated class to change the 'My Cases' filter to only include Cases which have the Contact for the current User assigned to them.
* 10/05/2014 - HL - Updated class to add Language cookie controller
* 20/10/2014 - HL - Updated class to add search functionality
*/
public with sharing class ctrlCommunitiesCasesListView {
public String languageVar { get; set; }
public String needsRefresh {get; set; }
public String getCurrentUserProfile() {
    String currentUserProfile;
    List<Profile> userprofile = [select name from Profile where id = :system.Userinfo.getProfileId() ];
    currentUserProfile = userprofile[0].Name;
    return currentUserProfile;
}
public Integer totalPages { get; set; }
public list<String> AlphaList {get; set;}
public String myAlphaFilter {get; set;}
//uncomment the below line for debugging
//public String DebugQuery {get; set; }  
public String SearchString {get; set;}
private String sortDirection = 'DESC';
private String sortExp = 'CreatedDate';

public List<SelectOption> getViewOptions()
{
    List<SelectOption> options = new List<SelectOption>();
    if (getCurrentUserProfile() == 'Community Power User' || getCurrentUserProfile() == 'Community User' ){
        options.add(new SelectOption('1My External','My Cases'));
        options.add(new SelectOption('2My Open External','My Open Cases'));
    }
    else {
        options.add(new SelectOption('1My Internal','My Cases'));
        options.add(new SelectOption('2My Open Internal','My Open Cases'));        
    }
    if (getCurrentUserProfile() == 'GBS Collaborator Profile' || getCurrentUserProfile() == 'System Administrator'){
        options.add(new SelectOption('6GRO','Manufacturing Cases'));
    }
    if (getCurrentUserProfile() == 'Community Power User' || getCurrentUserProfile() == 'System Administrator') {
        options.add(new SelectOption('3All','My Company\'s Cases'));
        options.add(new SelectOption('4All Open','My Company\'s Open Cases'));
    }
    if (getCurrentUserProfile() == 'Community Power User' || getCurrentUserProfile() == 'System Administrator' || getCurrentUserProfile() == 'EMC Employee Profile') {         
        options.add(new SelectOption('5Resolved Last 48 Hours','Resolved Last 48 Hours'));
    }
    return options; 
}
public String selectedViewOption
{
    get;set;
}
private String AlphaS = 'All';
public String AlphaFilter
{
    get
    {
        return AlphaS;
    }
    set
    {
        //if the column is clicked on then switch between Ascending and Descending modes
        AlphaS = value;
    }
}
public String sortExpression
{
    get
    {
        return sortExp;
    }
    set
    {
        //if the column is clicked on then switch between Ascending and Descending modes
        if (value == sortExp)
            sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
        else
            sortDirection = 'ASC';
        sortExp = value;
    }
}
public String getSortDirection()
{
    //if not column is selected 
    if (sortExpression == null || sortExpression == '')
        return 'ASC';
    else
        return sortDirection;
}
public void setSortDirection(String value)
{  
    sortDirection = value;
}
private User currentUser
{
    get
    {
        if (currentUser == null)
        {
            try
            {
                currentUser = [Select Id, ContactId from User where Id = :UserInfo.getUserId() limit 1];
            }
            catch (DmlException dex)
            {
                System.debug('Unable to fetch current user.  Error: '+dex.getMessage());
            }
        }   
        return currentUser;
    }
    private set;        
}
public ctrlCommunitiesCasesListView() {
    AlphaList = new list<String> {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Other', 'All'};
        if (getCurrentUserProfile() == 'Community Power User' || getCurrentUserProfile() == 'Community User' ){
            selectedViewOption = '2My Open External';
        }
    else {
        selectedViewOption = '2My Open Internal';
    }
}
public List<Case> lstCases
{
    get 
    {
        if (lstCases == null)
        {
            lstCases = (List<Case>) setCon.getRecords();
        }
        return lstCases;
    }
    private set;
}
public Boolean hasCases
{
    get { return (lstCases.size() > 0);}        
}
/*    public PageReference redirectToNewCase()
{
    return Page.CommunitiesHomePage;        
}*/

public ApexPages.StandardSetController setCon {
    
    get {
        Decimal dtotalPages;
        if(setCon == null || needsRefresh == 'Y') {              
            try
            {         
                if ((currentUser != null )) {
                    
                    String LiveSearchString = Apexpages.currentPage().getParameters().get('mySearchString');
                    string sortFullExp = sortExpression  + ' ' + sortDirection;
                    string WhereCondition = '';
                    string GROColumns = '';
                    if (getCurrentUserProfile() == 'Community Power User' || getCurrentUserProfile() == 'Community User'){
                        WhereCondition = WhereCondition + ' and RecordType.Name in (\'Accounts Payable\',\'Credit & Collections\',\'Global Revenue Operations\') ';
                    }
                    if (selectedViewOption.contains('My')) {
                        If (selectedViewOption.contains('External')) {
                            WhereCondition = WhereCondition + ' and ContactId = \''+currentUser.ContactId+'\' ';
                        }
                        else {
                            WhereCondition = WhereCondition + ' and Id in (select ParentId from CaseTeamMember where MemberId = \''+UserInfo.getUserId()+'\')  ';
                        }
                    }
                    if (LiveSearchString != '' && LiveSearchString != null ) {
                        WhereCondition = WhereCondition + ' and (subject LIKE \'%'+String.escapeSingleQuotes(LiveSearchString)+'%\'  or Type LIKE \'%'+String.escapeSingleQuotes(LiveSearchString)+'%\' or CaseNumber LIKE \'%'+String.escapeSingleQuotes(LiveSearchString)+'%\' or Purchase_Order_Number__c like \'%'+String.escapeSingleQuotes(LiveSearchString)+'%\' or Invoice_Number__c like \'%'+String.escapeSingleQuotes(LiveSearchString)+'%\' or Quote_Number__c like \'%'+String.escapeSingleQuotes(LiveSearchString)+'%\' or Purchase_Order_Number_s__c like \'%'+String.escapeSingleQuotes(LiveSearchString)+'%\' or Fill_Up_Order_Number__c like \'%'+String.escapeSingleQuotes(LiveSearchString)+'%\' or     SAP_Sales_Order__c like \'%'+String.escapeSingleQuotes(LiveSearchString)+'%\'  )  ';
                    }
                    if (selectedViewOption.contains('Open')) {
                        WhereCondition = WhereCondition  + ' and IsClosed = false ';
                    }
                    if (selectedViewOption == '5Resolved Last 48 Hours') {
                        WhereCondition = WhereCondition  + ' and (Status = \'Resolved\' and ClosedDate = LAST_N_DAYS:2) ';
                    }
                    if (selectedViewOption == '6GRO'){
                        WhereCondition = WhereCondition + ' and CreatedDate = LAST_N_DAYS:90 and ((RecordType.Name = \'Global Revenue Operations\' and Type in (\'GRO Order Inquiries\',\'Field Inventory Return Creation\',\'Field Inventory Return Inquiry\') ) or (RecordType.Name = \'Credit & Collections\' and Credit_Collection_Sub_Type__c = \'Customer/Order Release\')) ';
                        GROColumns = ' ,SAP_Sales_Order__c ';
                    }
                    if (AlphaFilter == 'Other') {
                        WhereCondition = WhereCondition + ' and (' + String.escapeSingleQuotes(sortExp) + ' < \'A\' OR ' + String.escapeSingleQuotes(sortExp) + ' > \'Z\') AND (NOT ' +  String.escapeSingleQuotes(sortExp) + ' LIKE \'Z%\') ';
                    } 
                    if (AlphaFilter != 'All' && AlphaFilter  != 'Other' && AlphaFilter != null) {
                        WhereCondition = WhereCondition + ' and (' + String.escapeSingleQuotes(sortExp) + ' LIKE \'' + String.escapeSingleQuotes(AlphaFilter) + '%\')' ;
                    }                        
                    String FinalQueryString = 'Select Id, CaseNumber, Status, CreatedDate, ClosedDate, RecordType.Name, Type, Subject,Priority,CreatedBy.Name,Contact_Name_Email__c' +GROColumns+ ' from Case where Id != null '+WhereCondition+' order by '+sortFullExp + ' Limit 10000';
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(FinalQueryString));
                    //uncomment the below for debugging
                    
                    //DebugQuery = FinalQueryString;
system.debug(FinalQueryString);
                }
            }
            catch (DmlException dex)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL , 'Error in Query, Please Contact Admins with the following error: ' + dex);
                ApexPages.addMessage(myMsg);
            }  
        }
        setCon.setPageSize(25);
        dtotalPages= (setCon.getResultSize() / setCon.getPageSize());
        dtotalPages = Math.floor(dtotalPages) + ((Math.mod(setCon.getResultSize(), 25)>0) ? 1 : 0);
        totalPages = Integer.ValueOf(dtotalPages);
        needsRefresh = 'N';
        return setCon;
    }
    set;
}
public List<Case> getSSCCases() {
    return (List<Case>) setCon.getRecords();
}
public PageReference runSearch() {
    needsRefresh = 'Y';
    lstCases = (List<Case>) setCon.getRecords();
    return null;
}
public List<Case> refreshCases()
{
    needsRefresh = 'Y';
    lstCases =  (List<Case>) setCon.getRecords();
    return null;
}
}