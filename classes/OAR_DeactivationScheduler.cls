/*========================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE           DEVELOPER                WR                DESCRIPTION                               
 |  ====           =========                ==                =========== 
    18/08/2013      Sneha Jain          WR 261820       Scheduler class to deactivate the OAR records for which the Resource Name user 
                                                        has been deactivated
 +======================================================================================================================================*/

global  class OAR_DeactivationScheduler implements Schedulable
{
    global void execute(SchedulableContext SC)
    {
        //Abort the existing schedule 
        CronTrigger ct = [SELECT id,CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :SC.getTriggerId()];
        if(ct != null) 
        {
            System.abortJob(ct.Id); 
        }
        updateOARIfUserIsInactive  deactivateOAR = new updateOARIfUserIsInactive();
        database.executebatch(deactivateOAR,200);
    }
}