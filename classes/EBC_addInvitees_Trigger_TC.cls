/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE       DEVELOPER       WR       DESCRIPTION                                
|
   ====       =========       ==       ===========  
|  22-Apr-2013  Prachi                 Created 
|  05-Jun-2014  Abinaya M S            Assigned 'Internal meeting' value to Briefing_Type__c field to satisfy validation rule.
+===========================================================================*/ 
@isTest(SeeAllData=true)
private Class EBC_addInvitees_Trigger_TC{

    public static testmethod void EBC_addInvitees_Trigger(){

        EBC_Briefing_Center__c objCen= new EBC_Briefing_Center__c();
        objCen.Name='Center';
        objCen.Country__c='Country';
        insert objCen;

        EBC_Briefing_Center__c objCen1= new EBC_Briefing_Center__c();
        objCen1.Name='Center1';
        objCen1.Country__c='Country1';
        insert objCen1;
        
        EBC_Briefing_Venue__c objVen= new EBC_Briefing_Venue__c();
        objVen.Name='Venue';
        objVen.Briefing_Center__c=objCen.id;
        insert objVen;

        EBC_Briefing_Venue__c objVen1= new EBC_Briefing_Venue__c();
        objVen1.Name='Venue';
        objVen1.Briefing_Center__c=objCen1.id;
        insert objVen1;

        Account acc= new Account();
        acc.Name='Account';
        insert acc;

        Contact Con = new Contact();     
        Con.Salutation = 'Mr';
        Con.FirstName = 'Jayaraju';
        Con.LastName = 'Nulakachandanam';
        Con.AccountId = acc.id;
        Con.Email='acha@b.com';
        insert Con;

        Contact Con1 = new Contact();     
        Con1.Salutation = 'Mr';
        Con1.FirstName = 'Jaya';
        Con1.LastName = 'Raman';
        Con1.AccountId = acc.id;
        Con1.Email='p@b.com';
        insert Con1;

        EBC_Room__c objRoom= new EBC_Room__c();
        objRoom.Briefing_Venue__c=objVen.id;
        objRoom.Reservation_Contact__c=Con.id;
        insert objRoom;

        EBC_Room__c objRoom1= new EBC_Room__c();
        objRoom1.Briefing_Venue__c=objVen1.id;
        objRoom1.Reservation_Contact__c=Con1.id;
        insert objRoom1;

        EBC_Briefing_Event__c objEve= new EBC_Briefing_Event__c();
        objEve.Name='Event';
        objEve.Briefing_Venue__c=objVen.id;
        objEve.Briefing_Center__c=objCen.id;
        objEve.Briefing_Status__c='Open';
        objEve.Requestor_Name__c=Con.id;
        objEve.Briefing_Type__c = 'Internal meeting';
        objEve.Room_Assignment__c=objRoom.id;
        insert objEve; 
        
        Event eve = new Event();
        eve.DurationInMinutes= 20;
        eve.ActivityDateTime=System.today()+1;
        insert eve;
        
        objEve.Briefing_Venue__c=objVen1.id;
        objEve.Briefing_Center__c=objCen1.id;
        objEve.Briefing_Status__c='Cancel';
        objEve.Requestor_Name__c=Con1.id;
        objEve.Standard_Event_Id__c=eve.id;
        objEve.Room_Assignment__c=objRoom1.id;
        update objEve;
        }  
    }