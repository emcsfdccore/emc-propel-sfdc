/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE               DEVELOPER         WR/Req                         DESCRIPTION                               
 |  ====               =========         ======                         =========== 
 |  13.03.2015      Harshad Bharsakle    DM PVR        Inline VisualForce Controller to populate values in Case Detail Page.
 |  24.03.2015      Harshad Bharsakle    DM PVR        Changed code for updation of any record value on case object 
+=========================================================================================================================*/



public with sharing class PVR_Expiries_record {
    Public List<PVR_Expiry__c> PVRList{get;set;}
    public PVR_Expiries_record(ApexPages.StandardController controller) 
    {
      PVRList = [SELECT Name,Core_Quota_rep__c,Case__c,Concession_End_Date__c,Contact_Name__c,Global_Ultimate_Identifier__c,Global_Multinational_Account__c,Internal_Comments__c,Notice_90_Day__c,Notice_45_Day__c,Notice_15_Day__c,Opportunity_Number__c,Opportunity_Owner__c,Opportunity_Name__c,PVR_Disposition__c FROM PVR_Expiry__c WHERE Case__r.Id = :Apexpages.CurrentPage().getParameters().get('id')];
    }
    
    Public Void PVRSave()
    {
      Update PVRList;
    }

}