/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  27.10.2012      Smitha Thomas       MOJO        Initial Creation.Creating this test class to unit test 
                                                SFA_MOJO_CreateOpptyController class.
 |  27.11.2013      Sneha Jain          R2R     Modified the whole class to optimize it and make it according to coding standards    
 |  02.04.2014      Sneha Jain          R2R     Modified the class to increase the code coverage 
 |  12/18/2014      Bindu Sri         CI 1559   Replaced Closed reason values
+========================================================================================================================*/ 
@isTest
private class SFA_MOJO_CreateOpptyController_TC 
{
     static testMethod void createOpptyTest()
    {   
        
        //Custom Setting data creation
        System.runAs(new User(id= UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        } 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, Forecast_Group__c = 'Maintenance Renewals', 
            TimeZoneSidKey='America/Los_Angeles', UserName='stduserr2rtest@testorg.com');
        
        //Account creation
        List<Account> lstAccts = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        insert lstAccts;
        
        //Account record creation
        List<Account> lstAccount = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        lstAccount[0].Account_Flag__c = 'Primary';
        lstAccount[0].Customer_Profiled_Account__c = true;
        lstAccount[1].Customer_Profiled_Account_Lookup__c = lstAccts[0].Id;
        insert lstAccount;
        
                
        //Opportunity records creation
        List<Opportunity> lstOppty = new List<Opportunity>();
        lstOppty.add(new Opportunity (Name = 'Test Closed Oppty ', AccountId = lstAccount[0].id, CurrencyIsoCode = 'USD', StageName = 'Upside',CloseDate = System.today(), Amount = 500, Closed_Reason__c = 'test Closed', Close_Comments__c = 'test closed'));
        lstOppty.add(new Opportunity (Name = 'Test Active Oppty ', AccountId = lstAccount[1].id, CurrencyIsoCode = 'USD', StageName = 'Pipeline',CloseDate = System.today(), Amount = 1000));
        //CI 1559 replaced competition with Lost to competition
        lstOppty.add(new Opportunity (Name = 'Test Closed Oppty2 ', AccountId = lstAccount[2].id, CurrencyIsoCode = 'USD', StageName = 'Closed',CloseDate = System.today(), Amount = 2000,Close_Comments__c = 'Test', Closed_Reason__c='Lost to Competition'));
        lstOppty.add(new Opportunity (Name = 'Test Active Oppty2 ', AccountId = lstAccount[3].id, CurrencyIsoCode = 'USD', StageName = 'Pipeline',CloseDate = System.today()+5, Amount = 3000,Opportunity_Type__c='Renewals'));
        insert lstOppty;
        
        lstOppty[3].Opportunity_Type__c='Renewals';
        update lstOppty[3];
        
        //Creating Lead
        List<Lead> leadList = new List<Lead>();
        leadList.add(new Lead(Company = 'test Comp1', LastName = 'Test Lead1', Related_Account__c = lstAccount[0].id, Status='Swap',Lead_Type_Based_on_Linking__c='Renewals'));
        leadList.add(new Lead(Company = 'test Comp2', LastName = 'Test Lead2', Related_Account__c = lstAccount[1].id, Status='Swap',Lead_Type_Based_on_Linking__c='Refresh'));
        leadList.add(new Lead(Company = 'test Comp3', LastName = 'Test Lead3', Related_Account__c = lstAccount[2].id, Status='Swap',Lead_Type_Based_on_Linking__c='Refresh'));
        insert leadList;
        
        leadList[0].Lead_Type_Based_on_Linking__c='Renewals';
        update leadList[0];
        
        RecordType recType = [Select Id, DeveloperName from RecordType where DeveloperName = 'Competitive_Install' limit 1];
        
        String emcInstallRecordType = '';
        String competitiveRecordType = '';
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where RecordType.DeveloperName like 'EMC_Install' or Name Like 'Competitive_Install'];
        if(recordType[0].DeveloperName == 'EMC_Install')
        {
            emcInstallRecordType = recordType[0].id;
            competitiveRecordType = recordType[1].id;
        }
        if(recordType[0].DeveloperName == 'Competitive_Install')
        {
            competitiveRecordType = recordType[0].id;
            emcInstallRecordType = recordType[1].id;
        }
        //Asset record creation
        List<Asset__c> lstAssets = new List<Asset__c>();
        //Competitive assets
        lstAssets.add(new Asset__c(name = 'Test 0', Customer_Name__c = lstAccount[0].id,RecordTypeId = competitiveRecordType));
        //EMC Install assets
        lstAssets.add(new Asset__c(name = 'Test 1', Customer_Name__c = lstAccount[0].id,RecordTypeId = emcInstallRecordType));
        lstAssets.add(new Asset__c(name = 'Test 2', Customer_Name__c = lstAccount[1].id,RecordTypeId = emcInstallRecordType));
        lstAssets.add(new Asset__c(name = 'Test 3', Customer_Name__c = lstAccount[1].id,RecordTypeId = emcInstallRecordType));
        lstAssets.add(new Asset__c(name = 'Test 4', Customer_Name__c = lstAccount[1].id,RecordTypeId = emcInstallRecordType));
        lstAssets.add(new Asset__c(name = 'Test 5', Customer_Name__c = lstAccount[1].id,RecordTypeId = emcInstallRecordType));
        lstAssets.add(new Asset__c(name = 'Test 6', Customer_Name__c = lstAccount[1].id,RecordTypeId = emcInstallRecordType));
        
        Test.startTest();
        
            insert lstAssets;
             
            SFA_MOJO_CreateOpptyController obj0 = new SFA_MOJO_CreateOpptyController();
            obj0.CreateOpty();
            
            //Opportunity Asset Junction record creation
            List<Opportunity_Asset_Junction__c> oajList = new List<Opportunity_Asset_Junction__c>();
            oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = lstAssets[0].id, Related_Opportunity__c = lstOppty[0].id));
            oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = lstAssets[1].id, Related_Opportunity__c = lstOppty[1].id));
            oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = lstAssets[4].id, Related_Opportunity__c = lstOppty[2].id));
            oajList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = lstAssets[6].id, Related_Opportunity__c = lstOppty[3].id));
            insert oajList;
            
            //Lead Asset Junction record creation
            List<Lead_Asset_Junction__c> lajList = new List<Lead_Asset_Junction__c>();
            lajList.add(new Lead_Asset_Junction__c(Related_Asset__c = lstAssets[2].Id,Related_Lead__c = leadList[0].Id));
            lajList.add(new Lead_Asset_Junction__c(Related_Asset__c = lstAssets[3].Id,Related_Lead__c = leadList[1].Id));
            lajList.add(new Lead_Asset_Junction__c(Related_Asset__c = lstAssets[0].Id,Related_Lead__c = leadList[2].Id));
            insert lajList;
            
            System.runAs(u)
            {   
                ApexPages.currentpage().getParameters().put('astId',lstAssets[0].Id);            
                ApexPages.StandardSetController setCon1 = new ApexPages.StandardSetController(lstAssets);               
                setCon1.setSelected((List<SObject>)lstAssets);                    
                SFA_MOJO_CreateOpptyController obj1 = new SFA_MOJO_CreateOpptyController(setCon1);
                obj1.CreateOpty();
            }
            
            ApexPages.currentpage().getParameters().put('astId',lstAssets[3].Id);            
            ApexPages.StandardSetController setCon2 = new ApexPages.StandardSetController(lstAssets);               
            setCon2.setSelected((List<SObject>)lstAssets);                    
            SFA_MOJO_CreateOpptyController obj2 = new SFA_MOJO_CreateOpptyController(setCon2);
            obj2.CreateOpty();
            
            ApexPages.currentpage().getParameters().put('astId',lstAssets[2].Id);            
            ApexPages.StandardSetController setCon3 = new ApexPages.StandardSetController(lstAssets);               
            setCon3.setSelected((List<SObject>)lstAssets);                    
            SFA_MOJO_CreateOpptyController obj3 = new SFA_MOJO_CreateOpptyController(setCon3);
            obj3.CreateOpty();
            
            ApexPages.currentpage().getParameters().put('astId',lstAssets[1].Id);            
            ApexPages.StandardSetController setCon4 = new ApexPages.StandardSetController(lstAssets);               
            setCon4.setSelected((List<SObject>)lstAssets);                    
            SFA_MOJO_CreateOpptyController obj4 = new SFA_MOJO_CreateOpptyController(setCon4);
            obj4.CreateOpty();
            
            String assetString = '';
            assetString = lstAssets[0].Id + ',' + lstAssets[1].Id + ',' + lstAssets[2].Id + ',' + lstAssets[3].Id +  ',' + lstAssets[4].Id;
            ApexPages.currentpage().getParameters().put('fcf',assetString);            
            ApexPages.StandardSetController setCon5 = new ApexPages.StandardSetController(lstAssets);               
            setCon5.setSelected((List<SObject>)lstAssets);                    
            SFA_MOJO_CreateOpptyController obj5 = new SFA_MOJO_CreateOpptyController(setCon5);      
            obj5.CreateOpty();
            
            ApexPages.currentpage().getParameters().put('astId',lstAssets[4].Id);            
            ApexPages.StandardSetController setCon6 = new ApexPages.StandardSetController(lstAssets);               
            setCon6.setSelected((List<SObject>)lstAssets);                    
            SFA_MOJO_CreateOpptyController obj6 = new SFA_MOJO_CreateOpptyController(setCon6);
            obj6.CreateOpty();
            
            System.runAs(u)
            {
                ApexPages.currentpage().getParameters().put('astId',lstAssets[6].Id);            
                ApexPages.StandardSetController setCon8 = new ApexPages.StandardSetController(lstAssets);               
                setCon8.setSelected((List<SObject>)lstAssets);                    
                SFA_MOJO_CreateOpptyController obj8 = new SFA_MOJO_CreateOpptyController(setCon8);
                obj8.CreateOpty();
            }
            
            ApexPages.currentpage().getParameters().put('astId',lstAssets[0].Id);            
            ApexPages.StandardSetController setCon7 = new ApexPages.StandardSetController(lstAssets);               
            setCon7.setSelected((List<SObject>)lstAssets);                    
            SFA_MOJO_CreateOpptyController obj7 = new SFA_MOJO_CreateOpptyController(setCon7);
            obj7.intNoRec = 1;
            obj7.CreateOpty();
            obj7.intNoRec = 0; 
            obj7.intErrored = 1;
            obj7.intActiveOpty = 0;
            obj7.intCompetitiveAsset =0 ;
            obj7.intCompetitiveAssetLinked =0 ;
            obj7.intLinkToRenewalsLead =0 ;
            obj7.intLinkToRefreshLead =0 ;
            obj7.intLinkToCompetitiveLead =0 ;
            obj7.intDiffAccount = 1;
            obj7.accId= lstAccount[0].id;
            obj7.accLkid = lstAccount[0].id;
            obj7.dataValueMap = 'test';
            obj7.CreateOpty();
        test.stopTest();
            
    }
}