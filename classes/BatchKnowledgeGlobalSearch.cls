/*==================================================================================================================+                                                                          
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        ===========  
 |  Dec/17/2014   Bisna V P    CI 1127     Updated to solve -Find and Replace tool discrepancy 
 |  Jan/15/2015   Bisna V P    CI 1631     Updated Find and Replace tool UI and Email Notifications enhancements
 ====================================================================================================================*/

global class BatchKnowledgeGlobalSearch implements Database.Batchable<sObject>, Database.Stateful {
    
        
    Private String strApexSearchJobId = ''; 
    Private String strApexReplacementJobId = '';     
    Private Boolean bSearchOnly = false;
    Private Boolean bExecuteReplacement = false;
    Private List<String> listSelectedArticles;
    Private integer nMaxSizePlainTextBody = 16384; // Arbitrary - Have no idea how large the plain text body can be 
    Public String strArticleType;
    Public String strPublishStatus;    
    public String strSearchFields = ''; 
    public List<String> listSearchFields; 
    public String strSearchString = '';
    public String strReplacementString = '';
    public Integer UpdateCount = 0;
    public Integer TotalCount = 0;
    Private Boolean publish = false;
    
    global BatchKnowledgeGlobalSearch(
        Boolean bSearchOnly, 
        String strArticleType, String strPublishStatus,
        String strSearchFields, String strSearchString, String strReplacementString, Boolean publish) {
        
        //Future use: Date dateLastModifiedFrom, Date dateLastModifiedTo) {
        
        this.bSearchOnly = bSearchOnly;
        this.strArticleType = strArticleType;
        this.strPublishStatus = strPublishStatus;
        this.strSearchFields = strSearchFields;    
        this.listSearchFields = strSearchFields.split(',');
        this.strSearchString = strSearchString;
        this.strReplacementString = strReplacementString;       
        this.listSelectedArticles = new List<String>();
        this.TotalCount = 0;
        this.UpdateCount = 0;  
        this.publish = publish;     
    } 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        this.strApexSearchJobId = BC.getJobId();
        
        // Return record Ids for all articles that meet high-level publish status and 
        // language constraints.  Note that internal and custom field values
        // are not returned here but, rather, in the execute method.  This minimizes
        // the physical size of the query locator and avoids governor constraints.
        String strQuery = '';
        strQuery += 'SELECT Id FROM '+this.strArticleType;
        strQuery += ' WHERE PublishStatus = \''+this.strPublishStatus+'\'';
        strQuery += ' AND Language = \'en_US\'';
        
        return database.getquerylocator(strQuery);      
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        // Record Ids are concatenated to allow all articles
        // for this batch scope to be returned in a single SOQL query
        String strScopeIds = '';
        for (sObject scopeObject : scope) {
            if (strScopeIds.length()>0) strScopeIds += ',';
            strScopeIds += '\''+(String)scopeObject.Id+'\'';
        }        
        
        // Base SOQL construct
        String strQuery = '';
        
        // Return all records for this batch scope in a single SOQL query, including internal
        // and custom field values for each article
        strQuery += 'SELECT Id, KnowledgeArticleId, ArticleNumber';
        // Include Title if not explicitly selected as a search field for email notification (log) purposes
        if (strSearchFields.toLowerCase().indexOf('title')<0) strQuery += ', Title';     
        if (strSearchFields != null && strSearchFields.length()>0) strQuery += ','+strSearchFields;
        strQuery += ' FROM '+this.strArticleType;          
        strQuery += ' WHERE Id IN ('+strScopeIds+')';

        List <sObject> articles = new List<sObject>();
        
        try {
            articles = Database.query(strQuery);
            globalSearchandReplace(articles);
        } catch (Exception ex){
            String errMsg = ex.getMessage();
            system.Debug(errMsg);
        } 
    }
    
    public void globalSearchAndReplace(List<sObject> batchArticles){
        // Compile the user-defined search expression
        String strRegEx = this.strSearchString;
        Pattern strSearchStringPattern = Pattern.compile(strRegEx);  
        
        // Initialize list of replacement tasks (work items) for this batch scope
        List<KB_Global_Search_And_Replace__c> listGSR = new List<KB_Global_Search_And_Replace__c>();
        
        // Iterate across all articles queried in a single batch scope
        for(sObject article : batchArticles) {
            this.TotalCount++;
            
            // Determine if one (or more) selected field values contains the user-defined search expression.
            // This logic is used to identify which field(s) in a specific article require replacement.
            // The physical replacement is performed by a separate class (BatchKnowledgeGlobalReplace) to
            // support the much smaller scope size (50) required due to governor constraints specific to
            // knowledge article updates (which are not bulk-enabled) 
            String strReplacementFields = '';              
            if (listSearchFields.size() > 0) {
                for (String strFieldName: listSearchFields) {
                    // Apply pattern to next field value
                    String strFieldValue = (String)article.get(strFieldName);
                    if (strFieldValue == null) continue;
                    Matcher matcher = strSearchStringPattern.matcher(strFieldValue);

                    // If this field value contains at least one occurrence of pattern, add to list
                    if (matcher.find()) {
                        if (strReplacementFields.length()>0) strReplacementFields += ',';
                        strReplacementFields +=strFieldName;
                    }
                }
            }
            
            // If at least one field has been identified, add article to replacement queue and audit log
            if (strReplacementFields.length()>0) {
                this.UpdateCount++;  

                // Add selected article to audit log
                this.listSelectedArticles.add('Article Number='+(String)article.get('ArticleNumber')+
                    ' Title='+(String)article.get('Title')+'\n');//' Language='+strLanguage+
                
                // If performing a physical replacement, add new record (replacement task) to work queue.  This record
                // serves to identify those articles to be processed in the BatchKnowledgeGlobalReplace class.
                // These records stand independently and identify which field(s) in a specific article require replacement.
                //if (!bSearchOnly) {         
                    KB_Global_Search_And_Replace__c modifyArticle = new KB_Global_Search_And_Replace__c();
                    System.debug('this.bSearchOnly111---->'+bSearchOnly);
                    // Copy metadata from batch article to work queue
                    modifyArticle.Apex_BatchId__c = this.strApexSearchJobId;
                    modifyArticle.ArticleId__c = article.Id;
                    modifyArticle.Article_Type__c = this.strArticleType;
                    modifyArticle.ArticleNumber__c = (String)article.get('ArticleNumber'); 
                    modifyArticle.KnowledgeArticleId__c = (String)article.get('KnowledgeArticleId'); 
                    modifyArticle.PublishStatus__c = this.strPublishStatus;                  
                    modifyArticle.Field_Names__c = strReplacementFields;
                    modifyArticle.Search_String__c = this.strSearchString;
                    modifyArticle.Replacement_String__c = this.strReplacementString;
                    //1631 change, added new field Title__c & Replacement_Requested__c
                    modifyArticle.Title__c = (String)article.get('Title');
                    if(this.bSearchOnly== true)
                    modifyArticle.Replacement_Requested__c=false;
                    else
                    modifyArticle.Replacement_Requested__c= true;
                                
                    // Add replacement task to list
                    listGSR.add(modifyArticle);
                    System.debug('listGSR----->'+listGSR);
                //}                
            }
        }
        
        // If at least one replacement task has been identified, insert into physical work queue
        if ( listGSR != null && listGSR.size() > 0) {//if(!bSearchOnly && listGSR != null && listGSR.size() > 0)
            this.bExecuteReplacement = true;
            try {
                System.debug('Inside try block----->');
                Database.insert(listGSR);      
            } catch (Exception ex){
                System.debug('Inside exception----->');
                String errMsg = ex.getMessage();
                system.Debug(errMsg);
            } 
         }        

    }
    
    global void finish(Database.BatchableContext BC) {
        // Launch physical replacement batch class, if required.  Note that the scope size defined
        // for the replacement batch job is limited to 50 articles.  This is required to avoid governor
        // constraints related to article updates which must be processed independently since the 
        // KbManagement.PublishingService class is not bulk-enabled.
        if (!bSearchOnly) {
            if (bExecuteReplacement) {
                BatchKnowledgeGlobalReplace batch = new BatchKnowledgeGlobalReplace(this.strApexSearchJobId,this.publish);
                this.strApexReplacementJobId = Database.executeBatch(batch, 50);  
            } else {
                this.strApexReplacementJobId = 'Not Executed'; 
            } 
        } else {
            this.strReplacementString = '**Search Only - No Replacement**';
        }
   
        KB_Global_Search_History__c searchHistory = new KB_Global_Search_History__c();
        
        // In parent controller, the Database.executebatch method only returns first 15 characters of job id
        searchHistory.Apex_BatchId__c = this.strApexSearchJobId;
        searchHistory.Articles_Searched__c = TotalCount;
        searchHistory.Articles_Selected__c = UpdateCount; 
        searchHistory.Apex_Replacement_BatchId__c = this.strApexReplacementJobId;
        searchHistory.Search_String__c = this.strSearchString;
        searchHistory.Replacement_String__c = this.strReplacementString;
        searchHistory.Field_Names__c = this.strSearchFields;
        searchHistory.Article_Type__c = this.strArticleType;
        searchHistory.PublishStatus__c = this.strPublishStatus;
        if(this.bSearchOnly== true)
        searchHistory.Replacement_Requested__c=false;
        else
        searchHistory.Replacement_Requested__c=true;
        

        
        try {
            Database.insert(searchHistory);      
        } catch (Exception ex){
            String errMsg = ex.getMessage();
            system.Debug(errMsg);
        }                          
                                    
        // Query the AsyncApexJob object to retrieve the current job's metadata.
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
           TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :this.strApexSearchJobId];             
               
        // Email the Batch Job's submitter that the Job is finished.
        // Note that this requires the email administration->deliverability->access level to be set to All Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Salesforce Knowledge Global Search and Replace-Search Complete');
        String strPlainTextBody = 

         '<p>Article Type='+this.strArticleType+'<br></br>'+
         'Publish Status='+this.strPublishStatus+'<br></br>'+                   
         'Search Fields='+this.strSearchFields+'<br></br>'+
         'Search String='+this.strSearchString+'<br></br>'+
         'Replacement String='+this.strReplacementString+'<br></br><br></br><br></br></p>'+
         '<p>Total number of articles searched='+TotalCount+'<br></br>'+
         'Total number of articles selected for update='+UpdateCount+'<br></br>';
         
         if (!bExecuteReplacement) strPlainTextBody += '[No Replacement Job Executed]<br></br></p>';
         
         strPlainTextBody += '<br></br><br></br>'+
         '<p>Salesforce.com Job Statistics:<br></br>'+
         'Number of Errors='+a.NumberOfErrors+'<br></br>'+
         'Status='+a.Status+'<br></br><br></br>';
         
             
        Integer nLogTitleCount = 0;
        if (this.listSelectedArticles != null && this.listSelectedArticles.size() > 0) {
            /*commented for 1631
            for (String strArticleLog : this.listSelectedArticles) {
                if (strPlainTextBody.length()<nMaxSizePlainTextBody ) {
                    nLogTitleCount++;
                    strPlainTextBody += strArticleLog+'\n';
                } else {
                    strPlainTextBody += '...'+(UpdateCount-nLogTitleCount)+' titles omitted';
                    break;
                }
            }*/
            strPlainTextBody +='Please ';
            strPlainTextBody += '<a href="'+system.label.KBViewStatusSearchDetail+this.strApexSearchJobId+'">Click Here</a>' ;
            strPlainTextBody +=' To View Articles Identified for Replacement:\n';
        } else {
            strPlainTextBody += 'None';
        }
         strPlainTextBody+='</p>';
        mail.setHTMLBody(strPlainTextBody);
          
        if (!Test.isRunningTest()) {  
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });         
        }
    
    }
}