/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               

 |  ====          =========      ==        ===========       
 
 | 15/06/2013     Leonard Victor       This Class is used for Landing PAges
 
 | 11/09/2013     Jaspreet Singh        Added method validateLanguage to check for Langauge validation for Case types GRO & MCO
 +==================================================================================================================**/

public class SC_CaseSearchController {

 
public String recordType {get; set;}

public Case caseObj {get; set;}
public List<Case> lstCase{get; set;}
public Boolean renderCaseTable{get; set;}
public Boolean searchBlock{get; set;}
public Boolean showError{get; set;}
public String selectedLanguage{get; set;}
public List<SelectOption> selectLanguage { get; set; }
      // paging
      public PageManager objPageManager       {get;set;}
      public List<Case> listCase = new List<Case>();

public Map<String,String>  recordTypeEmailMap{get; set;}

public Map<String,SC13_Mail_Mapping__c> mapMail = new Map<String,SC13_Mail_Mapping__c>();

/* @Method <This is Constructor For Landing Page>
@param <This method takes StandardController as parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/
    

    public SC_CaseSearchController(ApexPages.StandardController controller) {
    
    caseObj = (Case)controller.getRecord();  
    lstCase = new List<case>();
    renderCaseTable= false;
    searchBlock = true;
    showError = false;
    selectedLanguage= ApexPages.currentPage().getParameters().get('langForPage');
    recordTypeEmailMap = new Map<String,String>();

    mapMail = SC13_Mail_Mapping__c.getall();

    //Language Selection 


       //Intially Setting Lang to English
          selectedLanguage = 'en_US';

          selectLanguage = new List<SelectOption>();
     //     selectLanguage.add(new SelectOption('','--NONE--'));
          Map<String,SC13_Language__c> mapLangauage = SC13_Language__c.getall();

            //Creating and adding value to List for sorted Language display
            List<String> lstLangSorted = new List<String>();

                lstLangSorted.addall(mapLangauage.keyset());
                lstLangSorted.sort();


          for(String langName : lstLangSorted){


              selectLanguage.add(new SelectOption(mapLangauage.get(langName).Language_Code__c , langName));

          }
         

        //Page Maanger Logic
            Integer pageSize = 10;    
            objPageManager = new PageManager(pageSize);
                objPageManager.page = 0;
        objPageManager.numberOfRows =0;
        //End of Page Manager Logic


      
   }
   
   public PageReference validateLanguage(){
       PageReference pageRef;
       if(selectedLanguage=='en_US'){
            pageRef = new PageReference('/apex/SC_Case_Form?recordType='+recordType+'&langForPage='+selectedLanguage); 
       }else{ 
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, Label.SC_Landing_External_Language_Error);
            ApexPages.addMessage(myMsg);
            pageRef=null;
       }
       return pageRef ;
   }

/* @Method <This is Constructor For Landing Page>
@param <This Is Default Constructor For This Class>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/

      public SC_CaseSearchController() {

          //Intially Setting Lang to English
          selectedLanguage = 'en_US';

          selectLanguage = new List<SelectOption>();
     //     selectLanguage.add(new SelectOption('','--NONE--'));
          Map<String,SC13_Language__c> mapLangauage = SC13_Language__c.getall();

            //Creating and adding value to List for sorted Language display
            List<String> lstLangSorted = new List<String>();

                lstLangSorted.addall(mapLangauage.keyset());
                lstLangSorted.sort();


          for(String langName : lstLangSorted){


              selectLanguage.add(new SelectOption(mapLangauage.get(langName).Language_Code__c , langName));

          }




    }

/* @Method <This is Action Method For Search Case This Metthod is not used now>
@param <This method takes StandardController as parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/
      
    public PageReference searchCase(){
     

       String caseNumber = '';
       String requestorEmail = '';
       caseNumber = caseObj.CaseNumber;
       requestorEmail = caseObj.Contact_Email1__c;

        /*Taking Value from custom setting for Case Numer Lenth Because by default the size of Case Number field lenth is 30 , 
        if there is a case number field size increase from 8 digits to x digits need to change Custom Label value.*/

        Integer caseNumberDigits = Integer.valueof(Label.SC13_Case_Number_Length);
        

       
       if(caseNumber.length()>0 || requestorEmail.length()>0){

            caseNumber = caseNumber.leftPad(caseNumberDigits);
            caseNumber = caseNumber.Replace(' ','0');
       
            if(requestorEmail.length()>0){
                 lstCase = [Select id,CaseNumber,status,RecordType.name,CreatedDate,Type,Priority,Subject,ThreadId__c,LastModifiedDate from Case where CaseNumber=:caseNumber.trim() or Contact_Email1__c=:requestorEmail.trim() order by LastModifiedDate desc limit 50];
            }

            else{
                 lstCase = [Select id,CaseNumber,status,RecordType.name,CreatedDate,Type,Priority,Subject,ThreadId__c,LastModifiedDate from Case where CaseNumber=:caseNumber.trim() order by LastModifiedDate desc limit 50];

            }

           if(!lstCase.isEmpty()){
                   
              renderCaseTable= true;

            //  searchBlock = false;
           
           }
            else{
               renderCaseTable = false;
               showError = true;
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning, Label.SC13_Search_Msg1);
              ApexPages.addmessage(myMsg);     
          
           }


  
    }

    else{
            renderCaseTable = false;
            showError = true;
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning, Label.SC13_Search_Msg2);
         ApexPages.addmessage(myMsg);      

    }

    return null;

    }

     public List<Case> getListCase(){
     
      listCase.clear();
        if(lstCase != Null){        
           objPageManager.numberOfRows = lstCase.size();             
          for(integer i=objPageManager.startIndex;i<objPageManager.endIndex&&i<lstCase.size();i++){
                  listCase.add(lstCase.get(i));
                  if(mapMail.containsKey(lstCase.get(i).RecordType.name)){

                  }

                  else{

                      recordTypeEmailMap.put(lstCase.get(i).RecordType.name , 'presalessupportsfdc@emc.com');

                  }

                  
          }
        }       
       return listCase ;   
    } 

 public PageReference gotoMain(){
            lstCase.clear();

            renderCaseTable= false;
             searchBlock = true;
           
            
            return null;




    }

 
}