/*==========================================================================================
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
|  27/Oct/2014   Aarti          GBS Project     Created class for TR CR & Case mapping.
|
+===========================================================================================*/

public class GBS_Transaction_Register_CaseCreation {
    
    /*
     Method name : onInsertTRLogic
     parameters : newTRList
     return type : void
     Description : This method is used to implement logic after tr record is inserted
    */
    public static void onInsertTRLogic(List<Transaction_Register__c> newTRList){
    
        List<Case> trCaseList=new List<Case>();
        Set<String> custIds=new Set<String>();
        Case rejCase=new Case();
        List<Case> rejCaseList=new List<Case>();
        Map<Id,Case> TRandCaseMap=new Map<Id,Case>();
        Map<Customer_Requirements__c,List<Transaction_Register__c>> CRandTRmap=new Map<Customer_Requirements__c,List<Transaction_Register__c>>();
        List<Transaction_Register__c> TRWithCaseIdList=new List<Transaction_Register__c>();
        
        Map<String,Schema.RecordTypeInfo> recordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName(); 
        String caseTrRecordTypeId = recordTypes.get('Credit and Collections - Transaction Register').getRecordTypeId();
      
        // creation of case record when tr is created
        for(Transaction_Register__c tr:newTRList){
            
            custIds.add(tr.CUSTOMER_ID__c);
            Case trCase=new Case(RecordTypeId=caseTrRecordTypeId,Status='Open',Company_Code__c=tr.Company_Code__c,CUSTOMER_ID__c=tr.CUSTOMER_ID__c,Customer_Name__c=tr.Cust_Name__c,Collector_Code__c=tr.Collector_Code__c,Invoice_Date__c=tr.Invoice_Date__c,Invoice_Currency__c=tr.Currency__c);
            trCaseList.add(trCase);
            TRandCaseMap.put(tr.Id,trCase);
        }
        
        insert trCaseList;
        Map<String,Id> CustIdandCRmap=new Map<String,Id>();
        for(Customer_Requirements__c crObj:[SELECT id,Name from Customer_Requirements__c where Name in:custIds]){
        CustIdandCRmap.put(crObj.Name,crObj.Id);
        }
        
        //case status is updated as In Progress if tr record is rejected
        for(Transaction_Register__c tr:newTRList){
            Transaction_Register__c trUpdated=new Transaction_Register__c(Id=tr.Id);
            if(tr.Rejected__c==true){
                rejCase=TRandCaseMap.get(tr.Id);
                rejCase.Status='In Progress';
                rejCaseList.add(rejCase);
            }
            If(CustIdandCRmap.containsKey(tr.CUSTOMER_ID__c)){
            trUpdated.Customer_Requirements__c=CustIdandCRmap.get(tr.CUSTOMER_ID__c);
            }
            trUpdated.Case__c=TRandCaseMap.get(tr.Id).Id;
            TRWithCaseIdList.add(trUpdated);
        }
        update rejCaseList;
        update TRWithCaseIdList;
    }
    
   
    /*
     Method name : onUpdateTRLogic
     parameters : oldTRmap,newTRmap
     return type : void
     Description : This method is used to implement logic after tr record is updated
    */
    public static void onUpdateTRLogic(Map<Id,Transaction_Register__c> oldTRmap, Map<Id,Transaction_Register__c> newTRmap){
    
        //Identify modified records and create list of those records
        List<Transaction_Register__c> newTRList = new List<Transaction_Register__c>();
        Set<Id> caseIdSet=new Set<Id>();
        Transaction_Register__c transactionRecord;
        set<id> caseIdtoUpdate = new set<id>();
        set<id> caseIdtoUpdateNotClose = new set<id>();
        Case rejCase = new Case();
        List<Case> rejCaseList=new List<Case>();
        for(Id oldId : oldTRmap.keySet()){
        
            for(Id newId : newTRmap.keySet()){
            
                //variable having transactionRecord
                transactionRecord = new Transaction_Register__c();
                transactionRecord = newTRmap.get(newId);
             
                if((oldId == newId) && (oldTRmap.get(oldId).Rejected__c!= transactionRecord.Rejected__c) &&(transactionRecord.Rejected__c==true)){
                
                    newTRList.add(transactionRecord);
                    caseIdSet.add(transactionRecord.Case__c);
                }
            
                if(caseIdSet.isEmpty()){ 
                        
                    if((oldId == newId)&&(transactionRecord.Rejected__c!=true) && ((transactionRecord.TRX_TYPE__c != 'PENDING') && (transactionRecord.TRX_TYPE__c != 'CBI PENDING') && (transactionRecord.TRX_TYPE__c != null)))
                    {
                      caseIdtoUpdate.add(transactionRecord.Case__c);
                    }
                }
           
                if((caseIdSet.isEmpty()) && (caseIdtoUpdate.isEmpty())){
                    if((oldId == newId)&&(transactionRecord.Rejected__c!=true) && (transactionRecord.TRX_TYPE__c != null) && ((transactionRecord.TRX_TYPE__c == 'PENDING')||(transactionRecord.TRX_TYPE__c == 'CBI PENDING')))
                    {
                      caseIdtoUpdateNotClose.add(transactionRecord.Case__c);
                    } 
                }
            }
        }
 
        //case status is updated as In Progress if tr record is rejected
        if(!caseIdSet.isempty()){
        
            for(Case caseRecord:[SELECT id,Status from Case where Id in:caseIdSet]){
                caseRecord.Status='In Progress';
                caseRecord.Invoice_Rejected_by_customer__c= 'Yes';
                rejCaseList.add(caseRecord);
            }
        }
      
        //case status is updated as Closed based on TRX TYPE value
        if(!caseIdtoUpdate.isEmpty())
        {
            for(Case caseRecord:[SELECT id,Status from Case where Id in:caseIdtoUpdate])
            {
              caseRecord.Status='Closed';
              rejCaseList.add(caseRecord);
            }
        }
      
        //case status is updated as Pending Internal based on TRX TYPE value
        if(!caseIdtoUpdateNotClose.isEmpty())
        {
            for(Case caseRecord:[SELECT id,Status from Case where Id in:caseIdtoUpdateNotClose])
            {
              caseRecord.Status='Pending Internal';
              rejCaseList.add(caseRecord);
            }
        }
      
      update rejCaseList;
        
    }
}