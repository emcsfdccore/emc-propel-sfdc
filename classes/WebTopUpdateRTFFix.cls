@RestResource(urlMapping='/ArticleUpdateAndPublish/*')
global with sharing class WebTopUpdateRTFFix {
	@HttpPost
	global static void doPost(String articleId1, String articleType, String richtextcontent) {   
		// Use some regex to transform title into a valid urlname
		//Notes__c=richtextcontent	
       // Base SOQL construct
       try
       {
        List<Id> articleIDs = new List<Id>();
        List<String> articleNumbers = new List<String>();
         List<ETA__kav> articlesETA = new List<ETA__kav>();
         List<Break_Fix__kav> articlesBreakFix = new List<Break_Fix__kav>();
         List<How_To__kav> articlesHowTo = new List<How_To__kav>();
     if (articleType == 'Break Fix')
     {
      for (Break_Fix__kav a :[SELECT Id, ArticleNumber, KnowledgeArticleId FROM Break_Fix__kav WHERE PublishStatus = 'online' AND Language = 'en_US' and ArticleNumber = :articleId1 /*and ArticleNumber = '000120976'*/]) {
                  String Id = KbManagement.PublishingService.editOnlineArticle(a.KnowledgeArticleId, true);
                   if (Id == null) System.debug('##### ERROR putting Break Fix  solution into draft');
                  articleNumbers.add(a.ArticleNumber);
                  }


          
           for (Break_Fix__kav d :[SELECT Id, KnowledgeArticleId,Bug_Tracking_Number__c FROM Break_Fix__kav WHERE PublishStatus = 'draft' AND Language = 'en_US' AND ArticleNumber IN :articleNumbers]) {
            articleIDs.add(d.KnowledgeArticleId);
            d.Bug_Tracking_Number__c = richtextcontent;
            articlesBreakFix.add(d);
           }       
         update articlesBreakFix;	
     }  
     else if (articleType == 'How To')
     {
    
        	 for (How_To__kav a :[SELECT Id, ArticleNumber, KnowledgeArticleId FROM How_To__kav WHERE PublishStatus = 'online' AND Language = 'en_US' and ArticleNumber = :articleId1 /*and ArticleNumber = '000120976'*/]) {
                  String Id = KbManagement.PublishingService.editOnlineArticle(a.KnowledgeArticleId, true);
                   if (Id == null) System.debug('##### ERROR putting How To  solution into draft');
                  articleNumbers.add(a.ArticleNumber);
                  }


           List<How_To__kav> articles = new List<How_To__kav>();
           for (How_To__kav d :[SELECT Id, KnowledgeArticleId,Bug_Tracking_Number__c FROM How_To__kav WHERE PublishStatus = 'draft' AND Language = 'en_US' AND ArticleNumber IN :articleNumbers]) {
            articleIDs.add(d.KnowledgeArticleId);
            d.Bug_Tracking_Number__c = richtextcontent;
            articlesHowTo.add(d);
           }       
         update articlesHowTo;
        	
        	
     } 
     else if (articleType == 'ETABUG')
     {
     	for (ETA__kav a :[SELECT Id, ArticleNumber, KnowledgeArticleId FROM ETA__kav WHERE PublishStatus = 'online' AND Language = 'en_US' and ArticleNumber = :articleId1 /*and ArticleNumber = '000120976'*/]) {
                  String Id = KbManagement.PublishingService.editOnlineArticle(a.KnowledgeArticleId, true);
                   if (Id == null) System.debug('##### ERROR putting ETA  solution into draft');
                  articleNumbers.add(a.ArticleNumber);
                  }


          
           for (ETA__kav d :[SELECT Id, KnowledgeArticleId,Bug_Tracking_Number__c FROM ETA__kav WHERE PublishStatus = 'draft' AND Language = 'en_US' AND ArticleNumber IN :articleNumbers]) {
            articleIDs.add(d.KnowledgeArticleId);
            d.Bug_Tracking_Number__c = richtextcontent;
            articlesETA.add(d);
           }       
         update articlesETA;
     }
     else
     {
     
        if (articleType == 'BreakFix'){

             for (Break_Fix__kav a :[SELECT Id, ArticleNumber, KnowledgeArticleId FROM Break_Fix__kav WHERE PublishStatus = 'online' AND Language = 'en_US' and ArticleNumber = :articleId1 /*and ArticleNumber = '000120976'*/]) {
                  String Id = KbManagement.PublishingService.editOnlineArticle(a.KnowledgeArticleId, true);
                   if (Id == null) System.debug('##### ERROR putting Break Fix  solution into draft');
                  articleNumbers.add(a.ArticleNumber);
                  }


          
           for (Break_Fix__kav d :[SELECT Id, KnowledgeArticleId,Notes__c FROM Break_Fix__kav WHERE PublishStatus = 'draft' AND Language = 'en_US' AND ArticleNumber IN :articleNumbers]) {
            articleIDs.add(d.KnowledgeArticleId);
            d.Notes__c = richtextcontent;
            articlesBreakFix.add(d);
           }       
         update articlesBreakFix;
        }
        else if (articleType == 'HowTo'){
        	 for (How_To__kav a :[SELECT Id, ArticleNumber, KnowledgeArticleId FROM How_To__kav WHERE PublishStatus = 'online' AND Language = 'en_US' and ArticleNumber = :articleId1 /*and ArticleNumber = '000120976'*/]) {
                  String Id = KbManagement.PublishingService.editOnlineArticle(a.KnowledgeArticleId, true);
                   if (Id == null) System.debug('##### ERROR putting How To  solution into draft');
                  articleNumbers.add(a.ArticleNumber);
                  }


           List<How_To__kav> articles = new List<How_To__kav>();
           for (How_To__kav d :[SELECT Id, KnowledgeArticleId,Notes__c FROM How_To__kav WHERE PublishStatus = 'draft' AND Language = 'en_US' AND ArticleNumber IN :articleNumbers]) {
            articleIDs.add(d.KnowledgeArticleId);
            d.Notes__c = richtextcontent;
            articlesHowTo.add(d);
           }       
         update articlesHowTo;
        	
        }
        else {
        	 for (ETA__kav a :[SELECT Id, ArticleNumber, KnowledgeArticleId FROM ETA__kav WHERE PublishStatus = 'online' AND Language = 'en_US' and ArticleNumber = :articleId1 /*and ArticleNumber = '000120976'*/]) {
                  String Id = KbManagement.PublishingService.editOnlineArticle(a.KnowledgeArticleId, true);
                   if (Id == null) System.debug('##### ERROR putting ETA  solution into draft');
                  articleNumbers.add(a.ArticleNumber);
                  }


          
           for (ETA__kav d :[SELECT Id, KnowledgeArticleId,Notes__c FROM ETA__kav WHERE PublishStatus = 'draft' AND Language = 'en_US' AND ArticleNumber IN :articleNumbers]) {
            articleIDs.add(d.KnowledgeArticleId);
            d.Notes__c = richtextcontent;
            articlesETA.add(d);
           }       
         update articlesETA;
        	
        }
       }
        for (String articleId : articleIds) {

          KbManagement.PublishingService.publishArticle(articleId, true);
         }
        
        
        
        
        
       
		RestContext.response.addHeader('Content-Type', 'application/json');	    
		if (articleType == 'BreakFix' || articleType == 'Break Fix'){
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(articlesBreakFix));
		system.debug('jsonStringForResponse--->'+articlesBreakFix);
		}
		
		else if (articleType == 'HowTo'|| articleType == 'How To')
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(articlesHowTo));
		else
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(articlesETA));
		RestContext.response.statusCode = 200;
       }
       catch ( Exception ex ) {
            RestContext.response.statusCode = 500;
            String jsonResponse = '{"response": {"status": "Failure", "message": "' + ex + '"}}';
            RestContext.response.responseBody  = Blob.valueOf(JSON.serialize(jsonResponse));
            system.debug('jsonStringForResponse--->'+jsonResponse);
        }
      	
	}
	static testMethod void webTopUpdateArticleBF() {
        test.startTest();  
        
        Break_Fix__kav breakfix = new Break_Fix__kav();
        breakfix.Title = 'TTTest Class Break Fix';         
        breakfix.Notes__C='Testing of Webtop Update'; 
        breakfix.ValidationStatus = 'Work In Progress';
        breakfix.UrlName = 'TTTest-Class-Break-fix'; 
        breakfix.Language = 'en_US';      
        insert breakfix; 
        
        List<Id> idList = new List<Id>();
        idList.add(breakfix.id);
        
        KnowledgeArticleVersion kav= [SELECT id,ArticleType, KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE  id in :idList];                
        KbManagement.PublishingService.publishArticle(kav.KnowledgeArticleId, true); 
        String JSONMsg = '"{\"articleId1\":\"' + kav.ArticleNumber + '\",\"articleType\":\"' + 'BreakFix' + '\",\"richtextcontent\":' + 'Finished updating the RTF field' + '}';
        RestRequest req = new RestRequest(); // Build the REST Request for testing
      	req.addParameter('brand', 'brand1');
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated
        //Test Dev URI 02
        req.requestURI = 'https://cs12.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        //03 org with sso enablement.  Will need to know if this is different from system test. 
        //req.requestURI = 'https://cs12-api.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        //Production URI.
       // req.requestURI = 'https://na5-api.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        req.httpMethod = 'POST';        // Perform a POST
        req.requestBody = Blob.valueof(JSONMsg); // Add JSON Message as a POST
        System.assert(kav.id  != null);
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        WebTopUpdateRTFFix.doPost(kav.ArticleNumber,'BreakFix','This is the final output after change during test');
        for (Break_Fix__kav d :[SELECT Id, KnowledgeArticleId,Notes__c FROM Break_Fix__kav WHERE PublishStatus = 'Online' AND Language = 'en_US' AND ArticleNumber =  :kav.ArticleNumber]) {
            System.assert(d.KnowledgeArticleId  != null);
            System.assertEquals('This is the final output after change during test', d.Notes__c);
        }   
        Test.stopTest();   
	}
	static testMethod void webTopUpdateArticleHT() {
        test.startTest();  
        
        How_To__kav howto = new How_To__kav();
        howto.Title = 'TTTest Class Break Fix';         
        howto.Notes__C='Testing of Webtop Update'; 
        howto.ValidationStatus = 'Work In Progress';
        howto.UrlName = 'TTTest-Class-Break-fix'; 
        howto.Language = 'en_US';      
        insert howto; 
        
        List<Id> idList = new List<Id>();
        idList.add(howto.id);
        
        KnowledgeArticleVersion kav= [SELECT id,ArticleType, KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE  id in :idList];                
        KbManagement.PublishingService.publishArticle(kav.KnowledgeArticleId, true); 
        String JSONMsg = '"{\"articleId1\":\"' + kav.ArticleNumber + '\",\"articleType\":\"' + 'HowTo' + '\",\"richtextcontent\":' + 'Finished updating the RTF field' + '}';
        RestRequest req = new RestRequest(); // Build the REST Request for testing
      	req.addParameter('brand', 'brand1');
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated
        //Test Dev URI 02
        req.requestURI = 'https://cs12.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        //03 org with sso enablement.  Will need to know if this is different from system test. 
        //req.requestURI = 'https://cs12-api.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        //Production URI.
       // req.requestURI = 'https://na5-api.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
       req.httpMethod = 'POST';        // Perform a POST
        req.requestBody = Blob.valueof(JSONMsg); // Add JSON Message as a POST
        System.assert(kav.id  != null);
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        WebTopUpdateRTFFix.doPost(kav.ArticleNumber,'HowTo','This is the final output after change during test');
        for (How_To__kav d :[SELECT Id, KnowledgeArticleId,Notes__c FROM How_To__kav WHERE PublishStatus = 'Online' AND Language = 'en_US' AND ArticleNumber =  :kav.ArticleNumber]) {
            System.assert(d.KnowledgeArticleId  != null);
            System.assertEquals('This is the final output after change during test', d.Notes__c);
        }   
        Test.stopTest();   
	}
	static testMethod void webTopUpdateArticleETA() {
        test.startTest();  
        
        ETA__kav eta = new ETA__kav();
        eta.Title = 'TTTest Class Break Fix';         
        eta.Notes__C='Testing of Webtop Update'; 
        eta.ValidationStatus = 'Work In Progress';
        eta.UrlName = 'TTTest-Class-Break-fix'; 
        eta.Language = 'en_US';      
        insert eta; 
        
        List<Id> idList = new List<Id>();
        idList.add(eta.id);
        
        KnowledgeArticleVersion kav= [SELECT id,ArticleType, KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE  id in :idList];                
        KbManagement.PublishingService.publishArticle(kav.KnowledgeArticleId, true); 
        String JSONMsg = '"{\"articleId1\":\"' + kav.ArticleNumber + '\",\"articleType\":\"' + 'ETA' + '\",\"richtextcontent\":' + 'Finished updating the RTF field' + '}';
        RestRequest req = new RestRequest(); // Build the REST Request for testing
      	req.addParameter('brand', 'brand1');
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated
        //Test Dev URI 02
        req.requestURI = 'https://cs12.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        //03 org with sso enablement.  Will need to know if this is different from system test. 
        //req.requestURI = 'https://cs12-api.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        //Production URI.
       // req.requestURI = 'https://na5-api.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
       req.httpMethod = 'POST';        // Perform a POST
        req.requestBody = Blob.valueof(JSONMsg); // Add JSON Message as a POST
        System.assert(kav.id  != null);
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        WebTopUpdateRTFFix.doPost(kav.ArticleNumber,'ETA','This is the final output after change during test');
        for (ETA__kav d :[SELECT Id, KnowledgeArticleId,Notes__c FROM ETA__kav WHERE PublishStatus = 'Online' AND Language = 'en_US' AND ArticleNumber =  :kav.ArticleNumber]) {
            System.assert(d.KnowledgeArticleId  != null);
            System.assertEquals('This is the final output after change during test', d.Notes__c);
        }   
        Test.stopTest();   
	}
	static testMethod void webTopUpdateArticleBugBF() {
        test.startTest();  
        
       Break_Fix__kav breakfix = new Break_Fix__kav();
        breakfix.Title = 'TTTest Class Break Fix';         
        breakfix.Notes__C='Testing of Webtop Update'; 
        breakfix.ValidationStatus = 'Work In Progress';
        breakfix.UrlName = 'TTTest-Class-Break-fix'; 
        breakfix.Language = 'en_US';      
        insert breakfix; 
        
        List<Id> idList = new List<Id>();
        idList.add(breakfix.id);
        
        KnowledgeArticleVersion kav= [SELECT id,ArticleType, KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE  id in :idList];                
        KbManagement.PublishingService.publishArticle(kav.KnowledgeArticleId, true); 
        String JSONMsg = '"{\"articleId1\":\"' + kav.ArticleNumber + '\",\"articleType\":\"' + 'BreakFix' + '\",\"richtextcontent\":' + 'bugtest,bugtest1' + '}';
        RestRequest req = new RestRequest(); // Build the REST Request for testing
      	req.addParameter('brand', 'brand1');
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated
       //Test Dev URI 02
        req.requestURI = 'https://cs12.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        //03 org with sso enablement.  Will need to know if this is different from system test. 
        //req.requestURI = 'https://cs12-api.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        //Production URI.
       // req.requestURI = 'https://na5-api.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
       req.httpMethod = 'POST';        // Perform a POST
        req.requestBody = Blob.valueof(JSONMsg); // Add JSON Message as a POST
        System.assert(kav.id  != null);
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        WebTopUpdateRTFFix.doPost(kav.ArticleNumber,'Break Fix','bugtest,bugtest1');
        for (Break_Fix__kav d :[SELECT Id, KnowledgeArticleId,Bug_Tracking_Number__c FROM Break_Fix__kav WHERE PublishStatus = 'Online' AND Language = 'en_US' AND ArticleNumber =  :kav.ArticleNumber]) {
            System.assert(d.KnowledgeArticleId  != null);
            System.assertEquals('bugtest,bugtest1', d.Bug_Tracking_Number__c);
        }   
        Test.stopTest();   
	}
	static testMethod void webTopUpdateArticleBugHT() {
        test.startTest();  
        
        How_To__kav howto = new How_To__kav();
        howto.Title = 'TTTest Class Break Fix';         
        howto.Notes__C='Testing of Webtop Update'; 
        howto.ValidationStatus = 'Work In Progress';
        howto.UrlName = 'TTTest-Class-Break-fix'; 
        howto.Language = 'en_US';      
        insert howto; 
        
        List<Id> idList = new List<Id>();
        idList.add(howto.id);
        
        KnowledgeArticleVersion kav= [SELECT id,ArticleType, KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE  id in :idList];                
        KbManagement.PublishingService.publishArticle(kav.KnowledgeArticleId, true); 
        String JSONMsg = '"{\"articleId1\":\"' + kav.ArticleNumber + '\",\"articleType\":\"' + 'HowTo' + '\",\"richtextcontent\":' + 'Finished updating the RTF field' + '}';
        RestRequest req = new RestRequest(); // Build the REST Request for testing
      	req.addParameter('brand', 'brand1');
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated
        //Test Dev URI 02
        req.requestURI = 'https://cs12.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        //03 org with sso enablement.  Will need to know if this is different from system test. 
        //req.requestURI = 'https://cs12-api.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        //Production URI.
       // req.requestURI = 'https://na5-api.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
       req.httpMethod = 'POST';        // Perform a POST
        req.requestBody = Blob.valueof(JSONMsg); // Add JSON Message as a POST
        System.assert(kav.id  != null);
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        WebTopUpdateRTFFix.doPost(kav.ArticleNumber,'How To','bugtest,bugtest2');
        for (How_To__kav d :[SELECT Id, KnowledgeArticleId,Bug_Tracking_Number__c FROM How_To__kav WHERE PublishStatus = 'Online' AND Language = 'en_US' AND ArticleNumber =  :kav.ArticleNumber]) {
            System.assert(d.KnowledgeArticleId  != null);
            System.assertEquals('bugtest,bugtest2', d.Bug_Tracking_Number__c);
        }   
        Test.stopTest();   
	}
	static testMethod void webTopUpdateArticleBugETA() {
        test.startTest();  
        
        ETA__kav eta = new ETA__kav();
        eta.Title = 'TTTest Class Break Fix';         
        eta.Notes__C='Testing of Webtop Update'; 
        eta.ValidationStatus = 'Work In Progress';
        eta.UrlName = 'TTTest-Class-Break-fix'; 
        eta.Language = 'en_US';      
        insert eta; 
        
        List<Id> idList = new List<Id>();
        idList.add(eta.id);
        
        KnowledgeArticleVersion kav= [SELECT id,ArticleType, KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE  id in :idList];                
        KbManagement.PublishingService.publishArticle(kav.KnowledgeArticleId, true); 
        String JSONMsg = '"{\"articleId1\":\"' + kav.ArticleNumber + '\",\"articleType\":\"' + 'ETA' + '\",\"richtextcontent\":' + 'Finished updating the RTF field' + '}';
        RestRequest req = new RestRequest(); // Build the REST Request for testing
      	req.addParameter('brand', 'brand1');
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated
        //Test Dev URI 02
        req.requestURI = 'https://cs12.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        //03 org with sso enablement.  Will need to know if this is different from system test. 
        //req.requestURI = 'https://cs12-api.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
        //Production URI.
       // req.requestURI = 'https://na5-api.salesforce.com/services/apexrest/ArticleUpdateAndPublish';
       req.httpMethod = 'POST';        // Perform a POST
        req.requestBody = Blob.valueof(JSONMsg); // Add JSON Message as a POST
        System.assert(kav.id  != null);
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        WebTopUpdateRTFFix.doPost(kav.ArticleNumber,'ETABUG','bugtest,bugtest2');
        for (ETA__kav d :[SELECT Id, KnowledgeArticleId,Bug_Tracking_Number__c FROM ETA__kav WHERE PublishStatus = 'Online' AND Language = 'en_US' AND ArticleNumber =  :kav.ArticleNumber]) {
            System.assert(d.KnowledgeArticleId  != null);
            System.assertEquals('bugtest,bugtest2', d.Bug_Tracking_Number__c);
        }   
        Test.stopTest();   
	}
	
}