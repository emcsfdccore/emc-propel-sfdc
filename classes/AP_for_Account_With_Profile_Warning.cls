/*=============================================================================
    HISTORY                                                                  
                                                               
    DATE            DEVELOPER                WR             DESCRIPTION                               
    ====            =========                ==             =========== 
|  30/05/2013       Shipra Misra            255125         SFA - Account Presentation Long Term Maintenance
|  23/12/2013       Srikrishna SM           Backward Arrow Added Test.isRunningTest() condition
|  11 Feb 2014      Vivek Barange          #1751                       Modified code to restrict inactive to become CPA 
==============================================================================*/
global class AP_for_Account_With_Profile_Warning implements Database.Batchable<sObject>
{
 string query;
 //Initiate start method.  
 global Database.Querylocator start(Database.BatchableContext bc)
 {
      Map<String,CustomSettingDataValueMap__c> mapDataValueMap = CustomSettingDataValueMap__c.getAll();
      string setHouseAccountExcluded;
       if(mapDataValueMap!= null &&
            mapDataValueMap.get('House Account For AP Exclusion') != null &&
            mapDataValueMap.get('House Account For AP Exclusion').DataValue__c != null)
       {
            system.debug('#### Entered batch calling area');
            Integer countStr=0;
            String strHouseAccuontsExcluded = mapDataValueMap.get('House Account For AP Exclusion').DataValue__c;
            for(string s:strHouseAccuontsExcluded.split(','))
            {
                if(countStr==0)
                {
                    setHouseAccountExcluded='(\''+s+'\'';
                    countStr++;
                }
                else if(countStr>0)
                {
                    setHouseAccountExcluded=setHouseAccountExcluded+',\''+s+'\'';
                }
                
            }
            if(setHouseAccountExcluded.length()>0)
            {
                setHouseAccountExcluded=setHouseAccountExcluded+')';
                system.debug('setHouseAccountExcluded==>'+setHouseAccountExcluded);
            }
       }  
       
      //if(util.isTestCoverage)
      if(Test.isRunningTest())
        query='Select ID, Name, Account_District__c,District_Lookup__c,District_Lookup__r.name, Customer_Profiled_Account__c, Account_Profiling_Warning_Counts__c, type, Account_type__c From Account WHERE type !=  \'Partner\'  and Account_type__c !=  \'Partner\'  and Account_District__c != null and Account_Profiling_Warning_Counts__c>0 and (Not District_Lookup__r.name  in '+setHouseAccountExcluded+') Order by District_Lookup__c, Account_District__c asc , Customer_Profile_Value__c desc limit 2';
        
    else
        query='Select ID, Name, Account_District__c,District_Lookup__c,District_Lookup__r.name, Customer_Profiled_Account__c, Account_Profiling_Warning_Counts__c ,type, Account_type__c, Account_Status__c From Account WHERE type !=  \'Partner\'  and Account_type__c !=  \'Partner\'  and Account_District__c != null and(Not District_Lookup__r.name  in '+setHouseAccountExcluded+') Order by Account_Status__c asc, Protect_this_party_as_CPA__c desc, Customer_Profile_Value__c Desc ,Party_ID__c Asc';
    
        
    return Database.getQueryLocator(query);
 }
 //Initiate execute method.
 global void execute(Database.BatchableContext bc, List<sObject> scope)
 {
     
     //holds List of Account to be updated for Profile Warning.
     List<Account> lstAccountToBeUpdated= new List<Account>();
     set<String> setAccountDistrict= new set<string>();
     set<Id> setDistrictLookup= new set<Id>();
    //Logic to enlist Account Id involved in custom profiling.
      for(SObject sobjects:scope)
     {
        Account AcDist=(Account)sobjects;
        if(setAccountDistrict.contains(AcDist.Account_District__c))continue;    
        setAccountDistrict.add(AcDist.Account_District__c);
        setDistrictLookup.add(AcDist.District_Lookup__r.id);
        system.debug('setDistrictLookup-->'+setDistrictLookup);
     }
     Map<String, List<Account>> mapAccDistrict= new Map<String, List<Account>>();
     List<Account> lstAccount= new List<Account>([Select id, name, Account_District__c,Customer_Profiled_Account__c,Customer_Profiled_Account_Lookup__c from Account where District_Lookup__c in: setDistrictLookup and  Account_District__c =:setAccountDistrict and Customer_Profiled_Account__c=:true]);
     system.debug('lstAccount===>'+lstAccount);
     for(Account AcDistrict:lstAccount)
     {
        
     if(mapAccDistrict.containsKey(AcDistrict.Account_District__c))
            mapAccDistrict.get(AcDistrict.Account_District__c).add(AcDistrict);
        else
        {
            List<Account> temp = new List<Account>();
            temp.add(AcDistrict);
            mapAccDistrict.put(AcDistrict.Account_District__c, temp);
        }
     }
      
     for(SObject sobjects:scope)
     {
        system.debug('mapAccDistrict : '+mapAccDistrict);
        Account AcDist=(Account)sobjects;
        List<Account> lstAccCount=mapAccDistrict.get(AcDist.Account_District__c);
        if(mapAccDistrict.containsKey(AcDist.Account_District__c)  )
        {
            if(lstAccCount.size()==1)
            {
                Id CustomerProfiled=lstAccCount[0].id;
                AcDist.Customer_Profiled_Account_Lookup__c=CustomerProfiled;
                //Start - #1751
                if(AcDist.Account_Status__c=='Inactive') {
                    AcDist.Customer_Profiled_Account__c = false;
                }
                //End - #1751
                lstAccountToBeUpdated.add(AcDist);
            }
            system.debug('AcDist1 : '+AcDist);
        }
        else if(!mapAccDistrict.containsKey(AcDist.Account_District__c))
        {
            AcDist.Customer_Profiled_Account__c=true;
            AcDist.Customer_Profiled_Account_Lookup__c=null;
            system.debug('AcDist.Account_Status__c : '+AcDist.Account_Status__c);
            system.debug('Debug*******'+(AcDist.Account_Status__c=='Inactive'));
            //Start - #1751
            if(AcDist.Account_Status__c=='Inactive') {
                AcDist.Customer_Profiled_Account__c = false;
            }
            //End - #1751
            List<Account> temp = new List<Account>();
            temp.add(AcDist);
            lstAccountToBeUpdated.add(AcDist);
            if(AcDist.Account_Status__c=='Active') {
                mapAccDistrict.put(AcDist.Account_District__c,temp);
            }
            system.debug('AcDist2 :'+AcDist);
        }
     }    

    system.debug('lstAccountToBeUpdated :'+lstAccountToBeUpdated);
        
     
   //Check if list size is greater than 0.
   if(lstAccountToBeUpdated.size()>0)
   {
    Database.update(lstAccountToBeUpdated,false) ;
   }
    
 }
 global void finish(Database.BatchableContext BC)
 {
     
 }
}