@isTest
public class GBS_ChatterOnClosedCases_TC{

   public static testMethod void GBS_ChatterOnClosedCases()
   {
        System.runAs(new user(Id = UserInfo.getUserId()))
            {    
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.partnerBoardingCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
            CustomSettingDataHelper.VCEStaticCSData();
            CustomSettingDataHelper.PSCFieldMappingData();
            CustomSettingDataHelper.GBSConfigData();
           }
   
        Test.startTest();
            
            //Query for Case record type
            List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where RecordType.DeveloperName = 'Presales_Accounts_Payable_T_E_Vendor_Master_Treasury'];
            // Adding case records.
            list<case> listcase = new list<case>();
            listcase.add(new case( Origin ='Community', STATUS = 'Closed', parentid = null, SUBJECT='test case', DESCRIPTION = 'Testing description' , RecordTypeId = recordType[0].id));
            insert listcase;   
            //Chatter post on case
            list<FeedItem> lstchatterFeed = new list<FeedItem>();
            FeedItem  chatterfeed=new FeedItem();
            chatterfeed.ParentId=listcase[0].Id;
            chatterfeed.ContentFileName='test.jpg';
            chatterfeed.ContentData=EncodingUtil.base64Decode('testdata');
            lstchatterFeed.add(chatterfeed);
            insert lstchatterFeed;
            
            // Create FeedComment records.
            list<FeedComment> lstchatterFeedComment = new list<FeedComment>();
            FeedComment  chattercomment=new FeedComment();
            //chattercomment.ParentId=listcase[0].id;
            chattercomment.FeedItemId=lstchatterFeed[0].id;
            chattercomment.CommentBody='test body';
            lstchatterFeedComment.add(chattercomment);
            insert lstchatterFeedComment;
        
                GBS_ChatterOnClosedCases.ClosedCaseChatter(lstchatterFeed);
                GBS_ChatterOnClosedCases.ClosedCaseChatterComment(lstchatterFeedComment);
            
            Test.stopTest();            
   
   }

}