/*=====================================================================================================+
|  HISTORY  |                                                                            
|  DATE          DEVELOPER                WR            DESCRIPTION                               
|  ====          =========                ==            ===========                                                     
| 21/11/2014    Sneha Jain          R2R Dec'14 Rel      Test class for R2R_AssetUncheckDefer_Batch class
+=====================================================================================================*/

@isTest
private class R2R_AssetUncheckDefer_Batch_TC{

    static testmethod void deferAssetUncheck(){
        
        //Creating custom setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        }
        
        //Insert Account record
        Account acc = new Account(Name = 'Test Account',Status__c='A');
        insert acc;

        //Querying Record type details 
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where Name like 'EMC Install'];  
        
        //Create Asset data
        List<Asset__c> assetList= new List<Asset__c>();            
        for (Integer i = 1; i<=5; i++){
            Asset__c ast = new Asset__c (Name = 'Test',Customer_Name__c = acc.id,Install_Base_Status__c = 'Install',Product_Name_Vendor__c = 'EMC',Deffered_to_Renewals__c = true, RecordTypeId = recordType[0].id,  Deferral_Date__c = system.Today() - 10);
            assetList.add(ast);
        }
        
        Test.startTest();
            insert assetList;   
            String jobId = System.schedule('Test','0 0 20 * * ?',new R2R_AssetUncheckDefer_Batch());
            R2R_AssetUncheckDefer_Batch assetUncheck = new R2R_AssetUncheckDefer_Batch();
            assetUncheck.batchQuery = 'select id,Deffered_to_Renewals__c,Deferral_Date__c from Asset__c';
            Database.executeBatch(assetUncheck);
        Test.stopTest();         
    }
}