/*==================================================================================================================+

 |  HISTORY  |                                                                            

 |  DATE          DEVELOPER      WR        DESCRIPTION                              

 |  ====          =========      ==        =========== 
    05 Dec 13   Sneha Jain              Optimized the test class according to best practice, Removed seeAllData  
    22 Jan 15   Karan Shekhar           Optimized the test class according to best practice under platform innovation project   
 +==================================================================================================================*/
@isTest
private Class R2R_Opportunity_Asset_ForAllTriggers_TC{
    static testMethod void integrationManagement()
    {   
        List<Account> accList = new List<Account>();
        List<Account> accList1 = new List<Account>();
        List<Opportunity> OppList = new List<Opportunity>();
        List<Lead> leadList = new List <Lead>();
        List<Opportunity> OppInsertList = new List <Opportunity>();
        /*
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser){
        
          PRM_VPP_JobDataHelper.createVPPCustomSettingData();   
        }
        */
        
        //Creating Custom Setting data
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
        User houseAccount = [Select id from user where name ='House Account' and isActive=true limit 1];
        System.runAs(insertUser)
        {
    
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
            CustomSettingDataHelper.adminConversionCSData();
            CustomSettingDataHelper.houseAccountCSData();
            CustomSettingDataHelper.specialForecastGroupsData();
            CustomSettingDataHelper.manageMassReassignmentData();
        }

        Test.startTest();
        // for trigger OpportunityAssetAfterTriggers
        
        //Create account records
       // Account acc=testclassUtils.getAccount () ;
        //insert acc;
        
        //create Customer Account data , Karan 22 Jan 15, Platform innovation project
        accList=AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        if(!accList.isEmpty()) {
            
            insert accList;
        }
        //Account acc1=testclassUtils.getAccount () ;
        //[select id,name,IsPartner,Status__C,Profiled_Account__c,Type,Partner_Type__c from Account where IsPartner=true and Type='Partner' and Partner_Type__c='Service Provider' limit 1];
        //acc1.IsPartner= true;
        //acc1.IsActive=true;
        //create partner Account data , Karan 22 Jan 15, Platform innovation project
        accList1=AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        for(Integer i = 0;i<accList1.size();i++){
            
            accList1[i].Status__c='A';
            accList1[i].Profiled_Account__c=accList[0].id;
            accList1[i].Type='Partner';
            accList1[i].Partner_Type__c='Service Provider';
        }
        if(!accList1.isEmpty()) {
            
            insert accList1;
        }       
        //insert acc1;            
        //Create Asset records
        List<Asset__c> assetList = new List<Asset__c>();
        
        Asset__c asset1 = new Asset__c();
        asset1.Name='Testasset1';
        asset1.Customer_Name__c=accList[0].id;
        assetList.add(asset1);
        //insert asset1; 
        
        Asset__c asset2 = new Asset__c();
        asset2.Name='Testasset2';
        asset2.Customer_Name__c=accList[0].id;
        assetList.add(asset2);
        //insert asset2;
        if(!assetList.isEmpty()) {
            insert assetList;
        }
        //create Lead data , Karan 22 Jan 15, Platform innovation project
        leadList=LeadTestClassDataHelper.createLead();
        if(!leadList.isEmpty()) {
            
            insert leadList;
        }
        //create Opportunity data , Karan 22 Jan 15, Platform innovation project
        OppList=OpportunityTestClassDataHelper.createOpptys(accList[0],leadList[0]);
        for(Integer i=0;i<OppList.size();i++){
            
            OppList[i].House_Account_Name__c= 'House Account';
            OppList[i].Quote_Cart_Number__c='AXBPK3344';
        }
        if(!OppList.isEmpty()) {
            system.debug('**OppList***'+OppList);
            insert OppList;
        }
        //update Opportunity data, Karan 22 Jan 15, Platform innovation project
        for(Integer i=0;i<OppList.size();i++){
            OppList[i].asset_Ids__c=assetList[0].Id;
            OppList[i].OwnerId = houseAccount.Id;
            OppList[i].Quote_Cart_Number__c = 'AXBPK3344EE';
            OppList[i].LeadSource=System.Label.Contract_Renewal;
        }
        update OppList;
        
        //update Opportunity data, Karan 22 Jan 15, Platform innovation project
        for(Integer i=0;i<OppList.size();i++){
            OppList[i].Quote_Cart_Number__c = null;
        }
       update OppList;

        
        
        //Create oppty records
             /*   Opportunity opp1=testclassUtils.getOppty ();
        opp1.asset_Ids__c=asset1.id;    
        opp1.House_Account_Name__c= 'House Account';
        opp1.Quote_Cart_Number__c='AXBPK3344';
        //opp1.Service_Provider__c=acc1.id;
        insert opp1;
        
        opp1.OwnerId = '00570000001dlVAAAY';
        opp1.Quote_Cart_Number__c='shgkjhhguagf';
        opp1.LeadSource=System.Label.Contract_Renewal;
        
        update opp1;
        
        opp1.Quote_Cart_Number__c=null;
        update opp1;   */
                
        //Create Oppty asset junction records
        Opportunity_Asset_Junction__c oaj1=new Opportunity_Asset_Junction__c();    
        oaj1.Related_Asset__c=assetList[1].id;
        oaj1.Related_Account__c=accList[0].id;
        oaj1.Related_Opportunity__c=OppList[0].id;
        oaj1.Isactive__c=false;
        insert oaj1;
        /*
        Opportunity opp2=testclassUtils.getOppty ();
        opp1.asset_Ids__c=asset1.id;
        insert opp2;  
        
        oaj1.Isactive__c=true;
        update oaj1;
          
        delete oaj1;            
        */
        Test.stopTest();
        }
}