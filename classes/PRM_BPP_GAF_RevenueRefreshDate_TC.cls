@isTest
private class PRM_BPP_GAF_RevenueRefreshDate_TC{

	public static testmethod void PRM_BPP_GAF_RevenueRefreshDate(){
          
		//Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId())){
            CustomSettingDataHelper.dataValueMapCSData();
        }   
		
		//Create Account record
		Account accObj = new Account();        
        accObj.name = 'TestAccount';        
        accObj.BillingCountry ='United States';       
        accObj.BillingCity = 'EDWARDS AFB';         
        accobj.GAF_Data_Refresh_Date__c=system.today(); 
        insert accobj;
    
		//Create GAF R&R record
        GAF_Revenue_Rebate__c gafobj = new GAF_Revenue_Rebate__c();
        gafobj.GAF_Revenue_Rebate_Type__c='Country';
        gafobj.Profiled_Account__c=accobj.id;
        insert gafobj;       
 
		//Update GAF R&R record
        gafobj.GAF_Revenue_Rebate_Type__c='Custom';
        update gafobj;
	}
}