/*
*  Created By: Yamilett Salazar
*  Created Date: June 19, 2013
*  Last Modified By: Yamilett Salazar
*  Description: Test class for EBC_BypassNewBriefingEvent controller.
*  16-Sep-2014   Akash Rastogi  WR # 0659   Updated Sales Associate User Profile Id to Sales Associate User APJ profile Id '00en0000000LsVT'
*/

@isTest(SeeAllData = true)
private class EBC_BypassNewBriefingEvent_TC{

  static testMethod void testNewBriefingEvent(){
    RecordType rt = [SELECT Id FROM RecordType WHERE Name = 'Customer Account Record Type'];
    Account acc = [SELECT Id, Name FROM Account WHERE RecordTypeId = :rt.Id LIMIT 1];
  
    PageReference pageRef = Page.EBC_BypassNewBriefingEvent;      
    pageRef.getParameters().put('id', acc.Id);
    pageRef.getParameters().put('acc', acc.Name);
    Test.setCurrentPage(pageRef);
    Profile pro;
    try{
    pro =[Select Id,Name from profile where Name = 'Sales Associate User APJ'];
    }
    catch (exception e){
    pro =[Select Id,Name from profile where Name = 'System Administrator'];
    }
    if(pro!=null){
    Map<String,CustomSettingDataValueMap__c> DataValueMap= CustomSettingDataValueMap__c.getall();
        if(DataValueMap.get('Sales Associate User APJ')==null)
        {
        List<CustomSettingDataValueMap__c>  csDataList =  new List<CustomSettingDataValueMap__c>();
        csDataList.add(new CustomSettingDataValueMap__c(Name = 'Sales Associate User APJ', DataValue__c = pro.id)); 
        insert csDataList;
        }
        }
    //Test master page layout
    ApexPages.Standardcontroller c1 = new Apexpages.Standardcontroller(new EBC_Briefing_Event__c());
    EBC_BypassNewBriefingEvent be1 = new EBC_BypassNewBriefingEvent(c1);
    be1.newBriefingEvent();   
    
    //Test limited page layout
    // WR # 0659 Updated Sales Associate User Profile Id to Sales Associate User APJ profile Id '00en0000000LsVT'
    User u;
    System.runAs(new user(Id = UserInfo.getUserId())){
    if(pro!= null){
            
     u = new User(Alias = 'standt', Email='standarduser@emc.com.sfdc17abc', 
                      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                      LocaleSidKey='en_US', ProfileId = pro.id, TimeZoneSidKey='America/Los_Angeles',
                      UserName='standarduser@emc.com.sfdc17abc'); 
    insert u;
    }
    System.runAs(u){
    
      ApexPages.Standardcontroller c2 = new Apexpages.Standardcontroller(new EBC_Briefing_Event__c());
      EBC_BypassNewBriefingEvent be2 = new EBC_BypassNewBriefingEvent(c2);
      be2.newBriefingEvent(); 
    }
    }
  }
}