global class Schedule_AP_For_Account_District implements schedulable
{
    public boolean isRunTest=false;
    global void execute(SchedulableContext sc)
    {
        AP_Customer_Profiling_Account_District clsBatchAccDist = new AP_Customer_Profiling_Account_District();
        ID idBatch = Database.executeBatch(clsBatchAccDist, 100);
    }
}