/*=======================================================================================================================================+
 
 |  DATE      DEVELOPER        WR      DESCRIPTION                 

 |  ====      =========        ==      =========== 

 |  17/09/2014  Bisna         CI 1072  Added "Holding" as a filter in the query against AsyncApexJob                           
+=======================================================================================================================================*/


global class KB_BatchOutboundMessagesScheduler implements Schedulable {  
    global void execute(SchedulableContext SC){
        KB_BatchOutboundMessages obj = new KB_BatchOutboundMessages();
        try{
            //1072 adding 'Holding' in filter
            Integer runningBatchJobs = [select count() from AsyncApexJob where JobType = 'BatchApex' and status in ('Queued','Processing','Preparing','Holding')];
            if(runningBatchJobs>4){
                DateTime n = datetime.now().addMinutes(5);
                String cron = '';
                cron += n.second();
                cron += ' ' + n.minute();
                cron += ' ' + n.hour();
                cron += ' ' + n.day();
                cron += ' ' + n.month();
                cron += ' ' + '?';
                cron += ' ' + n.year();
                String jobName = 'Batch Job To Send OBMs - ' + n.format('MM-dd-yyyy-hh:mm:ss');
                KB_BatchOutboundMessagesScheduler nextBatch = new KB_BatchOutboundMessagesScheduler();
                Id scheduledJobID = System.schedule(jobName,cron,nextBatch);        
            }
            else{
                if(!Test.isRunningTest())            //Added by Naga for WR319143
                    database.executebatch(obj);
            }
            //system.abortJob(SC.getTriggerId());
        }
        catch(Exception e){
            String supportEmail = System.Label.CSK_Support;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {supportEmail};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Attention: Error in Batch Job to send OBMs');
            String strPlainTextBody = 

             'Fatal Error :'+e;
             
            mail.setPlainTextBody(strPlainTextBody);
              
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            //database.executebatch(obj);  
        }
    }
}