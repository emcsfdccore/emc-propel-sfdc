/**
 * An apex page controller that exposes the site login functionality
 * Modified in Back Arrow March release. Moved testMethod to test class. By - Jaypal Nimesh, 18-Feb-2014
 */
global class SiteLoginController {
    global String username {get; set;}
    global String password {get; set;}

    global PageReference login() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        return Site.login(username, password, startUrl);
    }
    
    global SiteLoginController () {}
    
    //Commented below testMethod for Back Arrow Optimization
    /*
    global static testMethod void testSiteLoginController () {
        // Instantiate a new controller with all parameters in the page
        SiteLoginController controller = new SiteLoginController ();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
                
        System.assertEquals(controller.login(),null);                           
    }
    */    
}