public class Cases_PSC_Portal_Success_Controller{
    public String caseNumber {get; set;}
    public String lang {get; set;}
    public String recType{get; set;}
    public Cases_PSC_Portal_Success_Controller(){
         caseNumber = ApexPages.currentPage().getParameters().get('caseNum');
         lang = ApexPages.currentPage().getParameters().get('langForPage');
         recType = ApexPages.currentPage().getParameters().get('recordType');
    }
       public PageReference backtoCase(){       
        PageReference pageRef = null;
        if(UserInfo.getUsertype()!='guest'){
         pageRef = new PageReference('/apex/Cases_PSC_Portal_MainPage');
        }
        else{
        pageRef = new PageReference('/apex/Cases_PSC_Site_MainPage');
        }
        pageRef.setRedirect(true);  
        return pageRef;
    }

}