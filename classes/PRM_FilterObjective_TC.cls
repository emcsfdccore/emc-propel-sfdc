/*===========================================================================+

|  HISTORY  |                                                                           

|  DATE          DEVELOPER                WR        DESCRIPTION                               

|  ====          =========                ==        =========== 

| 12/01/2011     Karthik Shivprakash      1173     This is the test class used
                                                   to test filter objectives on
|                                                  the channel account plan.
| 28/02/2011     Saravanan C              2430     udpated the code for EMC channel User profile name used
| 14/12/2011     Anil Sure                         Removed All  Queries.
| 04/12/2014   Srikrishna SM             Optimized this test class
| 19/02/205     Vinod Jetti              #1649     created Hub_Info__c objects data instead of Account 'Site_DUNS_Entity__c' field
+===========================================================================*/
@isTest
private class PRM_FilterObjective_TC {
    
    public PRM_FilterObjective_TC(){
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
        system.runAs(insertUser){
            CustomSettingDataHelper.dataValueMapCSData();   
        }       
    }
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        PRM_FilterObjective_TC fo1 = new PRM_FilterObjective_TC();
        Account acc1 = fo1.createTestAccount();
        insert acc1;
        Account_Groupings__c TestGroup1 = fo1.createAccountGrouping(acc1.Id);
        insert TestGroup1;
        Test.startTest();
        User user1 = fo1.createUser('AMER Channels User');
        RecordType recordType1 = fo1.createRecordType('Americas Distributor CAP');
        
        RecordType recordType2 = fo1.createRecordType('EMEA Reseller CAP');
        
        RecordType recordType3 = fo1.createRecordType('APJ Distributor CAP Locked');
        
        SFDC_Channel_Account_Plan__c channelAccount1 = fo1.createChannelAccountPlan(recordType1.Id,acc1.Id,user1.Id);
        
        SFDC_Channel_Account_Plan__c channelAccount2 = fo1.createChannelAccountPlan(recordType2.Id,acc1.Id,user1.Id);
        
        SFDC_Channel_Account_Plan__c channelAccount3 = fo1.createChannelAccountPlan(recordType3.Id,acc1.Id,user1.Id);
        
        ApexPages.StandardController channelPlan1 = new ApexPages.Standardcontroller(channelAccount1);
        ApexPages.StandardController channelPlan2 = new ApexPages.Standardcontroller(channelAccount2);
        ApexPages.StandardController channelPlan3 = new ApexPages.Standardcontroller(channelAccount3);
        PRM_FilterObjective Obj1 = new PRM_FilterObjective(channelPlan1);
        Obj1.getParentObjectiveTypeList();
        Obj1.getObjectiveTypeList();
        Obj1.getSolutionFocusList();
        Obj1.getIndustryFocusList();
        Obj1.getSolutionList();
        Obj1.getVerticalList();
        Obj1.getSegmentList();
        Obj1.goOnDetailPage();
        Obj1.OpenObjective();
        Obj1.EditRecord();
        Obj1.getGeographyList();
        Obj1.goonpdfpage();     
        Obj1.getObjectiveList();     
       
        PRM_FilterObjective Obj2 = new PRM_FilterObjective(channelPlan2);
        
        Obj2.EMEAFilters = true;
        Obj2.APJFilters = true;
        Obj2.AmerciaFilters = true;
        
        Obj2.ObjectiveType='--None--';
        Obj2.Segment ='--None--';
        Obj2.Vertical ='--None--';
        Obj2.Solutions = 'abc';     
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--None--';
        Obj2.Vertical ='--None--';
        Obj2.Solutions = '--None--';    
        Obj2.Segment = 'abc';       
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--None--';
        Obj2.Solutions = '--None--';    
        Obj2.Segment = '--None--';      
        Obj2.Vertical = 'abc';      
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--abc--';
        Obj2.Solutions = '--None--';    
        Obj2.Segment = '--None--';
        Obj2.Vertical = '--None--';     
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--None--';
        Obj2.Solutions = '--ace--'; 
        Obj2.Segment = '--abe--';
        Obj2.Vertical = '--None--';     
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--dse--';
        Obj2.Solutions = '--None--';    
        Obj2.Segment = '--None--';
        Obj2.Vertical = '--dsdas--';        
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--None--';
        Obj2.Solutions = '--None--';    
        Obj2.Segment = '--Ndsdone--';
        Obj2.Vertical = '--dasdsa--';       
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--fdfds--';
        Obj2.Solutions = '--fdsfds--';  
        Obj2.Segment = '--None--';
        Obj2.Vertical = '--None--';     
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--None--';
        Obj2.Solutions = '--fdfds--';   
        Obj2.Segment = '--None--';
        Obj2.Vertical = '--fdsfds--';       
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--fsdfdsf--';
        Obj2.Solutions = '--None--';    
        Obj2.Segment = '--None--';
        Obj2.Vertical = '--None--';     
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--Ndsadsadone--';
        Obj2.Solutions = '--None--';    
        Obj2.Segment = '--dsads--';
        Obj2.Vertical = '--None--';     
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--sdsad--';
        Obj2.Solutions = '--None--';    
        Obj2.Segment = '--fdssds--';
        Obj2.Vertical = '--dsadsa--';       
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--dsfdsf--';
        Obj2.Solutions = '--dsdfdsfd--';    
        Obj2.Segment = '--None--';
        Obj2.Vertical = '--Nonfvdsfdse--';      
        Obj2.goOnDetailPage();      
        
        Obj2.ObjectiveType='--None--';
        Obj2.Solutions = '--fdsfds--';  
        Obj2.Segment = '--fdsfd--';
        Obj2.Vertical = '--sdfdsfds--';     
        Obj2.goOnDetailPage();   
        
        Obj2.getParentObjectiveTypeList();
        Obj2.getObjectiveTypeList();
        Obj2.getSolutionFocusList();
        Obj2.getIndustryFocusList();
        Obj2.getSolutionList();
        Obj2.getVerticalList();
        Obj2.getSegmentList();
        Obj2.goOnDetailPage();
        Obj2.OpenObjective();
        Obj2.EditRecord();   
       
        Test.stopTest();       
    }
    
    private Account createTestAccount(){
        Account account1 = AccountAndProfileTestClassDataHelper.createSimpleAccount();
        account1.Partner_Type__c='Distribution Var';
        account1.Lead_oppty_enabled__c =True;
        account1.APPR_MTV__Association_Account__c=True;
        account1.PROFILED_ACCOUNT_FLAG__c = true;
        account1.CurrencyIsoCode='USD';
        //#1649
        account1.Hub_Info__c = account1.Hub_Info__c;
        //account1.Site_DUNS_Entity__c = '223652';
        return account1;
    }
    
    private Account_Groupings__c createAccountGrouping(Id accountId){
        Account_Groupings__c grouping = new Account_Groupings__c();
        grouping.Name = 'TestGrouping1';
        grouping.Active__c = true;
        grouping.Profiled_Account__c = accountId;
        grouping.No_Master_Required__c = False;
        return grouping;
    }
    
    private User createUser(String Profilename){
        Profile emcUser = [select Id from Profile where Name=:Profilename];        
        UserRole userRoles = [Select u.Id, u.Name from UserRole u where u.Name='EMC Reference Manager'];        
        User tempUsr = new User(
             Username='testing123'+Math.random()+'@acme4.com.test34',
             TimeZoneSidKey='America/New_York',
             ProfileId=emcUser.Id,
             FirstName='TestingCAP',
             LastName='LastCAP',
             email='john12456@acme.com',
             Alias='test1',             
             LocaleSidKey='en_US',             
             EmailEncodingKey='ISO-8859-1',
             LanguageLocaleKey='en_US',
             Role__c='N/A',
             UserRoleId=userRoles.Id,
             Forecast_Group__c='Direct',
             BU__c='NA',
             CAP_Approver__c=true,
             IsActive=true);
        //insert tempUsr;    
        return tempUsr;
    }
    
     public RecordType createRecordType(String RecordName){
        RecordType recordTypes = [Select r.Id, r.Name, r.SobjectType from RecordType r where r.name=:RecordName];
        return recordTypes;
    }
    
    public SFDC_Channel_Account_Plan__c createChannelAccountPlan(Id recordType,Id accountID,Id userID){
        SFDC_Channel_Account_Plan__c channelAccount = new SFDC_Channel_Account_Plan__c(
        Name='Testing',
        Partner_Account__c=accountID,
        Status__c='New',
        Plan_Period__c='Q1',
        EMC_CAM__c=userID,
        RecordTypeId=recordType);
        insert channelAccount;
        return channelAccount;
    }
}