/*============================================================================================================+
 |  HISTORY  |
 |  DATE          DEVELOPER               WR                        DESCRIPTION 
 |  ====          =========               ==                        =========== 
 | 15 Jan 2013   Hemavathi N M         Account Presentation        This Test Class is used to test Batch job for CQR       
 | 23 Dec 2013   Srikrishna S M        Backward Arrow        	   Test class is optimized for time and coverage       
 +============================================================================================================*/
 
@isTest
private class AP_AssignCQRtoAccount_TC {

    public static Job_Scheduler__c js;
	public static List<Account> cqrAccountList;
	public static List<CQR_Delta_Log__c> cqrDeltaList;
	public static List<District_Lookup__c> districtLookupList;
	public static List<Core_Quota_Rep__c> cqrList;
	public static List<District_Lookup__c> dlLists;
	public static User insertUser;
	public static User houseUser;
	public static String Query = 'Select Id, Account__c,Account_Id__c,Badge_ID__c,Batch_ID__c,Core_Quota_Rep_Id__c,Core_Quota_Rep_Name__c,District_Name__c ,External_ID__c,Oracle_District_ID__c,Role_Name__c,SFDC_User_ID__c from CQR_Delta_Log__c order by Account__c limit 5';
	 
	
	public AP_AssignCQRtoAccount_TC(){
		insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
		houseUser = [SELECT Id, Name FROM User where isActive=true and Name='House Account' limit 1];
		System.RunAs(insertUser){
			CustomSettingDataHelper.eBizSFDCIntCSData();
			CustomSettingDataHelper.dataValueMapCSData();
        }	
	}
	
	public static testMethod void testAP_AssignCQRtoAccountOne(){
		AP_AssignCQRtoAccount_TC constructorCall = new AP_AssignCQRtoAccount_TC();
		setupJobScheduler();
		createCQRAccounts(false,false,false);
		createCQRDeltaLogRecords();
		Test.startTest();
        AP_AssignCQRtoAccount batchExecute = new AP_AssignCQRtoAccount(js,Query);
        Database.executeBatch(batchExecute);
        Test.stopTest();
	}

	public static testMethod void testAP_AssignCQRtoAccountTwo(){
		AP_AssignCQRtoAccount_TC constructorCall = new AP_AssignCQRtoAccount_TC();
		setupJobScheduler();
		createCQRAccounts(false,false,false);
		createCQRDeltaLogRecords();
		createCQR(false,true);
		Test.startTest();
        AP_AssignCQRtoAccount batchExecute = new AP_AssignCQRtoAccount(js,Query);
        Database.executeBatch(batchExecute);
        Test.stopTest();
	}
	
	public static testMethod void testAP_AssignCQRtoAccountThree(){
		AP_AssignCQRtoAccount_TC constructorCall = new AP_AssignCQRtoAccount_TC();
		setupJobScheduler();
		dlLists = createDistrictLookups();
		createCQRAccounts(true,false,false);
		createCQRDeltaLogRecords();
		dlLists[0].Oracle_District_ID__c = cqrAccountList[1].Id;
		update dlLists;
		createCQR(true,false); 
		Test.startTest();
        AP_AssignCQRtoAccount batchExecute = new AP_AssignCQRtoAccount(js,Query);
        Database.executeBatch(batchExecute);
        Test.stopTest();
	}
	
	public static testMethod void testAP_AssignCQRtoAccountFour(){
		
		AP_AssignCQRtoAccount_TC constructorCall = new AP_AssignCQRtoAccount_TC();
		setupJobScheduler();
		dlLists = createDistrictLookups();
		createCQRAccounts(true,false,false);
		createCQRDeltaLogRecords();
		dlLists[0].Oracle_District_ID__c = cqrAccountList[1].Id;
		update dlLists;
		createCQR(true,false); 
		Test.startTest();
        AP_AssignCQRtoAccount batchExecute = new AP_AssignCQRtoAccount(js,Query);
        Database.executeBatch(batchExecute);
        Test.stopTest();
	}
	
	public static testMethod void testAP_AssignCQRtoAccountFive(){
		
		AP_AssignCQRtoAccount_TC constructorCall = new AP_AssignCQRtoAccount_TC();
		setupJobScheduler();
		dlLists = createDistrictLookups();
		createCQRAccounts(true,true,false);
		createCQRDeltaLogRecords();
		dlLists[0].Oracle_District_ID__c = cqrAccountList[1].Id;
		update dlLists;
		createCQR(true,true); 
		Test.startTest();
        AP_AssignCQRtoAccount batchExecute = new AP_AssignCQRtoAccount(js,Query);
        Database.executeBatch(batchExecute);
        Test.stopTest();
	}
	
	public static testMethod void testAP_AssignCQRtoAccountSix(){
		
		AP_AssignCQRtoAccount_TC constructorCall = new AP_AssignCQRtoAccount_TC();
		setupJobScheduler();
		dlLists = createDistrictLookups();
		createDistrictLookups();
		createCQRAccounts(false,true,true);
		createCQRDeltaLogRecords();
		dlLists[0].Oracle_District_ID__c = cqrAccountList[1].Id;
		update dlLists;
		createCQR(true,true); 
		Test.startTest();
        AP_AssignCQRtoAccount batchExecute = new AP_AssignCQRtoAccount(js,Query);
        Database.executeBatch(batchExecute);
        Test.stopTest();
	}
	
	public static void createCQRDeltaLogRecords(){
		cqrDeltaList = new List<CQR_Delta_Log__c>();
		CQR_Delta_Log__c objCQRDelta = new CQR_Delta_Log__c();
		objCQRDelta.Account__c = cqrAccountList[0].Id;
		objCQRDelta.Account_Id__c = cqrAccountList[0].Id;
		cqrDeltaList.add(objCQRDelta);
		insert cqrDeltaList; 
	}
	
	public static void createCQRAccounts(boolean flag, boolean flag1, boolean flag2){
		List<User> lstUser = [Select id from user where IsActive = true and IsPortalEnabled = false limit 2];
		cqrAccountList = new List<Account>();
		
		Account acc1 = AccountAndProfileTestClassDataHelper.createSimpleAccount();
		acc1.Name = 'Test Account 1';
		acc1.Core_Quota_House_Name__c=null;
		cqrAccountList.add(acc1);
		
		if(flag){
			
			Account acc4 = AccountAndProfileTestClassDataHelper.createSimpleAccount();
			acc4.Name = 'Test Account 4';
			acc4.District_Lookup__c = districtLookupList[0].Id;
			acc4.Core_Quota_Rep__c = houseUser.Id;
			cqrAccountList.add(acc4);
			
			Account acc2 = AccountAndProfileTestClassDataHelper.createSimpleAccount();
			acc2.Name = 'Test Account 5';
			acc2.District_Lookup__c = districtLookupList[0].Id;
			acc2.Core_Quota_Rep__c = houseUser.Id;
			cqrAccountList.add(acc2);	

			Account acc3 = AccountAndProfileTestClassDataHelper.createSimpleAccount();
			acc3.Name = 'Test Account 6';
			acc3.District_Lookup__c = districtLookupList[0].Id;
			acc3.Core_Quota_Rep__c = houseUser.Id;
			cqrAccountList.add(acc3);	
				
		}
		if(flag2){
			
			Account acc4 = AccountAndProfileTestClassDataHelper.createSimpleAccount();
			acc4.Name = 'Test Account 1';
			acc4.District_Lookup__c = districtLookupList[0].Id;
			acc4.Core_Quota_Rep__c = lstUser[0].Id;
			cqrAccountList.add(acc4);
			
			Account acc2 = AccountAndProfileTestClassDataHelper.createSimpleAccount();
			acc2.Name = 'Test Account 2';
			acc2.District_Lookup__c = districtLookupList[0].Id;
			acc2.Core_Quota_Rep__c = lstUser[0].Id;
			cqrAccountList.add(acc2);	

			Account acc3 = AccountAndProfileTestClassDataHelper.createSimpleAccount();
			acc3.Name = 'Test Account 3';
			acc3.District_Lookup__c = districtLookupList[0].Id;
			acc3.Core_Quota_Rep__c = lstUser[0].Id;
			cqrAccountList.add(acc3);	
				
		}
		if(flag1){
			Account acc4 = AccountAndProfileTestClassDataHelper.createSimpleAccount();
			acc4.Name = 'Test Account 7';
			acc4.District_Lookup__c = districtLookupList[0].Id;
			acc4.Core_Quota_Rep__c = houseUser.Id;
			acc4.Core_Quota_House_Name__c='TestCQR3';
			cqrAccountList.add(acc4);
			
			Account acc2 = AccountAndProfileTestClassDataHelper.createSimpleAccount();
			acc2.Name = 'Test Account 8';
			acc2.District_Lookup__c = districtLookupList[0].Id;
			acc2.Core_Quota_Rep__c = lstUser[0].Id;
			acc2.Core_Quota_House_Name__c='TestCQR2';
			cqrAccountList.add(acc2);
		}
		insert cqrAccountList; 
	}
	
	public static List<District_Lookup__c> createDistrictLookups(){
		districtLookupList = new List<District_Lookup__c>();
		District_Lookup__c objDistrictLookup1 = new District_Lookup__c();
		objDistrictLookup1.Name ='TestLookup1';
        objDistrictLookup1.Area__c='EDWARDS';
        districtLookupList.add(objDistrictLookup1);
        insert districtLookupList;
        return districtLookupList;
	}
	
	public static void createCQR(boolean flag1,boolean flag2){
		cqrList = new List<Core_Quota_Rep__c>();
		if(flag2){
			Core_Quota_Rep__c cqr1 = new Core_Quota_Rep__c();
			cqr1.Name = 'TestCQR1';
			cqr1.Batch_ID__c = 10;
			cqr1.Badge_ID__c = '111111';
			cqr1.Account_ID__c = cqrAccountList[0].Id;
			cqrList.add(cqr1);
		}
		if(flag1){
			if(flag2){
				Core_Quota_Rep__c cqr2 = new Core_Quota_Rep__c();
				cqr2.Name = 'TestCQR2';
				cqr2.Batch_ID__c = 10;
				cqr2.Badge_ID__c = 'D121212';
				cqr2.Account_ID__c = cqrAccountList[1].Id;
				cqr2.District_Name__c = cqrAccountList[1].District_Lookup__c;
				cqr2.Oracle_District_ID__c =  dlLists[0].Oracle_District_ID__c;
				cqr2.SFDC_User_ID__c =  insertUser.Id;
				cqrList.add(cqr2);	
				
				Core_Quota_Rep__c cqr3 = new Core_Quota_Rep__c();
				cqr3.Name = 'TestCQR3';
				cqr3.Batch_ID__c=10;
		        cqr3.Badge_ID__c = 'D61234';
		        cqr3.Account_ID__c = cqrAccountList[1].Id;
		        cqr3.District_Name__c = cqrAccountList[1].District_Lookup__c;
		        cqr3.Oracle_District_ID__c =  '11111111';
		        cqr3.SFDC_User_ID__c =  insertUser.Id;
		        cqrList.add(cqr3);
		        
		        Core_Quota_Rep__c cqr4 = new Core_Quota_Rep__c();
				cqr4.Name = 'TestCQR4';
				cqr4.Batch_ID__c=10;
		        cqr4.Badge_ID__c = 'D1235';
		        cqr4.Account_ID__c = cqrAccountList[2].Id;
		        cqr4.District_Name__c = cqrAccountList[2].District_Lookup__c;
		        cqr4.Oracle_District_ID__c =  '11111111';
		        cqrList.add(cqr4);
			}else{
				Core_Quota_Rep__c cqr2 = new Core_Quota_Rep__c();
				cqr2.Name = 'TestCQR2';
				cqr2.Batch_ID__c = 10;
				cqr2.Badge_ID__c = '111111';
				cqr2.Account_ID__c = cqrAccountList[1].Id;
				cqr2.District_Name__c = cqrAccountList[1].District_Lookup__c;
				cqr2.Oracle_District_ID__c =  dlLists[0].Oracle_District_ID__c;
				cqr2.SFDC_User_ID__c =  insertUser.Id;
				cqrList.add(cqr2);
				
				Core_Quota_Rep__c cqr5 = new Core_Quota_Rep__c();
				cqr5.Name = 'TestCQR4';
				cqr5.Batch_ID__c=10;
		        cqr5.Badge_ID__c = '11235';
		        cqr5.Account_ID__c = cqrAccountList[2].Id;
		        cqr5.District_Name__c = cqrAccountList[2].District_Lookup__c;
		        cqr5.Oracle_District_ID__c =  '11111111';
		        cqrList.add(cqr5);
			}
			
	        Core_Quota_Rep__c cqr5 = new Core_Quota_Rep__c();
			cqr5.Name = 'TestCQR4';
			cqr5.Batch_ID__c=10;
	        cqr5.Badge_ID__c = '612345';
	        cqr5.Account_ID__c = cqrAccountList[2].Id;
	        cqr5.District_Name__c = cqrAccountList[2].District_Lookup__c;
	        cqr5.Oracle_District_ID__c =  dlLists[0].Oracle_District_ID__c;
	        cqr5.SFDC_User_ID__c =  insertUser.Id;
	        cqrList.add(cqr5);
		}
		insert cqrList;
	}
	
	public static void setupJobScheduler(){
		if(js == null){
			Datetime sysTime = System.now();
		    js = new Job_Scheduler__c();
	        js.Name = 'Test';
	        js.Operations__c = 'Test';
	        js.Start_Date__c = date.today();
	        js.Account_Locking__c=true;
			js.status__c= 'Pending';
	        js.Schedule_Hour__c = sysTime.hour();
	        js.Minutes__c = sysTime.minute();
	        insert js;
		}
	}
}