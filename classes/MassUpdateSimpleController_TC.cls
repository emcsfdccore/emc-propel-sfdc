/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE       DEVELOPER       WR       DESCRIPTION                                
|
   ====       =========       ==       ===========  
|  03-Apr-2013  Prachi                 Created 
|  19-Mar-2014  Bisna V P    CI:445    Updated to cover Opportunity Owner field
|  28-Jan-2015  Karan Shekhar          Updated code with best practices under platform innovation project
+===========================================================================*/ 
@isTest
private class MassUpdateSimpleController_TC {
    public static List <Lead> leadLst= new List <Lead> ();
    public static List <Lead> leadLst1= new List <Lead> ();
    public static List <Lead> leadObj1= new List<Lead>();
    public static List <Opportunity> oppList= new List <Opportunity> ();
    public static List <Opportunity> oppList1= new List <Opportunity> ();       
    public static List <Account> accList= new List<Account>();
    static testMethod void MassUpdateSimpleControllerTest(){ 
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1]; 
        System.runAs(insertUser){
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
            CustomSettingDataHelper.adminConversionCSData();
            CustomSettingDataHelper.houseAccountCSData();
            CustomSettingDataHelper.specialForecastGroupsData();
            CustomSettingDataHelper.manageMassReassignmentData();
            CustomSettingDataHelper.massUpdateProfilesAccessCSData();

        } 
        //create Account data 
        accList=AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        if(!accList.isEmpty()) {            
            insert accList;
        }
        //create Lead data 
        leadLst=LeadTestClassDataHelper.createLead();
        if(!leadLst.isEmpty()) {
            
            insert leadLst;
        }
        //create Opportunity data 
        oppList=OpportunityTestClassDataHelper.createOpptys(accList[0],leadLst[0]);
        if(!oppList.isEmpty()) {
            
            insert oppList;
        }   
        Test.StartTest();                   
        leadLst1=[Select id from Lead where id IN :leadLst];
        oppList1=[Select id from Opportunity where Id IN :oppList];
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(leadLst1);
        sc.setSelected(leadLst1);
        MassUpdateSimpleController objC = new MassUpdateSimpleController(sc);    
        objC.restrictPermission();
        objC.getStep();
        objC.getsType();
        objC.getRecordSize();
        objC.setFlag (false);
        objC.cancel();
        objC.getFlag();
        objC.getFlag1();
        objC.setFlag1(false);
        objC.getFieldInfoToDisplay();
        objC.step1();
        objC.step2();
        objC.step3();
        objC.getFieldTypeOptions();
        objC.fieldName='Name';
        System.debug('objC.getFieldTypeOptions--->'+objC.getFieldTypeOptions());
        objC.step4();
        objC.step5();            
        objC.gettitle();
        objC.showOk();
        objC.getpreviousFlag();
        objC.getcancelFlag();
        objC.getokFlag();
        objC.settitle('title');
        objC.getNow(true);  
        //CI 445 updates
        ApexPages.StandardSetController scOpp = new ApexPages.StandardSetController(oppList1);
        scOpp.setSelected(oppList1);
        MassUpdateSimpleController objCOpp = new MassUpdateSimpleController(scOpp);    
        objCOpp.restrictPermission();
        objCOpp.getStep();
        objCOpp.getsType();
        objCOpp.getRecordSize();
        objCOpp.setFlag (false);
        objCOpp.cancel();
        objCOpp.getFlag();
        objCOpp.getFlag1();
        objCOpp.setFlag1(false);
        objCOpp.getFieldInfoToDisplay();
        objCOpp.step1();
        objCOpp.title();
        objCOpp.step2();
        objCOpp.title();
        objCOpp.step3();
        objCOpp.getFieldTypeOptions();
        objCOpp.title();
        objCOpp.fieldName='Deal_Lead__c';
        System.debug('objCOpp.getFieldTypeOptions--->'+objCOpp.getFieldTypeOptions());
       // objCOpp.step4();
        objCOpp.step5();            
        objCOpp.gettitle();
        objCOpp.showOk();
        objCOpp.getpreviousFlag();
        objCOpp.getcancelFlag();
        objCOpp.getokFlag();
        objCOpp.settitle('title');
        objCOpp.getNow(true);  
        //CI 445 update ends here    
        ApexPages.StandardSetController sc1 = new ApexPages.StandardSetController(leadObj1);
        MassUpdateSimpleController objC1 = new MassUpdateSimpleController(sc1);
        objC1.getRecordSize();
        objC1.getNow(false);
        //new conditions
        oppList1.clear();
        oppList1=[Select id from Opportunity where Id IN :oppList];
        ApexPages.StandardSetController scOpp1 = new ApexPages.StandardSetController(oppList1);
        scOpp1.setSelected(oppList1);
        MassUpdateSimpleController objCOpp1 = new MassUpdateSimpleController(scOpp1);    
        objCOpp1.restrictPermission();
        objCOpp1.getStep();
        objCOpp1.getsType();
        objCOpp1.getRecordSize();
        objCOpp1.setFlag (false);
        objCOpp1.cancel();
        objCOpp1.getFlag();
        objCOpp1.getFlag1();
        objCOpp1.setFlag1(false);
        objCOpp1.getFieldInfoToDisplay();
        objCOpp1.step1();
        objCOpp1.title();
        objCOpp1.step2();
        objCOpp1.title();
        objCOpp1.step3();
        objCOpp1.getFieldTypeOptions();
        objCOpp1.title();
        objCOpp1.fieldName='Agreements__c';
        System.debug('objCOpp1.getFieldTypeOptions--->'+objCOpp1.getFieldTypeOptions());
        objCOpp1.step4();
        objCOpp1.step5();            
        objCOpp1.gettitle();
        objCOpp1.showOk();
        objCOpp1.getpreviousFlag();
        objCOpp1.getcancelFlag();
        objCOpp1.getokFlag();
        objCOpp1.settitle('title');
        objCOpp1.getNow(true);  
        oppList1.clear();
        oppList1=[Select id from Opportunity where Id IN :oppList];
        ApexPages.StandardSetController scOpp2 = new ApexPages.StandardSetController(oppList1);
        scOpp2.setSelected(oppList1);
        MassUpdateSimpleController objCOpp2 = new MassUpdateSimpleController(scOpp2);    
        objCOpp2.restrictPermission();
        objCOpp2.getStep();
        objCOpp2.getsType();
        objCOpp2.getRecordSize();
        objCOpp2.setFlag (false);
        objCOpp2.cancel();
        objCOpp2.getFlag();
        objCOpp2.getFlag1();
        objCOpp2.setFlag1(false);
        objCOpp2.getFieldInfoToDisplay();
        objCOpp2.step1();
        objCOpp2.title();
        objCOpp2.step2();
        objCOpp2.title();
        objCOpp2.step3();
        objCOpp2.getFieldTypeOptions();
        objCOpp2.title();
        objCOpp2.fieldName='Amount';
        System.debug('objCOpp2.getFieldTypeOptions--->'+objCOpp2.getFieldTypeOptions());
        objCOpp2.step4();
        objCOpp2.step5();            
        objCOpp2.gettitle();
        objCOpp2.showOk();
        objCOpp2.getpreviousFlag();
        objCOpp2.getcancelFlag();
        objCOpp2.getokFlag();
        objCOpp2.settitle('title');
        objCOpp2.getNow(true); 
        oppList1.clear();
        oppList1=[Select id from Opportunity where Id IN :oppList];
        ApexPages.StandardSetController scOpp3 = new ApexPages.StandardSetController(oppList1);
        scOpp3.setSelected(oppList1);
        MassUpdateSimpleController objCOpp3 = new MassUpdateSimpleController(scOpp3);    
        objCOpp3.restrictPermission();
        objCOpp3.getStep();
        objCOpp3.getsType();
        objCOpp3.getRecordSize();
        objCOpp3.setFlag (false);
        objCOpp3.cancel();
        objCOpp3.getFlag();
        objCOpp3.getFlag1();
        objCOpp3.setFlag1(false);
        objCOpp3.getFieldInfoToDisplay();
        objCOpp3.step1();
        objCOpp3.title();
        objCOpp3.step2();
        objCOpp3.title();
        objCOpp3.step3();
        objCOpp3.getFieldTypeOptions();
        objCOpp3.title();
        objCOpp3.fieldName='Booking_Certificate__c';
        System.debug('objCOpp3.getFieldTypeOptions--->'+objCOpp3.getFieldTypeOptions());
        objCOpp3.step4();
        objCOpp3.step5();            
        objCOpp3.gettitle();
        objCOpp3.showOk();
        objCOpp3.getpreviousFlag();
        objCOpp3.getcancelFlag();
        objCOpp3.getokFlag();
        objCOpp3.settitle('title');
        objCOpp3.getNow(true);
        oppList1.clear();
        oppList1=[Select id from Opportunity where Id IN :oppList];
        ApexPages.StandardSetController scOpp4 = new ApexPages.StandardSetController(oppList1);
        scOpp4.setSelected(oppList1);
        MassUpdateSimpleController objCOpp4 = new MassUpdateSimpleController(scOpp4);    
        objCOpp4.restrictPermission();
        objCOpp4.getStep();
        objCOpp4.getsType();
        objCOpp4.getRecordSize();
        objCOpp4.setFlag (false);
        objCOpp4.cancel();
        objCOpp4.getFlag();
        objCOpp4.getFlag1();
        objCOpp4.setFlag1(false);
        objCOpp4.getFieldInfoToDisplay();
        objCOpp4.step1();
        objCOpp4.title();
        objCOpp4.step2();
        objCOpp4.title();
        objCOpp4.step3();
        objCOpp4.getFieldTypeOptions();
        objCOpp4.title();
        objCOpp4.fieldName='Assignment_date__c';
        System.debug('objCOpp4.getFieldTypeOptions--->'+objCOpp4.getFieldTypeOptions());
        objCOpp4.step4();
        objCOpp4.step5();            
        objCOpp4.gettitle();
        objCOpp4.showOk();
        objCOpp4.getpreviousFlag();
        objCOpp4.getcancelFlag();
        objCOpp4.getokFlag();
        objCOpp4.settitle('title');
        objCOpp4.getNow(true); 
        oppList1.clear();
        oppList1=[Select id from Opportunity where Id IN :oppList];
        ApexPages.StandardSetController scOpp5 = new ApexPages.StandardSetController(oppList1);
        scOpp5.setSelected(oppList1);
        MassUpdateSimpleController objCOpp5 = new MassUpdateSimpleController(scOpp5);    
        objCOpp5.restrictPermission();
        objCOpp5.getStep();
        objCOpp5.getsType();
        objCOpp5.getRecordSize();
        objCOpp5.setFlag (false);
        objCOpp5.cancel();
        objCOpp5.getFlag();
        objCOpp5.getFlag1();
        objCOpp5.setFlag1(false);
        objCOpp5.getFieldInfoToDisplay();
        objCOpp5.step1();
        objCOpp5.title();
        objCOpp5.step2();
        objCOpp5.title();
        objCOpp5.step3();
        objCOpp5.getFieldTypeOptions();
        objCOpp5.title();
        objCOpp5.fieldName='CloseDate';
        System.debug('objCOpp5.getFieldTypeOptions--->'+objCOpp5.getFieldTypeOptions());
        objCOpp5.step4();
        objCOpp5.step5();            
        objCOpp5.gettitle();
        objCOpp5.showOk();
        objCOpp5.getpreviousFlag();
        objCOpp5.getcancelFlag();
        objCOpp5.getokFlag();
        objCOpp5.settitle('title');
        objCOpp5.getNow(true); 
        oppList1.clear();
        oppList1=[Select id from Opportunity where Id IN :oppList];
        ApexPages.StandardSetController scOpp6 = new ApexPages.StandardSetController(oppList1);
        scOpp6.setSelected(oppList1);
        MassUpdateSimpleController objCOpp6 = new MassUpdateSimpleController(scOpp6);    
        objCOpp6.restrictPermission();
        objCOpp6.getStep();
        objCOpp6.getsType();
        objCOpp6.getRecordSize();
        objCOpp6.setFlag (false);
        objCOpp6.cancel();
        objCOpp6.getFlag();
        objCOpp6.getFlag1();
        objCOpp6.setFlag1(false);
        objCOpp6.getFieldInfoToDisplay();
        objCOpp6.step1();
        objCOpp6.title();
        objCOpp6.step2();
        objCOpp6.title();
        objCOpp6.step3();
        objCOpp6.getFieldTypeOptions();
        objCOpp6.title();
        objCOpp6.fieldName='StageName';
        System.debug('objCOpp6.getFieldTypeOptions--->'+objCOpp6.getFieldTypeOptions());
        objCOpp6.step4();
        objCOpp6.step5();            
        objCOpp6.gettitle();
        objCOpp6.showOk();
        objCOpp6.getpreviousFlag();
        objCOpp6.getcancelFlag();
        objCOpp6.getokFlag();
        objCOpp6.settitle('title');
        objCOpp6.getNow(true);      
        Test.StopTest();
    }

}