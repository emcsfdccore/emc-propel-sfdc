/**
===========================================================================+
 |  Created History                                                                  
                                                                 
 |  DATE        DEVELOPER       WORK REQUEST            DESCRIPTION                               
 |  ====        =========       ============            =========== 
 |  4 Mar 2014  Avinash K       Oppty Collaboration     This class is a custom controller to the VF page the lists Partner users for 
                                                        Auxiliary sharing of Opportunities.
                                                        This class lists the active Partner Users from the Distribution VAR & Distrbutor/Direct 
                                                        Reseller accounts of the Opportunty for Auxiliary sharing
=============================================================================+ 
**/


public class OPPTY_COLLABORATION_AddPartnerController 
{
    public Opportunity objOpp {get;set;}
    private Map<ID,String> mapIDPartnerAccount = new Map<Id,String>();
    private Map<Id,Contact> mapIdContact = new Map<Id,Contact>();
    private List<User> lstPartnerUser = new List<User>();
    private Map<Id, OpportunityTeamMember> mapIdExistingPartnerOTM = new Map<Id, OpportunityTeamMember>();
    private Set<Id> setUserIdExistingPartnerOTM = new set<Id>();
    public OPPTY_COLLABORATION_CustomIterable objCustomIterable {get;set;}
    public list<OPPTY_COLLABORATION_InnerUser> lstInnerUsersToShow {get;set;}
    private Map<String,CustomSettingDataValueMap__c> mapData = CustomSettingDataValueMap__c.getAll();

/**
Constructor
Gets the active Partner Users from the Opportunity's related Distribution VAR & Distributor/Direct Reseller accounts and calls the Custom Iterable class' constructor to build a list of non existing Sales Team Partner Users (i.e. wrapper class records) so that they are displayed with checkboxes on VF pages so that they can be selected via checkboxes for Auxiliary sharing of the opportunity
**/
    public OPPTY_COLLABORATION_AddPartnerController(ApexPages.StandardController controller) 
    {
        
        objOpp = (Opportunity)controller.getRecord();

        List<Opportunity> lstOpp = [SELECT id, name, Tier_2_Partner__c,Tier_2_Partner__r.Name, Partner__c,Partner__r.Name, Partner_Approval_Status__c, Linked_to_Deal_Registration__c, Approving_Partner_User1__c, Secondary_Partner_User1__c
                                        FROM Opportunity 
                                        WHERE id = :objOpp.id 
                                        LIMIT 1];
        
        if(lstOpp != null && lstOpp.size() > 0)
        {
            objOpp = lstOpp.get(0);
        }

        if(objOpp != null && (objOpp.Tier_2_Partner__c != null || objOpp.Partner__c != null))
        {
            if(objOpp.Tier_2_Partner__c != null)
            {
                mapIDPartnerAccount.put(objOpp.Tier_2_Partner__c, objOpp.Tier_2_Partner__r.Name);
            }    

            if(objOpp.Partner__c != null)
            {
                mapIDPartnerAccount.put(objOpp.Partner__c, objOpp.Partner__r.Name);
            }

            mapIdContact = new Map<Id,Contact> ([select name, id, Account.Grouping__c, Partner_Contact__c, Partner_User2__c
                                    FROM Contact
                                    WHERE Partner_Contact__c = 'True' AND Partner_User2__c = true AND Account.IsPartner = true AND Account.Id in :mapIDPartnerAccount.keyset()
                                    LIMIT 50000]);

            lstPartnerUser = [select Name, id, AccountId, Partner_Contact__c, email, MobilePhone, Phone
                                    FROM User
                                    WHERE AccountId in:mapIDPartnerAccount.keyset() AND Partner_Contact__c in :mapIdContact.keyset() AND isActive = true
                                    LIMIT 50000];

            mapIdExistingPartnerOTM = new Map<Id,OpportunityTeamMember>([select id, OpportunityId, UserId
                                                            FROM OpportunityTeamMember
                                                            WHERE OpportunityId = :objOpp.id  AND User.UserType = 'PowerPartner'
                                                            LIMIT 50000]);

            for (OpportunityTeamMember otm : mapIdExistingPartnerOTM.values())
            {
                setUserIdExistingPartnerOTM.add(otm.UserId);
            }
        }

        objCustomIterable = new OPPTY_COLLABORATION_CustomIterable (lstPartnerUser, mapIDPartnerAccount, setUserIdExistingPartnerOTM); 
        
        if(mapData != null && mapData.get('PartnerUserPageSize') != null && 
              mapData.get('PartnerUserPageSize').Datavalue__c != null)
        {
            objCustomIterable.intPageSize = INTEGER.valueOf(mapData.get('PartnerUserPageSize').Datavalue__c);
        }

        else
        {
            objCustomIterable.intPageSize = 10;
        }
        
        next();
    }
    
    public Boolean hasNext 
    {
        get 
        {
            return objCustomIterable.hasNext();
        }
        set;
    }
    
    public Boolean hasPrevious 
    {
        get 
        {
            return objCustomIterable.hasPrevious();
        }
        set;
    }
    

/**
next()
Returns a list of Partner Users (wrapper class records) to be shown initially and when the user clicks on "Next" button by calling Custom Iterable class method
**/
    public void next() 
    {
        lstInnerUsersToShow = objCustomIterable.next();
    }
    

/**
previous()
Returns a list of Partner Users (wrapper class records) to be shown when the user clicks on "Previous" button by calling Custom Iterable class method
**/
    public void previous() 
    {
        lstInnerUsersToShow = objCustomIterable.previous();
    }

    
/**
updateSalesTeam()
Inserts the selected partner users (via checkbox) to the Opportunity Sales Team with Role = 'Partner User' by calling Custom Iterable class method and passing Opportunity and Set of Sales Team Partner User Ids as arguments
**/

    public void updateSalesTeam()
    {
        objCustomIterable.updateSalesTeam(objOpp, setUserIdExistingPartnerOTM);
    }
}