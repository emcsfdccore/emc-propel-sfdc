@isTest
    public class cmc_WebServiceMockImpl implements WebServiceMock 
    {
       public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType) 
        {
            if(request instanceof emcComItEnterpriseDataV1.GetDataLookUpRequestType)
            {
            emcComItEnterpriseDataV1.GetDataLookUpResponseType respElement= new emcComItEnterpriseDataV1.GetDataLookUpResponseType();
            List<emcComItEnterpriseDataV1.CMCContractDataType> lstCMCContData = new List<emcComItEnterpriseDataV1.CMCContractDataType>();
            emcComItEnterpriseDataV1.CMCContractDataType cmcContData = new emcComItEnterpriseDataV1.CMCContractDataType();
            cmcContData.CMCContractID = '13712638';
            for(integer i=0;i<200;i++) {
                lstCMCContData.add(cmcContData);
            }
            respElement.ContractData = lstCMCContData;
              System.debug('**********respElement:'+respElement);
            response.put('response_x', respElement);
        }
            else if(request instanceof emcComItEnterpriseDataV1.GetVirtualLinkLookUpRequestType)
            {
                emcComItEnterpriseDataV1.GetVirtualLinkLookUpResponseType virtualLinkResponseType = new emcComItEnterpriseDataV1.GetVirtualLinkLookUpResponseType();
                emcComItEnterpriseDataV1.GetVirtualLinkLookUpResponseDocument_element virLinkResp = new emcComItEnterpriseDataV1.GetVirtualLinkLookUpResponseDocument_element();
                virLinkResp.VirtualLinkURL='viewVirualLinkPage';
                virtualLinkResponseType.GetVirtualLinkLookUpResponseDocument= virLinkResp;
                System.debug('************virtualLinkResponseType:'+virtualLinkResponseType);
                response.put('response_x', virtualLinkResponseType);   
            }
            
          else if(request instanceof emcComItEnterpriseDataV1.GetMetaDataLookUpRequestType)
            {
               emcComItEnterpriseDataV1.GetMetaDataLookUpResponseType metaDataRespElement= new emcComItEnterpriseDataV1.GetMetaDataLookUpResponseType();
                                             
         emcComItEnterpriseDataV1.ContractType Con= new emcComItEnterpriseDataV1.ContractType();
         emcComItEnterpriseDataV1.FundingFeeType Fun= new emcComItEnterpriseDataV1.FundingFeeType();
         emcComItEnterpriseDataV1.PartnerDetailsType Channel= new emcComItEnterpriseDataV1.PartnerDetailsType();
         emcComItEnterpriseDataV1.CustomerOrderType Cust= new emcComItEnterpriseDataV1.CustomerOrderType();
         emcComItEnterpriseDataV1.MasterContractType Master= new emcComItEnterpriseDataV1.MasterContractType();
         emcComItEnterpriseDataV1.ConcessionsType Conces= new emcComItEnterpriseDataV1.ConcessionsType();
         emcComItEnterpriseDataV1.OrderEgreementType Order= new emcComItEnterpriseDataV1.OrderEgreementType();
         emcComItEnterpriseDataV1.GovtContractType Gover= new emcComItEnterpriseDataV1.GovtContractType();
         emcComItEnterpriseDataV1.ContractTermsType Contr= new emcComItEnterpriseDataV1.ContractTermsType();
         emcComItEnterpriseDataV1.DocumentDetailsType Doc= new emcComItEnterpriseDataV1.DocumentDetailsType();
         emcComItEnterpriseDataV1.ContractClauseType Contrcls=new emcComItEnterpriseDataV1.ContractClauseType();
         emcComItEnterpriseDataV1.BusinessEntityType Business= new emcComItEnterpriseDataV1.BusinessEntityType();
         emcComItEnterpriseDataV1.MaintenanceDetailType Main= new emcComItEnterpriseDataV1.MaintenanceDetailType();
         emcComItEnterpriseDataV1.ContractCustomerEntityType CustEnt= new emcComItEnterpriseDataV1.ContractCustomerEntityType();
         emcComItEnterpriseDataV1.ContractRequirementType Req=new emcComItEnterpriseDataV1.ContractRequirementType();
                
		Con.EffectiveDate='2000-02-30';
                Con.ExpirationDate='2010-02-30';
                Con.ID='3245178';
                Fun.FundingFee='45000';
                Channel.OfficerName= 'vbjhfdj';
                Cust.StartDate='08/06/2011';
                Master.StartDate='08/06/2011';
                Conces.Description='bafsdak';
                Order.AgreementNo='35425667';
                Gover.ContractName='hfahfna';
                Contr.Acceptance='jsvblj';
                Doc.Name='vnolsd';
                Contrcls.FavoredCustomerPricing=false;
                Business.RiskOfLoss='deliver';
                Main.CoverageForHardware='odifjksdj';
                CustEnt.PartyNumber='345345';
                Req.ActionRequired='frifka'; 

           emcComItEnterpriseDataV1.MetaDataType cmcContMetaData = new emcComItEnterpriseDataV1.MetaDataType();
                cmcContMetaData.Contract = Con;
                cmcContMetaData.FundingFee = Fun;
                cmcContMetaData.ChannelPartner = Channel;
                cmcContMetaData.CustomerOrder = Cust;
                cmcContMetaData.MasterContract = Master;
                cmcContMetaData.Concessions = Conces;
                cmcContMetaData.OrderEgreement = Order;
                cmcContMetaData.GovernmentContract = Gover;
                cmcContMetaData.ContractTerms = Contr;
                cmcContMetaData.DocumentDetails = Doc;
                cmcContMetaData.ContractClause = Contrcls;
                cmcContMetaData.BusinessEntity = Business;
                cmcContMetaData.Maintenance = Main;
                cmcContMetaData.CustomerEntity = CustEnt;
                cmcContMetaData.Requirement = Req;
                
                List<emcComItEnterpriseDataV1.MetaDataType> lstMetaDataType = new List<emcComItEnterpriseDataV1.MetaDataType>();
                lstMetaDataType.add(cmcContMetaData);
                metaDataRespElement.MetaData = lstMetaDataType;
                System.debug('*******metaDataRespElement:'+metaDataRespElement);
                response.put('response_x',metaDataRespElement);
                
            } 
    }
}