@istest(SeeAllData = True)

public class PSC_CaseVisibilitySharing_TC
{
    public static testmethod void PSCVisShare()
    {
        
        /*System.runAs(new user(Id = UserInfo.getUserId()))
            //{    

                CustomSettingDataHelper.PSCFieldMappingData();
           //}*/
          
        Group testGroup = new Group();
        testGroup.Name = 'testGroup';
        testGroup.DeveloperName = 'PSC_Internal_Channel_Users1';
        INSERT testGroup;
        
        
        // Insert user
        List<User> LstUsr = new List<User>();
        List<User> LstUsr1 = new List<User>();
        
        Profile Prof = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile Prof1 = [SELECT Id FROM Profile WHERE Name = 'EMC System Admin'];
        LstUsr.add(new User(Alias = 'standt', isActive= True, Email='test@test.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = prof.Id,TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorgPSC.com'));
        LstUsr.add(new User(Alias = 'standt1',  Email='test1@test.com',EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = prof.Id,TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorgPSC1.com'));
        LstUsr1.add(new User(Alias = 'standt2',  Email='test2@test.com',EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = prof1.Id,TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorgPSC2.com'));
        LstUsr1.add(new User(Alias = 'standt3',  Email='test3@test.com',EmailEncodingKey='UTF-8', LastName='Testing3', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = prof1.Id,TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorgPSC3.com'));
        
        insert Lstusr;
        insert LstUsr1;

        System.runAs(Lstusr[0]){
         PSC_CaseVisibilitySharing pscCaseSharingObj1 = new PSC_CaseVisibilitySharing();
    pscCaseSharingObj1.userAssignmentToPSCGroup(Lstusr,LstUsr1);
            System.debug(LstUsr[0].ProfileId);
            System.debug(LstUsr1[0].ProfileId);
            system.assertEquals(LstUsr[0].IsActive, True);
            system.assertEquals(LstUsr[0].ProfileId != LstUsr1[0].ProfileId, true);
            
        }
    }
}