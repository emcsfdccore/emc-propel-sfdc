/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  23/04/2015      Partha Baruah   RMA May,15      This Class handles all Case RMA Tracking Statuses operation and value derivation. 
 +=======================================================================================================================*/
@isTest
public class CaseRMA_Operations_TC{

    static testMethod void processRMA()
    {
        System.runAs(new user(Id = UserInfo.getUserId())){
        CustomSettingDataHelper.dataValueMapCSData();
        CustomSettingDataHelper.bypassLogicCSData();
        CustomSettingDataHelper.rmaTrackingData();
        CustomSettingDataHelper.RMAIROPublicGroupData();
        
        }
        Group testRTT = new Group(Name='RTT Queue', type='Queue');
        insert testRTT;
        Group testIRO = new Group(Name='IRO Queue', type='Queue');
        insert testIRO;
        List <QueueSObject> testRMA= new List<QueueSObject>();
        testRMA.Add(new QueueSObject( QueueID = testIRO.id, SobjectType = 'Case_RMA__c'));
        testRMA.Add(new QueueSObject(QueueID = testRTT.id, SobjectType = 'Case_RMA__c'));
        insert testRMA;
        
        
        //Insert user record where Profile is Sys Admin and Forecast Group is Maintenance Renewals
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        ID EMCROid = [ Select id from Profile where name ='Read Only' limit 1].Id;
        List <User> insertUser = new List<User>();
        
        insertUser.add(new user(email='tes12ser@emailTest.com',profileId = sysid ,  UserName='tes12ass1@emaias12Test.com', alias='as12er1', CommunityNickName='tuassr1', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' ,Forecast_Group__c = 'Maintenance Renewals')); 
        
        insertUser.Add(new User(email='test-user23@emailTest.com',profileId = EMCROid ,  UserName='te2asd1@emaiRTTO.com', alias='asdR12r1', CommunityNickName='tu2aas3er1', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' ,Forecast_Group__c = 'Maintenance Renewals'));
        
        insert insertUser;
        
        system.debug('RmAUsers+++'+insertUser);
        List <PermissionSet> ps = [SELECT Id FROM PermissionSet WHERE Name = 'RMA_Tracking_Case_Access'];
        PermissionSetAssignment newPSA1 = new PermissionSetAssignment(); //PermissionSetAssignment sobject
        PermissionSetAssignment newPSA2 = new PermissionSetAssignment(); //PermissionSetAssignment sobject
        newPSA1.PermissionSetId = ps[0].Id; //set the permission set Id
        newPSA1.AssigneeId = insertUser[0].Id; //set the User Id
        newPSA2.PermissionSetId = ps[0].Id; //set the permission set Id
        newPSA2.AssigneeId = insertUser[1].Id; //set the User Id
        insert newPSA1 ;
        insert newPSA2 ;
        system.debug('+++PS+++'+newPSA1);
        system.debug('+++PS+++'+newPSA2);
       
        //testRTT.OwnerId=insertUser.id;
        //testIRO.AssigneeId=insertUser.id;
        //update testRTT;
        //update testIRO;
        
      System.runAs(insertUser[0])
      {
        test.startTest();
        
        //Create RMA Tracking records.
            list<Case_RMA__c> listRMA = new list<Case_RMA__c>();
            listRMA.add(new Case_RMA__c(RMA_Number__c = '1234', RMA_Case_Status__c = 'RMA Creation Pending'));
            listRMA.add(new Case_RMA__c(RMA_Number__c = '12345', RMA_Case_Status__c = 'Resolved' , Balance_left_in_Inventory__c = 123));
          //listRMA.add(new Case_RMA__c(RMA_Number__c = '1234', RMA_Case_Status__c = 'Closed'));
          //ListRMA.add(new Case_RMA__c(RMA_Number__c = '123', RMA_Case_Status__c = 'In Reconciliation'));
            insert listRMA ;
            
            // create SLA Tracking record.
            list<Cases_RMA_Sla_Tracking__c> listRMASlaTrack = new list<Cases_RMA_Sla_Tracking__c>();
            listRMASlaTrack.add(new Cases_RMA_Sla_Tracking__c(Cases_RMA__c = listRMA[0].id));
            listRMASlaTrack.add(new Cases_RMA_Sla_Tracking__c(Cases_RMA__c = listRMA[0].id , RMA_Case_End_Time__c = null));
            listRMASlaTrack.add(new Cases_RMA_Sla_Tracking__c(Cases_RMA__c = listRMA[1].id ));
            insert listRMASlaTrack;
            
            listRMA[1].RMA_Case_Status__c = 'In Reconciliation' ;
            listRMA[1].ownerId=testIRO.id ;
            update listRMA;
            listRMA[1].RMA_Case_Status__c = 'Closed' ;
            listRMA[1].ownerId=insertUser[1].id;
            update listRMA;
        test.stopTest(); 
      }
      
   /*   System.runAs(insertUser[1])
      {
        //Create RMA Tracking records.
            list<Case_RMA__c> listRMA = new list<Case_RMA__c>();
            listRMA.add(new Case_RMA__c(RMA_Number__c = '1234', RMA_Case_Status__c = 'RMA Creation Pending'));
            listRMA.add(new Case_RMA__c(RMA_Number__c = '12345', RMA_Case_Status__c = 'Resolved' , Balance_left_in_Inventory__c = 123));
            insert listRMA ;
            
            // create SLA Tracking record.
            list<Cases_RMA_Sla_Tracking__c> listRMASlaTrack = new list<Cases_RMA_Sla_Tracking__c>();
            listRMASlaTrack.add(new Cases_RMA_Sla_Tracking__c(Cases_RMA__c = listRMA[0].id));
            insert listRMASlaTrack;
            listRMA[1].RMA_Case_Status__c = 'In Reconciliation' ;
            listRMA[1].ownerId=testIRO.id ;
            update listRMA;
        
      }*/
    }
}