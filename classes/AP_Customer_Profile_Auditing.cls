/*================================================================================================================================================+
 |  HISTORY  |
 |  DATE          DEVELOPER               WR                        DESCRIPTION 
 |  ====          =========               ==                        =========== 
 |  01 Dec 2012   Hemavathi N M         Account Presentation         This will used for running batch job on customer profiled account   
 |  10 Nov 2014   Anand Sharma          Inactive Account 
 |  21 Nov 2014   Vivek Barange         875                          Modified record limit to 5 in test query 
 |  11 Feb 2014   Vivek Barange          #1751                       Modified code to restrict inactive to become CPA 
 |  17 Feb 2014   Vivek Barange          #1752                       Added filter in query where Protect_this_party_as_CPA__c = true
+===========================================================================================================================================================*/
 
global class AP_Customer_Profile_Auditing implements Database.Batchable<sObject>  {

  //Holds todays date. 
  global Date currentDate=date.today();
  public String accountQuery ;
  public Boolean isRunTest=false;
  /* constructor */
  global Database.QueryLocator start(Database.BatchableContext BC)
  {
       Map<String,CustomSettingDataValueMap__c> mapDataValueMap = CustomSettingDataValueMap__c.getAll();
       string setHouseAccountExcluded; 
       if(mapDataValueMap!= null &&
       mapDataValueMap.get('House Account For AP Exclusion') != null &&
       mapDataValueMap.get('House Account For AP Exclusion').DataValue__c != null)
       {
            system.debug('#### Entered batch calling area');
            Integer countStr=0;
            String strHouseAccuontsExcluded = mapDataValueMap.get('House Account For AP Exclusion').DataValue__c;
            for(string s:strHouseAccuontsExcluded.split(','))
            {
                if(countStr==0)
                {
                    setHouseAccountExcluded='(\''+s+'\'';
                    countStr++;
                }
                else if(countStr>0)
                {
                    setHouseAccountExcluded=setHouseAccountExcluded+',\''+s+'\'';
                }
                
            }
            if(setHouseAccountExcluded.length()>0)
            {
                setHouseAccountExcluded=setHouseAccountExcluded+')';
                system.debug('setHouseAccountExcluded==>'+setHouseAccountExcluded);
            }
       }  
       try 
       {
           if(Test.IsRunningTest()){
            //#875 - Modified record limit to 5 in test query
            //1752 - Added filter in query where Protect_this_party_as_CPA__c = true
            accountQuery ='Select ID, Name, Account_District__c,District_Lookup__c,District_Lookup__r.name, Customer_Profiled_Account__c,Account_Status__c, Account_Profiling_Warning_Counts__c From Account WHERE type != \'Partner\' and (Customer_Profiled_Account__c = true OR Protect_this_party_as_CPA__c = true) and isPartner = false and Account_District__c != null and (NOT District_Lookup__r.name  in ' + setHouseAccountExcluded + ') limit 5';
           }
           else{
                accountQuery ='Select ID, Name, Account_District__c,District_Lookup__c,District_Lookup__r.name, Customer_Profiled_Account__c,Account_Status__c, Account_Profiling_Warning_Counts__c From Account WHERE type != \'Partner\' and (Customer_Profiled_Account__c = true OR Protect_this_party_as_CPA__c = true) and isPartner = false and Account_District__c != null and (Not District_Lookup__r.name  in ' + setHouseAccountExcluded + ')';
           }
           
       system.debug('accountQuery===> '+accountQuery);
       }
       catch (Exception e) 
       {
            System.debug('There are no jobs currently scheduled. ' + e.getMessage()); 
       } 
       return Database.getQueryLocator(accountQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        List<Account> AccountList =(Account[]) scope;
        system.debug('AccountList--->'+AccountList );
        // Exception Handling.
        List <EMCException> errors = new List <EMCException>();
        Set<String> masterListTracker = new Set<String>();
        List<Customer_Profile_Audit_Master__c> masterList = new List<Customer_Profile_Audit_Master__c>();
        List<Customer_Profile_Audit_Detail__c> detailList = new List<Customer_Profile_Audit_Detail__c>();
        Map<Id,Account> AccountMapUpdate = new Map<Id,Account>();
        for(Account acct : AccountList){
           // create Master Rec for use as Detail Rec parent reference
           Customer_Profile_Audit_Master__c masterRec = new Customer_Profile_Audit_Master__c();
              masterRec.Account_District__c = acct.Account_District__c;

           if (masterListTracker.isEmpty() || !masterListTracker.contains(acct.Account_District__c)) 
           {
              // create additional Master rec for Upsert if we have not created one already
              Customer_Profile_Audit_Master__c masterUpsertRec = new Customer_Profile_Audit_Master__c();
              masterUpsertRec.Account_District__c = acct.Account_District__c;
              masterUpsertRec.District_Lookup__c = acct.District_Lookup__c;
              masterUpsertRec.name = acct.Account_District__c.abbreviate(80);
              masterList.add(masterUpsertRec);
              masterListTracker.add(acct.Account_District__c);
           }              
 
           Customer_Profile_Audit_Detail__c detailRec = new Customer_Profile_Audit_Detail__c();
              detailRec.Customer_Profile_Audit_Master__r = masterRec;
              detailRec.name               = acct.name.abbreviate(80);
              detailRec.Account_ID__c      = acct.id;
              detailRec.Account_Lookup__c  = acct.id;
              detailRec.Refresh_Date__c=currentDate;
              detailRec.Warning_Count__c   = acct.Account_Profiling_Warning_Counts__c;
              //if (acct.Customer_Profiled_Account__c == true && acct.Account_Status__c=='Active' ) 
              if(acct.Customer_Profiled_Account__c == true)
              {
                  detailRec.Profile_Value__c = 1;
              } 
              else 
              {
                  detailRec.Profile_Value__c = 0;
              }
              
           detailList.add(detailRec);
           
            // Start- #1751 - Commented below lines
            /*
            if(acct.Customer_Profiled_Account__c == true && acct.Account_Status__c=='Inactive'){
                acct.Customer_Profiled_Account__c = false;
                AccountMapUpdate.put(acct.id, acct);
            }
            */
            // End- #1751

        }
        
     
        if (masterList != null && masterList.size() > 0) 
        {
            List<Database.upsertResult> masterResults = Database.upsert(masterList, Customer_Profile_Audit_Master__c.Account_District__c, false);
        }
        // if (detailList != null && detailList.size() > 0) {    
            List<Database.upsertResult> detailResults = Database.upsert(detailList, Customer_Profile_Audit_Detail__c.Account_ID__c, false);
            Database.update(AccountMapUpdate.values(), false);
        // }
        
        
        

        for (Database.Upsertresult sr : detailResults) 
        {
            String dataErrs = '';
            if (!sr.isSuccess()) 
            {
                // if the particular record did not get updated, we log the data error 
                for (Database.Error err : sr.getErrors()) 
                {
                    dataErrs += err.getMessage();
                }
                errors.add(new EMCException(dataErrs, 'ACCOUNT AUDIT', new String [] {sr.getId()}));
            }
        }
        // log any errors that occurred
        if (errors.size() > 0) 
        { 
            EMC_UTILITY.logErrors(errors);  
        }
    }
    
    global void finish(Database.BatchableContext BC){
        System.debug(LoggingLevel.WARN,'Batch Process 1 Finished');
        //Build the system time of now + 20 seconds to schedule the batch apex.
        Datetime sysTime = System.now();
        sysTime = sysTime.addSeconds(20);
        String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        system.debug(chron_exp);
        //updated by shipra..
        AP_BatchDeleteReportScheduler apBatchDeleteReport = new AP_BatchDeleteReportScheduler();
        //Schedule the next job, and give it the system time so name is unique
        System.schedule('AP_Batch Delete Report' + sysTime.getTime(),chron_exp,apBatchDeleteReport );
        
    }
}