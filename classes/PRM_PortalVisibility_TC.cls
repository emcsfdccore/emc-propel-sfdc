/*===========================================================================+ 

 |  HISTORY  |                                                                            

 |  DATE          DEVELOPER                WR       DESCRIPTION                               

 |  ====          =========                ==       =========== 
 
 |  13/09/2010     Anand Sharma                 Create test class for the unit testing of PortalVisibility class.
 |  14/12/2011      Anil                        Removed Query for fetching Partner and used Custom setting Data Helper
 |  11-JAN-2012     Anil                        Removed role Id
 |  04-DEC-2013    Aagesh                       Modified for Back Arrow Project
 |  22-SEP-2014    Bhanuprkash                  Fixed Mixed DML exception
                                                                          
 +===========================================================================*/

@isTest
private class PRM_PortalVisibility_TC {

    public static testmethod void deployPRM_PortalVisibility(){
    User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
       
      System.runAs(insertUser){
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
        }
        List<Account> acc = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();       
        acc[0].Type='Partner';
        
        System.runAs(insertUser){
           
            insert acc;
           
        }
            Contact cont = UserProfileTestClassDataHelper.createContact();
            cont.AccountId=acc[0].Id;
            System.runAs(insertUser){
            insert cont;
            }
            Profile amerUserProf = [select Id from Profile where Name=: 'EMEA Distributor Super User'];
            User partner = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.id,null,cont.Id);
            System.runAs(insertUser){
            Test.startTest();
            insert partner;
            Test.stopTest();
            }
            //Create Lead Data              
            Lead lead = new Lead(Channel__c='Indirect',Company='Deploy Lead',LastName='Doe',Status ='New', Sales_Force__c='EMC', LeadSource='Manual', Lead_Originator__c='Field',email='doe_john1@salesforce.com');
            lead.OwnerId =partner.Id;
            System.runAs(insertUser){
            insert lead;
            }
        
        Lead lead02 = [Select id , OwnerId, Partner__c, Tier_2_Partner__c from Lead where id=:lead.Id];
        lead02.Tier_2_Partner__c = acc[0].Id;  
        lead02.Partner__c = acc[0].Id;
        Map<Id,Lead> oldLeadMap = new Map<Id,Lead>();
        Map<Id,Lead> newLeadMap = new Map<Id,Lead>();
        //Put leads into map 
        oldLeadMap.put(lead.id,lead);
        newLeadMap.put(lead02.id,lead02);
         
        PRM_PortalVisibility visibilty = new PRM_PortalVisibility();
        //call setVisibility method 
        visibilty.setVisibility(null,oldLeadMap);
        visibilty.setVisibility(oldLeadMap,newLeadMap);
        
    }
      
 }