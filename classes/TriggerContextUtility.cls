/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  15.04.2014      Srikrishna SM   353275      This class is created to make sure that the triggers are executing only once in a given context.
 |  04.06.2014      Sneha Jain      PROPEL FD.I.002 Added a new public static flag 'firstRun' to control recursive triggers during insert operation
 |  18-09-2014      Bisna VP        CI #1086    Added a method runOnce1
 +========================================================================================================================*/
public class TriggerContextUtility {

    public static boolean firstRun = true;
    private static boolean run = true;
    private static boolean run1 = true;

    
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
    public static boolean runOnce1(){
        if(run1){
            run1=false;
            return true;
        }else{
            return run1;
        }
    }
}