/*=====================================================================================================+

|  HISTORY  |                                                                            

|  DATE          DEVELOPER                WR            DESCRIPTION                               

|  ====          =========                ==            =========== 
                                                       
| 11/20/2012     Krishna Pydavula        212217        This Test class is used to coverage the code for "Populate_Detail_lines_House_Acc_Schedule" 
                                                       and "Populate_Detail_lines_House_Account" Classes. 
| 22/01/2015     Karan Shekhar                         updated code with best practices under platform innovation project                                                                                                   
+=====================================================================================================*/
@IsTest
private class Populate_Detail_lines_House_Acc_Sch_TC {

    static testMethod void populatedetails() {
        List<Account> accList = new List<Account>();
        List<Lead> leadList = new List <Lead>();
        List<Opportunity> OppList = new List<Opportunity>();
        List<Product2> productList = new List<Product2>();
        List<PricebookEntry> pbEntryList= new List<PricebookEntry>();
        List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
        List<PriceBook2> pb2List =new List<PriceBook2>();
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];                
        System.runAs(insertUser){
    
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
            CustomSettingDataHelper.opportunityIntegrationCSData();
            
            
        }  
        User u=[Select id,name from User where name='House Account' AND Isactive=True limit 1];
        HouseAccount__c houseacc=new HouseAccount__c(House_Account_User__c=u.id); 
        Database.upsert(houseacc,false);
        accList=AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        if(!accList.isEmpty()) {
            
            insert accList;
        }
        leadList=LeadTestClassDataHelper.createLead();
        if(!leadList.isEmpty()) {
            
            insert leadList;
        }
        OppList=OpportunityTestClassDataHelper.createOpptys(accList[0],leadList[0]);
        for(Integer i=0;i<OppList.size();i++){
      
            OppList[i].ownerId=u.Id;
            OppList[i].StageName='Pipeline';
            OppList[i].CloseDate=system.today()+7;
        }
        if(!OppList.isEmpty()) {
            
            insert OppList;
        }
        
        productList=OpportunityTestClassDataHelper.createProducts();
        if(!productList.isEmpty()) {
            
            insert productList;
        }
        Id pricebookId= Test.getStandardPricebookId();
        pbEntryList =OpportunityTestClassDataHelper.createPriceBookEntry(pricebookId,productList);
        if(!pbEntryList.isEmpty()) {
            
            insert pbEntryList;
        }
        oppLineItemList= OpportunityTestClassDataHelper.createOpptyLineItems(OppList,pbEntryList);
        if(!oppLineItemList.isEmpty()) {
            
            insert oppLineItemList;
        }
        Detail_Line__c detailLineObj = new Detail_Line__c(Name='testDetail',Opportunity__c=OppList[0].Id );
        Populate_Detail_lines_House_Account details=new Populate_Detail_lines_House_Account();
        //details.query ='select Id,OpportunityId,PricebookEntry.Product2Id,UnitPrice,Opportunity.CurrencyIsoCode,Quote_Amount__c from OpportunityLineItem where OpportunityId in:Opp';
        Populate_Detail_lines_House_Acc_Schedule sh1 = new Populate_Detail_lines_House_Acc_Schedule();        
        String sch = '0 0 23 * * ?';        
        System.Test.startTest();
        system.schedule('Test Check', sch, sh1);
        id  batchid = Database.executeBatch(details);
        System.Test.stopTest();
        
                  
    }
}