/*====================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR         DESCRIPTION                               

 |  ====          =========      ==         =========== 

 |  22/12/2010    Ashwini Gowda  Req# 1155  Test class for allow partners users
                                            and CAMs to access the channel account plan.
 |  01/03/2011    Anil Sure     2430        udpated the code for EMC channel User profile name used    

 |  23-Jul-2011   Anand Sharma              Updated email field value of user record creation
 |  28-Sep-2011   Anil                      Created Account record instead of fetching using Query
 |  11-JAN-2012   Anil                      Removed role Id
 |  22-SEP-2014   Bisna  V P                Fixed mixed DML
 |  22-Jan-2015 Garima Bansal Optimized the class, this TC covers 3 main class PRM_CAPVisibility ,PRM_NotificationToAccountOwners , PRM_ActivityLockonCAP
 |=============================================================================================================== */

@isTest
Private Class PRM_CAPVisibility_TC{
    Private static testMethod void PRM_CAPVisibility_TM(){        
        //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.VCEStaticCSData();
        } 
        
        Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
        String AmericasDistributorCAP = data.get('AmericasDistributorCAP').DataValue__c;
        String APJDistributorCAP = data.get('APJDistributorCAP').DataValue__c;
        String EMEADistributorCAP = data.get('EMEADistributorCAP').DataValue__c;
        String AmericasObjective = data.get('AmericasObjective').DataValue__c;
        
        String AmericasStrategy = data.get('AMERStrategy').DataValue__c;
        
        Map<String,VCEStatic__c>  dataVCE =  VCEStatic__c.getall();
        String VCETaskRecordType = dataVCE.get('VCETaskRecordType').Value__c; 
        
        List<Account> partnerAccount = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();  
        insert partnerAccount;
        Contact cont = UserProfileTestClassDataHelper.createContact();
        cont.AccountId=partnerAccount[0].Id;
        insert cont;
        
        //Insert user record where Profile is System Admin
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='testR2Ruser1@emailR2RTest.com', alias='tuser1', CommunityNickName='tuser1', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' ,Forecast_Group__c = 'Maintenance Renewals');  
        insert insertUser;
        User PartnerUser;
        System.runAs(insertUser)
            {       
                String distributorSuperUser = data.get('EMEA Distributor Super User').DataValue__c ;
                Profile amerUserProf = [select Id from Profile where Name=: distributorSuperUser];
                Test.startTest();
                PartnerUser = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.id,null,cont.Id);
                insert PartnerUser;
                Test.stopTest(); 
            }
        
        list<SFDC_Channel_Account_Plan__c> channelPlanList = new List<SFDC_Channel_Account_Plan__c>();
        list<SFDC_Objective__c> listObj = new list <SFDC_Objective__c> ();
        list<Strategy__c> listStrategy = new list <Strategy__c> ();
        
         
        Profile emcUser = [select Id from Profile where Name=:'AMER Channels User'];              
        User EMCUser1 = new User(Username='testing123'+Math.random()+'@acme4.com.test34', ProfileId = emcUser.Id, FirstName='TestingCAP',LastName='LastCAP',email='john12456@emc.com',Alias='test1', CAP_Approver__c=true, IsActive=true, TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        
        //Create CAPs of all RecordType Ids
        channelPlanList.add(new SFDC_Channel_Account_Plan__c ( Active__c = true, EMC_Sales_Mgt_Approver__c = EMCUser1.id, Name = 'ChannelPlan1',Partner_Account__c = partnerAccount[0].id,Plan_Period__c = 'Q1', Status__c = 'New', RecordTypeId = AmericasDistributorCAP, EMC_CAM__c = EMCUser1.id, Plan_End__c = System.today() , Partner_Approver__c = PartnerUser.id ));
        channelPlanList.add( new SFDC_Channel_Account_Plan__c ( Active__c = true, EMC_Sales_Mgt_Approver__c = EMCUser1.id, Name = 'ChannelPlan2',Partner_Account__c = partnerAccount[0].id,Plan_Period__c = 'Q1', Status__c = 'New', RecordTypeId = APJDistributorCAP, EMC_CAM__c = EMCUser1.id, Plan_End__c = System.today() ));
        channelPlanList.add(new SFDC_Channel_Account_Plan__c ( Active__c = true, EMC_Sales_Mgt_Approver__c = EMCUser1.id, Name = 'ChannelPlan3',Partner_Account__c = partnerAccount[0].id,Plan_Period__c = 'Q1', Status__c = 'New', RecordTypeId = EMEADistributorCAP, EMC_CAM__c = EMCUser1.id, Plan_End__c = System.today() ));
        insert channelPlanList; 
        
        listObj.add( new SFDC_Objective__c ( Channel_Account_Plan__c = channelPlanList[0].id , Name = 'Objective0' , RecordTypeId = AmericasObjective )); 
        insert listObj ;
        
        listStrategy.add(new Strategy__c (Channel_Plan__c = channelPlanList[0].id , Name = 'strategy0' , RecordTypeId = AmericasStrategy ));
        insert listStrategy ;
        
        channelPlanList[0].Lock_CAP__c= false;
        channelPlanList[0].Quarterly_Notification_Date__c = System.today();
        //channelPlanList[1].Status__c='Approved';  
        update channelPlanList;
        
        partnerAccount[0].CAP_Required__c='Yes';
        partnerAccount[0].CAP_Creation_Date__c=System.today();
        partnerAccount[0].BillingCountry  = 'Australia';
        partnerAccount[1].CAP_Required__c='Yes';
        partnerAccount[1].CAP_Creation_Date__c=System.today();
        partnerAccount[1].BillingCountry  = 'Canada';
        update partnerAccount;
        
        PRM_NotificationToAccountOwners sendNotification = new PRM_NotificationToAccountOwners();
        sendNotification.updateQtrNotificationDateOnCAP();
        sendNotification.updateProfiledAccountsForAPJEMEA();
        sendNotification.updateProfiledAccountsForAmericas();   
        
        list<Task> listTask = new list <task>();
        listTask.add(new task ( WhatId = listObj[0].id , RecordTypeId = VCETaskRecordType , VCE_Assigned_To__c = 'EMC', VCE_Last_Modifying_Partner__c = 'EMC',ReminderDateTime = System.now(), Subject = 'TestTask', Type = 'Power Day'));
        insert listTask ;
        
        Map<String,Schema.RecordTypeInfo> eventRecordTypes = Event.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id EventRecTypeId = eventRecordTypes.get('Standard Event').getRecordTypeId();
        
        list <Event> listEvent = new list<Event>();
        listEvent.add(new event ( WhatId = listObj[0].id , RecordTypeId = EventRecTypeId, Subject = 'TestTask',Type = 'Power Day', DurationInMinutes = 30,ActivityDateTime = System.now() ));
        insert listEvent;
       
        
        Map<Id,Task> TaskDetails = new Map<Id,Task>();
        TaskDetails.put(channelPlanList[0].id,listTask[0]);
        
        Map<Id,Event> EventDetails = new Map<Id,Event>();
        EventDetails.put(channelPlanList[0].id,listEvent[0]);
        
        Set<Id> channelplanId = new Set<Id>();
        channelplanId.add(channelPlanList[0].id);
        channelplanId.add(channelPlanList[0].id);
        
        PRM_ActivityLockonCAP activityLock = new PRM_ActivityLockonCAP();
        activityLock.getTaskErrorMessage(TaskDetails,channelplanId);
        activityLock.getEventErrorMessage(EventDetails,channelplanId); 
    }  
}