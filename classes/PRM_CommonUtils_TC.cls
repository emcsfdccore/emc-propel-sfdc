/*====================================================================================================================+

 |  HISTORY  |                                                                           
    DATE          DEVELOPER     WR   DESCRIPTION                               

 |  ====          =========     ==   =========== 
 |  8/10/2010     Ashwini Gowda      Commented utils.fromGroup(usrIds);
 |  20/10/2010    Ashwini Gowda 1318 UnCommented utils.fromGroup(usrIds) as the issue is fixed.
 |  26-Jun-11     Anil               Renamed Partner User Profiles as per July Release
 |  30-Nov-11     Anil               Removed Queries and MIXED DML Exceptions and Query Statements

 |  28 Nov 2013     Avinash K   319046      Updated the API version to 29.0 and made seeAllData = false 
                                                (adding custom setting data through the helper class CustomSettingDataHelper)
 |=============================================================================================================== */


@isTest (seeAllData = false)
Private Class PRM_CommonUtils_TC
{
    
     
     
     static PRM_CommonUtils utils = new PRM_CommonUtils();
     static list<String> usrIds = new list<String>();
     public static List<Account> account;
     
    Private static testMethod void startTest()
    {
        
             User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
             User user01;
                
                System.runAs(insertUser)
                {
                    CustomSettingDataHelper.dataValueMapCSData();
                    CustomSettingDataHelper.dealRegistrationCSData();
                    CustomSettingDataHelper.eBizSFDCIntCSData();

                    //PRM_VPP_JobDataHelper.createVPPCustomSettingData();
                    Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
                    String emeadistributorSuperUser = data.get('EMEA Distributor Super User').DataValue__c ;
                    user01 = createUser(emeadistributorSuperUser);
                }           
 
      usrIds.add(user01.id);

      utils.isPartnerUser(user01.id);

      utils.userType(usrIds);
      
      utils.fromUser(usrIds);
      
      /*Group grp = [select id from group limit 1];
    
      usrIds.add(grp.id);
      
      utils.fromGroup(usrIds);*/
     
      utils.userDetails(usrIds);


    //Avinash's code starts below

        String strDate = utils.getQueryFormattedDate(System.NOW());

        Set<String> setStr = new Set<String>();
        setStr.add('testQueue');

        Map<String, Group> mapQueue = new Map<String,Group>();
        System.runAs(insertUser)
        {
            mapQueue = QueueHelper.createQueue(setStr);
            List<Account> lstPAcc = new List<Account>();

            List<Account> acc = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();     
            insert acc;
            for(Account account1 : acc)
            {      
                account1.Type='Partner';
                account1.IsPartner=true;  
                account1.IsPartner=true;  
                account1.IsPartner=true;  
            }
            
            Group temp1 = new Group();
            for (Group g1: mapQueue.values()) 
              {
                  temp1 = g1;
              }
            acc[0].Queue_Name__c = temp1.id;
            
            Schema.DescribeSObjectResult accountSchema = Schema.SObjectType.Account;
            Map<String,Schema.RecordTypeInfo> rtMapByName = accountSchema.getRecordTypeInfosByName();
            Id precid;

            for ( string s : rtMapByName.keyset()) 
            {
                if (s != 'Standard Account Record Type'  )
                {
                    Schema.RecordTypeInfo accountRT = rtMapByName.get(s);
                    precid = accountRT.getRecordTypeId();
                    break;
                }
            }

            acc[0].recordtypeid = precid;
            update acc;
        }

        List<Id> idList = new List<Id>();
        Group temp = new Group();
        for (Group g: mapQueue.values()) 
        {
          temp = g;
        }
        idList.add(temp.id);

        utils.fromGroup(idList);
        //Avinash' Code ends above
    }
    
    private static User createUser(String Profilename){          
    
            Profile amerUserProf = [select Id from Profile where Name=:Profilename];
            System.debug('Profile---->'+amerUserProf);
            account = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
            insert account;
            account[0].IsPartner = true;
            update account[0];
            Contact contact = UserProfileTestClassDataHelper.createContact();
            contact.AccountId= account[0].Id;
            contact.Email='test2345@emc.com';           
            insert contact;
            
            System.debug('contact----->'+contact);
            
            User tempUsr = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.Id, null, contact.id);

            insert tempUsr;
            System.debug('insert tempUsr--->'+tempUsr);
            return tempUsr;
        }

}