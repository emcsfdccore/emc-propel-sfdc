public with sharing class ctrlCommunitiesRedirect {

    public PageReference reDirectToCommunity() {
    CommunitiesEmployeeLogin__c mhc = CommunitiesEmployeeLogin__c.getInstance();
string myURL = mhc.Community_URL__c;
        PageReference p = new PageReference(myURL);
        return p;
    }

}