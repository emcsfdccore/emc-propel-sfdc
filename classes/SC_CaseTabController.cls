/*==================================================================================================================+
  This Controller Accepts 'sfdc.tabName' parameter(tab name) and queries object SC_Tab_Mapping__c to get each case subtype link.
    These case subtype links will be displayed in page SCCreateCaseTab. This is common page for different type of Visualforce Tab 
 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        ===========       
 |
 | 10/21/2014     Prabhat Pradhan          This Class is used for SCCreateCaseTab page
 | 
 +==================================================================================================================**/

public class SC_CaseTabController{
  
    
    
    public List<String> fldString{get; set;}
    public Map<String,SC_Tab_Mapping__c> fldMappingMap{get; set;}
    
    public String currentApp {get;private set;}
    public String allTabsDebug {get; private set;}
    public String currentTab {get; private set;}
    public String currentTabDebug {get; private set;}
    
  


      public SC_CaseTabController(ApexPages.StandardController controller) {
       String tabId = ApexPages.currentPage().getParameters().get('sfdc.tabName');               
       
      }
 
/* @Method <This Constructor execute is used to Perform Initial level of logic on SC Crate Case Tab page>
@param <This method takes StandardController as parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/

    public SC_CaseTabController(/*ApexPages.StandardController controller*/) {
      //  this.cs = (Case)controller.getRecord();        
        
        String tabId = ApexPages.currentPage().getParameters().get('sfdc.tabName');
        System.debug('String tabId = ApexPages.currentPage():'+ApexPages.currentPage());
        System.debug('tabId::'+tabId );      
             
       currentTab= tabId ;
       System.debug('currentTab::'+currentTab );
       renderFields();       
          
       }       

/* @Method <This method sets the fields name>
@param <This method takes No parameter>
@return void - <Not returning anything>
@throws exception - <No Exception>
*/

     public void renderFields(){


         //fldString.clear();
         fldString = new List<String>();
         fldMappingMap = new Map<String,SC_Tab_Mapping__c>(); 
           
         System.debug('currentTab::'+currentTab );
         //currentTab  ='Account_Payable';
                       
         List<SC_Tab_Mapping__c> lstRecord = [SELECT Case_Subtype__c, Case_Subtype_Image__c, Case_Type__c, Case_Subtype_Label_Name__c,Subtype_Panel_Label__c ,
                                             CreatedById, CreatedDate, CurrencyIsoCode, IsDeleted, IsActive__c,  Id, Name, case_Subtype_URL__c 
                                             FROM SC_Tab_Mapping__c where Case_Type__c=:currentTab and IsActive__c=true  order by SortOrder__c ];

                for(SC_Tab_Mapping__c scTabObj: lstRecord){
                    fldString.add(scTabObj.Case_Subtype__c);
                    fldMappingMap.put(scTabObj.Case_Subtype__c,scTabObj);
                }
                
              
        
       
    } 

        
  }