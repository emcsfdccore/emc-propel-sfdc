/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER             WR        DESCRIPTION                               

 |  ====          =========             ==        =========== 
    18-12-2014      Aarti             395546      Test Class for techConnSubmissionController        
 | 
                      
 +==================================================================================================================**/
@isTest 
private class techConnSubmissionController_TC {
    //Test method to test positive scenario
	static testMethod  void posTestTechConnSubmissionControl(){
        test.startTest();
        //test data for positive test
		Tech_Connect_Partner__c techConRec1=new Tech_Connect_Partner__c(Application_Status__c='New',Company_Name__c='test',First_Name__c='test',Last_Name__c='test',Email__c='test@test.com',Phone_Number__c='1232321',Source__c='Other',Previous_Year_s_Revenue__c=2,Name_of_Product_Offering__c='test',Product_Category__c='Cloud',Product_Offering_Description__c='test',Interested_EMC_Products__c='DPA',Company_Website__c='abc.com');
        
        ApexPages.StandardController sc1 = new ApexPages.standardController(techConRec1);
        techConnSubmissionController tc1=new techConnSubmissionController(sc1);
        tc1.SubmissionAction();
		System.assertEquals(true,tc1.flag);
		test.stopTest();
    }
	//Test method to test negative scenario
	static testMethod  void negTestTechConnSubmissionControl(){
        test.startTest();
		//test data for negative test
		Tech_Connect_Partner__c techConRec2=new Tech_Connect_Partner__c(Application_Status__c='Active',Company_Name__c='test',First_Name__c='test',Last_Name__c='test',Email__c='test@test.com');
		
		ApexPages.StandardController sc2 = new ApexPages.standardController(techConRec2);
        techConnSubmissionController tc2=new techConnSubmissionController(sc2);
        tc2.SubmissionAction();
		System.assertEquals(false,tc2.flag);
        test.stopTest();
    }    
}