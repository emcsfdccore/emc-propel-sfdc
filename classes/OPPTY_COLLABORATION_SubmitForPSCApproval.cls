/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER          WR        DESCRIPTION                               

 |  ====          =========          ==        =========== 

 |  03/04/2014    Shalabh Sharma               This class is used to update the status of opportunity registration record 
                                               on click of Submit for PSC Approval/Cancel Registration button.

 | 12 July 2014     Avinash K       Oppty       Added a method "populateAssignOwner" to populate the PSC Assigned
                                                Owner on click of the detail page button "Assign Owner"

 +==================================================================================================================**/
global class OPPTY_COLLABORATION_SubmitForPSCApproval{

/* @Method <This method executes on click of Submit for PSC Approval Button,used to update 
            Status fields on opportunity registration record.>
    @param <It takes Opportunity Registration Id as parameter>
    @return String of Success/Exception>
    @throws exception - <No Exception>
*/
    webservice static string SubmitForApproval(Id opptyRegId){
        Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
        CustomSettingDataValueMap__c UserError = DataValueMap.get('OpptyCollab_InvalidEntryCriteria');
        String result;
        Opportunity_Registration__c opptyRegRecord = [Select Id,Status__c,PSC_Approval_Status__c,Submitted_for_PSC_Approval_Date_Time__c from Opportunity_Registration__c where Id=:opptyRegId LIMIT 1];
        try{
            if(opptyRegRecord.Status__c.equals('New - Unsubmitted')){
                opptyRegRecord.Status__c = 'Submitted to PSC';
                opptyRegRecord.PSC_Approval_Status__c = 'Submitted';
                opptyRegRecord.Submitted_for_PSC_Approval_Date_Time__c = System.Now();
                opptyRegRecord.Partner_Approval_Status__c='';
                database.saveResult res = database.update(opptyRegRecord);
                result = 'success';
            }
            else{
                result = UserError.DataValue__c;
            }
        }
        catch(Exception e){
            Integer i =  e.getMessage().indexOf('VALIDATION_EXCEPTION,');
            Integer j =  e.getMessage().indexOf('Class.');
            if(i!=-1 && j!=-1){
                result = e.getMessage().substring(i,j);
            }
        }
        return result;
    }
/* @Method <This method executes on click of Cancel Registration Button,used to update 
            Status fields on opportunity registration record.>
    @param <It takes Opportunity Registration Id as parameter>
    @return String of Success/Exception>
    @throws exception - <No Exception>
*/
    webservice static string CancelRegistration(Id regId){
        String result;
        Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
        CustomSettingDataValueMap__c UserError = DataValueMap.get('OpptyCollab_PopulateCancellationReason');

        Opportunity_Registration__c opptyRegRecord = [Select Id,Status__c,Cancellation_Reason__c,Cancellation_Comments__c,ISR_Cancellation_Date_Time__c from Opportunity_Registration__c where Id=:regId LIMIT 1];
        if(opptyRegRecord.Cancellation_Reason__c != null && opptyRegRecord.Cancellation_Comments__c!=null){
            try{
                if(opptyRegRecord.Status__c.equals('Submitted to Partner') || opptyRegRecord.Status__c.equals('Accepted by Partner') || opptyRegRecord.Status__c.equals('Rejected by Partner')){
                    opptyRegRecord.Status__c = 'Cancellation Requested';
                    opptyRegRecord.ISR_Cancellation_Date_Time__c = System.Now();
                    database.saveResult res = database.update(opptyRegRecord);
                    result = 'success';
                }
                else{
                    opptyRegRecord.Status__c = 'Cancelled';
                    opptyRegRecord.ISR_Cancellation_Date_Time__c = System.Now();
                    database.saveResult res = database.update(opptyRegRecord);
                    result = 'success';
                }
            }
            catch(Exception e){
                Integer i =  e.getMessage().indexOf('VALIDATION_EXCEPTION,');
                Integer j =  e.getMessage().indexOf('Class.');
                if(i!=-1 && j!=-1){
                    result = e.getMessage().substring(i,j);
                }
            }
        }
        else{
            result = UserError.DataValue__c;
        }
        return result;
    }
/* @Method <This method executes on click of Cancel Registration Button,used to update 
            Status fields on opportunity registration record.>
    @param <It takes Opportunity Registration Id as parameter>
    @return String of Success/Exception>
    @throws exception - <No Exception>
*/
    webservice static string CancelRegistrationPSC(Id regId)
    {
        String result;
        Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
        //Fetch all PSC user profiles
        CustomSettingDataValueMap__c siteUserId = DataValueMap.get('PSC User');
        String userProfile = Userinfo.getProfileId();
        String pscUser = siteUserId.DataValue__c;
        CustomSettingDataValueMap__c UserError = DataValueMap.get('PSC_CancelRegistration');
        CustomSettingDataValueMap__c UserError1 = DataValueMap.get('OpptyCollab_PopulateCancellationReason');
        
        if(userProfile != null && userProfile.length() >15)
        {
            userProfile = userProfile.substring(0, 15);
        }
        Opportunity_Registration__c opptyRegRecord = [Select Id,Status__c,PSC_Cancellation_Date_Time__c,Cancellation_Reason__c, Cancellation_Comments__c from Opportunity_Registration__c where Id=:regId LIMIT 1];
        //Allow cancellation for System Admins and PSC users
        if(pscUser.contains(userProfile) || UserInfo.getProfileId().contains(Profiles__c.getInstance().System_Administrator__c))
        {
            if(opptyRegRecord.Cancellation_Reason__c != null && opptyRegRecord.Cancellation_Comments__c!=null)
            {
                try
                {
                    opptyRegRecord.Status__c = 'Cancelled';
                    opptyRegRecord.PSC_Cancellation_Date_Time__c = System.Now();
                    database.saveResult res = database.update(opptyRegRecord);
                    result = 'success';
                }
                catch(Exception e)
                {
                    Integer i =  e.getMessage().indexOf('VALIDATION_EXCEPTION,');
                    Integer j =  e.getMessage().indexOf('Class.');
                    if(i!=-1 && j!=-1)
                    {
                        result = e.getMessage().substring(i,j);
                    }
                }
            }
            else
            {
                result = UserError1.DataValue__c;
            }
            
        }
        else
        {
            result = UserError.DataValue__c;
        }
        return result;
    }



/**
    @Method <This method executes on click of Assign Owner Button,used to update 
            PSC Assigned Owner field on opportunity registration record.>
    @param <It takes Opportunity Registration Id as parameter>
    @return String of Success/Exception>
    @throws exception - <No Exception>
*/
    webservice static string populateAssignOwner(Id regId)
    {
        String strResult;
        Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
        //Fetch all PSC user profiles
        CustomSettingDataValueMap__c siteUserId = DataValueMap.get('PSC User');
        String strUserProfile = Userinfo.getProfileId();
        String strPscUser = siteUserId.DataValue__c;
        CustomSettingDataValueMap__c datUserError = DataValueMap.get('NonPSCUserError');
        // CustomSettingDataValueMap__c UserError1 = DataValueMap.get('OpptyCollab_PopulateCancellationReason');
        
        if(strUserProfile != null && strUserProfile.length() >15)
        {
            strUserProfile = strUserProfile.substring(0, 15);
        }
        Opportunity_Registration__c opptyRegRecord = [Select Id,Status__c,PSC_Cancellation_Date_Time__c,Cancellation_Reason__c, Cancellation_Comments__c, PSC_Assigned_User__c from Opportunity_Registration__c where Id=:regId LIMIT 1];
        
        //Allow to populate PSC Assigned Owner field for only PSC users
        
        if(strPscUser.contains(strUserProfile))
        {
            try
            {
                opptyRegRecord.PSC_Assigned_User__c = '' + Userinfo.getFirstName() + ' ' + UserInfo.getLastName();
                // opptyRegRecord.PSC_Cancellation_Date_Time__c = System.Now();
                database.saveResult res = database.update(opptyRegRecord);
                strResult = 'success';
            }
            catch(Exception e)
            {
                Integer i =  e.getMessage().indexOf('VALIDATION_EXCEPTION,');
                Integer j =  e.getMessage().indexOf('Class.');
                if(i!=-1 && j!=-1)
                {
                    strResult = e.getMessage().substring(i,j);
                }
            }
        }
        else
        {
            strResult = datUserError.DataValue__c;
        }
        return strResult;
    }
}