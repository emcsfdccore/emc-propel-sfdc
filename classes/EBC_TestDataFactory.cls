/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE       DEVELOPER     WR       DESCRIPTION                               
 |  ====       =========     ==       =========== 
 |  7/17/2013  Y.Salazar     273539   Apex Class used to create static test data to ease test code generation.
 |  05-Jun-2014 Abinaya M S           Assigned 'Internal meeting' value to Briefing_Type__c field to satisfy validation rule.
 +===========================================================================*/

public without sharing class EBC_TestDataFactory{
  public static Integer counter = 0;
  public static Id emcInternalAccount;
  public static Id emcInternalRT;
  public static EBC_Briefing_Event__c briefingEvent;
  public static List<Contact> emcInternalContacts;
  public static List<EBC_Topics__c> ebcTopics;
  public static List<EBC_Topic_Presenters__c> topicPresenters;
  public static List<EBC_Session__c> sessions;
  public static List<EBC_Session_Presenter__c> sessionPresenters;
  
  public static void createContact(Integer c){
    emcInternalAccount = '0017000000OmdVa';
    emcInternalRT = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND Name = 'EMC Internal Contact' LIMIT 1].Id;
    
    emcInternalContacts = new List<Contact>();
    for(Integer i=0; i<c; i++){
      counter++;
      emcInternalContacts.add(new Contact(
        RecordTypeId = emcInternalRT,
        AccountId = emcInternalAccount,
        Salutation = 'Mr.',
        FirstName = 'Test' + i,
        LastName = 'Contact' + i,
        Title = 'Engineer',
        PowerlinkID_Badge__c = '00' + counter,
        Cost_Center__c = 'Mx1',
        EBC_Title__c = 'Developer',
        Organization__c = 'Global Solutions - Cloud Infrastructure',
        Active_Presenter__c = true,
        Language_Preference__c = 'English',
        Start_Date__c = date.newInstance(2010, 1, 1),
        Phone = '12345678',
        MobilePhone = '01234569',
        Email = 'contact@sfdc.com' + counter,
        EBC_Name__c = 'Test Contact' + i,
        Discussion_Leader_Location__c = 'Mx',
        Discussion_Leader_Area_of_Expertise__c = 'Dev',
        Active__c = true
      ));
    }
  }
  
  public static void createTopic(Integer t){
    ebcTopics = new List<EBC_Topics__c>();
    for(Integer i=0; i<t; i++){
      counter++;
      ebcTopics.add(new EBC_Topics__c(
        Name = 'EBC Topic' + i,
        Session_Title__c = 'Test Topic' + i,
        Session_Description__c = 'Testing topic.',
        Session_Length__c = 1,
        Category__c = 'EBC - No Content',
        Location__c = 'Mx',
        Active__c = true
      ));
    }
  }
  
  public static void createTopicPresenter(Integer p){
    topicPresenters = new List<EBC_Topic_Presenters__c>();
     for(Integer i=0; i<p; i++){
      counter++;
      topicPresenters.add(new EBC_Topic_Presenters__c(
        Topic__c = ebcTopics[i].Id,
        Presenter__c = emcInternalContacts[i].Id,
        Name = emcInternalContacts[i].FirstName + emcInternalContacts[i].LastName,
        Active__c = true,
        IsCertified__c = true
      ));
    }
  }
  
  public static void createBriefingEvent(){
    counter++;
    briefingEvent = new EBC_Briefing_Event__c(
      Name = 'Briefing Event Test ' + counter,
      Briefing_Status__c = 'Requested',
      Start_Date__c = date.today(),
      Briefing_Type__c = 'Internal meeting',
      End_Date__c = date.today().addDays(3)
    );
  }
  
  public static void createSession(Integer s){
    sessions = new List<EBC_Session__c>();
    for(Integer i=0; i<s; i++){
      counter++;
      sessions.add(new EBC_Session__c(
        Name = 'Session' + i,
        Briefing_Event__c = briefingEvent.Id,
        Topic__c = ebcTopics[i].Id,
        Session_Start_Time__c = datetime.now(),
        Session_End_Time__c = datetime.now().addHours(2),
        Briefing_Advisor_Notes__c = 'No advisor notes.',
        Requester_Notes__c = 'No requester notes.'
      ));
    }
  }
  
  public static void createSessionPresenter(Integer p){
    sessionPresenters = new List<EBC_Session_Presenter__c>();
    for(Integer i=0; i<p; i++){
      sessionPresenters.add(new EBC_Session_Presenter__c(
        Name = sessions[i].Name + topicPresenters[i].Name,
        Session__c = sessions[i].Id,
        Topic_Presenters__c = topicPresenters[i].Id
      ));
    }
  }
  
  public static void createGuestPresenter(Integer g){
    if(sessionPresenters == null){
      sessionPresenters = new List<EBC_Session_Presenter__c>();
    }
    
    for(Integer i=0; i<g; i++){
      counter++;
      sessionPresenters.add(new EBC_Session_Presenter__c(
        Name = sessions[i].Name + 'Guest Presenter ' + i,
        Session__c = sessions[i].Id,
        Guest_Presenter_First_Name__c = 'Guest',
        Guest_Presenter_Last_Name__c = 'Presenter ' + i,
        Guest_Presenter_Email__c = 'gpresenter@sfdc.com' + i,
        Guest_Presenter_EBC_Title__c = 'Developer'
      ));
    }
  }
  
  public static void createData(Integer i){
    createContact(i);
    insert emcInternalContacts;
    
    createTopic(i);
    insert ebcTopics;
    
    createTopicPresenter(i);
    insert topicPresenters;
    
    createBriefingEvent();
    insert briefingEvent;
    
    createSession(i);
    insert sessions;
    
    createSessionPresenter(i);
    createGuestPresenter(i);
    insert sessionPresenters;
  }
}