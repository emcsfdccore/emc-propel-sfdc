/*===========================================================================+
|  HISTORY                                                                  
|                                                                           
|  DATE       DEVELOPER     WR       DESCRIPTION                               
|  ====       =========     ==       =========== 
| 01-July-14  Naga                   Fetch Account related contracts from CMC by calling WB methods.
|  
| 01-Aug-14  Aarti & Vinod   
| 13-Feb-15  Bisna V P     CI 1530   Update CMC Lookup Window results to include Additional Attributes 
| +====================================================================================*/
public class cmc_ViewaccountContractsController {
    private final Account acct;
    public boolean renderTable {get;set;}
    public String contractID {get;set;}
    public contractwrapper tempContract {get;set;}  
    public User loggedInUser;
    Map<String,Contractwrapper> contractMap = new Map<String,Contractwrapper>();
    public List<Contractwrapper> contractWrapperLst {get;set;}
    public List<Contractwrapper> contractWrapperLstDisplay {get;set;}
    public List<List<Contractwrapper>> lstContractWrapperLst {get;set;}
    public Boolean hasNex {get;set;}
    public Boolean hasPre {get;set;}
    public Boolean hasFirs {get;set;}
    public Boolean hasLas {get;set;}
    public List<ContractPdfwrapper> contractPdfWrapperLst {get;set;}
    public ContractPdfwrapper contrPdfDisplay {get;set;}
    public emcComItEnterpriseDataV1.MetaDataType[] pdfData{get;set;}
    public String pdfRes {get;set;}
    public List<String> current_list = new List<String>(); //list for holding many record Ids
    public List<String> next_list = new List<String>(); //list for holding record Ids that are after the current records
    public List<String> previous_list = new List<String>(); //list for holding record Ids that are before the current records
    Integer list_size = 50; //number of records to display on the page
    public Integer currentPage{get;set;}
    public cmc_ViewaccountContractsController(ApexPages.StandardController stdController) 
    {
        this.acct = (Account)stdController.getRecord();
        loggedInUser = [Select id,FederationIdentifier from User where id=:Userinfo.getuserid()];
        Account acc =[select Id,Name,Golden_Site_Identifier__c,Global_DUNS_Entity__c,CurrencyIsoCode,Party_Number__c from Account where id =: acct.id ];
        contractMap.clear();
        emcComItEnterpriseContractCmccontra.cmcbaseSoap11 emcComDocobj = new  emcComItEnterpriseContractCmccontra.cmcbaseSoap11();
        emcComDocobj.timeout_x=120000;
        emcComItEnterpriseDataV1.GetDataLookUpRequestDocument_element cmcaccReqData = new emcComItEnterpriseDataV1.GetDataLookUpRequestDocument_element();
        cmcaccReqData.PartyNumber = acc.Party_Number__c;
        cmcaccReqData.GlobalUltimateID = acc.Global_DUNS_Entity__c;
        cmcaccReqData.MDMId = acc.Golden_Site_Identifier__c;
        cmcaccReqData.CustomerName =acc.Name;
        cmcaccReqData.BadgeID =loggedInUser.FederationIdentifier;
        //cmcaccReqData.OperatingUnit = acc.;
        emcComItEnterpriseDataV1.CMCContractDataType[] data  = emcComDocobj.GetContractDataLookUp(cmcaccReqData);
        system.debug('Reqparam*****'+cmcaccReqData);
        system.debug('ResData*************'+data);
        if(data==null)
        {
            renderTable=false;
            //ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'No Records found');            
            //ApexPages.addMessage(msg);
        }
        else{
            contractWrapperLst=new List<Contractwrapper>();
            contractWrapperLstDisplay=new List<Contractwrapper>();
            lstContractWrapperLst = new List<List<Contractwrapper>>();
            Integer record_counter = 0; //counter
            for (emcComItEnterpriseDataV1.CMCContractDataType contactObj: data) { 
                ContractWrapper cw = new ContractWrapper(contactObj);
                cw.CMCContractID = contactObj.CMCContractID;
                contractWrapperLst.add(cw);
                System.debug('----->contractWrapperLst 1'+contractWrapperLst);
                contractMap.put(contactObj.CMCContractID,cw);
                record_counter++;
                if(record_counter ==50) { 
                    lstContractWrapperLst.add(contractWrapperLst);
                    contractWrapperLst=new List<Contractwrapper>();
                    System.debug('----->lstContractWrapperLst Big one 1'+lstContractWrapperLst);
                    record_counter = 0;
                    //contractWrapperLst.clear();
                }
            }
            if(contractWrapperLst.size() > 0) {
                lstContractWrapperLst.add(contractWrapperLst);
                record_counter = 0;
            }
            System.debug('----->lstContractWrapperLst Size'+lstContractWrapperLst.size());
            currentPage = 0;
            contractWrapperLstDisplay = lstContractWrapperLst.get(currentPage);
            System.debug('---------->lstContractWrapperLst.get(currentPage)'+lstContractWrapperLst.get(currentPage));
            System.debug('----->contractWrapperLstDisplay'+contractWrapperLstDisplay);
            
            if(lstContractWrapperLst.size()>0){
                renderTable=true;
            }
            if(lstContractWrapperLst.size()==1){
                hasPre = false;
                hasNex = false;
                hasFirs = false;
                hasLas = false;
            }
            if(lstContractWrapperLst.size()>1){
                hasNex = true;
                hasPre = false;
                hasFirs = false;
                hasLas = true;
            }
        }  
    }
    /* @ Method           - <This method is called at on click of Contract ID>
@ Params           - <No Input Parameters>
@ Returns          - <PageReference>
@ throwsException  - <No Exception>
*/
    public Pagereference DetailedView()
    {
        contractPdfWrapperLst = new List<ContractPdfwrapper>();
        loggedInUser = [Select id,FederationIdentifier from User where id=:Userinfo.getuserid()];
        emcComItEnterpriseContractCmccontra.cmcbaseSoap11 emcComPdfobj = new  emcComItEnterpriseContractCmccontra.cmcbaseSoap11();
        emcComPdfobj.timeout_x=120000;
        emcComItEnterpriseDataV1.GetMetaDataLookUpRequestDocument_element pdfRequest = new emcComItEnterpriseDataV1.GetMetaDataLookUpRequestDocument_element();
        system.debug('contractID ******************'+contractID );
        pdfRequest.ContractID=contractID;
        pdfRequest.BadgeID =loggedInUser.FederationIdentifier;
        pdfData = emcComPdfobj.GetContractMetaDataLookUp(pdfRequest);
        System.debug('ReqMetaDataparam****'+pdfRequest);
        System.debug('ResMetaData------->'+pdfData);
        if(pdfData!=null){
            tempContract = contractMap.get(contractID);
            for (emcComItEnterpriseDataV1.MetaDataType contactPdfObj: pdfData)
            {
                ContractPdfwrapper contractPdfWrapper = new ContractPdfwrapper(contactPdfObj);
                contractPdfWrapperLst.add(contractPdfWrapper);
            }
            contrPdfDisplay = contractPdfWrapperLst.get(0);
            System.debug('contractPdfWrapperLst-->'+contractPdfWrapperLst);
            System.debug('contrPdfDisplay--> '+contrPdfDisplay);
            System.debug('contrPdfDisplay.Contract.Type_x--> '+contrPdfDisplay.Contract.Type_x);
            PageReference pageRef = new  PageReference('/apex/cmc_ContractsFullView');
            //pageRef.setRedirect(false);
            return pageRef ;
        }
        else{
            pagereference metadata = apexpages.Currentpage();
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'No Metadata Found for this Contract'); 
            ApexPages.addMessage(msg);
            return metadata;
        }
    } 
    public PageReference PdfView(){
        loggedInUser = [Select id,FederationIdentifier from User where id=:Userinfo.getuserid()];
        emcComItEnterpriseContractCmccontra.cmcbaseSoap11 emcComPdfdetailobj = new  emcComItEnterpriseContractCmccontra.cmcbaseSoap11();
        emcComPdfdetailobj.timeout_x=120000;
        emcComItEnterpriseDataV1.GetVirtualLinkLookUpRequestDocument_element virtualLink= new emcComItEnterpriseDataV1.GetVirtualLinkLookUpRequestDocument_element();
        virtualLink.ContractID=contractID;
        virtualLink.BadgeID =loggedInUser.FederationIdentifier;
        emcComItEnterpriseDataV1.GetVirtualLinkLookUpResponseDocument_element pdfRes1=emcComPdfdetailobj.GetContractVirtualLinkLookUp(virtualLink);
        pdfRes = pdfRes1.VirtualLinkURL;
        System.debug('ReqvirtualLinkparam****'+virtualLink);
        System.debug('Resvirtuallinkpdf--------->'+pdfRes);
            if(pdfRes==null){
            pagereference pdfdata = apexpages.Currentpage();
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'No PDF found'); 
            ApexPages.addMessage(msg);
            return pdfdata;
        }
        else{
            return  new  PageReference(pdfRes);
        }
    }  
    // returns the first page of records
    public void firs() {
        currentPage=0;
        if(lstContractWrapperLst.size()>1){
            hasNex = true;
            hasPre = false;
            hasLas=true;
            hasFirs=false;
        }          
        contractWrapperLstDisplay = lstContractWrapperLst.get(0);       
    }
    // returns the last page of records
    public void las() {
        currentPage=lstContractWrapperLst.size()-1;
        if(lstContractWrapperLst.size()>1){
            hasPre = true;
            hasNex = false;
            hasFirs=true;
            hasLas=false;
        }
        contractWrapperLstDisplay = lstContractWrapperLst.get(currentPage);
    }
    // returns the previous page of records
    public void prev() {
        currentPage--;
        if(currentPage>0 && currentPage<lstContractWrapperLst.size()-1){
            hasPre = true;
            hasNex = true;
            hasFirs=true;
            hasLas=true;
        }
        if(currentPage==0 && lstContractWrapperLst.size()>1){
            hasPre = false;
            hasNex = true;
            hasFirs=false;
            hasLas=true;
        }      
        contractWrapperLstDisplay = lstContractWrapperLst.get(currentPage);
    }
    // returns the next page of records
    public void nex() {
        currentPage++;
        System.debug('------->lstContractWrapperLst.size()'+lstContractWrapperLst.size());
        System.debug('------->currentPage'+currentPage);
        if(lstContractWrapperLst.size()-1==currentPage){
            hasNex = false;
            hasPre=true;
            hasFirs=true;
            hasLas=false;
        }  
        if(lstContractWrapperLst.size() > currentPage+1){
            hasNex=true;
            hasPre=true;
            hasFirs=true;
            hasLas=true;
        }
        contractWrapperLstDisplay = lstContractWrapperLst.get(currentPage);
    }
    public class Contractwrapper {
        public String CMCContractID {get;set;}
        public String CustomerName{get;set;}
        public String BillToCustomerName{get;set;}
        public String DocumentType{get;set;}
        public String DocumentName{get;set;} 
        public String ContractStatus{get;set;}
        public String ContractEffectiveDate{get;set;} 
        public String ContractExpirationDate{get;set;}
        public String BusinessUnit{get;set;} 
        public String OperatingUnit{get;set;} 
        public String CMCContractFileSize{get;set;} 
        //1530 changes
        public String FreightTerms{get;set;} 
        public String ShippingTerms{get;set;} 
        public String PaymentTerms{get;set;} 
        public emcComItEnterpriseDataV1.CMCContractDataType cmcObj {get;set;}
        
        public Contractwrapper(emcComItEnterpriseDataV1.CMCContractDataType cmcObj) 
        {
            this.cmcObj = cmcObj;
            
        }
    }
    public class ContractPdfwrapper {
        public emcComItEnterpriseDataV1.ContractType Contract{get;set;}
        public emcComItEnterpriseDataV1.FundingFeeType FundingFee{get;set;}
        public emcComItEnterpriseDataV1.PartnerDetailsType ChannelPartner{get;set;}
        public emcComItEnterpriseDataV1.CustomerOrderType CustomerOrder{get;set;}
        public emcComItEnterpriseDataV1.MasterContractType MasterContract{get;set;}
        public emcComItEnterpriseDataV1.ConcessionsType Concessions{get;set;}
        public emcComItEnterpriseDataV1.OrderEgreementType OrderEgreement{get;set;}
        public emcComItEnterpriseDataV1.GovtContractType GovernmentContract{get;set;}
        public emcComItEnterpriseDataV1.ContractTermsType ContractTerms{get;set;}
        public emcComItEnterpriseDataV1.DocumentDetailsType DocumentDetails{get;set;}
        public emcComItEnterpriseDataV1.ContractClauseType ContractClause{get;set;}
        public emcComItEnterpriseDataV1.BusinessEntityType BusinessEntity{get;set;}
        public emcComItEnterpriseDataV1.MaintenanceDetailType Maintenance{get;set;}
        public emcComItEnterpriseDataV1.ContractCustomerEntityType CustomerEntity{get;set;}
        public emcComItEnterpriseDataV1.ContractRequirementType Requirement{get;set;}
        public boolean isDateLessThn1900 {get;set;}
        public emcComItEnterpriseDataV1.MetaDataType cmcPdfObj {get;set;}
        public string MainUserComments{get;set;}
        
        public ContractPdfwrapper(emcComItEnterpriseDataV1.MetaDataType cmcPdfObj) 
        {
            this.Contract = cmcPdfObj.Contract;
            
            isDateLessThn1900 = false;
            
            if(this.Contract.EffectiveDate != null) {
                List<String> dateSplit = Contract.EffectiveDate.split('T')[0].split('-');
                Integer year = Integer.valueOf(dateSplit[0]);
                Integer month = Integer.valueOf(dateSplit[1]);
                Integer day = Integer.valueOf(dateSplit[2]);
                Date effDate = Date.newInstance(year, month, day);
                Date baseDate = Date.newInstance(1900, 01, 01);
                if(effDate <= baseDate) {
                    isDateLessThn1900 = true;
                }
            }  
            
            if(this.Contract.ExpirationDate != null){
                List<String> dateSplit = Contract.ExpirationDate.split('T')[0].split('-');
                Integer year = Integer.valueOf(dateSplit[0]);
                Integer month = Integer.valueOf(dateSplit[1]);
                Integer day = Integer.valueOf(dateSplit[2]);
                Date effDate = Date.newInstance(year, month, day);
                Date baseDate = Date.newInstance(1900, 01, 01);
                if(effDate <= baseDate) {
                    isDateLessThn1900 = true;
                }
            }  
            
            this.FundingFee=cmcPdfObj.FundingFee;
            this.ChannelPartner=cmcPdfObj.ChannelPartner;
            this.CustomerOrder=cmcPdfObj.CustomerOrder;
            this.MasterContract=cmcPdfObj.MasterContract;
            this.Concessions=cmcPdfObj.Concessions;
            this.OrderEgreement=cmcPdfObj.OrderEgreement;
            this.GovernmentContract=cmcPdfObj.GovernmentContract;
            this.ContractTerms=cmcPdfObj.ContractTerms;
            this.DocumentDetails=cmcPdfObj.DocumentDetails;
            this.ContractClause=cmcPdfObj.ContractClause;
            this.BusinessEntity=cmcPdfObj.BusinessEntity;
            this.Maintenance=cmcPdfObj.Maintenance;
            this.CustomerEntity=cmcPdfObj.CustomerEntity;
            this.Requirement=cmcPdfObj.Requirement;
            this.MainUserComments=cmcPdfObj.MainUserComments;
        }
        
    }    
    
}