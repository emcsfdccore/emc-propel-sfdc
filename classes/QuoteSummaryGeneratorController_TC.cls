/*==================================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        =========== 
 |  10/Apr/2015   Bisna V P     CI 1655    Created classs to cover QuoteSummaryGeneratorController
 ====================================================================================================================*/

@isTest
class QuoteSummaryGeneratorController_TC{
    static testMethod void test(){
        System.runAs(new user(Id = UserInfo.getUserId()))
            {
                CustomSettingDataHelper.dataValueMapCSData();
                CustomSettingDataHelper.bypassLogicCSData();
                CustomSettingDataHelper.eBizSFDCIntCSData();
                CustomSettingDataHelper.profilesCSData();
                CustomSettingDataHelper.countryTheaterMapCSData();
                CustomSettingDataHelper.dealRegistrationCSData();
            } 
        Test.startTest();   
        
        String emcInstallRecordType = '';
         List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where RecordType.DeveloperName like 'EMC_Install' ];
            if(recordType[0].DeveloperName == 'EMC_Install')
            {
                emcInstallRecordType = recordType[0].id;
            }
        List<Account> accList = new List<Account>();
                accList.add(new Account(Name = 'TestAcount0',Status__c='A'));
                accList.add(new Account(Name = 'TestAcount1',Customer_Profiled_Account__c = true,Status__c='A'));
        insert accList;
        
        List<Asset__c> assetList = new List<Asset__c>();
        
        Asset__c assetRec = new Asset__c(Name = 'TestAsset0',Customer_Name__c = accList[1].Id,RecordTypeId = emcInstallRecordType,Next_Quoted_Contract_Status__c = 'Customer Quoted',Quote_Number__c ='1234' );
        assetList.add(assetRec);
        
        Asset__c assetRec1 = new Asset__c(Name = 'TestAsset1',Customer_Name__c = accList[1].Id,RecordTypeId = emcInstallRecordType,Next_Quoted_Contract_Status__c = 'Budgetary Estimate' );
        assetList.add(assetRec1);
        
        insert assetList; 
        
        QuoteSummaryGeneratorController qgc = new QuoteSummaryGeneratorController();
        qgc.redirectPage();
        qgc.assetId =assetList[0].id;
        System.debug('assetRec---->'+assetRec.id);
        qgc.redirectPage();
        qgc.assetId =assetList[1].id;
        qgc.redirectPage();
                
        List<Profile> profiles = [ Select id from Profile where name ='System Administrator'];
        User test_user = new User( email='test-user@fakeemail.com', profileid = profiles[0].id, UserName='test-user@fakeemail.com', alias='tuser1', CommunityNickName='tuser1', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='fr', FirstName = 'Test', LastName = 'User', FederationIdentifier = '123' ,theater__c='Americas' );
        
        insert test_user;
        
        System.runAs(test_user){
            
            qgc.assetId =assetList[0].id;           
            qgc.redirectPage();
            qgc.assetId =assetList[1].id;
            qgc.redirectPage();
        }
        test_user.LanguageLocaleKey= 'de';
        test_user.theater__c='N/A';
        update test_user;
        
        System.runAs(test_user){            
            qgc.assetId =assetList[0].id;           
            qgc.redirectPage();
        }
        test_user.LanguageLocaleKey= 'it';
        test_user.theater__c='APJK';
        update test_user;
        
        System.runAs(test_user){            
            qgc.assetId =assetList[0].id;           
            qgc.redirectPage();
        }
            
        Test.stopTest();
    }    

}