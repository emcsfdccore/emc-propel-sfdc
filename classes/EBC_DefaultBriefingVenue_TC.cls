/*
*  Created By       :- Yamilett Salazar
*  Created Date     :- 06/10/20103
*  Last Modified By :- Yamilett Salazar
*  Description      :- Test Class for EBC_DefaultBriefingVenue trigger/class.
   Modified by :   Abinaya M S 
    Modified on :   05-Jun-2014
    Reason:     :   Assigned 'Internal meeting' value to Briefing_Type__c field to satisfy validation rule.

*/

@isTest(SeeAllData = true)
private class EBC_DefaultBriefingVenue_TC{

  public static testMethod void Test_EBC_DefaultBriefingVenue(){
  
    EBC_Briefing_Center__c b1 = new EBC_Briefing_Center__c();
    b1.Name = 'Test 1';
    insert b1;
    
    List<EBC_Briefing_Event__c> events = new List<EBC_Briefing_Event__c>();
    
    EBC_Briefing_Event__c be1 = new EBC_Briefing_Event__c();
    be1.Name = 'Testing Venue, Hopkinton';
    be1.Briefing_Center__c = b1.Id;
    be1.Briefing_Type__c = 'Internal meeting';
    events.add(be1);
    
    EBC_Briefing_Event__c be2 = new EBC_Briefing_Event__c();
    be2.Name = 'Testing Venue, Durham';
    be2.Briefing_Center__c = b1.Id;
    be2.Briefing_Type__c = 'Internal meeting';
    events.add(be2);
    
    EBC_Briefing_Event__c be3 = new EBC_Briefing_Event__c();
    be3.Name = 'Testing Venue, Washington D.C.';
    be3.Briefing_Center__c = b1.Id;
    be3.Briefing_Type__c = 'Internal meeting';
    events.add(be3);
    
    EBC_Briefing_Event__c be4 = new EBC_Briefing_Event__c();
    be4.Name = 'Testing Venue, Field Americas';
    be4.Briefing_Center__c = b1.Id;
    be4.Briefing_Type__c = 'Internal meeting';
    events.add(be4);
    
    insert events;
  }
}