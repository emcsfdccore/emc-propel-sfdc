//Generated by wsdl2apex

public class AsyncEmcComItEnterpriseContractCmccontra {
    public class AsynccmcbaseSoap11 {
        public String endpoint_x = 'http://soagtwdev.isus.emc.com:4400/sst/runtime.asvc/com.actional.intermediary.cmcContractLookup';
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://emc.com/it/enterprise/contract/CMCContractLookupService/v1', 'emcComItEnterpriseContractCmccontra', 'http://emc.com/it/enterprise/data/v1', 'emcComItEnterpriseDataV1'};
        public AsyncEmcComItEnterpriseDataV1.GetVirtualLinkLookUpResponseTypeFuture beginGetContractVirtualLinkLookUp(System.Continuation continuation,emcComItEnterpriseDataV1.GetVirtualLinkLookUpRequestDocument_element GetVirtualLinkLookUpRequestDocument) {
            emcComItEnterpriseDataV1.GetVirtualLinkLookUpRequestType request_x = new emcComItEnterpriseDataV1.GetVirtualLinkLookUpRequestType();
            request_x.GetVirtualLinkLookUpRequestDocument = GetVirtualLinkLookUpRequestDocument;
            return (AsyncEmcComItEnterpriseDataV1.GetVirtualLinkLookUpResponseTypeFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncEmcComItEnterpriseDataV1.GetVirtualLinkLookUpResponseTypeFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'http://emc.com/it/enterprise/contract/CMCContractLookupService/v1',
              'GetContractVirtualLinkLookUpRequest',
              'http://emc.com/it/enterprise/contract/CMCContractLookupService/v1',
              'GetContractVirtualLinkLookUpResponse',
              'emcComItEnterpriseDataV1.GetVirtualLinkLookUpResponseType'}
            );
        }
        public AsyncEmcComItEnterpriseDataV1.GetDataLookUpResponseTypeFuture beginGetContractDataLookUp(System.Continuation continuation,emcComItEnterpriseDataV1.GetDataLookUpRequestDocument_element GetDataLookUpRequestDocument) {
            emcComItEnterpriseDataV1.GetDataLookUpRequestType request_x = new emcComItEnterpriseDataV1.GetDataLookUpRequestType();
            request_x.GetDataLookUpRequestDocument = GetDataLookUpRequestDocument;
            return (AsyncEmcComItEnterpriseDataV1.GetDataLookUpResponseTypeFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncEmcComItEnterpriseDataV1.GetDataLookUpResponseTypeFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'http://emc.com/it/enterprise/contract/CMCContractLookupService/v1',
              'GetContractDataLookUpRequest',
              'http://emc.com/it/enterprise/contract/CMCContractLookupService/v1',
              'GetContractDataLookUpResponse',
              'emcComItEnterpriseDataV1.GetDataLookUpResponseType'}
            );
        }
        public AsyncEmcComItEnterpriseDataV1.GetMetaDataLookUpResponseTypeFuture beginGetContractMetaDataLookUp(System.Continuation continuation,emcComItEnterpriseDataV1.GetMetaDataLookUpRequestDocument_element GetMetaDataLookUpRequestDocument) {
            emcComItEnterpriseDataV1.GetMetaDataLookUpRequestType request_x = new emcComItEnterpriseDataV1.GetMetaDataLookUpRequestType();
            request_x.GetMetaDataLookUpRequestDocument = GetMetaDataLookUpRequestDocument;
            return (AsyncEmcComItEnterpriseDataV1.GetMetaDataLookUpResponseTypeFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncEmcComItEnterpriseDataV1.GetMetaDataLookUpResponseTypeFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'http://emc.com/it/enterprise/contract/CMCContractLookupService/v1',
              'GetContractMetaDataLookUpRequest',
              'http://emc.com/it/enterprise/contract/CMCContractLookupService/v1',
              'GetContractMetaDataLookUpResponse',
              'emcComItEnterpriseDataV1.GetMetaDataLookUpResponseType'}
            );
        }
    }
}