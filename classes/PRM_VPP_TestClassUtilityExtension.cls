/*========================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER                WR       DESCRIPTION                               
 |  ====          =========                ==       =========== 
 |  02/16/2011    Arif Sheikh                   Extension test class for the class PRM_VPP_TestClassUtility 
 |  10/11/2011    Suman B                           Calling createVPPCustomSettingData() in Test methods.                         
 |  04/30/2014    Jaspreet Singh                    Code Coverage, BPP
 |  7/19/2014     Leonard Victor                   Fix for mutiple batch execution
 +=======================================================================================================*/
@isTest

  private class PRM_VPP_TestClassUtilityExtension {
  
    static testMethod void myUnitTest() {
     User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser)
        {
        //PRM_VPP_JobDataHelper.createVPPCustomSettingData();   
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();  
            CustomSettingDataHelper.BusinessPartnerProgramData();
            CustomSettingDataHelper.velocityRulesData();
            CustomSettingDataHelper.RevenueSpecialityCSData();
            CustomSettingDataHelper.VPPSpecialityRevenueCalculationsData();
        
        }
         //Creation of Account Grouping record.
          Account_Groupings__c grouping = new Account_Groupings__c (Name = 'UNITTESTGrp');
          Insert grouping;
        
        List<Account> lstAccount = new List<Account>();
        lstAccount.add( new Account (Name = 'UNITTESTAcc0', Country_Local__c = 'Japan',cluster__C = 'APJ', Batch_Job_Operation__c = 'Clustor/Theather Updated', profiled_Account_flag__c=true, Deployed_Velocity_Services__c = 'QSS Implement VNXe;VS Manage NetWorker',Grouping__c = grouping.Id, BillingCountry = 'New Zealand'));
        lstAccount.add( new Account (Name = 'UNITTESTAcc1', Country_Local__c = 'Japan',cluster__C = 'APJ', Batch_Job_Operation__c = 'Clustor/Theather Updated', profiled_Account_flag__c=true, Deployed_Velocity_Services__c = 'QSS Implement VNXe;VS Manage NetWorker',Grouping__c = grouping.Id, BillingCountry = 'New Zealand'));
        lstAccount.add( new Account (Name = 'UNITTESTAcc2', Country_Local__c = 'Japan',cluster__C = 'APJ', Batch_Job_Operation__c = 'Clustor/Theather Updated', profiled_Account_flag__c=true, Deployed_Velocity_Services__c = 'QSS Implement VNXe;VS Manage NetWorker',Grouping__c = grouping.Id, BillingCountry = 'New Zealand'));
        lstAccount.add( new Account (Name = 'UNITTESTAcc3', Country_Local__c = 'Japan',cluster__C = 'APJ', Batch_Job_Operation__c = 'Clustor/Theather Updated', profiled_Account_flag__c=true, Deployed_Velocity_Services__c = 'QSS Implement VNXe;VS Manage NetWorker',Grouping__c = grouping.Id, BillingCountry = 'New Zealand'));
        lstAccount.add( new Account (Name = 'UNITTESTAcc4', Country_Local__c = 'Japan',cluster__C = 'APJ', Batch_Job_Operation__c = 'Clustor/Theather Updated', profiled_Account_flag__c=true, Deployed_Velocity_Services__c = 'QSS Implement VNXe;VS Manage NetWorker',Grouping__c = grouping.Id, BillingCountry = 'New Zealand'));
        insert lstAccount;
        
        //Creation of Contact record.
          Contact contact = new Contact (FirstName = 'UNIT', LastName = 'TESTCon', Accountid = lstAccount[0].id, Active__c = true);
          Insert contact;
        
        //Creation of Education group record.
          list<Education_Groups__c> educationGroup = new list<Education_Groups__c>();
          educationGroup.add(new Education_Groups__c (Education_Group_Name__c = 'UNITTESTEduGrp', Mentoring__c = true));
          educationGroup.add(new Education_Groups__c (Education_Group_Name__c = 'UNITTESTEduGrp0'));
          Insert educationGroup;
        
        //Creation of Education Master record.   
          Education_Master__c eduMaster = new Education_Master__c();
          Insert eduMaster;
    
        //Creation of Education Group Member record.  
          Education_Group_Member__c eduMasterRec = new Education_Group_Member__c (Education_Group__c = educationGroup[0].Id, Education_Master__c = eduMaster.Id);
          Insert eduMasterRec;
    
        //Creation of Education record.  
          list<Education__c>  lisEducation = new  list<Education__c>();
          lisEducation.add (new Education__c (Contact__c = contact.Id, Education_Master__c = eduMaster.Id,Partner_Grouping__c=grouping.Id, Batch_Job_Operation__c = 'Clustor/Theather Updated', Mentoring_Completed__c = 'Yes'));
          lisEducation.add (new Education__c (Contact__c = contact.Id, Education_Master__c = eduMaster.Id,Partner_Grouping__c=grouping.Id, Batch_Job_Operation__c = 'Clustor/Theather Updated', Mentoring_Completed__c = 'Yes'));
          Insert lisEducation;
    
        //Creation of Education Education Group Mapping record.   
        list<Education_EducationGroup_Mapping__c> lstEduedugroupmapping = new list <Education_EducationGroup_Mapping__c>();
        lstEduedugroupmapping.add(new Education_EducationGroup_Mapping__c (Education_Group__c = educationGroup[0].Id, Education__c = lisEducation[0].Id));
        lstEduedugroupmapping.add(new Education_EducationGroup_Mapping__c (Education_Group__c = educationGroup[1].Id, Education__c = lisEducation[1].Id));
        Insert lstEduedugroupmapping ;
        
        //Creation of Velocity Rule Record. 
        List<Velocity_Rules__c> velocityRulesRecords = new List<Velocity_Rules__c>();
        List<RecordType> lstRecordType = [SELECT Id, DeveloperName, Name FROM RecordType WHERE SobjectType = 'Velocity_Rules__c' AND (Name = 'Specialty Rule' OR Name = 'Group Rule' OR Name = 'Roll Up Rule' OR Name = 'Tier Rule' OR Name = 'Services Rule' OR Name = 'Field Based Rule Type')];
        RecordType recordTypeSpecialty = new RecordType();
        RecordType recordTypeGroup = new RecordType();
        RecordType recordTypeRollUp = new RecordType();
        RecordType recordTypeTier = new RecordType();
        RecordType recordTypeService = new RecordType();
        RecordType recordTypeField = new RecordType();
        for(RecordType objRecType : lstRecordType){
            if(objRecType.Name == 'Specialty Rule'){
                recordTypeSpecialty = objRecType;
            }else if(objRecType.Name == 'Group Rule'){
                recordTypeGroup = objRecType;
            }else if(objRecType.DeveloperName == 'Rollup_Rule'){
                recordTypeRollUp = objRecType;
            }else if(objRecType.Name == 'Tier Rule'){
                recordTypeTier = objRecType;
            }else if(objRecType.Name == 'Services Rule'){
                recordTypeService = objRecType;
            }else if(objRecType.Name == 'Field Based Rule Type'){
                recordTypeField = objRecType;
            }
        }
                                     
    for(integer i = 0; i<=11; i++){  
        if(i==0){  
            Velocity_Rules__c velocityRuleRecordSpecialty = new Velocity_Rules__c();
            velocityRuleRecordSpecialty.Display_Name__c = 'Consolidate';
            velocityRuleRecordSpecialty.RecordTypeId = recordTypeSpecialty.Id;
            velocityRuleRecordSpecialty.Cluster__c = 'NA';
            velocityRuleRecordSpecialty.Specialty_Rule_Type__c = 'LA1';
            velocityRuleRecordSpecialty.Bucket__c = 'Training';
            velocityRuleRecordSpecialty.AND_OR__c='OR';
            velocityRuleRecordSpecialty.Any_Value_Acceptable__c = false;
            velocityRuleRecordSpecialty.Required__c = '1';
            velocityRuleRecordSpecialty.Tier__c = 'Affiliate';
            velocityRuleRecordSpecialty.Theater__c = 'EMEA';
            velocityRuleRecordSpecialty.Required_Type__c = 'USD';
            velocityRuleRecordSpecialty.Specilaity__c = 'Backup and Recovery';
            velocityRuleRecordSpecialty.BR_Sub_category__c = 'Avamar';
            velocityRulesRecords.add(velocityRuleRecordSpecialty);  
        }
        if(i==1){  
            Velocity_Rules__c velocityRuleRecordSpecialty = new Velocity_Rules__c();
            velocityRuleRecordSpecialty.Display_Name__c = 'UNITTEST'+i;
            velocityRuleRecordSpecialty.RecordTypeId = recordTypeSpecialty.Id;
            velocityRuleRecordSpecialty.Cluster__c = 'APJ';
            velocityRuleRecordSpecialty.Specialty_Rule_Type__c = 'LA1';
            velocityRuleRecordSpecialty.Bucket__c = 'Revenue';
            velocityRuleRecordSpecialty.AND_OR__c='AND';
            velocityRuleRecordSpecialty.Any_Value_Acceptable__c = false;
            velocityRuleRecordSpecialty.Tier__c = 'Gold';
            velocityRuleRecordSpecialty.Theater__c = 'EMEA';
            velocityRulesRecords.add(velocityRuleRecordSpecialty);
        }
        if(i==2){  
            Velocity_Rules__c velocityRuleRecordGroup = new Velocity_Rules__c();  
            velocityRuleRecordGroup.Display_Name__c = 'UNITTEST'+i;
            velocityRuleRecordGroup.RecordTypeId = recordTypeGroup.Id;
            velocityRuleRecordGroup.Cluster__c = 'APJ';
            velocityRuleRecordGroup.Specialty_Rule_Type__c = 'EMEA';
            velocityRuleRecordGroup.AND_OR__c='AND';
            velocityRuleRecordGroup.Any_Value_Acceptable__c = false;
            velocityRuleRecordGroup.Bucket__c = 'Solution Provider Program';
            velocityRuleRecordGroup.Tier__c = 'Gold';
            velocityRuleRecordGroup.Theater__c = 'EMEA';
            velocityRulesRecords.add(velocityRuleRecordGroup);
        }
        if(i==3){  
            Velocity_Rules__c velocityRuleRecordGroup = new Velocity_Rules__c();  
            velocityRuleRecordGroup.Display_Name__c = 'UNITTEST'+i;
            velocityRuleRecordGroup.RecordTypeId = recordTypeService.Id;
            velocityRuleRecordGroup.Cluster__c = 'APJ';
            velocityRuleRecordGroup.Specialty_Rule_Type__c = 'EMEA';
            velocityRuleRecordGroup.AND_OR__c='OR';
            velocityRuleRecordGroup.Any_Value_Acceptable__c = false;
            velocityRuleRecordGroup.Bucket__c = 'Solution Provider Program';
            velocityRuleRecordGroup.Tier__c = 'Gold';
            velocityRuleRecordGroup.Theater__c = 'EMEA';
            velocityRuleRecordGroup.Field_Name__c = 'Services_Compliant__c';
            velocityRulesRecords.add(velocityRuleRecordGroup);
        }
        if(i==4){
            Velocity_Rules__c velocityRuleRecordRollUp = new Velocity_Rules__c(); 
            velocityRuleRecordRollUp.Display_Name__c = 'UNITTEST'+i;
            velocityRuleRecordRollUp.RecordTypeId = recordTypeRollUp.Id;
            velocityRuleRecordRollUp.Cluster__c = 'APJ';
            velocityRuleRecordRollUp.Specialty_Rule_Type__c = 'APJ-M';
            velocityRuleRecordRollUp.AND_OR__c='AND';
            velocityRuleRecordRollUp.Any_Value_Acceptable__c = false;
            velocityRuleRecordRollUp.Bucket__c = 'Solution Provider Program';
            velocityRuleRecordRollUp.Tier__c = 'Gold';
            velocityRuleRecordRollUp.Theater__c = 'EMEA';
            velocityRuleRecordRollUp.Field_Name__c = 'Advanced_Consolidate_Specialty__c';
            velocityRuleRecordRollUp.Any_Value_Acceptable__c = false;
            velocityRuleRecordRollUp.Value__c = 'text';
            velocityRuleRecordRollUp.Required_Type__c = 'USD';
            velocityRuleRecordRollUp.VS_Section__c = 'QSS Implement';
            velocityRuleRecordRollUp.Required__c = 'Yes';
            velocityRulesRecords.add(velocityRuleRecordRollUp);
        }
        if(i==5){  
            Velocity_Rules__c velocityRuleRecordSpecialty = new Velocity_Rules__c();
            velocityRuleRecordSpecialty.Display_Name__c = 'UNITTEST'+i;
            velocityRuleRecordSpecialty.RecordTypeId = recordTypeTier.Id;
            velocityRuleRecordSpecialty.Cluster__c = 'APJ';
            velocityRuleRecordSpecialty.Specialty_Rule_Type__c = 'LA1';
            velocityRuleRecordSpecialty.Bucket__c = 'Revenue';
            velocityRuleRecordSpecialty.AND_OR__c='OR';
            velocityRuleRecordSpecialty.Any_Value_Acceptable__c = false;
            velocityRuleRecordSpecialty.Tier__c = 'Gold';
            velocityRuleRecordSpecialty.Theater__c = 'EMEA';
            velocityRuleRecordSpecialty.Required_Type__c = 'USD';
            velocityRulesRecords.add(velocityRuleRecordSpecialty);
        }
        if(i==6){  
            Velocity_Rules__c velocityRuleRecordField = new Velocity_Rules__c();
            velocityRuleRecordField.Display_Name__c = 'UNITTEST'+i;
            velocityRuleRecordField.RecordTypeId = recordTypeField.Id;
            velocityRuleRecordField.Cluster__c = 'APJ';
            velocityRuleRecordField.Specialty_Rule_Type__c = 'LA1';
            velocityRuleRecordField.Bucket__c = 'Revenue';
            velocityRuleRecordField.AND_OR__c='AND';
            velocityRuleRecordField.Value__c ='test';
            velocityRuleRecordField.Any_Value_Acceptable__c = false;
            velocityRuleRecordField.Tier__c = 'Gold';
            velocityRuleRecordField.Theater__c = 'EMEA';
            velocityRulesRecords.add(velocityRuleRecordField);
        }
        if(i==7){  
            Velocity_Rules__c velocityRuleRecordSpecialty = new Velocity_Rules__c();
            velocityRuleRecordSpecialty.Display_Name__c = 'UNITTEST'+i;
            velocityRuleRecordSpecialty.RecordTypeId = recordTypeTier.Id;
            velocityRuleRecordSpecialty.Cluster__c = 'APJ';
            velocityRuleRecordSpecialty.Specialty_Rule_Type__c = 'LA1';
            velocityRuleRecordSpecialty.Bucket__c = 'SSCP';
            velocityRuleRecordSpecialty.AND_OR__c='OR';
            velocityRuleRecordSpecialty.Any_Value_Acceptable__c = false;
            velocityRuleRecordSpecialty.Tier__c = 'Gold';
            velocityRuleRecordSpecialty.Theater__c = 'EMEA';
            velocityRuleRecordSpecialty.Required_Type__c = 'USD';
            velocityRulesRecords.add(velocityRuleRecordSpecialty);
        }
        if(i==8){  
            Velocity_Rules__c velocityRuleRecordSpecialty = new Velocity_Rules__c();
            velocityRuleRecordSpecialty.Display_Name__c = 'UNITTEST'+i;
            velocityRuleRecordSpecialty.RecordTypeId = recordTypeTier.Id;
            velocityRuleRecordSpecialty.Cluster__c = 'APJ';
            velocityRuleRecordSpecialty.Specialty_Rule_Type__c = 'LA1';
            velocityRuleRecordSpecialty.Bucket__c = 'Services Implement';
            velocityRuleRecordSpecialty.AND_OR__c='OR';
            velocityRuleRecordSpecialty.Any_Value_Acceptable__c = false;
            velocityRuleRecordSpecialty.Tier__c = 'Gold';
            velocityRuleRecordSpecialty.Theater__c = 'EMEA';
            velocityRuleRecordSpecialty.Required_Type__c = 'USD';
            velocityRulesRecords.add(velocityRuleRecordSpecialty);
        }
        if(i==9){  
            Velocity_Rules__c velocityRuleRecordSpecialty = new Velocity_Rules__c();
            velocityRuleRecordSpecialty.Display_Name__c = 'UNITTEST'+i;
            velocityRuleRecordSpecialty.RecordTypeId = recordTypeTier.Id;
            velocityRuleRecordSpecialty.Cluster__c = 'APJ';
            velocityRuleRecordSpecialty.Specialty_Rule_Type__c = 'LA1';
            velocityRuleRecordSpecialty.Bucket__c = 'Technology Architect';
            velocityRuleRecordSpecialty.AND_OR__c='OR';
            velocityRuleRecordSpecialty.Any_Value_Acceptable__c = false;
            velocityRuleRecordSpecialty.Tier__c = 'Gold';
            velocityRuleRecordSpecialty.Theater__c = 'EMEA';
            velocityRuleRecordSpecialty.Required_Type__c = 'USD';
            velocityRulesRecords.add(velocityRuleRecordSpecialty);
        }
            if(i==10){  
            Velocity_Rules__c velocityRuleRecordSpecialty = new Velocity_Rules__c();
            velocityRuleRecordSpecialty.Display_Name__c = 'UNITTEST'+i;
            velocityRuleRecordSpecialty.RecordTypeId = recordTypeTier.Id;
            velocityRuleRecordSpecialty.Cluster__c = 'APJ';
            velocityRuleRecordSpecialty.Specialty_Rule_Type__c = 'LA1';
            velocityRuleRecordSpecialty.Bucket__c = 'Additional Requirements';
            velocityRuleRecordSpecialty.AND_OR__c='OR';
            velocityRuleRecordSpecialty.Any_Value_Acceptable__c = false;
            velocityRuleRecordSpecialty.Tier__c = 'Gold';
            velocityRuleRecordSpecialty.Theater__c = 'EMEA';
            velocityRuleRecordSpecialty.Required_Type__c = 'USD';
            velocityRulesRecords.add(velocityRuleRecordSpecialty);
        }
        if(i==11){  
            Velocity_Rules__c velocityRuleRecordSpecialty = new Velocity_Rules__c();
            velocityRuleRecordSpecialty.Display_Name__c = 'Consolidate';
            velocityRuleRecordSpecialty.RecordTypeId = recordTypeTier.Id;
            velocityRuleRecordSpecialty.Cluster__c = 'NA';
            velocityRuleRecordSpecialty.Specilaity__c = 'Consolidate';
            velocityRuleRecordSpecialty.Specialty_Rule_Type__c = 'APJ-I';
            velocityRuleRecordSpecialty.Bucket__c = 'SP Scorecard Revenue';
            velocityRuleRecordSpecialty.AND_OR__c='OR';
            velocityRuleRecordSpecialty.Any_Value_Acceptable__c = false;
            velocityRuleRecordSpecialty.Tier__c = 'Affiliate';
            velocityRuleRecordSpecialty.Theater__c = 'EMEA';
            velocityRuleRecordSpecialty.Required_Type__c = 'USD';
            velocityRulesRecords.add(velocityRuleRecordSpecialty);
        }

    }
    Insert velocityRulesRecords;
      system.debug('-----velocityRuleRecordSpecialty.Bucket__c---' +velocityRulesRecords[11].Bucket__c);
      
    List<Velocity_Rules__c> updatedLst = new List<Velocity_Rules__c>();
    for(Velocity_Rules__c tempObj : velocityRulesRecords){
        if(tempObj.Id != velocityRulesRecords[5].Id){
            tempObj.Velocity_Rule_1__c = velocityRulesRecords[5].Id;
        }
        if(tempObj.Id != velocityRulesRecords[4].Id){
            tempObj.Velocity_Rule_2__c = velocityRulesRecords[4].Id;
        }
        if(tempObj.Id != velocityRulesRecords[3].Id){
            tempObj.Group_Rule_3__c = velocityRulesRecords[3].Id;
        }
        if(tempObj.Id != velocityRulesRecords[2].Id){
            tempObj.Group_Rule_4__c = velocityRulesRecords[2].Id;
        }
        if(tempObj.Id != velocityRulesRecords[1].Id){
            tempObj.Group_Rule_5__c = velocityRulesRecords[1].Id;
        }
        updatedLst.add(tempObj);
    }
    update updatedLst;
        
    //Creating list of velocity rule Ids.
    List<Id> velocityRulesId = new list<Id>();
    for (Integer j=0; j<=11; j++) {
        velocityRulesId.add(velocityRulesRecords[j].Id);
    }
      
    //Creation of Education Group record.
    List<Velocity_Rule_Member__c> lstVRM = new List<Velocity_Rule_Member__c>();
    
    lstVRM.add(new Velocity_Rule_Member__c (Group__c = educationGroup[0].Id, Speciality_Rule__c = velocityRulesRecords[0].Id));
    lstVRM.add(new Velocity_Rule_Member__c (Group__c = educationGroup[0].Id, Speciality_Rule__c = velocityRulesRecords[2].Id));
    lstVRM.add(new Velocity_Rule_Member__c (Group__c = educationGroup[0].Id, Speciality_Rule__c = velocityRulesRecords[4].Id));
    lstVRM.add(new Velocity_Rule_Member__c (Group__c = educationGroup[0].Id, Speciality_Rule__c = velocityRulesRecords[5].Id));
    insert lstVRM;
      
    //Creating list of Velocity Rule Results.
    List<Velocity_Rule_Results__c> lstVelocityRuleResult = new List<Velocity_Rule_Results__c>();
    for(integer i = 0; i<=11; i++)
    {  
        lstVelocityRuleResult.add(new Velocity_Rule_Results__c (Revenue_Amount__c = '1', AccountID__c = lstAccount[0].Id, Grouping__c = grouping.Id,  Speciality_RuleID__c = velocityRulesRecords[i].Id  )); 
    }
    
    Insert lstVelocityRuleResult;
     
    List <EMCException> errors = new List <EMCException>();
    errors.add(new EMCException('Test Error Message','1111',new list<string>{'test1','test2'}));
     
    String ruleResultQuery1 = 'Select Id, Grouping__c,Batch_Job_Operation__c,Specialty_Rule_Type__c,Cluster__c from Account e where e.Name = \'UNITTESTAcc\''; 
    
    String ruleResultQuery2 = 'Select Revenue_Amount__c, Required_value_Type__c,v.Speciality_RuleID__c, v.Rule_theater__c, v.RequiredCount__c,Speciality_RuleID__r.RecordType.Name ,Speciality_RuleID__r.RecordType.DeveloperName,'
                              + 'v.Name, v.Grouping__c, v.Current__c, v.Account_Theater__c,Speciality_RuleID__r.RecordTypeId ,Speciality_RuleID__r.Bucket__c,'
                              + 'Speciality_RuleID__r.AND_OR__c, Speciality_RuleID__r.Velocity_Rule_2__c, Speciality_RuleID__r.Velocity_Rule_1__c,'
                              + 'Speciality_RuleID__r.Group_Rule_3__c, Speciality_RuleID__r.Group_Rule_4__c, Speciality_RuleID__r.Group_Rule_5__c, '
                              + 'Speciality_RuleID__r.BR_Sub_category__c,Speciality_RuleID__r.Display_Name__c,'
                              + 'Speciality_RuleID__r.Required_Type__c,Speciality_RuleID__r.Field_Name__c,Speciality_RuleID__r.Specialty_Rule_Type__c,Speciality_RuleID__r.Theater__c,Speciality_RuleID__r.Any_Value_Acceptable__c,'
                              + 'Speciality_RuleID__r.Specilaity__c,Speciality_RuleID__r.Tier__c,Speciality_RuleID__r.Is_Total_Revenue__c,Speciality_RuleID__r.Name,'
                              + 'v.AccountID__c From Velocity_Rule_Results__c v ORDER BY v.Grouping__c limit 200' ;
                                 
    Test.StartTest();
    
    /* Calling batch class PRM_VPP_VelocityRuleEvaluatorJob*/ 
    id batchinstanceid2 = database.executeBatch(new PRM_VPP_VelocityRuleEvaluatorJob());
    id batchinstanceid3 = database.executeBatch(new PRM_VPP_VelocityRuleEvaluatorJob(ruleResultQuery2));
    
     /* Calling batch class PRM_VPP_VelocityRuleResultJob1*/ 
    id batchinstanceid4 = database.executeBatch(new PRM_VPP_VelocityRuleResultJob1());      
    id batchinstanceid5 = database.executeBatch(new PRM_VPP_VelocityRuleResultJob1(ruleResultQuery1));

    Set<Id> tempId = new Set<Id>();
    for(Velocity_Rule_Results__c obj : lstVelocityRuleResult){
        tempId.add(obj.id);
    }
    List<Velocity_Rule_Results__c> vrrs= [ Select Revenue_Amount__c, Required_value_Type__c,v.Speciality_RuleID__c, Speciality_RuleID__r.Local_Requirment_Set__c,v.Rule_theater__c, status__c,v.RequiredCount__c,Speciality_RuleID__r.RecordType.Name , Speciality_RuleID__r.RecordType.DeveloperName, v.Name, v.Grouping__c, 
    v.Current__c, v.Account_Theater__c,Speciality_RuleID__r.RecordTypeId ,Speciality_RuleID__r.Bucket__c, Speciality_RuleID__r.AND_OR__c, Speciality_RuleID__r.Velocity_Rule_2__c, Speciality_RuleID__r.Velocity_Rule_1__c, Speciality_RuleID__r.Group_Rule_3__c, Speciality_RuleID__r.Group_Rule_4__c, 
    Speciality_RuleID__r.Group_Rule_5__c, Speciality_RuleID__r.Cluster__c, Speciality_RuleID__r.BR_Sub_category__c, Speciality_RuleID__r.Display_Name__c, Speciality_RuleID__r.Required_Type__c,Speciality_RuleID__r.Field_Name__c, Speciality_RuleID__r.Any_Value_Acceptable__c, 
    Speciality_RuleID__r.Specialty_Rule_Type__c,Speciality_RuleID__r.Theater__c,Speciality_RuleID__r.Specilaity__c,Speciality_RuleID__r.Tier__c, Speciality_RuleID__r.Is_Total_Revenue__c,Speciality_RuleID__r.Name,v.AccountID__c,VS_Rule_Result_Section__c, VS_Rule_Result_Display_Name__c 
    From Velocity_Rule_Results__c v where ID IN : tempId ORDER BY v.Grouping__c ];
    
    //Calling class PRM_VPP_RuleEvaluator
    PRM_VPP_RuleEvaluator obj1 = new PRM_VPP_RuleEvaluator(lstAccount);
    
    Set<Id> setId = obj1.evaluateGroupRule(velocityRulesRecords[0],lstVelocityRuleResult[0]); 
    Set<Id> setId1 = obj1.evaluateGroupRule(velocityRulesRecords[1],lstVelocityRuleResult[1]);
    
    Integer text = obj1.evaluateSpecialtyRule(velocityRulesRecords[1],lstVelocityRuleResult[1]);
    Integer text1 = obj1.evaluateSpecialtyRule(velocityRulesRecords[0],lstVelocityRuleResult[0]);
    
    Account acc1 = obj1.evaluateRollupRules(velocityRulesRecords[4],lstVelocityRuleResult[4]);
    
    obj1.evaluateVelocityRule(vrrs);
    obj1.evaluateVelocityRulewitherrors(vrrs,'2222',errors);
    
    list<account> acc = new list<account>();
    acc.add(lstAccount[0]);
    obj1.evaluateRollupRules(acc);
    obj1.evaluateRollupRuleswitherrors(acc, '2222',errors );
    
    lstAccount[0].Advanced_Consolidate_Compliance_Status__c='Yes';
    lstAccount[1].Backup_and_Recovery_Compliance_Status__c='Yes'; 
    lstAccount[2].Consolidate_Compliance_Status__c='Yes'; 
    lstAccount[3].Governance_and_Archive_Compliance_Status__c='Yes'; 
    lstAccount[0].Solution_Center_Implemented__c = 'Yes';
    lstAccount[1].Velocity_Services_Implement__c = 'Yes';
    update lstAccount;
    
    obj1.isVelocityProgram(lstAccount[0]);
    obj1.isVelocityProgram(lstAccount[1]);
    obj1.isVelocityProgram(lstAccount[2]);
    obj1.isVelocityProgram(lstAccount[3]);
    
    obj1.isSSCP(lstAccount[0]);
    obj1.isVSI(lstAccount[1]);
    obj1.isTraining(velocityRulesRecords[4],lstVelocityRuleResult[4]);
    string Text2 = obj1.isAdditional(velocityRulesRecords[4],lstVelocityRuleResult[4], lstAccount[0]);
    
    String revenueRangeResult = obj1.getRevenueRange(vrrs[0]);
    
    Map<String,String> mapstrString = obj1.checkComplianceStatus(lstAccount[0], vrrs);
    
    Map<Id,List<Velocity_Rule_Results__c>> mapAccountIdVelocityResults= new Map<Id,List<Velocity_Rule_Results__c>>();
    mapAccountIdVelocityResults.put(lstAccount[0].Id, vrrs);
    Map<Id,List<Velocity_Rule_Results__c>> mapIdLst = obj1.getTierComplianceValues(mapAccountIdVelocityResults);
    
    String eCBR = obj1.evaluateCloudBuilderRule(velocityRulesRecords[4],lstVelocityRuleResult[4]);
    
    String eSRRS = obj1.getServiceRuleResultStatus(velocityRulesRecords[4] , lstVelocityRuleResult[4]);
    
    Account tempAcc = obj1.updateCompilanceStatus(lstAccount[0], vrrs);
    
    Map<id,Velocity_Revenue__c> mp=new Map<id,Velocity_Revenue__c>();  
    Velocity_Revenue__c objVR = new Velocity_Revenue__c();
    objVR.Partner_Profiled_Account__c = lstAccount[0].Id;
    objVR.Solution_Provider_Revenue__c = 1234;
    insert objVR;
    mp.put(lstAccount[0].Id, objVR);
    
    String Text3 = obj1.getRevenueforTierRules(velocityRulesRecords[11], lstAccount[0], mp );
    obj1.getRevenueforLocalReqTierRules(velocityRulesRecords[11],lstAccount[0],mp);
    
    Map<String,boolean> complianceValues = new Map<String,boolean> ();
    complianceValues.put('Test0' , false);
    obj1.getComplianceStatus(complianceValues, 'Test0', vrrs[0]);
    obj1.getComplianceStatus(complianceValues, 'Test0', vrrs[4]);
    
    Map<Id,List<Velocity_Rule_Results__c>> mapAccIdVrr = obj1.fetchVelocityRuleResults(lstAccount);
    
    Test.StopTest();
    }
}