/*========================================================================================+
 | HISTORY                                                                                |
 |                                                                                        |
 | DATE        DEVELOPER   WR      DESCRIPTION                                            |
 | ==========  ==========  ======  ===========                                           
 | 08/13/2013  Y. Salazar  286622  Initial creation. Controller for Executive Briefing  
 |                                 Customer Profile distribution via EBC Application Site 
 | 02/10/2014  Abinaya             Added one more filter in briefingInvitees(Attending__c = true).
 | 06/12/2014  Abinaya     CI:269  send Briefing Center Time Zone instead of user TimeZone to EBC_SessionSort class to format Session timing.
 | 24/07/2014  Bisna               Reverting the changes made for CI:269   
 | 19/02/2015  Vinod Jetti  #1649  Replaced field 'Emc_classification__c'to 'Emc_classification_Hub__c'
 +=======================================================================================*/

public class EBC_InformationDistribution {
  private ApexPages.StandardController controller;
  public EBC_Briefing_Event__c briefingEvent {get;set;}
  public Contact accountExecutive {get;set;}
  public string createdBy {get;set;}
  public List<EBC_Invitees__c> briefingInvitees {get;set;}
  public Map<Id,EBC_Session__c> sessions {get;set;}
  public List<EBC_Session_Presenter__c> sessPresenters {get;set;}
  public List<EBC_SessionSort> sessionWrapper {get;set;}  
  
  public EBC_InformationDistribution(ApexPages.StandardController controller){
    this.controller = controller;
    
    if(ApexPages.currentPage().getParameters().get('qet') == 'true'){
    briefingEvent = [SELECT Id,Objective_of_the_Visit_picklist__c , Name, Requestor_Name__c, Start_Date__c,Sales_Rep_Cell__c,End_Date__c, Briefing_Center__r.Name, Briefing_Type__c,
      Division__c, Briefing_Advisor__r.Name, Area__c, Logistic_Advisor__r.Name, District__c, Jet_Service__c, Customer_Name__r.Name,
      Customer_Name__r.Address__c, Customer_Name__r.Website, Customer_Name__r.Industry__c, Customer_Name__r.EMC_Classification_Hub__c,
      EMC_Major_Vertical__c, EMC_Sub_Vertical__c, Under_Pen_Account_Type__c, Channel_Leverage_Account__c, Dell_Black_Account__c,
      NTApp_100_Account__c, Program_Geography__c, MOJO_Account__c, Net_New_Account__c, Global_Account__c, Year_1__c, Year_2__c,
      Year_3__c, Year_4__c, Year_1_Spend__c, Year_2_Spend__c, Year_3_Spend__c, Year_4_Spend__c, Executive_Sponsor_on_BE__c,
      Current_Opportunity_Size__c, Account_Designation__r.Name, Account_Designation__r.Account_Category__c, Objective_of_the_Visit__c,
      Customer_Spend_with_EMC_over_3_years__c, What_is_estimated_spend_for_fiscal_year__c, Customer_Business_Description__c,
      Customer_s_Relationship_with_EM__c, Briefing_Day_Objective__c, Customers_critical_issues__c, Describe_Competitive_Threats__c,
      Applications__c, Platforms__c, Critical_Data_Center_Information__c, EMC_Products_Installed__c, Customer_Name__r.BillingStreet,
      Customer_Name__r.BillingCity,Briefing_Center__r.Briefing_Center_Time_Zone__c, Customer_Name__r.BillingState, Customer_Name__r.BillingPostalCode, Customer_Name__r.BillingCountry,
      CreatedById,Briefing_Venue__r.Name FROM EBC_Briefing_Event__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    
    if(briefingEvent.Requestor_Name__c != null){  
      accountExecutive = [SELECT Name, MobilePhone, Phone, Cost_Center__c FROM Contact WHERE Id = :briefingEvent.Requestor_Name__c];
    }    
    briefingInvitees = [SELECT Id, Name, Invitee__c,Attending__c, Briefing_Team_Role__c, Key_Decision_Maker__c, EBC_Title__c, Company__c, Customer_Notes__c FROM EBC_Invitees__c WHERE Briefing_Event__c = :briefingEvent.Id and Attending__c = true];
    sessions = new Map<Id,EBC_Session__c>([SELECT Id, Topic_Override__c, Session_Start_Time__c, Session_End_Time__c, Concatenated_Presenter_Information__c, Requester_Notes__c FROM EBC_Session__c WHERE Briefing_Event__c = :briefingEvent.Id]);
    sessPresenters = [SELECT Id, Presenter_EBC_Name__c, Presenter_EBC_Title__c, Session__c FROM EBC_Session_Presenter__c WHERE Session__c IN :sessions.keySet()];
    
    //CI:269 Update Briefing Center Time Zone instead of User TimeZone.
    /*if(briefingEvent.Briefing_Center__r.Briefing_Center_Time_Zone__c!=null){
        createdBy = briefingEvent.Briefing_Center__r.Briefing_Center_Time_Zone__c;
    }else{*/
        createdBy = [SELECT Id, TimeZoneSidKey FROM User WHERE Id = :briefingEvent.CreatedById].TimeZoneSidKey;
   // }
    sessionWrapper = new List<EBC_SessionSort>();
    for(EBC_Session__c sess:sessions.values()){
      sessionWrapper.add(new EBC_SessionSort(sess, sessPresenters, createdBy));
    }
    sessionWrapper.sort();
    }    
    else{
    briefingEvent = [SELECT Id, Name FROM EBC_Briefing_Event__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    } 
  }
}