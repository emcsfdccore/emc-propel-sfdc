/**
===========================================================================+
 |  Created History                                                                  
                                                                 
 |  DATE        DEVELOPER       WORK REQUEST            DESCRIPTION                               
 |  ====        =========       ============            =========== 
 |  4 Mar 2014  Avinash K       Oppty Collaboration     This class is a does all the validation rule checks before redirecting the user to
                                                        create a new Opportunity Registration record to share the Opportunity with Partners
 |  24 Mar 2014 Shalabh Sharma  Oppty Collaboration     Updated code to fetch field ids from custom setting 
 |  10 May 2014 Avinash K       Oppty Collaboration     Updated code to encode URL parameters to (Breakfix)                                                    
=============================================================================+
**/

public class OPPTY_COLLABORATION_ValidationController 
{
    private Opportunity opp;

    public String strError {get; set;} {strError = '';}
    private Map<String,CustomSettingDataValueMap__c> mapData = CustomSettingDataValueMap__c.getAll();

    
/**
Constructor
Gets the Opportunity (and some fields) for validation from which the "New Opportunity Registration" button was clicked 
**/
    public OPPTY_COLLABORATION_ValidationController (ApexPages.StandardController controller) 
    {
        opp = (Opportunity)controller.getRecord();

        List<Opportunity> lstOpp = [select name, id, of_Active_Registrations__c, Lead_Level__c, Budget__c, Business_Need__c, Executive_Sponsor_Key_Players__c, Partner__c, Tier_2_Partner__c, Timeline__c, Partner__r.Name, Tier_2_Partner__r.Name, Sales_Channel__c, Partner_Approval_Status__c, Linked_to_Deal_Registration__c, Product_Count__c, Opportunity_Registration_Status__c 
                                        FROM Opportunity
                                        WHERE id = :opp.id
                                        LIMIT 1];

        if (lstOpp != null && lstOpp.size() > 0) 
        {
            opp = lstOpp.get(0);
        }
    }

    
/**
validate()
Checks for all the validations for the Opportunity before creation of an Opportunity Registration record and displays the error messages picking them from the Custom Settings
**/
    public PageReference validate()
    {
        PageReference redirect;

        strError = '';

        if (opp.of_Active_Registrations__c != null && opp.of_Active_Registrations__c == 0) 
        {
            if(!opp.Linked_to_Deal_Registration__c)
            {
                if(opp.Product_Count__c == null || opp.Product_Count__c == 0)
                {
                    strError += System.Label.OpptyCollabProductValidationMessage;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollabProductValidationMessage));
                }

                if(opp.Sales_Channel__c == null || !opp.Sales_Channel__c.equals('Indirect'))
                {
                    strError += System.Label.OpptyCollabChannelValidationMessage;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollabChannelValidationMessage));

                }

                if(opp.Sales_Channel__c != null && opp.Sales_Channel__c.equals('Indirect') && opp.Tier_2_Partner__c == null && opp.Partner__c == null)
                {
                    strError += System.Label.OpptyCollabAccountValidationMessage;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollabAccountValidationMessage));
                }

                if(opp.Lead_Level__c != 'Level 4')
                {
                    strError += System.Label.OpptyCollabLeadLevelValidationMessage;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollabLeadLevelValidationMessage));
                }
                    
                if(opp.Business_Need__c == null || opp.Business_Need__c.equals(''))
                {
                    strError += System.Label.OpptyCollabBusinessValidationMessage;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollabBusinessValidationMessage));
                }
                    
                if(opp.Budget__c == null || opp.Budget__c.equals(''))
                {
                    strError += System.Label.OpptyCollabBudgetValidationMessage;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollabBudgetValidationMessage));
                }
                    
                if(opp.Timeline__c == null || opp.Timeline__c.equals(''))
                {
                    strError += System.Label.OpptyCollabTimelineValidationMessage;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollabTimelineValidationMessage));
                }
                    
                if(opp.Executive_Sponsor_Key_Players__c == null || opp.Executive_Sponsor_Key_Players__c.equals(''))
                {
                    strError += System.Label.OpptyCollabExecutiveValidationMessage;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollabExecutiveValidationMessage));
                }
                
            }
            else
            {
                strError += System.Label.OpptyCollabDealRegValidationMessage;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollabDealRegValidationMessage));
            }
        }

        else
        {
            strError += System.Label.OpptyCollabSharedValidationMessage;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.OpptyCollabSharedValidationMessage));
        }

        

        
        if (strError == null || strError.equals('')) 
        {
            String strParamOppName = PageUtils.encodeVariable(opp.Name);

            String strURL = '';
            
            strURL = '/'+mapData.get('OpptyCollab_RegID').Datavalue__c+'/e?'+mapData.get('OpptyCollab_Related Opportunity').Datavalue__c+'='+strParamOppName+'&'+mapData.get('OpptyCollab_Related Opportunity').Datavalue__c+'_lkid='+ opp.id +'&retURL=%2F'+opp.id;

            if (opp.Partner__c != null) 
            {
                String strParamDistResellerName = PageUtils.encodeVariable(opp.Partner__r.Name);

                strURL += '&'+mapData.get('OpptyCollab_Distributor').Datavalue__c+'_lkid='+opp.Partner__c+'&'+mapData.get('OpptyCollab_Distributor').Datavalue__c+'='+strParamDistResellerName;
            }
            
            if (opp.Tier_2_Partner__c != null) 
            {
                String strParamDistVARName = PageUtils.encodeVariable(opp.Tier_2_Partner__r.Name);

                strURL += '&'+mapData.get('OpptyCollab_Distribution VAR').Datavalue__c+'_lkid='+opp.Tier_2_Partner__c+'&'+mapData.get('OpptyCollab_Distribution VAR').Datavalue__c+'='+strParamDistVARName;
            }
            
            redirect = new PageReference(strURL);

            return redirect;
        }
        
        return null;
    }

/**
Back()
Method that redirects the user back to Opportunity Detail page
**/
    public PageReference Back()
    {
        PageReference redirect = new PageReference('/' + opp.id);
        return redirect;
    }

/**
Edit()
Method that redirects the user back to Opportunity Edit page
**/
    public PageReference Edit()
    {
        PageReference redirect = new PageReference('/' + opp.id+'/e?retURL=/'+opp.id);
        return redirect;
    }

}