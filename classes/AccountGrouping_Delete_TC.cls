@isTest (SeeAllData=true)
private class AccountGrouping_Delete_TC {

    static testMethod  void AccountGrouping_Delete(){
        
        test.startTest();
        
        /*ID sysid = [ Select id from Profile where name ='Solution Delivery Team' limit 1].id;
        
        User user = new User( email='test-user@fakeemail.com',profileid = sysid ,  UserName='test-user@fakeemail.com', alias='tuser1', CommunityNickName='tuser1', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' );
        insert user;
        
        Account_Groupings__c ac= new Account_Groupings__c();
        ac.Active__c=true;        
        ac.Name='abcd';
        ac.Account__c='abcde';
        ac.EMC_Account_Contact__c=user.id;        
        insert ac;
        */
        Map<Id,Account_Groupings__c> mapold=new Map<Id,Account_Groupings__c>([Select id,Name,Active__c,Inactivation_Reason__c from Account_Groupings__c where Active__c=true limit 1]);
        Map<Id,Account_Groupings__c> mapnew=new Map<Id , Account_Groupings__c>(); 
                                               
        Account_Groupings__c ac= new Account_Groupings__c();
        
        ac.id=[Select id,Name from Account_Groupings__c where Active__c=true limit 1].id;
        ac.Active__c=false;  
        ac.Inactivation_Reason__c='No longer a partner';
        //update ac;
        
       mapnew.put(ac.id,ac);
       
       AccountGrouping_Delete agd= new AccountGrouping_Delete();
       agd.reportingFieldUpdation(mapold,mapnew);
       test.stopTest();
    }
}