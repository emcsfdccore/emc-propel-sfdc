/*======================================================================================+
 |  HISTORY  |                                                                         
 |  DATE          DEVELOPER             WR     DESCRIPTION                             
 |  ====          =========             ==     =========== 
 |  02/14/2013  Anand Sharma                      System Util Jobs 
 |  17/09/2014  Bisna         CI 1072  Added "Holding" as a filter in the query against AsyncApexJob 
 +=======================================================================================*/
global class SystemAdminAccessJob implements Database.Batchable<SObject>,Database.Stateful, Schedulable{
    global String Query = 'select id from user where isActive=true limit 1';
    
    public Set<Id> setRemoveSysAdminUserIds = new Set<Id>();
    public Set<Id> setGrantSysAdminAccess = new Set<Id>();  
    public Set<Id> setUpdateSysAdminReqLogIds = new Set<Id>();
    public boolean flagForAllUserRemoveAccess = false;
    public boolean isProduction = true;
    
    global SystemAdminAccessJob(){
        system.debug('DEFAULT - QUERY ### :' + query );  
    }
    
    global SystemAdminAccessJob(String query){
        this.Query = query ; 
    }
    
    global SystemAdminAccessJob(Boolean flagForAllUserRemoveAccess){ 
        this.flagForAllUserRemoveAccess = flagForAllUserRemoveAccess;
        system.debug('flagForAllUserRemoveAccess :' + flagForAllUserRemoveAccess );
        system.debug('DEFAULT - QUERY ### :' + query );                      
    }
    
    //create batch of records which is passed to execute method for processing 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(Query); 
    }
    
    // Deleting the records.  
    global void execute(Database.BatchableContext BC, LIST<SObject> scope) {
        isProduction = SystemAdministration_Utility.getEnviromentAsProduction();
        if(isProduction){
            setRemoveSysAdminUserIds.addAll(SystemAdministration_Utility.deactivateSysAdminUsers()); 
            setRemoveSysAdminUserIds.addAll(SystemAdministration_Utility.revokeSysAdminAccess(flagForAllUserRemoveAccess, setRemoveSysAdminUserIds)); 
            setGrantSysAdminAccess.addAll(SystemAdministration_Utility.getUsersForSystemAdminAccessFromLog());
            SystemAdministration_Utility.updateSystemAdminUserAndReqLog(setGrantSysAdminAccess);
        }
    }

    global void finish(Database.BatchableContext BC){
        // Revoke and Grant System admin profile   
        util.fromuserattributemapping = true;         
        if(isProduction){
            SystemAdministration_Utility.grantAndRevokeAccessToUser(setRemoveSysAdminUserIds, setGrantSysAdminAccess, flagForAllUserRemoveAccess);
        }else{
            SystemAdministration_Utility.grantAccessToAllEligibleUsers();
        }          
    } 
    
    //Execute batch class with default constructor from scheduler   
    global void execute(SchedulableContext sc){ 
        if(! SystemAdministration_Utility.getEnviromentAsProduction()) {
            try {
                //Abort the existing schedule 
                CronTrigger ct = [SELECT id,CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :SC.getTriggerId()];
                if(ct != null) {  
                    System.abortJob(ct.Id);
                }
            } catch (Exception e) {
                System.debug('There are no jobs currently scheduled. ' + e.getMessage()); 
            }
        }
        //1072 adding 'Holding' in filter    
        List<AsyncApexJob> activejobs = [SELECT Id, Status,ApexClassId,ApexClass.Name from AsyncApexJob WHERE ApexClass.Name like'%AdminAccessJob%' and (status='Queued' or status='Processing' or status='Preparing' or status='Holding')];
        system.debug('--------------'+activejobs.size());
        if(activejobs!= null && activejobs.size() > 0){
            //Abort the existing Batch job 
            for(AsyncApexJob a:activejobs){
                system.abortJob(a.id);
            }           
            system.debug('----------------'+activejobs.size());         
        }
 
 
        Database.executeBatch(new SystemAdminAccessJob());
    }
}