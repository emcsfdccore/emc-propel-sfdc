@istest
 public class GBS_CommunityHelpTrainingCtrl_TC
 {
     
     public static testMethod void GBS_CommunityHelpTrainingCtrlTM()
     {
       System.runAs(new user(Id = UserInfo.getUserId()))
            {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.partnerBoardingCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
            CustomSettingDataHelper.VCEStaticCSData();
            CustomSettingDataHelper.PSCFieldMappingData();
            CustomSettingDataHelper.GBSConfigData();
            }
        /* //Create Account
        List<Account> lstAccount = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();       
        insert lstAccount;
        
        //Create Contact
        Contact testContact = new Contact(Lastname = 'Test Contact', AccountId = lstAccount[0].Id);
        insert testContact;
        
        
            list<CollaborationGroup> listCG1 = new list<CollaborationGroup>();
           listCG1.add( new CollaborationGroup (name = 'GBS Community Help for External Users', CollaborationType = 'Public'));
           listCG1.add( new CollaborationGroup (name = 'GBS Community Help for Employees', CollaborationType = 'Public'));
           insert listCG1 ;
           
       Profile userProfile = [Select Id, Name from Profile where name like '%Community Power User%' limit 1];
       User insertUser = new user(email='test-user@emailTest.com',contactID = testContact.id, profileId = userProfile.id,  UserName='testR2Ruser1@emailR2RTest.com', alias='tuser1', CommunityNickName='tuser1', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' ,Forecast_Group__c = 'Maintenance Renewals'); 
           
           System.runAs(insertUser )
           {
           GBS_CommunityHelpTrainingCtrl test = new GBS_CommunityHelpTrainingCtrl();
           
          CollaborationGroup cGrp1 = new CollaborationGroup ();
          cGrp1.id = listCG1[0].id ;
           
           test.getGroupId();
           } */
           
           GBS_CommunityHelpTrainingCtrl test1 = new GBS_CommunityHelpTrainingCtrl();
           
           CollaborationGroup CG1 = new CollaborationGroup();
           CG1 .name = 'GBS Community Help for Employees';
           CG1.CollaborationType = 'Public';
           insert CG1;
           
           test1.getGroupId();
 }
 }