/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  7 Oct 2014      Partha Baruah  302/380      Created for deletion of assets with "marked for deletion" field's value set as yes
 | 
 +=========================================================================================================================*/

@isTest
public Class R2R_AssetDeletion_Batch_TC
{
    
    public static testmethod void R2R_AssetDeletion_Batch()
    {
        //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        }   
        //Creating Account
        Account acc= R2R_datahelper.createAccount();
        insert acc;

        //Creating Query from recordtype
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where Name like 'EMC_Install' or Name Like 'Competitive_Install'];

        //Creating assets
        List<Asset__c> assetList = new List<Asset__c>();
        //Calling helper class method by passing account object
        assetList = AssetTestClassDataHelper.createAsset(acc);
        //Change for asset1
        if(recordType[0].Name == 'Competitive_Install'){
            assetList[0].RecordTypeId=recordType[0].id;
            assetList[0].Marked_for_Deletion__c= 'No';
            assetList[1].RecordTypeId=recordType[0].id;
            assetList[1].Marked_for_Deletion__c= 'No' ;
        }
        else{
            assetList[0].RecordTypeId=recordType[1].id;
            assetList[0].Marked_for_Deletion__c= 'No';
            assetList[1].RecordTypeId=recordType[1].id;
            assetList[1].Marked_for_Deletion__c='No' ;
        }
        //Change for asset3
        assetList[2].RecordTypeId=recordType[0].id;
        assetList[2].Marked_for_Deletion__c= 'No' ;
        
        insert assetList;
        assetList[0].Marked_for_Deletion__c='Yes' ;
        assetList[2].Marked_for_Deletion__c='Yes' ;
        assetList[2].Marked_for_Deletion__c='No' ;
        update  assetList ;
        
        //Creating Lead
        List<Lead> leadList = new List<Lead>();
        
        leadList = LeadTestClassDataHelper.createLead();
        insert leadList;
        
        //creating list lead asset junction object
        List<Lead_Asset_Junction__c> listlaj= new List<Lead_Asset_Junction__c>();

        //Creating Lead asset junction 
        Lead_Asset_Junction__c laj1= new Lead_Asset_Junction__c();
        laj1.Related_Asset__c=assetList[0].id;
        laj1.Related_Lead__c=leadList[0].id;
        listlaj.add(laj1);
    
        insert listlaj;
        
        Database.BatchableContext bc;
        Test.startTest();
            R2R_AssetDeletion_Batch Assobj=new R2R_AssetDeletion_Batch(); 
            Assobj.batchQuery= Assobj.batchQuery + 'Limit 10' ;
            ID batchprocessid = Database.executeBatch(Assobj);
            Assobj.execute(bc,assetList);
            Assobj.finish(bc);
        Test.stopTest();
    }
}