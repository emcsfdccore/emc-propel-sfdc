/*=====================================================================================================+
|  HISTORY  |                                                                            
|  DATE          DEVELOPER                WR            DESCRIPTION                               
|  ====          =========                ==            ===========                                                     
| 17/11/2014    Garima Bansal     R2R Dec'14 Rel        Implemented Schedulable interface for scheduling the batch class to Uncheck the “Defer Asset” checkbox 
+=====================================================================================================*/

public class R2R_AssetUncheckDefer_Batch implements Database.Batchable<SObject> , Schedulable{
       
    public string batchQuery = '';  
    String emcInstallRT = 'EMC Install';
    String emcDeinstallRT= 'EMC De-Install';
    public String sysDate = String.valueOf(Date.Today());
    
    //Added to schedule the class from UI
    public void execute(SchedulableContext sc) {      
      
        Id batchProcessId = Database.executeBatch(new R2R_AssetUncheckDefer_Batch(), 20);
    }
    
    public R2R_AssetUncheckDefer_Batch (){

        //This query returns the assets (Install or De-Install) with "Deferral_Date__c" in past and Defer Asset checkbox = True
        batchQuery = 'select id,Deffered_to_Renewals__c,Deferral_Date__c from Asset__c where (RecordType.Name = \'' + emcInstallRT + '\' or RecordType.Name =  \'' + emcDeinstallRT + '\' )and Deferral_Date__c < ' + sysDate + ' and Deffered_to_Renewals__c = True ';
    }
    
    /*@Method <This method gets executed automatically when the batch job is started.>
    @param <Database.BatchableContext BC - Batchable context>
    @return <void> - <Not returning anything>
    @throws exception - <No Exception>
    */
    public Database.QueryLocator start(Database.BatchableContext BC){
            System.debug('batchQuery--->'+batchQuery);
            return Database.getQueryLocator(batchQuery);
    }
    
    /*@Method <This method gets executed automatically when the batch job is execution mode.>
    @param <Database.BatchableContext BC - Batchable context>
    @param <List<Sobject> scope - scope>
    @return <void> - <Not returning anything>
    @throws exception - <No Exception>
    */
    public void execute(Database.BatchableContext BC, list<SObject> scope){
        List<Asset__c> assetDeferralDateList = (Asset__c[]) scope;
        
        if(assetDeferralDateList.size()>0){
            for(Asset__c assetObj : assetDeferralDateList){
                if(assetObj.Deffered_to_Renewals__c){
                    assetObj.Deffered_to_Renewals__c = false;
                }   
            } 
        }
        update assetDeferralDateList;
    }
    
    /*@Method <This method gets executed acutomatically when the batch job is finised. We are deleting the job at the end.>
    @param <Database.BatchableContext BC - Batchable context>
    @return <void> - <Not returning anything>
    @throws exception - <No Exception>
    */
    public void finish(Database.BatchableContext BC){      
   
    }   
}