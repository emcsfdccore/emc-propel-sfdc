@isTest
Public class Cases_PSC_Site_Controller_TC
{
     public static Testmethod void CasesPSCSitetest()
    {
        System.runAs(new user(Id = UserInfo.getUserId()))
            {    
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.partnerBoardingCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData();
            CustomSettingDataHelper.VCEStaticCSData();
            CustomSettingDataHelper.PSCCaseTypeImageData();
            CustomSettingDataHelper.PSCCaseTypeImageSiteData();
            CustomSettingDataHelper.PSCFieldMappingData();
            CustomSettingDataHelper.PSCCasesLanguageData();
           }
        
        Test.StartTest();
        
        ApexPages.currentPage().getParameters().put('recordType', 'Business Partner Program');
        ApexPages.currentPage().getParameters().put('langForPage', null);
        Cases_PSC_Site_Controller siteCtrl = new Cases_PSC_Site_Controller();
        
        //Create Lead records.
        list<Lead> listLead = new list<lead> ();
        listLead.add(new lead(Company='TestLead' ,LastName='Test0', Status='Working',Sales_Force__c='EMC',Lead_Originator__c='Customer Intelligence')); 
        insert listLead;
        String LeadNum = [ select id, Lead_Number__c from lead where id = : listLead limit 1].Lead_Number__c ;
        
        // Adding case records.
        list<case> listcase = new list<case>();
        listcase.add(new case( Origin ='Community', STATUS = 'New', parentid = null, SUBJECT='testcase0', DESCRIPTION = 'Testing description'));
        listcase.add(new case( Origin ='Community', STATUS = 'New', parentid = null, SUBJECT='testcase1', DESCRIPTION = 'Testing description',Deal_Registration_Number__c = LeadNum));
        insert listcase;
        
        // create SC_RecordType_Mapping__c records.
        list<SC_RecordType_Mapping__c> listSCRTMap = new list<SC_RecordType_Mapping__c>();
        listSCRTMap.add(new SC_RecordType_Mapping__c(On_webform__c = true, RecordType_Subtype__c = 'PSC - Channel System Support+Channel Express', Section_Name__c ='Case Information', FieldName__c = 'test01'));
        listSCRTMap.add(new SC_RecordType_Mapping__c(On_webform__c = true, RecordType_Subtype__c = 'PSC - Channel System Support+Channel Express', Section_Name__c ='Case Information', FieldName__c = 'test01'));
        listSCRTMap.add(new SC_RecordType_Mapping__c(On_webform__c = true, RecordType_Subtype__c = 'PSC - Channel System Support+Channel Express', Section_Name__c ='Description Information', FieldName__c = 'Test02'));
        insert listSCRTMap;
        
        ApexPages.currentPage().getParameters().put('recordType', 'Channel System Support');
        ApexPages.currentPage().getParameters().put('subType', 'Channel Express');
        apexpages.standardcontroller Case1 = new apexpages.standardcontroller(listcase[0]);
        Cases_PSC_Site_Controller siteCtrl1 = new Cases_PSC_Site_Controller(Case1);
        
        ApexPages.currentPage().getParameters().put('recordType', 'Deal Registration and Lead Management');
        ApexPages.currentPage().getParameters().put('subType', 'Partner Onboarding');
        apexpages.standardcontroller Case2 = new apexpages.standardcontroller(listcase[1]);
        Cases_PSC_Site_Controller siteCtrl2 = new Cases_PSC_Site_Controller(Case2);
        siteCtrl2.getImgLogo();
        siteCtrl2.getisRenderLang();
        siteCtrl2.SaveAction();
        siteCtrl2.CancelAction();
        siteCtrl2.createInstance();
        siteCtrl2.caseSaved = true;
        siteCtrl2.saveAttachment();
        
        ApexPages.currentPage().getParameters().put('recordType', 'Channel System Support');
        ApexPages.currentPage().getParameters().put('subType', 'Partner Portal');
        apexpages.standardcontroller Case3 = new apexpages.standardcontroller(listcase[1]);
        Cases_PSC_Site_Controller siteCtrl3 = new Cases_PSC_Site_Controller(Case3);
        
        ApexPages.currentPage().getParameters().put('recordType', 'Channel System Support');
        ApexPages.currentPage().getParameters().put('subType', 'Program and Process Queries,Technical Queries');
        apexpages.standardcontroller Case4 = new apexpages.standardcontroller(listcase[1]);
        Cases_PSC_Site_Controller siteCtrl4 = new Cases_PSC_Site_Controller(Case4); 
        siteCtrl4.selectedInquiry = 'None';
        siteCtrl4.SaveAction();
        
        test.Stoptest();
    }
}