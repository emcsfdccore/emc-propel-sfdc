/*=============================================================================
    HISTORY                                                                  
                                                               
    DATE            DEVELOPER                WR             DESCRIPTION                               
    ====            =========                ==             =========== 
|  28/05/2013       Shipra Misra            255125         SFA - Account Presentation Long Term Maintenance
|  24/12/2013       Srikrishna SM           Backward Arrow Added Test.isRunningTest() condition
|  11 Feb 2014      Vivek Barange          #1751                       Modified code to restrict inactive to become CPA 
|  17 Feb 2014      Vivek Barange          #1752                       Added Lock CPA Related Functionalites
+======================================================================================================*/
global class AP_Customer_Profiling_Account_District implements Database.Batchable<sObject>
{
 public Map<String,CustomSettingDataValueMap__c> mapDataValueMap = CustomSettingDataValueMap__c.getAll();
 global set<string> setHouseAccountExcluded= new set<String>();
            
 global Database.QueryLocator start(Database.BatchableContext BC)
 {
    if(mapDataValueMap!= null &&
       mapDataValueMap.get('House Account For AP Exclusion') != null &&
       mapDataValueMap.get('House Account For AP Exclusion').DataValue__c != null )
    {
        System.debug('#### Entered batch calling area');
        String strHouseAccuontsExcluded = mapDataValueMap.get('House Account For AP Exclusion').DataValue__c;
         
        for(string s:strHouseAccuontsExcluded.split(','))
        {
            setHouseAccountExcluded.add(s);
        }
    }  
    //if(Util.isTestCoverage)
    if(Test.isRunningTest())
    {
        return Database.getQueryLocator([Select Account_District__c  From Customer_Profile_Audit_Master__c where Profiled_Account_Total__c>0 limit 2]);
    }
    else
    {
       return Database.getQueryLocator([Select Account_District__c  From Customer_Profile_Audit_Master__c where Profiled_Account_Total__c > 0]) ;
       // return Database.getQueryLocator([Select Account_District__c  From Customer_Profile_Audit_Master__c where Name= 'IDEA CELLULAR LIMITED: India West 1 District' OR Name ='SYMANTEC CORPORATION: Korea House District' ]);
    }
 }
 global void execute(Database.BatchableContext BC, List<SObject> scope)
 {
    List<Account> lstAccUpdate= new List<Account>();
    system.debug('setHouseAccountExcluded===>'+setHouseAccountExcluded);
    for(SObject sobjects:scope)
    {
      Integer count=0;
      Id customerProfiledAccount;
      set<id> accid= new set<Id>();
      Customer_Profile_Audit_Master__c AccDistrict=(Customer_Profile_Audit_Master__c)sobjects;
      string strAccDistrict=AccDistrict.Account_District__c;
      if(setHouseAccountExcluded.contains(strAccDistrict))return; 
      //#1751 - Added "Order by Account_Status__c asc" in below query
      //#1752 - Added "Order by Protect_this_party_as_CPA__c desc" in below query
      List<Customer_Profile_Audit_detail__c> CustProAudDet=new List<Customer_Profile_Audit_detail__c>([select  Account_Lookup__c, Account_ID__c,account_lookup__r.id, account_lookup__r.name,account_lookup__r.Account_District__c,account_lookup__r.Party_ID__c,account_lookup__r.Customer_Profiled_Account_Lookup__c, account_lookup__r.Customer_Profiled_Account__c, account_lookup__r.Account_Status__c from Customer_Profile_Audit_detail__c  where Customer_Profile_Audit_master__r.Account_District__c =:strAccDistrict and account_lookup__r.Customer_Profiled_Account__c=true order by account_lookup__r.Account_Status__c asc, account_lookup__r.Protect_this_party_as_CPA__c desc, account_lookup__r.Customer_Profile_Value__c Desc ,account_lookup__r.Party_ID__c Asc]);
      for(Customer_Profile_Audit_detail__c so:CustProAudDet)
      {
         accid.add(so.Account_ID__c);
         Account acc= new Account(id=so.Account_Lookup__c);
         system.debug('---count---'+count);
         if(count==0)
         {
            //#1751 - Added if condition to check inactive accounts
            if(so.account_lookup__r.Account_Status__c == 'Inactive') {
                acc.Customer_Profiled_Account__c = false;
                lstAccUpdate.add(acc);
            } else {
                customerProfiledAccount=so.Account_ID__c;
                count ++; //#1751 - Moved this line here from outside if else
            }
            system.debug('---customerProfiledAccount---'+customerProfiledAccount);
         }
         else if(count>0)
         {
            acc.Customer_Profiled_Account_Lookup__c=customerProfiledAccount;
            acc.Customer_Profiled_Account__c=false;
            lstAccUpdate.add(acc);
            count ++; //#1751 - Moved this line here from outside if else
            system.debug('---lstAccUpdate---'+lstAccUpdate);
        }
       
        system.debug('Count==>'+count);
        
      }
      
    }
    if(lstAccUpdate.size()>0)
    {
        system.debug('lstAccUpdate--->'+lstAccUpdate);
        Database.update(lstAccUpdate,false) ;
    }
 }
 global void finish(Database.BatchableContext BC)
 {
   System.debug(LoggingLevel.WARN,'Batch Process 4 Finished');
    //Build the system time of now + 20 seconds to schedule the batch apex.
  Datetime sysTime = System.now();
   sysTime = sysTime.addSeconds(20);
  String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
  system.debug(chron_exp);
   Schedule_AP_Account_Profile_Warning apAccountProfileWarning = new Schedule_AP_Account_Profile_Warning();
   //Schedule the next job, and give it the system time so name is unique
   System.schedule('AP_Batch for Account ProfileWarning' + sysTime.getTime(),chron_exp,apAccountProfileWarning);
    
 }
 
 
}