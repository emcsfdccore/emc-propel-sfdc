/*==========================================================================================================+
 |  HISTORY  |                                                                           
 |  DATE        DEVELOPER       WR      DESCRIPTION                               
 |  ====        =========       ==      =========== 
 |  12/11/2014  Sneha Jain              This class gets the last modified date of the GAF R&R record related to particular account and 
                                        stamps it on the Account field called GAF Data Refresh Date.
 |============================================================================================================*/

public class PRM_BPP_GAF_RevenueRefreshDate {
    
    public void updateProfiledAccount(Map<Id,GAF_Revenue_Rebate__c> GAF_RR_AccMap){
        
        //Query the list of Profiled Accounts to be updated with GAF timestamp value
        List<Account> accountsListToUpdate = [select GAF_Data_Refresh_Date__c from account where id in:GAF_RR_AccMap.keyset()];
        
        for(Account acct:accountsListToUpdate){
            acct.GAF_Data_Refresh_Date__c = GAF_RR_AccMap.get(acct.id).LastModifiedDate;
        }
        
        //Update account records
        if(accountsListToUpdate !=null && accountsListToUpdate.size()>0){
            update accountsListToUpdate;
        }
    }
}