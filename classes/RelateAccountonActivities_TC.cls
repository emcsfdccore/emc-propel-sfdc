/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE       DEVELOPER       WR       DESCRIPTION                                
|
   ====       =========       ==       ===========  
|  12-Apr-2013  Prachi                 Created 
+===========================================================================*/ 
    @isTest(SeeAllData = true) 
private class RelateAccountonActivities_TC {
    
     public Static List <Task> tasks=new List <Task>();
     public Static List <Event> events=new List <Event>();
     public Static List <String> contact_ids = new List <String>();
     public Static Map<String,String> activities = new Map<String,String>();
     
        
static testMethod void RelateAccountonActivities(){
    
    Test.StartTest();
    Date myDate = date.newinstance(2013, 4, 12);
    contact_ids.add('003Q000000h8ORK');
    contact_ids.add('003Q000000h8OIv'); 
    Task tsk = new Task();
    tsk.Priority='High';
    tsk.RecordTypeID='01270000000Q5Gh';
    tsk.Type='Account Plan';
    tasks.add(tsk);
    insert tasks;
    Event eve = new Event();
    eve.Type='Action Item';
    eve.DurationInMinutes=30;
    eve.ActivityDateTime=myDate ;
    events.add(eve);
    insert events;
    RelateAccountonActivities obj = new RelateAccountonActivities();
    obj.populateAccountOfContactOnTask(tasks,contact_ids);
    obj.populateAccountOfContactOnEvent(events,contact_ids);
    RelateAccountonActivities.populateAccountOfLeadOnTask(activities);
    RelateAccountonActivities.populateAccountOfLeadOnEvenet(activities);
    Test.StopTest();
}
}