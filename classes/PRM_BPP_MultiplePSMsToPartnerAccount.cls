/*=====================================================================================================+
|  HISTORY  |
|  DATE          DEVELOPER               WR            DESCRIPTION 
|  ====          =========               ==            =========== 
|  02/Sep/2014  Srikrishna SM       BPP Project       Class created for Assigning Multiple PSM's to Partner Account logic.
|                                                     Handles logic for adding, updating and removing PSM's
+=====================================================================================================*/
public class PRM_BPP_MultiplePSMsToPartnerAccount {
  
    //After insert Logic
    public static void onInsertPSMLogic(List<Additional_Partner_Sales_Managers__c> newPSMList){

        //Construct a Set of Account Ids
        Set<Id> accIDSet = new Set<Id>();
        Set<Id> userIDSet = new Set<Id>();
        
        for(Additional_Partner_Sales_Managers__c a : newPSMList){
            accIDSet.add(a.Account__c);
            userIDSet.add(a.TeamMember__c);
        }    
        //Account ID Set used to query Account Team Member records to check if the user is already exist
        List<AccountTeamMember> atmList = [Select Id, AccountId, UserId, TeamMemberRole from AccountTeamMember where AccountId in : accIDSet and UserId in : userIDSet];    
        //List of Account Team Members to insert
        List<AccountTeamMember> accTeamMemlist = new List<AccountTeamMember>();
        //List of Account Share records to add
        List<AccountShare> accShareList = new List<AccountShare>();
        
        //Create Account Team Member record for each new PSM record
        for(Additional_Partner_Sales_Managers__c aPSM : newPSMList){
          //Check if the list size is Zero
          if(!atmList.isEmpty()){
            for(AccountTeamMember atm : atmList){
            //Check if the record is already preset in Account Team Member with Same Account and user ID. If so, don't add record to Account Team Member
              if(!(aPSM.Account__c == atm.AccountId && aPSM.TeamMember__c == atm.UserId)){
                AccountTeamMember accTeamMem = new AccountTeamMember();
                accTeamMem.AccountId = aPSM.Account__c;
                accTeamMem.UserId = aPSM.TeamMember__c;
                accTeamMem.TeamMemberRole = aPSM.Team_Role__c;
                accTeamMemlist.add(accTeamMem);  
              }          
            }  
          }else{
            //No existing records in Account Team Member for this Account 
            AccountTeamMember accTeamMem = new AccountTeamMember();
            accTeamMem.AccountId = aPSM.Account__c;
            accTeamMem.UserId = aPSM.TeamMember__c;
            accTeamMem.TeamMemberRole = aPSM.Team_Role__c;
            accTeamMemlist.add(accTeamMem);
          }     
        }
    
        //Insert Account Team Member records
        if(!accTeamMemlist.isEmpty()){
            try{
                Database.SaveResult[] srList = Database.insert(accTeamMemlist, false);  
            }
            catch(Exception e){
                throw e;
            }
        }    
        //Check for existing Account Share records
        List<AccountShare> ashareList = [Select Id, AccountId, AccountAccessLevel, OpportunityAccessLevel, CaseAccessLevel, UserOrGroupId from AccountShare where AccountId in : accIDSet and UserOrGroupId in : userIDSet];
                
        //Create Account Share record for each PSM record if not exist earlier
         for(Additional_Partner_Sales_Managers__c aPSMforShare : newPSMList){
            if(!ashareList.isEmpty() && !accTeamMemlist.isEmpty()){
                
                for(AccountShare aShare : ashareList){
                    if(aPSMforShare.Account__c ==aShare.AccountId && aPSMforShare.TeamMember__c == aShare.UserOrGroupId){
                    aShare.AccountAccessLevel = 'Edit';
                    aShare.OpportunityAccessLevel = 'None';
                    aShare.CaseAccessLevel = 'None';
                    accShareList.add(aShare);                   
                    }
                }          
            }
         }      
        //Insert Account Team Member records
        if(!accShareList.isEmpty()){
            try{
                Database.SaveResult[] srList = Database.update(accShareList, false); 
            }
            catch(Exception e){
                throw e;
            }  
        }    
    }
  
    //After Update Logic
    public static void onUpdatePSMLogic(Map<Id, Additional_Partner_Sales_Managers__c> oldPSMmap, Map<Id, Additional_Partner_Sales_Managers__c> newPSMmap){
    
        //Identify modified records and create list of those records
        List<Additional_Partner_Sales_Managers__c> oldPSMList = new List<Additional_Partner_Sales_Managers__c>();
        
        //List<Additional_Partner_Sales_Managers__c> modifiedPSMList = new List<Additional_Partner_Sales_Managers__c>();
        List<AccountTeamMember> atmToUpdateList = new List<AccountTeamMember>();  
        
        Set<Id> psmIdSet = new Set<Id>(); 
        Set<Id> userIDSet = new Set<Id>();
        
        for(Id oldId : oldPSMmap.keySet()){
            for(Id newId : newPSMmap.keySet()){
                if((oldId == newId) && (oldPSMmap.get(oldId).Team_Role__c != newPSMmap.get(newId).Team_Role__c)){
                
                  oldPSMList.add(oldPSMmap.get(oldId));
                  psmIdSet.add(oldPSMmap.get(oldId).Account__c);
                  userIDSet.add(oldPSMmap.get(oldId).TeamMember__c);
                }
            }
        }       
        if(psmIdSet != null && userIDSet != null){
            
            //Query the impacted Account Team Member records
            List<AccountTeamMember> atmList = [Select Id, AccountId, UserId, TeamMemberRole from AccountTeamMember where AccountId IN: psmIdSet AND UserId IN: userIDSet];
            
            for(AccountTeamMember atm : atmList){
                for(Additional_Partner_Sales_Managers__c aPSM : oldPSMList){
                    if(atm.AccountId == aPSM.Account__c && atm.UserId == aPSM.TeamMember__c && atm.TeamMemberRole == aPSM.Team_Role__c){

                      atm.TeamMemberRole = newPSMmap.get(aPSM.Id).Team_Role__c;
                      atmToUpdateList.add(atm);
                    }
                }
            }           
            if(!atmToUpdateList.isEmpty()){
                try{
                    Database.SaveResult[] srList = Database.update(atmToUpdateList, false); 
                }
                catch(Exception e){
                    throw e;
                }
            }
        }
    }
  
    //Before Delete Logic
    public static void beforeDeletePSMLogic(List<Additional_Partner_Sales_Managers__c> deletedPSMList){
    
        Set<Id> accIDSet = new Set<Id>();
        Set<Id> userIDSet = new Set<Id>();
        
        for(Additional_Partner_Sales_Managers__c a : deletedPSMList){
            accIDSet.add(a.Account__c);
            userIDSet.add(a.TeamMember__c);
        }       
        List<AccountTeamMember> atmList = [Select Id, AccountId, UserId, TeamMemberRole from AccountTeamMember where AccountId IN: accIDSet AND UserId IN: userIDSet];
        
        //List of Account Team Members to be deleted
        List<AccountTeamMember> atmToBeDeletedList = new List<AccountTeamMember>(); 
        
        for(AccountTeamMember atm : atmList){
            for(Additional_Partner_Sales_Managers__c aPSM : deletedPSMList){
                if((atm.AccountId == aPSM.Account__c) && (atm.UserId == aPSM.TeamMember__c) && (atm.TeamMemberRole == aPSM.Team_Role__c)){
                    atmToBeDeletedList.add(atm);
                }
            }
        }    
        if(!atmToBeDeletedList.isEmpty()){
            try{
                Database.DeleteResult[] drList = Database.delete(atmToBeDeletedList, false); 
            }
            catch(Exception e){
                throw e;
            }
        }
    }
}