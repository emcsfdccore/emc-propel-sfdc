@isTest
private class PRM_BPP_DistiScoreCardController_TC {

    static testMethod void myUnitTest() {
  
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser)
        {
        //PRM_VPP_JobDataHelper.createVPPCustomSettingData(); 
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.CustomSettingCountryTheaterMappingCSData(); 
            //CustomSettingDataHelper.BusinessPartnerProgramData();
            CustomSettingDataHelper.federationTierData();
            
        }
         
           
         
        /*Creating account record*/
       List<Account> objAccount = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
       insert objAccount;
      
       
       /*Creation of Grouping record */ 
       List<Account_Groupings__c> groupinglst=new List<Account_Groupings__c>();
       for(integer grpCount=0;grpCount<objAccount.size();grpCount++){
       Account_Groupings__c grouping = new Account_Groupings__c (Name = 'UNITTESTGrp',Profiled_Account__c=objAccount[grpCount].Id);
       groupinglst.add(grouping);
       }
       insert groupinglst;
       
       objAccount[0].Cluster__c='Group A';
       objAccount[0].Federation_Program_Tier__c='VCE Gold;RSA Platinum';
       objAccount[0].Profiled_Account__c = objAccount[1].id ;
       objAccount[0].PROFILED_ACCOUNT_FLAG__c = false ;
       objAccount[0].Grouping__c = groupinglst[0].Id;
       update objAccount[0];
       
      
       
       Map<String,Schema.RecordTypeInfo> recordTypes1 = Velocity_Rules__c.sObjectType.getDescribe().getRecordTypeInfosByName();
       Id veloRecordTypeId = recordTypes1.get('Tier Rule').getRecordTypeId();
       List<Velocity_Rules__c> lstVelocityRules=new List<Velocity_Rules__c>();
       for(Integer iCounter=0;iCounter<6;iCounter++){
            Velocity_Rules__c velocityRuleObj= new Velocity_Rules__c();
            velocityRuleObj.Display_Name__c='test data';
            velocityRuleObj.Cluster__c='Group A';
            velocityRuleObj.AND_OR__c='OR';
            velocityRuleObj.Sort__c=1;
            velocityRuleObj.recordtypeid=veloRecordTypeId;
            if(iCounter == 0){
            velocityRuleObj.Bucket__c='Sales Foundation';
            velocityRuleObj.Tier__c= 'Value Accelerator';
            
            }
            else if(iCounter == 1){
            velocityRuleObj.Bucket__c='SE Foundation';
            velocityRuleObj.Tier__c= 'Value Accelerator';
            }
            else if(iCounter == 2){
            velocityRuleObj.Bucket__c='Implementation Engineer';
            velocityRuleObj.Tier__c= 'Value Accelerator';
            }
            else if(iCounter == 3){
            velocityRuleObj.Bucket__c='Technology Architect';
            velocityRuleObj.Tier__c= 'Value Accelerator';
            }
            else if(iCounter == 4){
            velocityRuleObj.Bucket__c='Training';
            velocityRuleObj.Tier__c= 'Value Accelerator';
            }
            else{
            velocityRuleObj.Bucket__c='Training';
            velocityRuleObj.Tier__c= 'Base';
            }
            velocityRuleObj.Is_Total_Revenue__c= false;
            velocityRuleObj.Evaluation_at__c ='Country Grouping Level';
            velocityRuleObj.Required_Type__c='Number/Count';
            velocityRuleObj.Required__c='2';
            velocityRuleObj.Required_to_Cover_Technology__c=1;
            //insert velocityRuleObj;
            lstVelocityRules.add(velocityRuleObj);
        }
        insert lstVelocityRules;
           system.debug('velocity rules created for testing--->'+lstVelocityRules);
           
         List<Velocity_Rule_Results__c> lstVelocityRuleResult=new List<Velocity_Rule_Results__c>();
        for(Integer iCounter=0;iCounter<6;iCounter++)
        {
            Velocity_Rule_Results__c veloRuleResultObj=new Velocity_Rule_Results__c();
            veloRuleResultObj.Speciality_RuleID__c=  lstVelocityRules[iCounter].Id; //velocityRuleObj.id;//;
            veloRuleResultObj.AccountID__c=objAccount[0].Id;
            veloRuleResultObj.Grouping__c = groupinglst[0].Id;
            veloRuleResultObj.Current__c='3';
            veloRuleResultObj.AccountID__c = objAccount[0].id;
            //veloRuleResultObj.
            //veloRuleResultObj.
            
            lstVelocityRuleResult.add(veloRuleResultObj);               
        }
           insert lstVelocityRuleResult;
              List<BusinessPartnerProgram__c> bppDataList=new List<BusinessPartnerProgram__c>();
              bppDataList.add(new BusinessPartnerProgram__c(Name='SequenceOfTiersForDisti', APInames__c='Base,Value Accelerator', UniqueKey__c='SequenceOfTiersForDisti'));
              bppDataList.add(new BusinessPartnerProgram__c(Name='ScorecardInvisibility', APInames__c='Distribution VAR', UniqueKey__c='ScorecardInvisibility'));
              bppDataList.add(new BusinessPartnerProgram__c(Name='ESBI_VPP_Scheduled_Tiers_Job1', APInames__c='Silver', UniqueKey__c=''));
              bppDataList.add(new BusinessPartnerProgram__c(Name='ESBI_VPP_Silver_Achiever_Flag1', APInames__c='Yes', UniqueKey__c=''));
              bppDataList.add(new BusinessPartnerProgram__c(Name='ESBI_VPP_Silver_Achiever_Flag2', APInames__c='Yes', UniqueKey__c=''));
              bppDataList.add(new BusinessPartnerProgram__c(Name='SPScorecardRevenueBucket',APInames__c='SP Scorecard Revenue',UniqueKey__c='SPScorecardRevenueBucket'));
              insert bppDataList;
              System.debug('BusinessPartnerProgram__c lis is-->'+bppDataList);
              
           Map<String,Schema.RecordTypeInfo> recordTypes = Velocity_Rules__c.sObjectType.getDescribe().getRecordTypeInfosByName();
            Id veloRecordTypeId1 = recordTypes.get('Tier Rule').getRecordTypeId();
           ApexPages.StandardController controller01 = new ApexPages.StandardController(objAccount[0]); 
           ApexPages.currentPage().getParameters().put('id',objAccount[0].Id);
           PRM_BPP_DistiScoreCardController scorecard = new PRM_BPP_DistiScoreCardController() ; 
         
         
           
           PRM_VPP_VelocityScorecardController.PRM_TierNameValueContainer tiercntnr=new PRM_VPP_VelocityScorecardController.PRM_TierNameValueContainer();
           
           PRM_VPP_VelocityScorecardController.PRM_DisplayNameDataContainer namedatacntnr = new PRM_VPP_VelocityScorecardController.PRM_DisplayNameDataContainer();
           //namedatacntnr.setmapTierNameValueDataContainer('testtier','test'); 
           namedatacntnr.setmapTierNameValueDataContainer('Base','Value Accelerator'); 
          PRM_VPP_VelocityScorecardController.PRM_BucketNameDataContainer bucketcntnr = new PRM_VPP_VelocityScorecardController.PRM_BucketNameDataContainer();
          bucketcntnr.setmapBucketNameValueDataContainer(namedatacntnr);
          
          PRM_VPP_VelocityScorecardController.PRM_TierNameValueContainer tierContainer = new PRM_VPP_VelocityScorecardController.PRM_TierNameValueContainer();
           
          /*Test class coverage*/
             
           User insertUser1 = [Select id from User where isActive=true and profile.Name='AMER Distributor Partner User' limit 1];            
        System.runAs(insertUser1)
        {
            PRM_BPP_DistiScoreCardController.PRM_TierNameValueContainer tiercntnr1=new PRM_BPP_DistiScoreCardController.PRM_TierNameValueContainer();
           
           PRM_BPP_DistiScoreCardController.PRM_DisplayNameDataContainer namedatacntnr1 = new PRM_BPP_DistiScoreCardController.PRM_DisplayNameDataContainer();
           //namedatacntnr.setmapTierNameValueDataContainer('testtier','test'); 
           namedatacntnr1.setmapTierNameValueDataContainer('Base','Value Accelerator'); 
          PRM_BPP_DistiScoreCardController.PRM_BucketNameDataContainer bucketcntnr1 = new PRM_BPP_DistiScoreCardController.PRM_BucketNameDataContainer();
          bucketcntnr1.setmapBucketNameValueDataContainer(namedatacntnr1);
          
          PRM_BPP_DistiScoreCardController.PRM_TierNameValueContainer tierContainer1 = new PRM_BPP_DistiScoreCardController.PRM_TierNameValueContainer();
          PRM_BPP_DistiScoreCardController.PRM_TrainingDataWrapper trainingWrapper = new PRM_BPP_DistiScoreCardController.PRM_TrainingDataWrapper('Sales Foundation',1,2,lstVelocityRuleResult[0]);
          trainingWrapper.compareTo(trainingWrapper);
          PRM_BPP_DistiScoreCardController.PRM_TechCoveredWrapper coveredWrapper = new PRM_BPP_DistiScoreCardController.PRM_TechCoveredWrapper('Sales Foundation',1,1,1);
           List<PRM_BPP_DistiScoreCardController.PRM_TechCoveredWrapper> wrapperList = new List<PRM_BPP_DistiScoreCardController.PRM_TechCoveredWrapper>();
           wrapperList.add(coveredWrapper);
           scorecard.cancelUpdates ();
           scorecard.getDistiCardData();
          scorecard.techCoveredAchieved();
          scorecard.techCoveredLstofLst(wrapperList,'Sales Foundation');
          scorecard.techCoveredLstofLst(wrapperList,'SE Foundation');
          scorecard.techCoveredLstofLst(wrapperList,'SE Foundation');
          
       
 
        }
          User insertUser2 = [Select id from User where isActive=true and UserType = 'PowerPartner' and profile.Name='AMER Distribution VAR Partner User' limit 1];            
        System.runAs(insertUser2)
        {
          scorecard.cancelUpdates ();
           scorecard.getDistiCardData();
          scorecard.techCoveredAchieved();
        }
     /* End of test class Coverage */
    
    }
    
}