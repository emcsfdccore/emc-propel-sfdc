public class GFS_Email_Template_Controller {
     
     private list<Asset__c> assetToDisplay;
     public String OpportunityId {get; set;} 
     public String AssetTypeToFetch{get; set;} 
     
     public GFS_Email_Template_Controller() {}

     public list<Asset__c> getassetToDisplay() {
      integer totalCapacity = 0;
      string totalRawCapacity = '0.00';
     
      String pattern = '[,]'; 
      List<Asset__c> lstAssetToDisplay = new list<Asset__c>();
       if(opportunityid !=null){
         list<Opportunity_Asset_Junction__c> lstOpptyJunction = new list<Opportunity_Asset_Junction__c>();
         set<id> setAssetIds = new set<Id>();         
         lstOpptyJunction = [Select Id,Related_Opportunity__c,Related_Asset__c from Opportunity_Asset_Junction__c where Related_Opportunity__c =:OpportunityId];
         for(Opportunity_Asset_Junction__c opptJunctionObj :lstOpptyJunction){
             setAssetIds.add(opptJunctionObj.Related_Asset__c);
         }
         if(AssetTypeToFetch =='Yes'){
             lstAssetToDisplay = [Select id,Product_Name_Vendor__c,Model__c,Drive_Size_GB__c,Drive_Quantity__c,Total_Raw_Capacity_GB__c from Asset__c 
                                  where id in :setAssetIds and RecordType.Name='Competitive Install'];
         } 
         if(AssetTypeToFetch =='No'){
             lstAssetToDisplay = [Select id,Product_Name_Vendor__c,Model__c,Drive_Size_GB__c,Drive_Quantity__c,Total_Raw_Capacity_GB__c from Asset__c 
                                  where id in :setAssetIds and RecordType.Name='EMC Install'];
         }   
       }
       for(asset__c assetObj :lstAssetToDisplay){
           if(assetObj.Total_Raw_Capacity_GB__c != null){
               totalRawCapacity = assetObj.Total_Raw_Capacity_GB__c.replaceall(pattern,'');
               totalCapacity  =+ integer.valueof(totalRawCapacity);
               
              
           }
           
       }
       List<String> args = new String[]{'0','number','###,###,##0.00'};
       string capacityToDisplay = string.format(totalCapacity.format(),args);
       capacityToDisplay = capacityToDisplay +'.00';
       lstAssetToDisplay.add(new asset__c(Product_Name_Vendor__c='Total:',Drive_Size_GB__c='Total:',Total_Raw_Capacity_GB__c = capacityToDisplay ));
       this.assetToDisplay = lstAssetToDisplay;
       return assetToDisplay;
       
     }
 }