/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  21/03/2013      Anirudh Singh               This Class will be used for all the Operations on Opportunity Related to 
 |                                              Management
 |  15 May 2013    Prasad                       changed the webservice to delete links    
 |  19-Feb-2014   Jaypal Nimesh     WR 340066   Changes made to bypass validation during lead convert & asset linking (Back Arrow)
 |
 |  21.Feb.2014     Srikrishan SM   FR-9640     Validation rule System.Label.R2R_Asset_Link_Not_Same_CPA is commented
 |  14.03.2014      Srikrishna SM    FR 9646     Added validaition for unlinking assets if the user's profile is EMC Contract Renewals user 
 |  24 Mar 2014     Sneha Jain                  Bypassed the Validation rules for System Admin API profile users
 |  28 Mar 2014     Sneha Jain                  Remove the Won stage name from checking the condition on Asset Unlinking.This value is now considered as Open Status
 |  20 Aug 2014     Partha Baruah   WR-273      Modified AssetUnlinking method to introduce the code logic for 'Unlink & Defer' Button in opp detail page
 |  18 Sep 2014     Sneha Jain      WR-301      If user closes the Opportunity with Reason = Duplicate, unlink assets from closing oppty and link to Duplicate one
 |  24 Sep 2014     Sneha Jain      WR 291(c)   R2R Nov'14 Rel : Reps should be allowed to link/unlink assets from BOOKED,EVAL and SUBMITTED Non-Renewals opportunity
 |  15 Oct 2014     Sneha Jain      INC0729833  R2R Nov'14 Rel : Related Leads/Opps should appear in global search of Asset Serial Number
 |  18/02/2015      Vinod Jetti     #1649       Replaced field 'Global_Duns_Entity__c' to 'Global_Duns_Entity_Hub__c'
 |  2/19/2015       Bindu           CI 1673     Assigning Re-Registration reason field during Unlink Operation
 +=========================================================================================================================*/

global class R2R_Opportunity_Management{
    
    //Added a static flag to defer assets for 'Unlink & Defer' Button 
    static boolean isDefer = false;
    
  /* @Method <This method is used to set Opportunity Type Value On Opportunity.>
    @param <It is taking List<Opportunity> as Parameter>
    @returntype - <void> >
    @throws exception - <No Exception>
    */  
        

  public void setOpportunityType(List<Opportunity> lstOpportunity){
    set<Id> setOpptyOwnerId = new set<Id>();  
    
    for(Opportunity oppobj :lstOpportunity){
        setOpptyOwnerId.add(oppobj.OwnerId);    
    }
    Map<Id,User> mapOwnerDetail = new map<Id,User>([Select Id,Forecast_Group__c from User where id in :setOpptyOwnerId and Forecast_Group__c ='Maintenance Renewals']);
    for(Opportunity oppObj: lstOpportunity){
        if(mapOwnerDetail.containskey(oppObj.OwnerId) && oppObj.of_Related_Assets__c==0){            
           oppObj.Opportunity_Type__c = 'Renewals';
            
        }
        else if(oppObj.Opportunity_Type__c == 'Renewals' && oppObj.of_Related_Assets__c==0){
            oppObj.Opportunity_Type__c = '';
        }       
    }    
  }
   /* @Method <This method is used to set Opportunity Type Value On Opportunity.
    and invoke setSalesPlan() method of R2R_AssetStatus class which will be taking 
    List<Opportunity_Asset_Junction__c> ,setAssetIds<Id>,map<Id,Opportunity>>
    @param <It is taking List<Opportunity_Asset_Junction__c> as Parameter>
    @returntype - <void> >
    @throws exception - <No Exception>
    */  
  public void setOpportunityType(List<Opportunity_Asset_Junction__c> lstOpportunityAssetJunction, boolean isLinking){
    set<Id> setOpportunityIds = new set<Id>(); 
    set<Id> setAssetIds = new set<Id>();
    //INC0729833    R2R Nov'14 Rel : Related Leads/Opps should appear in global search of Asset Serial Number
    Set<Id> assetIdFullSet = new Set<Id>();
    list<Opportunity> listopptyToUpdate= new list<Opportunity>();
    Map<Id,List<Opportunity_Asset_Junction__c>> mapOpptyWithEMCAssets = new map<Id,List<Opportunity_Asset_Junction__c>>();  
    Map<Id,List<Opportunity_Asset_Junction__c>> mapOpptyWithCompetitiveAssets = new Map<Id,List<Opportunity_Asset_Junction__c>>();  
    for(Opportunity_Asset_Junction__c asstJunctionObj : lstOpportunityAssetJunction){
        setOpportunityIds.add(asstJunctionObj.Related_Opportunity__c);
        setAssetIds.add(asstJunctionObj.Related_Asset__c);
        assetIdFullSet.add(asstJunctionObj.Related_Asset__c);
    }
    
    //Added bypass_validation__c field in query below to bypass the validation during asset linking (Back Arrow - Jaypal Nimesh - 19-Feb-2014)
    map<Id,Opportunity> mapOpportunityToUpdate = new map<Id,Opportunity>([Select Id,bypass_validation__c,StageName,Opportunity_number__c,Opportunity_Type__c,Account.Global_DUNS_Entity_Hub__c,(SELECT Id,Record_Type_Name__c,Related_Asset__c,Related_Opportunity__c FROM Opportunity.Opportunity_Asset_Junction__r order by Record_Type_Name__c) from Opportunity where id in:setOpportunityIds]);
    //INC0729833    R2R Nov'14 Rel : Related Leads/Opps should appear in global search of Asset Serial Number
    //Creating Map to hold all Opp Asset junction records
    Map<Id,Opportunity_Asset_Junction__c> oajMap = new Map<Id,Opportunity_Asset_Junction__c>();
        for(Opportunity oppObj :mapOpportunityToUpdate.values()){            
            list<Opportunity_Asset_Junction__c> lstAssetJunction = oppObj.Opportunity_Asset_Junction__r;           
            for(Opportunity_Asset_Junction__c assetJuncnObj :lstAssetJunction){    
                //INC0729833    R2R Nov'14 Rel : Related Leads/Opps should appear in global search of Asset Serial Number
                oajMap.put(assetJuncnObj.Id,assetJuncnObj);
                if(assetJuncnObj.Record_Type_Name__c == 'EMC_Install' ){
                   List<Opportunity_Asset_Junction__c> lstEMCInstallOpptyAssets = mapOpptyWithEMCAssets.get(oppObj.Id);
                   if(lstEMCInstallOpptyAssets == null){
                    lstEMCInstallOpptyAssets = new List<Opportunity_Asset_Junction__c>();
                   }
                    lstEMCInstallOpptyAssets.add(assetJuncnObj);
                    mapOpptyWithEMCAssets.put(oppObj.Id,lstEMCInstallOpptyAssets);
                }
                if(assetJuncnObj.Record_Type_Name__c == 'Competitive_Install'){
                   List<Opportunity_Asset_Junction__c> lstCompetitiveAssets = mapOpptyWithCompetitiveAssets.get(oppObj.Id);
                   if(lstCompetitiveAssets == null){
                    lstCompetitiveAssets = new List<Opportunity_Asset_Junction__c>();
                   }
                    lstCompetitiveAssets.add(assetJuncnObj);
                    mapOpptyWithCompetitiveAssets.put(oppObj.Id,lstCompetitiveAssets);
                }
            }   
                         
        }
        //INC0729833    R2R Nov'14 Rel : Related Leads/Opps should appear in global search of Asset Serial Number
        //Creating map to hold Opportunity Id with String of all associated Asset's serial number
        Map<Id,String> oppIdAssetSerialMap = new Map<Id,String>();
        if(oajMap !=null && oajMap.size()>0){
            for(Opportunity_Asset_Junction__c oaj:oajMap.values()){
                assetIdFullSet.add(oaj.Related_Asset__c);
            }
            if(assetIdFullSet !=null && assetIdFullSet.size()>0){
                Map<Id,Asset__c> assetIdMap = new Map<Id,Asset__c>([Select Id,Serial_Number__c from Asset__c where Id IN: assetIdFullSet]);
                for(Opportunity_Asset_Junction__c oaj:oajMap.values()){
                    if(oppIdAssetSerialMap !=null && oppIdAssetSerialMap.containsKey(oaj.Related_Opportunity__c)){
                        String tempStr = oppIdAssetSerialMap.get(oaj.Related_Opportunity__c);
                        if(assetIdMap !=null && assetIdMap.containsKey(oaj.Related_Asset__c)){
                            tempStr = tempStr + ';' + assetIdMap.get(oaj.Related_Asset__c).Serial_Number__c;
                        }
                        oppIdAssetSerialMap.put(oaj.Related_Opportunity__c,tempStr);
                    }
                    else{
                        if(assetIdMap !=null && assetIdMap.containsKey(oaj.Related_Asset__c)){
                            String tempStr = assetIdMap.get(oaj.Related_Asset__c).Serial_Number__c;
                            oppIdAssetSerialMap.put(oaj.Related_Opportunity__c,tempStr);
                        }
                    }
                }
            }
        }

        
        for(Opportunity oppObj :mapOpportunityToUpdate.values()){
            string OpportunityType= '';
            if(oppObj.Opportunity_Type__c != 'Renewals'){
                if(!mapOpptyWithEMCAssets.isempty() && !mapOpptyWithCompetitiveAssets.isempty() && mapOpptyWithEMCAssets.containskey(oppObj.Id) && 
                   mapOpptyWithCompetitiveAssets.containskey(oppObj.Id)){
                   OpportunityType = 'Refresh/Swap';                   
                }
                if(mapOpptyWithEMCAssets.containskey(oppObj.Id) && !mapOpptyWithCompetitiveAssets.containskey(oppObj.Id)){
                   OpportunityType = 'Refresh'; 
                }
                if(!mapOpptyWithEMCAssets.containskey(oppObj.Id) && mapOpptyWithCompetitiveAssets.containskey(oppObj.Id)){
                   OpportunityType = 'Swap';    
                }
                oppObj.Opportunity_Type__c = OpportunityType;
                //INC0729833    R2R Nov'14 Rel : Related Leads/Opps should appear in global search of Asset Serial Number
                //Assigning the Serial Number values to the Opportunity field 
                oppObj.Assets_Serial_Number__c = oppIdAssetSerialMap.get(oppObj.Id);
                //Updating bypass_validation__c field to true (Back Arrow - Jaypal Nimesh - 19-Feb-2014)
                oppObj.bypass_validation__c = true;
                listopptyToUpdate.add(oppObj);
            }
            //INC0729833    R2R Nov'14 Rel : Related Leads/Opps should appear in global search of Asset Serial Number
            else{
                oppObj.Assets_Serial_Number__c = oppIdAssetSerialMap.get(oppObj.Id);
                //Assigning the Serial Number values to the Opportunity field 
                listopptyToUpdate.add(oppObj);
            }        
        }      
       try{ 
         System.debug('ListofOpptytoUpdate-->'+listopptyToUpdate);   
        Database.SaveResult[] result = Database.Update(listopptyToUpdate,true);

     }
     catch(exception e){
         //e.setMessage('Error In Update');
            system.debug('***** Error in Exception'+e.getMessage());
            throw new customexception ('There is an error with the opportunity that is preventing you from adding assets at this time. Please go back and correct the opportunity before attempting to attach any assets.'); 
     }
     
     //Added finally block to reset bypass validation field in future method after transaction is over (Back Arrow - Jaypal Nimesh - 19-Feb-2014)
     finally {   
        Set<Id> oppIdSet = new Set<Id>();
        for(Opportunity opp : listopptyToUpdate){
            oppIdSet.add(opp.Id);
        }
        OppyOEMPartnerAccounts.resetOpptyBypassValidation(oppIdSet);
     }
     
     system.debug('**** lstOpportunityAssetJunction ' +lstOpportunityAssetJunction);
     system.debug('**** setAssetIds '+setAssetIds);
     system.debug('**** mapOpportunityToUpdate '+mapOpportunityToUpdate);
     system.debug('**** isLinking '+isLinking);
     new R2R_AssetStatus().setAssetStatusOnLinkUnlink(lstOpportunityAssetJunction,setAssetIds,mapOpportunityToUpdate,isLinking);

    } 
    
    
    // This webservice will add the logic to the custom button unlink & Defer(in object Opportunity_Asset_Junction__c) 
    webservice static string assetDeferUnlinking(Id OpportunityId, List<Id> linkIds){
        String returnMessage='';
        isDefer = true;
        returnMessage=AssetUnlinking(OpportunityId, linkIds);
        return returnMessage;
    }
    
    //WR- 273  Checks the "Defer Asset" checkbox in one or more assets which were associated with the Opportunity on 'Unlink & Defer' button click
    public static void deferAsset (List<Opportunity_Asset_Junction__c> lstOppAsset){
        Set<Id> assetIds = new Set<Id>();
        for(Opportunity_Asset_Junction__c assetJunc : lstOppAsset) {
            assetIds.add(assetJunc.Related_Asset__c);
        }
        if(assetIds != null && assetIds.size()>0)
        {
            List<Asset__c> lstAsset = [select Deffered_to_Renewals__c from Asset__c where id in :assetIds];
            if(lstAsset != null && lstAsset.size()>0)
            {
                for(Asset__c asset : lstAsset) {
                    asset.Deffered_to_Renewals__c = true;
                }
                try{
                    update lstAsset;
                }catch(exception e){}
            }   
        }           
    }
    
/* @Method <This method is used to Asset Unlinking>
    @param <It is taking OpportunityId as Parameter>
    @returntype - String SuccessMessage >
    @throws exception - <No Exception>
    */  
    
    
   webservice static string AssetUnlinking(Id OpportunityId, List<Id> linkIds){
        //Added for restricting access to 'EMC Contract Renewals User' Profile from linking an Asset to Opportunities
        String returnMessage = 'cantUnLink';
        User u = [select Id,Name, ProfileID from User WHERE id = :UserInfo.getUserId()];
        Profile pr = [SELECT Id, Name FROM Profile WHERE id = :u.ProfileID];
        if(pr.Name == System.Label.R2R_Restricted_Profile_Name){
            return returnMessage; //System.Label.R2R_Insufficient_Privilege_Error           
        }
        //Added for restricting access to 'EMC Contract Renewals User' Profile from linking an Asset to Opportunities - Ends here
        String successmessage = 'failure';
        Opportunity opp = new Opportunity();
        map<string,customsettingdatavaluemap__c> datavaluemap = customsettingdatavaluemap__c.getall();
        string R2R_Admin_Profiles = datavaluemap.get('R2R_Admin_Profiles').datavalue__c;
        string OpptyClosedStatus = datavaluemap.get('OpptyClosedStatus').datavalue__c;
        String R2R_UserIdsToByPass= datavaluemap.get('R2R_UserIdsToByPass').datavalue__c;
        try {
            if ((R2R_Admin_Profiles.contains(userinfo.getProfileId())|| R2R_UserIdsToByPass.contains(userinfo.getUserId()))){
                 List<Opportunity_Asset_Junction__c> lstlinks=[select id,Related_Asset__c from Opportunity_Asset_Junction__c where id in :linkIds];
            
                // WR- 273 Calls the deferAsset() method if the 'Unlink & Defer' button is clicked and not the 'Unlink' button
                if(isDefer) {
                    deferAsset(lstlinks);
                }
                 delete lstlinks;
                 successmessage = 'success';
             }
             opp  = [Select Id,StageName,Opportunity_Type__c, (select id,Related_Asset__c from Opportunity_Asset_Junction__r) ,name,Asset_Count__c,Re_Registration_Reason__c from Opportunity where id =:OpportunityId];
             
             if(successmessage !='success'){                 
            //}  
            //Remove the Won stage name from checking the condition on Asset Unlinking.This value is now considered as Open Status
            //if (opp.StageName!='Closed'&& opp.StageName!='Won' && opp.StageName!='Submitted' && opp.StageName!='Booked' && opp.StageName!='Eval'){
            //if (opp.StageName!='Closed' && opp.StageName!='Submitted' && opp.StageName!='Booked' && opp.StageName!='Eval'){
            //WR 291(c) - R2R Nov'14 Rel - Reps should be allowed to link/unlink assets from BOOKED,EVAL and SUBMITTED Non-Renewals opportunity
            if (!OpptyClosedStatus.contains(opp.StageName) || (opp.StageName!='Closed' && opp.Opportunity_Type__c != 'Renewals' )){
                 List<Opportunity_Asset_Junction__c> lstlinks=[select id,Related_Asset__c from Opportunity_Asset_Junction__c where id in :linkIds];
                 System.debug('lstlinks --->'+lstlinks);
                // WR- 273 Calls the deferAsset() method if the 'Unlink & Defer' button is clicked and not the 'Unlink' button
                if(isDefer) {
                    deferAsset(lstlinks);
                }
                 delete lstlinks;
                 successmessage = 'success';
                   }
            }
            //1673 changes
            
            
            List<Opportunity_Asset_Junction__c> lstlinkscheckCount= new List<Opportunity_Asset_Junction__c> ();
            lstlinkscheckCount=[select id,Related_Asset__c from Opportunity_Asset_Junction__c where Related_Opportunity__c =:OpportunityId];
            
            if(lstlinkscheckCount.size()<=0)
            
                opp.Re_Registration_Reason__c= 'Asset Unlinked';
            else
                opp.Re_Registration_Reason__c= 'Asset Linked';
                update opp;
            //1673 changes ends here
            
        }catch(exception e){
            successmessage = 'failure'+e;
        }
        return  successmessage;
   }
  /* @Method <This method is used to validate Asset Linking>
    @param <It is taking Map<Id,List<Id>> mapOpptyIdWithAssets>
    @returntype - String ValidationResult >
    @throws exception - <No Exception>
    */  
  public string validateAssetLinking(Map<Id,List<Id>> mapOpptyIdWithAssets){
        String validationmessage = '';
        set<Id> setAssetId = new set<Id>();     
        for(Id opptyId :mapOpptyIdWithAssets.keyset()){
            for(id assetId : mapOpptyIdWithAssets.get(opptyId)){
                setAssetId.add(assetId);
            }   
                
        }
        Map<Id,Opportunity_Asset_Junction__c> mapOpptyAssetJunction = new map<id,Opportunity_Asset_Junction__c>([Select Id, Record_Type_Name__c,  Related_Asset__c,Related_Opportunity__c, Related_Opportunity__r.Opportunity_Type__c from Opportunity_Asset_Junction__c where Related_Asset__c in :setAssetId and Related_Opportunity__c in :mapOpptyIdWithAssets.keyset()]);
        
        Map<Id,Opportunity> mapOpportunity = new map<Id,Opportunity>([Select Id,Opportunity_Type__c from Opportunity where id in :mapOpptyIdWithAssets.keyset() and Opportunity_Type__c ='Renewals']);
        Map<Id,Asset__c> mapAssets = new map<Id,Asset__c>([Select Id,RecordType.DeveloperName from Asset__c where id in:setAssetId and RecordType.DeveloperName ='Competitive_Install']);for(Id opptyId :mapOpptyIdWithAssets.keyset()){
            for(id assetId : mapOpptyIdWithAssets.get(opptyId)){
                if(mapOpportunity.containskey(opptyId) && mapAssets.containskey(assetId)){
                    validationmessage = 'A Competitive asset cannot be linked to an Renewal opportunity';
                    break;
                }
                
            }   
                
        }       
                                                                                                                
        for(Opportunity_Asset_Junction__c asset : mapOpptyAssetJunction.values()){
            if(asset.Related_Opportunity__r.Opportunity_Type__c=='Refresh/Swap'){
               validationmessage = 'An EMC or Competitive asset is already linked to Refresh / Swap opportunity.';  
            }
            else if(asset.Record_Type_Name__c =='EMC_Install' && asset.Related_Opportunity__r.Opportunity_Type__c=='Renewals'){
               validationmessage = 'An EMC asset is already linked to Renewal opportunities.';  
            }
            
        }
        return  validationmessage;  
   }
   
   public void validateAssetOpportunityLinking(List<Opportunity_Asset_Junction__c> lstOpptyJunctionFromTrigger){
        set<Id> setAssetId = new set<Id>();
        set<Id> setOpportunityId = new set<Id>();
        Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
        string LeadOpenStatus = data.get('R2R_lead_Status').datavalue__c;
        string R2R_Admin_Profiles = data.get('R2R_Admin_Profiles').datavalue__c;
        String R2R_UserIdsToByPass= data.get('R2R_UserIdsToByPass').datavalue__c;
        string LeadClosedStatus = data.get('R2R_Closed_lead_Staus').datavalue__c;
        string OpptyClosedLostStatus = data.get('OpptyClosedStatus').datavalue__c;
        string installbaseStatus = data.get('R2R_Install_Base_Status').datavalue__c;  
        string actionablestatus= data.get('R2R Actionable Status').datavalue__c;
        string nonactionableStatus = data.get('R2R Non-Actionable Asset Status').datavalue__c+data.get('R2R Non-Actionable Asset Status1').datavalue__c; 
        string R2R_Sys_Admin_Profiles = data.get('R2R_SYS_Admin_Profiles').datavalue__c;        
        List<Opportunity_Asset_Junction__c> lstOpptyJnObj = new List<Opportunity_Asset_Junction__c>();   
        List<Lead_Asset_Junction__c> lstLeadJnObj = new List<Lead_Asset_Junction__c>();
        map<Id,list<Opportunity_Asset_Junction__c>> mapAssetWithJunction = new map<Id,list<Opportunity_Asset_Junction__c>>();
        map<Id,list<Lead_Asset_Junction__c>> mapAssetWithLeadJunction = new map<Id,list<Lead_Asset_Junction__c>>();
        set<id> setOpenLinkedOppty = new set<id>();
        for(Opportunity_Asset_Junction__c junctionObj :lstOpptyJunctionFromTrigger){
            setAssetId.add(junctionObj.Related_Asset__c);
            setOpportunityId.add(junctionObj.Related_Opportunity__c);
        }
        Map<Id,Asset__c> mapAssetsToFetch = new map<Id,Asset__c>([Select Id ,RecordType.DeveloperName,Customer_Name__r.Account_District__c ,Red_Zone_Priority__c,Target_Zone__c,Install_City__c,Sales_Plan__c,Install_Base_Status__c,Contract_End_Date__c,Maintenance_Engagement_Status1__c,Lease_Exp_Date__c,Model__c,Install_Date__c, Customer_Name__r.Party_Number__c, Customer_Name__r.Customer_Profiled_Account_Lookup__c,Custom_Asset_Name__c,Serial_Number__c,Name,Disposition_Status__c ,Account_Record_Type__c ,RecordType.Name ,Product_Name_Vendor__c,Product_Family__c,Customer_Name__c,Contract_Number__c,Customer_Name__r.Global_DUNS_Entity_Hub__c from Asset__c where id in :setAssetId]);
                                                                  
        lstLeadJnObj = [Select Id,Name,Related_Asset__c,Related_Asset__r.RecordType.DeveloperName,Related_Lead__r.Lead_Type_Based_on_Linking__c from Lead_Asset_Junction__c where Related_Asset__c in :setAssetId
                        and Related_Lead__r.Status not in ('Closed','Converted To Opportunity')];
        lstOpptyJnObj = [select id,Asset_Record_Type_ID__c,Opportunity_Forecast_Status__c,Opportunity_Close_Date__c,Related_Asset__c,Related_Asset__r.id,Related_Opportunity__c,
                        Related_Opportunity__r.Opportunity_Type__c,Related_Opportunity__r.StageName,Record_Type_Name__c from Opportunity_Asset_Junction__c where Related_Asset__r.id in:setAssetId];
        for(Opportunity_Asset_Junction__c junctionObj :lstOpptyJnObj){
            list<Opportunity_Asset_Junction__c> lstJunction = mapAssetWithJunction.get(junctionObj.Related_Asset__c);
            if(lstJunction == null){
               lstJunction = new list<Opportunity_Asset_Junction__c>(); 
            }
            lstJunction.add(junctionObj);
            mapAssetWithJunction.put(junctionObj.Related_Asset__c,lstJunction);
            if(!OpptyClosedLostStatus.contains(junctionObj.Related_Opportunity__r.StageName)){
                setOpenLinkedOppty.add(junctionObj.Related_Opportunity__c);
            }
        }
        for(Lead_Asset_Junction__c leadjunctionObj :lstLeadJnObj){
            list<Lead_Asset_Junction__c> lstJunction = mapAssetWithLeadJunction.get(leadjunctionObj.Related_Asset__c);
            if(lstJunction == null){
               lstJunction = new list<Lead_Asset_Junction__c>(); 
            }
            lstJunction.add(leadjunctionObj);
            mapAssetWithLeadJunction.put(leadjunctionObj.Related_Asset__c,lstJunction);         
        }
        map<Id,Opportunity> mapOpptyToLinkAsset = new map<Id,Opportunity>([Select Id,StageName,Account.Account_District__c,Opportunity_Type__c,Account.Global_DUNS_Entity_Hub__c from Opportunity where id =:setOpportunityId]);  
        for(Opportunity_Asset_Junction__c junctionObj: lstOpptyJunctionFromTrigger){
            //Bypassed the Validation rule for System Admin API profile users
            //Modified for null pointer check
            if(mapAssetsToFetch.containsKey(junctionObj.Related_Asset__c) && installbaseStatus.contains(mapAssetsToFetch.get(junctionObj.Related_Asset__c).Install_Base_Status__c) && !R2R_Sys_Admin_Profiles.contains(userinfo.getProfileId())){
               junctionObj.Adderror(System.Label.R2R_Asset_InstallBaseStatusError); 
            }
            //Bypassed the Validation rule for System Admin API profile users
            //Modified for null pointer check
            if(mapAssetsToFetch.containsKey(junctionObj.Related_Asset__c) && !actionablestatus.contains(mapAssetsToFetch.get(junctionObj.Related_Asset__c).Install_Base_Status__c) && !R2R_Sys_Admin_Profiles.contains(userinfo.getProfileId())){
               junctionObj.Adderror(System.Label.R2R_Non_Actionable_Asset); 
            }
            //Below code is commented to avoid validation error in the visualforce page (Transformation_Link_Assets)
            /*if(mapAssetsToFetch.get(junctionObj.Related_Asset__c).Customer_Name__r.Account_District__c != mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Account.Account_District__c){
               junctionObj.adderror(System.Label.R2R_Asset_Link_Not_Same_CPA);
            }
        */
            //Modified for null pointer check
            //WR 291(c) - R2R Nov'14 Rel - Reps should be allowed to link/unlink assets from BOOKED,EVAL and SUBMITTED Non-Renewals opportunity
            //if(mapOpptyToLinkAsset.containsKey(junctionObj.Related_Opportunity__c) && OpptyClosedLostStatus.contains(mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).StageName)&& !(R2R_Admin_Profiles.contains(userinfo.getProfileId()) || R2R_UserIdsToByPass.contains(userinfo.getUserId())) )
            //if(!(R2R_Admin_Profiles.contains(userinfo.getProfileId()) || R2R_UserIdsToByPass.contains(userinfo.getUserId())) && mapOpptyToLinkAsset.containsKey(junctionObj.Related_Opportunity__c) && ((OpptyClosedLostStatus.contains(mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).stagename) && mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).stagename != 'Booked') || (OpptyClosedLostStatus.contains(mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).stagename) && mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Opportunity_Type__c == 'Renewals' )))
            if(!(R2R_Admin_Profiles.contains(userinfo.getProfileId()) || R2R_UserIdsToByPass.contains(userinfo.getUserId())) && mapOpptyToLinkAsset.containsKey(junctionObj.Related_Opportunity__c) && ((mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).stagename == 'Closed') || (OpptyClosedLostStatus.contains(mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).stagename) && mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Opportunity_Type__c == 'Renewals' )))
            {
               junctionObj.adderror(System.Label.R2R_Asset_Link_closed_Opty);
            }
            
            if(mapAssetWithJunction.get(junctionObj.Related_Asset__c) !=null){
                //System.Debug(mapOpptyWithJunction.get(junctionObj.Related_Opportunity__c));
                //System.Debug(mapOpptyToLinkAsset);
               for(Opportunity_Asset_Junction__c linkedOppty : mapAssetWithJunction.get(junctionObj.Related_Asset__c)){
               
                   if(linkedOppty.Related_Opportunity__r.Opportunity_Type__c !=null){
                        /*if(!OpptyClosedLostStatus.contains(linkedOppty.Related_Opportunity__r.StageName) && 
                        linkedOppty.Related_Opportunity__r.Opportunity_Type__c.contains('Refresh') && 
                        mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Opportunity_Type__c !='Renewals' &&
                        !(R2R_Admin_Profiles.contains(userinfo.getProfileId()) || R2R_UserIdsToByPass.contains(userinfo.getUserId())))*/
                        //Modified to throw error message only when two open Refresh Types are getting linked - R2R Nov'14 Release
                        if(!OpptyClosedLostStatus.contains(linkedOppty.Related_Opportunity__r.StageName) && !OpptyClosedLostStatus.contains(mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).StageName) && 
                        linkedOppty.Related_Opportunity__r.Opportunity_Type__c.contains('Refresh') && 
                        mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Opportunity_Type__c !='Renewals' &&
                        !(R2R_Admin_Profiles.contains(userinfo.getProfileId()) || R2R_UserIdsToByPass.contains(userinfo.getUserId())))
                        {
                          junctionObj.adderror(System.Label.R2R_Asset_Link_Already_Linked_2);
                          break;
                        }
                        //Bypassed the Validation rule for System Admin API profile users
                        if(!OpptyClosedLostStatus.contains(mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).StageName) && 
                        !OpptyClosedLostStatus.contains(linkedOppty.Related_Opportunity__r.StageName) 
                        && linkedOppty.Related_Opportunity__r.Opportunity_Type__c.contains('Renewals') 
                        && mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Opportunity_Type__c =='Renewals'
                        && !R2R_Sys_Admin_Profiles.contains(userinfo.getProfileId()))
                        {
                        junctionObj.adderror(System.Label.R2R_Asset_Link_Already_Linked_1);
                        break;
                        }
                        if(OpptyClosedLostStatus.contains(mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).StageName) 
                        && linkedOppty.Related_Opportunity__r.Opportunity_Type__c.contains('Renewals') 
                        && mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Opportunity_Type__c =='Renewals' && !(R2R_Admin_Profiles.contains(userinfo.getProfileId()) || R2R_UserIdsToByPass.contains(userinfo.getUserId())) )
                        {
                        junctionObj.adderror(System.Label.R2R_Asset_Link_Already_Linked_1);
                        break;
                        }
                   }    
               }
            }
            
            if(mapAssetWithLeadJunction !=null){
               if(mapAssetWithLeadJunction.containskey(junctionObj.Related_Asset__c)){
                 for(Lead_Asset_Junction__c leadjunction :mapAssetWithLeadJunction.get(junctionObj.Related_Asset__c)){
                    //Bypassed the Validation rule for System Admin API profile users
                    if
                    (leadjunction.Related_Lead__c!=null && 
                    leadjunction.Related_Lead__r.Lead_Type_Based_on_Linking__c.contains('Refresh') && 
                    mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Opportunity_Type__c !='Renewals' &&
                    junctionObj.Related_Opportunity__r.StageName!=null && 
                    !OpptyClosedLostStatus.contains(junctionObj.Related_Opportunity__r.StageName) &&
                    !R2R_Sys_Admin_Profiles.contains(userinfo.getProfileId()))
                    {
                    junctionObj.adderror(System.Label.R2R_Asset_Linked_To_Oppty);
                     break;
                    }
                    else if
                    (leadjunction.Related_Lead__c!=null && 
                    leadjunction.Related_Lead__r.Lead_Type_Based_on_Linking__c.contains('Refresh') && 
                    mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Opportunity_Type__c !='Renewals' &&
                    junctionObj.Related_Opportunity__r.StageName!=null &&
                    OpptyClosedLostStatus.contains(mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).StageName) && 
                    !(R2R_Admin_Profiles.contains(userinfo.getProfileId()) || R2R_UserIdsToByPass.contains(userinfo.getUserId()))
                    )
                    {
                    junctionObj.adderror(System.Label.R2R_Asset_Linked_To_Oppty);
                    break;
                    }
                    system.debug('leadjunction--->'+leadjunction);
                    system.debug('leadjunction--->'+junctionObj);
                    system.debug('leadjunction--->'+leadjunction.Related_Lead__r.Lead_Type_Based_on_Linking__c.contains('Renewals'));
                    system.debug('leadjunction--->'+mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Opportunity_Type__c);
                    system.debug('leadjunction--->'+junctionObj.Related_Opportunity__r.StageName);
                    if(leadjunction.Related_Lead__c!=null && 
                    leadjunction.Related_Lead__r.Lead_Type_Based_on_Linking__c.contains('Renewals') && 
                    mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Opportunity_Type__c =='Renewals' && 
                    !OpptyClosedLostStatus.contains(mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).StageName) &&
                    !R2R_Sys_Admin_Profiles.contains(userinfo.getProfileId()))
                    
                    {
                    system.debug('leadjunction inside if------' +leadjunction);
                        junctionObj.adderror(System.Label.R2R_Asset_Linked_To_Oppty);
                        break;
                    }
                    else if(leadjunction.Related_Lead__c!=null && leadjunction.Related_Lead__r.Lead_Type_Based_on_Linking__c.contains('Renewals') && 
mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Opportunity_Type__c =='Renewals' && OpptyClosedLostStatus.contains(mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).StageName)
&& !(R2R_Admin_Profiles.contains(userinfo.getProfileId()) || R2R_UserIdsToByPass.contains(userinfo.getUserId()))
){
                        junctionObj.adderror(System.Label.R2R_Asset_Linked_To_Oppty);
                        break;
                    }
                 }
               }
            }
            //Bypassed the Validation rule for System Admin API profile users
            //Modified for null pointer check
            if(mapAssetsToFetch.containsKey(junctionObj.Related_Asset__c) && mapOpptyToLinkAsset.containsKey(junctionObj.Related_Opportunity__c) && mapAssetsToFetch.get(junctionObj.Related_Asset__c).recordType.developername=='Competitive_Install' && mapOpptyToLinkAsset.get(junctionObj.Related_Opportunity__c).Opportunity_Type__c =='Renewals' && !R2R_Sys_Admin_Profiles.contains(userinfo.getProfileId())){
               junctionObj.adderror(System.Label.R2R_Asset_Link_Competitive);
            }
            
            
        }
        
   }
   
    //WR-301 :R2R Nov'14 Rel - If user closes the Opportunity with Reason = Duplicate, unlink assets from closing oppty and link to Duplicate one
    public void transferAssetToDuplicateOpp(Set<Opportunity> oppSet,Map<Id,Id> oppDupIdMap){
        
        Map<Id,Id> oajIdAssetIdMap = new Map<Id,Id>();
        Map<Id,Id> oajIdOppIdMap = new Map<Id,Id>();
        List<Opportunity_Asset_Junction__c> oajInsertList = new List<Opportunity_Asset_Junction__c>();
        List<Opportunity_Asset_Junction__c> oajList = new List<Opportunity_Asset_Junction__c>();
        List<Opportunity> oppList = new List<Opportunity>();
        
        for(Opportunity opp: oppSet){
            oppList.add(opp);
        }
        //Query all oppty asset junction records for which oppty is getting closed. Create map for OAJ Id and Related Asset
        oajList = [select id,Related_Asset__c,Related_Opportunity__c from Opportunity_Asset_Junction__c where Related_Opportunity__c IN: oppSet];
        system.debug('---oajList---'+oajList);
        if(oajList !=null && oajList.size()>0){
            for(Opportunity_Asset_Junction__c oaj:oajList){
                oajIdAssetIdMap.put(oaj.Id,oaj.Related_Asset__c);
                oajIdOppIdMap.put(oaj.Id,oaj.Related_Opportunity__c);   
            }
        }
        system.debug('---oajIdAssetIdMap---'+oajIdAssetIdMap);
        if(oajIdAssetIdMap !=null && oajIdAssetIdMap.size()>0 && oppDupIdMap !=null && oppDupIdMap.size()>0 && oajIdOppIdMap != null && oajIdOppIdMap.size()>0){
            for(Id oajNew: oajIdAssetIdMap.keySet()){
                if(oppDupIdMap.containsKey(oajIdOppIdMap.get(oajNew))){
                    Id tempAsset = oajIdAssetIdMap.get(oajNew);
                    Id dupOpp = oppDupIdMap.get(oajIdOppIdMap.get(oajNew));
                    //Create list for new OAJ records to be inserted on the duplicate opportunity
                    oajInsertList.add(new Opportunity_Asset_Junction__c(Related_Asset__c = tempAsset,Related_Opportunity__c = dupOpp));
                }   
            }
            system.debug('---oajInsertList---'+oajInsertList);
        }
        if(oajInsertList !=null && oajInsertList.size()>0){
            system.debug('----Insert this list ----');
            //Creating a savepoint to rollback the transaction if their is any failure
            Savepoint sp = Database.setSavepoint();
            try{
                Database.insert(oajInsertList,false);
                //insert oajInsertList;
                //delete oajList;
            }
            catch(Exception e){
                Database.rollback(sp);
                system.debug('----Catch exception----'+e.getMessage());
                if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                    String fullError = e.getMessage();
                    Integer startIndex = fullError.lastindexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,') + 35;
                    String displayError = fullError.substring(startIndex,fullError.IndexOf(':',startIndex));
                    system.debug('----displayError----'+displayError);
                    oppList[0].addError(displayError);
                }
            }
        }
    }
}