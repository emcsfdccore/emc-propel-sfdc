/*==================================================================================================================+

 |  HISTORY  |                                                                           
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        =========== 
 |  27/10/2014    Srikrishna SM         This Test class is used for PRM_Portal_Homepage_Lists_Controller code coverage
 +==================================================================================================================**/
@isTest
private class PRM_Portal_Homepage_Lists_Controller_TC {

    static testMethod void testController() {
        /*
        SELECT Company, DealReg_Department_Project_Name__c, DealReg_Deal_Registration_Number__c, 
                                                                        Synergy_Opportunity_Number_1__c, Name, DealReg_Deal_Registration_Status__c, Country__c, 
                                                                        DealReg_Deal_Registration_Type__c, DealReg_Expiration_Date__c, Partner__c, Partner__r.Name,
                                                                        Tier_2_Partner__c, Tier_2_Partner__r.Name, DealReg_Submission_Source__c, DealReg_Submission_Date__c, Cloned_Deal_Registration__c 
        */
        //Create required custom setting data for the test class
        System.runAs(new user(Id = UserInfo.getUserId())){
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.BusinessPartnerProgramData();
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        }
        //Insert Opportunities for test data
        List<Account> acc = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        insert acc[0];
        List<Opportunity> oppList = OpportunityTestClassDataHelper.createOpptys(acc[0], null);
        oppList[0].Partner_Approval_Status__c = 'Awaiting Response';
        oppList[1].Partner_Approval_Status__c = 'Awaiting Response';
        insert oppList;
        
        //Insert Opportunities for test data
        Lead ld1 = new Lead();
        ld1.DealReg_Deal_Registration_Status__c = 'Approved';
        ld1.DealReg_Deal_Registration__c = true;
        ld1.isConverted = false;
        ld1.DealReg_Expiration_Date__c = System.Today();
        ld1.Company ='TestDealreg2';
        ld1.LastName ='DealregLastName3';
        insert ld1;
        
        Lead ld2 = new Lead();
        ld2.isConverted = false;
		ld2.Status = 'Submitted';
        ld2.DealReg_Deal_Registration__c = false;
        ld2.Company ='TestDealreg2';
        ld2.LastName ='DealregLastName3';
        insert ld2;
        
        Test.startTest();
        PageReference pageRef = Page.PRM_Portal_Homepage_standardUser_Lists; 
        Test.setCurrentPage(pageref);

        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Id, Opportunity_Number__c, Name, Account.name, Partner__c, Partner__r.Name,
                                                                    Tier_2_Partner__c, Tier_2_Partner__r.Name, PSC_Approval_Date_Time__c, Partner_Approval_Status__c, 
                                                                    Opportunity_Registration_Status__c FROM Opportunity 
                                                                    WHERE Partner_Approval_Status__c = 'Awaiting Response']));
        ApexPages.StandardSetController ssc1 = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Company, DealReg_Department_Project_Name__c, DealReg_Deal_Registration_Number__c, 
                                                                        Synergy_Opportunity_Number_1__c, Name, DealReg_Deal_Registration_Status__c, Country__c, 
                                                                        DealReg_Deal_Registration_Type__c, DealReg_Expiration_Date__c, Partner__c, Partner__r.Name,
                                                                        Tier_2_Partner__c, Tier_2_Partner__r.Name, DealReg_Submission_Source__c, DealReg_Submission_Date__c, Cloned_Deal_Registration__c 
                                                                        FROM LEAD WHERE DealReg_Deal_Registration_Status__c = 'Approved'
                                                                        AND DealReg_Expiration_Date__c <= NEXT_N_DAYS:10
                                                                        AND DealReg_Deal_Registration__c = true 
                                                                        AND DealReg_Expiration_Date__c >= Today 
                                                                        AND isConverted = false]));
                                                                        
		ApexPages.StandardSetController ssc2 = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Name, Status, Createddate, Lead_Number__c, Accept_Lead__c, Lead_Rank__c,
                                                                            Passed_to_Partner_Date_Time__c, Email, Company, State, PostalCode, Partner__c, Partner__r.Name, 
                                                                            Tier_2_Partner__c, Tier_2_Partner__r.Name, owner.alias, IsUnreadByOwner FROM LEAD 
                                                                            where isConverted__c = false AND Status not in ('Rejected to Marketing','Closed') 
                                                                            AND DealReg_Deal_Registration__c = false]));
                                                                                                                                                    
        PRM_Portal_Homepage_Lists_Controller test1 = new PRM_Portal_Homepage_Lists_Controller();
        
        List<Opportunity> listOfOpp = test1.getOpportunityList();
        List<Lead> listOfLead1 = test1.getDealsList();
        List<Lead> listOfLead2 = test1.getLeadList();
        Test.stopTest();
        
        // TO DO: implement unit test
    }
}