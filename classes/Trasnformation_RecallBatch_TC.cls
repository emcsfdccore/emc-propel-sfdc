/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |                                              Initial Creation.
 |  26.12.2013      Naga         WR319143       Re-written test class to improve performance.
 +===========================================================================*/
@isTest
public class Trasnformation_RecallBatch_TC {
	
  Trasnformation_RecallBatch_TC(){
  	        CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
  }
    static testmethod void batchTrasnformation_RecallBatchTest() {
    	 //User testUser= [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
         User testUser = new User(Id = Userinfo.getUserId());
    	 System.runAs(testUser){
             Trasnformation_RecallBatch_TC transRecallObj = new Trasnformation_RecallBatch_TC();
    	 }
    	user usr = [Select Id, Theater__c from user where Id =: UserInfo.getUserId()];
        usr.Theater__c = 'APJ';
        update usr;
	    List<Account>  acc = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
	      insert acc;
        Opportunity opp = new Opportunity(AccountId = acc[0].Id, Name = 'Test Opp', Quote_Cart_Number__c = 'Test1234', Quote_Version__c = 'Opp v2', 
                                          Quote_Type__c = 'CxP', Sales_Channel__c = 'Direct', StageName = 'Upside', CloseDate = Date.today()+10, Sales_Force__c = 'EMC', 
                                          VMWare_Related__c = 'VMWare Backup', Amount = 222.00);
        insert opp;
        Pricing_Requests__c par = new Pricing_Requests__c(Opportunity_Name__c = opp.Id, PR_Account_Owner_Divison__c = 'India', Price_Floor_Level__c = 'L4', 
                                                          How_can_we_mask_actual_discount__c = 'TEST 1234', Approval_Status__c ='New', Recalled_Flag__c = true, 
                                                          APJ_Request_Escalation__c = false, EMC_PRODUCTS__c='test',PR_Account_Owner_Theater__c ='APJ');
         Database.insert(par);
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval');
        req.setObjectId(par.Id);
        Approval.ProcessResult result = Approval.process(req); 
        Pricing_Requests__c pricinglst =[Select Name, Owner.Name,Recalled_Flag__c from Pricing_Requests__c where Id =: par.id limit 1];
	       pricinglst.Recalled_Flag__c = true;
	       update pricinglst;
        Test.StartTest();   
            //Changes made to cover the Scheduler class and Recall Batch class
            String CRON_EXP = '0 0 */1 * * ?';
            List<CronTrigger> cronTriggerToAbort = [Select c.State, c.NextFireTime, c.Id, c.CronExpression From CronTrigger c where CronExpression =: CRON_EXP];
            //Iterating over cronTriggerToAbort list
            for(Integer i = 0 ; i < cronTriggerToAbort.size() ; i++){
                System.abortJob(cronTriggerToAbort[i].Id);
            }
            Transformation_RecallScheduler.runScheduler();
            //Changes made by Jaypal Singh Dated: 07-16-2013
        Test.StopTest();
  }
}