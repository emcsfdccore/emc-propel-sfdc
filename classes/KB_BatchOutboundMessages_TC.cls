@isTest
public class KB_BatchOutboundMessages_TC{
    static testMethod void KBBatchOutboundMessages() {
	//Query profile of System Admin
    Profile pId = [select id from profile where name='System Administrator' limit 1];
    User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];

    //Create Custom Setting Data required for the test class
    System.runAs(insertUser){
	    CustomSettingDataHelper.edServiceKBData();
    }
    Test.startTest();
    List<Id> idList = new List<Id>();
    List<CronTrigger> listJobsRunning = new List<CronTrigger>();
    List <Break_Fix__kav> listBreakFixKav =  new List<Break_Fix__kav>();
    //Get List of jobs currently running and abort them
    listJobsRunning=[SELECT Id FROM CronTrigger WHERE (State='WAITING' OR State='EXECUTING')
                     ];
    System.debug('CronTriger'+listJobsRunning);
    If(listJobsRunning.size()>=1) {
        for(CronTrigger jobs:listJobsRunning) {
            System.abortJob(jobs.Id);
        }      
    }
    //Insert record for Custom Setting KB_BatchStartTime__c
    KB_BatchStartTime__c csKBBatchTime = new KB_BatchStartTime__c();
    csKBBatchTime.Name='Start Time';
    csKBBatchTime.Start_Time__c=System.now();
    insert csKBBatchTime;
    // Create 2 Article objects, publish both and arhive the first one
    Break_Fix__kav breakfix=new Break_Fix__kav ();
    breakfix.Title = 'Test Class Break Fix';
    breakfix.UrlName='Test-Class-Breakfix';
    breakfix.Language='en_US';
    breakfix.ValidationStatus='Work In Progress';
    insert breakfix;
    
    Break_Fix__kav breakfix1=new Break_Fix__kav();
    breakfix1.Title='Test Class Break Fix1';
    breakfix1.UrlName='Test-Class-Breakfix1';
    breakfix1.Language='en_US';
    breakfix1.ValidationStatus='Work In Progress';
    insert breakfix1;
    
    Education_Services__kav edService1 = new Education_Services__kav();
    edService1.Title = 'Ed Test 1';
    edService1.UrlName = 'Ed-Test-1';
    edService1.Language='en_US';
    edService1.ValidationStatus='Work In Progress';
    insert edService1;
    
    idList.add(edService1.Id);
    idList.add(breakfix.Id);
    idList.add(breakfix1.Id);
    
    List<KnowledgeArticleVersion> kav= [SELECT id,ArticleType, KnowledgeArticleId,ArticleNumber,PublishStatus FROM KnowledgeArticleVersion WHERE  id in :idList];
    // Publish and Archive the First Article
    KbManagement.PublishingService.publishArticle(kav[0].KnowledgeArticleId,true);
    KbManagement.PublishingService.archiveOnlineArticle(kav[0].KnowledgeArticleId,null);
    // Publish the second Article
    KbManagement.PublishingService.publishArticle(kav[1].KnowledgeArticleId,true);
    KbManagement.PublishingService.publishArticle(kav[2].KnowledgeArticleId,true);
    DataBase.executebatch(new KB_BatchOutboundMessages());
    Test.stopTest();
    }
}