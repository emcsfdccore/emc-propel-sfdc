/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR          DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |                                              Initial Creation.
 |  27.01.2011      Shipra Misra    151892      Updated test class as two new required fields which have been introduced to the system. "Primary ISV & Infrastructure" & "Primary Outsourcer & Integrator".
 |  09.01.2013      Medhavi D.      222446      Updated test class to cover 90% and Feb release changes
 |  09.04.2013      Uday            246616      Fixed test class error.
 |  06.11.2013      Naga            311032      Re-written test class to improve performance.
 +===========================================================================*/
@isTest
private class OpportunityIntegrationOperation_TC{ 
 public Static List<Account> acctLst = new List<Account>();
 public Static List<Opportunity> lstOpp= new List<Opportunity>();
 public static List<OpportunityDetails> OppDetails=new List<OpportunityDetails>();
 
 OpportunityIntegrationOperation_TC(){
      CustomSettingDataHelper.eBizSFDCIntCSData();
      CustomSettingDataHelper.bypassLogicCSData();
      CustomSettingDataHelper.dataValueMapCSData();
 }
 
 //Method for Creation Of Account    
     public static void createAccount(){
       acctLst = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();       
       insert acctLst ;
     } 
    
//Method for Creation Of Opportunity
     public static void createOpportunity(){
        CustomSettingDataHelper.profilesCSData();
        for(Integer k=0;k<2;k++){
           Opportunity opp_Obj = new Opportunity();
           opp_Obj.AccountId =acctLst[k].id;
           opp_Obj.Name = 'TEST'+ k;
           opp_Obj.StageName = 'Test';
           opp_Obj.Quote_Created_by__c = 'Test';
           opp_Obj.HW_TLA_Start_Date__c = Date.today();
           if(k==0){
             opp_Obj.StageName = 'Pipeline';
           }
           opp_Obj.CloseDate = Date.today()+10;
           opp_Obj.Sales_Force__c = 'EMC';
           opp_Obj.Sales_Channel__c = 'Direct';
           opp_Obj.VMWare_Related__c = 'VMWare Backup';
           opp_Obj.Amount = 222.00;
          
           lstOpp.add(opp_Obj);
         }
         
        insert lstOpp;
        //lstOpp[0].Opportunity_Number__c = lstOpp[0];
        //update lstOpp;
     }  //end of method Creation Of Opportunity
    
    //Method for creation of Oppty Line Item
     public static void createOpptyLineItem(){
           CustomSettingDataHelper.adminConversionCSData();
           CustomSettingDataHelper.houseAccountCSData();
           Product2 prod = new Product2(Name = 'Test Product',CurrencyIsoCode = 'USD',productCode = 'ABC', IsActive = true);
             insert prod;
            String standardPriceBookId = '01s70000000EkOZ';
            PricebookEntry pricebook = new PricebookEntry(Pricebook2Id = standardPriceBookId, Product2Id = prod.Id, UnitPrice = 10000,IsActive = true);
             insert pricebook; 
            List<OpportunityLineItem> opptyLineItem= new List<OpportunityLineItem>();
               for(Integer k=0;k<2;k++){
                    OpportunityLineItem opptyLineItem1= new OpportunityLineItem();
                    opptyLineItem1.OpportunityId=lstOpp[k].id;
                    opptyLineItem1.Quantity=5;
                    opptyLineItem1.Quote_Amount__c=5000;
                    opptyLineItem1.PricebookEntryId=pricebook.Id;
                    opptyLineItem.add(opptyLineItem1);
               }
                 insert opptyLineItem;  
     } 
    
    //Method to create OpptyDetails
    public static void createOpptyDetails(){
            OpportunityDetails opptyDetail = OpportunityOperation.getSFDCOpportunity(lstOpp[0].id);
            //OpportunityDetails opptyDetail2 = OpportunityOperation.getSFDCOpportunity(null);
            OppDetails.add(opptyDetail);
     }

     static testMethod void runcreateOpportunities() {
            OpportunityIntegrationOperation_TC oppIntObj = new OpportunityIntegrationOperation_TC();
            //User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
            User insertUser = new User(Id = Userinfo.getUserId());
            createAccount();
            createOpportunity();
            createOpptyLineItem();
            createOpptyDetails();
            OpportunityIntegrationOperation OppIntOperation =new OpportunityIntegrationOperation();
            
            System.runAs(insertUser){
                OppIntOperation.createOpportunities(OppDetails);
//Written by Bhanu(Code Coverage) Start             
                OppDetails[0].POpptyHeaderBo.OracleOppId = OppDetails[0].POpptyHeaderBo.SfdcOpptyId;
                 OppIntOperation.createOpportunities(OppDetails);
                 
                OppDetails[0].POpptyProductBoTblItem= null;
                OppIntOperation.createOpportunities(OppDetails);
//Written by Bhanu(Code Coverage) End
                
                OppIntOperation.udpateCreateOpportunityStatus(OppDetails[0]);
                OppIntOperation.udpateCreateQuoteStatus(OppDetails[0]);
                //OppIntOperation.udpateCreateOpportunityStatus(new OpportunityDetails());
                OppDetails[0].POpptyHeaderBo.SfdcOpptyId = null;
                OppIntOperation.udpateCreateOpportunityStatus(OppDetails[0]);
                OppIntOperation.udpateCreateQuoteStatus(OppDetails[0]);
            } 
               
    }//End of method runcreateOpportunities
    
     static testMethod void runUpdateeOpportunities() {
            OpportunityIntegrationOperation_TC oppIntObj = new OpportunityIntegrationOperation_TC();
            //User testUser= [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
         	User testUser = new User(Id = Userinfo.getUserId());
            createAccount();
            createOpportunity();
            createOpptyLineItem();
            createOpptyDetails();
                       
            OpportunityIntegrationOperation OppIntOperationUpdate =new OpportunityIntegrationOperation();
            System.runAs(testUser){
            OppIntOperationUpdate.updateOpportunities(OppDetails);
//Written by Bhanu - for code coverage - Start
            String oracleOppId = OppDetails[0].POpptyHeaderBo.OracleOppId;
            String sfdcOpptyId = OppDetails[0].POpptyHeaderBo.SfdcOpptyId;
            System.debug('***128, oracleOppId= ' + oracleOppId + '  sfdcOpptyId= ' + sfdcOpptyId);
            OppDetails[0].POpptyHeaderBo.SfdcOpptyId = null;//covers 406 condition
            //OppIntOperationUpdate.updateOpportunities(OppDetails);//covers 431 condition
            OppDetails[0].POpptyHeaderBo.OracleOppId = sfdcOpptyId;//covers 406 condition
            OppIntOperationUpdate.updateOpportunities(OppDetails);//covers 406 condition
            //OppIntOperationUpdate.updateOpportunities(OppDetails);//covers 406 condition
            
            OppDetails[0].POpptyHeaderBo.SfdcOpptyId = null;//covers 406 condition
            OppDetails[0].POpptyHeaderBo.OracleOppId = null;//covers 406 condition
            //OppDetails[0].POpptyHeaderBo.SfdcOpptyId = sfdcOpptyId;//covers 431 condition
            OppIntOperationUpdate.updateOpportunities(OppDetails);//covers 431 condition
            
            //OppDetails[0].POpptyHeaderBo.SfdcOpptyId = sfdcOpptyId;//covers 431 condition
            //OppDetails[0].POpptyHeaderBo.OracleOppId = oracleOppId;//covers 438 condition
            //OppIntOperationUpdate.updateOpportunities(OppDetails);//covers 438 condition
            
            
//Written by Bhanu - for code coverage - End            
            } 
     }//End of method runUpdateeOpportunities
}//End of class