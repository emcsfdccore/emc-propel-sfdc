/*=======================================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER                WR          DESCRIPTION                               

 |  ====          =========                ==          =========== 

 |  15 Oct 2013    Srikrishnadevaraya                  This class will be used to populate Related Account Local Info on corresponding DRs,
                                                        
+=======================================================================================================================================*/

@isTest
private class PRM_DEALREG_UpdateLocalFieldsOnDR_TC{

    private static testMethod void updateLocalFieldsOnDR(){
        
        CustomSettingDataHelper.dataValueMapCSData();
        CustomSettingDataHelper.eBizSFDCIntCSData();
        CustomSettingDataHelper.profilesCSData();
        CustomSettingDataHelper.dealRegistrationCSData();
        CustomSettingDataHelper.bypassLogicCSData();
        
        List<Account> accList = new List<Account>();
        
        ID sysid = [ Select id from Profile where name ='System Administrator - API Only' limit 1].id;
        User testUser = new User( email='test-user@fakeemail.com',profileid = sysid ,  UserName='test-userunique1@fakeemail.com', alias='tuser1', CommunityNickName='tuser1', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' );
        insert testUser;        
        
        Map<Id,Account> mapAccount = new Map<Id,Account>();
        
        List<account> lstAccount = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        List<account> lstPartnerAccount = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
        insert lstAccount;
        system.debug('lstAccount--->' +lstAccount);
        insert lstPartnerAccount;
        system.debug('lstPartnerAccount--->' +lstPartnerAccount);
        for(Account account:lstPartnerAccount){
            account.IsPartner = true;
        }
        update lstPartnerAccount;
        
        List<lead> lstLead = LeadTestClassDataHelper.createDealReg(lstAccount[0],null,lstPartnerAccount[0],lstPartnerAccount[0]);
        System.runAs(testUser)
        {
            insert lstlead[0];
        }
        
        Test.StartTest();
        
            system.debug('lstlead[0]--->' +lstlead[0]);
            lstAccount[0].State_Province_Local__c = 'testState1';
            system.debug('lstAccount[0]------>' +lstAccount[0]); 
            mapAccount.put(lstAccount[0].Id,lstAccount[0]);
            //update lstAccount[0];
         
            PRM_DEALREG_UpdateLocalFieldsOnDR obj = new PRM_DEALREG_UpdateLocalFieldsOnDR();
            obj.updateAddressOnDR(mapAccount);
        
        Test.StopTest();
    }   
  
}