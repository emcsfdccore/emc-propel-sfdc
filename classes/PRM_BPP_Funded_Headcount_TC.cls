/*=====================================================================================================+
 |  HISTORY  |
 |  DATE          DEVELOPER               WR            DESCRIPTION 
 |  ====          =========               ==            =========== 
 |  18/Sep/2014  Jaypal Nimesh      BPP WR 379763      Created test class for PRM_BPP_Funded_Headcount class.
 |
 +=====================================================================================================*/
 
@isTest
private class PRM_BPP_Funded_Headcount_TC{

    public static testmethod void createNewContact() {
    
      //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        } 
       
        List<Contact> contLst = new List<Contact>();
        
        //Creating custom setting data
        CustomSettingDataHelper.BusinessPartnerProgramData();

        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
           
        System.runAs(thisUser){
            
            List<Account> objAccountList = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();
            
            for(Account acc : objAccountList){
                acc.Partner_Type__c = 'Distribution VAR';
            }
            //Inserted partner Account
            insert objAccountList;
       
            BusinessPartnerProgram__c contactRecordTypes= BusinessPartnerProgram__c.getValues('ContactFundedHeadRecTypeIds');
            String str = contactRecordTypes.APInames__c;

            String[] sp = str.split(';');
            
            //Creating contacts for bulk testing for different record Types     
            for(integer i=0;i<200;i++){
                if(i<=50){                                
                    contLst.add(new Contact(lastname = 'testcontact' + i, RecordTypeId=sp[0], accountId=objAccountList[0].Id,Funded_Head__c= True, Partner_Contact2__c = true));
                }
                if(i>50 && i<=100){    
                    contLst.add(new Contact(lastname = 'testcontact' + i, RecordTypeId=sp[1], accountId=objAccountList[0].Id,Funded_Head__c= True, Partner_Contact2__c = true));
                }
                if(i>100 && i<=150){    
                    contLst.add(new Contact(lastname = 'testcontact' + i, RecordTypeId=sp[2], accountId=objAccountList[0].Id,Funded_Head__c= True, Partner_Contact2__c = true));
                }
                if(i>150){    
                    contLst.add(new Contact(lastname = 'testcontact' + i, RecordTypeId=sp[3], accountId=objAccountList[0].Id,Funded_Head__c= True, Partner_Contact2__c = true));
                }
            }            
            Insert contLst;          
            
            //Chnaged funded flag to false for update scenario
            for(Contact con : contLst){               
                con.Funded_Head__c = false;         
            }            
            update contLst; 
        } 
    } 
}