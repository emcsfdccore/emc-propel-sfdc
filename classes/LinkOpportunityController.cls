/*==================================================================================================================+
 |  HISTORY  |                                                                            
 |  DATE          DEVELOPER        WR        DESCRIPTION                               
 |  ====          =========        ==        =========== 
 |  16/12/2014   Vivek Barange    1557       Created controller to associated multiple child opportunities to an opportunity
==========================================================================================*/
public class LinkOpportunityController {
    public Search_Proxy_Container__c proxy {get;set;}
    public String errorMsg {get;set;}
  public String currentOptyId {get;set;}
    public boolean displayCheckbox {get;set;}
    public boolean doOverride {get;set;}
  public Map<Id, Opportunity> mapChildOpties {get;set;}
    public LinkOpportunityController(ApexPages.StandardController controller) {
    currentOptyId = controller.getId();
    proxy = new Search_Proxy_Container__c();
        errorMsg = '';
        displayCheckbox = false;
        doOverride = false;
    mapChildOpties = new Map<Id, Opportunity>([Select Id from Opportunity where Related_Opportunity__c = :currentOptyId]);
  }
    
    public PageReference linkOppty() {
        displayCheckbox = false;
        if(proxy.Select_Child_Opportunity__c == null) {
      errorMsg = System.Label.Please_Select_Opty;
            return null;
        }
    
    if(mapChildOpties.containsKey(proxy.Select_Child_Opportunity__c)) {
      errorMsg = System.Label.Opty_Already_Associated;
      return null;
    }
      List<Opportunity> lstOpty = [Select Id, Related_Opportunity__c from Opportunity where Id = :proxy.Select_Child_Opportunity__c];
        if(!lstOpty.isEmpty()) {
            List<UserRecordAccess> lstUserRecordAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :proxy.Select_Child_Opportunity__c];
            system.debug('lstUserRecordAccess : '+lstUserRecordAccess.size());
      if(!Opportunity.Related_Opportunity__c.getDescribe().isUpdateable()) {
                errorMsg = System.Label.Not_Permitted_To_Relate_Child;
                return null;                
      } else {
        if(lstUserRecordAccess[0].HasEditAccess) {
                    if(lstOpty[0].Related_Opportunity__c == null || doOverride) {
            lstOpty[0].Related_Opportunity__c = currentOptyId;
                        try {
                            system.debug('lstOpty : '+lstOpty);
              update lstOpty;
                        } catch(DMLException dml) {
                            errorMsg = dml.getDmlMessage(0);
                            system.debug('dml: '+dml);
              if(String.valueOf(dml).contains('CIRCULAR_DEPENDENCY')) {
                errorMsg = System.Label.Opty_CIRCULAR_DEPENDENCY;
              }
                            return null;
                        } catch(Exception ex) {
                            errorMsg = ex.getMessage();
                            system.debug('ex: '+ex);
                            return null;
                        }
          } else {
                        errorMsg = System.Label.Override_Opty;
            displayCheckbox = true;
            return null;
          }
                } else {
                    errorMsg = System.Label.Opty_Not_Accessible;
          return null;
                }
      }
        } 
        return new PageReference('/'+currentOptyId+'#'+currentOptyId+'_00N70000002qD7K_target');
    }
    
    public PageReference cancel() {
    return new PageReference('/'+currentOptyId);
  }
}