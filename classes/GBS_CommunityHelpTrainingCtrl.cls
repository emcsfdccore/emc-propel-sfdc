public class GBS_CommunityHelpTrainingCtrl{
    
    public String getGroupId() {
        
        GBS_Config__c gbsConfigGrpInfo = null;
        
        Profile userProfile = [Select Id, Name from Profile where Id =: UserInfo.getProfileId()];
        
        GBS_Config__c gbsConfigExternalProfile = GBS_Config__c.getValues('GBSHelpTrainingExternalProfiles');
        
        if(gbsConfigExternalProfile.ParamValue__c.containsIgnoreCase(userProfile.Name)){
            
            gbsConfigGrpInfo = GBS_Config__c.getValues('GBSHelpTrainingGroupExternal');
        }
        else{
            gbsConfigGrpInfo = GBS_Config__c.getValues('GBSHelpTrainingGroupEmployee');
        }
        
        CollaborationGroup gbsHelpTrainGrp = [Select Id, Name from CollaborationGroup where Name =: gbsConfigGrpInfo.ParamValue__c];
        
        return gbsHelpTrainGrp.Id;
    
    }
    
    public GBS_CommunityHelpTrainingCtrl(){
        
    }
}