/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req     DESCRIPTION                               
 |  ====            =========       ======     ===========             
 |  05.May.2015    Bhanuprakash     PROPEL     Created to test Record_RedirectController class
 +=========================================================================================================================*/
 
@isTest
private class Record_RedirectController_TC{
    public Static List<Account> acctLst = new List<Account>();
    public Static List<Opportunity> lstOpp= new List<Opportunity>();
 
    Record_RedirectController_TC() {}
    
     //Method for Creation Of Test Data    
     @testSetup static void setupData(){
        //Create Custom settings
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        }   
       //Create Account
       acctLst = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();       
       insert acctLst ;
       
       // Create list of Opptys
        for(Integer k=0;k<2;k++){
           Opportunity opp_Obj = new Opportunity();
           opp_Obj.AccountId =acctLst[0].id;
           opp_Obj.Name = 'TEST'+ k;
           opp_Obj.StageName = 'Test';
           opp_Obj.StageName = 'Pipeline';
           opp_Obj.CloseDate = Date.today()+10;
           opp_Obj.Sales_Force__c = 'EMC';
           opp_Obj.Sales_Channel__c = 'Direct';
          
           lstOpp.add(opp_Obj);
         }//end - for
         
        insert lstOpp;
     }
     //Test method to test 'Record_RedirectController.redirect()' method
    @isTest static void  testRedirect() {
    Test.starttest();
        
        //Prepare request context
        Test.setCurrentPageReference(new PageReference('Page.Record_Redirect'));
        System.currentPageReference().getParameters().put('object', 'Opportunity');
        System.currentPageReference().getParameters().put('field', 'opportunity_number__c');

        //Pagereference pageref = Page.Record_Redirect;
        //pageref.getParameters().put('object', 'Opportunity');
        //pageref.getParameters().put('field', 'opportunity_number__c');
        
        List<Opportunity> oppRequest = [SELECT opportunity_number__c FROM Opportunity LIMIT 10];
        System.assertNotEquals(oppRequest, null);
        System.debug('*** queried oppty = '+ oppRequest);
        
        //pageref.getParameters().put('value', oppRequest[0].opportunity_number__c);
        System.currentPageReference().getParameters().put('value', oppRequest[0].opportunity_number__c);
        //call actual class and method
        Record_RedirectController record = new Record_RedirectController();
        //Test : positive scenario
        record.redirect();
        
        //Test : Negetive : if parameters are empty
        System.currentPageReference().getParameters().put('object', '');
        System.currentPageReference().getParameters().put('field', '');
        System.currentPageReference().getParameters().put('value', '');
        record.redirect();
        System.assertNotEquals(record,null);
        
        //Test : Negetive : if DML exception
        System.currentPageReference().getParameters().put('object', 'Opportunity');
        System.currentPageReference().getParameters().put('field', 'opportunity_number_');
        System.currentPageReference().getParameters().put('value', 'Test');
        record.redirect();
        
        //Test : Negetive : if more than 1 record found
        //Update 2nd opty to fetch 2 oppty records
        Opportunity updateOppty = oppRequest[1];
        updateOppty.Name = 'TEST0';
        update updateOppty;
        
        System.currentPageReference().getParameters().put('object', 'Opportunity');
        System.currentPageReference().getParameters().put('field', 'Name');
        System.currentPageReference().getParameters().put('value', 'TEST0');
        record.redirect();
        
        //Test : Negetive : if more 0 record found
        System.currentPageReference().getParameters().put('object', 'Account');
        System.currentPageReference().getParameters().put('field', 'name');
        System.currentPageReference().getParameters().put('value', 'ABCD_Account	');
        record.redirect();
        System.assertNotEquals(record,null);
        
    Test.stoptest();
    }
}