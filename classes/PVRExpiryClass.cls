/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req     DESCRIPTION                               
 |  ====            =========       ======     =========== 
 |  11.03.2015      Paridhi Aggarwal    DM PVR      To create a PVR record with same value as Case of PVR-Data migration\data migration extension
                                                            
 |  23.03.2015      Harshad                         Added method to update the value of PVR Record on updation of Case
 |  
+=========================================================================================================================*/

public class PVRExpiryClass
 {
 
 /*Method to create the record of PVR Expiry automatically on creation of case PVR-datamigration or extension*/

   public  void creationPVRExpiry(List<Case> caseList)
     {
       //Set<Id> CaseId = New Set<Id>();
       List<PVR_Expiry__c> PVRExpiryRecordList = new List<PVR_Expiry__c>();
       List<case> CList = new List<case>();

       CList = [Select id,Account.Core_Quota_Rep__r.email,Account.Core_Quota_Rep__r.name,Account.name,Contact.email,Contact.name,Concession_End_Date__c,Field_Rep__r.name,Global_Ultimate_Identifier__c,Global_Account__c,Opportunity_Name__r.Opportunity_Number__c,Opportunity_Name__r.Name,Opportunity_Name__r.Owner.name,Record_Type_Developer_Name__c,Type FROM Case WHERE Id IN :CaseList];
if(CList.Size()>0)
{       
       for(Case caseObj:CList)
        {               
          if(caseObj.Record_Type_Developer_Name__c  == 'Presales_Renewal_Policy_Variation_Request_PVR' && (caseObj.Type == 'Data Migration'|| caseObj.Type =='Data Migration Extension' ))
        {
            PVR_Expiry__c expiryobj= new PVR_Expiry__c( );
            
            expiryobj.Concession_End_Date__c = caseObj.Concession_End_Date__c;
            expiryobj.Opportunity_Owner__c = CaseObj.Opportunity_Name__r.Owner.name;
            expiryobj.Field_Rep__c =caseObj.Field_Rep__r.name;
            expiryobj.Case__c = caseObj.Id;
            expiryobj.Account_Name__c = caseObj.Account.name;
            expiryobj.Contact_Name__c =caseObj.Contact.name;
            expiryobj.Opportunity_Name__c =caseObj.Opportunity_Name__r.Name;
  //          expiryobj.Delivery_Manager__c=caseObj.Delivery_Manager__r.name;
            expiryobj.Global_Ultimate_Identifier__c = caseObj.Global_Ultimate_Identifier__c;
            expiryobj.Global_Multinational_Account__c = caseObj.Global_Account__c;
           
           expiryobj.Core_Quota_rep__c = caseobj.Account.Core_Quota_Rep__r.name;
           system.debug(caseobj.Account.Core_Quota_Rep__r.name);
            expiryobj.Core_Quota_rep_Email__c =caseobj.Account.Core_Quota_Rep__r.email;
            expiryobj.Renewals_Rep_Email__c = caseobj.Contact.email;
            expiryobj.Opportunity_Number__c = caseobj.Opportunity_Name__r.Opportunity_Number__c;
            
system.debug(caseobj.Account.Core_Quota_Rep__c);
            PVRExpiryRecordList.add(expiryobj);
            
            
           
        }
    }
    }
    if(!PVRExpiryRecordList.isEmpty() )
        insert PVRExpiryRecordList;
   
   }
   
   /*Method to update the record of PVR Expiry automatically on changes made in  case PVR-datamigration or extension*/
   
    public  void updationPVRExpiry(List<Case> caseList)
     {
        List<case> CList = new List<case>();
        //Set<Id> CaseId = New Set<Id>();
        Map<Id,List<PVR_Expiry__c>> mapCasePvr = new Map<Id,List<PVR_Expiry__c>>();
       list<PVR_Expiry__c> listPVRExpiry = [select id,Case__c from PVR_Expiry__c where Case__c in :  CaseList ];
       list<PVR_Expiry__c> updatelistPVRExpiry = new list<PVR_Expiry__c>();
       
       for(PVR_Expiry__c pvrExpObj : listPVRExpiry){
            if(mapCasePvr.containsKey(pvrExpObj.Case__c)){
                mapCasePvr.get(pvrExpObj.Case__c).add(pvrExpObj);
            }
            else{
                List<PVR_Expiry__c> lstPVR = new List<PVR_Expiry__c>();
                lstPVR.add(pvrExpObj);
                mapCasePvr.put(pvrExpObj.case__c , lstPVR);
            }
       }
       
       
       if (listPVRExpiry.size()>0)
       {
       CList = [Select id,Account.Core_Quota_Rep__r.name,Account.name,Type,Contact.name,Concession_End_Date__c,Field_Rep__r.name,Opportunity_Name__r.Opportunity_Number__c,Opportunity_Name__r.Owner.name,Opportunity_Name__r.Name,Record_Type_Developer_Name__c,Global_Ultimate_Identifier__c,Global_Account__c,Account.Core_Quota_Rep__r.email,Contact.email FROM Case WHERE Id IN :CaseList];
       for(Case caseObj : CList )
        {
            if(mapCasePvr.ContainsKey(caseObj.id)){
            for (PVR_Expiry__c PVR : mapCasePvr.get(caseObj.id) )
            {
                if(caseObj.Record_Type_Developer_Name__c  == 'Presales_Renewal_Policy_Variation_Request_PVR' && (caseObj.Type == 'Data Migration'|| caseObj.Type =='Data Migration Extension' ))
                {       
                    PVR.Concession_End_Date__c = caseObj.Concession_End_Date__c;
                    PVR.Opportunity_Owner__c = CaseObj.Opportunity_Name__r.Owner.name;
                    PVR.Field_Rep__c =caseObj.Field_Rep__r.name;
                    PVR.Account_Name__c = caseObj.Account.name;
                    PVR.Contact_Name__c =caseObj.Contact.name;
                    PVR.Opportunity_Name__c =caseObj.Opportunity_Name__r.Name;
                    PVR.Global_Ultimate_Identifier__c = caseObj.Global_Ultimate_Identifier__c;
                    PVR.Global_Multinational_Account__c = caseObj.Global_Account__c;
                    PVR.Core_Quota_rep__c = caseobj.Account.Core_Quota_Rep__r.name;
                    PVR.Core_Quota_rep_Email__c =caseobj.Account.Core_Quota_Rep__r.email;
                    PVR.Renewals_Rep_Email__c = caseobj.Contact.email;
                    PVR.Opportunity_Number__c = caseobj.Opportunity_Name__r.Opportunity_Number__c;
                    updatelistPVRExpiry.add(PVR);
                }
            }
            }           
        }
       }
       if (updatelistPVRExpiry.size()>0 )
        {
            update updatelistPVRExpiry;
            }
     }
     
     
     /*To display error message whenever case of PVR  doesn't have concession end date
     public  void concessionerror(List<Case> caseList)
{ 
    if(caseList.Size()>0)
    {  
     for(case caseobj:caseList)
     {   
      if(caseobj.Record_Type_Developer_Name__c=='Presales_Renewal_Policy_Variation_Request_PVR'&&
         (caseobj.Type == 'Data Migration'|| caseobj.Type == 'Data Migration Extension')&&
          caseobj.Concession_End_Date__c== null &&
           caseobj.Theatre__c != 'APJ')
           {
              caseobj.Concession_End_Date__c.addError('Please enter concession end date');
            }
      }
    }
     
  }   */
}