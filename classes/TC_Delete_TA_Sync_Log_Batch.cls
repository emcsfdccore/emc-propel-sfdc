/*=============================================================================
    HISTORY                                                                  
                                                               
    DATE            DEVELOPER                WR             DESCRIPTION                               
    ====            =========                ==             =========== 
    04-Apr-2011     Srinivas Nallapati      139074          test class for Delete_TA_Sync_Log_Batch 
    26-Dec-2013     Jaypal Nimesh       Backward Arrow      Optimized class and added LIMIT in query   
    16-Apr-2014     Matthew Roark           355859          Updated class to fix errors where the class was failing due to
                                                            mixed DML errors   
  24-Apr-2014     Matthew Roark           355859          Applied another fix due to a failure which was occurring in SFDC03
															when the current context user was already assigned to the PermissionSet permSet
 |  04 Dec 2014     Bhanuprakash    PROPEL 	Part of E.002, Replaced 'Sales-Sales Rep’ with ‘Enterprise Sales Rep’                                                        
==============================================================================*/

@isTest
private class TC_Delete_TA_Sync_Log_Batch
{
    static testMethod void myUnitTest() 
    {   
      User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
        
        PermissionSet permSet = [select Id, name from PermissionSet where name = :System.Label.TA_Sync_Log_Permission_Set_Name limit 1];
     
             
        PermissionSetAssignment permSetAssign;
        
        try
        {
          permSetAssign = [Select Id, PermissionSetId, AssigneeId from PermissionSetAssignment where PermissionSetId= :permSet.Id and AssigneeId = :System.Userinfo.getUserId() limit 1];
        }
        catch (Exception ex)
        {
          permSetAssign = new PermissionSetAssignment(PermissionSetId=permSet.Id,AssigneeId = System.Userinfo.getUserId()); 
        }
        //Inserting custom setting data in different context
        System.runAs(insertUser)
        {   
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.adminConversionCSData();
            CustomSettingDataHelper.houseAccountCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.accessTASyncLogCSData();
            if (permSetAssign.Id == null)
            {
                insert permSetAssign;
            }
        }
        
        Profile p = [select id from profile where name='System Administrator' limit 1];
        
        List<User> lstUser = new List<User>();
        
        for(Integer i = 0 ; i < 7 ; i++){
            
            User u = new User();
            u.alias = 'utest' + i;
            u.email= i + 'testSample@test.com';
            u.emailencodingkey='UTF-8';
            u.lastname='Unit Test' + i;
            u.languagelocalekey='en_US';
            u.localesidkey='en_GB';
            u.profileid = p.Id;
            u.timezonesidkey='Europe/London';
            u.username='sysAdmin' + i + 'Test@emc.com';
            u.Last_TA_Synch_Date__c = system.today()-30;
            
            lstUser.add(u);
        }
        
        //Inserting users with Specific data
        System.runAs(insertUser){
            insert lstUser;
        }

        Profiles__c profile = Profiles__c.getOrgDefaults();
        Id apiOnlyProfileId = profile.System_Admin_API_Only__c;
        
        //Creating accounts with specific data
        List<Account> lstAccount = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        System.runAs(insertUser){
        insert lstAccount;
        }
        User sysAdmAPIUser=[Select id,name from User where profileId =: apiOnlyProfileId and Isactive= true limit 1];
        
        //Creating AccountTeamMember records
        List<AccountTeamMember> lstATM= new List<AccountTeamMember>();
        
        System.runAs(sysAdmAPIUser)
        {
            AccountTeamMember atm1= new AccountTeamMember();
            atm1.accountId= lstAccount[0].id;
            atm1.UserId= lstUser[2].id;
            atm1.teammemberrole='Enterprise Sales Rep';
            
            AccountTeamMember atm2= new AccountTeamMember();
            atm2.accountId= lstAccount[1].id;
            atm2.UserId= lstUser[1].id;
            atm2.teammemberrole='Enterprise Sales Rep';
            
            AccountTeamMember atm3= new AccountTeamMember();
            atm3.accountId= lstAccount[2].id;
            atm3.UserId= lstUser[1].id;
            atm3.teammemberrole='Enterprise Sales Rep';
            
            AccountTeamMember atm4= new AccountTeamMember();
            atm4.accountId= lstAccount[3].id;
            atm4.UserId= lstUser[2].id;
            atm4.teammemberrole='Enterprise Sales Rep';

            lstATM.add(atm1);
            lstATM.add(atm2);
            lstATM.add(atm3);
            lstATM.add(atm4);
            
            Insert lstATM;
        }
        
        //List TA Sync Logs
        list<TA_Sync_Log__c> lstTA = new list<TA_Sync_Log__c>();
        
        System.assert(lstUser[0].Id != null, 'Error when inserting test User 1.  Id not set.');
        TA_Sync_Log__c ta1 = new TA_Sync_Log__c(Status__c='Open',Sales_Resource__c =lstUser[0].id );
        System.assert(lstUser[1].Id != null, 'Error when inserting test User 2.  Id not set.');
        TA_Sync_Log__c ta2 = new TA_Sync_Log__c(Status__c='Sync Complete',Sales_Resource__c =lstUser[1].id );
        System.assert(lstUser[2].Id != null, 'Error when inserting test User 3.  Id not set.');
        TA_Sync_Log__c ta3 = new TA_Sync_Log__c(Status__c='EIS Insertions Complete' ,Row_Count__c=5,Sales_Resource__c =lstUser[2].id );
        System.assert(lstUser[3].Id != null, 'Error when inserting test User 4.  Id not set.');
        TA_Sync_Log__c ta4 = new TA_Sync_Log__c(Status__c='EIS Insertions Complete' ,Row_Count__c=0,Sales_Resource__c =lstUser[3].id );
        System.assert(lstUser[4].Id != null, 'Error when inserting test User 5.  Id not set.');
        TA_Sync_Log__c ta5 = new TA_Sync_Log__c(Status__c='EIS Insertions Complete' ,Row_Count__c=10,Sales_Resource__c =lstUser[4].id );
        System.assert(lstUser[5].Id != null, 'Error when inserting test User 6.  Id not set.');
        TA_Sync_Log__c ta6 = new TA_Sync_Log__c(Status__c='Rowcount Validated' ,Row_Count__c=5,Sales_Resource__c =lstUser[5].id );
        System.assert(lstUser[6].Id != null, 'Error when inserting test User 7.  Id not set.');
        TA_Sync_Log__c ta7 = new TA_Sync_Log__c(Status__c='EIS Insertions Complete' ,Sales_Resource__c =lstUser[6].id );
                
        lstTA.add(ta1);
        lstTA.add(ta2);
        lstTA.add(ta3);
        lstTA.add(ta4);
        lstTA.add(ta5);
        lstTA.add(ta6);
        lstTA.add(ta7);
        
        
       User usrPermSetAssign = [Select Id from User where IsActive = true and Id in (Select p.AssigneeId From PermissionSetAssignment p where PermissionSet.Name = :System.Label.TA_Sync_Log_Permission_Set_Name) limit 1];
            
         System.runAs(usrPermSetAssign ){
        insert lstTA;
        }
        Datetime sysTime = System.now();
        
        //Creating Job Scheduler record
        Job_Scheduler__c js = new Job_Scheduler__c();
        
        js.Name = 'Inbound New Schedules';
        js.Operations__c = 'Update TA Sync Record';
        js.Start_Date__c = date.today();
        js.Account_Locking__c=true;
        js.status__c= 'Pending';
        js.Schedule_Hour__c = sysTime.hour();
        js.Minutes__c = sysTime.minute();
        System.runAs(insertUser){
        system.debug('****js*'+js);
        insert js;
        }
                             
        String query = 'Select Sales_Resource__c, Status__c, LastModifiedDate, OwnerId, Name, Id, Error__c, Count__c From TA_Sync_Log__c limit 10';
        //Added LIMIT by Jaypal Nimesh for Backward Arrow Project - 17 Dec 2013
        
        //Creating Team Members after TA Sync records are created.
        List<AccountTeamMember> lstATMeafterTASYNCCreation= new List<AccountTeamMember>();
        
        System.runAs(sysAdmAPIUser)
        {
            AccountTeamMember atm6= new AccountTeamMember();
            atm6.accountId= lstAccount[3].id;
            atm6.UserId= lstUser[4].id;
            atm6.teammemberrole='Enterprise Sales Rep';
            
            AccountTeamMember atm7= new AccountTeamMember();
            atm7.accountId= lstAccount[3].id;
            atm7.UserId= lstUser[3].id;
            atm7.teammemberrole='Enterprise Sales Rep';
            
            AccountTeamMember atm8= new AccountTeamMember();
            atm8.accountId= lstAccount[2].id;
            atm8.UserId= lstUser[2].id;
            atm8.teammemberrole='Enterprise Sales Rep';
            
            AccountTeamMember atm9= new AccountTeamMember();
            atm9.accountId= lstAccount[1].id;
            atm9.UserId= lstUser[1].id;
            atm9.teammemberrole='Enterprise Sales Rep';
            
            AccountTeamMember atm10= new AccountTeamMember();
            atm10.accountId= lstAccount[0].id;
            atm10.UserId= lstUser[1].id;
            atm10.teammemberrole='Enterprise Sales Rep';
            
            AccountTeamMember atm11= new AccountTeamMember();
            atm11.accountId= lstAccount[0].id;
            atm11.UserId= lstUser[5].id;
            atm11.teammemberrole='Enterprise Sales Rep';
            
            lstATMeafterTASYNCCreation.add(atm6);
            lstATMeafterTASYNCCreation.add(atm7);
            lstATMeafterTASYNCCreation.add(atm8);
            lstATMeafterTASYNCCreation.add(atm9);
            lstATMeafterTASYNCCreation.add(atm10);
            lstATMeafterTASYNCCreation.add(atm11);
            
            Insert lstATMeafterTASYNCCreation;
            
            //List Staging Data
            List<Staging_Data__c> lststgData = new List<Staging_Data__c>();
            
            for(Integer i = 0 ; i < 7 ; i++){
                
                Staging_Data__c stgData = new Staging_Data__c();
                
                stgData.Object_Name__c = 'TASyncDelete';
                stgData.Text1__c = lstTA[i].id;
                if(i == 0){
                    stgData.Text2__c = lstATM[0].id;
                }
                else if(i == 1){
                    stgData.Text2__c = lstATM[1].id;
                }
                else{
                    stgData.Text2__c = lstATM[2].id;
                }
                
                lststgData.add(stgData);
            }           
            insert lststgData;
        }
        
        ta1.status__c='EIS Insertions Complete'; 
         System.runAs(insertUser){
        update ta1;
        }
        
        System.runAs(insertUser) {
        //Test started
        Test.StartTest();
        
        Util.isTestCoverage=true;
        
        //Creating instance of batch and executing
        Update_TA_Sync_Records_SFDC_Count TABatch = new Update_TA_Sync_Records_SFDC_Count(js,query);
        TABatch.Query = query;
        TABatch.js=js;

        ID batchprocessid = Database.executeBatch(TABatch);
        
        sysTime=sysTime.addSeconds(59);
        
        String chron_exp = '' + sysTime.second()+ ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ';
        
        System.runAs(insertUser){
        //Scheduling job and executed Batch
        System.schedule('testScheduledApex2', chron_exp, new Schedule_Delete_TA_SYNC_LOG(js,query));
        }
        //System.runAs(insertUser){
        system.debug('****batch*'+js);
        database.executeBatch(new Delete_TA_Sync_Log_Batch(query,js));
        //}
        Test.stopTest();  
        }       
    }
}