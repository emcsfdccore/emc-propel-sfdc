/*=====================================================================================================+
 |  HISTORY  |
 |  DATE          DEVELOPER               WR         DESCRIPTION 
 |  ====          =========               ==         =========== 
 |  28/06/2011    Anand Sharma                      This class is used to unit test  PRM_DEALREG_RegistrationConversion
 |  04/10/2011    Anand Sharma                      Updated test class to fix too many Query 
 |  20.10.2011    Shipra Misra          173695      Worked for Nov release.
                                                    Introduction new allaince partner & deleting existing ISV & Outsourcer field.Commenting existing code for ISV & Outsourcer.
 |  08/11/2011    Anand Sharma                      Fixed test class issue for Partner Field of opportinity record
 |  14/12/2011      Anil                            Removed Query for fetching Partner and used Custom setting Data Helper
    19/12/2012    Anand Sharma                      Exception Issue
|   09/12/2013   Jaypal Nimesh      Backward Arrow  Changes made to optimize the class processing time and also improved code coverage.
|   23/04/2014   Bisna V P                          Fixed System.LimitException: Too many SOQL queries, Code moved to new method 'myUnitTest2'
|   30/05/2014   Bisna V P           CI WR#936      Updated to cover changes made by CI 936
|   04 Dec 2014     Bhanuprakash    PROPEL          Part of E.002, Replaced 'Sales-Sales Rep’ with ‘Enterprise Sales Rep’  
    30 Dec 2014  Amit                PROPEL         Fixed TC Failure issue
 +=====================================================================================================*/
 
@isTest
public class PRM_DEALREG_RegistrationConversion_TC {
    
    static Map<String,Schema.RecordTypeInfo> recordTypesDetails = Lead.sObjectType.getDescribe().getRecordTypeInfosByName();
   /* public void insertBypassAction(){
        CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c(); 
        custset.By_Pass_Opportunity_Triggers__c = true;
        custset.SetupOwnerId = UserInfo.getUserId();
        insert custset;
    }*/
    static testMethod void myUnitTest() {        
        
        PRM_DEALREG_RegistrationConversion_TC PRM_DEALREG = new PRM_DEALREG_RegistrationConversion_TC ();
               
        ID sysid = [ Select id from Profile where name ='System Administrator' limit 1].Id;
        
        User insertUser = new user(email='test-user@emailTest.com',profileId = sysid ,  UserName='test-user2015@emailTest.com', alias='tuser1', CommunityNickName='tuser1', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User', BU_Attribute__c = 'Core', Forecast_Group__c = 'Maintenance Renewals'); 
        
        insert insertUser;        
        
        
        System.runAs(new user(Id = UserInfo.getUserId())){
        
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.adminConversionCSData();
            CustomSettingDataHelper.houseAccountCSData();
        }  
        
        List<Account> accPartnerList = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();       
        insert accPartnerList;
        Contact cont = UserProfileTestClassDataHelper.createContact();
        cont.AccountId=accPartnerList[0].Id;
        insert cont;
                     
        //List<Account> accList = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        
        //Amit: Creation of Partner Accounts explicitly to overcome "Duplicate" error
                
         List<Account> accList = new List<Account>();
         
         Account objAccount = new Account();
         objAccount.name = 'TestPartnerAcc';
         objAccount.Party_Number__c = '232323';            
         objAccount.BillingCountry ='Colombia';
         objAccount.Synergy_Account_Number__c = '10';
         objAccount.Type = 'Partner';
         objAccount.Lead_Oppty_Enabled__c = true;
         objAccount.Partner_Type__c ='Distributor';
         objAccount.Site_DUNS_Entity__c='001017771';
         objAccount.PROFILED_ACCOUNT_FLAG__c = true;
         accList.add(objAccount);
         
         insert accList;
        
        //PRM_DEALREG.insertBypassAction();  
        
        List<Opportunity> OppListToInsert = new List<Opportunity>();
        
        for(Integer i = 0 ; i < 10 ; i++){
            
            Opportunity Oppty = new Opportunity();
            Oppty.Name = 'TestOpp' + i;
            Oppty.AccountId = accList[0].Id;
            Oppty.Sales_Channel__c = 'Direct';
            Oppty.Sales_Force__c = 'EMC';
            Oppty.StageName = 'Pipeline';
            Oppty.Closed_Reason__c = 'Loss';
            Oppty.Close_Comments__c = 'Lost';
            Oppty.CloseDate =  system.today() + 90;
            Oppty.Sell_Relationship__c = 'Direct';
            Oppty.Quote_Version__c='v13';
            Oppty.Quote_Type__c='New';
            Oppty.CurrencyIsoCode = 'USD';
            Oppty.Approval_Date__c= system.today() ;
            Oppty.Expiration_Date__c = system.today() + 30;
            Oppty.Primary_Outsourcer_System_Integrator__c=accList[0].Id; // Amit: Changes the index as 0 from 1,because the index was out of bound 
            Oppty.Primary_ISV_Infrastructure__c=accList[0].Id; // Amit: Changes the index as 0 from 1,because the index was out of bound
            Oppty.bypass_validation__c = true;
            
            OppListToInsert.add(Oppty);
        }
        
        insert OppListToInsert;
        
        Id approvedDealregTypeId = recordTypesDetails.get('Deal Registration - Approved').getRecordTypeId();
                    
        List<Lead> lstLead = LeadTestClassDataHelper.createDealReg(accList[0], null, accPartnerList[0], accPartnerList[1]);
        
        for(Lead lead : lstLead){
            
            lead.RecordTypeId = approvedDealregTypeId;
            lead.Related_Opportunity__c = OppListToInsert[0].Id;
            lead.Related_Account__c = accList[0].Id;
            lead.DealReg_Deal_Registration__c = true;
            lead.Sales_Force__c = 'EMC';
        }
        insert lstLead;
        
        for(Opportunity opp : OppListToInsert){
            opp.Related_Deal_Registration__c = lstLead[0].Id;
        }
        
        Product2 testProduct = new Product2();
        testProduct.Name = 'EMC Product';
        testProduct.CurrencyIsoCode ='USD';
        testProduct.Business_Unit__c = 'CMA';
        
        insert testProduct;
        
        Partner_Product_Catalog__c objPPCatalog = new Partner_Product_Catalog__c();
        objPPCatalog.Name = testProduct.Name;
        objPPCatalog.Product__c = testProduct.Id;
        insert objPPCatalog;
        
        List<Registration_Product__c> regProdList = new List<Registration_Product__c>();
        
        regProdList.add(new Registration_Product__c(Deal_Registration__c = lstLead[0].Id, Partner_Product_Catalog__c = objPPCatalog.Id));
        regProdList.add(new Registration_Product__c(Deal_Registration__c = lstLead[0].Id, Partner_Product_Catalog__c = objPPCatalog.Id));
        
        insert regProdList;
        
        
        
        Map<Id, Opportunity> idOppMap = new Map<Id, Opportunity>();
        Map<String, Opportunity> strOppMap = new Map<String, Opportunity>();
        
        for(Opportunity o : OppListToInsert){
            
            strOppMap.put(o.Related_Deal_Registration__c, o);
            idOppMap.put(o.Id, o);
        }
              
        List<AccountTeamMember> lstATM = new List<AccountTeamMember>();
        AccountTeamMember objATM = new AccountTeamMember();
        objATM.AccountId =  accList[0].Id;
        objATM.TeamMemberRole ='Enterprise Sales Rep';
        objATM.UserId = insertUser.Id;
        lstATM.add(objATM);
       
        AccountTeamMember objATM1 = new AccountTeamMember();
        objATM1.AccountId =  accList[0].Id;
        objATM1.TeamMemberRole ='Enterprise Sales Rep';
        objATM1.UserId = insertUser.Id;
        lstATM.add(objATM1);        
        insert lstATM;
        
        //Updated for CI 936
        Map<String,CustomSettingDataValueMap__c> DataValueMap = CustomSettingDataValueMap__c.getAll();
        CustomSettingDataValueMap__c DealregDirectRepRoleOrder = DataValueMap.get('DealregDirectRepRoleOrder');
        CustomSettingDataValueMap__c DealregDirectRepRoleOrder1 = DataValueMap.get('DealregDirectRepRoleOrder1'); 
        CustomSettingDataValueMap__c DealregDirectRepRoleOrder2 =DataValueMap.get('DealregDirectRepRoleOrder2');      
        String setOfRoles = DealregDirectRepRoleOrder.DataValue__c+';'+DealregDirectRepRoleOrder1.DataValue__c+';'+DealregDirectRepRoleOrder2.DataValue__c;
        System.debug('setOfRoles--->'+setOfRoles);
        // Code update ends for CI WR#936
        
        test.startTest();
       
        EMC_ConvertLead.convertLeads(lstLead);
       
        PRM_DEALREG_RegistrationConversion.updateSalesforceStatusOfLead(lstLead);
        
        PRM_DEALREG_RegistrationConversion.getOpportunityOwner(lstLead);
        
        PRM_DEALREG_RegistrationConversion.addProductOnOpportunity(strOppMap);
        
        PRM_DEALREG_RegistrationConversion.syncDealRelatedOpportunity(lstLead);
        
        PRM_DEALREG_RegistrationConversion.validateRelatedOpportunity(lstLead);
        
        PRM_DEALREG_RegistrationConversion.updateDealStatusFromRelatedOpportunity(OppListToInsert);
        
        PRM_DEALREG_RegistrationConversion.populateProductDetailsOnOpportunity(idOppMap);
        
        Test.stopTest();
    }
    public static testmethod void myUnitTest2(){
            
    System.runAs(new user(Id = UserInfo.getUserId())){        
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.adminConversionCSData();
            CustomSettingDataHelper.houseAccountCSData();
        }  
    List<Account> accList = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        insert accList;
        
    Opportunity Oppty = new Opportunity();
            Oppty.Name = 'TestOpp' ;
            Oppty.AccountId = accList[0].Id;
            Oppty.Sales_Channel__c = 'Direct';
            Oppty.Sales_Force__c = 'EMC';
            Oppty.StageName = 'Pipeline';
            Oppty.Closed_Reason__c = 'Loss';
            Oppty.Close_Comments__c = 'Lost';
            Oppty.CloseDate =  system.today() + 90;
            Oppty.Sell_Relationship__c = 'Direct';
            Oppty.Quote_Version__c='v13';
            Oppty.Quote_Type__c='New';
            Oppty.CurrencyIsoCode = 'USD';
            Oppty.Approval_Date__c= system.today() ;
            Oppty.Expiration_Date__c = system.today() + 30;
            Oppty.Primary_Outsourcer_System_Integrator__c=accList[1].Id;
            Oppty.Primary_ISV_Infrastructure__c=accList[2].Id;
            Oppty.bypass_validation__c = true;
    insert Oppty;
    
    Product2 prod = new Product2(Name = 'Test Product',CurrencyIsoCode = 'USD',productCode = 'ABC', IsActive = true);
        insert prod;
        
        String standardPriceBookId = '01s70000000EkOZ';
        PricebookEntry pricebook = new PricebookEntry(Pricebook2Id = standardPriceBookId, Product2Id = prod.Id, UnitPrice = 10000,IsActive = true);
        insert pricebook; 
        
        List<OpportunityLineItem> opptyLineItemList= new List<OpportunityLineItem>();
        
        OpportunityLineItem opptyLineItem = new OpportunityLineItem();
        opptyLineItem.OpportunityId=Oppty.Id;
        opptyLineItem.Quantity=5;
        opptyLineItem.Quote_Amount__c=5000;
        opptyLineItem.PricebookEntryId=pricebook.Id;
        opptyLineItemList.add(opptyLineItem);
        
        test.startTest();
        insert opptyLineItemList; 
        Test.stopTest();    
    }
            
    public static testmethod void deployEMC_DealReg(){
      
        //User  standardUser;   
        User insertUser = [Select Id, Name From User u where UserRole.Name = 'Worldwide EMC Corporate' AND Profile.Name = 'System Administrator' AND IsActive = true limit 1];
        
        System.runAs(new user(Id = UserInfo.getUserId())){
            
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.profilesCSData();
        }  
        
        List<Account> accPartnerList = AccountAndProfileTestClassDataHelper.CreatePartnerAccount();       
        insert accPartnerList;
       
        Contact cont = UserProfileTestClassDataHelper.createContact();
        cont.AccountId=accPartnerList[0].Id;
        insert cont;       
   
        //List<Account> accList = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        
        //Amit: Creation of Partner Accounts explicitly to overcome "Duplicate" error
                
         List<Account> accList = new List<Account>();
         
         Account objAccount = new Account();
         objAccount.name = 'TestPartnerAcc';
         objAccount.Party_Number__c = '232323';            
         objAccount.BillingCountry ='Colombia';
         objAccount.Synergy_Account_Number__c = '10';
         objAccount.Type = 'Partner';
         objAccount.Lead_Oppty_Enabled__c = true;
         objAccount.Partner_Type__c ='Distributor';
         objAccount.Site_DUNS_Entity__c='001017771';
         objAccount.PROFILED_ACCOUNT_FLAG__c = true;
         accList.add(objAccount);
         
         insert accList;
              
        List<AccountTeamMember> lstATM = new List<AccountTeamMember>();
        AccountTeamMember objATM = new AccountTeamMember();
        objATM.AccountId =  accList[0].Id;
        objATM.TeamMemberRole ='Enterprise Sales Rep';
        objATM.UserId = insertUser.Id;
        lstATM.add(objATM);
       
        AccountTeamMember objATM1 = new AccountTeamMember();
        objATM1.AccountId =  accList[0].Id;
        objATM1.TeamMemberRole ='Enterprise Sales Rep';
        objATM1.UserId = insertUser.Id;
        lstATM.add(objATM1);
       
        insert lstATM; 
       
        Id approvedDealregTypeId = recordTypesDetails.get('Deal Registration - Approved').getRecordTypeId();
   
        List<Lead> lstLead = LeadTestClassDataHelper.createDealReg(accList[0], null, accPartnerList[0], accPartnerList[1]);
            
        for(Lead lead : lstLead){
            
            lead.RecordTypeId = approvedDealregTypeId;
        }
        insert lstLead;
        
        Test.startTest();
        
        EMC_ConvertLead.convertLeads(lstLead);
        
        Test.stopTest();
 
    }
}