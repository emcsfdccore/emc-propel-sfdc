/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  13-APR-2015    Akash Rastogi    1810        Added a test method for covering code changes
 +=========================================================================================================================*/


@isTest
private class TASyncLogCreateController_TC {

    static testMethod void myUnitTest() {
      Pagereference pgf = Page.TASyncLogCreate;
      Test.setCurrentPage(pgf);
      
      User u = [select id from user where isactive=true limit 1];
      
      TASyncLogCreateController taCon = new TASyncLogCreateController(new Apexpages.Standardcontroller(new TA_Sync_Log__c()));
      taCon.talog.Sales_Resource__c = u.id;
      taCon.save();
     
      TASyncLogCreateController.SendTASyncLimitNotification();
    }
    
    // WR#1810 Started
    static testMethod void ValidateTASyncLogCreation() {
        TA_Sync_Log__c taRequest = new TA_Sync_Log__c();
        
            Id permissionSetId;
            List<PermissionSet> lstPS = [select name from PermissionSet where name = :System.Label.TA_Sync_Log_Permission_Set_Name limit 1];
            if(lstPS != null && lstPS.size() > 0)
            permissionSetId = lstPS.get(0).id;
            
            List<PermissionSetAssignment> lstPermissionSetAssignments = [Select p.SystemModstamp, p.PermissionSetId,p.Id, p.AssigneeId From PermissionSetAssignment p where permissionsetid = :permissionSetId  limit 1];
            
            User insertUser = [Select id from User where id = :lstPermissionSetAssignments[0].AssigneeId];       
            System.runAs(insertUser){
                User u = [select id from user where isactive=true and Profile.Name like '%AMER Enterprise%' limit 1];
                Test.startTest();
                CustomSettingDataHelper.dataValueMapCSData();
                taRequest.Sales_Resource__c = u.id ;
                taRequest.Status__c='Open';
                insert taRequest;
                
                taRequest.Status__c='Error';
                taRequest.Error__c=System.Label.TA_Sync_No_Access_Records;
                update taRequest;
                try {
                    TA_Sync_Log__c taRequest1 = new TA_Sync_Log__c();
                    taRequest1.Sales_Resource__c = u.id ;
                    taRequest1.Status__c='Open';
                    insert taRequest1;
                } catch(Exception e) {
                    system.debug('#### Exception occurred in Test Class : TASyncLogCreateController_TC');
                }
            }
        
              
    }
     //WR#1810 Ends
}