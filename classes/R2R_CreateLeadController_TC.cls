/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ======      =========== 
 |  06 Dec 2013      Sneha Jain                 Optimized the test class according to coding standards, removed seeAllData
 +=========================================================================================================================*/

@isTest
public Class R2R_CreateLeadController_TC
{

    public static testmethod void testsLeadController()
    { 
        //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId()))
        {
            CustomSettingDataHelper.dataValueMapCSData();
            CustomSettingDataHelper.bypassLogicCSData();
            CustomSettingDataHelper.eBizSFDCIntCSData();
            CustomSettingDataHelper.profilesCSData();
            CustomSettingDataHelper.countryTheaterMapCSData();
            CustomSettingDataHelper.dealRegistrationCSData();
        } 
        
        //Creating Account
        List<Account> lstAccountToInsert = new list<Account>();
        //Creating Customer profiled account
        Account acc= R2R_datahelper.createAccount();
        lstAccountToInsert.add(acc);

        Account isacc= new Account();
        isacc.Name='Testacc';
        isacc.Customer_Profiled_Account__c=false;
        isacc.Customer_Profiled_Account_Lookup__c=acc.id;
        lstAccountToInsert.add(isacc);

        Account isacc1= new Account();
        isacc1.Name='Testacc12';
        isacc1.Customer_Profiled_Account__c=false;
        isacc1.Customer_Profiled_Account_Lookup__c=isacc.id;
        lstAccountToInsert.add(isacc1);
        
        Account isacc2= new Account();
        isacc2.Name='Testacc34';
        isacc2.Customer_Profiled_Account__c=false;
        isacc2.Customer_Profiled_Account_Lookup__c=isacc.id;
        lstAccountToInsert.add(isacc2);
        
        insert lstAccountToInsert;
        
        //Creating account groupings
        Account_Groupings__c isgd=new Account_Groupings__c();
        isgd.Account__c='Test Account';
        isGd.Profiled_Account__c = isacc.id;
        insert isgd;

        //Creating Lead
        List<Lead> lstLeadToInsert = new list<Lead>();
        Lead lead1= new Lead();
        lead1.Company='TestLead';
        lead1.LastName='Test123';
        lead1.Status='Swap';
        lead1.Sales_Force__c='EMC';
        lead1.Lead_Originator__c='Customer Intelligence';
        lstLeadToInsert.add(lead1);

        Lead lead2= new Lead();
        lead2.Company='TestLead1';
        lead2.LastName='Test1231';
        lead2.Status='Refresh';
        lead2.Sales_Force__c='EMC';
        lead2.Lead_Originator__c='Customer Intelligence';
        lstLeadToInsert.add(lead2);

        insert lstLeadToInsert;
        
        //Querying the record type for Asset
        List<RecordType> recordType= [select id,DeveloperName,Name from RecordType where Name like 'EMC_Install' or Name Like 'Competitive_Install'];
        
        //Creating asset
        List<Asset__c> listSelectedAssets=new List<Asset__c>();
        List<Asset__c> lstSelectedAssetsforCreateLead= new List<Asset__c>();
        
        //Creating EMC Install Asset
        Asset__c asset1= new Asset__c();
        asset1.Name='TestassetComp';
        asset1.Customer_Name__c=isacc.id;
        if(recordType[0].Name == 'EMC_Install')
        {
            asset1.RecordTypeId=recordType[0].id;
        }
        else
        {
            asset1.RecordTypeId=recordType[1].id;
        }
        asset1.Product_Name_Vendor__c='Dell';
        listSelectedAssets.add(asset1);
        
        //Creating Competitive Install Asset
        Asset__c asset2= new Asset__c();
        asset2.Name='TestassetEMC';
        asset2.Customer_Name__c=acc.id;
        if(recordType[0].Name == 'Competitive_Install')
        {
            asset2.RecordTypeId=recordType[0].id;
        }
        else
        {
            asset2.RecordTypeId=recordType[1].id;
        }
        asset2.Product_Name_Vendor__c='EMC';
        listSelectedAssets.add(asset2);

        Asset__c asset3= new Asset__c();
        asset3.Name='TestassetEMC2';
        asset3.Customer_Name__c=acc.id;
        asset3.RecordTypeId=recordType[0].id;
        asset3.Product_Name_Vendor__c='EMC';
        listSelectedAssets.add(asset3);
        lstSelectedAssetsforCreateLead.add(asset3);
        
        Asset__c asset4= new Asset__c();
        asset4.Name='TestassetEMC3';
        asset4.Customer_Name__c=lstAccountToInsert[1].id;
        asset4.RecordTypeId=recordType[0].id;
        asset4.Product_Name_Vendor__c='EMC';
        listSelectedAssets.add(asset4);
        lstSelectedAssetsforCreateLead.add(asset4);
        
        Asset__c asset5= new Asset__c();
        asset5.Name='TestassetEMC4';
        asset5.Customer_Name__c=lstAccountToInsert[1].id;
        asset5.RecordTypeId=recordType[0].id;
        asset5.Product_Name_Vendor__c='EMC';
        listSelectedAssets.add(asset5);
        lstSelectedAssetsforCreateLead.add(asset5);
        
        insert listSelectedAssets;

        //Creating Oppty
        List<Opportunity> listOpp=new List<Opportunity>();
        Opportunity opp=new Opportunity();
        opp.Name='Testoppty12';
        opp.AccountId = acc.Id;
        opp.Opportunity_Type__c='Swap';
        Date closeDate =  System.today()+5;
        opp.CloseDate=closeDate;
        opp.Sales_Channel__c='Direct';
        opp.Sales_Force__c='EMC';
        opp.StageName='Pipeline';

        Opportunity opp1=new Opportunity();
        opp1.Name='Testoppty1234';
        opp1.AccountId = acc.Id;
        opp1.Opportunity_Type__c='Refresh';
        Date closeDate1 =  System.today()+5;
        opp1.CloseDate=closeDate1;
        opp1.Sales_Channel__c='Direct';
        opp1.Sales_Force__c='EMC';
        opp1.StageName='Pipeline';

        listOpp.add(opp);
        listOpp.add(opp1);
        insert listOpp;
      
        //Create oppty asset junction records
        List<Opportunity_Asset_Junction__c> oajList = new List<Opportunity_Asset_Junction__c>();
        Opportunity_Asset_Junction__c Oaj= new Opportunity_Asset_Junction__c();
        Oaj.Related_Asset__c=listSelectedAssets[0].id;
        Oaj.Related_Opportunity__c=opp.id;
        Oaj.Related_Account__c=acc.id;
        oajList.add(Oaj);
        
        Opportunity_Asset_Junction__c Oaj1= new Opportunity_Asset_Junction__c();
        Oaj1.Related_Asset__c=listSelectedAssets[1].id;
        Oaj1.Related_Opportunity__c=opp1.id;
        Oaj1.Related_Account__c=acc.id;
        oajList.add(Oaj1);
        
        Opportunity_Asset_Junction__c Oaj2= new Opportunity_Asset_Junction__c();
        Oaj2.Related_Asset__c=listSelectedAssets[1].id;
        Oaj2.Related_Opportunity__c=opp.id;
        Oaj2.Related_Account__c=acc.id;
        oajList.add(Oaj2);
        
        try{
           insert oajList;
        }catch(Exception e){System.debug(+e);}


        //Creating Lead asset junction with lead status =Swap and EMC asset attached
        List<Lead_Asset_Junction__c> listlaj= new List<Lead_Asset_Junction__c>();
        
        Lead_Asset_Junction__c laj1= new Lead_Asset_Junction__c();
        laj1.Related_Asset__c=listSelectedAssets[0].id;
        laj1.Related_Lead__c=lstLeadToInsert[0].id;
        listlaj.add(laj1);

        //Creating Lead asset junction with Lead status= Refresh and COmp asset attached
        Lead_Asset_Junction__c laj2= new Lead_Asset_Junction__c();
        laj2.Related_Asset__c=listSelectedAssets[1].id;
        laj2.Related_Lead__c=lstLeadToInsert[1].id;
        listlaj.add(laj2);

        insert listlaj;

        //Creating StandardSetController Object
        Test.startTest();
            ApexPages.StandardSetController con = new ApexPages.StandardSetController(listSelectedAssets);
            con.SetSelected(listSelectedAssets);
            R2R_CreateLeadController obj=new R2R_CreateLeadController(con); 
            obj.CreateLead();
            ApexPages.StandardSetController setcontroller = new ApexPages.StandardSetController(lstSelectedAssetsforCreateLead);
            setcontroller.SetSelected(lstSelectedAssetsforCreateLead);
            R2R_CreateLeadController createLeadObj=new R2R_CreateLeadController(setcontroller);
            createLeadObj.intNoRec = 1;
            createLeadObj.intLinkToOpenLead = 1;
            createLeadObj.intDiffAccount = 1;
            createLeadObj.intLinkToOpenOpportunity = 1;
            createLeadObj.intNoAccDistrict = 1;
            createLeadObj.intErrored = 0;
            createLeadObj.lstCreateLead.add(lstLeadToInsert[0].Id); 
            string idListView = Apexpages.currentPage().getParameters().put('fcf','00000000000');
            string strAssetIds = 'test';
            createLeadObj.CreateLead();
        Test.stopTest();
    }
}