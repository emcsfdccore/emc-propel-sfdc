/*===========================================================================+
|  HISTORY                                                                   
| 
|  DATE       DEVELOPER       WR       DESCRIPTION                                
|
   ====       =========       ==       ===========  
|  18-Apr-2013  Prachi                 Created 
+===========================================================================*/ 
@isTest(SeeAllData=true)
private Class afterUpdatePartnerLocation_Trigger_TC{

    public static testmethod void afterUpdatePartnerLocation_Trigger(){
            Partner_Location__c obj= new Partner_Location__c();
            obj.Country__c='ALBANIA (AL)';
            obj.City__c='A';
            obj.Street__c='B';
            obj.Postal_Code__c='C';
            obj.State_Province__c='D';
            insert(obj);

            obj.Country__c='ARUBA (AW)';
            obj.City__c='E';
            obj.Street__c='P';
            obj.Postal_Code__c='G';
            obj.State_Province__c='H';
            update(obj);

            
        }  
    }