/*=========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req      DESCRIPTION                               
 |  ====            =========       ==          =========== 
 |  26.11.2013		Srikrishnadevaraya			This test class is modified to reduce the processing time
 +=========================================================================================================================*/
@isTest
public class TC_UserInactiveOARUpdate 
{
	static Opportunity_Assignment_Rule__c oppAssigRuleRecord = new Opportunity_Assignment_Rule__c();
    static Profile objProfile = [Select id,Name from Profile where name=:'System Administrator'];
    static User usrObj,tempUsr;
    //static User usrObj,tempUsr,updateUsr;
	
    static testMethod void UserInactiveOARInsert() {
    	//insert custom setting data required for the test class - starts here
    	CustomSettingDataHelper.dataValueMapCSData();
    	CustomSettingDataHelper.eBizSFDCIntCSData();
    	//insert custom setting data required for the test class - ends here
    	
    	List<Account> acc = AccountAndProfileTestClassDataHelper.CreateCustomerAccount();
        insert acc;
        
        Contact cont = UserProfileTestClassDataHelper.createContact();
        cont.AccountId=acc[0].id;
        cont.Email='test24@abc.com';
        
        User admintUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];
        System.runAs(admintUser){
        	insert cont;	
        }
        
        Map<String,CustomSettingDataValueMap__c>  data =  CustomSettingDataValueMap__c.getall();
        String distributorSuperUser = data.get('EMEA Distributor Super User').DataValue__c ;
        Profile amerUserProf = [select Id from Profile where Name=: distributorSuperUser];
        usrObj = UserProfileTestClassDataHelper.createPortalUser(amerUserProf.id,null,cont.Id);  
        usrObj.IsActive=true;
        insert usrObj;
        
        tempUsr = new User(Username='test1234'+Math.random()+'@emc.com.test', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', FirstName='Direct',
             			   LastName='Rep', ProfileId = objProfile.Id, email='john123Test@emc.com', Alias='test', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US',
             			   Forecast_Group__c='Direct', BU__c='NA', Employee_Number__c='9323782818', IsActive=true );
             
     	insert tempUsr;
     	
     	User runningUser = [Select id from User where isActive=true and profile.Name='System Administrator'  Order by createdDate asc limit 1];           
        System.runAs(runningUser)
        {
        
            set<id> setId=new set<Id>();
            setId.add(usrObj.id);
            //system.debug('test user'+userObj);
            
            //Insert OAR record
            oppAssigRuleRecord.Resource_Name__c = tempUsr.Id;
            oppAssigRuleRecord.Resource_Sales_Force__c='EMC';
            oppAssigRuleRecord.Active__c=true;
            oppAssigRuleRecord.Partner_Type__c='All Partners';
            oppAssigRuleRecord.Coverage_Model__c='A-Hunting';
            oppAssigRuleRecord.Customer_Segment__c='Enterprise Tier 1';
            oppAssigRuleRecord.Resource_Opportunity_Sales_Team_Role__c='Channel Ã¢â‚¬â€œ Execution Sales Rep';        
            insert oppAssigRuleRecord;
            
            String CRON_EXP = '0 0 */3 * * ?';
          
            List<CronTrigger> cronTriggerToAbort = [Select c.State, c.NextFireTime, c.Id, c.CronExpression From CronTrigger c where CronExpression =: CRON_EXP];
          
            //Iterating over cronTriggerToAbort list
            for(Integer i = 0 ; i < cronTriggerToAbort.size() ; i++){
                
                System.abortJob(cronTriggerToAbort[i].Id);
            }
            
            test.startTest();
            tempUsr.isActive = false;
            update tempUsr;
            OAR_DeactivationScheduler runSchd = new OAR_DeactivationScheduler();
            system.schedule('Test_OAR_DeactivationScheduler',CRON_EXP,runSchd);
            test.stopTest(); 
        }
        
    }    
}