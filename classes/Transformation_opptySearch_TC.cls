/*==================================================================================================================+

 |  HISTORY  |                                                                           

 |  DATE          DEVELOPER             WR        DESCRIPTION                               

 |  ====          =========             ==        =========== 

 |  28/10/2013    Srikrishnadevaraya SM             This Test class is used to test the functionality of Transformation Recovery Oppty.   
                      
 +==================================================================================================================**/
@isTest
 private class Transformation_opptySearch_TC{
 
 
public Static List<Account> acctLst = new List<Account>();
    public Static List<Opportunity> lstOpp= new List<Opportunity>();
    public Static List<Pricing_Requests__c> lstPricing = new List<Pricing_Requests__c>();
    
     static testMethod void testMethod1(){
        CustomSettingDataHelper.eBizSFDCIntCSData();
        CustomSettingDataHelper.bypassLogicCSData();
        CustomSettingDataHelper.profilesCSData();
        CustomSettingDataHelper.dataValueMapCSData();
        
        Test.startTest();
        Transformation_opptySearch_TC testObj=new Transformation_opptySearch_TC();
        Account acctObj = new Account();
        acctObj.Name = 'Pricing Testing';
        acctObj.Synergy_Account_Number__c = '123456';
        acctLst.add(acctObj);
        Database.insert(acctLst);
        System.debug('Account Created'); 
        acctLst = [Select ID,Synergy_Account_Number__c  from Account where Name =:acctLst[0].Name];
            
        List<User> lstUser = [Select id from user where IsActive = true and Theater__c in ('APJ')limit 4];
        for(Integer i=0;i<3;i++){ 
            Opportunity opp_Obj = new Opportunity();
            if(i==0){  
              opp_Obj.AccountId =acctLst[0].id;
              opp_Obj.Opportunity_Owner__c = lstUser[0].id;
              opp_Obj.Name = 'TEST';
              opp_Obj.Opportunity_Number__c='1';
              opp_Obj.StageName = 'TEST';
              opp_Obj.CloseDate = Date.today()+10;
              opp_Obj.Sales_Force__c = 'EMC';
              opp_Obj.Sales_Channel__c = 'Direct';
              opp_Obj.VMWare_Related__c = 'VMWare Backup';
              opp_Obj.Amount = 222.00;
            }
            
             if(i==1){  
              opp_Obj.AccountId =acctLst[0].id;
              opp_Obj.Opportunity_Owner__c = lstUser[1].id;
              opp_Obj.Name = 'TEST 1';
              opp_Obj.Opportunity_Number__c='2';
              opp_Obj.StageName = 'TEST 1';
              opp_Obj.CloseDate = Date.today()+10;
              opp_Obj.Sales_Force__c = 'EMC';
              opp_Obj.Sales_Channel__c = 'Direct';
              opp_Obj.VMWare_Related__c = 'VMWare Backup';
              opp_Obj.Amount = 222.00;
            }
            
            if(i==2){  
              opp_Obj.AccountId =acctLst[0].id;
              opp_Obj.Opportunity_Owner__c = lstUser[2].id;
              opp_Obj.Name = 'T';
              opp_Obj.Opportunity_Number__c='3';
              opp_Obj.StageName = 'TEST 2';
              opp_Obj.CloseDate = Date.today()+10;
              opp_Obj.Sales_Force__c = 'EMC';
              opp_Obj.Sales_Channel__c = 'Direct';
              opp_Obj.VMWare_Related__c = 'VMWare Backup';
              opp_Obj.Amount = 222.00;
            }       
          lstOpp.add(opp_Obj);
        }
        
        Database.insert(lstOpp);
        lstOpp =[select ID,Name,Opportunity_Owner__c ,Opportunity_Number__c, Account_Owner_Theater__c , Opportunity_Owner__r.Theater__c from Opportunity where id in:lstOpp];
        System.debug('lstOpp--->' + lstOpp.size() + lstOpp);
    
        List<Pricing_Requests__c> lstPric = new List<Pricing_Requests__c>();
        //Select id from Opportunity
        for(Integer i=0;i<1;i++){   
          Pricing_Requests__c priReq_Obj = new Pricing_Requests__c();
          priReq_Obj.Opportunity_Name__c = lstOpp[i].id;
          priReq_Obj.Price_Floor_Level__c = 'L4';
          priReq_Obj.How_can_we_mask_actual_discount__c = 'TEST 1234';
          priReq_Obj.Approval_Status__c ='New';
          priReq_Obj.EMC_PRODUCTS__c='test';
          lstPric.add(priReq_Obj);
        }
        Database.insert(lstPric);
     
        lstPricing =[select id,Approval_Status__c,createdById, Opportunity_Name__r.Opportunity_Owner__r.Theater__c,Opportunity_Name__r.Opportunity_Owner__c ,Opportunity_Name__r.Account_Owner_Theater__c from Pricing_Requests__c  where id in:lstPric];
        PageReference pageRef = Page.Transformation_PAR_OpptyLookup;
        pageRef.getParameters().put('lksrch',lstOpp[0].Name);
        Test.setCurrentPage(pageRef);   
        Opportunity o = new opportunity();
        Transformation_opptySearch i = new Transformation_opptySearch(new ApexPages.StandardController(o));
        i.search();
        i.getOpptyDetails();
        i.clearresults();
        i.cancelLink();  
        
        PageReference pageRef1 = Page.Transformation_PAR_OpptyLookup;
        pageRef1.getParameters().put('lksrch','');
        Test.setCurrentPage(pageRef1);   
        Opportunity o1 = new opportunity();
        Transformation_opptySearch i1 = new Transformation_opptySearch(new ApexPages.StandardController(o1));
        i1.inputOpptyNumber =lstOpp[0].Opportunity_Number__c;
        i1.search();
        
        PageReference pageRef2 = Page.Transformation_PAR_OpptyLookup;
        pageRef2.getParameters().put('lksrch',lstOpp[0].Name);
        Test.setCurrentPage(pageRef2);   
        Opportunity o2= new opportunity();
        Transformation_opptySearch i2 = new Transformation_opptySearch(new ApexPages.StandardController(o2));
        i2.inputOpptyNumber =lstOpp[1].Opportunity_Number__c;
        i2.search();   
        
        PageReference pageRef3 = Page.Transformation_PAR_OpptyLookup;
        pageRef3.getParameters().put('lksrch','');
        Test.setCurrentPage(pageRef3);   
        Opportunity o3= new opportunity();
        Transformation_opptySearch i3 = new Transformation_opptySearch(new ApexPages.StandardController(o3));
        i3.search();  
         
        User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
        System.runAs(insertUser)
        {
            PageReference pageRef5 = Page.Transformation_PAR_OpptyLookup;
            pageRef5.getParameters().put('lksrch',lstOpp[2].Name);
            Test.setCurrentPage(pageRef5);   
            Opportunity o5= new opportunity();
            Transformation_opptySearch i5 = new Transformation_opptySearch(new ApexPages.StandardController(o5));
            i5.inputOpptyNumber =lstOpp[2].Opportunity_Number__c;
            i5.search();         
            Test.stopTest();
        }
     }   
}