/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE        DEVELOPER     DESCRIPTION                               
 |  ====        =========     =========== 
 | 16.12.2011   Shipra Misra  SFA: TA Assignment Rule Functionality enhancements
 |                            This functionality fires whenever a new TA AssignmentRule 
 |                            is inserted or updated.When there exist any matching account for Country, state and Classification.
 |                            Also the functionality would fire for only when one �TA_Assignment_Rule� record 
 |                            is created or updated in single trigger execution. 
 |                            If more records are getting updated via DataLoader at same time, 
 |                            then it is advised to set batch size of data loader to 'one' record.This class is called from 'AfterInsertUpdateTA_AssignmentRule.trigger'.
 |06.02.2014    Naga          Removed TA assignment Rule Classification field   WR#339289
+===========================================================================*/
public class TA_AssignmentRuleAccountUpdate 
{
    //Added by Naga to get country and stae values from custom settings
    Set<String> ContryStateToSkip = new Set<String>();
    public TA_AssignmentRuleAccountUpdate(){
         Map<String, TO_EMC_Classification_To_Be_Skipped__c>  mapTO_EMC_Classification_To_Be_Skipped = TO_EMC_Classification_To_Be_Skipped__c.getAll();
         for(TO_EMC_Classification_To_Be_Skipped__c skipvalue : mapTO_EMC_Classification_To_Be_Skipped.values()){
           ContryStateToSkip.add((skipvalue.country__c+ skipvalue.State__c).toUpperCase());
         }
    }
    //method to update the associated account for inserted or updated TA Assignment Rule.
    
    public void UpdateAccountOnTARuleUpdate(TA_Assignment_Rule__c taRule)
    {
        List<Account> lstAcc= new List<Account> ();
        Set<id> accId= new Set<Id>();
        String strCountry=taRule.Country__c;
        String strState=taRule.State_Or_Province__c;
        if(strCountry=='United States' || strCountry=='Canada')
        {
            lstAcc = [Select id, TA_Assignment_Type__c From Account where TA_Assignment_Rule__c=:taRule.id OR( BillingCountry =:strCountry  AND BillingState =:strState) limit : limits.getLimitDmlRows()]; 
        }
        else
        {
            lstAcc = [Select id, TA_Assignment_Type__c  From Account where TA_Assignment_Rule__c=:taRule.id OR(BillingCountry =:strCountry) limit : limits.getLimitDmlRows()];
        }
        system.debug('ACCOUNT LIST ====>  '+lstAcc);
        if(lstAcc!=null && lstAcc.size()>0)
        {
            for(Account objAcc:lstAcc)
            {
                accId.add(objAcc.id);
            }
            // Calling this method to increase governer limit using future method. 
            TA_AssignmentRuleUpdateAccountOnFuturel.UpdateAccountOnTARuleUpdateFuture(accId);
        }
    }
    //Added by Naga to set Account TA Assignment type based on classification
    public void setTAAssignmentType(Account acc, String AssignmnetType ){
        String ConcatSkipValue;
        map<string,customsettingdatavaluemap__c> datavaluemap = customsettingdatavaluemap__c.getall();
        customsettingdatavaluemap__c strCountryState =datavaluemap.get('TA Country State Concatination');
        if(strCountryState!=null && strCountryState.datavalue__c!=null && strCountryState.datavalue__c.contains(acc.BillingCountry)){
            ConcatSkipValue=(acc.BillingCountry+acc.BillingState).toUpperCase();
        }else{
            ConcatSkipValue=(acc.BillingCountry).toUpperCase();
        }
        if(ContryStateToSkip.contains(ConcatSkipValue)){
                acc.TA_Assignment_Type__c='Exempt';
        }else{
                acc.TA_Assignment_Type__c=AssignmnetType;
        } 
        
    }
    
}