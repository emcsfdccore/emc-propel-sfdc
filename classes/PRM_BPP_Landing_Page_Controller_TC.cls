@isTest
public class PRM_BPP_Landing_Page_Controller_TC{

    public static testMethod void testPRM_BPP_Landing_Page_Controller(){

        //Creating Custom Setting data
        System.runAs(new user(Id = UserInfo.getUserId())){
            CustomSettingDataHelper.dataValueMapCSData();
        }   
        
        PageReference pageref =Page.PRM_BPP_Landing_Page;
        Test.setCurrentPage(pageref);
        List<Account> accList=new List<Account>();    
        
        for(Integer i=0 ; i<=1 ; i++){
        
            Account accObj = new Account();
            accObj.Name='Testing';
            accObj.DMR_Tier__c='Silver';
            if(i==0){
                accObj.Distributor_Tier__c='Value Accelerator';
            }   
            else{
                accObj.Velocity_Solution_Provider_Tier__c='Distributor';
            }
            accList.add(accObj);
        }
        
        //Insert account records
        insert accList;
        
        Test.startTest();
        pageref.getParameters().put('id',accList[0].id);
        PRM_BPP_Landing_Page_Controller bpp = new PRM_BPP_Landing_Page_Controller();
        bpp.displayScorecardLinks();
        bpp.getdistiLink() ;
        bpp.getdistiVAR();
        bpp.getsolProvider();
        pageref.getParameters().put('id',accList[1].id);
        PRM_BPP_Landing_Page_Controller bppObj = new PRM_BPP_Landing_Page_Controller();
        bppObj.displayScorecardLinks();
        Test.stopTest();
    }
}