/*==================================================================================================================+                                                                          
 |  DATE          DEVELOPER      WR        DESCRIPTION                               
 |  ====          =========      ==        ===========  
 |  March/11/2014    Leonard    CI 186     Updated code to add a space (" ") between products and commas in T3 list view 
 |  Nov/06/2014      Bisna V P  CI 1496    Adding the new T3 values for solutions & using custom setting record
 |  Dec/11/2014      Bisna V P  CI 1413    Fixing Article Metadata Quick Save -- List index out of bounds error   
+==================================================================================================================**/
public class KB_T3PickerScreenController  {
    public string articleId {get;set;}
    public List<T3_Categories__c> FamProdProdSub {get;set;}
    public List<T3_Categories__c> fppsSelects {get;set;}
    public List<T3_Categories__c> fppsSelects2 {get;set;}
    public List<T3_Categories__c> fppsSelects3 {get;set;}
    public String parentRecordId {get;set;}
     public String parentName {get;set;}
    public List<T3_Categories__c> childRecords {get;set;}
    public String allNames {get;set;}
    public String allDescriptions {get;set;}
    public String allProducts {get;set;}
    public List<Break_Fix__kav> primusArtList {get;set;}
    public List<How_To__kav> howtoArtList {get;set;}
    public List<Break_Fix__kav> breakArtList {get;set;}
    public List<ETA__kav> etaArtList {get;set;}
     public List<How_To__kav> howArt {get;set;}
    public List<TSA__kav> webtopArtList {get;set;}
    public string NoVers {get;set;}
    public List<currentArticleInfo> cAI {get;set;}
    public String articleType {get; set;}
    public String query{get; set;}
    public String description{get; set;}
    public String t3ID{get; set;}
     public String prod{get; set;}
    public Boolean noDraft {get; set;}
    public String sourceArtTypw = '';
    public transient Map<String,T3_Categories__c> mapAllName = new Map<String,T3_Categories__c>();
    //Fields For Showing Attached product on load
    public List<String> descLst = new List<String>();
    public List<String> t3idLst = new List<String>();
    public List<String> prodLst = new List<String>();
    public String displayedProduct {get; set;}    
    Set<String> setProd = new Set<String>();
    //Genric SObject

    //Req 186 added string variable for displaying category and product with spacing 

    public String displayCategory {get; set;}
    public String displayProduct {get; set;}

    //End of 186 Changes End

    public sObject sObj{get; set;}    
    public KB_T3PickerScreenController(){   
         query = 'Select ';
        howtoArtList  = new List<How_To__kav>();
        breakArtList = new List<Break_Fix__kav>();
        etaArtList = new List<ETA__kav>();
        NoVers = '';    
        parentRecordId = '';
        fppsSelects = new List<T3_Categories__c>();
        fppsSelects2 = new List<T3_Categories__c>();
        fppsSelects3 = new List<T3_Categories__c>();
        howArt = new List<How_To__kav>();
        articleId = ApexPages.currentpage().getParameters().get('aId');
        articleType = ApexPages.currentpage().getParameters().get('aType');
        sourceArtTypw = articleType;
        cAI = new List<currentArticleInfo>();
        articleType = articleType.replace(' ','_')+'__kav';
        system.debug('articleType--->'+articleType);
        sObj = Schema.getGlobalDescribe().get(articleType).newSObject() ;                
          Schema.DescribeSObjectResult dsor = sObj.getSObjectType().getDescribe();
                Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
                List<String> lstFields = new List<String>();
                
                    for(String field : objectFields.keyset()){
                            lstFields.add(field);
                        }
                        
                         Integer counter = lstFields.size();
                         for(String s :lstFields){
                                if(!s.startsWithIgnoreCase('Attachment')){
                                    if(counter==lstFields.size())
                                        query = query+' '+s;

                                    else
                                        query = query+','+s;
                                     }

                                    counter--;
                                    if(counter ==0)
                                        {
                                            query = query + ' from ' +articleType+ ' where PublishStatus=\'Draft\' and Language = \'en_US\' and id =\'' +articleId+'\'';
                                            
                                        }                              
                                }
                                System.debug('query------>'+query);        
                        try{
                         sObj = Database.query(query);
                        }
                        catch (Exception ex){
                            System.debug('ex-------->'+ex.getMessage());
                            noDraft = true;
                        }
                        description = (String)sObj.get('T3_Product__c');
                        //displayedProduct = description;
                        t3ID = (String)sObj.get('T3_Categories__c');
                        prod = (String)sObj.get('T3_Version__c');
            if(description!=null && t3ID!=null && prod!=null ){
            descLst = description.split(',');
      if(t3ID!='')
            t3idLst = t3ID.split(',');
            prodLst = prod.split(',');          
            Integer totalProd = t3idLst.size();
            prod = '';
            for (Integer i=0;i<totalProd ; i++ )
            {
                //prod = prod+t3idLst[i]+'*'+descLst[i]+'*'+prodLst[i] + ',';
                
                if(t3idLst.size()>i)
                prod = prod+t3idLst[i];
                if(descLst.size()>i)
                prod = prod+'*'+descLst[i];
                if(prodLst.size()>i)
                prod = prod+'*'+prodLst[i] ;
                
                prod= prod+ ',';
            
            }
            }
            else{
                prod = '';
                 if(t3ID!=null && t3ID!=''){              
                for(String str : t3ID.split(',')){
                    String t3String = str.trim();
                      System.debug('t3String---->'+t3String);
                    t3idLst.add(t3String);
                }
                }
            }
            displayedProduct= '';
            Map<String,String> mapAll = new Map<String,String>();
            //Map<String,List<String>> mapT3 = new Map<String,List<String>>();
            System.debug('limit--->'+Limits.getScriptStatements());
            List<T3_Categories__c> listCate  = new List<T3_Categories__c>();
            if(Test.isRunningTest())
            listCate = [Select id,Term_Type__c,Parent_T3_ID__c,name,T3_ID__c from T3_Categories__c where (T3_ID__c !=null) limit 5];
            else            
            listCate = [Select id,Term_Type__c,Parent_T3_ID__c,name,T3_ID__c from T3_Categories__c where (T3_ID__c !=null) ];
            for(T3_Categories__c s:listCate){
                mapAllName.put(s.T3_ID__c,s);
            }
            //Req 186 Changes
            displayCategory = '';
            //186 changes Ends here
              //if(t3idLst.size()<5){
                for(String s:t3idLst){
                //lstT3Id =[Select id,Term_Type__c,Parent_T3_ID__c,name,T3_ID__c from T3_Categories__c where T3_ID__c = :s limit 1];
                //if(lstT3Id.size()>0){
                    //Changes For 186
                    s=s.trim();
                    if(mapAllName.containsKey(s) && (mapAllName.get(s).Term_Type__c == 'Version' || mapAllName.get(s).Term_Type__c == 'Version Subgroup')){
                        displayedProduct = displayedProduct+getParent(s,mapAllName)+mapAllName.get(s).name+',';
                        system.debug('displayedProduct--->1'+displayedProduct);
                    }
                    else if(mapAllName.containsKey(s) && (mapAllName.get(s).Term_Type__c == 'Tool' || mapAllName.get(s).Term_Type__c == 'Family' || mapAllName.get(s).Term_Type__c == 'Product Subgroup'|| mapAllName.get(s).Term_Type__c == 'Product'|| mapAllName.get(s).Term_Type__c == 'Solution'|| mapAllName.get(s).Term_Type__c == 'Suite'|| mapAllName.get(s).Term_Type__c == 'Bundle'|| mapAllName.get(s).Term_Type__c == 'Service')){
                        displayedProduct = displayedProduct+' '+mapAllName.get(s).name+',';
                        system.debug('displayedProduct--->2'+displayedProduct);
                    }
                //}
                //Req 186 Changes
                      displayCategory = displayCategory  + s + ', ';
                //186 changes Ends here
                }
            //}
            //Req 186 Changes
            displayProduct = '';
            for(String product : descLst){
                displayProduct = displayProduct + product + ', ';
            }
            //End of 186 Changes
            //t3Name = description;
            //Req 186 Changes
                      displayCategory = displayCategory.removeend(', ');
                      displayProduct = displayProduct.removeend(', ');
            //186 changes Ends here

            if(displayedProduct !=null)
            displayedProduct = displayedProduct.removeend(', ');
            if(description==null || prod==''){
                   sObj.put('T3_Product__c',displayedProduct);
                   description = displayedProduct;
                    prod = '';
                    descLst.clear();
                    t3idLst.clear();
                    prodLst.clear();
                    if(description!=null)
                    descLst = description.split(',');
                    if(t3ID!=null)
                     t3idLst = t3ID.split(',');
                    if(prod!=null)
                     prodLst = prod.split(',');
                  Integer totalProd = t3idLst.size();                    
                     for (Integer i=0;i<totalProd && i<t3idLst.size() && i<descLst.size(); i++ )
                     {
                        if(!prodLst.isEmpty())
                        {
                            //prod = prod+t3idLst[i]+'*'+descLst[i]+'*'+i + ',';
                //1413 changes
                            if(t3idLst.size()>i)
                            prod = prod+t3idLst[i];
                            if(descLst.size()>i)
                            prod = prod+'*'+descLst[i];
                            prod= prod+ ',';
                        }                      
                      else{
                          //prod = prod+t3idLst[i]+'*'+descLst[i]+'*'+prodLst[i] + ',';
                          if(t3idLst.size()>i)
                            prod = prod+t3idLst[i];
                            if(descLst.size()>i)
                            prod = prod+'*'+descLst[i];
                            if(prodLst.size()>i)
                            prod = prod+'*'+prodLst[i] ;
                            
                            prod= prod+ ',';
              //1413 changes ends here
                      }
                 }            
            }
            system.debug('displayedProduct--->3'+displayedProduct);
                            
           /* List<T3_Categories__c> lstT3ids1 =[Select id,Term_Type__c,Parent_T3_ID__c,name,T3_ID__c from T3_Categories__c where T3_ID__c in :t3idLst];        
            for(T3_Categories__c obj1 :lstT3ids1){
                    
                    if(mapFirstT3.containskey(obj1.Parent_T3_ID__c)){
                        mapFirstT3.get(obj1.Parent_T3_ID__c).add(obj1);
                    }
                    else{
                        List<T3_Categories__c> lstT3= new List<T3_Categories__c>();
                        lstT3.add(obj1);
                        mapFirstT3.put(obj1.Parent_T3_ID__c , lstT3);
                    }                                       
            }
            List<T3_Categories__c> lstT3ids2 =  [Select id,Term_Type__c,Parent_T3_ID__c,name,T3_ID__c from T3_Categories__c where T3_ID__c in :mapFirstT3.keyset()];
            Set<String> setParent = new Set<String>();
                for(T3_Categories__c obj2 :lstT3ids2){                    
                    mapSecondT3.put(obj2.T3_ID__c , obj2);
                    setParent.add(obj2.Parent_T3_ID__c);                    
            }
            List<T3_Categories__c> lstT3ids3 =  [Select id,Term_Type__c,Parent_T3_ID__c,name,T3_ID__c from T3_Categories__c where T3_ID__c in :setParent];
            for(T3_Categories__c obj3 :lstT3ids3){                    
                    mapThirdT3.put(obj3.T3_ID__c , obj3);                   
            }
            for(String keyParent : mapFirstT3.keyset()){

            for(T3_Categories__c t3Obj : mapFirstT3.get(keyParent)){

                    if(t3Obj.Term_Type__c=='Product'){
                            displayedProduct = displayedProduct+t3Obj.name+',';
                    }
                    else{
                        if(mapSecondT3.get(t3Obj.Parent_T3_ID__c).Term_Type__c=='Product'){

                            displayedProduct = displayedProduct+ mapSecondT3.get(t3Obj.Parent_T3_ID__c).name+' '+t3Obj.name +',';

                        }
                        else{
                            displayedProduct = displayedProduct +  mapThirdT3.get(mapSecondT3.get(t3Obj.Parent_T3_ID__c).Parent_T3_ID__c).name +' ' + t3Obj.name +',';
                        }                        
                    }
                System.debug('displayedProduct---->'+displayedProduct);
            }
            }
            displayedProduct = displayedProduct.removeend(',');

            */ 
        
        //1496 Adding the new T3 values for solutions & using custom setting record
        Map<String,CustomSettingDataValueMap__c> termTypes = CustomSettingDataValueMap__c.getAll();
        String strlistTermType = termTypes.get('KbT3PickerScreenTermTypeT3categories').dataValue__c;
        List<String> listTermType = strlistTermType.split(',');
        FamProdProdSub = [Select t.Term_Type__c, t.SystemModstamp, t.Parent_T3__c,t.Parent_T3_ID__c,t.T3_ID__c, t.OwnerId, 
                                  t.Name, t.LastModifiedDate, t.LastModifiedById, t.IsDeleted, t.Id, 
                                 t.CreatedDate, t.CreatedById From T3_Categories__c t Where t.Term_Type__c in :listTermType]; 
        // 1496 changes ends here
        for(T3_Categories__c t: FamProdProdSub ){
            if(fppsSelects.size() < 1000){
                fppsSelects.add(t);   
            }
            else  if(fppsSelects2.size() < 1000) {
                fppsSelects2.add(t);
            } else {
            fppsSelects3.add(t);
            }
        } 
        if(fppsSelects.size()>0)
        {
            fppsSelects.sort();
        }
       if(fppsSelects2.size()>0){
                fppsSelects2.sort();
        }
         if(fppsSelects3.size()>0){
                fppsSelects3.sort();
        }

    }
    
    public class currentArticleInfo{
        public string curEmcId {get;set;}
        public string curTitle {get;set;}
        public string curNumber {get;set;}        
        public currentArticleInfo(string cEmc, string cTitle, string cNum){
            curEmcId = cEmc;
            curTitle = cTitle;
            curNumber = cNum;
        } 
    }    
    public void showChildren(){       
        Set<String> partentSet = new Set<String>();
        List<T3_Categories__c> lstT3 = new List<T3_Categories__c>();
        childRecords = new List<T3_Categories__c>();
        Boolean endofWhile = false;
        partentSet.add(parentRecordId);      
        while(!endofWhile){
             lstT3 = [select id, name,T3_ID__c  from T3_Categories__c where Parent_T3_ID__c in : partentSet AND Term_Type__c = 'Version' order by name];

             if(lstT3.size()>0){
                 childRecords.addall(lstT3);
                 partentSet.clear();
                 for(T3_Categories__c t3catObj : lstT3){                    
                    partentSet.add(t3catObj.T3_ID__c);
                }                    
             }
             else{
                 endofWhile = true;
             }
             lstT3.clear();             
        }                
        /*
        for(T3_Categories__c t3catObj : lstT3){
            partentSet.add(t3catObj.T3_ID__c);
            //t3catObj.name = parentName+' '+t3catObj.name;
            childRecords.add(t3catObj);
        }
        lstT3.clear();
        lstT3 = [select id, name,T3_ID__c  from T3_Categories__c where Parent_T3_ID__c in : partentSet AND Term_Type__c = 'Version' order by name ];
        for(T3_Categories__c t3Child : lstT3){
        //  t3Child.name = parentName+' '+t3Child.name;
            childRecords.add(t3Child);
        } */
        if(childRecords.size() == 0){
            NoVers = 'No Versions Available';
        }
        else{
            NoVers = '';
        }
    }   
    public pageReference updateArticle(){
               String descriptionT3 = '';
           String names = '';
           String product = '';
            allDescriptions =  allDescriptions.trim();                        
         for(String descs :allDescriptions.split(',')){           
             descs = descs.trim();
                    if(descs.length()>0 &&!descriptionT3.containsIgnoreCase(descs)){                        
                         descriptionT3 = descriptionT3+descs+',';                        
                    }
         }
         descriptionT3 = descriptionT3.removeEnd(',');
          System.debug('descriptionT3--->'+descriptionT3);        
          for(String nam :allNames.split(',')){
             nam = nam.trim();
                    if(nam.length()>0 && !names.containsIgnoreCase(nam)){
                        names = names+nam+',';                                                
                    }
         }
          names = names.removeEnd(',');
          for(String ver :allProducts.split(',')){
             ver = ver.trim();
                    if(ver.length()>0 ){                       
                        product = product+ver+',';                                                
                    }
         }
         product = product.removeEnd(',');
         System.debug('product--->'+product);
         sObj.put('T3_Categories__c',names);
         sObj.put('T3_Product__c',descriptionT3);
         sObj.put('T3_Version__c',product);
         update sObj;
        return new pageReference('/apex/KB_T3_Assign');
    } 
    
     public pageReference quickSaveArticle(){
           String descriptionT3 = '';
           String names = '';
           String product = '';
            allDescriptions =  allDescriptions.trim();
             System.debug('allDescriptions--->'+allDescriptions);
        Set<String> descSet = new Set<String>();            
         for(String descs :allDescriptions.split(',')){            
             descs = descs.trim();
              System.debug('descs--->'+descs);
              System.debug('descriptionT3--->'+descriptionT3);
                    if(descs.length()>0 &&!descSet.contains(descs)){
                        descSet.add(descs);
                         descriptionT3 = descriptionT3+descs+', ';                        
                    }                    
         }                               
         descriptionT3 = descriptionT3.removeEnd(', ');                 
          for(String nam :allNames.split(',')){
             nam = nam.trim();
                    if(nam.length()>0 && !names.containsIgnoreCase(nam)){
                        names = names+nam+', ';                                               
                    }
         }
          names = names.removeEnd(', ');
         System.debug('names--->'+names);
        system.debug('allProducts--->'+allProducts);
          for(String ver :allProducts.split(',')){
             ver = ver.trim();
                    if(ver.length()>0 ){                      
                        product = product+ver+', ';                                                
                    }
         }
         product = product.removeEnd(', ');         
         sObj.put('T3_Categories__c',names);
         sObj.put('T3_Product__c',descriptionT3);
         sObj.put('T3_Version__c',product);
         update sObj;
            descLst.clear();
            t3idLst.clear();
            prodLst.clear();
            system.debug('names--->'+names);
            descLst = descriptionT3.split(',');
            if(names!='')
            t3idLst = names.split(',');
            system.debug('t3idLst--->'+t3idLst);
            prodLst = product.split(',');            
            Integer totalProd = t3idLst.size();           
            prod = '';
            for (Integer i=0;i<totalProd ; i++ )
            {
                
                //prod = prod+t3idLst[i]+'*'+descLst[i]+'*'+prodLst[i] + ',';
                //1413 changes
                if(t3idLst.size()>i)
                prod = prod+t3idLst[i];
                if(descLst.size()>i)
                prod = prod+'*'+descLst[i];
                if(prodLst.size()>i)
                prod = prod+'*'+prodLst[i] ;
                
                prod= prod+ ',';
                //1413 changes ends here
                
            }
            displayedProduct= '';
            /*for(Integer i=0;i<11 && i<t3idLst.size();i++){
                t3idLst1.add(t3idLst[i]);   
            }
            Map<String,List<T3_Categories__c>> mapFirstT3 = new Map<String,List<T3_Categories__c>>();
            Map<String,T3_Categories__c> mapSecondT3 = new Map<String,T3_Categories__c>();
            Map<String,T3_Categories__c> mapThirdT3 = new Map<String,T3_Categories__c>();
            List<T3_Categories__c> lstT3Id = new List<T3_Categories__c>();*/
            transient Map<String,T3_Categories__c> mapAllName1 = new Map<String,T3_Categories__c>();
            System.debug('limit--->'+Limits.getScriptStatements());
            List<T3_Categories__c> listCate1 = new List<T3_Categories__c>();
            if(Test.isRunningTest())            
            listCate1 = [Select id,Term_Type__c,Parent_T3_ID__c,name,T3_ID__c from T3_Categories__c where (T3_ID__c !=null)  limit 5];
            else            
            listCate1 = [Select id,Term_Type__c,Parent_T3_ID__c,name,T3_ID__c from T3_Categories__c where (T3_ID__c !=null) ];
            for(T3_Categories__c s: listCate1){//AND Term_Type__c in('Version','Version Subgroup','Tool','Family','Product Subgroup','Product')
                mapAllName1.put(s.T3_ID__c,s);
            }
            //Req 186 Changes
            displayCategory = '';
            //186 changes Ends here
            if(t3idLst.size()>0){
                for(String s:t3idLst){
                      s=s.trim();
                      if(mapAllName1.containsKey(s) && (mapAllName1.get(s).Term_Type__c == 'Version' || mapAllName1.get(s).Term_Type__c == 'Version Subgroup')){
                        displayedProduct = displayedProduct+getParent(s,mapAllName1)+mapAllName1.get(s).name+', ';                       
                    }
                    else if(mapAllName1.containsKey(s) && (mapAllName1.get(s).Term_Type__c == 'Tool' || mapAllName1.get(s).Term_Type__c == 'Family' || mapAllName1.get(s).Term_Type__c == 'Product Subgroup'
                    || mapAllName1.get(s).Term_Type__c == 'Collection'
                    || mapAllName1.get(s).Term_Type__c == 'Product'
                    || mapAllName1.get(s).Term_Type__c == 'Solution'
                    || mapAllName1.get(s).Term_Type__c == 'Suite'
                    || mapAllName1.get(s).Term_Type__c == 'Bundle'
                    || mapAllName1.get(s).Term_Type__c == 'Service')){
                        displayedProduct = displayedProduct+' '+mapAllName1.get(s).name+', ';
                    }
                    //Req 186 Changes
                      displayCategory = displayCategory  + s + ', ';
                    //186 changes Ends here
                }
            }
            //Req 186 Changes
            displayProduct = '';
            for(String disProd : descLst){
                displayProduct = displayProduct + disProd + ', ';
            }
            //t3Name = description;
            //Req 186 Changes
                      displayCategory = displayCategory.removeend(', ');
                      displayProduct = displayProduct.removeend(', ');
            //186 changes Ends here
            if(displayedProduct !=null)
            displayedProduct = displayedProduct.removeend(', ');
        String url = '/apex/KB_T3PickerScreen?aId='+articleId+'&aType='+sourceArtTypw;
         PageReference pg = new PageReference(url);
        return pg;       
    } 
    public String getParent(String s,Map<String,T3_Categories__c> mapAllT3){
            //List<T3_Categories__c> lstT3ids1 = new List<T3_Categories__c>();
            String parent = '';
            Boolean isEnd = false;
            String s1 = '';
           while(!isEnd){
                    if(mapAllT3.containsKey(s))
                        s1 = mapAllT3.get(s).Parent_T3_ID__c;
                //lstT3ids1 =[Select id,Term_Type__c,Parent_T3_ID__c,name,T3_ID__c from T3_Categories__c where T3_ID__c = :s];
                if(s1!=null && s1!='' && S1!='0'){
                    //t3idLst.clear();
                    //for(T3_Categories__c obj: lstT3ids1){
                        s = s1; 
                        if(mapAllT3.containsKey(s) && (mapAllT3.get(s).Term_Type__c =='Product' || mapAllT3.get(s).Term_Type__c =='Product Subgroup')){
                            parent = mapAllT3.get(s).name;
                            isEnd = true;
                        }
                    //}
                }
                else{
                    isEnd = true;   
                }
                //lstT3ids1.clear();
            }
            return parent;
            
 
    }
}