/*=======================================================================================================+
|  HISTORY  |                                                                           
|  DATE          DEVELOPER        WR                    DESCRIPTION                               
|  ====          =========        ==                    =========== 
| 03-Mar-2014    Srikrishna SM    FR-9549          This class is used to populate Comma Separated List of
                                                   Asset Campaign Junction object names on Parent Asset object
+=====================================================================================================*/

public with sharing class AssetCampaignJunctionHandler{

//processAssetCampaignJunctions
    public static void processAssetCampaignJunctions(Set<Id> assetIds){
    	Database.DMLOptions dml = new Database.DMLOptions();
		dml.allowFieldTruncation = true;
    
        //Construct a Map of Id and Junction Object passing all Parent Ids
        Map<Id, Asset_Campaign_Junction__c > assetCampaignObjectMap = new Map<Id, Asset_Campaign_Junction__c >([select Id, Name, Eligibility__c, End_date__c, External_Id__c, 
                                                                                                    Related_Asset__c, Related_Asset_Campaign__c, Related_Asset_Campaign__r.Name, Related_Asset__r.Name, Asset_Campaign_Name__c from Asset_Campaign_Junction__c 
                                                                                                    where Eligibility__c != 'Ineligible' and Related_Asset__c in:assetIds]);
        //Create a Map of Id and comma separated ids of its childs
        Map<Id, String> assetIdCampaignNameListMap = new Map<Id, String>();
        //Construct a Map of Parent Id and associated list of Child Ids
        for(Asset_Campaign_Junction__c c : assetCampaignObjectMap.values()){
            //String stringId = String.valueOf(c.Id);
            if(!assetIdCampaignNameListMap.containsKey(c.Related_Asset__c)){
                assetIdCampaignNameListMap.put(c.Related_Asset__c, c.Asset_Campaign_Name__c);
            }else{
                assetIdCampaignNameListMap.put(c.Related_Asset__c, assetIdCampaignNameListMap.get(c.Related_Asset__c)+','+c.Asset_Campaign_Name__c);
            }
        }
        //Construct a List of Parent objects
        List<Asset__c> assetList = [select id, Asset_Based_Campaigns__c from Asset__c where id in : assetIds];
        //Update Asset_Based_Campaigns__c field of all parent objects accordingly
        
        for(Asset__c asset : assetList){
        	asset.setOptions(dml);
            asset.Asset_Based_Campaigns__c = assetIdCampaignNameListMap.get(asset.Id);
        }
        if(!assetList.isEmpty())
        update assetList;
    }
    
}