/* ctrlCommunitiesUserPreferences class 
* Created by:  Matthew Roark
* Created Date: 09/09/2014
* Last Updated by:  Matthew Roark
* Last Update Date:  09/09/2014
*
* Description:  This class acts as the controller for the 'CommunitiesUserPreferences' Visualforce page
*  
* Change log:
*
* 09/09/2014 - MR - Class created.
*/
public with sharing class ctrlCommunitiesUserPreferences {

/* PUBLIC PROPERTIES */

    public String getMyUserLanguage() {
        return system.Userinfo.getLanguage();
    }

public Boolean caseCreated
{
    get; set;    
}

public Boolean caseResolved
{
    get; set;    
}

public Boolean caseAssigned
{
    get; set;    
}

public Boolean caseClosed
{
    get; set;    
}

public Boolean caseCloseAutoReply
{
    get; set;    
}

public Boolean caseEscalated
{
    get; set;    
}

public Boolean casePendingInternal
{
    get; set;    
}

/* currentUser property 
* Created by:  Matthew Roark
* Created Date: 09/09/2014
* Last Updated by:  Matthew Roark
* Last Update Date:  09/09/2014
*
* Description:  This property fetches the fields from the User record which are needed for this Visualforce page.
*  
* Change log:
*
* 09/09/2014 - MR - method created.
*/    
public User currentUser
{
    get
    {
        if(currentUser == null)
        {
            try
            {
                currentUser = [Select Id,PreSales_Case_Pending_Internal__c,PreSales_Case_Created__c, PreSales_Case_Resolved__c,PreSales_Case_Assigned__c,PreSales_Case_Closed__c, PreSales_Case_Close_AutoReply__c,PreSales_Case_Escalated__c from User where Id = :UserInfo.GetUserID() limit 1];
            }
            catch (DmlException dmlEx)
            {
                System.debug('Unable to fetch user.  Error: '+dmlEx.getMessage());
            }
        }        
        return currentUser;
    }
    private set;    
}


/* ctrlCommunitiesUserPreferences constructor 
* Created by:  Matthew Roark
* Created Date: 09/09/2014
* Last Updated by:  Matthew Roark
* Last Update Date:  09/09/2014
*
* Description:  This method acts as the constructor for the controller of the 'CommunitiesUserPreferences' Visualforce page, and sets the properties for the page.
*  
* Change log:
*
* 09/09/2014 - MR - method created.
*/
        public ctrlCommunitiesUserPreferences()
    {
        if (currentUser != null)
        {
            caseCreated = currentUser.PreSales_Case_Created__c;
            caseResolved = currentUser.PreSales_Case_Resolved__c;
            caseAssigned = currentUser.PreSales_Case_Assigned__c;
            caseClosed = currentUser.PreSales_Case_Closed__c;
            caseCloseAutoReply = currentUser.PreSales_Case_Close_AutoReply__c;
            caseEscalated = currentUser.PreSales_Case_Escalated__c;
            casePendingInternal = currentUser.PreSales_Case_Pending_Internal__c;
        }
    }


/* save() method 
* Created by:  Matthew Roark
* Created Date: 09/09/2014
* Last Updated by:  Matthew Roark
* Last Update Date:  09/09/2014
*
* Description:  This method updates the current User's record based on the values selected by the user.  If any errors are encountered, a message is displayed to the user.
*  
* Change log:
*
* 09/09/2014 - MR - method created.
*/
    public PageReference save()
    {
        try
        {
            if (currentUser != null)
            {
                 currentUser.PreSales_Case_Created__c = caseCreated;
                 currentUser.PreSales_Case_Resolved__c = caseResolved;
                 currentUser.PreSales_Case_Assigned__c = caseAssigned;
                 currentUser.PreSales_Case_Closed__c = caseClosed;
                 currentUser.PreSales_Case_Close_AutoReply__c = caseCloseAutoReply;
                 currentUser.PreSales_Case_Escalated__c = caseEscalated;
                 currentUser.PreSales_Case_Pending_Internal__c = casePendingInternal ;
                 update currentUser;
            }
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,currentUser != null ? 'Record updated' : 'User could not be identified.  Please contact support.'));
         }
         catch (Exception ex)
         {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Unable to update User.  Please try again later.'));
            System.debug('Unable to update User.  Error: '+ex.getMessage());
         }
         return null;
    }    
}