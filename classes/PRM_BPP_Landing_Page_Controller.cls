/*========================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER       WR/Req     DESCRIPTION                               
 |  ====            =========       ======     ===========  
 |  19/12/2014     Karan Shekhar               Added logic for not showing landing page to show VAR's Var
 |  23/12/2014     Karan Shekhar               Added logic to dynamically determine partner types for whom My Var's link should not be displayed
+=============================================================================================================================*/
public class PRM_BPP_Landing_Page_Controller{
    
    public Boolean displayDisti{get;set;}
    public Boolean displayDistiVar{get;set;}
    public Boolean displaySP{get;set;}
    public Boolean isVarLinkVisible{get;set;}
    public Boolean renderLandingPage{get;set;}
    Account acc = new Account();
    Id profiledAccId;
    Map<String,BusinessPartnerProgram__c> businessPartnerCSMap = BusinessPartnerProgram__c.getAll();
    Map<String,String> partnerTypeToNameMap = new  Map<String,String>();
    
    public PRM_BPP_Landing_Page_Controller(){
    }
    
    public PageReference displayScorecardLinks(){
        
        displayDisti = false;
        displayDistiVar = false;
        displaySP = false;
        isVarLinkVisible=true;
        renderLandingPage = true;
        
        profiledAccId = ApexPages.currentPage().getParameters().get('id');
        
        user loggedInUser = [Select Id,Contact.AccountId,Contact.Account.Partner_Type__c from User where Id =: userinfo.getUserId() limit 1];
        
        //Added logic to dynamically fetch partner types for whom my vars link should not be displayed - Karan,BPP 23 Dec 2014
        if(businessPartnerCSMap!=null && businessPartnerCSMap.containsKey('ScorecardInvisibility')){
            
            if((businessPartnerCSMap.get('ScorecardInvisibility').APInames__c).contains(';')) {
                
                for(String str : (businessPartnerCSMap.get('ScorecardInvisibility').APInames__c).split(';')) {
                    
                    partnerTypeToNameMap.put(str,str);
                }
            }
            else{            
                partnerTypeToNameMap.put(businessPartnerCSMap.get('ScorecardInvisibility').APInames__c,businessPartnerCSMap.get('ScorecardInvisibility').APInames__c);
            }
            
        }
        
        if(profiledAccId != null){
            acc = [select Id,Velocity_Solution_Provider_Tier__c,DMR_Tier__c,Distributor_Tier__c,Partner_Type__c,Cluster__c from Account where Id =: profiledAccId];
            
            if(acc != null){
                if(acc.Cluster__c==null || (acc.DMR_Tier__c==null && acc.Velocity_Solution_Provider_Tier__c==null && acc.Distributor_Tier__c==null)){
                    ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.INFO, System.Label.Velocity_Tier_Error_Message);
                    ApexPages.addMessage(errorMessage);
                    renderLandingPage = false;
                    return null;
                }
                
                //Added logic to check partner types for whom my vars link should not be displayed - Karan,BPP 23 Dec 2014
                if(UserInfo.getUserType() == 'PowerPartner') {
                    if(loggedInUser.Contact.Account.Partner_Type__c.contains(';')) {
                        for(String str : loggedInUser.Contact.Account.Partner_Type__c.split(';')) {
                            if(partnerTypeToNameMap!=null && partnerTypeToNameMap.containsKey(str)) {                       
                                isVarLinkVisible=false;
                                break;
                            }
                            isVarLinkVisible=true;                            
                        }
                    }
                    else{
                        if(partnerTypeToNameMap.containsKey(loggedInUser.Contact.Account.Partner_Type__c)) {
                            isVarLinkVisible=false;
                        }
                        else{
                            isVarLinkVisible=true;
                        }
                    }
                }
            }
        }
        
        //addedlogic to not show landing page for Var's Var : Karan, BPP 19 Dec 2014
        Id currentUserAccId= loggedInUser.Contact.AccountId;
        
        if(currentUserAccId!= null) {
        
            if(currentUserAccId!=profiledAccId) {                
                isVarLinkVisible=false;
            }
        }// changes end:  Karan, BPP 19 Dec 2014

        if(acc !=null){
            if(acc.Distributor_Tier__c !=null && acc.Distributor_Tier__c !=''){
                displayDisti = true;
                displayDistiVar = true;
            }
            if((acc.Velocity_Solution_Provider_Tier__c !=null && acc.Velocity_Solution_Provider_Tier__c !='') || (acc.DMR_Tier__c !=null && acc.DMR_Tier__c !='')){
            
             // added logic to redirect to scorecards rather than showing landing page,Karan, BPP 19 Dec 2014
                if(displayDisti == false || isVarLinkVisible == false){
                    displaySP = false;
                    return new PageReference('/apex/PRM_VPP_Velocity_Scorecard?id='+acc.Id);
                }
                else{
                    displaySP = true;
                }
            }
        }        
        return null;
    }
    
    //Method to return the URL to the apex page for Distributor Scorecard
    public PageReference getdistiLink() {    
        return new PageReference('/apex/PRM_BPP_Disti_ScoreCard?id='+acc.Id);
    }
    
    //Method to return the URL to the apex page for Distribution VAR Scorecard
    public PageReference getdistiVAR() {    
        return new PageReference('/apex/PRM_BPP_Disti_Scorecard_List?id='+acc.Id);
    }
    
    //Method to return the URL to the apex page for Solution Provider Scorecard
    public PageReference getsolProvider() {    
        return new PageReference('/apex/PRM_VPP_Velocity_Scorecard?id='+acc.Id);
    }
        
    //Method to display Cancel button in case Landing Page is not rendered    
    public pageReference cancelPage(){
        PageReference returnPage = new PageReference('/'+profiledAccId);
        returnPage.setredirect(true);
        return returnPage;
    }
}