/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER               WR          DESCRIPTION                               
 |  ====            =========               ==          =========== 
 |  19/Jan/2015      Bisna V P              CI 1631     Initial Creation.
 ===========================================================================+*/
@isTest
public class KBArticleSearchReplaceStatus_TC{
    static testMethod void KBArticleSearchReplaceStatus(){        
        Test.startTest(); 
        KBArticleSearchReplaceStatus kb= new KBArticleSearchReplaceStatus();
        kb.getsearchDispHistoryList();
        kb.getsearchDispArtList();
        kb.getstatus();
        Test.stopTest();
    }
}