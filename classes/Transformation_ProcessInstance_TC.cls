/*=============================================================================
    HISTORY                                                                  
                                                               
    DATE            DEVELOPER                WR             DESCRIPTION                               
    ====            =========                ==             =========== 
|  06/12/2013       Aagesh Jose            311032   Re-written test class to improve performance.
   04/10/2015       Amit Giri              PROPEL   Modify "Transformation_ProcessInstanceController" as part of TC Fix
==============================================================================*/@isTest
private class Transformation_ProcessInstance_TC {
 public Static List<Account> acc = new List<Account>();
 //public Static List<Opportunity> lstOpp= new List<Opportunity>();
 public Static List<Pricing_Requests__c> lstPricing = new List<Pricing_Requests__c>();
 List<Opportunity> opp_Obj = new List<Opportunity>();

 public void createOpportunity(){
                                 
                                 List<User> lstUser = [Select id from user where IsActive = true and Theater__c in ('APJ')limit 4];
                                 for(Integer i=0;i<3;i++){ 
                                      Opportunity newOpportunity=new Opportunity(
                                      AccountId =acc[0].id,
                                      Opportunity_Owner__c = lstUser[i].id,
                                      Name = 'TEST'+i,
                                      StageName = 'TEST'+i,
                                      CloseDate = Date.today()+10,
                                      Sales_Force__c = 'EMC',
                                      Sales_Channel__c = 'Direct',
                                      VMWare_Related__c = 'VMWare Backup',                                      
                                      Amount = 222.00);
                                      opp_Obj.add(newOpportunity);
                                    }
                                    Database.insert(opp_Obj);
                                    
                                }  //end of method Creation Of Opportunity
         
 public void createPricingRequest(){
                                    List<Pricing_Requests__c> lstPric = new List<Pricing_Requests__c>();
                                    for(Integer i=0;i<3;i++){   
                                        Pricing_Requests__c priReq_Obj = new Pricing_Requests__c();
                                        priReq_Obj.Opportunity_Name__c = opp_Obj[i].id;
                                        priReq_Obj.Price_Floor_Level__c = 'L4';
                                        priReq_Obj.How_can_we_mask_actual_discount__c = 'TEST 1234';
                                        priReq_Obj.Approval_Status__c ='New';
                                        priReq_Obj.APJ_Request_Escalation__c =false;
                                        priReq_Obj.Recalled_Flag__c=false;
                                        priReq_Obj.EMC_PRODUCTS__c='test';
                                        lstPric.add(priReq_Obj);
                                    }
                                    Database.insert(lstPric);
                                   
                                    lstPricing =lstPric;
                                } ///end of method Creation Of Pricing Requests 
  
static testMethod void Transformation_ProcessInstanceController()
    {   
     Transformation_ProcessInstance_TC instanceObj = new Transformation_ProcessInstance_TC();
     User insertUser = [Select id from User where isActive=true and profile.Name='System Administrator' limit 1];            
     System.runAs(insertUser)
     {
         CustomSettingDataHelper.dataValueMapCSData();
         CustomSettingDataHelper.eBizSFDCIntCSData();
         CustomSettingDataHelper.profilesCSData();
         CustomSettingDataHelper.bypassLogicCSData();
     }
     acc =AccountAndProfileTestClassDataHelper.CreateCustomerAccount();      
     insert acc;
     
     //acc = [Select ID,Synergy_Account_Number__c  from Account where Name =:acc[0].Name];
     
     //Amit: Commented below code as part of TC failure Fix
     /*** START ***/
      /* //Creation of Customer Accounts
        Account objAccount = new Account();
        objAccount.name = 'UNITTESTAcc';
        objAccount.Party_Number__c = '1234';        
        objAccount.BillingCountry ='Colombia';
        objAccount.Synergy_Account_Number__c = '10';
        objAccount.Status__c='A';
        objAccount.State_Province_Local__c = 'testState1';
        objAccount.Street_Local__c='test1';
        objAccount.Zip_Postal_Code_Local__c='23454';
        //objAccount.DealReg_Address_Local__c='test2';
        acc.add(objAccount);
        
        Account objAccount1 = new Account();
        objAccount1.name = 'UNITTESTAcc1';
        objAccount1.Party_Number__c = '12345';        
        objAccount1.BillingCountry ='Colombia';
        objAccount1.Synergy_Account_Number__c = '10';
        objAccount1.Status__c='A';
        objAccount1.State_Province_Local__c = 'testState2';
        objAccount1.Street_Local__c='test1';
        objAccount1.Zip_Postal_Code_Local__c='23454';
        //objAccount1.DealReg_Address_Local__c='test2';
        acc.add(objAccount1);
        
        Account objAccount2 = new Account();
        objAccount2.name = 'UNITTESTAcc1';
        objAccount2.Party_Number__c = '123456';        
        objAccount2.BillingCountry ='Colombia';
        objAccount2.Synergy_Account_Number__c = '10';
        objAccount2.Status__c='A';
        objAccount2.State_Province_Local__c = 'testState3';
        objAccount2.Street_Local__c='test1';
        objAccount2.Zip_Postal_Code_Local__c='23454';
        //objAccount2.DealReg_Address_Local__c='test2';
        acc.add(objAccount2);
        
        Account objAccount3 = new Account();
        objAccount3.name = 'UNITTESTAcc1';
        objAccount3.Party_Number__c = '123457';        
        objAccount3.BillingCountry ='Colombia';
        objAccount3.Synergy_Account_Number__c = '10';
        objAccount3.Status__c='A';
        objAccount3.State_Province_Local__c = 'testState3';
        objAccount3.Street_Local__c='test1';
        objAccount3.Zip_Postal_Code_Local__c='23454';
        //objAccount3.DealReg_Address_Local__c='test2';
        acc.add(objAccount3);
        
        Insert acc;    */
             
     /*** END ***/
        
     instanceObj.createOpportunity();
     instanceObj.createPricingRequest();
     
     Test.StartTest();
     System.runAs(insertUser)
     {
        PageReference pageRef = Page.Transformation_RecallVF; 
        Test.setCurrentPage(pageref);
        ApexPages.currentPage().getParameters().put('id', lstPricing[0].ID); 
        Transformation_ProcessInstanceController  controller = new Transformation_ProcessInstanceController();
        String  nextPage = controller.recall().getUrl();
        String  nextPageCancel = controller.Cancel().getUrl();
     }
      Test.stopTest();
    }
}