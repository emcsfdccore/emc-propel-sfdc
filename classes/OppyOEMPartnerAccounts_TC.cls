/*=====================================================================================================+
|  HISTORY  | |  DATE          DEVELOPER     WR         DESCRIPTION  |  
====          =========                      ==         ===========  |
               12/Dec/2013    Bisna V P                This class is used to unit test OppyOEMPartnerAccounts
               10/Jan/2014    Aagesh Jose              Modified based on the change in base class for WR#340066
               18/Jun/2014    Abinaya M S   CI:1013    Called resetOpptyBypassValidation method to increase the code coverage.            
               19 Feb 2015    Vinod Jetti   #1649      created Hub_Info__c objects data instead of Account 'Emc_classification__c' field
+=====================================================================================================*/
@isTest
Private class OppyOEMPartnerAccounts_TC{  
    
    public static testmethod void methodOppyOEMPartnerAccounts(){
    
    /*Test.startTest();   
    
    
    
    List<RecordType> rectypelist = [select id,name from RecordType where name in ('Partner Account Record Type','Customer Account Record Type')];
    List<Account> listAcc = new List<Account>();
    
    Account acc= new Account();
    acc.Name = 'abcd';
    //insert acc;
    Opportunity opp = new Opportunity();
    opp.Name = 'Opp with partner';
   // opp.Account = acc ;
    opp.StageName = 'pipeline';
    opp.CloseDate = Date.newInstance(2013,12,1);
    //opp.OEM_Partner__c = .id;
    
    
    Test.stopTest();*/
   map<id,Opportunity> Map_OldOpp = new map<id,Opportunity>();
   CustomSettingBypassLogic__c custset =  new CustomSettingBypassLogic__c();
   custset.By_Pass_Account_Triggers__c = true;
   custset.By_Pass_Account_Validation_Rules__c = true;
   custset.By_Pass_Opportunity_Triggers__c = true;
   custset.By_Pass_Opportunity_Validation_Rules__c = true;
   insert custset;
   Set<Id> opptyIdSet = new Set<Id>();
    //#1649 - Start
    Hub_Info__c objHubInfo = new Hub_Info__c();
    objHubInfo.EMC_CLASSIFICATION__c = 'EMC Classification';
    objHubInfo.Golden_Site_Identifier__c = 305051;
    insert objHubInfo;
    //#1649 - End    
    Account acc= new Account();
    acc.Name = 'abcd';
    acc.PROFILED_ACCOUNT_FLAG__c = true;
    acc.Type = 'Partner';
    acc.Status__c = 'A';
    acc.Partner_Type_Picklist__c = 'OEM Partner';
    acc.Partner_Type__c = 'OEM';
    acc.Hub_Info__c = objHubInfo.Id;
    //acc.EMC_Classification__c = 'EMC Classification';    
    insert acc;
    Hub_Info__c objHubInfo1 = new Hub_Info__c();
    objHubInfo1.EMC_CLASSIFICATION__c = 'EMC Classification';
    objHubInfo1.Golden_Site_Identifier__c = 305052;
    insert objHubInfo1;
    Account acc1= new Account();
    acc1.Name = 'abcd1';
    acc1.PROFILED_ACCOUNT_FLAG__c = true;
    acc1.Type = 'Customer';
    acc1.Status__c = 'A';
    acc1.Partner_Type_Picklist__c = 'OEM Partner';
    acc1.Partner_Type__c = 'OEM';
    acc1.Hub_Info__c = objHubInfo1.Id;
    //acc1.EMC_Classification__c = 'EMC Classification';    
    insert acc1;    
    list<Opportunity> oppList = new list<Opportunity>(); 
    Opportunity opp1 = new Opportunity(
        name = 'test opp 1',
        StageName = 'Oferecer projeto/oramento',  
        Account = acc,   
        OEM_Partner__c = acc.id,  
        Accountid = acc.id,     
        Sales_Channel__c = 'Direct',
        Direct_Deal_Override__c = true, 
        CloseDate = system.today());     
    insert opp1;
    oppList.add(opp1);
    opptyIdSet.add(opp1.id);
    Opportunity opp2 = new Opportunity(
        name = 'test opp 2',
        StageName = 'Oferecer projeto/oramento',   
        Account = acc,
        OEM_Partner__c = acc1.id,  
        Sales_Channel__c = 'Direct',
        Direct_Deal_Override__c = false,
        Accountid = acc.id,      
        CloseDate = system.today()) ;
        insert opp2;
        oppList.add(opp2);
        opptyIdSet.add(opp2.id);            
    Map_OldOpp.put(opp1.id,opp2);
    Map_OldOpp.put(opp2.id,opp2);
    system.debug('Map_OldOpp****'+Map_OldOpp);
    CustomSettingDataHelper.dataValueMapCSData();
    Test.startTest(); 
    OppyOEMPartnerAccounts oppyOEMClass = new OppyOEMPartnerAccounts();
    oppyOEMClass.OEMPartnerAccounts(oppList);
    oppyOEMClass.verifiedOEM = false;
    //OppyOEMPartnerAccounts oppyOEMClass1 = new OppyOEMPartnerAccounts();
    oppyOEMClass.OEMPartnerAccountsUpdate(oppList, Map_OldOpp);
    oppyOEMClass.salesChannel(oppList, Map_OldOpp);         //Modified for WR#340066
    //Called resetOpptyBypassValidation method to increase the code coverage. CI:1013           
    OppyOEMPartnerAccounts.resetOpptyBypassValidation(opptyIdSet);
    Test.stopTest();
    }

}