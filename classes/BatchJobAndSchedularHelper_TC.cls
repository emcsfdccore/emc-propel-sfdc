/*=====================================================================================================+
 |  HISTORY  |
 |  DATE          DEVELOPER               WR                           DESCRIPTION 
 |  ====          =========               ==                            =========== 
 | 18/feb/2014     Aarti Jindal         Visibility Program          Test Class for BatchJobAndSchedularHelper
 |                                                                  class.                               
 +=====================================================================================================*/
@isTest(seealldata=true)
private class BatchJobAndSchedularHelper_TC{
    
        static testMethod void schedularHelperUnitTest(){
        Test.startTest();
        if(BatchJobAndSchedularManager__c.getvalues('Account')==null)
        {
        CustomSettingDataHelper.BatchJobAndSchedularmanagerData();
        }
        if(CustomSettingDataValueMap__c.getvalues('ReschdAT_BatchDeleteAccountTeamMembers')==null || CustomSettingDataValueMap__c.getvalues('ReschdDelete_TA_Sync_Log_Batch')==null)
        {
        CustomSettingDataHelper.BatchReschedulDataValueMapCSData();
        }
        Boolean ATjobFlag=BatchJobAndSchedularHelper.isRescheduleJob('Account','AT_BatchDeleteAccountTeamMembers');
        Boolean logSyncjobFlag=BatchJobAndSchedularHelper.isRescheduleJob('Account','Delete_TA_Sync_Log_Batch');
        Test.stopTest();

        }

}